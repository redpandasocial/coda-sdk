(function($, window, document, undefined) {

	__coda_editor = undefined;

	$.fn.coda = function(options) {
		$(this).each(function() {
			var _coda_options = $.extend({}, $.fn._coda_options, options);

			if ($(this).attr('data-editable') == 'true') {
				$(this).on('focus', function() {
					$(this).css('outline', '2px dotted ' + $(this).css('color'));
				})
				$(this).on('blur', function() {
					$(this).css('outline', 'none');
					$(this).saveContent();
				})
				$(this).attr('contenteditable', 'true');

				if ($(this).attr('data-richeditor') == 'true') {
					CKEDITOR.inline($(this).get(0), {
						language : 'pt',
						coreStyles_bold : {
							element : 'b'
						},
						coreStyles_italic : {
							element : 'i'
						},
						fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
						fontSize_style : {
							element : 'font',
							attributes : {
								'size' : '#(size)'
							}
						},
						toolbar : [
						[ 'Bold', 'Italic', 'Underline' ],
						[ 'NumberedList', 'BulletedList', '-', 'Blockquote' ],
						[ 'Source', '-', 'Undo', 'Redo' ],
						[ 'Link', 'Unlink', '-', 'RemoveFormat' ],
						[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight' ],
						[ 'FontSize', 'Format' ]
						]
					});
				}

				switch ($(this).get(0).tagName.toLowerCase()) {
					case 'input' : {
						var type = $(this).attr('type');
						if (type != 'button' && type != 'submit') {
							break;
						}
					}
					case 'a' :
					case 'button' : {
						$(this).on('click', function(event) {
							if (event.button == 1) {
								event.preventDefault();
								return false;
							}
						});
						$(this).on('contextmenu', function(event) {
							event.preventDefault();
							return false;
						});
						break;
					}
				}
			}

			$(this).children().coda(_coda_options);
		});
	};

	$.fn.saveContent = function() {
		var req = { content : $('html').html() };
		$.post($('#k_server_url').val() + "/applications/self/save", { content : $('html').html() }, function( data ) {
		}, "json");
	};

	$.fn._coda_options = {
	};

})(jQuery, window, document);

$(document).ready(function() {
	var referrer = window.document.referrer;
	if (window.top != window.self && referrer.indexOf('//my.redpanda') != -1) {
		referrer = referrer.substring(referrer.indexOf('://') + 3);
		referrer = referrer.substring(0, referrer.indexOf('/'));

		var cke = window.document.createElement('script');
		cke.charset = 'utf-8';
		cke.src = '//' + referrer + '/ckeditor/ckeditor.js'
		window.document.getElementsByTagName('head')[0].appendChild(cke);

		var waitForCKEditor = setInterval(function() { 
			if (!!CKEDITOR) {
				clearInterval(waitForCKEditor);
				CKEDITOR.disableAutoInline = true;
				$('html').coda();
			}
		}, 1000);
		/*var nTries = 0;
		var waitForFB = setInterval(function() { 
			nTries++;
			if (nTries == 20) {
				clearInterval(waitForFB);
			}
			if (!!FB && !!FB.Canvas) {
				clearInterval(waitForFB);

				FB.Canvas.__setSize = FB.Canvas.setSize;
				FB.Canvas.__scrollTo = FB.Canvas.scrollTo;

				FB.Canvas.setSize = function(size) {
					FB.Canvas.__setSize(size);
				};

				FB.Canvas.scrollTo = function(size) {
					FB.Canvas.__scrollTo(size);
				};
			} 
		}, 1000);*/
	}
});