
var scripts = document.getElementsByTagName('script');
var index = scripts.length - 1;
var _script = scripts[index];

var Base64 = {
	
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	
	// public method for encoding
	encode : function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		
		input = Base64._utf8_encode(input);
		
		while (i < input.length) {
			
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
			
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			}
			else if (isNaN(chr3)) {
				enc4 = 64;
			}
			
			output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
			
		}
		
		return output;
	},
	
	// public method for decoding
	decode : function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		
		while (i < input.length) {
			
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
			
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			
			output = output + String.fromCharCode(chr1);
			
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
			
		}
		
		output = Base64._utf8_decode(output);
		
		return output;
		
	},
	
	// private method for UTF-8 encoding
	_utf8_encode : function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";
		
		for ( var n = 0; n < string.length; n++) {
			
			var c = string.charCodeAt(n);
			
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			
		}
		
		return utftext;
	},
	
	// private method for UTF-8 decoding
	_utf8_decode : function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
		
		while (i < utftext.length) {
			
			c = utftext.charCodeAt(i);
			
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
			
		}
		
		return string;
	}

};

var PopupBlock = {
	check: function(popup_window){
		if (popup_window) {
			if(/chrome/.test(navigator.userAgent.toLowerCase())){
				setTimeout(function () {
					PopupBlock._is_popup_blocked(PopupBlock, popup_window);
				},200);
			}else{
				popup_window.onload = function () {
					PopupBlock._is_popup_blocked(PopupBlock, popup_window);
				};
			}
		}else{
			PopupBlock.popupBlocked();
		}
		return popup_window;
	},
	_is_popup_blocked: function(scope, popup_window){
		if ((popup_window.innerHeight > 0)==false){
			scope.popupBlocked(); 
		}
		delete popup_window;
	},
	popupBlocked : function () {
		if (!!translations && !!translations.popup_error) {
			alert(translations.popup_error);
		}
		else {
			alert('A pop-up blocker is preventing you from continuing your interaction with this page.\nPlease, add this domain as an exception in order to proceed.');
		}
	}
};

(function($, window, document, undefined) {
	var __coda_tag_tainted = false;

	$.fn.coda = function(options) {
		if (!!options) {
			$.fn._coda_options = $.extend({}, $.fn._coda_options, options);
		}

		$(this).each(function() {
			/*var tagName = $(this). prop("tagName").toLowerCase();
			if (tagName == 'a') {
				$(this).attr('href', '#');
			}
			$(this).off('click', '**');;
			if (!!$(this).prop('onclick')) {
				$(this).prop('onclick', '');
			}*/

			if ($(this).attr('data-editable') == 'true') {
				$(this).on('focus', function() {
					$(this).css('outline', '2px dotted ' + $(this).css('color'));
				});
				$(this).on('blur', function() {
					$(this).css('outline', 'none');
					if (__coda_tag_tainted) {
						__coda_tag_tainted = false;
						$(this).saveContent();
					}
				});
				$(this).on('keypress', function() {
					__coda_tag_tainted = true;
					window.parent.postMessage({ action : 'tainted' }, '*');
				});
				$(this).on('keydown', function() {
					__coda_tag_tainted = true;
					window.parent.postMessage({ action : 'tainted' }, '*');
				});
				$(this).on('paste', function(e) {
					if (e.preventDefault) {
						e.preventDefault();
					}
					e.returnValue = false;
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					e.cancelBubble = true;
					var text = (e.originalEvent || e).clipboardData.getData('text/plain');
					document.execCommand('insertText', false, text);
				});
				$(this).attr('contenteditable', 'true');

				if ($(this).attr('data-richeditor') == 'true') {
					$(this).on('focus', function() {
						/*for(var name in CKEDITOR.instances) {
							var instance = CKEDITOR.instances[name];
							if(this && this == instance.element.$) {
								return;
							}
						}*/
						CKEDITOR.inline($(this).get(0), {
							language : 'pt',
							coreStyles_bold : {
								element : 'b'
							},
							coreStyles_italic : {
								element : 'i'
							},
							fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
							fontSize_style : {
								element : 'font',
								attributes : {
									'size' : '#(size)'
								}
							},
							toolbar : [
							[ 'Bold', 'Italic', 'Underline' ],
							[ 'NumberedList', 'BulletedList', '-', 'Blockquote' ],
							[ 'Source', '-', 'Undo', 'Redo' ],
							[ 'Link', 'Unlink', '-', 'RemoveFormat' ],
							[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight' ],
							[ 'FontSize', 'Format' ]
							]
						});
					});
					$(this).on('blur', function() {
						for(var name in CKEDITOR.instances) {
							var instance = CKEDITOR.instances[name];
							if(this && this == instance.element.$) {
								instance.destroy();
							}
						}
					});
				}

				switch ($(this).get(0).tagName.toLowerCase()) {
					case 'input' : {
						var type = $(this).attr('type');
						if (type != 'button' && type != 'submit') {
							break;
						}
					}
					case 'a' :
					case 'button' : {
						$(this).on('click', function(event) {
							if (event.button == 1) {
								event.preventDefault();
								return false;
							}
						});
						$(this).on('contextmenu', function(event) {
							event.preventDefault();
							return false;
						});
						break;
					}
				}
			}

			$(this).children().coda();
		});
	};

	$.fn.saveContent = function() {
		if ($('#k_access_token').val() != '') {
			var page = window.document.location.href.split('?')[0];
			if (page.indexOf('.html') == -1) {
				if (page.lastIndexOf('/') == page.length - 1) {
					page += 'index.html';
				}
				else {
					page += '.html';
				}
			}
			var req = { 
				directive : 'POST',
				uri : '/applications/self/save/frontend',
				body : { 
					type : "facebook", 
					uri : page,
					selector_path : $(this).selectorPath(), 
					js_path : $(this).javascriptPath(), 
					dom_path : $(this).domPath(), 
					content : Base64.encode($(this).html()) 
				}
			};

			$.ajax({
				type : 'post',
				contentType : 'application/json;charset=utf-8',
				dataType : 'json',
				data : JSON.stringify(req),
				url: $('#k_server_url').val() + "/deferrer/actions/execute",
				headers : { 'Authorization' : 'OAuth2.0 ' + $.fn._coda_options.token }
			}).complete(function(response) {
				window.parent.postMessage({action : 'saved' }, '*');
			});
		}
	};

	$.fn.selectorPath = function() {
		var ret = '';
		var filter = '[data-editable=true]';

		for (var element = $(this).get(0); !!element && element.tagName.toLowerCase() != 'html'; element = element.parentNode) {
			if (element.nodeType != 1) {
				continue;
			}
			if (ret.length != 0) {
				ret = ' ' + ret;
			}

			if (!!element.id && element.id != '') {
				ret = element.tagName.toLowerCase() + '#' + element.id + ret;
			}
			else if (!!element.className && element.className != '') {
				var cname = element.className.trim().split(" ")[0];
				if (cname.indexOf('cke_') != -1) {
					ret = element.tagName.toLowerCase() + ret;
				}
				else {
					ret = element.tagName.toLowerCase() + '.' + cname + ret;
				}
			}
			else {
				ret = element.tagName.toLowerCase() + ret;
				/*var nth = 1;
				if (!!element.parentNode) {
					var nodeList = Array.prototype.slice.call(element.parentNode.childNodes);
					nth = nodeList.indexOf(element) + 1 + (element.parentNode.tagName.toLowerCase() == 'html' ? 1 : 0);
				}
				ret = element.tagName.toLowerCase() + ':nth-child(' + nth + ')' + ret;*/
			}
		}
		ret = 'html ' + ret;// + filter;
		return ret;
	};

	$.fn.domPath = function() {
		var ret = '';
		for (var element = $(this).get(0); !!element && element.tagName.toLowerCase() != 'html'; element = element.parentNode) {
			if (element.nodeType != 1) {
				continue;
			}
			if (ret.length != 0) {
				ret = ' / ' + ret;
			}
			ret = element.tagName.toLowerCase() + ret;
		}
		ret = 'html / ' + ret;
		return ret;
	};

	$.fn.javascriptPath = function() {
		var ret = '';
		for (var element = $(this).get(0); !!element && element.tagName.toLowerCase() != 'html'; element = element.parentNode) {
			if (ret.length != 0) {
				ret = '.' + ret;
			}
			ret = 'childNodes[' + (!!element.parentNode ? Array.prototype.indexOf.call(element.parentNode.childNodes, element) : 0) + ']' + ret;
		}
		ret = 'document.' + ret;
		return ret;
	};

	$.fn._coda_options = {
		token : undefined
	};

})(jQuery, window, document);

$(document).ready(function() {
	var referrer = window.document.referrer;
	var refindex = referrer.indexOf('//');
	var refdomainstr = referrer.substring(refindex + 2, referrer.indexOf('/', refindex + 2));
	var refdomain = refdomainstr.split('.');
	var selfdomain = window.document.location.host.split('.');
	var selfmaindomain = selfdomain[selfdomain.length - 2] + '.' + selfdomain[selfdomain.length - 1];

	if (window.top != window.self && refdomainstr.indexOf(selfmaindomain) != -1) {
		var $_GET = {};
		window.document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
			function decode(s) {
				return decodeURIComponent(s.split("+").join(" "));
			}
			$_GET[decode(arguments[1])] = decode(arguments[2]);
		});

		var ecount = 0;
		for (var i = refdomain.length, j = selfdomain.length; i != 0 && j != 0; i--, j--) {
			if (refdomain[i] != selfdomain[j]) {
				if (ecount < 2) {
					return;
				}
				break;
			}
			ecount++;
		}
		if (refdomain[0] != 'prototype') {

			var cke = window.document.createElement('script');
			cke.charset = 'utf-8';
			cke.src = '//' + refdomain.join('.') + '/1.0/ckeditor/ckeditor.js'
			window.document.getElementsByTagName('head')[0].appendChild(cke);

			var waitForCKEditor = setInterval(function() { 
				if (!!CKEDITOR) {
					clearInterval(waitForCKEditor);
					setTimeout(function(){
						CKEDITOR.disableAutoInline = true;
						$('html').coda({
							token : $_GET["token"]
						});
					}, 1000);
				}
			}, 1000);
		}

		var args = _script.src.split('?');

		if (args.length == 1) {
			var hb = $('body').height();
			window.parent.postMessage({action : 'resize', height : hb}, '*');
			var resizeBody = setInterval(function() { 
				if ($('body').height() != hb) {
					hb = $('body').height();
					window.parent.postMessage({action : 'resize', height : hb}, '*');
				}
			}, 1000);
		}
		else {
			var hb = args[1].replace('h=', '');
			window.parent.postMessage({action : 'resize', height : hb}, '*');
		}

		window._open = window.open;
		window.open = function(url, name, specs, replace) {
			return PopupBlock.check(window._open(url, name, specs, replace));
		};
	}
});
