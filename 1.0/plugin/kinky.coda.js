function load_coda() {
	var referrer = window.document.referrer;
	referrer = referrer.substring(referrer.indexOf('://') + 3);
	referrer = referrer.substring(0, referrer.indexOf('/'));

	var cke = window.document.createElement('script');
	cke.charset = 'utf-8';
	cke.src = '//' + referrer + '/ckeditor/ckeditor.js'
	window.document.getElementsByTagName('head')[0].appendChild(cke);

	CODA = {
		ACTIVE_SHELL : null,
		OVER_WIDGET : null,
		OVER_LAYOUT : {
			className : ''
		},
		moving : false,

		isChild : function(widget, id) {
			if (widget.id == id) {
				return true;
			}

			for ( var c in widget.childWidgets()) {
				if (widget.childWidget(c).id == id) {
					return true;
				}
			}

			for ( var c in widget.childWidgets()) {
				if (window.parent.CODA.isChild(widget.childWidget(c), id)) {
					return true;
				}
			}

			return false;
		},

		setState : function() {
			for ( var c in Kinky.SHELL_CONFIG) {
				window.parent.document.location.href = window.parent.document.location.href.split('#')[0] + '#/load-page' + Kinky.SHELL_CONFIG[c].startPage;
				break;
			}

			// >>>
			// TOOLBAR CREATION
			{
				
				var buttons = window.document.createElement('div');
				{
					buttons.className = ' CODAWidgetSmallEditButtons ';
					
					var button = window.document.createElement('button');
					{
						button.innerHTML = '<i class="fa fa-pencil"></i>';
						button.title = window.parent.gettext('$CODA_WIDGET_TOOLBAR_EDIT');
						button.className = '  CODAWidgetSmallEditButton CODAWidgetSmallButton ';
						KDOM.addEventListener(button, 'click', CODA.editWidget);
					}
					buttons.appendChild(button);

					var delButton = window.document.createElement('button');
					{
						delButton.innerHTML = '<i class="fa fa-trash-o"></i>';
						delButton.title = window.parent.gettext('$CODA_WIDGET_TOOLBAR_DELETE');
						delButton.className = '  CODAWidgetSmallRemoveButton CODAWidgetSmallButton ';
						KDOM.addEventListener(delButton, 'click', CODA.removeWidget);
					}
					buttons.appendChild(delButton);
					
					var saveButton = window.document.createElement('button');
					{
						saveButton.innerHTML = '<i class="fa fa-save"></i>';
						saveButton.title = window.parent.gettext('$CODA_WIDGET_TOOLBAR_SAVE');
						saveButton.className = '  CODAWidgetSmallRemoveButton CODAWidgetSmallButton ';
						KDOM.addEventListener(saveButton, 'click', CODA.updateWidget);
					}
					buttons.appendChild(saveButton);

					var addButton = window.document.createElement('button');
					{
						addButton.innerHTML = '<i class="fa fa-plus"></i>';
						addButton.title = window.parent.gettext('$CODA_WIDGET_TOOLBAR_ADD');
						addButton.className = '  CODAWidgetSmallRemoveButton CODAWidgetSmallButton ';
						KDOM.addEventListener(addButton, 'click', CODA.addWidget);
					}
					buttons.appendChild(addButton);

					KCSS.setStyle({
						display : 'none'
					}, [
					buttons
					]);
				}

				CODA.TOOLBAR_BUTTONS = buttons;
				window.document.body.appendChild(CODA.TOOLBAR_BUTTONS);
			}
			
			// >>>
			// EDITING TOOLBAR
			{
				CKEDITOR.disableAutoInline = true;
			}


			// >>>
			// KWIDGET MODS
			{
				KWidget.prototype.CODA_visible = function() {
					if ((KSystem.isIncluded('KShellPart') && this instanceof KShellPart)) {
						this.addLocationListener(function(widget, hash) {
							if (widget.activated()) {
								window.parent.KBreadcrumb.dispatchURL({
									hash : '/load-page' + hash
								});
							}
						});
					}

					if (!!this.config && !!this.config.href) {
						for (var c in CODA.EDIT_MODE) {
							var is_instance = false;
							try {
								eval('is_instance = this instanceof ' + c);
							}
							catch (e) {
								is_instance = false;
							}

							if (is_instance) {
								for (var f in CODA.EDIT_MODE[c].fields) {
									var node = this[CODA.EDIT_MODE[c].fields[f]];
									if (node !== undefined) {
										// >>> 
										// Add editing capabilities to the node
										{
											node.setAttribute('contenteditable', 'true');
											if (!!CODA.EDIT_MODE[c].richtext && CODA.EDIT_MODE[c].richtext[CODA.EDIT_MODE[c].fields[f]]) {
												this.CODA_hash_richtext = true;
												CKEDITOR.inline(node, {
													language : 'en',
													coreStyles_bold : {
														element : 'b'
													},
													coreStyles_italic : {
														element : 'i'
													},
													fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
													fontSize_style : {
														element : 'font',
														attributes : {
															'size' : '#(size)'
														}
													},
													toolbar : [
														[ 'Bold', 'Italic', 'Underline' ],
														[ 'NumberedList', 'BulletedList', '-', 'Blockquote' ],
														[ 'Source', '-', 'Undo', 'Redo' ],
														[ 'Link', 'Unlink', '-', 'RemoveFormat' ],
														[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight' ],
														[ 'FontSize', 'Format' ]
													]
												});
											}
										}
										// <<<

										// >>>
										// Process special cases, such has images and links
										switch (node.tagName.toLowerCase()) {
											case 'img': {
												break;
											}
											case 'a' : {
												break;
											}
										}
										// <<<
									}
								}
							}
						}

						this.addEventListener('click', function(event) {
							KDOM.stopEvent(event);
							var widget = CODA.searchWidget(KDOM.getEventWidget(event));

							if (CODA.FOCUSED_WIDGET !== undefined && CODA.FOCUSED_WIDGET.id == widget.id) {
								return;
							}

							if (CODA.FOCUSED_WIDGET !== undefined) {
								CODA.hideActions(CODA.FOCUSED_WIDGET);
								CODA.FOCUSED_WIDGET = undefined;
							}
							CODA.showActions(widget);
							CODA.FOCUSED_WIDGET = widget;
						});
					}
					this.default_visible();
				};

				KWidget.prototype.CODA_invisible = function() {
					if (CODA.FOCUSED_WIDGET !== undefined && CODA.FOCUSED_WIDGET.id == this.id) {
						KCSS.setStyle({
							display : 'none'
						}, [
						CODA.TOOLBAR_BUTTONS
						]);
						CODA.hideActions(CODA.FOCUSED_WIDGET);
						CODA.FOCUSED_WIDGET = undefined;
					}
					this.default_invisible();
				};

			}
			// <<<
			
			// >>>
			// KLIST MODS
			{
				KList.prototype.CODA_onNoContent = function(data, request) {
					if (!this.feed_retrieved && !this.no_feed) {
						this.retrieveFeed(data, request);
						return;
					}
					else {
						this.no_feed = !this.data || !this.data.feed || (this.data.feed.size == 0);
					}
					
					if (this.feed_retrieved && !this.no_children && !this.children_retrieved) {
						this.retrieveChildren(data, request);
						return;
					}
					else {
						this.no_children = !this.data || !this.data.children || (this.data.children.size == 0);
					}
					
					this.draw();
				};
			}
			// <<<

			return true;
		},

		editWidget : function(event) {
			var widget = CODA.FOCUSED_WIDGET;
			window.parent.KBreadcrumb.dispatchURL({
				hash : '/edit' + CODA.searchWidgetHref(widget)
			});
		},
		
		addWidget : function(event) {
			var widget = CODA.FOCUSED_WIDGET;
			window.parent.KBreadcrumb.dispatchURL({
				hash : '/add-to' + CODA.searchWidgetHref(widget)
			});
		},
		
		removeWidget : function(event) {
			var widget = CODA.FOCUSED_WIDGET;
			window.parent.KBreadcrumb.dispatchURL({
				hash : '/remove' + CODA.searchWidgetHref(widget)
			});
		},
		
		moveWidget : function(event) {
			var widget = CODA.FOCUSED_WIDGET;
			var parent = widget.parent;
			while (!parent.config.href) {
				parent = parent.parent;
			}
			
			window.parent.KBreadcrumb.dispatchURL({
				hash : '/move' + parent.config.href + CODA.searchWidgetHref(widget)
			});
		},

		updateWidget : function(event) {
			var widget = CODA.FOCUSED_WIDGET;
			var params = { fields : {}};

			for (var c in CODA.EDIT_MODE) {
				var is_instance = false;
				try {
					eval('is_instance = widget instanceof ' + c);
				}
				catch (e) {
					is_instance = false;
				}

				if (is_instance) {
					params.form = CODA.EDIT_MODE[c].form;
					
					for (var f in CODA.EDIT_MODE[c].fields) {
						var node = widget[CODA.EDIT_MODE[c].fields[f]];
						if (node !== undefined) {
							if (node.innerHTML == '' && !widget.data[f]) {
								continue;
							}
							params.fields[[CODA.EDIT_MODE[c].fields[f]]] = node.outerHTML;
						}
					}
				}
			}

			var data = Base64.encode(JSON.stringify(params));
			window.parent.KBreadcrumb.dispatchURL({
				hash : '/save' + CODA.searchWidgetHref(widget) + '/data/' + data
			});
		},

		searchWidgetHref : function(widget) {
			if (!widget) {
				return null;
			}

			if (!!widget.config && !!widget.config.href) {
				return widget.config.href.replace(/page/g, 'widgets').replace(/website/g, 'docroots');
			}

			return CODA.searchWidgetHref(widget.parent);
		},

		searchWidget : function(widget) {
			if (!widget) {
				return null;
			}

			if (!!widget.config && !!widget.config.href) {
				return widget;
			}

			return CODA.searchWidget(widget.parent);
		},

		showActions : function(widget) {
			widget.addCSSClass('CODAWidgetSelected');
			CODA.TOOLBAR_BUTTONS.className = ' CODAWidgetSmallEditButtons ';

			KCSS.setStyle({
				display : 'block',
			}, [
			CODA.TOOLBAR_BUTTONS
			]);
		},
		
		hideActions : function(widget) {
			widget.removeCSSClass('CODAWidgetSelected');

			KCSS.setStyle({
				display : 'none',
			}, [
			CODA.TOOLBAR_BUTTONS
			]);
		},

		TOOLBAR_WIDTH : 120,

		EDIT_MODE : {
			'KWidget' :  { form : 'CODAWidgetForm', fields : [ 'title', 'description' ], richtext : { 'decription' : true } },
			'KText' : { form : 'CODATextForm', fields : [ 'lead', 'body' ], richtext : { 'lead' : true, 'body' :true } },
			'KImage' : { form : 'CODAImageForm', fields : [ 'image' ] },
			'KMenu' : { form : 'CODAMenuForm', fields : [ 'menu'] },
			'KHighlight' : { form : 'CODAHighlightForm', fields : [ 'link', 'text', 'image'], richtext : { 'text' :true } },
			'KLink' : { form : 'CODALinkForm', fields : [ 'link', 'text', 'image'] },
			'KList' : { form : 'CODAListForm', fields : [ 'title' ] },
			'KPanel' : { form : 'CODAPanelForm', fields : [ 'title' ] },
			'KShell' : { form : 'CODADocrootForm', fields : [ 'title' ] },
			'KShellPart' : { form : 'CODAPageForm', fields : [ 'title' ] },
			'KFeed' : { form : 'CODAFeedForm', fields : [ 'title' ] },
			'KFeedback' : { form : 'CODAFeedbackForm', fields : [ 'title' ] },
			'KMap' : { form : 'CODAMapForm', fields : [ 'title' ] }
		}
	};
}

var referrer = window.document.referrer;
if (window.top != window.self && referrer.indexOf('//my.redpanda') != -1) {
	load_coda();
}
