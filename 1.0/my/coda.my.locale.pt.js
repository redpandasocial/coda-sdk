
///////////////////////////////////////////////////////////////////
// USER INFO

settext('$CODA_USERS_MUG_HELP', 'Imagem que te irá representar nesta plataforma, sempre que uma imagem representativa do utilizador for necessária.', 'pt');
settext('$CODA_USERS_TITLE_HELP', 'Nome que te irá representar nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'pt');
settext('$CODA_USERS_EMAIL_HELP', 'Endereço de e-mail no qual desejas receber as comunicações da plataforma. Caso o alteres, o processo de validação de endereço de e-mail será desencadeado.', 'pt');
settext('$CODA_USERS_ADDRESS_HELP', 'Morada de faturação que desejas que figure nos recibos e faturas emitidos pela plataforma.', 'pt');
settext('$CODA_USERS_ZIPCODE_HELP', 'Código postal associado à Morada de Faturação.', 'pt');
settext('$CODA_USERS_COUNTRY_HELP', 'País de origem da Morada de Faturação', 'pt');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAMyLocale_pt');

///////////////////////////////////////////////////////////////////
// SHELL
settext('$MY_SHELL_TITLE', 'O meu perfil', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_BUTTON_CONFIGURE_INFO', 'Informação Pessoal', 'pt');
settext('$DASHBOARD_BUTTON_CONFIGURE_INFO_DESC', 'Administra a tua informação pessoal e ligações a redes sociais.', 'pt');
settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD', 'Subscrições', 'pt');
//settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD_DESC', 'Administre a informação sobre os seus cartões de crédito, extraia faturas e veja os pagamentos que tem pendentes.', 'pt');
settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD_DESC', 'Vê as subscrições que já fizeste, confirma datas de validade e extrai recibos e faturas.', 'pt');
settext('$DASHBOARD_BUTTON_MY_APPS_LIST', 'Aplicações', 'pt');
settext('$DASHBOARD_BUTTON_MY_APPS_LIST_DESC', 'Consulta a lista de aplicações em que participas e administra o teu envolvimento em cada uma.', 'pt');
settext('$CODA_MY_REMOVE', '<i class="fa fa-trash-o"></i> REMOVER PERFIL', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// USER INFO
settext('$CODA_USERS_EDIT_PROPERTIES', 'Utilizador ', 'pt');
settext('$CODA_USERS_EDIT_PROPERTIES_ADDRESS', 'MORADA', 'pt');

settext('$CODA_USERS_TITLE', 'Nome completo', 'pt');
settext('$CODA_USERS_PASSWORD', 'Password', 'pt');
settext('$CODA_USERS_CONF_PASSWORD', 'Confirmar password', 'pt');
settext('$CODA_USERS_EMAIL', 'Endereço electrónico ', 'pt');
settext('$CODA_USERS_MUG', 'Avatar ', 'pt');
settext('$CODA_USERS_ADDRESS', 'Morada de faturação ', 'pt');
settext('$CODA_USERS_ZIPCODE', 'Código Postal', 'pt');
settext('$CODA_USERS_COUNTRY', 'País', 'pt');

settext('$CODA_USERS_INFO_GENERAL_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$CODA_USERS_INFO_CHANGEPW_ACTION', '<i class="fa fa-key"></i> ALTERAR PASSWORD', 'pt');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// PAYMENTS LIST
settext('$PAYMENTS_LIST_REFERENCE', 'Referência', 'pt');
settext('$PAYMENTS_LIST_STATUS', 'Estado', 'pt');
settext('$PAYMENTS_LIST_VALUE', 'Valor', 'pt');
settext('$PAYMENTS_LIST_DATE', 'Data', 'pt');
settext('$PAYMENTS_LIST_PAYMENTPROVIDER', 'Meio de Pagamento', 'pt');
settext('$PAYMENTS_LIST_DATE_VALID_UNTIL', 'Válido até', 'pt');
settext('$PAYMENTS_LIST_PAGE_NAME', 'Página associada', 'pt');

settext('$PAYMENT_STATUS_PAYED', 'Pago', 'pt');
settext('$PAYMENT_STATUS_SCHEDULED', 'Agendado', 'pt');
settext('$PAYMENT_STATUS_VOIDED', 'Devolvido', 'pt');
settext('$PAYMENT_STATUS_UNAUTHORIZED', 'N\u00e3o Autorizado', 'pt');

settext('$CODA_MY_PAYMENTS_LIST_TITLE', 'Subscrições', 'pt');
settext('$CODA_MY_PAYMENTS_LIST_GET_INVOICE', 'Extrair recibo', 'pt');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// APPLICATIONS LIST
settext('$CODA_MY_APPLICATIONS_LIST_ICON', '<i class="fa fa-picture-o"></i>', 'pt');
settext('$CODA_MY_APPLICATIONS_LIST_NAME', '<i class="fa fa-cog"></i> Nome', 'pt');
settext('$CODA_MY_APPLICATIONS_LIST_ID', '<i class="fa fa-barcode"></i> Identificador (client_id)', 'pt');
settext('$CODA_MY_APPLICATIONS_LIST_STATE', '<i class="fa fa-check"></i> Estado', 'pt');
settext('$CODA_MY_APPLICATIONS_LIST_TITLE', 'Aplicações', 'pt');
settext('$CODA_MY_APPLICATIONS_LIST_REACTIVATE', 'Reactivar', 'pt');

settext('created', 'Por ativar', 'pt');
settext('canceled', 'Desligada', 'pt');
settext('active', 'Ativa', 'pt');

///////////////////////////////////////////////////////////////////

KSystem.included('CODAMyLocale_pt');

