KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAMyApplicationsListItem(parent) {
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAMyApplicationsListItem.prototype = new CODAListItem();
	CODAMyApplicationsListItem.prototype.constructor = CODAMyApplicationsListItem;

	CODAMyApplicationsListItem.prototype.addMenuButtons = function() {
		if (this.data.state == 'canceled' || this.data.state == 'blocked') {
			var reactivate = new KButton(this, '', 'reactivate', function(event) {
				KDOM.stopEvent(event);
				KBreadcrumb.dispatchURL({
					hash : '/activate-app/' +  KDOM.getEventWidget(event).parent.data.id
				});
			});
			{
				reactivate.setText('<i class="fa fa-check"></i> ' + gettext('$CODA_MY_APPLICATIONS_LIST_REACTIVATE'), true);
			}
			this.appendChild(reactivate);
		}
	};

	CODAMyApplicationsListItem.prototype.onPaymentDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	KSystem.included('CODAMyApplicationsListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAMyApplicationsList (parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, '', { one_choice : true, add_button : false, edit_button : false, delete_button : false });
		this.pageSize = 5;
		this.table = {
			"icon" : {
				label : gettext('$CODA_MY_APPLICATIONS_LIST_ICON'),
				fixed : 20,
				align : 'center'
			},
			"name" : {
				label : gettext('$CODA_MY_APPLICATIONS_LIST_NAME'),
				orderBy : true
			},
			"id" : {
				label : gettext('$CODA_MY_APPLICATIONS_LIST_ID'),
				orderBy : true
			},
			"state" : {
				label : gettext('$CODA_MY_APPLICATIONS_LIST_STATE'),
				orderBy : true
			}
		};
		this.filterFields = [ 'name' ];
		this.orderBy = '-state';
	}
}

KSystem.include([
	'CODAList',
	'CODAMyApplicationsListItem',
	'KLink',
	'KButton'
], function() {
	CODAMyApplicationsList.prototype = new CODAList();
	CODAMyApplicationsList.prototype.constructor = CODAMyApplicationsList;

	CODAMyApplicationsList.prototype.addItems = function(list) {
		for(var index in this.elements) {
			var listItem = new CODAMyApplicationsListItem(list);
			listItem.index = index;
			listItem.data = this.elements[index];
			listItem.data.icon = '<img src="' + listItem.data.icon + '"></img>';
			list.appendChild(listItem);
		}
	};

    KSystem.included('CODAMyApplicationsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAMyDashboard(parent) {
	if (parent != null) {
		var icons = [];

		icons.push({
			icon : 'css:fa fa-user',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_CONFIGURE_INFO'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_CONFIGURE_INFO_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/edit-info'
			}]
		});

		icons.push({
			icon : 'css:fa fa-credit-card',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/edit-payments'
			}]
		});

		icons.push({
			icon : 'css:fa fa-puzzle-piece',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_MY_APPS_LIST'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_MY_APPS_LIST_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-apps'
			}]
		});

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODAMyDashboard.prototype = new CODAMenuDashboard();
	CODAMyDashboard.prototype.constructor = CODAMyDashboard;

	CODAMyDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAMyDashboard');
}, Kinky.CODA_INCLUDER_URL);

function CODAMyInfoForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.retain(arguments);

		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.target.collection = '/users';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KInput',
	'KPassword'
], function() {
	CODAMyInfoForm.prototype = new CODAForm();
	CODAMyInfoForm.prototype.constructor = CODAMyInfoForm;
 
	CODAMyInfoForm.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target.id = this.target.href = data._id;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAMyInfoForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

		var advanced = new KButton(this.getShell(), gettext('$CODA_USERS_INFO_CHANGEPW_ACTION'), 'changepw', function(e) {
			KDOM.stopEvent(e);
			KBreadcrumb.dispatchURL({ hash : '/change-password'});
		});
		this.addAction(advanced);
	};

	CODAMyInfoForm.prototype.addMinimizedPanel = function() {
		var data = this.data;

		if (!!data.id) {
			this.setTitle(gettext('$CODA_USERS_EDIT_PROPERTIES') + ' \u00ab' + data.name + '\u00bb');
		}

		var infoPanel = new KPanel(this);
		{
			infoPanel.hash = '/general';

			var mug = new KImageUpload(infoPanel, gettext('$CODA_USERS_MUG') + '*', 'mug', CODAGenericShell.getUploadAuthorization());
			{
				if (!!data && !!data.mug && data.mug.indexOf('css:') != 0) {
					mug.setValue(data.mug);
				}
				var mediaList = new CODAMediaList(mug, { 
					mimetype : 'image/*', 
					size : { 
						min : { 
							width  : 256, 
							height : 256
						}
					}
				});
				mug.appendChild(mediaList);
				CODAImageCrop.addCrop(mug, {
					force_resize : true,
					keep_aspect_ratio : true,
					only_metadata : false,
					width : 256,
					height : 256,
					media_server : 'media_url_1',
					target_uri : Kinky.getLoggedUser()._id + '/mug.png',
					out_mime : 'image/png'
				});
				mug.setHelp(gettext('$CODA_USERS_MUG_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(mug);

			var name = new KInput(infoPanel, gettext('$CODA_USERS_TITLE') + ' *', 'name');
			{
				if (!!data && !!data.name) {
					name.setValue(data.name);
				}
				name.setHelp(gettext('$CODA_USERS_TITLE_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			infoPanel.appendChild(name);

			var email = new KInput(infoPanel, gettext('$CODA_USERS_EMAIL'), 'email');
			{
				if (!!data && !!data.email) {
					email.setValue(data.email);
				}
				email.setHelp(gettext('$CODA_USERS_EMAIL_HELP'), CODAForm.getHelpOptions());
				email.addValidator(/(^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9_\.\-]+$)|(^$)/, gettext('$ERROR_MUST_BE_EMAIL'));
			}
			infoPanel.appendChild(email);

			this.addSeparator(gettext('$CODA_USERS_EDIT_PROPERTIES_ADDRESS'), infoPanel);

			var address = new KTextArea(infoPanel, gettext('$CODA_USERS_ADDRESS'), 'address');
			{
				if (!!data && !!data.address) {
					address.setValue(data.address);
				}
				address.setHelp(gettext('$CODA_USERS_ADDRESS_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(address);

			var zipcode = new KInput(infoPanel, gettext('$CODA_USERS_ZIPCODE'), 'zipcode');
			{
				if (!!data && !!data.zipcode) {
					zipcode.setValue(data.zipcode);
				}
				zipcode.setHelp(gettext('$CODA_USERS_ZIPCODE_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(zipcode);

			var country = new KAutoCompletion(infoPanel, gettext('$CODA_USERS_COUNTRY'), 'country');
			{
				for (var c in CODA_COUNTRIES) {
					country.addOption(getlocale(CODA_COUNTRIES[c].name), getlocale(CODA_COUNTRIES[c].name), false);
				}

				country.setHelp(gettext('$CODA_USERS_COUNTRY_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(country);

		}	
		this.addPanel(infoPanel, gettext('$CODA_USERS_INFO_GENERAL_PANEL'), true);

		this.addHeaderSeparator(gettext('$CODA_USERS_INFO_SOCIALNETWORK_SEPARATOR'));

		var social = {
			"instagram" : {
				type : 'oauth2',
				url : '/accounts/instagram/connect',
				width : 820,
				height : 520,
				mug : 'account.profile_picture',
				icon : 'fa fa-instagram'
			},
			"facebook" : {
				type : 'oauth2',
				url : '/accounts/facebook/connect',
				width : 820,
				height : 450,
				mug : '\'https://graph.facebook.com/\' + account.id + \'/picture?type=large\'',
				icon : 'fa fa-facebook-square'
			},
			"flickr" : {
				type : 'oauth1',
				url : '/accounts/flickr/connect',
				width : 980,
				height : 700,
				mug : '"http://farm" + account.iconfarm + ".staticflickr.com/" + account.iconserver + "/buddyicons/" + account.id + ".jpg"',
				icon : 'fa fa-flickr'
			},
			"google" : {
				type : 'oauth2',
				url : '/accounts/google/connect',
				width : 820,
				height : 570,
				mug : 'account.picture',
				icon : 'fa fa-google-plus-square'
			},
			"linkedin" : {
				type : 'oauth2',
				url : '/accounts/linkedin/connect',
				width : 820,
				height : 570,
				mug : 'account.picture',
				icon : 'fa fa-linkedin-square'
			},
			"tumblr" : {
				type : 'oauth1',
				url : '/accounts/tumblr/connect',
				width : 820,
				height : 640,
				mug : 'null',
				icon : 'fa fa-tumblr-square'
			},
			"twitter" : {
				type : 'oauth1',
				url : '/accounts/twitter/connect',
				width : 820,
				height : 640,
				mug : 'account.profile_image_url_https',
				icon : 'fa fa-twitter'
			},
			"yahoo" : {
				type : 'oauth1',
				url : '/accounts/yahoo/connect',
				width : 820,
				height : 370,
				mug : 'account.image.imageUrl',
				icon : 'rp rp-yahoo'
			}
		};

		for (var s in social) {
			var prefix = s.toUpperCase();
			var panel = this.createPanel(s, social[s], data);
			this.addPanel(panel, gettext('$CODA_USERS_INFO_' + prefix + '_LINK'), false);
		}

		this.addHeaderSeparator(gettext('$CODA_USERS_INFO_CLOUDSTORAGE_SEPARATOR'));

		var cloud = {
			"box" : {
				type : 'oauth2',
				url : '/accounts/box/connect',
				width : 820,
				height : 640,
				mug : 'account.avatar_url',
				icon : 'rp rp-box-rect'
			},
			"copy" : {
				type : 'oauth1',
				url : '/accounts/copy/connect',
				width : 820,
				height : 700,
				mug : 'account.avatar_url',
				icon : 'fa fa-cloud'
			},
			"dropbox" : {
				type : 'oauth2',
				url : '/accounts/dropbox/connect',
				width : 1000,
				height : 660,
				mug : 'null',
				icon : 'fa fa-dropbox'
			}/*,
			"bitbucket" : {
				type : 'oauth1',
				url : '/accounts/bitbucket/connect',
				width : 820,
				height : 370,
				mug : 'account.avatar',
				icon : 'fa fa-bitbucket'
			}*/
		};

		for (var s in cloud) {
			var prefix = s.toUpperCase();
			var panel = this.createPanel(s, cloud[s], data);
			this.addPanel(panel, gettext('$CODA_USERS_INFO_' + prefix + '_LINK'), false);
		}
	};

	CODAMyInfoForm.addWizardPanel = function(parent)  {
		var infoPanel = new KPanel(parent);
		{
			infoPanel.hash = '/general';


			var password = new KPassword(infoPanel, gettext('$CODA_USERS_PASSWORD'), 'password');
			{
				password.setHelp(gettext('$CODA_USERS_PASSWORD_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(password);

			var confirmpass = new KPassword(infoPanel, gettext('$CODA_USERS_CONF_PASSWORD'), 'confpassword');
			{
				confirmpass.setHelp(gettext('$CODA_USERS_CONF_PASSWORD_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(confirmpass);
		}

		return [ infoPanel ];
	};

	CODAMyInfoForm.prototype.setDefaults = function(params){
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;

		delete params.accounts;
	};

	CODAMyInfoForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAMyInfoForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAMyInfoForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_USERS_NOT_FOUND'));
	};

	CODAMyInfoForm.prototype.onPost = CODAMyInfoForm.prototype.onCreated = function(data, request){
		this.getShell().dashboard.refresh();
		this.parent.closeMe();
	};

	CODAMyInfoForm.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAMyInfoForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAMyInfoForm.prototype.createPanel = function(s, social, data) {
		var prefix = s.toUpperCase();
		var panel = new KPanel(this);
		{
			panel.hash = '/' + s;
			var account = CODAMyInfoForm.getAccount(data, s);
			var handler = null;
			if (social.type == 'oauth2') {
				handler = CODAMyInfoForm.oauth2Event(social.url, social.width, social.height);
			}
			else {
				handler = CODAMyInfoForm.oauth1Event(social.url, social.width, social.height);
			}

			if (!account) {
				var calltoaction = new KText(panel);
				{
					calltoaction.setLead(gettext('$CODA_USERS_NO_' + prefix));
					calltoaction.setText(gettext('$CODA_USERS_INFO_NOT_CONNECTED_DESC'));
				}
				panel.appendChild(calltoaction);

				var action = new KButton(panel, '', 'link-' + s, handler);
				{
					action.setText(gettext('$CODA_USERS_' + prefix + '_LINK_BUTTON'), true);
					action.setStyle({
						textAlign : 'right',
						width: '500px'
					});
				}
				panel.appendChild(action);
			}
			else {
				var calltoaction = new KText(panel);
				{
					calltoaction.addCSSClass('MyConnected');
					calltoaction.setLead(gettext('$CODA_USERS_CONNECTED_TO_' + prefix));
					calltoaction.setText(gettext('$CODA_USERS_INFO_CONNECTED_DESC'));
				}
				panel.appendChild(calltoaction);

				var img = null;
				eval('img = ' + social.mug + ';');
				if (!img) {
					if (!!data && !!data.mug) {
						img = data.mug;
					}
					else {
						img = 'http://assets.tumblr.com/images/default_avatar_128.gif';
					}
				}

				var mug = window.document.createElement('img');
				mug.src = img;
				calltoaction.content.appendChild(mug);
				var ar = window.document.createElement('i');
				ar.className = 'fa fa-arrow-right';
				calltoaction.content.appendChild(ar);
				var fbi = window.document.createElement('i');
				fbi.className = social.icon;
				calltoaction.content.appendChild(fbi);

				var action = new KButton(panel, '', 'link-' + s, handler);
				{
					action.setText(gettext('$CODA_USERS_INFO_REFRESH_BUTTON'), true);
					action.setStyle({
						position : 'absolute',
						textAlign : 'right',
						top: '15px',
						right: 'auto',
						left: '25px',
						bottom: 'auto'
					});
				}
				panel.appendChild(action);
			}
		}
		return panel;
	}

	CODAMyInfoForm.getAccount = function(data, type) {
		if (!!data.accounts && !!data.accounts.elements) {
			for (var a in data.accounts.elements) {
				if (data.accounts.elements[a].type.toLowerCase() == type.toLowerCase()) {
					if (!!data.accounts.elements[a].profile) {
						return data.accounts.elements[a].profile;
					}
					else {
						return data.accounts.elements[a];
					}
				}
			}
		}
		return false;
	};

	CODAMyInfoForm.oauth1Event = function(url, width, height) {
		return function(e) {
			window.CODA_WAITING_AUTH = KDOM.getEventWidget(e, 'CODAMyInfoForm');

			var docroot = Kinky.getDefaultShell().data.docroot;
			if (docroot.indexOf('http') == -1) {
				docroot = window.document.location.protocol + docroot;
			}
			var restroot = Kinky.getDefaultShell().data.providers.rest.services;
			if (restroot.indexOf('http') == -1) {
				restroot = window.document.location.protocol + restroot;
			}

			var reredir = restroot + url + '?redirect_uri=' + encodeURIComponent(docroot + CODA_VERSION + '/social/connected.html');
			var pup = window.open(restroot + url + '?display=popup&redirect_uri=' + encodeURIComponent(reredir), "", "width=" + width + ",height=" + height + ",status=1,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());
		};
	};

	CODAMyInfoForm.oauth2Event = function(url, width, height) {
		return function(e) {
			window.CODA_WAITING_AUTH = KDOM.getEventWidget(e, 'CODAMyInfoForm');

			var docroot = Kinky.getDefaultShell().data.docroot;
			if (docroot.indexOf('http') == -1) {
				docroot = window.document.location.protocol + docroot;
			}
			var restroot = Kinky.getDefaultShell().data.providers.rest.services;
			if (restroot.indexOf('http') == -1) {
				restroot = window.document.location.protocol + restroot;
			}
			
			var pup = window.open(restroot + url + '?display=popup&redirect_uri=' + encodeURIComponent(docroot + CODA_VERSION + '/social/connected.html'), "", "width=" + width + ",height=" + height + ",status=1,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());
		};
	};

	KSystem.included('CODAMyInfoForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMyPaymentsListItem(parent) {
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAMyPaymentsListItem.prototype = new CODAListItem();
	CODAMyPaymentsListItem.prototype.constructor = CODAMyPaymentsListItem;

	CODAMyPaymentsListItem.prototype.addMenuButtons = function() {
		if (this.data.status == 'payed') {
			var getInvoice = new KButton(this, '', 'getinvoice', function(event) {
				KDOM.stopEvent(event);
			});
			{
				getInvoice.setText('<i class="fa fa-files-o"></i> ' + gettext('$CODA_MY_PAYMENTS_LIST_GET_INVOICE'), true);
			}
			this.appendChild(getInvoice);
		}
	};

	KSystem.included('CODAMyPaymentsListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAMyPaymentsList (parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, '', { one_choice : true, add_button : false, edit_button : false, delete_button : false });
		this.pageSize = 5;
		this.table = {
			"reference" : {
				label : gettext('$PAYMENTS_LIST_REFERENCE'),
				orderBy : true
			},
			"total_value" : {
				label : gettext('$PAYMENTS_LIST_VALUE'),
				fixed : 130,
				align : 'center'
			},
			"date" : {
				label : gettext('$PAYMENTS_LIST_DATE'),
				fixed : 160,
				orderBy : true
			},
			"valid_until" : {
				label : gettext('$PAYMENTS_LIST_DATE_VALID_UNTIL'),
				fixed : 160,
				orderBy : true
			},
			"owner_page.name" : {
				label : gettext('$PAYMENTS_LIST_PAGE_NAME')
			}
		};
		this.filterFields = [ 'reference' ];
		this.orderBy = '-valid_until';
	}
}

KSystem.include([
	'CODAList',
	'CODAMyPaymentsListItem',
	'KLink',
	'KButton'
], function() {
	CODAMyPaymentsList.prototype = new CODAList();
	CODAMyPaymentsList.prototype.constructor = CODAMyPaymentsList;

	CODAMyPaymentsList.prototype.addItems = function(list) {
		for(var index in this.elements) {
			var listItem = new CODAMyPaymentsListItem(list);
			listItem.data = this.elements[index];
			if (!listItem.data.owner_page) {
				listItem.data.owner_page = {
					name : listItem.data.fb_page_id
				};
			}

			if(!!this.elements[index]["paymentparcels"]){
				var total_value = 0;
				for (var index2 in this.elements[index]["paymentparcels"]["elements"]){
					total_value = total_value + (this.elements[index]["paymentparcels"]["elements"][index2]["total_value"]);
				}
				listItem.data.total_value = KSystem.truncateDouble(total_value, 2);
			}

			if(!!this.elements[index].status){
				switch (this.elements[index].status){
					case 'payed' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_PAYED');
						break;
					}
					case 'scheduled' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_SCHEDULED');
						break;
					}
					case 'voided' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_VOIDED');
						break;
					}
					case 'unauthorized' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_UNAUTHORIZED');
						break;
					}
				}
			}

			list.appendChild(listItem);
		}
	};

    KSystem.included('CODAMyPaymentsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAMyShell(parent) {
	if (parent != null) {
		if(typeof(Storage) !== "undefined") {
			localStorage.dashboard = '[]';
		}

		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAMyShell.actions);
	}
}

KSystem.include([
	'KButton',
	'CODAGenericShell',
	'KPanel',
	'KImage',
	'KText',
	'CODAMyPaymentsList',
	'CODAMyDashboard',
	'CODAMyInfoForm',
	'HTMLEntities'
], function() {
	CODAMyShell.prototype = new CODAGenericShell();
	CODAMyShell.prototype.constructor = CODAMyShell;
	
	/*CODAMyShell.prototype.loadShell = function() {
		
	};*/

	CODAMyShell.prototype.onNoContent = function(data, request) {
		this.draw();
	};
	
	CODAMyShell.prototype.draw = function() {
		var modulesContainer = new CODAMyDashboard(this);
		this.appendChild(modulesContainer);

		var addbt = new KButton(this, gettext('$CODA_MY_REMOVE'), 'add', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/remove'
			});
		});
		this.addBottomButton(addbt, 3);

		this.setTitle(gettext('$MY_SHELL_TITLE'));

		CODAGenericShell.prototype.draw.call(this);
	};

	CODAMyShell.prototype.changePassword = function() {
		var screens = [ 'CODAMyInfoForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$CODA_USERS_INFO_CHANGE_PASSWORD'));
		wiz.hash = '/my/info/change-password';

		wiz.mergeValues = function() {
			var params = KSystem.clone(Kinky.getLoggedUser());
			delete params.accounts;
			delete params.links;
			delete params.scopes;

			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			this.enable();
			this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.put(this, 'rest:/users/me', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);
	};

	
	CODAMyShell.prototype.editUser = function() {
		var user_form = new CODAMyInfoForm(this, 'oauth:/users/me?embed=accounts,scopes', null, null);
		user_form.hash = '/my/info/edit';
		this.makeDialog(user_form, { minimized : false, modal : true });
	};

	CODAMyShell.prototype.editPayments = function() {
		var users_list = new CODAMyPaymentsList(this, {
			links : {
				feed : { href :  '/payments/owners/me?status=payed&embed=elements(*(paymentparcels,paymentmethod))' }
			}
		});
		users_list.hash = '/my/info/payments/list';
		users_list.setTitle(gettext('$CODA_MY_PAYMENTS_LIST_TITLE'));
		this.makeDialog(users_list, { minimized : false, modal : true });		
	};

	CODAMyShell.prototype.listApps = function() {
		var appslist = new CODAMyApplicationsList(this, {
			links : {
				feed : { href :  '/applications/owners/me?state=m/active|no_user_content|created|canceled/i' }
			}
		});
		appslist.hash = '/my/info/applications/list';
		appslist.setTitle(gettext('$CODA_MY_APPLICATIONS_LIST_TITLE'));
		this.makeDialog(appslist, { minimized : false, modal : true });		
	};

	CODAMyShell.prototype.activateApp = function(app_id) {
		this.kinky.post(this, 'rest:/applications/' + app_id + '/activate', { }, Kinky.SHELL_CONFIG[this.shell].headers, 'onAppActivated');
	};

	CODAMyShell.prototype.subscribePayment = function() {
		this.kinky.get(this, 'rest:/products?orderBy=base_reference', { }, null, 'onLoadProducts');
	};

	CODAMyShell.prototype.onAppActivated = function(data, request) {
		if (!!CODA_FOCUSED_WINDOW) {
			CODA_FOCUSED_WINDOW.target.list.refreshPages();
		}
	};

	CODAMyShell.prototype.onLoadProducts = function(data, request) {
		this.makeWizard(new CODAApplicationSubsriptionWizard(this, data));
	};

	CODAMyShell.prototype.onPreConditionFailed = function(data, request) {
		if (request.service.indexOf('/activate') != -1) {
			this.subscriptionData = data.data;
			this.data.app_id = request.service.replace('/applications/', '').replace('/activate', '');

			KBreadcrumb.dispatchURL({
				hash : '/subscribe-payment/' + this.data.app_id
			});
			return;
		}
		this.draw();
	};

	CODAMyShell.prototype.onRemove = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location.reload(true);
	};

	CODAMyShell.prototype.onPost = function(data, request) {
	};

	CODAMyShell.prototype.onPut = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location.reload(true);
	};

	CODAMyShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'edit-info': {
				Kinky.getDefaultShell().editUser();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-payments': {
				Kinky.getDefaultShell().editPayments();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'list-apps': {
				Kinky.getDefaultShell().listApps();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'change-password': {
				Kinky.getDefaultShell().changePassword();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'activate-app': {
				Kinky.getDefaultShell().activateApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'subscribe-payment': {
				Kinky.getDefaultShell().subscribePayment(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	CODAMyShell.CHANNELS = [ "facebook", "backoffice", "iframe", "website", "module" ];

	KSystem.included('CODAMyShell');

}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

