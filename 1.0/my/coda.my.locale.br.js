///////////////////////////////////////////////////////////////////
// SHELL
settext('$MY_SHELL_TITLE', 'O meu perfil', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_BUTTON_CONFIGURE_INFO', 'Informação Pessoal', 'br');
settext('$DASHBOARD_BUTTON_CONFIGURE_INFO_DESC', 'Administra a sua informação pessoal e ligações a redes sociais.', 'br');
settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD', 'Assinaturas', 'br');
//settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD_DESC', 'Administre a informação sobre os seus cartões de crédito, extraia faturas e veja os pagamentos que tem pendentes.', 'br');
settext('$DASHBOARD_BUTTON_CONFIGURE_CREDIT_CARD_DESC', 'Vê as Assinaturas que já fizeste, confirma datas de validade e extrai recibos e faturas.', 'br');
settext('$DASHBOARD_BUTTON_MY_APPS_LIST', 'Aplicativos', 'br');
settext('$DASHBOARD_BUTTON_MY_APPS_LIST_DESC', 'Consulta a lista de aplicativos em que participa e administra o seu envolvimento em cada uma.', 'br');
settext('$CODA_MY_REMOVE', '<i class="fa fa-trash-o"></i> REMOVER PERFIL', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// USER INFO
settext('$CODA_USERS_EDIT_PROPERTIES', 'Usuário ', 'br');
settext('$CODA_USERS_EDIT_PROPERTIES_ADDRESS', 'ENDEREÇO', 'br');

settext('$CODA_USERS_TITLE', 'Nome completo', 'br');
settext('$CODA_USERS_PASSWORD', 'Senha', 'br');
settext('$CODA_USERS_CONF_PASSWORD', 'Confirmar senha', 'br');
settext('$CODA_USERS_EMAIL', 'Endereço eletrônico ', 'br');
settext('$CODA_USERS_MUG', 'Avatar ', 'br');
settext('$CODA_USERS_ADDRESS', 'Endereço de faturação ', 'br');
settext('$CODA_USERS_ZIPCODE', 'Código Postal', 'br');

settext('$CODA_USERS_INFO_GENERAL_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$CODA_USERS_INFO_CHANGEPW_ACTION', '<i class="fa fa-key"></i> ALTERAR SENHA', 'br');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// PAYMENTS LIST
settext('$PAYMENTS_LIST_REFERENCE', 'Referência', 'br');
settext('$PAYMENTS_LIST_STATUS', 'Estado', 'br');
settext('$PAYMENTS_LIST_VALUE', 'Valor', 'br');
settext('$PAYMENTS_LIST_DATE', 'Data', 'br');
settext('$PAYMENTS_LIST_PAYMENTPROVIDER', 'Meio de Pagamento', 'br');
settext('$PAYMENTS_LIST_DATE_VALID_UNTIL', 'Válido até', 'br');
settext('$PAYMENTS_LIST_PAGE_NAME', 'Página associada', 'br');

settext('$PAYMENT_STATUS_PAYED', 'Pago', 'br');
settext('$PAYMENT_STATUS_SCHEDULED', 'Agendado', 'br');
settext('$PAYMENT_STATUS_VOIDED', 'Devolvido', 'br');
settext('$PAYMENT_STATUS_UNAUTHORIZED', 'N\u00e3o Autorizado', 'br');

settext('$CODA_MY_PAYMENTS_LIST_TITLE', 'Assinaturas', 'br');
settext('$CODA_MY_PAYMENTS_LIST_GET_INVOICE', 'Extrair recibo', 'br');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// APPLICATIONS LIST
settext('$CODA_MY_APPLICATIONS_LIST_ICON', '<i class="fa fa-picture-o"></i>', 'br');
settext('$CODA_MY_APPLICATIONS_LIST_NAME', '<i class="fa fa-cog"></i> Nome', 'br');
settext('$CODA_MY_APPLICATIONS_LIST_ID', '<i class="fa fa-barcode"></i> Identificador (client_id)', 'br');
settext('$CODA_MY_APPLICATIONS_LIST_STATE', '<i class="fa fa-check"></i> Estado', 'br');
settext('$CODA_MY_APPLICATIONS_LIST_TITLE', 'Aplicativos', 'br');
settext('$CODA_MY_APPLICATIONS_LIST_REACTIVATE', 'Reativar', 'br');

settext('created', 'Por ativar', 'br');
settext('canceled', 'Desligada', 'br');
settext('active', 'Ativa', 'br');

///////////////////////////////////////////////////////////////////

KSystem.included('CODAMyLocale_pt');


///////////////////////////////////////////////////////////////////
// USER INFO

settext('$CODA_USERS_MUG_HELP', 'Imagem que te representará nesta plataforma, sempre que uma imagem representativa do usuário for necessária.', 'br');
settext('$CODA_USERS_TITLE_HELP', 'Nome que te representará nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'br');
settext('$CODA_USERS_EMAIL_HELP', 'Endereço de e-mail no qual desejas receber as comunicações da plataforma. Caso o alteres, o processo de validação de endereço de e-mail será desencadeado.', 'br');
settext('$CODA_USERS_ADDRESS_HELP', 'Endereço de faturamento que deseja que figure nos recibos e faturas emitidos pela plataforma.', 'br');
settext('$CODA_USERS_ZIPCODE_HELP', 'Código postal associado à Endereço de faturamento.', 'br');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAMyLocale_pt');

