function KSystem() {
}
{
	KSystem.prototype = {};
	KSystem.prototype.constructor = KSystem;
	
	KSystem.construct = function(jsonObject, parent, throwException) {
		var jsClass = jsonObject.jsClass;
		
		if (!jsClass) {
			jsClass = KSystem.getWRMLClass(jsonObject);
		}
		if (!!jsClass) {
			var object = null;
			var first = true;
			var newString = 'object = new ' + jsClass + "(";
			if (!!parent) {
				newString += 'parent';
				first = false;
			}
			
			for ( var arg in jsonObject.args) {
				if (!first) {
					newString += ', ';
				}
				first = false;
				newString += JSON.stringify(jsonObject.args[arg]);
			}
			newString += ');';
			eval(newString);
			
			object.config = jsonObject;
			if (object.data == null) {
				if (!!object.config.embedded) {
					object.data = object.config;
				}
				else {
					object.data = new Object();
				}
			}
			
			if ((object.config.hash != null) && (object.hash == null)) {
				object.hash = object.config.hash;
			}
			return object;
		}
		else {
			throw new Error('KSystem: can\'t create KWidget from empty source (parentID=\'' + parent.id + '\', parentClass=\'' + KSystem.getObjectClass(parent) + '\', parentHash=\'' + parent.hash + '\').');
		}
	};
	
	KSystem.getWRMLClass = function(wrmlConf, mimePrefix) {
		mimePrefix = (!!mimePrefix ? mimePrefix : 'application/vnd.kinky.');
		var jsClass = null;
		var types = null;
		
		if (wrmlConf.requestTypes) {
			types = wrmlConf.requestTypes;
		}
		else if (wrmlConf.responseTypes) {
			types = wrmlConf.responseTypes;
		}
		
		if (!!types) {
			for ( var type in types) {
				var regex = new RegExp(mimePrefix.replace(/\//, '\\/'));
				if (regex.test(types[type])) {
					var part = types[type].split(regex);
					part = (part.length > 1 ? part[1] : part[0]);
					part = part.split(/\+/);
					jsClass = part[0];
					if (part.length > 1) {
						wrmlConf.args = (!!wrmlConf.args ? wrmlConf.args : []);
						for ( var i = 1; i != part.length; i++) {
							wrmlConf.args.push(part[i]);
						}
					}
					
				}
			}
		}
		return jsClass;
	};
	
	KSystem.getWRMLSchema = function(wrmlConf, mimePrefix) {
		mimePrefix = (!!mimePrefix ? mimePrefix : 'application/wrml;schema="');
		var schemaClass = null;
		var types = null;
		
		if (wrmlConf.requestTypes) {
			types = wrmlConf.requestTypes;
		}
		else if (wrmlConf.responseTypes) {
			types = wrmlConf.responseTypes;
		}
		
		if (!!types) {
			for ( var type in types) {
				var regex = new RegExp(mimePrefix);
				if (regex.test(types[type])) {
					var part = types[type].split(regex)[1];
					part = part.split(/"/);
					schemaClass = part[0];
				}
			}
		}
		return schemaClass;
	};
	
	KSystem.getWRMLForm = function(wrmlSchema, prefix) {
		var jsClass = null;
		var types = wrmlSchema.stateFacts;
		
		if (!!types) {
			for ( var type in types) {
				if (types[type].indexOf("Form(") != -1) {
					jsClass = types[type].split("Form(")[1].split(")")[0];
					jsClass = jsClass.replace(prefix, '');
				}
			}
		}
		return jsClass;
	};
	
	KSystem.include = function(jsClass, callback, includer) {
		if (!(jsClass instanceof Array)) {
			jsClass = [
				jsClass
			];
		}
		for ( var index in jsClass) {
			if (!KSystem.includes[jsClass[index]]) {
				var include = window.document.createElement('script');
				KSystem.includes[jsClass[index]] = {
					include : include
				};
				var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
				include.charset = 'utf-8';
				include.src = url;
				window.document.getElementsByTagName('head')[0].appendChild(include);
			}
		}
		if (callback) {
			KSystem.addTimer(function() {
				KSystem.includeWait(callback, jsClass);
			}, 20);
		}
	};
	
	KSystem.isIncluded = function(jsClass) {
		return !!KSystem.includes[jsClass] && !!KSystem.includes[jsClass].finished;
	};
	
	KSystem.includeWait = function(callback, jsClass) {
		for ( var index in jsClass) {
			if (!KSystem.includes[jsClass[index]].finished) {
				KSystem.addTimer(function() {
					KSystem.includeWait(callback, jsClass);
				}, 20);
				return;
			}
		}
		callback();
		delete jsClass;
	};

	KSystem.hasPendingImports = function() {
		for ( var index in KSystem.includes) {
			if (!KSystem.includes[index].finished) {
				return true;
			}
		}
		return false;
	};
	
	KSystem.included = function(id) {
		if (!KSystem.includes[id]) {
			KSystem.includes[id] = {
				finished : true
			};
		}

		if (!KSystem.includes[id].finished) {
			if (!!KSystem.includes[id].include) {
				window.document.getElementsByTagName('head')[0].removeChild(KSystem.includes[id].include);
				delete KSystem.includes[id].include;
			}
			KSystem.includes[id].finished = true;
		}
		
		if (KSystem.isIncluded('KWidget')) {
			var isFromKinky = false;
			try {
				eval('isFromKinky = typeof ' + id + ' !== \'undefined\'');
			}
			catch (e) {
				isFromKinky = false;
			}
			if (isFromKinky) {
				for ( var func in KWidget.STATE_FUNCTIONS) {
					var exists = false;
					eval('exists = !!' + id + '.prototype && !!' + id + '.prototype.' + KWidget.STATE_FUNCTIONS[func] + ';');
					if (exists) {
						eval(id + '.prototype.default_' + KWidget.STATE_FUNCTIONS[func] + ' = ' + id + '.prototype.' + KWidget.STATE_FUNCTIONS[func] + ';');
					}
				}
			}
		}
		
		if (KSystem.isIncluded('KAddOn') && KSystem.isAddOn(id)) {
			var isFromKinky = false;
			try {
				eval('isFromKinky = typeof ' + id + ' !== \'undefined\'');
			}
			catch (e) {
				isFromKinky = false;
			}
			if (isFromKinky) {
				for ( var func in KAddOn.STATE_FUNCTIONS) {
					var exists = false;
					eval('exists = !!' + id + '.prototype.' + KAddOn.STATE_FUNCTIONS[func] + ';');
					if (exists) {
						eval(id + '.prototype.default_' + KAddOn.STATE_FUNCTIONS[func] + ' = ' + id + '.prototype.' + KAddOn.STATE_FUNCTIONS[func] + ';');
					}
				}
			}
		}
		
	};
	
	KSystem.urlify = function(url, params) {
		var uri = '';
		var first = true;
		for ( var param in params) {
			if (url.indexOf(param + '=') != -1) {
				continue;
			}
			if (!first) {
				uri += '&';
			}
			first = false;
			uri += param + '=' + encodeURIComponent(params[param]);
		}
		if (uri.length != 0) {
			return url + (url.indexOf('?') != -1 ? '&' : '?') + uri;
		}
		return url;
	};
	
	KSystem.clone = function(object, exceptions, fromRecurrence) {
		if (object == null) {
			return fromRecurrence ? null : {};
		}
		
		var toReturn = null;
		if (object instanceof Array) {
			toReturn = new Array();
			for ( var i in object) {
				if (typeof object[i] == 'object') {
					toReturn[i] = KSystem.clone(object[i], exceptions, true);
				}
				else {
					toReturn[i] = object[i];
				}
			}
		}
		else if (typeof object == 'object') {
			toReturn = new Object();
			for ( var i in object) {
				if (exceptions && exceptions.test(i)) {
					continue;
				}
				if (typeof object[i] == 'object') {
					toReturn[i] = KSystem.clone(object[i], exceptions, true);
				}
				else {
					toReturn[i] = object[i];
				}
			}
		}
		else {
			return object;
		}
		return toReturn;
		
	};
	
	KSystem.merge = function(object1, object2) {
		if (object1 instanceof Array && object2 instanceof Array) {
			for ( var i in object2) {
				object1.push(object2[i]);
			}
			return object1;
		}
		for ( var i in object2) {
			object1[i] = (object2[i] != null ? object2[i] : object1[i]);
		}
		return object1;
	};
	
	KSystem.setCookie = function(name, value, expires) {
		KSystem.removeCookie(name);
		var exdate = new Date();
		if (expires) {
			exdate.setTime(expires);
		}
		else {
			exdate.setFullYear(exdate.getFullYear() + 10);
		}
		window.document.cookie = name + "=" + encodeURIComponent(JSON.stringify(value)) + "; expires=" + exdate.toUTCString() + '; path=/';
	};
	
	KSystem.getCookie = function(name) {
		if (document.cookie.length > 0) {
			var start = document.cookie.indexOf(name + "=");
			if (start != -1) {
				start = start + name.length + 1;
				var end = document.cookie.indexOf(";", start);
				if (end == -1) {
					end = document.cookie.length;
				}
				var toReturn = null;
				eval('toReturn = ' + unescape(document.cookie.substring(start, end)) + ';');
				return toReturn || {};
			}
		}
		return {};
	};
	
	KSystem.removeCookie = function(name) {
		var exdate = new Date();
		exdate.setTime(exdate.getTime() - 3600000);
		window.document.cookie = name + "=delete; expires=" + exdate.toUTCString() + '; path=/';
	};
	
	KSystem.addTimer = function(code, miliseconds, nonStop) {
		if (nonStop) {
			return setInterval(code, miliseconds);
		}
		else {
			return setTimeout(code, miliseconds);
		}
	};
	
	KSystem.removeTimer = function(timer) {
		clearInterval(timer);
		clearTimeout(timer);
	};
	
	KSystem.getObjectClass = function(obj) {
		if (obj && obj.constructor && obj.constructor.toString) {
			var arr = obj.constructor.toString().match(/function\s*(\w+)/);
			if (arr && (arr.length == 2)) {
				return arr[1];
			}
		}
		return undefined;
	};
	
	KSystem.formatDouble = function(v, p) {
		var precision = (!!p ? p : 0), neg = v < 0, power = Math.pow(10, precision), value = Math.round(v * power), integral = String((neg ? Math.ceil : Math.floor)(value / power)), fraction = String((neg ? -value : value) % power), padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
		
		return precision ? integral + '.' + padding + fraction : integral;
	};

	KSystem.truncateDouble = function(v, p) {
		var s = "" + v;
		var p = s.split(".");
		if (p.length == 1) {
			return p[0] + '.00';
		}
		else {
			return p[0] + '.' + p[1][0] + (p[1].length > 1 ? p[1][1] : '0');
		}
	};
	
	KSystem.months = [
		'Janeiro',
		'Fevereiro',
		'Mar\u00e7o',
		'Abril',
		'Maio',
		'Junho',
		'Julho',
		'Agosto',
		'Setembro',
		'Outubro',
		'Novembro',
		'Dezembro'
	];
	KSystem.monthsAbrev = [
		'Jan',
		'Fev',
		'Mar',
		'Abr',
		'Mai',
		'Jun',
		'Jul',
		'Ago',
		'Set',
		'Out',
		'Nov',
		'Dez'
	];
	
	KSystem.monthsIndex = {
		'Janeiro' : 0,
		'Fevereiro' : 1,
		'Mar\u00e7o' : 2,
		'Abril' : 3,
		'Maio' : 4,
		'Junho' : 5,
		'Julho' : 6,
		'Agosto' : 7,
		'Setembro' : 8,
		'Outubro' : 9,
		'Novembro' : 10,
		'Dezembro' : 11
	};
	KSystem.monthsAbrevIndex = {
		'Jan' : 0,
		'Fev' : 1,
		'Mar' : 2,
		'Abr' : 3,
		'Mai' : 4,
		'Jun' : 5,
		'Jul' : 6,
		'Ago' : 7,
		'Set' : 8,
		'Out' : 9,
		'Nov' : 10,
		'Dez' : 11
	};
	
	KSystem.days = [
		'Domingo',
		'Segunda-feira',
		'Ter\u00e7a-feira',
		'Quarta-feira',
		'Quinta-feira',
		'Sexta-feira',
		'S\u00e1bado',
		'Domingo'
	];
	KSystem.daysAbrev = [
		'Dom',
		'Seg',
		'Ter',
		'Qua',
		'Qui',
		'Sext',
		'Sab',
		'Dom'
	];
	
	KSystem.daysIndex = {
		'Domingo' : 0,
		'Segunda-feira' : 1,
		'Ter\u00e7a-feira' : 2,
		'Quarta-feira' : 3,
		'Quinta-feira' : 4,
		'Sexta-feira' : 5,
		'S\u00e1bado' : 6,
		'Domingo' : 7
	};
	KSystem.daysAbrevIndex = {
		'Dom' : 0,
		'Seg' : 1,
		'Ter' : 2,
		'Qua' : 3,
		'Qui' : 4,
		'Sex' : 5,
		'Sab' : 6,
		'Dom' : 7
	};
	
	KSystem.formatDate = function(format, date) {
		var toReturn = '';
		var escape = false;
		toReturn = format.replace(/(\w+)|%/g, function(wholematch, match) {
			if (wholematch == '%') {
				escape = true;
				return '';
			}
			if (escape) {
				escape = false;
				return match;
			}
			switch (match) {
				case 'Y': {
					return date.getFullYear();
				}
				case 'M': {
					return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
				}
				case 'MM': {
					return KSystem.monthsAbrev[date.getMonth()];
				}
				case 'MMM': {
					return KSystem.months[date.getMonth()];
				}
				case 'D': {
					return KSystem.daysAbrev[date.getDay()];
				}
				case 'DD': {
					return KSystem.days[date.getDay()];
				}
				case 'd': {
					return (date.getDate() < 10 ? '0' : '') + date.getDate();
				}
				case 'H': {
					return date.getHours();
				}
				case 'i': {
					return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
				}
				case 's': {
					return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
				}
				default: {
					return match;
				}
			}
		});
		return toReturn.replace(/\|/g, '');
	};
	
	KSystem.parseDate = function(format, date) {
		var toReturn = new Date();
		format += ' ';
		var pattIndex = 0;
		var valIndex = 0;
		var day = -1;
		var month = -1;
		
		format.replace(/(\w+)/g, function(wholematch, match) {
			var auxIndex = (format.indexOf(match, pattIndex) + match.length);
			var index = 0;
			if (auxIndex != 0) {
				index = date.indexOf(format.charAt(auxIndex), valIndex);
			}
			else {
				index = date.length;
			}
			pattIndex += match.length + 1;
			var value = date.substring(valIndex, index != -1 ? index : date.length);
			value = value.indexOf('0') == 0 ? value.substr(1) : value;
			
			valIndex = index + 1;
			if (value == '') {
				return value;
			}
			
			switch (match) {
				case 'Y': {
					toReturn.setFullYear(parseInt(value));
					break;
				}
				case 'M': {
					if (day != -1) {
						toReturn.setMonth(parseInt(value) - 1);
					}
					else {
						month = parseInt(value) - 1;
					}
					break;
				}
				case 'MM': {
					toReturn.setMonth(KSystem.monthsAbrevIndex(value));
					break;
				}
				case 'MMM': {
					toReturn.setMonth(KSystem.monthsIndex(value));
					break;
				}
				case 'd': {
					day = parseInt(value);
					toReturn.setDate(day);
					if (month != -1) {
						toReturn.setMonth(month);
					}
					break;
				}
				case 'H': {
					toReturn.setHours(parseInt(value));
					break;
				}
				case 'i': {
					toReturn.setMinutes(parseInt(value));
					break;
				}
				case 's': {
					toReturn.setSeconds(parseInt(value));
					break;
				}
				default: {
					return match;
					break;
				}
			}
			return value;
		});
		return toReturn;
	};
	
	KSystem.registerAddOn = function(addon) {
		KSystem.addons += ' ' + addon;
	};
	
	KSystem.isAddOn = function(addon) {
		return KSystem.addons.indexOf(addon) != -1;
	};

	KSystem.popup = {
		check: function(popup_window, widget){
			var _scope = this;
			if (popup_window) {
				if(/chrome/.test(navigator.userAgent.toLowerCase())){
					setTimeout(function () {
						_scope._is_popup_blocked(widget, popup_window);
					},200);
				}
				else{
					popup_window.onload = function () {
						_scope._is_popup_blocked(widget, popup_window);
					};
				}
			}
			else{
				widget.getShell().popupBlocked();
			}
		},
		_is_popup_blocked: function(scope, popup_window){
			if ((popup_window.innerHeight > 0)==false){ 				
				scope.getShell().popupBlocked(); 
			}
			delete popup_window;
		}
	};
	
	KSystem.deserializeURI = function (str, options) {
		var pairs = str.split(/&amp;|&/i), h = {}, options = options || {};
		for(var i = 0; i < pairs.length; i++) {
			var kv = pairs[i].split('=');
			kv[0] = decodeURIComponent(kv[0]);
			if(!options.except || options.except.indexOf(kv[0]) == -1) {
				if((/^\w+\[\w+\]$/).test(kv[0])) {
					var matches = kv[0].match(/^(\w+)\[(\w+)\]$/);
					if(typeof h[matches[1]] === 'undefined') {
						h[matches[1]] = {};
					}
					h[matches[1]][matches[2]] = decodeURIComponent(kv[1]);
				} 
				else {
					h[kv[0]] = decodeURIComponent(kv[1]);
				}
			}
		}
		return h;
	};

	KSystem.ident = 0;
	KSystem.identation = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	KSystem.includes = {};
	KSystem.addons = '';
	
	KSystem.KVAR = null;
	
}

function debug() {
	try {
		if (console && console.log) {
			console.log(arguments);
			return;
		}
	}
	catch (e) {
		return;
	}
	
	if (!Kinky.DEV_MODE) {
		return;
	}
	alert(JSON.stringify(arguments));
}

function dump() {
	if (!Kinky.DEV_MODE) {
		return;
	}
	
	if (Kinky.DUMP_MODE == 'console') {
		try {
			if (console && console.info) {
				console.info(arguments);
				return;
			}
		}
		catch (e) {
			return;
		}
	}
	else {
		if (KSystem.dump) {
			KSystem.dump(JSON.stringify(arguments));
			return;
		}
		
	}
}

function oalert() {
	if (!Kinky.DEV_MODE) {
		return;
	}
	
	alert(JSON.stringify(arguments));
}

function HSV(h, s, v) {
	if (h <= 0) { h = 0; }
	if (s <= 0) { s = 0; }
	if (v <= 0) { v = 0; }
 
	if (h > 360) { h = 360; }
	if (s > 100) { s = 100; }
	if (v > 100) { v = 100; }
 
	this.h = h;
	this.s = s;
	this.v = v;
}
 
function RGB(r, g, b, a) {
	if (r <= 0) { r = 0; }
	if (g <= 0) { g = 0; }
	if (b <= 0) { b = 0; }
 
	if (r > 255) { r = 255; }
	if (g > 255) { g = 255; }
	if (b > 255) { b = 255; }
 
	this.r = r;
	this.g = g;
	this.b = b;
	this.a = a || 1;
}
 
function CMYK(c, m, y, k) {
	if (c <= 0) { c = 0; }
	if (m <= 0) { m = 0; }
	if (y <= 0) { y = 0; }
	if (k <= 0) { k = 0; }
 
	if (c > 100) { c = 100; }
	if (m > 100) { m = 100; }
	if (y > 100) { y = 100; }
	if (k > 100) { k = 100; }
 
	this.c = c;
	this.m = m;
	this.y = y;
	this.k = k;
}
 
var ColorConverter = {
 
	_RGBtoHSV : function  (RGB) {
		var result = new HSV(0, 0, 0);
 
		r = RGB.r / 255;
		g = RGB.g / 255;
		b = RGB.b / 255;
 
		var minVal = Math.min(r, g, b);
		var maxVal = Math.max(r, g, b);
		var delta = maxVal - minVal;
 
		result.v = maxVal;
 
		if (delta == 0) {
			result.h = 0;
			result.s = 0;
		} else {
			result.s = delta / maxVal;
			var del_R = (((maxVal - r) / 6) + (delta / 2)) / delta;
			var del_G = (((maxVal - g) / 6) + (delta / 2)) / delta;
			var del_B = (((maxVal - b) / 6) + (delta / 2)) / delta;
 
			if (r == maxVal) { result.h = del_B - del_G; }
			else if (g == maxVal) { result.h = (1 / 3) + del_R - del_B; }
			else if (b == maxVal) { result.h = (2 / 3) + del_G - del_R; }
 
			if (result.h < 0) { result.h += 1; }
			if (result.h > 1) { result.h -= 1; }
		}
 
		result.h = Math.round(result.h * 360);
		result.s = Math.round(result.s * 100);
		result.v = Math.round(result.v * 100);
 
		return result;
	},
 
	_HSVtoRGB : function  (HSV) {
		var result = new RGB(0, 0, 0);
 
		var h = HSV.h / 360;
		var s = HSV.s / 100;
		var v = HSV.v / 100;
 
		if (s == 0) {
			result.r = v * 255;
			result.g = v * 255;
			result.v = v * 255;
		} else {
			var_h = h * 6;
			var_i = Math.floor(var_h);
			var_1 = v * (1 - s);
			var_2 = v * (1 - s * (var_h - var_i));
			var_3 = v * (1 - s * (1 - (var_h - var_i)));
 
			if (var_i == 0) {var_r = v; var_g = var_3; var_b = var_1}
			else if (var_i == 1) {var_r = var_2; var_g = v; var_b = var_1}
			else if (var_i == 2) {var_r = var_1; var_g = v; var_b = var_3}
			else if (var_i == 3) {var_r = var_1; var_g = var_2; var_b = v}
			else if (var_i == 4) {var_r = var_3; var_g = var_1; var_b = v}
			else {var_r = v; var_g = var_1; var_b = var_2};
 
			result.r = var_r * 255;
			result.g = var_g * 255;
			result.b = var_b * 255;
 
			result.r = Math.round(result.r);
			result.g = Math.round(result.g);
			result.b = Math.round(result.b);
		}
 
		return result;
	},
 
	_CMYKtoRGB : function (CMYK){
		var result = new RGB(0, 0, 0);
 
		c = CMYK.c / 100;
		m = CMYK.m / 100;
		y = CMYK.y / 100;
		k = CMYK.k / 100;
 
		result.r = 1 - Math.min( 1, c * ( 1 - k ) + k );
		result.g = 1 - Math.min( 1, m * ( 1 - k ) + k );
		result.b = 1 - Math.min( 1, y * ( 1 - k ) + k );
 
		result.r = Math.round( result.r * 255 );
		result.g = Math.round( result.g * 255 );
		result.b = Math.round( result.b * 255 );
 
		return result;
	},
 
	_RGBtoCMYK : function (RGB){
		var result = new CMYK(0, 0, 0, 0);
 
		r = RGB.r / 255;
		g = RGB.g / 255;
		b = RGB.b / 255;
 
		result.k = Math.min( 1 - r, 1 - g, 1 - b );
		result.c = ( 1 - r - result.k ) / ( 1 - result.k );
		result.m = ( 1 - g - result.k ) / ( 1 - result.k );
		result.y = ( 1 - b - result.k ) / ( 1 - result.k );
 
		result.c = Math.round( result.c * 100 );
		result.m = Math.round( result.m * 100 );
		result.y = Math.round( result.y * 100 );
		result.k = Math.round( result.k * 100 );
 
		return result;
	},
 
	toRGB : function (o) {
		if (typeof o == 'string') { 
			if (o.charAt(0)=="#") { return new RGB(this.hexToR(o), this.hexToG(o), this.hexToB(o)); }
			return this.rgbaToRGBA(o);
		}
		if (o instanceof RGB) { return o; }
		if (o instanceof HSV) {	return this._HSVtoRGB(o); }
		if (o instanceof CMYK) { return this._CMYKtoRGB(o); }
	},
 
	toHSV : function (o) {
		if (o instanceof HSV) { return o; }
		if (o instanceof RGB) { return this._RGBtoHSV(o); }
		if (o instanceof CMYK) { return this._RGBtoHSV(this._CMYKtoRGB(o)); }
	},
 
	toCMYK : function (o) {
		if (o instanceof CMYK) { return o; }
		if (o instanceof RGB) { return this._RGBtoCMYK(o); }
		if (o instanceof HSV) { return this._RGBtoCMYK(this._HSVtoRGB(o)); }
	},

	componentToHex : function (c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	},

	toHex : function(o) {
		if (o instanceof RGB) { 
			return (this.componentToHex(o.r) + this.componentToHex(o.g) + this.componentToHex(o.b)).toUpperCase();
		}
	},

	toString : function (o) {
		if (o instanceof CMYK) { return 'cmyk(' + o.c + '%, ' + o.m + '%, ' + o.y + '%, ' + o.k + '%)'; }
		if (o instanceof RGB) { return 'rgba(' + o.r + ', ' + o.g + ', ' + o.b + ', ' + o.a + ')'; }
		if (o instanceof HSV) { return 'hsv(' + o.h + ', ' + o.s + ', ' + o.v + ')'; }
	},

	hexToR : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(0,1),16);
		}
		return parseInt((this.cutHex(h)).substring(0,2),16);
	},

	hexToG : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(1,2),16);
		}
		return parseInt((this.cutHex(h)).substring(2,4),16);
	},

	hexToB : function(h) {
		if (h.length < 6) {
			return parseInt((this.cutHex(h)).substring(2,3),16);
		}
		return parseInt((this.cutHex(h)).substring(4,6),16);
	},

	rgbaToRGBA : function(h) {
		if (h.indexOf('rgba') != -1) {
			var parts = h.split('rgba')[1].split('(')[1].split(')')[0].split(',');
			return new RGB(parseInt(parts[0].trim()), parseInt(parts[1].trim()), parseInt(parts[2].trim()), parseInt(parts[3].trim()));
		} 
		else if (h.indexOf('rgb') != -1) {
			var parts = h.split('rgb')[1].split('(')[1].split(')')[0].split(',');
			return new RGB(parseInt(parts[0].trim()), parseInt(parts[1].trim()), parseInt(parts[2].trim()));
		}
	},

	cutHex : function(h) {
		return (h.charAt(0)=="#") ? h.substring(1,7) : h;
	}
 
};
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function KConnectionsPool() {
}
{
	
	KConnectionsPool.getConnection = function(type, shell) {
		if (!shell) {
			throw Error('No Shell defined for connection pool');
		}
		if (!KConnectionsPool.connections[shell]) {
			KConnectionsPool.connections[shell] = {};
		}
		var connection = KConnectionsPool.connections[shell][type];
		if (!connection) {
			KConnectionsPool.connections[shell][type] = connection = KSystem.construct({
				jsClass : KConnectionsPool.connectors[type],
				args : {
					servicesURL : !!Kinky.SHELL_CONFIG[shell].providers[type] ? Kinky.SHELL_CONFIG[shell].providers[type].services : undefined,
					dataURL : !!Kinky.SHELL_CONFIG[shell].providers[type] ? Kinky.SHELL_CONFIG[shell].providers[type].data : undefined,
					headers : Kinky.SHELL_CONFIG[shell].headers
				}
			});
			connection.shell = shell;
		}
		if (Kinky.getShellConfig(shell)) {
			connection.headers = Kinky.getShellConfig(shell).headers;
		}
		return connection;
		
	};
	
	KConnectionsPool.registerConnector = function(type, connectorClass) {
		KConnectionsPool.connectors[type] = connectorClass;
	};
	
	KConnectionsPool.connectors = {};
	KConnectionsPool.connections = {};
	
	KConnectionsPool.registerConnector('php', 'KConnectionPHP');
	KConnectionsPool.registerConnector('jsonp', 'KConnectionJSONP');
	KConnectionsPool.registerConnector('js', 'KConnectionJSON');
	KConnectionsPool.registerConnector('rest', 'KConnectionREST');
	KConnectionsPool.registerConnector('oauth', 'KConnectionOAuth');
	KConnectionsPool.registerConnector('https', 'KConnectionHTTP');
	KConnectionsPool.registerConnector('http', 'KConnectionHTTP');
	
	KSystem.included('KConnectionsPool');
}
function KConnection(url, dataURL, headers) {
	if (!!url && !!dataURL) {
		this.urlBase = url;
		this.dataUrlBase = dataURL;
		this.headers = headers;
		this.shell = null;
	}
}
{
	KConnection.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		var connection = this;
		channel.onreadystatechange = function() {
			connection.receive(request, channel);
		};
		return channel;
	};
	
	KConnection.prototype.setURL = function(url) {
		this.urlBase = url;
	};
	
	KConnection.prototype.setDataURL = function(url) {
		this.dataUrlBase = url;
	};
	
	KConnection.prototype.getServiceParams = function(request, params) {
		if (request) {
			if (params) {
				for ( var paramName in params) {
					request[paramName] = params[paramName];
				}
			}
			return 'id=' + request.id + '&request=' + encodeURIComponent(JSON.stringify(request));
			
		}
		return null;
	};
	
	KConnection.prototype.send = function(request, params, user, password) {
		throw new KNoExtensionException(this, 'send');
	};
	
	KConnection.prototype.openChannel = function() {
		return KConnection.createAJAXChannel();
	};
	
	KConnection.createAJAXChannel = function() {
		var channel = null;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { // IE
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}
		return channel;
	};
	
	KConnection.prototype.setHeaders = function(headers, channel) {
		var merged = {};
		for ( var header in this.headers) {
			merged[header] = this.headers[header];
		}
		for ( var header in headers) {
			merged[header] = headers[header];
		}
		for ( var header in merged) {
			channel.setRequestHeader(header, merged[header]);
		}
	};
	
	KConnection.getBasicHTTPAuthentication = function(user, password) {
		var tok = user + ':' + password;
		var hash = Base64.encode(tok);
		return "Basic " + hash;
	};
	
	KConnection.getOAuthHTTPAuthentication = function(access_token) {
		access_token = access_token || KOAuth.getAccessToken();
		if (access_token) {
			return (/OAuth2.0 /.test(access_token) ? access_token : "OAuth2.0 " + access_token);
		}
		else {
			return null;
		}
	};
	
	KConnection.prototype.receive = function(request, ajaxResponse) {
		if (ajaxResponse.readyState == 4) {
			if (ajaxResponse.getResponseHeader('X-Access-Token')) {
				// REMOVED DUE TO PROBLEMS WHEN CONNECTING TO SEVERAL BACKENDS -> DO NOT UNCOMMENT
				/*KOAuth.session({
					authentication : ajaxResponse.getResponseHeader('X-Access-Token')
				}, request, this.shell);*/
			}
			var widget = Kinky.getWidget(request.id);
			
			if (widget) {
				if (ajaxResponse.status == 200) {
					var response = null;
					var contentType = ajaxResponse.getResponseHeader('Content-Type');
					
					if (contentType && ((contentType.indexOf('text/plain') != -1) || (contentType.indexOf('javascript') != -1) || (contentType.indexOf('json') != -1))) {
						try {
							eval('response = ' + ajaxResponse.responseText + ';');
							response.http = new Object();
							response.http.status = 200;
						}
						catch (e) {
							if ((ajaxResponse.responseText == null) || (ajaxResponse.responseText == '')) {
								response = {
									http : {
										status : 200
									}
								};
							}
							else {
								response = {
									http : {
										status : -1
									},
									code : ajaxResponse.status,
									message : "Could not parse string:\n" + ajaxResponse.responseText
								};
							}
						}
					}
					else {
						response = {
							http : {
								status : 200
							},
							text : ajaxResponse.responseText,
							type : contentType
						};
					}
					
					this.includeHeaders(ajaxResponse, response);
					if (!!response.http.headers.Location) {
						if (!!response.links) {
							response.links.redirect = new Object();
							response.links.redirect.href = response.http.headers.Location;
						}
						else {
							response.links = {
								redirect : {
									href : response.http.headers.Location
								}
							};
						}
					}
					
					if (response.http.status && (response.http.status != 200)) {
						this.processNonOk(widget, response, request);
					}
					else {
						Kinky.startSession(request.user, request.secret, response.user || Kinky.bunnyMan.loggedUser);
						
						for ( var callback in request.callback) {
							eval(request.callback[callback] + '(response, request);');
						}
						
						delete request;
					}
				}
				else {
					var response = null;
					var contentType = ajaxResponse.getResponseHeader('Content-Type');
					
					if (contentType && ((contentType.indexOf('text/plain') != -1) || (contentType.indexOf('json') != -1))) {
						try {
							eval('response = ' + ajaxResponse.responseText + ';');
							response.http = new Object();
							response.http.status = ajaxResponse.status;
						}
						catch (e) {
							if ((ajaxResponse.responseText == null) || (ajaxResponse.responseText == '')) {
								response = {
									http : {
										status : ajaxResponse.status
									}
								};
							}
							else {
								response = {
									http : {
										status : ajaxResponse.status
									},
									code : ajaxResponse.status,
									message : "Could not parse string:\n" + ajaxResponse.responseText
								};
							}
						}
					}
					else {
						response = {
							http : {
								status : ajaxResponse.status
							},
							text : ajaxResponse.responseText,
							type : contentType
						};
					}
					
					this.includeHeaders(ajaxResponse, response);
					
					delete request;
					if (!!response.http.headers.Location) {
						if (!!response.links) {
							response.links.redirect = new Object();
							response.links.redirect.href = response.http.headers.Location;
						}
						else {
							response.links = {
								redirect : {
									href : response.http.headers.Location
								}
							};
						}
					}
					
					response.http.status = response.http.status || ajaxResponse.status;
					this.processNonOk(widget, response, request);
				}
				if (widget && widget.resume) {
					widget.resume(true);
				}
			}
			else {
				Kinky.getShell(this.shell).usingLoader--;
				if ((Kinky.getShell(this.shell).usingLoader <= 0) && Kinky.getShell(this.shell).loader) {
					KCSS.setStyle({
						display : 'none'
					}, [
						Kinky.getShell(this.shell).loader
					]);
				}
			}
		}
	};
	
	KConnection.prototype.includeHeaders = function(ajaxResponse, response) {
		response.http.statusLine = ajaxResponse.status + ' ' + ajaxResponse.statusText + ' HTTP/1.1';
		var headers = [
			"X-Access-Token",
			"X-Access-Token-Expires",
			"X-Error-Reason",
			"X-Error",
			"Cache-Control",
			"Content-Length",
			"Content-Type",
			"Content-Language",
			"Expires",
			"Location"
		];
		response.http.headers = {};
		for ( var header in headers) {
			var headerVal = ajaxResponse.getResponseHeader(headers[header]);
			if (headerVal) {
				response.http.headers[headers[header]] = headerVal;
			}
		}
	};
	
	KConnection.prototype.processNonOk = function(widget, nonOk, request) {
		if (widget && widget.resume) {
			widget.resume(true);
		}

		switch (nonOk.http.status) {
			case 200: {
				break;
			}
			case 302: {
				break;
			}
			case 303: {
				alert(JSON.stringify(nonOk));
				window.document.location = nonOk.links.redirect.href;
				break;
			}
			case 307: {
				var regex = new RegExp(Kinky.SITE_URL);
				if (!/http([s]*):\/\//.test(nonOk.links.redirect.href) || regex.test(nonOk.links.redirect.href)) {
					KBreadcrumb.dispatchURL({
						url : nonOk.links.redirect.href.replace(Kinky.SITE_URL, '').replace('#', '')
					});
				}
				else {
					var url = encodeURIComponent(window.document.location.href);
					window.document.location = nonOk.links.redirect.href + (/\?/.test(nonOk.links.redirect.href) ? '&' : '?') + 'redirect_uri=' + url;
				}
				break;
			}
			case 201: {
				widget.onCreated(nonOk, request);
				break;
			}
			case 202: {
				widget.onAccepted(nonOk, request);
				break;
			}
			case 1223:
			case 204: {
				widget.onNoContent(nonOk, request);
				break;
			}
			case 400: {
				nonOk.error = true;
				widget.onBadRequest(nonOk, request);
				break;
			}
			case 401: {
				nonOk.error = true;
				widget.onUnauthorized(nonOk, request);
				break;
			}
			case 403: {
				nonOk.error = true;
				widget.onForbidden(nonOk, request);
				break;
			}
			case 404: {
				nonOk.error = true;
				widget.onNotFound(nonOk, request);
				break;
			}
			case 405: {
				nonOk.error = true;
				widget.onMethodNotAllowed(nonOk, request);
				break;
			}
			case 412: {
				nonOk.error = true;
				widget.onPreConditionFailed(nonOk, request);
				break;
			}
			case 500: {
				nonOk.error = true;
				widget.onInternalError(nonOk, request);
				break;
			}
			default: {
				nonOk.error = true;
				widget.onError(nonOk, request);
				break;
			}
		}
	};
	
	KSystem.included('KConnection');
}
function KConnectionREST(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.call(this, url, dataURL, headers);
	}
}
{
	KConnectionsPool.registerConnector('rest', 'KConnectionREST');
	
	KConnectionREST.prototype = new KConnection();
	KConnectionREST.prototype.constructor = KConnectionREST;
	
	KConnectionREST.prototype.setURL = function(url) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.prototype.setURL.call(this, url);
	};
	
	KConnectionREST.prototype.getServiceParams = function(request, params) {
		var toSend = '';
		if (params) {
			for ( var paramName in params) {
				if (/format/.test(paramName)) {
					continue;
				}
				if (toSend.length != 0) {
					toSend += '&';
				}
				else {
					toSend += '?';
				}
				toSend += paramName + '=' + encodeURIComponent(params[paramName]);
			}
		}
		return toSend;
	};
	
	KConnectionREST.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var content = null;
		var url = this.urlBase + request.service;
		var method = request.action.toUpperCase();
		

		switch (request.action) {
			case 'head':
			case 'get': {
				url += this.getServiceParams(null, params);
				break;
			}
			default: {
				content = JSON.stringify(params);
				break;
			}
		}
		
		channel.open(method, url, true);
		channel.setRequestHeader('Accept', 'application/json');
		if (content) {
			channel.setRequestHeader('Content-Type', 'application/json');
		}
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KSystem.included('KConnectionREST');
}
function KDOM() {
}
{
	KDOM.prototype = {};
	KDOM.prototype.constructor = KDOM;
	
	KDOM.removeEventListener = function(element, eventName, handler) {
		if (element.detachEvent) {
			element.detachEvent('on' + eventName, handler);
		}
		else {
			element.removeEventListener(eventName, handler, false);
		}
	};
	
	KDOM.addEventListener = function(element, eventName, handler) {
		if (element.attachEvent) {
			element.attachEvent('on' + eventName, handler);
		}
		else {
			element.addEventListener(eventName, handler, false);
		}
	};
	
	KDOM.fireEvent = function(element, eventName) {
		if (element.fireEvent) {
			if (element.fireEvent) {
				try {
					element.fireEvent('on' + eventName);
				}
				catch (e) {
				}
			}
			else {
				debug('No fireevent for ' + element.tagName);
			}
		}
		else {
			var evt = document.createEvent("HTMLEvents");
			evt.initEvent(eventName, true, true);
			return !element.dispatchEvent(evt);
		}
	};
	
	KDOM.stopEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		}
		event.returnValue = false;
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		event.cancelBubble = true;
	};
	
	KDOM.getEventTarget = function(event) {
		event = event || window.event;
		
		if (event == null) {
			return null;
		}
		
		return event.target || event.srcElement;
	};
	
	KDOM.getEventCurrentTarget = function(event) {
		event = event || window.event;
		
		if (event == null) {
			return null;
		}
		
		return event.currentTarget || event.srcElement;
	};
	
	KDOM.isRelatedTarget = function(event, parent, mouseOut) {
		var mouseEvent = KDOM.getEvent(event);
		var element = null;
		if (mouseOut) {
			element = mouseEvent.toElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		else {
			element = mouseEvent.fromElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		if (!element) {
			return false;
		}
		if (element.id == parent.id) {
			return true;
		}
		if ((element != parent) && (element != null) && (parent != null)) {
			var nodes = parent.getElementsByTagName(element.tagName);
			for ( var i = 0; i != nodes.length; i++) {
				if (nodes[i].id == element.id) {
					return true;
				}
			}
		}
		return false;
	};
	
	KDOM.getMouseCoords = function(event) {
// var posx = 0;
// var posy = 0;
		event = event || window.event;
// if (event.pageX || event.pageY) {
// posx = event.pageX;
// posy = event.pageY;
// }
// else if (event.clientX || event.clientY) {
// posx = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
// posy = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
// }
		return {
			x : event.clientX,
			y : event.clientY
		};
		
	};
	
	KDOM.getEventRelatedTarget = function(event, mouseOut) {
		var mouseEvent = KDOM.getEvent(event);
		var element = null;
		if (mouseOut) {
			element = mouseEvent.toElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		else {
			element = mouseEvent.fromElement || (mouseEvent.relatedTarget && mouseEvent.relatedTarget.nodeName ? mouseEvent.relatedTarget : null);
		}
		return element;
	};
	
	KDOM.getEvent = function(event) {
		var toReturn = null;
		toReturn = event || window.event;
		return toReturn;
	};
	
	KDOM.getWidget = function(element, className) {
		var toReturn = element;
		
		if (className != null) {
			while (toReturn != null) {
				if (/^widget([0-9]+)$/.test(toReturn.id)) {
					var object = Kinky.bunnyMan.widgets[toReturn.id];
					if (object) {
						var isParent = false;
						eval('isParent = object instanceof ' + className + ';');
						if (isParent) {
							break;
						}
					}
				}
				toReturn = toReturn.parentNode;
			}
		}
		else {
			try {
				while (!/^widget([0-9]+)$/.test(toReturn.id)) {
					toReturn = toReturn.parentNode;
				}
			}
			catch (e) {
				return null;
			}
		}
		if (toReturn) {
			return Kinky.getWidget(toReturn.id);
		}
		return null;
	};
	
	KDOM.getEventWidget = function(event, className) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		if (className != null) {
			while (element != null) {
				if (/^widget([0-9]+)$/.test(element.id)) {
					var object = Kinky.bunnyMan.widgets[element.id];
					if (object) {
						var isParent = false;
						eval('isParent = object instanceof ' + className + ';');
						if (isParent) {
							break;
						}
					}
				}
				element = element.parentNode;
			}
		}
		else {
			while ((element != null) && !/^widget([0-9]+)$/.test(element.id)) {
				element = element.parentNode;
			}
		}
		if (!element) {
			// throw new Error('KDOM', "No Widget for element");
			return null;
		}
		return Kinky.bunnyMan.widgets[element.id];
	};
	
	KDOM.normalizePixelValue = function(value) {
		var v = value.replace(/px/g, '');
		var ret = parseInt(v);
		if (isNaN(ret)) {
			ret = parseFloat(v);
			if (isNaN(ret)) {
				return undefined;
			}
		}
		return ret;
	};
	
	KDOM.extractColorInfo = function(value) {
		try {
			if (value.substr(0, 1) === '#') {
				var info = new Object();
				
				var rgb_from = parseInt(value.replace("#", ""), 16);
				
				var red = (rgb_from & (255 << 16)) >> 16;
				var green = (rgb_from & (255 << 8)) >> 8;
				var blue = (rgb_from & 255) >> 0;
				
				info.red = red;
				info.green = green;
				info.blue = blue;
				
				return info;
				
			}
			else if (value.substr(0, 4) === 'rgb(') {
				var info = new Object();
				
				var digits = /(.*?)rgb\((\d+),(\d+),(\d+)\)/.exec(value.replace(/ /g, ''));
				var red = parseInt(digits[2]);
				var green = parseInt(digits[3]);
				var blue = parseInt(digits[4]);
				
				info.red = red;
				info.green = green;
				info.blue = blue;
				
				return info;
				
			}
			
		}
		catch (e) {
			alert(value + ": " + e.message);
		}
	};
	
	KDOM.getBrowserHeight = function() {
		if (window.innerHeight) {
			return window.innerHeight;
		}
		else {
			if (window.document.documentElement && window.document.documentElement.clientHeight) {
				return window.document.documentElement.clientHeight;
			}
			else if (window.document.body) {
				return document.body.clientHeight;
			}
		}
		return 0;
	};
	
	KDOM.getBrowserWidth = function() {
		if (window.document.documentElement && window.document.documentElement.clientWidth) {
			return window.document.documentElement.clientWidth;
		}
		else if (window.document.body) {
			return document.body.clientWidth;
		}
		else if (window.innerWidth) {
			return window.innerWidth;
		}
		
		return 0;
	};
	
	KDOM.mouseX = function(event) {
		if (event.pageX) {
			return event.pageX;
		}
		else if (event.clientX) {
			return event.clientX + (window.document.documentElement.scrollLeft ? window.document.documentElement.scrollLeft : window.document.body.scrollLeft);
		}
		else {
			return null;
		}
	};
	
	KDOM.mouseY = function(event) {
		if (event.pageY) {
			return event.pageY;
		}
		else if (event.clientY) {
			return event.clientY + (window.document.documentElement.scrollTop ? window.document.documentElement.scrollTop : window.document.body.scrollTop);
		}
		else {
			return null;
		}
	};

	KDOM.offsetTop = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetTop;
			parent = parent.offsetParent;
		}
		return toReturn;
	};

	KDOM.offsetLeft = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetLeft;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parent.offsetParent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KDOM.getWidth = function(target) {
		return target.offsetWidth;
	};

	KDOM.getHeight = function(target) {
		return target.offsetHeight;
	};

	KDOM.scrollTop = function(parent) {
		var toReturn = 0;
		while (parent != null) {
			if ((parent.nodeName.toLowerCase() != 'html') && parent.scrollTop !== undefined) {
				toReturn += parent.scrollTop;
			}
			parent = parent.parentNode;
		}
		if (!!window.document.body.style.overflow && ((window.document.body.style.overflow == 'hidden'))) {
			return toReturn - (!!window.pageYOffset ? window.pageYOffset : 0);
		}
		return toReturn;
	};
	
	KDOM.moveToEndOfContenteditable = function(contentEditableElement){
		contentEditableElement = contentEditableElement.lastChild;
		if (!contentEditableElement) {
			return;
		}

		var range,selection;
		if(document.createRange) {    
			range = document.createRange();
			range.selectNodeContents(contentEditableElement);
			range.collapse(false);
			selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
		}
		else if(document.selection) { 
			range = document.body.createTextRange();
			range.moveToElementText(contentEditableElement);
			range.collapse(false);
			range.select();
		}
	}

	KSystem.included('KDOM');
}

var Base64 = {
	
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	
	// public method for encoding
	encode : function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		
		input = Base64._utf8_encode(input);
		
		while (i < input.length) {
			
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
			
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			}
			else if (isNaN(chr3)) {
				enc4 = 64;
			}
			
			output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
			
		}
		
		return output;
	},
	
	// public method for decoding
	decode : function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		
		while (i < input.length) {
			
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
			
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			
			output = output + String.fromCharCode(chr1);
			
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
			
		}
		
		output = Base64._utf8_decode(output);
		
		return output;
		
	},
	
	// private method for UTF-8 encoding
	_utf8_encode : function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";
		
		for ( var n = 0; n < string.length; n++) {
			
			var c = string.charCodeAt(n);
			
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			
		}
		
		return utftext;
	},
	
	// private method for UTF-8 decoding
	_utf8_decode : function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
		
		while (i < utftext.length) {
			
			c = utftext.charCodeAt(i);
			
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
			
		}
		
		return string;
	}

};

KSystem.included('Base64');
var HTMLEntities = {
	
	decode : function(data) {
		data = data.replace(/&(\w+);/g, function(wholematch, match) {
			switch (match) {
				case 'nbsp':
					return String.fromCharCode(32);
					
				case 'iexcl':
					return String.fromCharCode(161);
					
				case 'cent':
					return String.fromCharCode(162);
					
				case 'pound':
					return String.fromCharCode(163);
					
				case 'curren':
					return String.fromCharCode(164);
					
				case 'yen':
					return String.fromCharCode(165);
					
				case 'brvbar':
					return String.fromCharCode(166);
					
				case 'sect':
					return String.fromCharCode(167);
					
				case 'uml':
					return String.fromCharCode(168);
					
				case 'copy':
					return String.fromCharCode(168);
					
				case 'ordf':
					return String.fromCharCode(170);
					
				case 'laquo':
					return String.fromCharCode(171);
					
				case 'not':
					return String.fromCharCode(172);
					
				case 'shy':
					return String.fromCharCode(173);
					
				case 'reg':
					return String.fromCharCode(174);
					
				case 'macr':
					return String.fromCharCode(175);
					
				case 'deg':
					return String.fromCharCode(176);
					
				case 'plusmn':
					return String.fromCharCode(177);
					
				case 'sup2':
					return String.fromCharCode(178);
					
				case 'sup3':
					return String.fromCharCode(179);
					
				case 'acute':
					return String.fromCharCode(180);
					
				case 'micro':
					return String.fromCharCode(181);
					
				case 'para':
					return String.fromCharCode(182);
					
				case 'middot':
					return String.fromCharCode(183);
					
				case 'cedil':
					return String.fromCharCode(184);
					
				case 'sup1':
					return String.fromCharCode(185);
					
				case 'ordm':
					return String.fromCharCode(186);
					
				case 'raquo':
					return String.fromCharCode(187);
					
				case 'frac14':
					return String.fromCharCode(188);
					
				case 'frac12':
					return String.fromCharCode(189);
					
				case 'frac34':
					return String.fromCharCode(190);
					
				case 'iquest':
					return String.fromCharCode(191);
					
				case 'Agrave':
					return String.fromCharCode(192);
					
				case 'Aacute':
					return String.fromCharCode(193);
					
				case 'Acirc':
					return String.fromCharCode(194);
					
				case 'Atilde':
					return String.fromCharCode(195);
					
				case 'Auml':
					return String.fromCharCode(196);
					
				case 'Aring':
					return String.fromCharCode(197);
					
				case 'AElig':
					return String.fromCharCode(198);
					
				case 'Ccedil':
					return String.fromCharCode(199);
					
				case 'Egrave':
					return String.fromCharCode(200);
					
				case 'Eacute':
					return String.fromCharCode(201);
					
				case 'Ecirc':
					return String.fromCharCode(202);
					
				case 'Euml':
					return String.fromCharCode(203);
					
				case 'Igrave':
					return String.fromCharCode(204);
					
				case 'Iacute':
					return String.fromCharCode(205);
					
				case 'Icirc':
					return String.fromCharCode(206);
					
				case 'Iuml':
					return String.fromCharCode(207);
					
				case 'ETH':
					return String.fromCharCode(208);
					
				case 'Ntilde':
					return String.fromCharCode(209);
					
				case 'Ograve':
					return String.fromCharCode(210);
					
				case 'Oacute':
					return String.fromCharCode(211);
					
				case 'Ocirc':
					return String.fromCharCode(212);
					
				case 'Otilde':
					return String.fromCharCode(213);
					
				case 'Ouml':
					return String.fromCharCode(214);
					
				case 'times':
					return String.fromCharCode(215);
					
				case 'Oslash':
					return String.fromCharCode(216);
					
				case 'Ugrave':
					return String.fromCharCode(217);
					
				case 'Uacute':
					return String.fromCharCode(218);
					
				case 'Ucirc':
					return String.fromCharCode(219);
					
				case 'Uuml':
					return String.fromCharCode(220);
					
				case 'Yacute':
					return String.fromCharCode(221);
					
				case 'THORN':
					return String.fromCharCode(222);
					
				case 'szlig':
					return String.fromCharCode(223);
					
				case 'agrave':
					return String.fromCharCode(224);
					
				case 'aacute':
					return String.fromCharCode(225);
					
				case 'acirc':
					return String.fromCharCode(226);
					
				case 'atilde':
					return String.fromCharCode(227);
					
				case 'auml':
					return String.fromCharCode(228);
					
				case 'aring':
					return String.fromCharCode(229);
					
				case 'aelig':
					return String.fromCharCode(230);
					
				case 'ccedil':
					return String.fromCharCode(231);
					
				case 'egrave':
					return String.fromCharCode(232);
					
				case 'eacute':
					return String.fromCharCode(233);
					
				case 'ecirc':
					return String.fromCharCode(234);
					
				case 'euml':
					return String.fromCharCode(235);
					
				case 'igrave':
					return String.fromCharCode(236);
					
				case 'iacute':
					return String.fromCharCode(237);
					
				case 'icirc':
					return String.fromCharCode(238);
					
				case 'iuml':
					return String.fromCharCode(239);
					
				case 'eth':
					return String.fromCharCode(240);
					
				case 'ntilde':
					return String.fromCharCode(241);
					
				case 'ograve':
					return String.fromCharCode(242);
					
				case 'oacute':
					return String.fromCharCode(243);
					
				case 'ocirc':
					return String.fromCharCode(244);
					
				case 'otilde':
					return String.fromCharCode(245);
					
				case 'ouml':
					return String.fromCharCode(246);
					
				case 'divide':
					return String.fromCharCode(247);
					
				case 'oslash':
					return String.fromCharCode(248);
					
				case 'ugrave':
					return String.fromCharCode(249);
					
				case 'uacute':
					return String.fromCharCode(250);
					
				case 'ucirc':
					return String.fromCharCode(251);
					
				case 'uuml':
					return String.fromCharCode(252);
					
				case 'yacute':
					return String.fromCharCode(253);
					
				case 'thorn':
					return String.fromCharCode(254);
					
				case 'yuml':
					return String.fromCharCode(255);
					
				case 'quot':
					return String.fromCharCode(34);
					
				case 'apos':
					return String.fromCharCode(39);
					
				case 'lt':
					return String.fromCharCode(60);
					
				case 'gt':
					return String.fromCharCode(62);
					
				case 'amp':
					return String.fromCharCode(38);
					
			}
			return "";
		});
		return data;
	},
	
	encode : function(data, isHTML) {
		data = data.replace(/(.)/g, function(wholematch, match) {
			switch (match) {
				case String.fromCharCode(161):
					return '&iexcl;';
					
				case String.fromCharCode(162):
					return '&cent;';
					
				case String.fromCharCode(163):
					return '&pound;';
					
				case String.fromCharCode(164):
					return '&curren;';
					
				case String.fromCharCode(165):
					return '&yen;';
					
				case String.fromCharCode(166):
					return '&brvbar;';
					
				case String.fromCharCode(167):
					return '&sect;';
					
				case String.fromCharCode(168):
					return '&uml;';
					
				case String.fromCharCode(168):
					return '&copy;';
					
				case String.fromCharCode(170):
					return '&ordf;';
					
				case String.fromCharCode(171):
					return '&laquo;';
					
				case String.fromCharCode(172):
					return '&not;';
					
				case String.fromCharCode(173):
					return '&shy;';
					
				case String.fromCharCode(174):
					return '&reg;';
					
				case String.fromCharCode(175):
					return '&macr;';
					
				case String.fromCharCode(176):
					return '&deg;';
					
				case String.fromCharCode(177):
					return '&plusmn;';
					
				case String.fromCharCode(178):
					return '&sup2;';
					
				case String.fromCharCode(179):
					return '&sup3;';
					
				case String.fromCharCode(180):
					return '&acute;';
					
				case String.fromCharCode(181):
					return '&micro;';
					
				case String.fromCharCode(182):
					return '&para;';
					
				case String.fromCharCode(183):
					return '&middot;';
					
				case String.fromCharCode(184):
					return '&cedil;';
					
				case String.fromCharCode(185):
					return '&sup1;';
					
				case String.fromCharCode(186):
					return '&ordm;';
					
				case String.fromCharCode(187):
					return '&raquo;';
					
				case String.fromCharCode(188):
					return '&frac14;';
					
				case String.fromCharCode(189):
					return '&frac12;';
					
				case String.fromCharCode(190):
					return '&frac34;';
					
				case String.fromCharCode(191):
					return '&iquest;';
					
				case String.fromCharCode(192):
					return '&Agrave;';
					
				case String.fromCharCode(193):
					return '&Aacute;';
					
				case String.fromCharCode(194):
					return '&Acirc;';
					
				case String.fromCharCode(195):
					return '&Atilde;';
					
				case String.fromCharCode(196):
					return '&Auml;';
					
				case String.fromCharCode(197):
					return '&Aring;';
					
				case String.fromCharCode(198):
					return '&AElig;';
					
				case String.fromCharCode(199):
					return '&Ccedil;';
					
				case String.fromCharCode(200):
					return '&Egrave;';
					
				case String.fromCharCode(201):
					return '&Eacute;';
					
				case String.fromCharCode(202):
					return '&Ecirc;';
					
				case String.fromCharCode(203):
					return '&Euml;';
					
				case String.fromCharCode(204):
					return '&Igrave;';
					
				case String.fromCharCode(205):
					return '&Iacute;';
					
				case String.fromCharCode(206):
					return '&Icirc;';
					
				case String.fromCharCode(207):
					return '&Iuml;';
					
				case String.fromCharCode(208):
					return '&ETH;';
					
				case String.fromCharCode(209):
					return '&Ntilde;';
					
				case String.fromCharCode(210):
					return '&Ograve;';
					
				case String.fromCharCode(211):
					return '&Oacute;';
					
				case String.fromCharCode(212):
					return '&Ocirc;';
					
				case String.fromCharCode(213):
					return '&Otilde;';
					
				case String.fromCharCode(214):
					return '&Ouml;';
					
				case String.fromCharCode(215):
					return '&times;';
					
				case String.fromCharCode(216):
					return '&Oslash;';
					
				case String.fromCharCode(217):
					return '&Ugrave;';
					
				case String.fromCharCode(218):
					return '&Uacute;';
					
				case String.fromCharCode(219):
					return '&Ucirc;';
					
				case String.fromCharCode(220):
					return '&Uuml;';
					
				case String.fromCharCode(221):
					return '&Yacute;';
					
				case String.fromCharCode(222):
					return '&THORN;';
					
				case String.fromCharCode(223):
					return '&szlig;';
					
				case String.fromCharCode(224):
					return '&agrave;';
					
				case String.fromCharCode(225):
					return '&aacute;';
					
				case String.fromCharCode(226):
					return '&acirc;';
					
				case String.fromCharCode(227):
					return '&atilde;';
					
				case String.fromCharCode(228):
					return '&auml;';
					
				case String.fromCharCode(229):
					return '&aring;';
					
				case String.fromCharCode(230):
					return '&aelig;';
					
				case String.fromCharCode(231):
					return '&ccedil;';
					
				case String.fromCharCode(232):
					return '&egrave;';
					
				case String.fromCharCode(233):
					return '&eacute;';
					
				case String.fromCharCode(234):
					return '&ecirc;';
					
				case String.fromCharCode(235):
					return '&euml;';
					
				case String.fromCharCode(236):
					return '&igrave;';
					
				case String.fromCharCode(237):
					return '&iacute;';
					
				case String.fromCharCode(238):
					return '&icirc;';
					
				case String.fromCharCode(239):
					return '&iuml;';
					
				case String.fromCharCode(240):
					return '&eth;';
					
				case String.fromCharCode(241):
					return '&ntilde;';
					
				case String.fromCharCode(242):
					return '&ograve;';
					
				case String.fromCharCode(243):
					return '&oacute;';
					
				case String.fromCharCode(244):
					return '&ocirc;';
					
				case String.fromCharCode(245):
					return '&otilde;';
					
				case String.fromCharCode(246):
					return '&ouml;';
					
				case String.fromCharCode(247):
					return '&divide;';
					
				case String.fromCharCode(248):
					return '&oslash;';
					
				case String.fromCharCode(249):
					return '&ugrave;';
					
				case String.fromCharCode(250):
					return '&uacute;';
					
				case String.fromCharCode(251):
					return '&ucirc;';
					
				case String.fromCharCode(252):
					return '&uuml;';
					
				case String.fromCharCode(253):
					return '&yacute;';
					
				case String.fromCharCode(254):
					return '&thorn;';
					
				case String.fromCharCode(255):
					return '&yuml;';
					
				case String.fromCharCode(34):
					if (isHTML) {
						return '"';
					}
					return '&quot;';
					
				case String.fromCharCode(39):
					if (isHTML) {
						return '\'';
					}
					return '&apos;';
					
				case String.fromCharCode(60):
					if (isHTML) {
						return '<';
					}
					return '&lt;';
					
				case String.fromCharCode(62):
					if (isHTML) {
						return '>';
					}
					return '&gt;';
					
				case String.fromCharCode(38):
					if (isHTML) {
						return '&';
					}
					return '&amp;';
					
				default:
					return match;
					
			}
		});
		return data;
	}
};

KSystem.included('HTMLEntities');
var HTML = {
	
	stripTags : function(data, tag) {
		if (!data) {
			return '';
		}
		var regex = null;
		if (tag) {
			regex = new RegExp("<" + tag + "([^>]*)>|<\/" + tag + "([^>]*)>", 'g');
		}
		else {
			regex = /<([^>]*)>|<\/([^>]*)>/g;
		}
		data = data.replace(regex, function(wholematch, match) {
			switch (match) {
				case 'p':
					return '\n';
					
				case 'br':
					return String.fromCharCode(161);
			}
			return "";
		});
		return data.replace(/^\s*/, "").replace(/\s*$/, "");
	}

};

KSystem.included('HTML');
if (!this.JSON) {
	
	JSON = function() {
		
		function f(n) { // Format integers to have at least two digits.
			return n < 10 ? '0' + n : n;
		}
		
		Date.prototype.toJSON = function() {
			
			// Eventually, this method will be based on the
			// date.toISOString method.
			
			return this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z';
		};
		
		var m = { // table of character substitutions
			'\b' : '\\b',
			'\t' : '\\t',
			'\n' : '\\n',
			'\f' : '\\f',
			'\r' : '\\r',
			'"' : '\\"',
			'\\' : '\\\\'
		};
		
		function stringify(value, whitelist) {
			var a, // The array holding the partial texts.
			i, // The loop counter.
			k = null, // The member key.
			l, // Length.
			r = /["\\\x00-\x1f\x7f-\x9f]/g, v; // The member
			// value.
			
			switch (typeof value) {
				case 'string':

					// If the string contains no control
					// characters, no quote characters, and
					// no
					// backslash characters, then we can
					// safely slap some quotes around it.
					// Otherwise we must also replace the
					// offending characters with safe
					// sequences.
					
					return r.test(value) ? '"' + value.replace(r, function(a1) {
						var c = m[a1];
						if (c) {
							return c;
						}
						c = a1.charCodeAt();
						return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
					}) + '"' : '"' + value + '"';
					
				case 'number':

					// JSON numbers must be finite. Encode
					// non-finite numbers as null.
					
					return isFinite(value) ? String(value) : 'null';
					
				case 'boolean':
				case 'null':
					return String(value);
					
				case 'object':

					// Due to a specification blunder in
					// ECMAScript,
					// typeof null is 'object', so watch out
					// for that case.
					
					if (!value) {
						return 'null';
					}
					
					// If the object has a toJSON method,
					// call it, and stringify the result.
					
					if (typeof value.toJSON === 'function') {
						return stringify(value.toJSON());
					}
					a = [];
					if (typeof value.length === 'number' && !(value.propertyIsEnumerable('length'))) {
						
						// The object is an array.
						// Stringify every element. Use
						// null as a placeholder
						// for non-JSON values.
						
						l = value.length;
						for (i = 0; i < l; i += 1) {
							a.push(stringify(value[i], whitelist) || 'null');
						}
						
						// Join all of the elements
						// together and wrap them in
						// brackets.
						
						return '[' + a.join(',') + ']';
					}
					if (whitelist) {
						
						// If a whitelist (array of
						// keys) is provided, use it to
						// select the components
						// of the object.
						
						l = whitelist.length;
						for (i = 0; i < l; i += 1) {
							k = whitelist[i];
							if (typeof k === 'string') {
								v = stringify(value[k], whitelist);
								if (v) {
									a.push(stringify(k) + ':' + v);
								}
							}
						}
					}
					else {
						
						// Otherwise, iterate through
						// all of the keys in the
						// object.
						
						for (k in value) {
							if (typeof k === 'string') {
								v = stringify(value[k], whitelist);
								if (v) {
									a.push(stringify(k) + ':' + v);
								}
							}
						}
					}
					
					// Join all of the member texts together
					// and wrap them in braces.
					
					return '{' + a.join(',') + '}';
			}
		}
		
		return {
			stringify : stringify,
			parse : function(text, filter) {
				var j;
				
				function walk(k, v) {
					var i = null, n;
					if (v && typeof v === 'object') {
						for (i in v) {
							if (Object.prototype.hasOwnProperty.apply(v, [
								i
							])) {
								n = walk(i, v[i]);
								if (n !== undefined) {
									v[i] = n;
								}
							}
						}
					}
					return filter(k, v);
				}
				
				// Parsing happens in three stages. In the first
				// stage, we run the text against
				// regular expressions that look for non-JSON
				// patterns. We are especially
				// concerned with '()' and 'new' because they
				// can cause invocation, and '='
				// because it can cause mutation. But just to be
				// safe, we want to reject all
				// unexpected forms.
				
				// We split the first stage into 4 regexp
				// operations in order to work around
				// crippling inefficiencies in IE's and Safari's
				// regexp engines. First we
				// replace all backslash pairs with '@' (a
				// non-JSON character). Second, we
				// replace all simple value tokens with ']'
				// characters. Third, we delete all
				// open brackets that follow a colon or comma or
				// that begin the text. Finally,
				// we look to see that the remaining characters
				// are only whitespace or ']' or
				// ',' or ':' or '{' or '}'. If that is so, then
				// the text is safe for eval.
				
				if (/^[\],:{}\s]*$/.test(text.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
					
					// In the second stage we use the eval
					// function to compile the text into a
					// JavaScript structure. The '{'
					// operator is subject to a syntactic
					// ambiguity
					// in JavaScript: it can begin a block
					// or an object literal. We wrap the
					// text
					// in parens to eliminate the ambiguity.
					
					j = eval('(' + text + ')');
					
					// In the optional third stage, we
					// recursively walk the new structure,
					// passing
					// each name/value pair to a filter
					// function for possible transformation.
					
					return typeof filter === 'function' ? walk('', j) : j;
				}
				
				// If the text is not JSON parseable, then a
				// SyntaxError is thrown.
				
				throw new SyntaxError('parseJSON');
			}
		};
	}();
	
}
KSystem.included('JSON2');

function KAbstractInput(parent, label, id) {
	if (/^widget([0-9]+)$/.test(id)) {
		throw Error('KAbstractInput: input id cannot be of the form "widget([0-9]+)".');
	}
	if (parent != null) {
		KWidget.call(this, parent);
		this.addCSSClass('KAbstractInput');
		
		this.labelText = label;
		this.inputID = id;
		this.validations = new Array();
		this.validationIndex = null;
		this.errorArea = null;
		this.noEncryption = false;
		this.inputListeners = new Array();
		this.onEnterSubmit = true;
		this.parentForm = null;
		
		this.input = window.document.createElement('input');
		{
			this.input.setAttribute('type', 'hidden');
			this.input.id = this.input.name = this.inputID + "Value";
		}
		this.panel.appendChild(this.input);
		
		this.innerLabel = window.document.createElement('div');
		{
			KCSS.setStyle({
				position : 'absolute',
				top : '0',
				left : '0'
			}, [
				this.innerLabel
			]);
		}
		
		this.inputContainer = null;
		this.addEventListener('blur', KAbstractInput.onBlur);
		this.addEventListener('blur', KAbstractInput.onBlur);
	}
}

KSystem.include([
	'SHA1',
	'HTMLEntities',
	'KWidget'
], function() {
	KAbstractInput.prototype = new KWidget();
	KAbstractInput.prototype.constructor = KAbstractInput;
	
	KAbstractInput.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			value = !!value ? value : '';
			if ((typeof value == 'object') || (value instanceof Array)) {
				value = JSON.stringify(value);
			}
			for ( var index in this.validations) {
				if (!!this.validations[index].operator && this.validations[index].options.comparison) {
					var result = true;
					if (this.validations[index].options.comparison instanceof KAbstractInput) {
						var cvalue = this.validations[index].options.comparison.getValue();
						if (!!cvalue) {
							eval('result = (value ' + this.validations[index].operator + ' cvalue)');
						}
					}
					else {
						eval('result = (value ' + this.validations[index].operator + ' this.validations[index].options.comparison)');
					}
					
					if (!result) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}					
				}
				else if (!this.validations[index].regex.test(value)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				
				if (!!this.errorArea) {
					this.errorArea.innerHTML = '';
				}
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KAbstractInput.prototype.addValidator = function(test, message, options) {
		if (typeof(test) == 'string' && (!!options && !!options.comparison)) {
			this.validations.push({
				operator : test,
				message : message,
				options : options
			});
		}
		else if (!(test instanceof RegExp)) {
			if (typeof test != 'string') {
				return;
			}
			test = new RegExp(test);
			this.validations.push({
				regex : test,
				message : message,
				options : options
			});
		}
		else {
			this.validations.push({
				regex : test,
				message : message,
				options : options
			});
		}
	};
	
	KAbstractInput.prototype.removeValidator = function(test) {
		for ( var index in this.validations) {
			if (this.validations[index].regex.toString() == test.toString()) {
				delete this.validations[index];
			}
		}
	};
	
	KAbstractInput.prototype.addEventListener = function(eventName, callback, target) {
		if (!target) {
			target = this.INPUT_ELEMENT;
		}
		if (!this.activated()) {
			this.inputListeners.push({
				event : eventName,
				callback : callback,
				target : target
			});
		}
		else {
			KWidget.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KAbstractInput.prototype.addEventListeners = function() {
		for ( var index in this.inputListeners) {
			KWidget.prototype.addEventListener.call(this, this.inputListeners[index].event, this.inputListeners[index].callback, this.inputListeners[index].target);
		}
	};
	
	KAbstractInput.prototype.focus = function(dispatcher) {
		var inputChildDiv = this.childDiv(this.INPUT_ELEMENT);
		this.parent.focus(this);
		if ((inputChildDiv.style.display != 'none') && (inputChildDiv.style.visibility != 'hidden') && !inputChildDiv.disabled && (inputChildDiv.type != 'hidden') && (KCSS.getComputedStyle(inputChildDiv).offsetWidth != 0)) {
			inputChildDiv.focus();
			//this.scrollIntoView();
			var v = inputChildDiv.value;
			inputChildDiv.value = '';
			inputChildDiv.value = v;
		}
	};
	
	KAbstractInput.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KAbstractInput.prototype.getInput = function() {
		return this.childDiv(this.INPUT_ELEMENT);
	};
	
	KAbstractInput.prototype.setHelp = function(text, baloon) {
		this.helpText = text;
		if (!baloon) {
			this.helpParams = {
				gravity : {
					x : 'left',
					y : 'top'
				},
				offsetX : 0,
				offsetY : 0
			};
		}
		else {
			this.helpParams = baloon;
		}
	};
	
	KAbstractInput.prototype.setLabel = function(text) {
		this.labelText = text;
		if (this.activated()) {
			var label = this.content.getElementsByTagName('label')[0];
			label.removeChild(label.childNodes[0]);
			if (label.childNodes.length != 0) {
				label.insertBefore(window.document.createTextNode(text), label.childNodes[label.childNodes.length - 1]);
			}
			else {
				label.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KAbstractInput.prototype.setInnerLabel = function(text) {
		this.innerLabel.innerHTML = text;
		this.innerLabel.className = 'InnerLabel';
		KDOM.addEventListener(this.innerLabel, 'click', KAbstractInput.onFocus);
	};
	
	KAbstractInput.prototype.getErrorMessage = function(index) {
		return this.validations[index].message;
	};
	
	KAbstractInput.prototype.showErrorMessage = function(params) {
		if (!params || !params.tooltip) {
			this.errorArea.innerHTML = '';
			if (params && params.isHtml) {
				this.errorArea.innerHTML += '* ' + (this.validationIndex ? this.getErrorMessage(this.validationIndex) : '');
			}
			else {
				this.errorArea.appendChild(window.document.createTextNode('* ' + (this.validationIndex ? this.getErrorMessage(this.validationIndex) : '')));
			}
		}
		else if (params && params.tooltip) {
			KDOM.addEventListener(this.childDiv(this.INPUT_ELEMENT), 'mouseover', KAbstractInput.onMouseOver);
		}
	};
	
	KAbstractInput.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(mouseEvent);
		
		KTooltip.showTooltip(widget.inputs[0], {
			text : '* ' + (widget.validationIndex ? widget.getErrorMessage(widget.validationIndex) : ''),
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			left : KDOM.mouseX(mouseEvent),
			top : (KDOM.mouseY(mouseEvent) + 15),
			cssClass : 'KAbstractInputTooltip'
		});
	};
	
	KAbstractInput.prototype.setTitle = function() {
	};
	
	KAbstractInput.prototype.go = function() {
		this.draw();
	};
	
	KAbstractInput.prototype.clear = function() {
		this.input.value = '';
	};
	
	KAbstractInput.prototype.createInput = function() {
		var input = window.document.createElement('input');
		{
			input.setAttribute('type', 'text');
			input.className = ' KAbstractInputElement ' + this.className + 'InputElement ';
			input.name = (!!this.inputID ? this.inputID : this.hash);
		}
		return input;
	};
	
	KAbstractInput.prototype.createLabel = function() {
		var label = window.document.createElement('label');
		{
			label.className = ' KAbstractInputLabelElement ' + this.className + 'LabelElement ';
			label.setAttribute('for', this.inputID);
			label.innerHTML = this.labelText;
		}
		return label;
	};
	
	KAbstractInput.prototype.createHelp = function() {
		if (!!this.helpText) {
			var help = window.document.createElement('div');
			{
				help.className = ' KAbstractInputHelpElement ' + this.className + 'HelpElement ';
				var params = {
					text : this.helpText,
					delay : 300,
					gravity : this.helpParams.gravity,
					max : this.helpParams.max,
					offsetX : this.helpParams.offsetX,
					offsetY : this.helpParams.offsetY,
					zoom : this.helpParams.zoom,
					offsetParent: help,
					target : help,
					cssClass : !!this.helpParams.cssClass ? this.helpParams.cssClass : 'KInputBaloon '
				};
				KBaloon.make(this, params);
			}
			return help;
		}
		return null;
	};
	
	KAbstractInput.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		this.inputContainer = this.inputContainer || window.document.createElement('div');

		var label = this.createLabel();
		var input = this.createInput();
		var help = this.createHelp();
		
		this.inputContainer.appendChild(input);
		this.inputContainer.appendChild(this.innerLabel);
		
		this.content.appendChild(label);
		this.content.appendChild(this.inputContainer);
		if (!!help) {
			label.appendChild(help);
		}
		
		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea ' + this.className + 'ErrorArea';
		this.content.appendChild(this.errorArea);
		
		if (this.onEnterSubmit) {
			this.addEventListener('keyup', KAbstractInput.onEnterPressed, this.childDiv(this.INPUT_ELEMENT));
		}
		this.addEventListener('keydown', KAbstractInput.onTabPressed, this.childDiv(this.INPUT_ELEMENT));
		
		this.addEventListeners();
		this.activate();
	};
	
	KAbstractInput.prototype.selectNextWidget = function(up) {
		var index = this.parent.indexOf(this);
		var next = undefined;

		if (up) {
			if (index == 0) {
				next = this.parent.childAt(this.parent.nChildren - 1);
			}
			else {
				next = this.parent.childAt(index - 1);
			}
		}
		else {
			if (index < this.parent.nChildren - 1) {
				next = this.parent.childAt(index + 1);
			}
			else {
				next = this.parent.childAt(0);
			}
		}

		if (next instanceof KFile) {
			return next.selectNextWidget(up);
		}
		if (next instanceof KHidden) {
			return next.selectNextWidget(up);
		}
		return next;
	};
	
	KAbstractInput.prototype.actOnEnter = function() {
		return true;
	};
	
	KAbstractInput.onEnterPressed = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (widget.actOnEnter() && KSystem.isIncluded('KForm') && widget.parent instanceof KForm) {
			KDOM.stopEvent(event);
			if (KForm.onEnterPressed(event)) {
				KForm.goSubmit(event);
			}
		}
	};
	
	KAbstractInput.onTabPressed = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		if (keyCode == 9) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event);
			var next = widget.selectNextWidget(event.shiftKey);
			if (!!next) {
				next.focus();
			}
			return;
		}
	};

	KAbstractInput.onFocus = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		mouseEvent.returnValue = false;

		if (KSystem.isIncluded('KScroll') && KScroll.activeScroll) {
			KScroll.disableKeys();
		}

		var widget = KDOM.getEventWidget(event);
		if (widget.innerLabel.innerHTML != '') {
			widget.innerLabel.style.display = 'none';
			widget.focus();
		}
	};
	
	KAbstractInput.onBlur = function(event) {
		if (KSystem.isIncluded('KScroll') && KScroll.activeScroll) {
			KScroll.enableKeys();
		}
		var widget = KDOM.getEventWidget(event);
		if ((widget.innerLabel.innerHTML != '') && (widget.getValue() == '')) {
			widget.innerLabel.style.display = 'block';
		}
	};
	
	KAbstractInput.prototype.getValue = function() {
		if (this.input.value != '') {
			return JSON.parse(this.input.value);
		}
		else {
			return '';
		}
	};
	
	KAbstractInput.prototype.setValue = function(value) {
		if (value === undefined || value === null) {
			this.input.value = '';
		}
		else {
			this.input.value = JSON.stringify(value);
		}
	};
	
	KAbstractInput.prototype.INPUT_ELEMENT = 'input';
	KAbstractInput.LABEL_CONTAINER = 'label';
	KAbstractInput.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KAbstractInput');
});
function KAddOn(parent) {
	if (!!parent) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.shell = this.parent.shell;
		this.kinky = Kinky.bunnyMan;
		this.id = Kinky.bunnyMan.addWidget(this, this.prefix);
		this.hash = '/' + this.id;
		this.parent.addons[this.id] = this;
		
		if (Kinky.bunnyMan.currentState.length != 1) {
			for ( var i in Kinky.bunnyMan.currentState) {
				this.dispatchState(Kinky.bunnyMan.currentState[i]);
			}
		}
	}
}
{
	KAddOn.prototype = new Object();
	KAddOn.prototype.constructor = KAddOn;
	KAddOn.prototype.prefix = 'addon';
	
	KAddOn.prototype.dispatchState = function(stateName) {
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
	};
	
	KAddOn.prototype.undispatchState = function(stateName) {
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!KAddOn.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = KAddOn.prototype.default_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
	};
	
	KAddOn.prototype.getShell = function() {
		return Kinky.shell[this.shell];
	};
	
	KAddOn.prototype.unload = function() {
		this.destroy();
	};
	
	KAddOn.prototype.destroy = function() {
		var id = this.id;
		
		for ( var att in this) {
			if (this[att]) {
				delete this[att];
			}
		}
		
		Kinky.bunnyMan.removeWidget(id);
	};
	
	KAddOn.prototype.visible = function() {
		return;
	};
	
	KAddOn.prototype.getConnection = function(type) {
		return KConnectionsPool.getConnection(type, this.shell);
	};
	
	KAddOn.prototype.activated = function() {
		return true;
	};
	
	KAddOn.prototype.setStyle = function(cssStyle) {
		KWidget.prototype.setStyle.call(this, cssStyle, this.panel);
	};
	
	KAddOn.prototype.go = function() {
		if (!!this.config) {
			KCache.commit(this.ref + '/config', this.config);
		}
		else {
			this.config = KCache.restore(this.ref + '/config');
		}
		
		this.draw();
	};
	
	KAddOn.prototype.draw = function() {
	};
	
	KAddOn.STATE_FUNCTIONS = [
		"go",
		"load",
		"draw",
		"focus",
		"blur",
		"enable",
		"disable",
		"visible",
		"invisible",
		"abort",
		"onLoad",
		"onSave",
		"onRemove",
		"onPost",
		"onPut",
		"onError",
		"onOk",
		"onHead",
		"onCreated",
		"onAccepted",
		"onNoContent",
		"onBadRequest",
		"onUnauthorized",
		"onForbidden",
		"onNotFound",
		"onMethodNotAllowed",
		"onPreConditionFailed",
		"onInternalError"
	];
	
	KSystem.included('KAddOn');
}

function KAutoCompletion(parent, label, id) {
	if (parent) {
		KCombo.call(this, parent, label, id);
		this.resultsArrived = true;
		this.changeCallback = KAutoCompletion.dropResults;
		this.search_changed = false;

		this.combo.style.cursor = 'text';
		KDOM.removeEventListener(this.combo, 'click', KCombo.toggleOptions);
		this.comboText.setAttribute('contenteditable', 'true');

		this.addActionListener(KAutoCompletion.actions, [
			'/no-content'
		]);
	}
}

KSystem.include([
	'KCombo'
], function() {
	KAutoCompletion.prototype = new KCombo();
	KAutoCompletion.prototype.constructor = KAutoCompletion;
	
	KAutoCompletion.prototype.focus = function(dispatcher) {
		KCSS.addCSSClass('KComboFocused', this.combo);
		this.comboText.focus();
	};	

	KAutoCompletion.prototype.setSearchParams = function(params) {
		this.searchParams = params;
	};
	
	KAutoCompletion.prototype.releaseSearchInput = function() {
		this.combo.contenteditable = true;
		this.resultsArrived = true;
	};
	
	KAutoCompletion.prototype.blockSearchInput = function() {
		this.combo.contenteditable = false;
		this.resultsArrived = false;
	};
	
	KAutoCompletion.prototype.draw = function() {
		this.addCSSClass('KCombo');
		this.addCSSClass('KComboContent', this.content);

		if (!!this.searchParams) {
			KDOM.removeEventListener(this.dropButton, 'click', KCombo.toggleOptions);
			KDOM.addEventListener(this.dropButton, 'click', KAutoCompletion.sendSearch);
		}
		KDOM.addEventListener(this.comboText, 'focus', KAutoCompletion.focusSearch);
		KCombo.prototype.draw.call(this);
	};

	KAutoCompletion.prototype.actOnEnter = function() {
		if (!this.getValue()) {
			return false;
		}
		return true;
	};	

	KAutoCompletion.prototype.onKeyPressed = function(event) {
		var keyCode = event.keyCode || event.which;
		switch (keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				this.releaseSearchInput();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				this.releaseSearchInput();
				break;
			}
			case 13: {
				if (!!this.getValue() && this.optionsToggled) {
					break;
				}
				KDOM.stopEvent(event);
				if (this.comboText.textContent != '') {
					this.blockSearchInput();
					this.changeCallback(this, this.comboText.textContent);
					if (!this.searchParams && this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
				}
				break;
			}
			case 18: 
			case 17: 
			case 16: {
				break;
			}
			case 9: {
				this.blur();
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.selectedOption.value = '';
				this.combo.value = '';
				this.releaseSearchInput();
				this.combo.blur();
				if (!!this.optionsToggled) {
					KCombo.toggleOptions(null, this);					
				}
				break;
			}
			default: {
				if (!this.onEnterPressed && !this.searchParams) {
					var self = this;
					if (this.changeCallbackTimer !== undefined) {
						KSystem.removeTimer(this.changeCallbackTimer);
					}
					this.changeCallbackTimer = KSystem.addTimer(function() {
						self.changeCallbackTimer = undefined;
						self.blockSearchInput();
						self.changeCallback(self, self.comboText.textContent);
					}, 100);
				}
			}
		}
	};
	
	KAutoCompletion.prototype.searchServer = function(search) {
		if (!!this.searchParams) {
			this.kinky.get(this, 'rest:' + this.searchParams.service + '?' + this.searchParams.field + '=m/' + search + '/i', {}, null, 'onChange');
		}
		else {
			if (!this.optionsToggled) {
				KCombo.toggleOptions(null, this);
			}
			this.filter(search);
		}
	};
	
	KAutoCompletion.prototype.onChange = function(data, request) {
		this.optionsContent.innerHTML = '';
		
		for ( var index in data.elements) {
			var value = null;
			var caption = null;
			var first = true;

			eval('value = data.elements[index].' + this.searchParams.value_field + ';');
			if (!!this.searchParams.icon_field) {
				eval('caption = \'<img style="display: inline-block; vertical-align: middle; width: auto; height: ' + (this.comboText.offsetHeight) +  'px" src="\' + data.elements[index].' + this.searchParams.icon_field + ' + \'"></img> \' + data.elements[index].' + this.searchParams.label_field + ';');
			}
			else {
				eval('caption = data.elements[index].' + this.searchParams.label_field + ';');
			}
			this.addOption(value, caption, false);
		}
		this.releaseSearchInput();
		if (!this.optionsToggled) {
			KCombo.toggleOptions(null, this);
		}
	};

	KAutoCompletion.focusSearch = function(event) {
		var widget = KDOM.getEventWidget(event);
		KDOM.moveToEndOfContenteditable(widget.comboText);
		KCSS.addCSSClass('KComboFocused', widget.combo);
	};	

	KAutoCompletion.sendSearch = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (widget.comboText.textContent != '') {
			widget.blockSearchInput();
			widget.changeCallback(widget, widget.comboText.textContent);
		}
	};
	
	KAutoCompletion.dropResults = function(widget, search) {
		widget.searchServer(search);
	};
		
	KAutoCompletion.actions = function(widget, action) {
		widget.releaseSearchInput();
		widget.optionsContent.innerHTML = '';
		widget.options.splice(1, widget.options.length - 1);
		widget.setValue('');
	};
	
	KSystem.included('KAutoCompletion');
});
function KBaloon(parent, params) {
	if (!!parent) {
		this.prefix = 'baloon';
		KAddOn.call(this, parent);
		this.hash = '/kbaloon';
		this.params = params;
		if (!this.params.gravity) {
			this.params.gravity = {
				x : 'left',
				y : 'top'
			};
		}
		this.parentRootDiv = this.parent.childDiv(!!this.params.target ? this.params.target : KWidget.ROOT_DIV);
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KBaloon ' + (!!params.cssClass ? params.cssClass : this.parent.className + 'Baloon') + ' ';
		this.panel.innerHTML = '<p>' + this.params.text + '</p>';
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;
		
		parent.baloon = this;

		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KBaloon.prototype = new KAddOn();
	KBaloon.prototype.constructor = KBaloon;
	
	KBaloon.prototype.destroy = function() {
		this.hide();
	};

	KBaloon.prototype.blur = function() {
		this.zoomed = false;
		this.getShell().hideOverlay();

		this.hide();

		this.panel.style.position = 'absolute';
		this.panel.style.top = 'auto';
		this.panel.style.left = 'auto';
		this.panel.style.maxWidth = 'auto';
		this.panel.style.maxHeight = 'auto';
		this.panel.style.opacity = '0';

	};
	
	KBaloon.prototype.go = function() {		
		this.params.timer = -1;
		KDOM.addEventListener(this.parentRootDiv, 'mouseover', KBaloon.over);
		KDOM.addEventListener(this.parentRootDiv, 'mouseout', KBaloon.out);
		if (!!this.params.zoom) {
			KDOM.addEventListener(this.parentRootDiv, 'click', KBaloon.click);
		}
		this.parentRootDiv.appendChild(this.panel);
	};

	KBaloon.over = function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		var offsetX = (!!widget.params.offsetX ? widget.params.offsetX : 0);
		var offsetY = (!!widget.params.offsetY ? widget.params.offsetY : 0);

		if (!!widget.params.delay) {
			widget.params.timer = KSystem.addTimer(function() {
				widget.params.timer = -1;
				KBaloon.show(widget, offsetX, offsetY, widget.params.gravity, KDOM.getMouseCoords(event));
			}, widget.params.delay);
			return;
		}
		KBaloon.show(widget, offsetX, offsetY, null, KDOM.getMouseCoords(event));
	};

	KBaloon.out =  function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		if (!widget || !!widget.zoomed) {
			return;
		}

		if (widget.params.timer != -1) {
			KSystem.removeTimer(widget.params.timer);
			widget.params.timer = -1;
			return;
		}
		widget.transition_hide();
	};

	KBaloon.click =  function(event) {
		var widget = KDOM.getEventWidget(event).baloon;
		if (!widget) {
			return;
		}
		widget.zoomed = true;
		widget.getShell().showOverlay();
		
		widget.panel.style.position = 'fixed';
		widget.panel.style.maxHeight = !!widget.params.zoom.height ? widget.params.zoom.height + 'px' : (KDOM.getBrowserHeight() - 100) + 'px';
		widget.panel.style.maxWidth = !!widget.params.zoom.width ? widget.params.zoom.width + 'px' : '980px';

		KSystem.addTimer(function() {
			widget.panel.style.top = Math.round((KDOM.getBrowserHeight() - (!!widget.params.zoom.height ? widget.params.zoom.height : widget.panel.offsetHeight)) / 2) + 'px';
			widget.panel.style.left = Math.round((KDOM.getBrowserWidth() - (!!widget.params.zoom.width ? widget.params.zoom.width : widget.panel.offsetWidth)) / 2) + 'px';
			Kinky.setModal(widget);
		}, 100);

	};
	
	KBaloon.unmake = function(baloon) {
		baloon.parentRootDiv.removeChild(baloon.panel);
		KDOM.removeEventListener(baloon.parentRootDiv, 'mouseover', KBaloon.over);
		KDOM.removeEventListener(baloon.parentRootDiv, 'mouseout', KBaloon.out);
		KAddOn.prototype.unload.call(baloon);
	};
	
	KBaloon.make = function(target, params) {
		var tooltip = new KBaloon(target, params);
		return tooltip;
	};
	
	KBaloon.show = function(widget, offsetX, offsetY, gravity, pos) {
		widget.panel.style.position = 'absolute';
		widget.panel.style.display = 'block';
		window.document.body.appendChild(widget.panel);
		
		var MARGIN_W = 60;
		var MARGIN_H = 60;
		var parentOffsetX = 0;
		var parentOffsetY = 0;
		var reference = (!!widget.params && !!widget.params.offsetParent ? widget.params.offsetParent : widget.parent.panel);

		if (!!gravity) {
			if (gravity.x == 'right') {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (pos.x - MARGIN_W) ? widget.params.max.width : (pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + KDOM.getWidth(reference) - widget.panel.offsetWidth - offsetX) + 'px';
			}
			else if (gravity.x == 'center') {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (pos.x - MARGIN_W) ? widget.params.max.width : (pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + KDOM.getWidth(reference) - Math.round(widget.panel.offsetWidth / 2) - offsetX) + 'px';
			}
			else {
				widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (KDOM.getBrowserWidth() - pos.x - MARGIN_W) ? widget.params.max.width : (KDOM.getBrowserWidth() - pos.x - MARGIN_W)) + 'px';
				parentOffsetX = KDOM.offsetLeft(reference);
				widget.panel.style.left = (parentOffsetX + offsetX) + 'px';
			}
			if (gravity.y == 'bottom') {
				widget.panel.style.maxHeight = (pos.y - MARGIN_H) + 'px';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = (parentOffsetY + KDOM.getHeight(reference) - widget.panel.offsetHeight - offsetY - KDOM.scrollTop(reference)) + 'px';
			}
			else if (gravity.y == 'middle') {
				widget.panel.style.maxHeight = 'auto';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = Math.min(KDOM.getBrowserHeight() - Math.round(widget.panel.offsetHeight) - offsetY, Math.max(0, parentOffsetY + KDOM.getHeight(reference) - Math.round(widget.panel.offsetHeight / 2) - offsetY - KDOM.scrollTop(reference))) + 'px';
			}
			else {
				widget.panel.style.maxHeight = (KDOM.getBrowserHeight() - pos.y - MARGIN_H) + 'px';
				parentOffsetY = KDOM.offsetTop(reference);
				widget.panel.style.top = (parentOffsetY + offsetY - KDOM.scrollTop(reference)) + 'px';
			}
		}
		else {
			widget.panel.style.maxWidth = (!!widget.params && !!widget.params.max && !!widget.params.max.width && widget.params.max.width < (KDOM.getBrowserWidth() - pos.x - MARGIN_W) ? widget.params.max.width : (KDOM.getBrowserWidth() - pos.x - MARGIN_W)) + 'px';
			widget.panel.style.maxHeight = (KDOM.getBrowserHeight() - pos.y - MARGIN_H) + 'px';
			parentOffsetX = KDOM.offsetLeft(reference);
			widget.panel.style.left = (parentOffsetX + offsetX) + 'px';
			parentOffsetY = KDOM.offsetTop(reference);
			widget.panel.style.top = (parentOffsetY + offsetY - KDOM.scrollTop(reference)) + 'px';
		}

		widget.transition_show();
	};
	
	KBaloon.prototype.hide = function() {
		this.parentRootDiv.appendChild(this.panel);
		this.panel.style.display = 'none';
		this.panel.style.position = 'relative';
		if (!!this.animating) {
			this.panel.childNodes[0].style.marginTop = '0px';
			KEffects.cancelEffect(this.animating.id);
			delete this.animating;
			this.animating = undefined;
		}
	};
	
	KBaloon.prototype.transition_show = function() {
		var _this = this;
		var defaultEnterTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			},
		};
		KEffects.addEffect(this, (!!this.params.enterTween ? this.params.enterTween : defaultEnterTween));
	};
	
	KBaloon.prototype.transition_hide = function() {
		var widget = this;
		var defaultExitTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 0
			},
			from : {
				alpha : 1
			},
			onComplete : function() {
				// caso seja subcarregado este metodo NAO ESQUECER
				widget.hide();
			}
		};
		KEffects.addEffect(this, (!!this.params.exitTween ? this.params.exitTween : defaultExitTween));
	};
	
	KSystem.registerAddOn('KBaloon');
	KSystem.included('KBaloon');
});

function KBreadcrumb() {
	this.query = null;
	this.action = null;
	this.hash = null;
	this.url = null;
	this.last = null;
	this.history = new Array();
	this.redirect = new Object();
	this.byWidget = {
		location : {},
		query : {},
		action : {}
	};
	
	this.listeners = {
		location : {},
		query : {},
		action : {}
	};
}

{
	KBreadcrumb.prototype.constructor = KBreadcrumb;
	
	KBreadcrumb.addLocationListener = function(widget, callback, hashes) {
		if (!KBreadcrumb.sparrow.byWidget.location[widget.id]) {
			KBreadcrumb.sparrow.byWidget.location[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!hashes) {
			hashes = [
				'*'
			];
		}
		
		for ( var hash in hashes) {
			var all = new RegExp('^' + hashes[hash].replace(/\*/g, '(.*)'));
			
			if (KBreadcrumb.sparrow.listeners.location[hashes[hash]] == null) {
				KBreadcrumb.sparrow.listeners.location[hashes[hash]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.location[hashes[hash]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getHash())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					hash : KBreadcrumb.getHash()
				});
			}
			
			if (!KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]]) {
				KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.location[widget.id][hashes[hash]].listeners.push(listener);
		}
	};
	
	KBreadcrumb.addQueryListener = function(widget, callback, queries) {
		if (!KBreadcrumb.sparrow.byWidget.query[widget.id]) {
			KBreadcrumb.sparrow.byWidget.query[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!queries) {
			queries = [
				'*'
			];
		}
		
		for ( var query in queries) {
			var all = new RegExp('^' + queries[query].replace(/\*/g, '(.*)'));
			if (KBreadcrumb.sparrow.listeners.query[queries[query]] == null) {
				KBreadcrumb.sparrow.listeners.query[queries[query]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.query[queries[query]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getQuery())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					query : KBreadcrumb.getQuery()
				});
			}
			
			if (!KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]]) {
				KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.query[widget.id][queries[query]].listeners.push(listener);
		}
		
	};
	
	KBreadcrumb.addActionListener = function(widget, callback, actions) {
		if (!KBreadcrumb.sparrow.byWidget.action[widget.id]) {
			KBreadcrumb.sparrow.byWidget.action[widget.id] = {};
		}
		
		var listener = {
			widget : widget,
			callback : callback
		};
		
		if (!actions) {
			actions = [
				'*'
			];
		}
		
		for ( var action in actions) {
			var all = new RegExp('^' + actions[action].replace(/\*/g, '(.*)'));
			if (KBreadcrumb.sparrow.listeners.action[actions[action]] == null) {
				KBreadcrumb.sparrow.listeners.action[actions[action]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.listeners.action[actions[action]].listeners.push(listener);
			if (widget.activated() && all.test(KBreadcrumb.getAction())) {
				KBreadcrumb.dispatchEvent(widget.id, {
					action : KBreadcrumb.getAction()
				});
			}
			if (!KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]]) {
				KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]] = {
					regex : all,
					listeners : new Array()
				};
			}
			KBreadcrumb.sparrow.byWidget.action[widget.id][actions[action]].listeners.push(listener);
		}
	};
	
	KBreadcrumb.back = function(params) {
		
		var nTimes = 1;
		if (params && params.nTimes) {
			nTimes = params.nTimes;
		}
		
		KBreadcrumb.sparrow.history.pop();
		var url = null;
		for ( var time = 0; time != nTimes; time++) {
			
			url = KBreadcrumb.sparrow.history.pop();
			if (url == null) {
				return;
			}
			url = '#' + url.url;
			url = KBreadcrumb.processURL(url);
			params = params || {};
			params.url = params.url;
			params.query = params.query || url.query || '';
			params.action = params.action || url.action || '';
			params.hash = url.hash;
			
		}
		KBreadcrumb.dispatchURL(params);
	};
	
	KBreadcrumb.clearHistory = function(leave) {
		var toLeave = [];
		
		for ( var i = 0; i != leave; i++) {
			var url = KBreadcrumb.sparrow.history.pop();
			toLeave.push(url);
		}
		
		delete KBreadcrumb.sparrow.history;
		KBreadcrumb.sparrow.history = toLeave;
	};
	
	KBreadcrumb.popHistory = function(nTimes) {
		if (nTimes == null) {
			nTimes = 1;
		}
		var toReturn = {};
		
		for ( var i = 0; i != nTimes; i++) {
			var url = KBreadcrumb.sparrow.history.pop();
			toReturn['#' + url.url] = url.title;
		}
		
		return toReturn;
	};
	
	KBreadcrumb.historyLen = function() {
		return KBreadcrumb.sparrow.history.length;
	};
	
	KBreadcrumb.getHistory = function(offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		return '#' + url.url;
	};
	
	KBreadcrumb.getHistoryTitle = function(offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		return url.title;
	};
	
	KBreadcrumb.setHistoryTitle = function(title, offset) {
		var url = KBreadcrumb.sparrow.history[KBreadcrumb.sparrow.history.length - (offset || 0) - 1];
		if (url == null) {
			return;
		}
		
		url.title = title;
	};
	
	KBreadcrumb.getLink = function(params) {
		if (params.url) {
			return '#' + params.url;
		}
		
		if (params.hash && KBreadcrumb.sparrow.redirect[params.hash]) {
			params.hash = KBreadcrumb.sparrow.redirect[params.hash];
		}
		
		var link = '#' + (params.hash != null ? params.hash : (params.hash == '' ? '' : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.hash != null) ? KBreadcrumb.sparrow.hash : '')));
		link += (params.query != null ? (params.query == '' ? '' : '/?' + params.query) : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.query != null) ? '/?' + KBreadcrumb.sparrow.query : ''));
		link += (params.action != null ? (params.action == '' ? '' : '/@' + params.action) : (KBreadcrumb.sparrow && (KBreadcrumb.sparrow.action != null) ? '/@' + KBreadcrumb.sparrow.action : ''));
		return link;
	};
	
	KBreadcrumb.dispatchURL = function(params) {
		KBreadcrumb.sparrow.last = window.document.location.href.split('#')[1];
		window.document.location = window.document.location.href.split('#')[0] + KBreadcrumb.getLink(params);
	};
	
	KBreadcrumb.dispatchEvent = function(widgetID, params) {
		if (!!widgetID && !params) {
			if (!!KBreadcrumb.drilldown) {
				try {
					KBreadcrumb.sendRegex('query', KBreadcrumb.drilldown, widgetID);
				}
				catch (e) {
					throw new KException(KBreadcrumb.sparrow, e.message);
				}
			}
		}
		if (!params) {
			params = {
				hash : KBreadcrumb.getHash(),
				query : KBreadcrumb.getQuery(),
				action : KBreadcrumb.getAction()
			};
		}
		if (params.hash && KBreadcrumb.sparrow.redirect[params.hash]) {
			params.hash = KBreadcrumb.sparrow.redirect[params.hash];
		}
		
		if (params.hash != null) {
			try {
				KBreadcrumb.sendRegex('location', params.hash, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (params.query != null) {
			try {
				KBreadcrumb.sendRegex('query', params.query, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (params.action != null) {
			try {
				KBreadcrumb.sendRegex('action', params.action, widgetID);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
	};
	
	KBreadcrumb.processURL = function(current) {
		if (!current) {
			current = window.document.location.href.split('#');
		}
		else {
			current = current.split('#');
		}
		
		var breadcrumb = new Object();
		
		if (current.length > 1) {
			breadcrumb.url = current[1];
			
			var hash = breadcrumb.url.split('/?');
			if (hash.length > 1) {
				breadcrumb.hash = hash[0];
				breadcrumb.query = hash[1];
				
				var actions = hash[1].split('/@');
				if (actions.length > 1) {
					breadcrumb.query = actions[0];
					breadcrumb.action = actions[1];
				}
			}
			else {
				breadcrumb.hash = breadcrumb.url;
				var actions = breadcrumb.hash.split('/@');
				if (actions.length > 1) {
					breadcrumb.hash = actions[0];
					breadcrumb.action = actions[1];
				}
			}
		}
		
		return breadcrumb;
	};
	
	KBreadcrumb.sendRegex = function(list, hash, widgetID) {
		
		if (widgetID) {
			if (!KBreadcrumb.sparrow.byWidget[list][widgetID]) {
				return;
			}
			var listenersList = KBreadcrumb.sparrow.byWidget[list][widgetID];
			for ( var pattern in listenersList) {
				if (listenersList[pattern].regex.test(hash)) {
					for ( var listener in listenersList[pattern].listeners) {
						if (!listenersList[pattern].listeners[listener].widget || !Kinky.getWidget(listenersList[pattern].listeners[listener].widget.id)) {
							delete listenersList[pattern].listeners[listener];
							continue;
						}
						try {
							KSystem.addTimer('if (!!KBreadcrumb.sparrow.byWidget.' + list + '[\'' + widgetID + '\']) KBreadcrumb.sendEvent(KBreadcrumb.sparrow.byWidget.' + list + '[\'' + widgetID + '\'][\'' + pattern + '\'].listeners[' + listener + '], \'' + hash + '\')', 1);
						}
						catch (e) {
						}
					}
				}
			}
		}
		else {
			var listenersList = KBreadcrumb.sparrow.listeners[list];
			for ( var pattern in listenersList) {
				if (listenersList[pattern].regex.test(hash)) {
					for ( var listener in listenersList[pattern].listeners) {
						if (!listenersList[pattern].listeners[listener].widget || !Kinky.getWidget(listenersList[pattern].listeners[listener].widget.id)) {
							delete listenersList[pattern].listeners[listener];
							continue;
						}
						try {
							KSystem.addTimer('if (!!KBreadcrumb.sparrow.listeners.' + list + '[\'' + pattern + '\']) KBreadcrumb.sendEvent(KBreadcrumb.sparrow.listeners.' + list + '[\'' + pattern + '\'].listeners[' + listener + '], \'' + hash + '\')', 1);
						}
						catch (e) {
						}
					}
				}
			}
		}
	};
	
	KBreadcrumb.sendEvent = function(listener, event) {
		try {
			listener.widget.fromListener = true;
			listener.callback(listener.widget, event);
		}
		catch (e) {
			throw e;
		}
	};
	
	KBreadcrumb.getHash = function() {
		return KBreadcrumb.sparrow.hash;
	};
	
	KBreadcrumb.getQuery = function() {
		return KBreadcrumb.sparrow.query;
	};
	
	KBreadcrumb.getAction = function() {
		return KBreadcrumb.sparrow.action;
	};
	
	KBreadcrumb.getURL = function() {
		return KBreadcrumb.sparrow.url;
	};
	
	KBreadcrumb.onLocationChange = function() {
		KBreadcrumb.sparrow.processing = true;
		
		var breadcrumb = KBreadcrumb.processURL();
		if (KBreadcrumb.sparrow.url != breadcrumb.url) {
			KBreadcrumb.sparrow.history.push({
				title : null,
				url : breadcrumb.url
			});
			
			KBreadcrumb.sparrow.url = breadcrumb.url;
		}
		else {
			KBreadcrumb.sparrow.last = KBreadcrumb.sparrow.url;
		}
		
		if (breadcrumb.hash != KBreadcrumb.sparrow.hash) {
			KBreadcrumb.sparrow.hash = breadcrumb.hash;
			try {
				KBreadcrumb.sendRegex('location', breadcrumb.hash);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (breadcrumb.query != KBreadcrumb.sparrow.query) {
			KBreadcrumb.sparrow.query = breadcrumb.query;
			try {
				KBreadcrumb.sendRegex('query', breadcrumb.query);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		if (breadcrumb.action != KBreadcrumb.sparrow.action) {
			KBreadcrumb.sparrow.action = breadcrumb.action;
			try {
				KBreadcrumb.sendRegex('action', breadcrumb.action);
			}
			catch (e) {
				throw new KException(KBreadcrumb.sparrow, e.message);
			}
		}
		KBreadcrumb.sparrow.processing = false;
		KSystem.addTimer(KBreadcrumb.onLocationChange, 80);
	};
	
	KBreadcrumb.sparrow = null;
	KBreadcrumb.DISPATCH_URL = 'url';
	KBreadcrumb.DISPATCH_EVENT = 'event';
	
	KSystem.included('KBreadcrumb');
}
var KBrowserDetect = {
	init : function() {
		this.browsername = this.searchString(this.dataBrowser);
		this.browsername = (!!this.browsername ? this.browsername : "An unknown browser");
		this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
		this.OS = this.searchString(this.dataOS);
		this.OS = (!!this.OS ? this.OS : "an unknown OS");
		
		if (((this.browsername.indexOf("Firefox") != -1) || (this.browsername.indexOf("Iceweasel") != -1) || (this.browsername.indexOf("Mozilla") != -1)) && (parseInt(navigator.appVersion) >= 5)) {
			this.browser = 1;
		}
		else if ((this.browsername.indexOf("Explorer") != -1) && (parseInt(navigator.appVersion) >= 4)) {
			this.browser = 2;
		}
		else if (this.browsername.indexOf("Opera") != -1) {
			this.browser = 3;
		}
		else if (this.browsername.indexOf("Safari") != -1) {
			this.browser = 4;
		}
		else if (this.browsername.indexOf("Chrome") != -1) {
			this.browser = 5;
		}
	},
	searchString : function(data) {
		for ( var i = 0; i < data.length; i++) {
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1) {
					return data[i].identity;
				}
			}
			else if (dataProp) {
				return data[i].identity;
			}
		}
	},
	searchVersion : function(dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) {
			return null;
		}
		return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	},
	dataBrowser : [
		{
			string : navigator.userAgent,
			subString : "Chrome",
			identity : "Chrome"
		},
		{
			string : navigator.userAgent,
			subString : "OmniWeb",
			versionSearch : "OmniWeb/",
			identity : "OmniWeb"
		},
		{
			string : navigator.vendor,
			subString : "Apple",
			identity : "Safari",
			versionSearch : "Version"
		},
		{
			prop : window.opera,
			identity : "Opera"
		},
		{
			string : navigator.vendor,
			subString : "iCab",
			identity : "iCab"
		},
		{
			string : navigator.vendor,
			subString : "KDE",
			identity : "Konqueror"
		},
		{
			string : navigator.userAgent,
			subString : "Firefox",
			identity : "Firefox"
		},
		{
			string : navigator.vendor,
			subString : "Camino",
			identity : "Camino"
		},
		{ // for newer Netscapes (6+)
			string : navigator.userAgent,
			subString : "Netscape",
			identity : "Netscape"
		},
		{
			string : navigator.userAgent,
			subString : "MSIE",
			identity : "Explorer",
			versionSearch : "MSIE"
		},
		{
			string : navigator.userAgent,
			subString : "Gecko",
			identity : "Mozilla",
			versionSearch : "rv"
		},
		{ // for older Netscapes (4-)
			string : navigator.userAgent,
			subString : "Mozilla",
			identity : "Netscape",
			versionSearch : "Mozilla"
		}
	],
	dataOS : [
		{
			string : navigator.platform,
			subString : "Win",
			identity : "Windows"
		},
		{
			string : navigator.platform,
			subString : "Mac",
			identity : "Mac"
		},
		{
			string : navigator.platform,
			subString : "Linux",
			identity : "Linux"
		}
	]

};

KBrowserDetect.init();
KSystem.included('KBrowserDetect');

function KBubble(parent, params) {
	if (!!parent) {
		this.prefix = 'bubble';
		KAddOn.call(this, parent);
		this.params = params;
		if (!this.params.gravity) {
			this.params.gravity = {
				x : 'left',
				y : 'top'
			};
		}
		this.parentRootDiv = this.parent.childDiv(!!this.params.target ? this.params.target : KWidget.ROOT_DIV);
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KBubble ' + (!!params.cssClass ? params.cssClass : this.parent.className + 'Bubble') + ' ';
		this.panel.innerHTML = this.params.text;
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;
		
		parent.baloon = this;

		if (!!this.params.callback) {
			this.panel.style.cursor = 'pointer';
			KDOM.addEventListener(this.panel, 'click', this.params.callback);
		}

		KBreadcrumb.addActionListener(this, KBubble.unmark, [ '/bubble/unmark/' + params.id ]);
		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KBubble.prototype = new KAddOn();
	KBubble.prototype.constructor = KBubble;

	KBubble.prototype.go = function() {		
		var offsetX = (!!this.params.offsetX ? this.params.offsetX : 0);
		var offsetY = (!!this.params.offsetY ? this.params.offsetY : 0);
		this.params.timer = -1;		
		this.parentRootDiv.appendChild(this.panel);
		KBubble.show(this, offsetX, offsetY, this.params.gravity);

	};
	
	KBubble.unmake = function(baloon) {
		if (!!baloon.panel && !!baloon.panel.parentNode) {
			baloon.panel.parentNode.removeChild(baloon.panel);
			delete baloon.panel;
			baloon.panel = undefined;
		}
		KAddOn.prototype.unload.call(baloon);
	};
	
	KBubble.make = function(target, params) {
		var tooltip = new KBubble(target, params);
		return tooltip;
	};
	
	KBubble.show = function(widget, offsetX, offsetY, gravity) {
		
		widget.panel.style.position = 'absolute';
		widget.panel.style.display = 'block';
		
		var parentOffsetX = 0;
		var parentOffsetY = 0;
		if (!!gravity) {
			if (gravity.x == 'right') {
				widget.panel.style.right = (typeof(offsetX) == 'string' ? offsetX : offsetX + 'px');
			}
			else if (gravity.x == 'center') {
				parentOffsetX = widget.parent.offsetLeft();
				widget.panel.style.left = (parentOffsetX + widget.parent.getWidth() - Math.round(widget.panel.offsetWidth / 2) - offsetX) + 'px';
			}
			else {
				widget.panel.style.left = (typeof(offsetX) == 'string' ? offsetX : offsetX + 'px');
			}
			if (gravity.y == 'bottom') {
				widget.panel.style.bottom = (typeof(offsetY) == 'string' ? offsetY : offsetY + 'px');
			}
			else {
				widget.panel.style.top = (typeof(offsetY) == 'string' ? offsetY : offsetY + 'px');
			}
		}
		else {
			widget.panel.style.left = offsetX + 'px';
			widget.panel.style.top = offsetY + 'px';
		}

		widget.panel.style.opacity = 1;
	};

	KBubble.unmark = function(widget, action) {
		var n = parseInt(widget.panel.innerHTML);
		if (!isNaN(n) && n != 1) {
			widget.panel.innerHTML = '' + (n - 1);
		}
		else {
			KBubble.unmake(widget);
		}
	};

	KSystem.registerAddOn('KBubble');
	KSystem.included('KBubble');
});

function KButton(parent, label, id, callback) {
	if (parent != null) {
		KWidget.call(this, parent);
		
		this.callback = callback;
		this.label = label;
		
		this.input = window.document.createElement('button');
		this.input.className = ' KButtonInput ';
		this.input.setAttribute('type', 'button');
		this.inputID = this.input.id = id;
		
		KDOM.addEventListener(this.input, 'click', callback);
		
		var labelDiv = window.document.createElement('div');
		labelDiv.className = ' KButtonInputLabel ';
		labelDiv.appendChild(window.document.createTextNode(label));
		this.input.appendChild(labelDiv);
		
		this.INPUT_ELEMENT = 'input';
	}
}

KSystem.include([
	'KTooltip',
	'KBaloon',
	'KWidget'
], function() {
	KButton.prototype = new KWidget();
	KButton.prototype.constructor = KButton;
	KButton.prototype.isLeaf = true;
	
	KButton.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KButton.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		//KDOM.fireEvent(this.input, 'focus');
		this.input.focus();
		this.parent.focus(this);
	};

	KButton.prototype.enable = function() {
		this.input.disabled = '';
	};
	
	KButton.prototype.disable = function() {
		this.input.disabled = 'disabled';
	};
	
	KButton.prototype.go = function() {
		this.gone = true;
		if (!this.activated()) {
			this.draw();
		}
		else {
			this.redraw();
		}
	};
	
	KButton.prototype.setCallback = function(callback) {
		KDOM.removeEventListener(this.input, 'click', this.callback);
		KDOM.addEventListener(this.input, 'click', callback);
		this.callback = callback;
	};
	
	KButton.prototype.setText = function(text, html) {
		var labelDiv = this.input.childNodes[0];
		labelDiv.innerHTML = '';
		if (html) {
			labelDiv.innerHTML = text;
		}
		else {
			labelDiv.appendChild(window.document.createTextNode(text));
		}
	};
	
	KButton.prototype.getText = function(html) {
		if (html) {
			return this.input.childNodes[0].innerHTML;
		}
		else {
			return this.input.childNodes[0].textContent;
		}
	};
	
	KButton.prototype.setHelpText = function(text) {
		this.helpText = text;
	};
	
	KButton.prototype.draw = function() {
		if (this.helpText) {
			KTooltip.make(this, {
				text : this.helpText,
				offsetX : 15,
				offsetY : 25
			});
		}
		
		this.content.appendChild(this.input);
		this.activate();
	};
	
	KButton.BUTTON_ELEMENT = 'input';
	
	KSystem.included('KButton');
});
function KCache() {
	this.cache = {};
}
{
	KCache.prototype = {};
	KCache.prototype.constructor = KCache;
	
	KCache.commit = function(key, value) {
		KCache.bumbleBee.cache[key] = value;
	};
	
	KCache.restore = function(key) {
		return KCache.bumbleBee.cache[key];
	};
	
	KCache.clear = function(key) {
		delete KCache.bumbleBee.cache[key];
	};
	
	KCache.find = function(regex, action) {
		if (action instanceof Function) {
			for ( var key in KCache.bumbleBee.cache) {
				if (regex.test(key)) {
					action(key, KCache.bumbleBee.cache[key]);
				}
			}
			return null;
		}
		else {
			var ret = [];
			for ( var key in KCache.bumbleBee.cache) {
				if (regex.test(key)) {
					ret.push(KCache.bumbleBee.cache[key]);
				}
			}
			return ret;
		}
	};
	
	KCache.bumbleBee = new KCache();
	
	KSystem.included('KCache');
}

function KCalendar(parent, label, id) {
	if (parent) {
		this.isLeaf = false;
		KAbstractInput.call(this, parent, label, id);
		this.currentDate = new Date();
		this.days = new Array();
		this.editable = false;
		this.calendarToggled = false;
		this.contentDiv = window.document.createElement('div');
		this.contentDiv.className = ' KCalendarContentDiv_' + id.toUpperCase() + ' KCalendarContentDiv ';

		this.setValue(this.currentDate);
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KCalendar.prototype = new KAbstractInput();
	KCalendar.prototype.constructor = KCalendar;
	
	KCalendar.prototype.setValue = function(value) {
		if (!value) {
			return;
		}
		
		if (value != '') {
			var formattedDate = value.getDate() + '/' + (value.getMonth() + 1) + '/' + value.getFullYear();
			KAbstractInput.prototype.setValue.call(this, formattedDate);
			this.currentDate = value;
		}
	};
	
	KCalendar.prototype.draw = function() {		
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.childNodes[0].setAttribute('type', 'text');
		this.inputContainer.childNodes[0].setAttribute('readonly', 'readonly');

		var calendarButton = window.document.createElement('button');
		calendarButton.setAttribute('type', 'button');
		KDOM.addEventListener(calendarButton, 'click', KCalendar.toggleCalendar);
		this.content.appendChild(calendarButton);

		var table = window.document.createElement('table');
		var tHead = table.appendChild(window.document.createElement('thead'));
		var tHeadNav = tHead.appendChild(window.document.createElement('tr'));
		var tHeadDays = tHead.appendChild(window.document.createElement('tr'));
		var tBody = table.appendChild(window.document.createElement('tbody'));
		var monthDate = new Date();
		monthDate.setMonth(this.currentDate.getMonth());
		monthDate.setDate(1);
		
		this.contentDiv.appendChild(table);
		table.className = ' KCalendarTable ';
		tHead.className = ' KCalendarHeader ';
		tBody.className = ' KCalendarBody ';
		
		this.titlePreviousMonth = tHeadNav.appendChild(window.document.createElement('th')).appendChild(window.document.createElement('button'));
		this.titlePreviousMonth.setAttribute('type', 'button');
		this.titlePreviousMonth.name = this.id;
		this.titlePreviousMonth.appendChild(window.document.createTextNode('<'));
		this.titlePreviousMonth.className = ' KCalendarPrevButton ';
		this.titleText = tHeadNav.appendChild(window.document.createElement('th'));
		this.titleText.colSpan = '5';
		this.titleText.className = ' KCalendarTitle ';
		this.titleText.appendChild(window.document.createTextNode(' '));
		this.titleNextMonth = tHeadNav.appendChild(window.document.createElement('th')).appendChild(window.document.createElement('button'));
		this.titleNextMonth.setAttribute('type', 'button');
		this.titleNextMonth.name = this.id;
		this.titleNextMonth.appendChild(window.document.createTextNode('>'));
		this.titleNextMonth.className = ' KCalendarNextButton ';
		
		KDOM.addEventListener(this.titlePreviousMonth, 'click', function(e) {
			var element = KDOM.getEventTarget(e);
			var widget = Kinky.getWidget(element.name);
			widget.showMonth(widget.currentDate.getMonth() - 1, widget.currentDate.getFullYear());
		});
		KDOM.addEventListener(this.titleNextMonth, 'click', function(e) {
			var element = KDOM.getEventTarget(e);
			var widget = Kinky.getWidget(element.name);
			widget.showMonth(widget.currentDate.getMonth() + 1, widget.currentDate.getFullYear());
		});
		
		for ( var i = 0; i != 6; i++) {
			var tRow = tBody.appendChild(window.document.createElement('tr'));
			this.days[i] = new Array();
			for ( var k = 0; k != 7; k++) {
				if (i == 0) {
					var th = window.document.createElement('th');
					th.className = ' KCalendarDayName ';
					var abbr = window.document.createElement('abbr');
					abbr.title = KCalendar.localization['weekDayName'][k];
					abbr.appendChild(window.document.createTextNode(KCalendar.localization['weekDayName'][k].charAt(0)));
					th.appendChild(abbr);
					tHeadDays.appendChild(th);
				}
				this.days[i][k] = tRow.appendChild(window.document.createElement('td')).appendChild(window.document.createElement('button'));
				this.days[i][k].setAttribute('type', 'button');
				this.days[i][k].name = this.id;
				tRow.style.textAlign = 'right';
				this.days[i][k].appendChild(window.document.createTextNode(' '));
				this.days[i][k].className = ' KCalendarDay ';
				KDOM.addEventListener(this.days[i][k], 'click', function(e) {
					var element = KDOM.getEventTarget(e);
					var widget = Kinky.getWidget(element.name);
					
					widget.currentDate.setDate(element.innerHTML);
					widget.setValue(widget.currentDate);
					widget.inputContainer.childNodes[0].value = widget.currentDate.getDate() + '/' + (widget.currentDate.getMonth() + 1) + '/' + widget.currentDate.getFullYear();
					KCalendar.toggleCalendar(null, widget);
				});
			}
		}
		this.showMonth(this.currentDate.getMonth(), this.currentDate.getFullYear());
	};
	
	KCalendar.prototype.showMonth = function(monthToShow, monthYear) {
		if (monthToShow > 11) {
			monthToShow = 0;
			monthYear++;
		}
		if (monthToShow < 0) {
			monthToShow = 11;
			monthYear--;
		}
		var monthDate = new Date();
		var nDayInMonth = this.getDaysInMonth(monthToShow, monthYear);
		monthDate.setDate(1);
		monthDate.setMonth(monthToShow);
		monthDate.setFullYear(monthYear);
		this.currentDate = monthDate;
		
		if (this.titleText.childNodes.length != 0) {
			this.titleText.removeChild(this.titleText.childNodes[0]);
		}
		
		this.titleText.appendChild(window.document.createTextNode(' ' + KCalendar.localization['monthName'][monthToShow] + ' ' + monthYear + ' '));
		
		for ( var i = 0, dayOfMonth = 0; i != 6; i++) {
			for ( var k = 0; k != 7; k++) {
				if ((dayOfMonth == 0) && (k == monthDate.getDay())) {
					dayOfMonth = 1;
				}
				if (this.days[i][k].childNodes.length != 0) {
					this.days[i][k].removeChild(this.days[i][k].childNodes[0]);
				}
				this.days[i][k].style.visibility = ((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? 'hidden' : 'visible');
				this.days[i][k].appendChild(window.document.createTextNode((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? '0' : '' + dayOfMonth++));
			}
		}
	};
	
	KCalendar.prototype.getDaysInMonth = function(month, year) {
		var m = [
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		];
		if (month != 1) {
			return m[month];
		}
		if (year % 4 != 0) {
			return m[1];
		}
		if ((year % 100 == 0) && (year % 400 != 0)) {
			return m[1];
		}
		return m[1] + 1;
	};
	
	KCalendar.toggleCalendar = function(event, cal) {
		var widget = cal || KDOM.getEventWidget(event);
		
		var top = (widget.offsetTop() - widget.scrollTop() - window.pageYOffset + widget.getHeight());

		widget.setStyle({
			position : 'absolute',
			top : top + 'px',
			right : (KDOM.getBrowserWidth() - (widget.offsetLeft() + widget.panel.offsetWidth)) + 'px'
		}, widget.contentDiv);
		if (widget.calendarToggled) {
			window.document.body.removeChild(widget.contentDiv);
			widget.calendarToggled = false;
			KCalendar.OPENED = null;
		}
		else {
			window.document.body.appendChild(widget.contentDiv);
			widget.calendarToggled = true;
			KCalendar.OPENED = widget;
		}
	};
	
	KCalendar.localization = KCalendar.localization || {};
	KCalendar.OPENED = null;
	
	KCalendar.clickOut = function() {
		KDOM.addEventListener(window.document.body, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			if (!!widget) {
				if (widget.parent instanceof KCalendar && (widget.parent.id == KCalendar.OPENED.id)) {
					return;
				}
			}
			else {
				var element = KDOM.getEventTarget(event);
				widget = Kinky.getWidget(element.name);
			}
			
			if (KCalendar.OPENED && (!widget || (widget && (widget.id != KCalendar.OPENED.id)))) {
				window.document.body.removeChild(KCalendar.OPENED.contentDiv);
				KCalendar.OPENED.calendarToggled = false;
				KCalendar.OPENED = null;
			}
		});
	};
	
	if (window.attachEvent) {
		window.attachEvent('onload', KCalendar.clickOut);
	}
	else {
		window.addEventListener('load', KCalendar.clickOut, false);
	}
	
	KSystem.included('KCalendar');
});
function KCanvas(parent, params) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		this.pencil = this.content.getContext("2d");
		if (params && params.width) {
			this.content.width = params.width;
		}
		if (params && params.height) {
			this.content.height = params.height;
		}
	}
}

KSystem.include([
	'KWidget'
], function() {
	KCanvas.prototype = new KWidget();
	KCanvas.prototype.constructor = KCanvas;
	
	KCanvas.prototype.init = function(data) {
		this.data = data;
	};
	
	KCanvas.prototype.go = function() {
		this.draw();
	};
	
	KCanvas.prototype.getImage = function(mime) {
		var dataURL = this.content.toDataURL(mime || KCanvas.IMAGE_MIME.PNG).split(',');
		return {
			mime : mime || KCanvas.IMAGE_MIME.PNG,
			rawData : dataURL[1]
		};
	};
	
	KCanvas.prototype.draw = function(params) {
		if (!params && this.data) {
			params = this.data;
		}
		if (params) {
			if (!(params instanceof Array)) {
				params = [
					params
				];
			}
			for ( var shape in params) {
				this.pencil.save();
				this[draw + params[shape].shape](params[shape]);
				this.pencil.restore();
			}
		}
		this.activate();
	};
	
	KCanvas.prototype.saveContext = function() {
		this.pencil.save();
	};
	
	KCanvas.prototype.restoreContext = function() {
		this.pencil.restore();
	};
	
	KCanvas.prototype.setCanvasStyle = function(params) {
		if (params) {
			for ( var param in params) {
				this.pencil[param] = params[param];
			}
		}
	};
	
	KCanvas.prototype.drawRect = function(params) {
		this.setCanvasStyle(params.style);
		if (params.style && params.style.fillStyle) {
			this.pencil.fillRect(params.x, params.y, params.width, params.height);
		}
		if (params.style && params.style.strokeStyle) {
			this.pencil.strokeRect(params.x, params.y, params.width, params.height);
		}
	};
	
	KCanvas.prototype.drawLine = function(params, noPath) {
		if (!(params.point instanceof Array)) {
			params.point = [
				params.point
			];
		}
		if (!noPath) {
			this.pencil.beginPath();
		}
		this.setCanvasStyle(params.style);
		this.pencil.moveTo(params.point[0].x, params.point[0].y);
		for ( var index = 1; index != params.point.length; index++) {
			this.pencil.lineTo(params.point[index].x, params.point[index].y);
		}
		if (!noPath) {
			this.pencil.stroke();
		}
	};
	
	KCanvas.prototype.drawArc = function(params, noPath) {
		this.setCanvasStyle(params.style);
		if (!noPath) {
			this.pencil.beginPath();
		}
		this.pencil.arc(params.x, params.y, params.radius, params.start, params.end, params.direction || KCanvas.ARC.DIRECTION.COUNTER_CLOCK_WISE);
		if (!noPath && params.style && params.style.fillStyle) {
			this.pencil.fill();
		}
		if (!noPath && params.style && params.style.strokeStyle) {
			this.pencil.stroke();
		}
	};
	
	KCanvas.prototype.drawPath = function(params) {
		this.setCanvasStyle(params.style);
		this.pencil.beginPath();
		for ( var shape in params.path) {
			if (/Arc|Line/.test(params.path[shape].shape)) {
				this['draw' + params.path[shape].shape](params.path[shape], true);
			}
		}
		this.pencil.stroke();
	};
	
	KCanvas.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'div',
			content : 'canvas',
			background : 'div'
		}
	};
	
	KCanvas.SHAPE = {
		IMAGE : 'Image',
		RECTANGLE : 'Rect',
		LINE : 'Line',
		PATH : 'Path',
		ARC : 'Arc',
		BEZIER : 'Bezier'
	};
	
	KCanvas.LINE = {
		LINE_CAP : {
			BUTT : 'butt',
			ROUND : 'round',
			SQUARE : 'square'
		},
		LINE_JOIN : {
			ROUND : 'round',
			BEVEL : 'bevel',
			MITER : 'miter'
		}
	};
	
	KCanvas.RECTANGLE = {};
	
	KCanvas.ARC = {
		DIRECTION : {
			COUNTER_CLOCK_WISE : true,
			CLOCK_WISE : false
		}
	};
	
	KCanvas.IMAGE_MIME = {
		JPEG : 'image/jpeg',
		PNG : 'image/png'
	};
	
	KSystem.included('KCanvas');
});
function KCaptcha(parent, label, id, params) {
	if (parent != null) {
		KAbstractInput.call(this, parent, 'text', label, id, true);

		this.image = new Image();
		this.image.className = 'KCaptchaImage';

		this.button = window.document.createElement('button');
		this.button.setAttribute('type', 'button');
		this.button.className = 'KCaptchaButton';
		this.button.innerHTML = 'Reload';
		KDOM.addEventListener(this.button, 'click', KCaptcha.reloadImage);

		this.data = params;
	}
}
KSystem.include( [ 'KAbstractInput' ], function() {

	KCaptcha.prototype = new KAbstractInput;
	KCaptcha.prototype.constructor = KCaptcha;

	KCaptcha.prototype.go = function() {
		this.load();
	};

	KCaptcha.prototype.load = function() {
		this.draw();
	};

	KCaptcha.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);

		var url = '';
		for ( var index in this.data) {
			url += '&' + index + '=' + this.data[index];
		}

		this.image.src = KCaptcha.CAPTCHA_SERVICE_URL + "?date=" + new Date().getMilliseconds() + url;
		this.inputContainer.appendChild(this.button);
		this.inputContainer.appendChild(this.image);
	};

	KCaptcha.reloadImage = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.refresh();
	};

	KCaptcha.IMAGE_ELEMENT = 'image';
	KCaptcha.CAPTCHA_SERVICE_URL = null;

	KSystem.included('KCaptcha');

});
function KCell(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.editMode = false;
		this.isDrawn = false;
		this.data = null;
		this.children = null;
		this.requestedPage = this.nPage = 0;
		this.nChildren = 0;
		this.perPage = 10;
		this.totalCount = null;
		this.display = false;
		
		this.kinky = Kinky.bunnyMan;
		this.breadcrumb = '';
		
		this.id = this.id || this.kinky.addWidget(this);
		this.hash = null;
		
		this.panel = this.content = window.document.createElement('td');
		this.panel.id = this.id;
		this.panel.className = ' KWidgetPanel ' + this.className + ' ';
		this.panel.style.overflow = this.overflow;
		this.panel.style.position = 'relative';
		// this.panel.style.clear = 'both';
		
		this.window = false;
		this.closeButton = null;
		this.minimizeButton = null;
		this.maximizeButton = null;
		this.buttons = 0;
		this.movable = false;
		this.frame = false;
		this.effects = new Object();
		this.waiting = false;
		
		this.error = null;
		this.request = null;
		this.response = null;
		this.needAuthentication = false;
		
		this.fromListener = false;
		this.lastWidth = 0;
		this.lastHeight = 0;
		this.scroll = {};
	}
}

KSystem.include([
	'KWidget'
], function() {
	KCell.prototype = new KWidget();
	KCell.prototype.constructor = KCell;
	
	KCell.prototype.go = function() {
		this.draw();
	};
	
	KCell.prototype.draw = function() {
		this.panel.style.overflow = 'visible';
		
		for ( var element in this.childWidgets()) {
			this.childWidget(element).go();
		}
		this.activate();
	};
	
	KCell.prototype.appendChild = function(element) {
		if (element instanceof KWidget) {
			KWidget.prototype.appendChild.call(this, element);
		}
	};
	
	KSystem.included('KCell');
});

function KCheckBox(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.selectedOptions = {};
		
		this.checkbox = window.document.createElement('a');
		this.checkbox.href = 'javascript:void(0)';
		this.checkbox.style.cursor = 'normal';
		this.checkbox.className = ' KCheckBoxOptionsContent ';
		this.checkbox.style.overflow = 'visible';
		this.checkbox.style.position = 'relative';
		
		this.INPUT_ELEMENT = 'checkbox';
		
		this.lastElement = null;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KCheckBox.prototype = new KAbstractInput();
	KCheckBox.prototype.constructor = KCheckBox;
	
	KCheckBox.prototype.clear = function() {
		this.checkbox.innerHTML = '';
		this.setValue(null);
		this.selectedOptions = {};
		KAbstractInput.prototype.clear.call(this);
	};
	
	KCheckBox.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			for ( var index in this.validations) {
				if (((this.validations[index].regex.source == '(.+)') || (this.validations[index].regex.source == '.+')) && (value.length == 0)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				for ( var k in value) {
					if (!this.validations[index].regex.test(value[k])) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}
				}
				
				this.errorArea.innerHTML = '';
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KCheckBox.prototype.addOption = function(value, caption, selected) {
		var checkBoxItemContainer = window.document.createElement('div');
		checkBoxItemContainer.className = ' KCheckBoxOption ';
		
		var check = window.document.createElement('button');
		check.src = selected ? this.imageOn : this.image;
		check.innerHTML = '&nbsp;';
		check.setAttribute('type', 'button');
		check.value = JSON.stringify(value);
		check.id = this.inputID + this.checkbox.childNodes.length;
		checkBoxItemContainer.appendChild(check);
		
		var label = window.document.createElement('label');
		label.setAttribute('for', check.id);
		label.alt = check.value;
		label.name = check.id;
		label.innerHTML = caption;
		
		KDOM.addEventListener(checkBoxItemContainer, 'click', KCheckBox.selectOption);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseover', KCheckBox.mouseOver);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseout', KCheckBox.mouseOut);
		checkBoxItemContainer.appendChild(label);
		this.checkbox.appendChild(checkBoxItemContainer);
		
		if (selected) {
			this.selectedOptions[check.value] = true;
			KCSS.addCSSClass('KCheckBoxOptionSelected', checkBoxItemContainer);
		}
		
		return true;
	};
	
	KCheckBox.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			eventName = 'propertychange';
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};
	
	KCheckBox.prototype.createInput = function() {
		return this.checkbox;
	};
	
	KCheckBox.prototype.check = function(idx) {
		KDOM.fireEvent(this.checkbox.childNodes[idx], 'click');
	};
	
	KCheckBox.prototype.uncheck = function(idx) {
		KDOM.fireEvent(this.checkbox.childNodes[idx], 'click');
	};
	
	KCheckBox.prototype.select = function(element) {
		if (!element) {
			this.selectedOption = null;
			this.input.value = '';
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				child.childNodes[0].innerHTML = '&nbsp;';
			}
			return;
		}
		
		if (element.tagName.toLowerCase() == 'label') {
			element = element.previousSibling;
		}
		if (element.tagName.toLowerCase() == 'div') {
			element = element.childNodes[0];
		}
		
		if (element.parentNode.className.indexOf('KCheckBoxOptionSelected') != -1) {
			delete this.selectedOptions[element.value];
			KCSS.removeCSSClass('KCheckBoxOptionSelected', element.parentNode);
			element.innerHTML = '&nbsp;';
		}
		else {
			KCSS.addCSSClass('KCheckBoxOptionSelected', element.parentNode);
			this.selectedOptions[element.value] = true;
			element.innerHTML = '&nbsp;';
			
		}
		
		KDOM.fireEvent(this.childDiv(this.INPUT_ELEMENT), 'propertychange');
	};
	
	KCheckBox.prototype.getValue = function() {
		var toReturn = new Array();
		
		for ( var index in this.selectedOptions) {
			toReturn.push(JSON.parse(index));
		}
		
		return toReturn;
	};
	
	KCheckBox.prototype.setValue = function(value) {
		if (!value) {
			delete this.selectedOptions;
			this.selectedOptions = new Object();
		}
		else {
			this.selectedOptions[JSON.stringify(value)] = true;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
			var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
			if (child.childNodes[0].value == this.input.value) {
				this.select(child.childNodes[0]);
			}
		}
	};
	
	KCheckBox.mouseOver = function(event) {
		var element = KDOM.getEventTarget(event);
		if ((element.tagName.toLowerCase() == 'label') || (element.tagName.toLowerCase() == 'button')) {
			element = element.parentNode;
		}
		if (element.className.indexOf('KCheckBoxOptionSelected') == -1) {
			KCSS.addCSSClass('KCheckBoxOptionOver', element);
		}
	};
	
	KCheckBox.mouseOut = function(event) {
		var element = KDOM.getEventTarget(event);
		if ((element.tagName.toLowerCase() == 'label') || (element.tagName.toLowerCase() == 'button')) {
			element = element.parentNode;
		}
		KCSS.removeCSSClass('KCheckBoxOptionOver', element);
	};
	
	KCheckBox.selectOption = function(event) {
		KDOM.stopEvent(event);
		var element = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);
		
		widget.select(element);
	};
	
	KCheckBox.updateValue = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed();
	};
	
	KCheckBox.OPTION_CONTAINER_DIV = 'checkbox';
	KCheckBox.OPENED = null;
	
	KSystem.included('KCheckBox');
});

function KColorPicker(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.options = options || {};
	}
}

KSystem.include( [ 
	'KEffects',
	'KAbstractInput' 
], function() {
	KColorPicker.prototype = new KAbstractInput;
	KColorPicker.prototype.constructor = KColorPicker;
		
	KColorPicker.prototype.createInput = function() {
		this.legend = window.document.createElement('input');
		{
			this.legend.setAttribute('type', 'text');
			this.legend.className = ' KColorPickerLegend ' + this.className + 'Legend color ';
			this.legend.name = (!!this.inputID ? this.inputID : this.hash);
		}
		return this.legend;
	};

	KColorPicker.prototype.getValue = function() {
		return this.input.value;
	};

	KColorPicker.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = ColorConverter.toString(value);
			if (!!this.legend) {
				this.legend.value = ColorConverter.toHex(ColorConverter.toRGB(this.input.value));
			}
		}
	};

	KColorPicker.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		var self = this;
		this.readyTimer = KSystem.addTimer(function() {
			var width = KDOM.normalizePixelValue(KCSS.getComputedStyle(self.inputContainer).width);
			if (isNaN(width) || width == 0) {
				return;
			}
			var exists = jscolor || 'off'
			if (exists === 'off') {
				return;
			}
			KSystem.removeTimer(self.readyTimer);
			jscolor.bind();
		}, 100, true);
	};

	KColorPicker.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this); 

		if (!!this.input.value && this.input.value != '') {
			this.legend.value = ColorConverter.toHex(ColorConverter.toRGB(this.input.value));
		}

		KDOM.addEventListener(this.legend, 'change', function(e) {
			var element = KDOM.getEventTarget(e);
			var w = KDOM.getEventWidget(e);
			w.input.value = ColorConverter.toString(ColorConverter.toRGB('#' + element.value));
		});

		if (!KColorPicker.external_loader) {
			KColorPicker.external_loader = window.document.createElement('script');
			KColorPicker.external_loader.charset = 'utf-8';
			KColorPicker.external_loader.src = (CODA_VERSION || '') + '/jscolor/jscolor.js';
			window.document.getElementsByTagName('head')[0].appendChild(KColorPicker.external_loader);
		}

	};

	KColorPicker.prototype.INPUT_ELEMENT = 'input';
	KColorPicker.external_loader = undefined;
	KColorPicker.LABEL_CONTAINER = 'label';
	KColorPicker.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KColorPicker');
});
function KCombo(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.optionsToggled = false;
		
		this.combo = window.document.createElement('a');
		{
			this.combo.id = id;
			this.combo.href = 'javascript:void(0)';
			this.combo.style.cursor = 'pointer';
			this.combo.style.textDecoration = 'none';
			this.combo.className = ' KComboValue ';
			this.combo.style.overflow = 'visible';
			this.combo.style.position = 'relative';

			KDOM.addEventListener(this.combo, 'click', KCombo.toggleOptions);
		}
		this.INPUT_ELEMENT = 'combo';
		
		this.selectedOption = null;
		this.comboText = window.document.createElement('div');
		{
			this.comboText.className = ' ' + this.className + 'Text KComboText ';
		}
		this.combo.appendChild(this.comboText);
		
		this.optionsPanel = window.document.createElement('div');
		{
			this.optionsPanel.className = ' ' + this.className + 'OptionsPanel KComboOptionsPanel ';
			this.optionsPanel.style.overflow = 'visible';
			this.optionsPanel.style.position = 'fixed';
			this.optionsPanel.name = this.id;
		}
		
		this.optionsContainer = window.document.createElement('div');
		{
			this.optionsContainer.className = ' KComboOptionsContainer_' + id.toUpperCase() + ' ' + this.className + 'OptionsContainer KComboOptionsContainer ';
			this.optionsContainer.style.overflow = 'hidden';
			this.optionsContainer.style.position = 'relative';
		}
		this.optionsPanel.appendChild(this.optionsContainer);
		
		this.optionsContent = window.document.createElement('div');
		{
			this.optionsContent.className = ' KComboOptionsContent_' + id.toUpperCase() + ' ' + this.className + 'OptionsContent KComboOptionsContent ';
			this.optionsContent.style.overflow = 'visible';
			this.optionsContent.style.position = 'absolute';
			this.optionsContent.style.left = '0';
			this.optionsContent.style.right = '0';
		}
		this.optionsContainer.appendChild(this.optionsContent);
		
		this.dropButton = window.document.createElement('button');
		{
			this.dropButton.innerHTML = '&nbsp;';
			this.dropButton.setAttribute('type', 'button');
			this.dropButton.className = ' ' + this.className + 'DropButton KComboDropButton ';

			KDOM.addEventListener(this.dropButton, 'click', KCombo.toggleOptions);
		}
		
		this.currentIndex = -1;
	}
}

KSystem.include([
	'KAbstractInput',
	'HTML'
], function() {
	KCombo.prototype = new KAbstractInput();
	KCombo.prototype.constructor = KCombo;
	
	KCombo.prototype.clear = function() {
		this.selectedOption = null;
		this.setValue(null);
		this.comboText.innerHTML = '';
		this.optionsContent.innerHTML = '';
		KAbstractInput.prototype.clear.call(this);
	};
	
	KCombo.prototype.focus = function(dispatcher) {
		KAbstractInput.prototype.focus.call(this, dispatcher);
	};
	
	KCombo.prototype.markNext = function() {
		var beginIndex = this.currentIndex;

		for ( var index = beginIndex + 1; index < this.optionsContent.childNodes.length; index++) {
			if (this.optionsContent.childNodes[index].style.display != 'none') {
				if (this.currentIndex >= 0) {
					this.removeCSSClass('KComboOptionOver', this.optionsContent.childNodes[this.currentIndex]);
				}
				this.currentIndex = index;
				KCombo.selectOption(null, this.optionsContent.childNodes[index], this, true);
				this.optionsPanel.scrollTop = this.optionsContent.childNodes[index].offsetTop;
				this.addCSSClass('KComboOptionOver', this.optionsContent.childNodes[index]);
				return this.currentIndex;
			}
		}
		return -1;
	};
	
	KCombo.prototype.markPrevious = function() {
		var beginIndex = this.currentIndex;

		for ( var index = beginIndex - 1; index > -1; index--) {
			if (this.optionsContent.childNodes[index].className && /KComboOption/.test(this.optionsContent.childNodes[index].className) && ((this.optionsContent.childNodes[index].style.display != 'none'))) {
				if (this.currentIndex >= 0) {
					this.removeCSSClass('KComboOptionOver', this.optionsContent.childNodes[this.currentIndex]);
				}
				this.currentIndex = index;
				KCombo.selectOption(null, this.optionsContent.childNodes[index], this, true);
				this.optionsPanel.scrollTop = this.optionsContent.childNodes[index].offsetTop;
				this.addCSSClass('KComboOptionOver', this.optionsContent.childNodes[index]);
				return this.currentIndex;
			}
		}
		return -1;
	};
	
	KCombo.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			target = this.input;
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};
	
	KCombo.prototype.blur = function() {
		KCSS.removeCSSClass('KComboOpened', this.dropButton);
		KCSS.removeCSSClass('KComboOpened', this.content);
		KCSS.removeCSSClass('KComboFocused', this.combo);

		if (this.optionsPanel.style.opacity != '0') {
			this.optionsPanel.style.opacity = '0';
		}
		this.optionsPanel.parentNode.removeChild(this.optionsPanel);
		this.optionsToggled = false;
	};
	
	KCombo.prototype.onKeyPressed = function(event) {
		var keyCode = event.keyCode || event.which;
		switch (keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				break;
			}
			case 8: {
				KDOM.stopEvent(event);
				var text = '' + this.innerLabel.innerHTML;
				if (text.length != 0) {
					text = text.substr(0, text.length - 1);
					this.innerLabel.innerHTML = text;
					this.filter(this.innerLabel.textContent);
				}
				break;
			}
			case 18: 
			case 17: 
			case 16: {
				break;
			}
			case 9: {
				Kinky.unsetModal(widget);
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.innerLabel.innerHTML = '';
				this.filter(this.innerLabel.textContent);
				KCombo.toggleOptions(null, this, true);
				break;
			}
			default: {
				var key = (event.charCode != 0 ? event.charCode : keyCode);
				if (key != 0) {
					if (!this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
					this.innerLabel.style.visibility = 'hidden';
					this.innerLabel.innerHTML += String.fromCharCode(key);
					this.filter(this.innerLabel.textContent);
				}
			}
		}
	};
	
	KCombo.prototype.filter = function(search) {
		search = search.toLowerCase();
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (!!this.optionsContent.childNodes[i].tagName && this.optionsContent.childNodes[i].tagName.toLowerCase() == 'a') {
				var v = this.optionsContent.childNodes[i].textContent.toLowerCase();
				if (v.indexOf(search) == 0) {
					this.optionsPanel.scrollTop = this.optionsContent.childNodes[i].offsetTop;
					this.currentIndex = i;
					break;
				}
			}
		}
	};
	
	KCombo.prototype.select = function(element, noToggle) {
		if (!this.input) {
			return;
		}
		if (!!element) {
			this.selectedOption = JSON.parse(element.alt);
			if (this.input.value != element.alt) {
				KDOM.fireEvent(this.input, 'change');
			}
			if (!this.input) {
				return;
			}
			this.input.value = element.alt;
			this.comboText.innerHTML = element.innerHTML;
			this.validate();
		}
		else {
			this.selectedOption = null;
			this.input.value = '';
			this.comboText.innerHTML = '';
		}
		this.innerLabel.innerHTML = '';
		this.innerLabel.style.visibility = 'visible';
		if (!noToggle) {
			KCombo.toggleOptions(null, this, true);
		}
		
	};
	
	KCombo.prototype.addOption = function(value, caption, selected) {
		var label = window.document.createElement('a');
		label.className = ' KComboOption ';
		label.href = 'javascript:void(0)';
		label.alt = JSON.stringify(value);
		label.name = this.inputID;
		label.innerHTML = caption;
		
		KDOM.addEventListener(label, 'click', KCombo.selectOption);
		this.optionsContent.appendChild(label);
		
		if (selected) {
			this.setValue(value);
		}
	};
	
	KCombo.prototype.createLabel = function() {
		var label = KAbstractInput.prototype.createLabel.call(this);
		this.addEventListener('click', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.combo.focus();
		}, label);
		return label;
	};
	
	KCombo.prototype.createInput = function() {
		return this.combo;
	};
	
	KCombo.prototype.setValue = function(value) {
		if (!value) {
			this.select();
			return;
		}
		
		this.selectedOption = value;
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].alt == this.input.value) {
				this.select(this.optionsContent.childNodes[i]);
			}
		}
	};

	KCombo.prototype.getValueLabel = function() {
		if (this.selectedOption != null) {
			return this.comboText.innerHTML;
		}
		else {
			return '';
		}
	};	
	
	KCombo.prototype.getValue = function() {
		if (this.selectedOption != null) {
			return this.selectedOption;
		}
		else {
			return '';
		}
	};
	
	KCombo.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.appendChild(this.dropButton);
		this.optionsPanel.style.overflow = 'auto';
		this.optionsContainer.style.overflow = 'visible';
		this.optionsContent.style.position = 'relative';
		
		this.addEventListener('keydown', KCombo.keyPressed, this.INPUT_ELEMENT);
	};
	
	KCombo.prototype.destroy = function() {
		Kinky.unsetModal(this);
		KWidget.prototype.destroy.call(this);
	};
	
	KCombo.keyPressed = function(event) {
		event = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed(event);
	};
	
	KCombo.selectOption = function(event, element, widget, noToggle) {
		if (!element) {
			KDOM.stopEvent(event);
			element = KDOM.getEventTarget(event);
			widget = KDOM.getEventWidget(event);
			if (!element) {
				return;
			}
		}
		
		element = KCombo.getLabel(element);
		if (!!element && ((element.alt != null)) && ((element.alt != ''))) {
			widget.select(element, noToggle);
		}
		else {
			widget.select(null, noToggle);
		}
	};
	
	KCombo.getLabel = function(element) {
		var current = element;
		while (!!current && ((current.tagName.toLowerCase() != 'a'))) {
			if (!!current && !!current.className && /KComboOption/.test(current.className)) {
				return current.childNodes[current.childNodes.length - 1];
			}
			current = current.parentNode;
		}
		return current;
	};
	
	KCombo.toggleOptions = function(event, widget, forceClose) {
		if (!widget) {
			KDOM.stopEvent(event);
			widget = KDOM.getEventWidget(event);
		}
		
		if (widget.optionsToggled || forceClose) {
			Kinky.unsetModal(widget);
			KCSS.removeCSSClass('KComboFocused', widget.combo);
		}
		else {
			KCSS.addCSSClass('KComboOpened', widget.dropButton);
			KCSS.addCSSClass('KComboOpened', widget.content);
			widget.childDiv(KWidget.ROOT_DIV).appendChild(widget.optionsPanel);


			var top = (widget.offsetTop() - widget.scrollTop() - window.pageYOffset + widget.getHeight());

			if (widget.optionsPanel.offsetHeight > KDOM.getBrowserHeight() - top) {
				widget.optionsPanel.style.maxHeight = (KDOM.getBrowserHeight() - top - 30) + 'px';
			}

			var left = widget.offsetLeft();
			var width = widget.getWidth();
			var delta = Math.round(widget.optionsPanel.offsetHeight / 2);

			KCSS.setStyle({
				opacity : '0',
				width : width + 'px',
				top : (top - delta) + 'px',
				left : left + 'px'
			}, [
				widget.optionsPanel
			]);
			widget.optionsToggled = true;
			var opened = Kinky.getActiveModalWidget();
			if (!!opened && opened instanceof KCombo) {
				Kinky.unsetModal(opened);
			}
			Kinky.setModal(widget, true);

			KEffects.addEffect(widget.optionsPanel,  [ 
				{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 200,
					applyToElement : true,
					lock : {
						x : true
					},
					go : {
						y : top
					},
					from : {
						y : top - delta
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					applyToElement : true,
					duration : 500,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}
			]);

			var v = widget.comboText.textContent;
			if(!!v && v != '') {
				for (var i = 0; i != widget.optionsContent.childNodes.length; i++) {
					var ov = widget.optionsContent.childNodes[i].textContent;
					if (ov != '' && ov == v) {
						widget.optionsPanel.scrollTop = widget.optionsContent.childNodes[i].offsetTop;
						widget.currentIndex = i;
						return;
					}
				}
			}
			KCSS.addCSSClass('KComboFocused', widget.combo);
		}
	};

	KCombo.focused = function(e) {
		KDOM.stopEvent(e);
		var self = KDOM.getEventWidget(e);
		KCSS.addCSSClass('KComboFocused', self.combo);
	};
	
	KCombo.blured = function(e) {
		KDOM.stopEvent(e);
		KCSS.removeCSSClass('KComboFocused', KDOM.getEventWidget(e).combo);
	};
	
	KCombo.OPTIONS_DIV = 'optionsPanel';
	KCombo.OPTIONS_CONTENT_DIV = 'optionsContent';
	KCombo.OPTIONS_CONTAINER_DIV = 'optionsContainer';
	
	KSystem.included('KCombo');
});
function KComment(parent, params) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.data = params || {};
		this.exceptions = /urlHashText|actions|user|type|info|reply|cssClass|feClass|feService/;
		this.fieldInfoExceptions = null;
		this.infoFields = {};
		this.hasAttach = false;
		this.removeCallback = null;
		this.dateFormat = 'd de MMM Y, H:i:s';
		this.maxAttachs = 1;
		this.labels = {
			go : 'go',
			cancel : 'cancel',
			remove : 'delete'
		};
		
		this.infoArea = window.document.createElement('div');
		this.infoArea.className = 'KCommentInfoArea';
		this.content.appendChild(this.infoArea);
		
		this.commentArea = window.document.createElement('div');
		this.commentArea.className = 'KCommentArea';
		this.content.appendChild(this.commentArea);
		
	}
}
KSystem.include([
	'KWidget',
	'KFileDropPanel',
	'KFile'
], function() {
	KComment.prototype = new KWidget;
	KComment.prototype.constructor = KComment;
	
	KComment.prototype.go = function() {
		if (this.data.type != null) {
			this.draw();
		}
		else {
			this.load();
		}
	};
	
	KComment.prototype.clear = function() {
		KWidget.prototype.clear.call(this);
		
		this.infoArea = window.document.createElement('div');
		this.infoArea.className = 'KCommentInfoArea';
		this.content.appendChild(this.infoArea);
		
		this.commentArea = window.document.createElement('div');
		this.commentArea.className = 'KCommentArea';
		this.content.appendChild(this.commentArea);
	};
	
	KComment.prototype.load = function() {
		var params = new Object();
		params.contentView = this.data.contentView;
		params.contentID = this.data.contentID;
		this.kinky.get(this, this.data.feService, params);
	};
	
	KComment.prototype.onLoad = function(data, request) {
		for ( var element in data) {
			for ( var att in data[element]) {
				this.data[att] = data[element][att];
			}
		}
		if (!this.activated()) {
			this.draw();
		}
	};
	
	KComment.prototype.removeComment = function(node) {
		var parent = node.parentNode;
		var nodeHTML = '<div class="' + parent.className + '"' + (parent.style.cssText && (typeof (parent.style.cssText) == 'string') ? ' style="' + parent.style.cssText + '"' : '') + '>' + parent.innerHTML.replace('clear: both; "', 'clear: both; "') + '</div>';
		parent.parentNode.removeChild(parent);
		this.removeCallback(this, nodeHTML);
	};
	
	KComment.prototype.addComment = function(params) {
		var commentDiv = window.document.createElement('div');
		commentDiv.className = ' KCommentEntry ';
		
		var header = window.document.createElement('h3');
		header.appendChild(window.document.createTextNode(params.name || (Kinky.getLoggedUser() ? Kinky.getLoggedUser().name : 'anonymous')));
		commentDiv.appendChild(header);
		
		var now = new Date();
		var date = window.document.createElement('div');
		date.id = 'date' + now.getTime();
		date.className = ' KCommentEntryDate ';
		date.appendChild(window.document.createTextNode(KSystem.formatDate(this.dateFormat, now)));
		commentDiv.appendChild(date);
		
		var img = window.document.createElement('img');
		img.className = ' KCommentEntryPhoto ';
		img.src = (Kinky.getLoggedUser() ? Kinky.getLoggedUser().photo : '');
		img.alt = img.title = Kinky.getLoggedUser().name;
		commentDiv.appendChild(img);
		
		var input = window.document.createElement('textarea');
		input.id = 'id' + now.getTime() + Kinky.getLoggedUser().id;
		input.className = ' KCommentEntryInput ';
		commentDiv.appendChild(input);
		
		if (this.hasAttach) {
			this.file = (Kinky.HTML_READY == 'html5' ? new KFileDropPanel(this) : new KFile(this, 'Anexo(s)', 'attach'));
			if (Kinky.HTML_READY == 'html5') {
				this.file.hash = '/file-panel';
				this.file.setTitle('Anexo(s)<div style="float: right; font-size: 12px; color: #AAAAAA; text-transform: lowercase;">&larr; drag from desktop</div>', true);
			}
			else {
				this.file.maxFile = this.maxAttachs;
			}
			this.appendChild(this.file, commentDiv);
			this.file.go();
		}
		
		var button = window.document.createElement('button');
		button.setAttribute('type', 'button');
		button.innerHTML = this.labels.go;
		button.className = ' KCommentEntryButton ';
		KDOM.addEventListener(button, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			var html = KComment.addHTML(event);
			if (!html) {
				return;
			}
			if (params && params.commentCallback) {
				params.commentCallback(widget, html);
			}
		});
		commentDiv.appendChild(button);
		
		var cancel = window.document.createElement('button');
		cancel.setAttribute('type', 'button');
		cancel.innerHTML = this.labels.cancel;
		cancel.className = ' KCommentEntryButtonCancel ';
		KDOM.addEventListener(cancel, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			if (widget.hasAttach) {
				widget.removeChild(widget.file);
			}
			commentDiv.parentNode.removeChild(commentDiv);
			if (params && params.cancelCallback) {
				params.cancelCallback(widget);
			}
		});
		commentDiv.appendChild(cancel);
		
		this.setStyle({
			clear : 'both'
		}, commentDiv);
		
		this.commentArea.appendChild(commentDiv);
	};
	
	KComment.addHTML = function(event) {
		var widget = KDOM.getEventWidget(event);
		var target = KDOM.getEventTarget(event);
		var parent = target.parentNode;
		var textarea = parent.getElementsByTagName('textarea')[0];
		var text = textarea.value;
		
		if (text == '') {
			return false;
		}
		
		var div = window.document.createElement('div');
		div.className = textarea.className;
		div.innerHTML = text.replace(/\n/g, '<br/>');
		
		parent.replaceChild(div, textarea);
		
		if (widget.hasAttach) {
			if (Kinky.HTML_READY != 'html5') {
				var files = widget.file.getValue();
				var names = widget.file.getFileName();
				
				if (files.length != 0) {
					var fileDiv = window.document.createElement('div');
					fileDiv.className = ' ' + widget.file.className + ' ' + widget.file.className + 'Readonly ';
					
					var label = window.document.createElement('label');
					label.appendChild(window.document.createTextNode('Anexo(s): '));
					fileDiv.appendChild(label);
					fileDiv.appendChild(KCSS.br());
					
					var ul = window.document.createElement('ul');
					
					for ( var file in files) {
						var li = window.document.createElement('li');
						var a = window.document.createElement('a');
						a.href = files[file].replace(Kinky.BASE_TEMP_DIR, Kinky.BASE_TEMP_URL);
						a.innerHTML = '&#183;&nbsp;' + names[file];
						a.target = '_blank';
						li.appendChild(a);
						ul.appendChild(li);
					}
					fileDiv.appendChild(ul);
					parent.replaceChild(fileDiv, widget.file.panel);
				}
				else {
					parent.removeChild(widget.file.panel);
				}
			}
			else {
				var droppedFiles = widget.file.getDropped();
				var files = [];
				
				for ( var file in droppedFiles) {
					var li = window.document.createElement('li');
					var a = window.document.createElement('a');
					a.href = droppedFiles[file].uploadURL;
					a.innerHTML = '&#183;&nbsp;' + droppedFiles[file].name;
					a.target = '_blank';
					li.appendChild(a);
					files.push(li);
				}
				if (files.length != 0) {
					var fileDiv = window.document.createElement('div');
					fileDiv.className = ' ' + widget.file.className + ' ' + widget.file.className + 'Readonly ';
					
					var label = window.document.createElement('label');
					label.appendChild(window.document.createTextNode('Anexo(s): '));
					fileDiv.appendChild(label);
					fileDiv.appendChild(KCSS.br());
					
					var ul = window.document.createElement('ul');
					for ( var file in files) {
						ul.appendChild(files[file]);
					}
					
					fileDiv.appendChild(ul);
					parent.replaceChild(fileDiv, widget.file.panel);
				}
				else {
					parent.removeChild(widget.file.panel);
				}
			}
			widget.removeChild(widget.file);
		}
		
		var buttons = parent.getElementsByTagName('button');
		for (; buttons.length != 0;) {
			parent.removeChild(buttons[0]);
		}
		
		if (widget.removeCallback) {
			parent.innerHTML += '<button id="button' + textarea.id + '" onclick="KDOM.getEventWidget(event).removeComment(this);">Apagar</button><script>if(Kinky.getLoggedUser().id != ' + Kinky.getLoggedUser().id + ') document.getElementById("button' + textarea.id + '").style.display = "none";</script>';
		}
		
		return '<div class="' + parent.className + '"' + (parent.style.cssText && (typeof (parent.style.cssText) == 'string') ? ' style="' + parent.style.cssText + '"' : '') + '>' + parent.innerHTML + '</div>';
	};
	
	KComment.prototype.draw = function() {
		
		if (this.data.info) {
			var info = window.document.createElement('div');
			{
				info.className = 'KCommentInfo ' + (this.data.info.cssClass || '') + ' ';
				
				var title = window.document.createElement('h2');
				{
					if (this.data.info.title != null) {
						title.appendChild(window.document.createTextNode(this.data.info.title));
					}
					else if (this.data.info.titleHTML != null) {
						title.innerHTML = this.data.info.titleHTML;
					}
					if (this.data.info.tooltip) {
						KDOM.addEventListener(title, 'mouseover', KComment.onTitleMouseOver);
					}
				}
				info.appendChild(title);
				
				var description = window.document.createElement('p');
				{
					if (this.data.info.description != null) {
						description.appendChild(window.document.createTextNode(this.data.info.description));
					}
					else if (this.data.info.descriptionHTML != null) {
						description.innerHTML = HTML.stripTags(this.data.info.descriptionHTML, 'p');
					}
				}
				info.appendChild(description);
			}
			this.infoArea.appendChild(info);
		}
		
		if (this.data.user) {
			if (this.data.user.photo) {
				for ( var photo in this.data.user.photo) {
					var image = window.document.createElement('img');
					image.src = this.data.user.photo[photo];
					image.alt = image.title = this.data.user.name || image.src.substr(image.src.lastIndexOf('/') + 1);
					this.infoArea.appendChild(image);
				}
			}
		}
		
		for ( var att in this.data) {
			if (this.exceptions.test(att) || (this.fieldInfoExceptions && this.fieldInfoExceptions.test(att))) {
				continue;
			}
			
			var infoField = this.getField(this.data, att);
			this.infoArea.appendChild(infoField);
			
		}
		
		this.infoArea.appendChild(KCSS.clearBoth());
		
		if (this.data.actions) {
			this.actions = window.document.createElement('div');
			{
				this.actions.className = 'KCommentActions';
				this.infoArea.appendChild(this.actions);
				
				for ( var att in this.data.actions) {
					var action = window.document.createElement('a');
					{
						action.href = this.getLink({
							hash : this.data.actions[att].hash || (this.getParent('KShellPart') ? this.getParent('KShellPart').hash : null),
							action : this.data.actions[att].url
						});
						action.className = 'KCommentInfoAction ' + (this.data.actions[att].cssClass || '') + ' ';
						if (this.data.actions[att].isHTML) {
							action.innerHTML = this.data.actions[att].label;
						}
						else {
							action.appendChild(window.document.createTextNode(this.data.actions[att].label));
						}
						if (this.data.actions[att].tooltip) {
							action.alt = this.data.actions[att].tooltip;
							KDOM.addEventListener(action, 'mouseover', KComment.onMouseOver);
						}
					}
					this.actions.appendChild(action);
				}
			}
		}
		
		if (this.data.reply != null) {
			this.commentArea.innerHTML = this.data.reply;
			var scripts = this.commentArea.getElementsByTagName('script');
			for ( var script = 0; script != scripts.length; script++) {
				try {
					eval(scripts[script].innerHTML.replace(/[\n\r\f\t]/g, ''));
				}
				catch (e) {
				}
			}
		}
		
		for ( var element in this.childWidgets()) {
			this.childWidget(element).go();
		}
		
		this.activate();
	};
	
	KComment.prototype.getInfoField = function(field) {
		return this.infoFields[field];
	};
	
	KComment.prototype.setInfoFieldText = function(field, value, isHTML) {
		var infoField = this.infoFields[field];
		if (infoField) {
			if (isHTML) {
				infoField.childNodes[1].innerHTML = value;
			}
			else {
				infoField.childNodes[1].removeChild(infoField.childNodes[1].childNodes[0]);
				infoField.childNodes[1].appendChild(window.document.createTextNode(value));
			}
		}
	};
	
	KComment.prototype.getField = function(data, field) {
		if (!data[field].value) {
			var infoField = window.document.createElement('div');
			infoField.className = 'KCommentInfoFieldPanel ' + (data[field].cssClass || '') + ' ';
			
			for ( var subField in data[field]) {
				if (this.exceptions.test(subField) || (this.fieldInfoExceptions && this.fieldInfoExceptions.test(subField))) {
					continue;
				}
				
				var subInfoField = this.getField(data[field], subField);
				infoField.appendChild(subInfoField);
			}
			infoField.appendChild(KCSS.clearBoth());
			
			this.infoFields[field] = infoField;
			
			return infoField;
		}
		else {
			var infoField = window.document.createElement('div');
			infoField.className = 'KCommentInfoField ' + (data[field].cssClass || '') + ' ';
			
			if (data[field].label) {
				var label = window.document.createElement('div');
				label.className = 'KCommentInfoFieldLabel';
				label.appendChild(window.document.createTextNode(data[field].label + ':'));
				infoField.appendChild(label);
			}
			
			var value = window.document.createElement('div');
			value.className = 'KCommentInfoFieldValue';
			if (data[field].isHTML) {
				value.innerHTML = data[field].value;
			}
			else {
				value.appendChild(window.document.createTextNode(data[field].value));
			}
			if (data[field].tooltip) {
				value.alt = data[field].tooltip;
				KDOM.addEventListener(value, 'mouseover', KComment.onMouseOver);
			}
			infoField.appendChild(value);
			
			this.infoFields[field] = infoField;
			
			return infoField;
		}
	};
	
	KComment.onMouseOver = function(event) {
		var target = KDOM.getEventTarget(event);
		if (!target.alt) {
			target = target.parentNode;
		}
		
		KTooltip.showTooltip(target, {
			text : target.alt,
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			cssClass : 'KCommentActionTooltip'
		});
	};
	
	KComment.onTitleMouseOver = function(event) {
		var target = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);
		
		KTooltip.showTooltip(target, {
			text : widget.data.info.tooltip,
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			cssClass : 'KCommentActionTooltip'
		}, event);
	};
	
	KComment.INFO_DIV = 'infoArea';
	KComment.COMMENT_DIV = 'commentArea';
	KComment.ACTIONS_DIV = 'actions';
	
	KSystem.included('KComment');
});
function KConfirmDialog(target, params) {
	if (target != null) {
		KFloatable.call(this, target, params);
	}
}
function KConfirmPanel(params) {
	if (params != null) {
		KPanel.call(this, Kinky.getShell(params.shell));
		this.config = params;
	}
}

KSystem.include([
	'KFloatable',
	'KButton',
	'KPanel'
], function() {
	KConfirmDialog.prototype = new KFloatable();
	KConfirmDialog.prototype.constructor = KConfirmDialog;
	
	KConfirmDialog.confirm = function(params) {
		params.modal = true;
		
		var target = new KConfirmPanel(params);
		var dialog = KFloatable.make(target, params);
		dialog.addCSSClass('KConfirmDialog');
		KFloatable.show(dialog);
	};
	
	KSystem.included('KConfirmDialog');
	
	KConfirmPanel.prototype = new KPanel();
	KConfirmPanel.prototype.constructor = KConfirmPanel;
	
	KConfirmPanel.prototype.onOpen = function() {
		this.childWidget('/buttons/ok').focus();
	};

	KConfirmPanel.prototype.invisible = function() {
		if (!!this.config.onClose) {
			this.config.onClose();
		}
		KPanel.prototype.invisible.call(this);
	};

	KConfirmPanel.prototype.load = function() {
		this.draw();
	};
	
	KConfirmPanel.prototype.draw = function() {
		var text = window.document.createElement('div');
		{
			if (!!this.config.html) {
				text.innerHTML = this.config.question;
			}
			else {
				text.appendChild(window.document.createTextNode(this.config.question));
			}
			text.className = ' KConfirmDialogQuestionText ';
		}
		this.container.appendChild(text);

		var nogo = new KButton(this, gettext('$KCONFIRMDIALOG_CANCEL_BUTTON_LABEL'), 'nogo', function(event) {
			KDOM.stopEvent(event);
			
			KDOM.getEventWidget(event).parent.parent.closeMe();
		});
		{
			nogo.hash = '/buttons/cancel';
			nogo.addCSSClass('KConfirmDialogCancelButton');
		}
		this.appendChild(nogo);

		var go = new KButton(this, gettext('$KCONFIRMDIALOG_OK_BUTTON_LABEL'), 'go', function(event) {
			KDOM.stopEvent(event);
			
			var panel = KDOM.getEventWidget(event).parent;
			panel.config.callback();
			panel.parent.closeMe();
		});
		{
			go.hash = '/buttons/ok';
			go.addCSSClass('KConfirmDialogOKButton');
		}
		this.appendChild(go);

		KPanel.prototype.draw.call(this);
	};
	
	KSystem.included('KConfirmPanel');
});
function KConnectionHTTP(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnection.call(this, url, dataURL, headers);
	}
}
{
	KConnectionsPool.registerConnector('https', 'KConnectionHTTP');
	KConnectionsPool.registerConnector('http', 'KConnectionHTTP');
	
	KConnectionHTTP.prototype = new KConnection();
	KConnectionHTTP.prototype.constructor = KConnectionHTTP;
	
	KConnectionHTTP.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		return channel;
	};
	
	KConnectionHTTP.prototype.getServiceParams = function(request, params, channel) {
		var ret = '?';
		for ( var paramName in params) {
			if (ret.length != 1) {
				ret += '&';
			}
			ret += paramName + '=' + encodeURIComponent(params[paramName]);
		}
		if (ret.length != 1) {
			ret += '&';
		}

		var req = {
			channel : channel,
			request : request
		};
		var index = KConnectionHTTP.stack.push(req) - 1;
		req.receive = function(data) {
			eval(request.callback[0] + '(data, request)' );
			channel.parentNode.removeChild(channel);
			KConnectionHTTP.stack.splice(index, 1);
			var widget = Kinky.getWidget(request.id);
			if (widget && widget.resume) {
				widget.resume(true);
			}
		};

		return ret + 'callback=' + encodeURIComponent('KConnectionHTTP.stack[' + index + '].receive');
	}; 
	
	KConnectionHTTP.prototype.send = function(request, params, headers) {
		params = params || {};

		var channel = this.connect(request);
		channel.charset = 'utf-8';

		var src = request.type + ':' + request.service + this.getServiceParams(request, params, channel);
		channel.src = src;
		
		window.document.getElementsByTagName('head')[0].appendChild(channel);
	};
	
	KConnectionHTTP.prototype.openChannel = function() {
		return window.document.createElement('script');
	};
	
	KConnectionHTTP.stack = [];

	KSystem.included('KConnectionHTTP');
}
function KConnectionJSON(url, dataURL, headers) {
	if (url && dataURL) {
		KConnection.call(this, url, dataURL, headers);
	}
}

{
	KConnectionsPool.registerConnector('js', 'KConnectionJSON');
	
	KConnectionJSON.prototype = new KConnection();
	KConnectionJSON.prototype.constructor = KConnectionJSON;
	
	KConnectionJSON.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var content = null;
		var url = null;
		var method = 'GET';
		
		var queryString = (request.service.indexOf('?') == -1 ? '' : request.service.substr(request.service.indexOf('?')));
		var file = (request.service.indexOf('?') == -1 ? request.service : request.service.substr(0, request.service.indexOf('?')));
		url = this.dataUrlBase + (request.namespace != ':' ? request.namespace + '/' : '') + file + KConnectionJSON.processPagination(queryString) + KConnectionJSON.JSON_FILE_EXTENSION + queryString;
		
		channel.open(method, url, true);
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KConnectionJSON.processPagination = function(query) {
		if ((query.indexOf('pageSize') != -1) && (query.indexOf('pageStartIndex') != -1)) {
			var pageSize = 0;
			var pageStartIndex = 0;
			var parts = query.substr(1).split('&');
			for ( var p in parts) {
				if (parts[p].indexOf('pageSize') != -1) {
					var vals = parts[p].split('=');
					pageSize = parseInt(vals[1]);
				}
				else if (parts[p].indexOf('pageStartIndex') != -1) {
					var vals = parts[p].split('=');
					pageStartIndex = parseInt(vals[1]);
				}
			}
			
			return Math.round(pageStartIndex / pageSize);
		}
		return '';
	};
	
	KConnectionJSON.JSON_FILE_EXTENSION = '.json';
	
	KSystem.included('KConnectionJSON');
}

function KConnectionJSONP(url, dataURL) {
	this.urlBase = url;
	this.dataUrlBase = dataURL;
	this.requestCount = 0;
	this.channels = {};
}

{
	
	KConnectionJSONP.prototype = new KConnection();
	KConnectionJSONP.prototype.constructor = KConnectionJSONP;
	
	KConnectionsPool.registerConnector('jsonp', 'KConnectionJSONP');
	
	KConnectionJSONP.prototype.connect = function(request, callback) {
		var channel = this.openChannel();
		return channel;
	};
	
	KConnectionJSONP.prototype.getServiceParams = function(request, params) {
		if (request) {
			if (params) {
				for ( var paramName in params) {
					request[paramName] = params[paramName];
				}
			}
			params.requestID = this.requestCount++;
			return 'id=' + params.requestID + '&request=' + encodeURIComponent(Base64.encode(JSON.stringify(request))) + '&callback=KConnection.getConnection(\'jsonp\').receive(' + params.requestID + ', $$)';
			
		}
		return null;
	};
	
	KConnectionJSONP.prototype.send = function(request, params, headers) {
		params = params || {};
		var accessToken = (!!headers && !!headers.access_token ? headers.access_token : KOAuth.getAccessToken());
		var src = this.urlBase + '?' + this.getServiceParams(request, params) + (!!accessToken ? '&access_token=' + accessToken : '');
		this.channels[params.requestID] = this.connect(request);
		this.channels[params.requestID].charset = 'utf-8';
		this.channels[params.requestID].src = src;
		
		window.document.getElementsByTagName('head')[0].appendChild(this.channels[params.requestID]);
	};
	
	KConnectionJSONP.prototype.openChannel = function() {
		return window.document.createElement('script');
	};
	
	KConnectionJSONP.prototype.receive = function(requestID, response) {
		var widget = Kinky.getWidget(response.id);
		
		if (response.httpStatus && (response.httpStatus != 200)) {
			this.processNonOk(widget, response);
			return;
		}
		
		Kinky.bunnyMan.loggedUser = response.user || Kinky.bunnyMan.loggedUser;
		if (widget != null) {
			if (widget.resume) {
				widget.resume(true);
			}
		}
		
		eval(response.callback + '(response, { href : this.channels[requestID].src });');
		
		window.document.getElementsByTagName('head')[0].removeChild(this.channels[requestID]);
		delete this.channels[requestID];
	};
	
	KSystem.included('KConnectionJSONP');
}

function KConnectionOAuth(url, dataURL, headers) {
	if (url && dataURL) {
		if (url.charAt(url.length - 1) == '/') {
			url = url.substring(0, url.length - 1);
		}
		KConnectionREST.call(this, url, dataURL, headers);
	}
}
KSystem.include([
	'KConnectionREST'
], function() {
	KConnectionsPool.registerConnector('oauth', 'KConnectionOAuth');
	
	KConnectionOAuth.prototype = new KConnectionREST();
	KConnectionOAuth.prototype.constructor = KConnectionOAuth;
	
	KSystem.included('KConnectionOAuth');
});
function KConnectionPHP(url, dataURL, headers) {
	if (url && dataURL) {
		KConnection.call(this, url, dataURL, headers);
	}
}

{
	KConnectionPHP.prototype = new KConnection();
	KConnectionPHP.prototype.constructor = KConnectionPHP;
	
	KConnectionsPool.registerConnector('php', 'KConnectionPHP');
	
	KConnectionPHP.prototype.send = function(request, params, headers) {
		var channel = this.connect(request);
		var method = 'POST';
		var url = this.urlBase;
		var content = this.getServiceParams(request, params);
		var setHeader = true;
		
		channel.open(method, url, true);
		if (setHeader) {
			channel.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		}
		this.setHeaders(headers, channel);
		channel.send(content);
	};
	
	KSystem.included('KConnectionPHP');
}

function KCSS() {
}
{
	KCSS.prototype.constructor = KCSS;
	KCSS.styles = new Object();
	
	KCSS.getCSS = function(regex) {
		if (typeof regex == 'string') {
			regex = new RegExp(regex);
		}
		for ( var k = window.document.styleSheets.length - 1; k != -1; k--) {
			var rules = window.document.styleSheets.item(k);
			try {
				for ( var r = 0; r != rules.cssRules.length; r++) {
					var rule = rules.cssRules[r];
					if (regex.test(rule.selectorText)) {
						return rule;
					}
				}
			}
			catch (e) {
			}
		}
	};
	
	KCSS.clearBoth = function(noIE) {
		var toReturn = window.document.createElement('div');
		if (!noIE) {
			toReturn.className = 'ClearBothIe';
		}
		toReturn.style.clear = 'both';
		return toReturn;
	};
	
	KCSS.br = function() {
		var toReturn = window.document.createElement('br');
		return toReturn;
	};
	
	KCSS.img = function(src, alt, title) {
		var toReturn = window.document.createElement('img');
		toReturn.src = src;
		if (alt) {
			toReturn.alt = alt;
		}
		if (title) {
			toReturn.title = title;
		}
		return toReturn;
	};
	
	KCSS.getComputedStyle = function(domElement) {
		var computedStyle;
		if (typeof domElement.currentStyle != 'undefined') {
			computedStyle = domElement.currentStyle;
		}
		else {
			computedStyle = document.defaultView.getComputedStyle(domElement, null);
		}
		
		return computedStyle;
	};
	
	KCSS.addCSSClass = function(cssClass, target) {
		target.className += ' ' + cssClass + ' ';
	};
	
	KCSS.removeCSSClass = function(cssClass, target) {
		var regex = new RegExp(' ' + cssClass + ' ', 'g');
		target.className = target.className.replace(regex, '');
	};
	
	KCSS.setStyle = function(cssStyle, domElements) {
		
		for ( var index in domElements) {
			var domElement = domElements[index];
			for ( var property in cssStyle) {
				if (cssStyle[property] == null) {
					continue;
				}
				switch (property) {
					case 'clear': {
						domElement.appendChild(KCSS.clearBoth());
						break;
					}
					case 'cssFloat': {
						if (KBrowserDetect.browser == 2) {
							domElement.style.cssFloat = '\'' + cssStyle[property] + '\'';
						}
						else {
							domElement.style.cssFloat = '\'' + cssStyle[property] + '\'';
						}
						break;
					}
					case 'opacity': {
						switch (KBrowserDetect.browser) {
							case 1:
								domElement.style.MozOpacity = cssStyle[property];
								domElement.style.opacity = cssStyle[property];
								break;
							case 2:
								if (!KCSS.NO_IE_OPACITY) {
									domElement.style.MsFilter = '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + Math.round(cssStyle[property] * 100) + ')"';
									domElement.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + Math.round(cssStyle[property] * 100) + ')';
								}
								domElement.style.opacity = cssStyle[property];
								break;
							case 3:
								domElement.style.opacity = cssStyle[property];
								break;
							case 4:
								domElement.style.KhtmlOpacity = cssStyle[property];
								domElement.style.opacity = cssStyle[property];
								break;
							case 5:
								domElement.style.opacity = cssStyle[property];
								break;
						}
						break;
					}
					default: {
						try {
							eval('domElement.style.' + property + ' = \'' + cssStyle[property] + '\'');
						}
						catch (err) {
							if (Kinky.DEV) {
								alert(property + "=" + cssStyle[property] + " " + err.toString());
							}
						}
						break;
					}
				}
			}
		}
	};
	
	KCSS.normalizBackgroundPosition = function(value) {
		var splited = value.split(/ /);
		
		return {
			x : KDOM.normalizePixelValue(splited[0]),
			y : KDOM.normalizePixelValue(splited[1])
		};
	};
	
	KCSS.NO_IE_OPACITY = false;
	
	KSystem.included('KCSS');
}
function KDate(parent, label, id, dateFormat, parseFormat) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.format = dateFormat;
		this.fparse = parseFormat;
		this.inputContainer = window.document.createElement('div');
		this.addDateElements();
		this.focused = -1;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KDate.prototype = new KAbstractInput();
	KDate.prototype.constructor = KDate;

	KDate.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		this.focused = 0;
		this.inputContainer.childNodes[this.focused].focus();
		this.parent.focus(this);
	};	

	KDate.prototype.selectNextWidget = function(up) {
		if ((up && this.focused == 0) || (!up && this.focused >= this.inputContainer.childNodes.length - 4)) {
			return KAbstractInput.prototype.selectNextWidget.call(this, up);
		}
		else {
			this.focused = (up ? this.focused - 2 : this.focused + 2);
			this.inputContainer.childNodes[this.focused].focus();
			return undefined;
		}
	};
	
	KDate.prototype.addDateElements = function() {
		var escape = false;
		var widget = this;
		var format = '' + this.format;
		var input = null;
		format.replace(/(\w+)|%/g, function(wholematch, match) {
			if (wholematch == '%') {
				escape = true;
				return '';
			}
			if (escape) {
				escape = false;
				return match;
			}
			switch (match) {
				case 'Y': {
					input = widget.addYear();
					break;
				}
				case 'M':
				case 'MM':
				case 'MMM': {
					input = widget.addMonth();
					break;
				}
				case 'D':
				case 'DD':
				case 'd': {
					input = widget.addDay();
					break;
				}
				case 'H': {
					input = widget.addHour();
					break;
				}
				case 'i': {
					input = widget.addMinute();
					break;
				}
				case 's': {
					input = widget.addSecond();
					break;
				}
				default: {
				}
			}
			
			if (!!input) {
				KDOM.addEventListener(input, 'focus', KDate.labelFocus);
				KDOM.addEventListener(input, 'blur', KDate.labelBlur);
				KDOM.addEventListener(input, 'change', KDate.valueChange);
				KDOM.addEventListener(input, 'keyup', KAbstractInput.onEnterPressed);
				KDOM.addEventListener(input, 'keydown', KAbstractInput.onTabPressed);
			}
			return match;
		});
	};

	KDate.prototype.setSeparator = function(cssClass) {
		var sep = window.document.createElement('i');
		sep.className = ' ' + cssClass + 'Separator ' + cssClass + 'Separator' + KLocale.LANG.toUpperCase() + ' KDateSeparator ';
		this.inputContainer.appendChild(sep);
	};

	KDate.prototype.addDay = function() {
		this.day = window.document.createElement('input');
		this.day.className += ' ' + this.className + 'Day ';
		this.day.name = 'day';
		this.day.maxLength = 2;
		this.day.value = 'DD';
		this.day.alt = 'DD';
		this.inputContainer.appendChild(this.day);
		this.setSeparator(this.className + 'Day');
		return this.day;
	};
	
	KDate.prototype.addMonth = function() {
		this.month = window.document.createElement('input');
		this.month.className += ' ' + this.className + 'Month ';
		this.month.name = 'month';
		this.month.maxLength = 2;
		this.month.value = 'MM';
		this.month.alt = 'MM';
		this.inputContainer.appendChild(this.month);
		this.setSeparator(this.className + 'Month');
		return this.month;
	};
	
	KDate.prototype.addYear = function() {
		this.year = window.document.createElement('input');
		this.year.className += ' ' + this.className + 'Year ';
		this.year.name = 'year';
		this.year.maxLength = 4;
		this.year.value = 'YYYY';
		this.year.alt = 'YYYY';
		this.inputContainer.appendChild(this.year);
		this.setSeparator(this.className + 'Year');
		return this.year;
	};
	
	KDate.prototype.addHour = function() {
		this.hour = window.document.createElement('input');
		this.hour.className += ' ' + this.className + 'Hour ';
		this.hour.name = 'hour';
		this.hour.value = 'hh';
		this.hour.alt = 'hh';
		this.inputContainer.appendChild(this.hour);
		this.setSeparator(this.className + 'Hour');
		return this.hour;
	};
	
	KDate.prototype.addMinute = function() {
		this.minute = window.document.createElement('input');
		this.minute.className += ' ' + this.className + 'Minute ';
		this.minute.name = 'minute';
		this.minute.value = 'mm';
		this.minute.alt = 'mm';
		this.inputContainer.appendChild(this.minute);
		this.setSeparator(this.className + 'Minute');
		return this.minute;
	};
	
	KDate.prototype.addSecond = function() {
		this.second = window.document.createElement('input');
		this.second.className += ' ' + this.className + 'Second ';
		this.second.name = 'second';
		this.second.value = 'ss';
		this.second.alt = 'ss';
		this.inputContainer.appendChild(this.second);
		this.setSeparator(this.className + 'Second');
		return this.second;
	};
	
	KDate.prototype.validate = function() {
		var regexValidation = KAbstractInput.prototype.validate.call(this);
		if (regexValidation != true) {
			return regexValidation;
		}
		
		if ((this.day.value == 'DD') && (this.month.value == 'MM') && (this.year.value == 'YYYY')) {
			return true;
		}
		
		// var date = new Date(1900, this.month.value, 1);
		var day = null;
		// var day = parseInt(this.day.value);
		
		if (this.day.value.charAt(0) == 0) {
			day = parseInt(this.day.value.charAt(1));
		}
		else {
			day = parseInt(this.day.value);
		}
		
		// var month = date.getMonth() || this.month.value;
		// alert(this.month.value.charAt(0));
		var month = null;
		if (this.month.value.charAt(0) == 0) {
			month = parseInt(this.month.value.charAt(1));
		}
		else {
			month = parseInt(this.month.value);
		}
		
		if (!month || (month < 1) || (month > 12)) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		if (!(/\d{4}/.test(this.year.value))) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		var availableDays = KDate.getDaysInMonth(month - 1, parseInt(this.year.value));
		if (!day || !(day < (availableDays + 1))) {
			this.addCSSClass('KAbstractInputError');
			return this.defaultErrorMessage || 'data inv\u00e1lida';
		}
		
		if (this.hasHours) {
			var hour = parseInt(this.hour.value);
			if (!hour || isNaN(hour) || (hour < 0) || (hour > 23)) {
				this.addCSSClass('KAbstractInputError');
				return this.defaultErrorMessage || 'hora inv\u00e1lida';
			}
			
			var minute = parseInt(this.minute.value);
			if (!minute || isNaN(minute) || (minute < 0) || (minute > 59)) {
				this.addCSSClass('KAbstractInputError');
				return this.defaultErrorMessage || 'minutos inv\u00e1lidos';
			}
		}
		
		this.removeCSSClass('KAbstractInputError');
		this.errorArea.innerHTML = '';
		return true;
	};
	
	KDate.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
	};
	
	KDate.prototype.getValue = function() {
		var ret = KAbstractInput.prototype.getValue.call(this);
		if (ret == '') {
			return ret;
		}
		return ret.date;
	};
	
	KDate.prototype.setValue = function(value, dontUpdate) {
		if (!value) {
			KAbstractInput.prototype.setValue.call(this, '');
			return;
		}

		if (typeof value == 'string') {
			value = KSystem.parseDate(!!this.fparse ? this.fparse : this.format, value);
		}
		
		if (!!this.day && (!dontUpdate || (dontUpdate && (this.day.value != this.day.alt)))) {
			var day = value.getDate();
			if (day < 10) {
				this.day.value = '0' + day;
			}
			else {
				this.day.value = day;
			}
		}
		if (!!this.month && (!dontUpdate || (dontUpdate && (this.month.value != this.month.alt)))) {
			var month = value.getMonth() + 1;
			if (month < 10) {
				this.month.value = '0' + month;
			}
			else {
				this.month.value = month;
			}
		}
		if (!!this.year && (!dontUpdate || (dontUpdate && (this.year.value != this.year.alt)))) {
			this.year.value = value.getFullYear();
		}
		if (!!this.hour && (!dontUpdate || (dontUpdate && (this.hour.value != this.hour.alt)))) {
			this.hour.value = value.getHours();
		}
		if (!!this.minute && (!dontUpdate || (dontUpdate && (this.minute.value != this.minute.alt)))) {
			this.minute.value = value.getMinutes();
		}
		if (!!this.second && (!dontUpdate || (dontUpdate && (this.second.value != this.second.alt)))) {
			this.second.value = value.getSeconds();
		}
		
		KAbstractInput.prototype.setValue.call(this, {
			date : value
		});
	};
	
	KDate.labelFocus = function(event) {
		var target = KDOM.getEventTarget(event);
		if ((target.value == null) || isNaN(parseInt(target.value))) {
			target.value = '';
		}
		else {
			target.select();
		}
	};
	
	KDate.labelBlur = function(event) {
		var target = KDOM.getEventTarget(event);
		if ((target.value == null) || (target.value == '')) {
			target.value = target.alt;
		}
	};
	
	KDate.valueChange = function(event) {
		var widget = KDOM.getEventWidget(event);
		var date = (widget.getValue() != '' ? new Date(widget.getValue()) : new Date());
		var hasAllValues = true;
		var hasNoValues = true;
		if (!!widget.day && (widget.day.value != '') && (widget.day.value != widget.day.alt)) {
			var day = 0;
			if (widget.day.value.indexOf('0') == 0) {
				day = parseInt(widget.day.value.replace('0', ''));
			}
			else {
				day = parseInt(widget.day.value);
			}
			date.setDate(day);
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.day;
		}
		if (!!widget.month && (widget.month.value != '') && (widget.month.value != widget.month.alt)) {
			var month = 0;
			if (widget.month.value.indexOf('0') == 0) {
				month = parseInt(widget.month.value.replace('0', ''));
			}
			else {
				month = parseInt(widget.month.value);
			}
			date.setMonth(month - 1);
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.month;
		}
		if (!!widget.year && (widget.year.value != '') && (widget.year.value != widget.year.alt)) {
			date.setFullYear(parseInt(widget.year.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.year;
		}
		if (!!widget.hour && (widget.hour.value != '') && (widget.hour.value != widget.hour.alt)) {
			date.setHours(parseInt(widget.hour.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.hour;
		}
		if (!!widget.minute && (widget.minute.value != '') && (widget.minute.value != widget.minute.alt)) {
			date.setMinutes(parseInt(widget.minute.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.minute;
		}
		if (!!widget.second && (widget.second.value != '') && (widget.second.value != widget.second.alt)) {
			date.setSeconds(parseInt(widget.second.value));
			hasNoValues = false;
		}
		else {
			hasAllValues = hasAllValues && !widget.second;
		}
		if (hasAllValues) {
			widget.setValue(date, true);
		}
		else if (hasNoValues) {
			widget.setValue(undefined);
		}
		KDOM.fireEvent(widget.input, 'change');
	};
	
	KDate.getDaysInMonth = function(month, year) {
		var m = [
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		];
		if (month != 1) {
			return m[month];
		}
		if (year % 4 != 0) {
			return m[1];
		}
		if ((year % 100 == 0) && (year % 400 != 0)) {
			return m[1];
		}
		return m[1] + 1;
	};
	
	KSystem.included('KDate');
});
function KDraggable() {
}
{	
	KDraggable.make = function(target, css) {
		target.dropped = function() {
			KDraggable.dragging = false;
			if (!!KDraggable.target) {
				KDraggable.target.droppedIt(this);
			}
		};

		KEffects.addEffect(target, {
			type : 'drag',
			dragndrop : true,
			onStart : function(widget, tween) {
				KDraggable.dragging = true;
			},
			onComplete : function(widget, tween) {
				widget.dropped();
				KDraggable.dragging = false;
			}
		});
	};
	
	KDraggable.dragging = false;
	KDraggable.target = null;
	
	KSystem.included('KDraggable');
}
function KDropPanel(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.droppedData = {};
	}
}

KSystem.include([
	'KDraggable',
	'KPanel'
], function() {
	KDropPanel.prototype = new KPanel();
	KDropPanel.prototype.constructor = KDropPanel;
	
	KDropPanel.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
		this.addEventListener('mouseover', KDropPanel.onMouseOver);
		this.addEventListener('mousemove', KDropPanel.onMouseOver);
		this.addEventListener('mouseout', KDropPanel.onMouseOut);
		this.addEventListener('mouseup', KDropPanel.onMouseOut);
	};
	
	KDropPanel.prototype.getDropped = function() {
		return this.droppedData;
	};
	
	KDropPanel.prototype.reset = function() {
		var h2 = null;
		if (this.content.getElementsByTagName('h2').length != 0) {
			h2 = this.content.getElementsByTagName('h2')[0];
		}
		this.content.innerHTML = '';
		if (!!h2) {
			this.content.appendChild(h2);
		}
		delete this.droppedData;
		this.droppedData = new Object();
	};
	
	KDropPanel.prototype.onDrop = function(widgetDropped) {
		this.droppedData[widgetDropped.id] = widgetDropped.data;
		var clone = null;
		if (!!widgetDropped.prepareDrop) {
			clone = widgetDropped.prepareDrop(this);
		}
		else {
			clone = widgetDropped.panel.cloneNode(true);
		}
		clone.name = widgetDropped.id;
		clone.id = null;

		var removeButton = window.document.createElement('button');
		removeButton.className = ' KDropPanelRemoveButton ';
		removeButton.name = widgetDropped.id;
		removeButton.setAttribute('type', 'button');
		removeButton.appendChild(window.document.createTextNode('\u00d7'));
		clone.style.overflow = 'visible';
		clone.appendChild(removeButton);
		KDOM.addEventListener(removeButton, 'click', KDropPanel.removeElement);

		KDOM.addEventListener(clone, 'mousedown', function(event) {
			clone.id = clone.name;
		});
		KDOM.addEventListener(clone, 'mouseup', function(event) {
			clone.id = null;
		});
		KEffects.addEffect(clone, {
			type : 'drag',
			dragndrop : true,
			applyToElement : true,
			onStart : function(widget, tween) {
				KDraggable.dragging = true;
			},
			onComplete : function(widget, tween) {
				tween.original.target.parentNode.removeChild(tween.original.target);
				widget.dropped();
				KDraggable.dragging = false;
			}
		}, widgetDropped);
		
		this.childDiv(KWidget.CONTENT_DIV).appendChild(clone);
	};
	
	KDropPanel.removeElement = function(event) {
		var target = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event, 'KDropPanel');
		
		widget.content.removeChild(target.parentNode);
		delete widget.droppedData[target.name];
	};
	
	KDropPanel.onMouseOver = function(event) {
		var widget = KDOM.getEventWidget(event);
		KDraggable.target = widget;
		if (KDraggable.dragging) {
			widget.addCSSClass(widget.className + 'Over');
		}
	};
	
	KDropPanel.onMouseOut = function(event) {
		var widget = KDOM.getEventWidget(event);
		var relTarget = KDOM.getEventRelatedTarget(event);
		var relWidget = KDOM.getWidget(relTarget);
		
		if (!KDraggable.dragging || ((relWidget != null) && (widget.id != relWidget.id) && (KDraggable.target.id == widget.id))) {
			KDraggable.target = null;
		}
		if (!!widget) {
			widget.removeCSSClass(widget.className + 'Over');
		}
	};
	
	KDropPanel.prototype.droppedIt = function(widgetDropped) {		
		if ((!this.droppedData[widgetDropped.id])) {
			this.onDrop(widgetDropped);
		}
	};
	
	KSystem.included('KDropPanel');
});
function KEffectBackgroundMove(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectBackgroundMove.prototype = new KEffect();
	KEffectBackgroundMove.prototype.constructor = KEffectBackgroundMove;
	
	KEffects.installEffect('background-move', 'KEffectBackgroundMove');
	
	KEffectBackgroundMove.prototype.config = function() {
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowMove();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		if (!!this.tween.from && !!this.tween.from.y) {
			this.tween.y = this.tween.from.y;
		}
		else {
			this.tween.y = parseInt(KDOM.normalizePixelValue(this.tween.target.style.backgroundPosition.split(' ')[1]));
			if (isNaN(this.tween.y)) {
				this.tween.y = 0;
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.x) {
			this.tween.x = this.tween.from.x;
		}
		else {
			this.tween.x = parseInt(KDOM.normalizePixelValue(this.tween.target.style.backgroundPosition.split(' ')[0]));
			if (isNaN(this.tween.x)) {
				this.tween.x = 0;
			}
		}
		
		this.tween.target.style.backgroundPosition = this.tween.x + 'px ' + this.tween.y + 'px';
		
		this.visible();
	};
	
	KEffectBackgroundMove.prototype.go = function() {
		if (!Kinky.ALLOW_EFFECTS) {
			this.tween.target.style.backgroundPosition = this.tween.go.x + 'px ' + this.tween.go.y + 'px';
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x || 0,
				y : this.tween.go.y || 0
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectBackgroundMove.prototype.slowMove = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (!this.tween.lock || !this.tween.lock.y) {
			this.tween.y = y;
		}
		if (!this.tween.lock || !this.tween.lock.x) {
			this.tween.x = x;
		}
		
		this.tween.target.style.backgroundPosition = this.tween.x + 'px ' + this.tween.y + 'px';
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectBackgroundMove');
});
function KEffectDrag(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectDrag.prototype = new KEffect();
	KEffectDrag.prototype.constructor = KEffectDrag;
	
	KEffects.installEffect('drag', 'KEffectDrag');
	
	KEffectDrag.prototype.config = function() {
		this.widget.drag = this;
		this.tween.target = this.tween.applyToElement ? this.tween.target : this.widget.panel;
		if (this.tween.dragndrop) {
			KDOM.addEventListener(this.element, 'mousedown', function(event) {
				var mouseEvent = KDOM.getEvent(event);
				
				if (mouseEvent.preventDefault) {
					mouseEvent.preventDefault();
				}
				else {
					mouseEvent.returnValue = false;
				}
				if (mouseEvent.button == 2) {
					return;
				}

				var effect = KDOM.getEventWidget(event).drag;
				if (!effect) {
					return;
				}
				var drag = effect.tween;
				var element = KDOM.getEventTarget(event);
				drag.stillDown = true;
				KDOM.addEventListener(element, 'mouseup', KEffectDrag.timeOutDown);
				
				KSystem.addTimer(function() {
					if (drag.stillDown) {
						KDOM.removeEventListener(element, 'mouseup', KEffectDrag.timeOutDown);
						KEffectDrag.startDrag(event);
					}
				}, 200);
			});
		}
		else {
			KDOM.addEventListener(this.element, 'mousedown', KEffectDrag.startDrag);
		}
		
	};
	
	KEffectDrag.prototype.go = function() {
	};
	
	KEffectDrag.timeOutDown = function(event) {
		var effect = KDOM.getEventWidget(event).drag;
		var drag = effect.tween;
		drag.stillDown = false;
	};
	
	KEffectDrag.startDrag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var effect = KDOM.getEventWidget(event).drag;
		var drag = effect.tween;
		KEffectDrag.dragging = effect;
		
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		if (mouseEvent.button == 2) {
			return;
		}

		drag.x = (mouseEvent.pageX ? mouseEvent.pageX : (window.document.documentElement ? mouseEvent.clientX + window.document.documentElement.scrollLeft : mouseEvent.clientX + window.document.body.scrollLeft));
		drag.y = (mouseEvent.pageY ? mouseEvent.pageY : (window.document.documentElement ? mouseEvent.clientY + window.document.documentElement.scrollTop : mouseEvent.clientY + window.document.body.scrollTop));
		
		if (drag.onStart != null) {
			drag.onStart(effect.widget, drag);
		}
		
		drag.animating = true;
		drag.original = {
			x : KDOM.normalizePixelValue(drag.target.style.left),
			y : KDOM.normalizePixelValue(drag.target.style.top),
			left : KDOM.normalizePixelValue(drag.target.style.left),
			top : KDOM.normalizePixelValue(drag.target.style.top)
		};
		
		var left = KDOM.normalizePixelValue(drag.target.style.left);
		var top = KDOM.normalizePixelValue(drag.target.style.top);
		if (drag.dragndrop) {
			left = effect.widget.offsetLeft();
			top = effect.widget.offsetTop();

			drag.original.target = drag.target;
			if (!!effect.widget.prepareDrag) {
				drag.target = effect.widget.prepareDrag();
			}
			else {
				drag.target = drag.target.cloneNode(true);
			}
			drag.target.style.position = 'fixed';
			drag.target.style.left = drag.x + 'px';
			drag.target.style.top = drag.y + 'px';
			drag.target.id = null;
			effect.widget.getShell().panel.appendChild(drag.target);
		}
		
		if (isNaN(left) && (!!drag.from && !!drag.from.x != null)) {
			drag.target.style.left = drag.from.x + 'px';
			left = drag.from.x;
		}
		if (isNaN(top) && (!!drag.from && drag.from.y != null)) {
			drag.target.style.top = drag.from.y + 'px';
			top = drag.from.y;
		}
		
		drag.left = left;
		drag.top = top;
		
		KDOM.addEventListener(window.document, 'mouseup', KEffectDrag.stopDrag);
		KDOM.addEventListener(window.document, 'mousemove', KEffectDrag.drag);
	};
	
	KEffectDrag.drag = function(event) {
		var effect = KEffectDrag.dragging;
		var mouseEvent = KDOM.getEvent(event);
		
		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		var drag = effect.tween;
		
		var y = null;
		var x = null;
		if (!drag.lock || !drag.lock.y) {
			y = (mouseEvent.pageY ? mouseEvent.pageY : (window.document.documentElement ? mouseEvent.clientY + window.document.documentElement.scrollTop : mouseEvent.clientY + window.document.body.scrollTop));
			var top = (parseInt(drag.target.style.top.replace(/px/, '')) + (y - drag.y));
			if (drag.rectangle) {
				if (top < drag.rectangle[0]) {
					top = drag.rectangle[0];
				}
				else if (top > drag.rectangle[2]) {
					top = drag.rectangle[2];
				}
			}
			
			drag.top = top;
			drag.target.style.top = drag.top + 'px';
		}
		if (!drag.lock || !drag.lock.x) {
			x = (mouseEvent.pageX ? mouseEvent.pageX : (window.document.documentElement ? mouseEvent.clientX + window.document.documentElement.scrollLeft : mouseEvent.clientX + window.document.body.scrollLeft));
			var left = (parseInt(drag.target.style.left.replace(/px/, '')) + (x - drag.x));
			if (drag.rectangle) {
				if (left < drag.rectangle[1]) {
					left = drag.rectangle[1];
				}
				else if (left > drag.rectangle[3]) {
					left = drag.rectangle[3];
				}
			}
			drag.left = left;
			drag.target.style.left = drag.left + 'px';
		}
		
		drag.x = x;
		drag.y = y;
		if (drag.onAnimate != null) {
			KSystem.addTimer(function() {
				drag.onAnimate(effect.widget, drag);
			}, 1);
		}
	};
	
	KEffectDrag.stopDrag = function(event) {
		var effect = KEffectDrag.dragging;
		var drag = effect.tween;
		
		if ((drag == null) || !drag.animating) {
			return;
		}
		
		KDOM.removeEventListener(window.document, 'mouseup', KEffectDrag.stopDrag);
		KDOM.removeEventListener(window.document, 'mousemove', KEffectDrag.drag);
		drag.animating = false;
		KEffectDrag.dragging = null;
		
		if (drag.onComplete != null) {
			drag.onComplete(effect.widget, drag);
		}
		if (drag.dragndrop) {
			drag.target.parentNode.removeChild(drag.target);
			drag.target = drag.original.target;
		}
	};
	
	KSystem.included('KEffectDrag');
});
function KEffectFade(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectFade.prototype = new KEffect();
	KEffectFade.prototype.constructor = KEffectFade;
	
	KEffects.installEffect('fade', 'KEffectFade');
	
	KEffectFade.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowFade();
			}
		};

		if (!!this.tween.go && (this.tween.go.alpha != null) && this.tween.go.alpha > 1) {
			this.tween.go.alpha = 1;
		}

		if (!!this.tween.from && (this.tween.from.alpha != null)) {
			this.widget.setStyle({
				opacity : this.tween.from.alpha
			}, this.tween.target);
		}
		else {
			var value = undefined;
			if (!!this.tween.target.style.opacity && this.tween.target.style.opacity != '') {
				value = parseFloat(this.tween.target.style.opacity);
			}
			else {
				value = this.tween.go.alpha ^ 1;
			}

			this.tween.from = {
				alpha :  value
			}
		}
		this.visible();
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
	};
	
	KEffectFade.prototype.go = function() {
		var id = this.id;
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'opacity',
				'WebkitTransitionProperty' : 'opacity',
				'OTransitionProperty' : 'opacity'
			};
			
			style.opacity = this.tween.go.alpha;
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KCSS.setStyle(style, [
						KEffects.effecting[id].tween.target
					]);
				}
			}, 15);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.from.alpha) : this.tween.from.alpha
			},
			go : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.go.alpha) : this.tween.go.alpha
			},
			from : {
				alpha : this.tween.fakeAlpha ? 1 - (this.tween.from.alpha) : this.tween.from.alpha
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectFade.prototype.slowFade = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var opacity = Math.round(this.fValue.value.alpha * Math.pow(10, 2)) / Math.pow(10, 2);
		this.fValue.value.alpha = opacity;
		
		this.widget.setStyle({
			opacity : opacity
		}, this.tween.target);
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectFade');
});
function KEffectFontResize(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectFontResize.prototype = new KEffect();
	KEffectFontResize.prototype.constructor = KEffectFontResize;
	
	KEffects.installEffect('font-resize', 'KEffectFontResize');
	
	KEffectFontResize.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.fontSize) {
			this.widget.setStyle({
				fontSize : '' + this.tween.from.fontSize
			}, this.tween.target);
			this.tween.fontSize = this.tween.from.fontSize;
		}
		
		this.visible();
		
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowResize();
			}
		};
		
		this.tween.unit = this.tween.unit || 'em';
		
		this.tween.original = {
			overflow : this.tween.target.style.overflow,
			position : this.tween.target.style.position
		};
		
		this.tween.fontSize = parseInt(this.tween.target.style.fontSize.replace(/px|pt|em/, ''));
		if (isNaN(this.tween.fontSize) || (this.tween.fontSize == null)) {
			this.tween.fontSize = 8;
		}
		
	};
	
	KEffectFontResize.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'font-size',
				'WebkitTransitionProperty' : 'font-size',
				'OTransitionProperty' : 'font-size'
			};
			
			style.fontSize = this.tween.go.fontSize + this.tween.unit;
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			this.tween.target.style.fontSize = this.tween.go.fontSize + this.tween.unit;
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				fontSize : this.tween.fontSize
			},
			go : {
				fontSize : this.tween.go.fontSize
			},
			from : {
				fontSize : this.tween.fontSize
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectFontResize.prototype.slowResize = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var fontSize = this.fValue.value.fontSize;
		
		this.tween.fontSize = fontSize;
		this.tween.target.style.fontSize = this.tween.fontSize + this.tween.unit;
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectFontResize');
});
function KEffect(widget, tween, element) {
	if (widget) {
		this.widget = widget;
		this.type = tween.type;
		this.tween = tween;
		this.tween.delay = this.tween.delay == null ? 0 : this.tween.delay;
		this.stop = false;
		this.element = element;
		this.id = null;
		this.timerCode = function() {
		};
	}
}

{
	KEffect.prototype.constructor = KEffect;
	
	KEffect.prototype.config = function() {
	};
	
	KEffect.prototype.go = function() {
	};
	
	KEffect.prototype.visible = function() {
		this.widget.visible();
	};
	
	KEffect.getEventEffect = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		var effect = null;
		while ((effect = KEffects.getEffectByElement(element)) == null) {
			element = element.parentNode;
			if (element == null) {
				break;
			}
		}
		
		return effect;
	};
	
	KEffect.getEffect = function(domElement) {
		var element = domElement;
		var effect = null;
		while ((effect = KEffects.getEffectByElement(element)) == null) {
			element = element.parentNode;
			if (element == null) {
				break;
			}
		}
		
		return effect;
	};
	
	KEffect.prototype.cancel = function(noOnComplete) {
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var id = this.id;
			KCSS.setStyle({
				'MozTransition' : 'none',
				'WebkitTransition' : 'none',
				'OTransition' : 'none'
			}, [
				KEffects.effecting[id].tween.target
			]);
		}
		
		this.stop = true;
		this.cancel = function() {
			return;
		};
		
		var tween = this.tween;
		var widget = this.widget;
		KEffects.stopEffect(this);
		if (!noOnComplete && (tween.onComplete != null)) {
			tween.onComplete(widget, tween);
		}
		delete tween;
	};
	
	KSystem.included('KEffect');
}
function KEffectMove(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectMove.prototype = new KEffect();
	KEffectMove.prototype.constructor = KEffectMove;
	
	KEffects.installEffect('move', 'KEffectMove');
	
	KEffectMove.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		this.yAxis = this.tween.gravity && this.tween.gravity.y ? this.tween.gravity.y : 'top';
		this.xAxis = this.tween.gravity && this.tween.gravity.x ? this.tween.gravity.x : 'left';
		
		this.timerCode = function() {
			if (!!KEffects.effecting[id] && !KEffects.effecting[id].stop) {
				KEffects.effecting[id].slowMove();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		if (!!this.tween.from && !!this.tween.from.x) {
			eval('this.tween.target.style.' + this.xAxis + ' = \'' + this.tween.from.x + '' + this.tween.unit + '\';');
			this.tween.x = this.tween.from.x;
		}
		else {
			eval('this.tween.x = parseInt(KDOM.normalizePixelValue(this.tween.target.style.' + this.xAxis + '));');
			if (isNaN(this.tween.x)) {
				this.tween.x = 0;
				if (!this.tween.lock || !this.tween.lock.x) {
					eval('this.tween.target.style.' + this.xAxis + ' = \'0\';');
				}
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.y) {
			eval('this.tween.target.style.' + this.yAxis + ' = \'' + this.tween.from.y + '' + this.tween.unit + '\';');
			this.tween.y = this.tween.from.y;
		}
		else {
			eval('this.tween.y = parseInt(KDOM.normalizePixelValue(this.tween.target.style.' + this.yAxis + '));');
			if (isNaN(this.tween.y)) {
				this.tween.y = 0;
				if (!this.tween.lock || !this.tween.lock.y) {
					eval('this.tween.target.style.' + this.yAxis + ' = \'0\';');
				}
			}
		}
		
		this.visible();
		
	};
	
	KEffectMove.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : this.yAxis + ',' + this.xAxis,
				'WebkitTransitionProperty' : this.yAxis + ',' + this.xAxis,
				'OTransitionProperty' : this.yAxis + ',' + this.xAxis
			};
			
			if (!this.tween.lock || !this.tween.lock.x) {
				eval('this.tween.target.style.' + this.yAxis + ' = this.tween.go.y + \'px\';');
			}
			
			if (!this.tween.lock || !this.tween.lock.y) {
				eval('this.tween.target.style.' + this.xAxis + ' = this.tween.go.x + \'px\';');
			}
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (!this.tween.lock || !this.tween.lock.y) {
				eval('this.tween.target.style.' + this.yAxis + ' = this.tween.go.y + \'px\';');
			}
			if (!this.tween.lock || !this.tween.lock.x) {
				eval('this.tween.target.style.' + this.xAxis + ' = this.tween.go.x + \'px\';');
			}
			this.tween.y = this.tween.go.y;
			this.tween.x = this.tween.go.x;
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration + 10,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x || 0,
				y : this.tween.go.y || 0
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectMove.prototype.slowMove = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (!this.tween.lock || !this.tween.lock.y) {
			this.tween.y = y;
			eval('this.tween.target.style.' + this.yAxis + ' = Math.ceil(this.tween.y) + \'px\';');
		}
		if (!this.tween.lock || !this.tween.lock.x) {
			this.tween.x = x;
			eval('this.tween.target.style.' + this.xAxis + ' = Math.ceil(this.tween.x) + \'px\';');
		}
		
		if (!this.fValue.stop && !this.stop) {
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectMove');
});
function KEffectResize(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectResize.prototype = new KEffect();
	KEffectResize.prototype.constructor = KEffectResize;
	
	KEffects.installEffect('resize', 'KEffectResize');
	
	KEffectResize.prototype.config = function() {
		var id = this.id;
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowResize();
			}
		};
		
		this.tween.unit = this.tween.unit || 'px';
		
		this.tween.original = {
			overflow : this.tween.target.style.overflow,
			position : this.tween.target.style.position
		};
		
		this.tween.gravity = this.tween.gravity || {
			yAxis : 'height',
			xAxis : 'width'
		};
		
		if (!!this.tween.from && !!this.tween.from.width) {
			this.tween.width = this.tween.from.width;
			this.tween.target.style.width = this.tween.width + 'px';
		}
		else {
			this.tween.width = parseInt(this.tween.target.style.width.replace(/px/, ''));
			if (isNaN(this.tween.width) || (this.tween.width == null)) {
				this.tween.width = 0;
				if (!this.tween.lock || !this.tween.lock.width) {
					this.tween.target.style.width = '0px';
				}
			}
		}
		
		if (!!this.tween.from && !!this.tween.from.height) {
			this.tween.height = this.tween.from.height;
			this.tween.target.style.height = this.tween.height + 'px';
		}
		else {
			this.tween.height = parseInt(this.tween.target.style.height.replace(/px/, ''));
			if (isNaN(this.tween.height) || (this.tween.height == null)) {
				this.tween.height = 0;
				if (!this.tween.lock || !this.tween.lock.height) {
					this.tween.target.style.height = '0px';
				}
			}
		}
		
		this.visible();
	};
	
	KEffectResize.prototype.go = function() {
		var id = this.id;
		
		if (this.tween.lock && this.tween.lock.height) {
			this.tween.height = this.tween.go.height = 0;
		}
		if (this.tween.lock && this.tween.lock.width) {
			this.tween.width = this.tween.go.width = 0;
		}
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'width,height',
				'WebkitTransitionProperty' : 'width,height',
				'OTransitionProperty' : 'width,height'
			};
			
			if (!this.tween.lock || !this.tween.lock.width) {
				style.width = this.tween.go.width + 'px';
			}
			
			if (!this.tween.lock || !this.tween.lock.height) {
				style.height = this.tween.go.height + 'px';
			}
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			if (!this.tween.lock || !this.tween.lock.width) {
				this.tween.target.style.width = this.tween.go.width + this.tween.unit;
			}
			if (!this.tween.lock || !this.tween.lock.height) {
				this.tween.target.style.height = this.tween.go.height + this.tween.unit;
			}
			
			this.widget.setStyle(this.tween.original, this.tween.target);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				width : this.tween.width,
				height : this.tween.height
			},
			go : {
				width : this.tween.go.width || 0,
				height : this.tween.go.height || 0
			},
			from : {
				width : this.tween.width,
				height : this.tween.height
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectResize.prototype.slowResize = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var width = this.fValue.value.width = Math.round(this.fValue.value.width);
		var height = this.fValue.value.height = Math.round(this.fValue.value.height);
		
		if (!this.tween.lock || !this.tween.lock.width) {
			this.tween.width = width;
			this.tween.target.style.width = Math.ceil(this.tween.width) + this.tween.unit;
		}
		if (!this.tween.lock || !this.tween.lock.height) {
			this.tween.height = height;
			this.tween.target.style.height = Math.ceil(this.tween.height) + this.tween.unit;
		}
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectResize');
});
function KEffectRGB(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectRGB.prototype = new KEffect();
	KEffectRGB.prototype.constructor = KEffectRGB;
	
	KEffects.installEffect('rgb', 'KEffectRGB');
	
	KEffectRGB.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowRGB();
			}
		};
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.rgb) {
			this.tween.color = this.tween.from.rgb;
			
			var color_info_from = KDOM.extractColorInfo(this.tween.color);
			this.tween.red = color_info_from.red;
			this.tween.green = color_info_from.green;
			this.tween.blue = color_info_from.blue;
			
			this.tween.target.style.color = this.tween.from.rgb;
		}
		else {
			this.tween.color = this.tween.target.style.color || '#000000';
			
			var color_info_from = KDOM.extractColorInfo(this.tween.color);
			this.tween.red = color_info_from.red;
			this.tween.green = color_info_from.green;
			this.tween.blue = color_info_from.blue;
		}
		
		if (!!this.tween.go.rgb) {
			var color_info_to = KDOM.extractColorInfo(this.tween.go.rgb);
			this.tween.go.red = color_info_to.red;
			this.tween.go.green = color_info_to.green;
			this.tween.go.blue = color_info_to.blue;
			
			delete this.tween.go.rgb;
		}
		
		this.visible();
	};
	
	KEffectRGB.prototype.go = function() {
		var id = this.id;
		
		if ((this.tween.cssReady && (this.tween.cssReady == 'css3')) || ((!this.cssReady || (this.cssReady != 'css2')) && (Kinky.CSS_READY == 'css3'))) {
			var style = {
				'MozTransitionDuration' : this.tween.duration + 'ms',
				'WebkitTransitionDuration' : this.tween.duration + 'ms',
				'OTransitionDuration' : this.tween.duration + 'ms',
				'MozTransitionTimingFunction' : this.tween.f,
				'WebkitTransitionTimingFunction' : this.tween.f,
				'MozTransitionProperty' : 'opacity',
				'WebkitTransitionProperty' : 'opacity',
				'OTransitionProperty' : 'opacity'
			};
			
			style.color = 'rgb(' + this.tween.go.red + ', ' + this.tween.go.green + ', ' + this.tween.go.blue + ')';
			KCSS.setStyle(style, [
				this.tween.target
			]);
			
			KSystem.addTimer(function() {
				if (!!KEffects.effecting[id]) {
					KEffects.effecting[id].cancel();
				}
			}, this.tween.duration + 20);
			return;
		}
		
		if (!Kinky.ALLOW_EFFECTS) {
			this.widget.setStyle({
				color : 'rgb(' + this.tween.go.red + ', ' + this.tween.go.green + ', ' + this.tween.go.blue + ')'
			}, this.tween.target);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				red : this.tween.red,
				green : this.tween.green,
				blue : this.tween.blue
			},
			go : {
				red : this.tween.go.red,
				green : this.tween.go.green,
				blue : this.tween.go.blue
			},
			from : {
				red : this.tween.red,
				green : this.tween.green,
				blue : this.tween.blue
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectRGB.prototype.slowRGB = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var red = this.fValue.value.red = Math.round(this.fValue.value.red);
		var green = this.fValue.value.green = Math.round(this.fValue.value.green);
		var blue = this.fValue.value.blue = Math.round(this.fValue.value.blue);
		
		this.widget.setStyle({
			color : 'rgb(' + red + ', ' + green + ', ' + blue + ')'
		}, this.tween.target);
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectRGB');
});
function KEffects() {
}
{
	KEffects.prototype.constructor = KEffects;
	
	KEffects.effecting = new Object();
	KEffects.installed = new Object();
	
	KEffects.installEffect = function(typeName, className) {
		KEffects.installed[typeName] = className;
	};
	
	KEffects.stopEffect = function(effect) {
		delete KEffects.effecting[effect.id];
	};
	
	KEffects.cancelEffect = function(elementOrID, effectType) {
		if (!!effectType) {
			if (KEffects.effecting[elementOrID.id + effectType]) {
				KEffects.effecting[elementOrID.id + effectType].cancel();
			}
		}
		else {
			if (KEffects.effecting[elementOrID]) {
				KEffects.effecting[elementOrID].cancel();
			}
		}
	};
	
	KEffects.getEffectByElement = function(element, effectType) {
		if (!element || !element.id) {
			return null;
		}
		return KEffects.effecting[element.id + effectType];
	};
	
	KEffects.getEffectById = function(id) {
		return KEffects.effecting[id];
	};
	
	KEffects.addEffect = function(element, tweenToAdd, targetW) {
		var widget = targetW || ((element instanceof KWidget || (!!element.className && KSystem.isAddOn(element.className))) ? element : KDOM.getWidget(element, tweenToAdd.targetClass));
		var tweens = null;
		
		if (tweenToAdd instanceof Array) {
			tweens = tweenToAdd;
		}
		else {
			tweens = new Array();
			tweens.push(tweenToAdd);
		}
		
		var includes = [];
		for ( var index in tweens) {
			includes.push(KEffects.installed[tweens[index].type]);
		}
		
		KSystem.include(includes, function() {
			if (!element || element == undefined) {
				return;
			}

			for ( var index in tweens) {
				var tween = tweens[index];
				var elementid = null;
				
				if (element instanceof KWidget) {
					tween.target = widget.panel;
					elementid = element.id;
				}
				else {
					tween.target = element;
					if (!element.id || (element.id == '')) {
						if (!widget || widget == undefined) {
							return;
						}
						elementid = 'child' + widget.id.replace(/widget/, '') + '.' + widget.childrenid++;
						element.id = elementid;
					}
					else {
						elementid = element.id;
					}
					
				}
				var effectid = elementid + tween.type;
				tween.id = effectid;
				
				if (!!KEffects.effecting[effectid]) {
					KEffects.effecting[effectid].cancel(true);
				}
				
				KEffects.loadEffect(tween, element, widget);
			}
		});
	};
	
	KEffects.loadEffect = function(tween, element, widget) {
		var effect = null;
		
		if (tween.type != 'drag') {
			if (!tween.duration) {
				throw new KException('KEffects', 'loadEffect: "duration" parameter must be a number greater than 0');
			}
			if (!tween.go) {
				throw new KException('KEffects', 'loadEffect: the "go" parameter must be instantiated');
			}
		}

		eval('effect = new ' + KEffects.installed[tween.type] + '(widget, tween, element);');
		if (!effect.tween) {
			return;
		}
		effect.id = effect.tween.id;
		KEffects.effecting[effect.id] = effect;
		effect.start = (new Date()).getTime();
		effect.config();
		effect.go();
	};
	
	KEffects.loadTimmingFunctions = function() {
		KEffects.linear = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			// c*t/d + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * t / d + b;
			}
			return toReturn;
		} : 'linear';
		
		KEffects.easeOutCubic = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			// c*t/d + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * ((t = t / d - 1) * t * t + 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutQuart = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// -c * ((t=t/d-1)*t*t*t - 1) + b
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : -c * ((t = t / d - 1) * t * t * t - 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutSine = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// c * Math.sin(t/d * (Math.PI/2)) + b;
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * Math.sin(t / d * (Math.PI / 2)) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeOutExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			// (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) +
			// 1) + b;
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : c * (-Math.pow(2, -10 * t / d) + 1) + b;
			}
			return toReturn;
		} : 'ease-out';
		
		KEffects.easeInExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			var t = now - time;
			var d = duration;
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				toReturn.value[index] = stop ? targetValue[index] : (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
			}
			
			return toReturn;
		} : 'ease-in';
		
		KEffects.easeInOutExpo = Kinky.CSS_READY != 'css3' ? function(duration, time, targetValue, initialValue, currentValue, fArgs) {
			var fps = 1000 / (fArgs.fps * 2);
			var now = (new Date()).getTime();
			var stop = now >= time + duration;
			
			var toReturn = {
				stop : stop,
				step : Math.round(fps),
				time : time,
				duration : duration,
				go : targetValue,
				from : initialValue,
				fArgs : fArgs
			};
			toReturn.value = {};
			
			var t = now - time;
			var d = duration;
			var n_t = (t / (d / 2));
			for ( var index in targetValue) {
				var c = (targetValue[index] - initialValue[index]);
				var b = initialValue[index];
				
				if (stop) {
					toReturn.value[index] = targetValue[index];
				}
				else if (t == 0) {
					toReturn.value[index] = b;
				}
				else if (t == d) {
					toReturn.value[index] = b + c;
				}
				else if (n_t < 1) {
					toReturn.value[index] = c / 2 * Math.pow(2, 10 * (n_t - 1)) + b;
				}
				else {
					toReturn.value[index] = c / 2 * (-Math.pow(2, -10 * --n_t) + 2) + b;
				}
			}
			
			return toReturn;
		} : 'ease-in-out';
		
	};
	
	KEffects.installEffect('background-move', 'KEffectBackgroundMove');
	KEffects.installEffect('drag', 'KEffectDrag');
	KEffects.installEffect('fade', 'KEffectFade');
	KEffects.installEffect('font-resize', 'KEffectFontResize');
	KEffects.installEffect('move', 'KEffectMove');
	KEffects.installEffect('resize', 'KEffectResize');
	KEffects.installEffect('rgb', 'KEffectRGB');
	KEffects.installEffect('window-scroll-to', 'KEffectWindowScrollTo');
	
	KSystem.included('KEffects');
}

function KEffectWindowScrollTo(widget, tween, element) {
	if (widget) {
		KEffect.call(this, widget, tween, element);
	}
}

KSystem.include([
	'KEffect'
], function() {
	KEffectWindowScrollTo.prototype = new KEffect();
	KEffectWindowScrollTo.prototype.constructor = KEffectWindowScrollTo;
	
	KEffects.installEffect('window-scroll-to', 'KEffectWindowScrollTo');
	
	KEffectWindowScrollTo.prototype.config = function() {
		var id = this.id;
		this.timerCode = function() {
			if (KEffects.effecting[id]) {
				KEffects.effecting[id].slowScrollTo();
			}
		};
		
		if (this.tween.onStart != null) {
			this.tween.onStart(this.widget, this.tween);
		}
		
		if (!!this.tween.from && !!this.tween.from.x) {
			this.tween.x = this.tween.from.x;
		}
		if (!!this.tween.from && !!this.tween.from.y) {
			this.tween.y = this.tween.from.y;
		}
		
		if (this.tween.applyToElement) {
			this.tween.x = this.element.scrollLeft;
			this.tween.y = this.element.scrollTop;
		}
		else {
			this.tween.x = window.pageXOffset || window.scrollLeft || document.body.scrollLeft || 0;
			this.tween.y = window.pageYOffset || window.scrollTop || document.body.scrollTop || 0;
		}
	};
	
	KEffectWindowScrollTo.prototype.go = function() {
		if (!Kinky.ALLOW_EFFECTS) {
			window.scrollTo(this.tween.go.x || 0, this.tween.go.y || 0);
			if (this.tween.onAnimate != null) {
				this.tween.onAnimate(this.widget, this.tween);
			}
			this.cancel();
			return;
		}
		
		this.fValue = {
			stop : false,
			duration : this.tween.duration,
			time : (new Date()).getTime() + this.tween.delay,
			step : 0,
			value : {
				x : this.tween.x,
				y : this.tween.y
			},
			go : {
				x : this.tween.go.x,
				y : this.tween.go.y
			},
			from : {
				x : this.tween.x,
				y : this.tween.y
			},
			fArgs : {
				fps : this.tween.fps || 24
			}
		};
		
		KSystem.addTimer(this.timerCode, this.tween.delay);
	};
	
	KEffectWindowScrollTo.prototype.slowScrollTo = function() {
		if (this.fValue.stop || this.stop) {
			this.cancel();
			return;
		}
		
		this.fValue = this.tween.f(this.fValue.duration, this.fValue.time, this.fValue.go, this.fValue.from, this.fValue.value, this.fValue.fArgs);
		var x = this.fValue.value.x = Math.round(this.fValue.value.x);
		var y = this.fValue.value.y = Math.round(this.fValue.value.y);
		
		if (this.tween.applyToElement) {
			this.element.scrollTop = y;
			this.element.scrollLeft = x;
		}
		else {
			window.scrollTo(x || 0, y || 0);
		}
		
		if (this.tween.onAnimate != null) {
			this.tween.onAnimate(this.widget, this.tween);
		}
		
		if (!this.fValue.stop && !this.stop) {
			KSystem.addTimer(this.timerCode, this.fValue.step);
		}
		else {
			this.cancel();
		}
	};
	
	KSystem.included('KEffectWindowScrollTo');
});
function KException(invoker, message) {
	if (invoker != null) {
		if (typeof invoker == 'string') {
			this.message = invoker + '.' + message;
		}
		else {
			this.message = (KSystem.getObjectClass(invoker)) + (!!invoker.id ? '[' + invoker.id + ']' : '') + '.' + message;
		}
	}
}
{
	KException.prototype = new Error();
	KException.prototype.constructor = KException;
	
	KException.prototype.printStackTrace = function() {
		var callstack = new Array();
		
		if (this.stack) { // Firefox
			var lines = this.stack.split("\n");
			for ( var i = 0, len = lines.length; i != len; i++) {
				if (lines[i] != '') {
					callstack.push(lines[i]);
				}
			}
		}
		else if (window.opera && this.message) { // Opera
			var lines = this.message.split("\n");
			for ( var i = 0, len = lines.length; i != len; i++) {
				if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
					var entry = lines[i];
					if (lines[i + 1]) {
						entry += " at " + lines[i + 1];
						i++;
					}
					callstack.push(entry);
				}
			}
		}
		else {
			var currentFunction = arguments.callee.caller;
			while (currentFunction) {
				var fn = currentFunction.toString();
				var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf("(")) || "anonymous";
				callstack.push(fname);
				currentFunction = currentFunction.caller;
			}
		}
		
		var list = window.document.createElement('ol');
		for ( var index in callstack) {
			var error = list.appendChild(window.document.createElement('li'));
			error.appendChild(window.document.createTextNode(callstack[index]));
		}
		return list;
	};
	
	KSystem.included('KException');
}

function KNoExtensionException(invoker, methodName) {
	if (invoker) {
		KException.call(this, invoker, KSystem.getObjectClass(invoker) + '[' + invoker.id + ']' + ': ' + methodName + ': child class must implement this method.');
	}
}
{
	KNoExtensionException.prototype = new KException();
	KNoExtensionException.prototype.constructor = KNoExtensionException;
	
	KSystem.included('KNoExtensionException');
}
function KFBConnect(parent) {
	if (parent != null) {
		KButton.call(this, parent, '', 'fb-connect', KFBConnect.connect, 'button');
		this.addActionListener(KFBConnect.actions, [
			'/fb-connect'
		]);
	}
}

KSystem.include([
	'KButton'
], function() {
	KFBConnect.prototype = new KButton();
	KFBConnect.prototype.constructor = KFBConnect;
	
	KFBConnect.prototype.go = function() {
		if (!this.activated()) {
			this.load();
		}
		else {
			this.redraw();
		}
	};
	
	KFBConnect.prototype.load = function() {
		KFBOpenGraph.queryFB(this, {
			hash : '/me',
			args : {
				fields : 'id,first_name,last_name,name,link,about,birthday,work,education,email,website,hometown,location,bio,quotes,gender,interested_in,meeting_for,relationship_status,religion,political,verified,significant_other,timezone,picture',
				type : 'small'
			}
		}, 'onLoad');
	};
	
	KFBConnect.prototype.onLoad = function(data, request) {
		if (data.error) {
		}
		else {
			Kinky.setLoggedFB(data);
			this.onAuthorize({
				code : Kinky.getLoggedUser() ? 200 : 401
			});
		}
		this.draw();
	};
	
	KFBConnect.prototype.onAuthorize = function(data, request) {
	};
	
	KFBConnect.connect = function(event) {
		var button = KDOM.getEventWidget(event);
		KBreadcrumb.dispatchEvent(button.id, {
			action : '/fb-connect'
		});
	};
	
	KFBConnect.actions = function(widget, action) {
		if (widget.activated()) {
			switch (action) {
				case '/fb-connect': {
					if (Kinky.getLoggedFB()) {
						var pup = window.open(Kinky.getLoggedFB().link);
						KSystem.popup.check(pup, Kinky.getShell());
					}
					else {
						var url = encodeURIComponent(window.document.location.href);
						window.document.location = KFBOpenGraph.FB_APP_AUTH_URL + '&redirect_uri=' + url;
					}
					break;
				}
			}
		}
	};
	
	KSystem.included('KFBConnect');
});
function KFBOpenGraph() {
}
{
	KFBOpenGraph.prototype.constructor = KFBOpenGraph;

	KFBOpenGraph.queryFB = function(widget, params, callback) {
		if (!KFBOpenGraph.FB_ACCESS_TOKEN) {
			KFBOpenGraph.FB_ACCESS_TOKEN = KOAuth.getAccessToken();
		}
		params.id = widget.id;
		params.callback = callback;
		KFBOpenGraph.send(params);

	};

	KFBOpenGraph.postFB = function(widget, params, callback) {
		if (!KFBOpenGraph.FB_ACCESS_TOKEN) {
			KFBOpenGraph.FB_ACCESS_TOKEN = KOAuth.getAccessToken();
		}
		params.args = params.args || {};
		params.args.access_token = KFBOpenGraph.FB_ACCESS_TOKEN;
		Kinky.bunnyMan.get(widget, 'php:Facebook:Post', params, callback);
	};

	KFBOpenGraph.send = function(params) {
		var url = (params.url || 'https://graph.facebook.com') + (params.hash || '') + '?' + (!params.noToken && KFBOpenGraph.FB_ACCESS_TOKEN ? 'access_token=' + KFBOpenGraph.FB_ACCESS_TOKEN : '') + (params.callback ? '&callback=' + encodeURIComponent('Kinky.bunnyMan.widgets[\'' + params.id + '\'].' + params.callback) : '&callback=void');

		for ( var index in params.args) {
			url += '&' + index + '=' + params.args[index];
		}

		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		head.appendChild(script);
	};

	KFBOpenGraph.setAccessToken = function(token) {
		KFBOpenGraph.FB_ACCESS_TOKEN = token;
		if (KFBOpenGraph.FB_PERSISTENT_SESSION) {

		}
	};

	KFBOpenGraph.FB_APP_ID = null;
	KFBOpenGraph.FB_APP_URL = null;
	KFBOpenGraph.FB_APP_AUTH_URL = null;
	KFBOpenGraph.FB_ACCESS_TOKEN = null;
	KFBOpenGraph.FB_PERSISTENT_SESSION = true;

	KSystem.included('KFBOpenGraph');
}
function KFeedbackItem(parent, config) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.isLeaf = true;
		this.config = config;
		this.data = {};
		
		this.icon = window.document.createElement('img');
		{
			this.addCSSClass('KFeedbackItemIcon', this.icon);
		}
		
		this.user = window.document.createElement('a');
		{
			this.addCSSClass('KFeedbackItemUser', this.user);
		}
		
		this.picture = window.document.createElement('img');
		{
			this.addCSSClass('KFeedbackItemPicture', this.picture);
		}
		
		this.caption = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemCaption', this.caption);
		}
		
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KFeedbackItemBody', this.body);
		}
		
		this.link = window.document.createElement('a');
		{
			this.link.href = 'javascript:void(0)';
			this.addCSSClass('KFeedbackItemLink', this.link);
		}
		
		this.report = window.document.createElement('a');
		{
			this.report.href = 'javascript:void(0)';
			this.report.appendChild(window.document.createTextNode(gettext('$KFEEDBACK_ITEM_REPORT_LINK')));
			this.addCSSClass('KFeedbackItemReportLink', this.report);
		}
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFeedbackItem.prototype = new KPanel();
	KFeedbackItem.prototype.constructor = KFeedbackItem;
	
	KFeedbackItem.prototype.setTitle = function() {
		if (!!this.config.data.title) {
			this.link.appendChild(window.document.createTextNode(this.config.data.title));
			this.addCSSClass('KFeedbackItemLink' + this.config.type, this.link);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setCaption = function() {
		if (!!this.config.data.caption) {
			this.caption.appendChild(window.document.createTextNode(this.config.data.caption));
			this.addCSSClass('KFeedbackItemCaption' + this.config.type, this.caption);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setLead = function() {
		if (!!this.config.data.lead) {
			this.lead.appendChild(window.document.createTextNode(this.config.data.lead));
			this.addCSSClass('KFeedbackItemLead' + this.config.type, this.lead);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setBody = function() {
		if (!!this.config.data.body) {
			this.body.appendChild(window.document.createTextNode(this.config.data.body));
			this.addCSSClass('KFeedbackItemBody' + this.config.type, this.body);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setLink = function() {
		if (!!this.config.data.link) {
			this.link.href = this.config.data.link;
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setReportLink = function() {
		if (this.config.data.state == 'active') {
			KDOM.addEventListener(this.report, 'click', KFeedbackItem.sendReport);
		}
		else {
			this.report.innerHTML = gettext('$KFEEDBACK_ITEM_REPORTED_LINK');
		}
		this.container.appendChild(this.report);
	};
	
	KFeedbackItem.prototype.reportFeedback = function() {
		var headers = null;
		this.kinky.post(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.data.href + '/reports', {}, headers, 'onReport');
	};
	
	KFeedbackItem.prototype.onCreated = KFeedbackItem.prototype.onReport = function(data, request) {
		KDOM.removeEventListener(this.report, 'click', KFeedbackItem.sendReport);
		this.report.innerHTML = '';
		this.report.appendChild(window.document.createTextNode(gettext('$KFEEDBACK_ITEM_REPORTED_LINK')));
	};
	
	KFeedbackItem.prototype.setPicture = function() {
		if (!!this.config.data.picture) {
			this.picture.src = this.config.data.picture;
			this.addCSSClass('KFeedbackItemPicture' + this.config.type, this.picture);
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.setOwner = function() {
		if (!!this.config.data.owner) {
			if (!!this.config.data.owner.picture) {
				this.icon.src = this.config.data.owner.picture;
				this.addCSSClass('KFeedbackItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			else if (!!this.config.data.owner.avatar) {
				this.icon.src = this.config.data.owner.avatar;
				this.addCSSClass('KFeedbackItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			if (!!this.config.data.owner.name) {
				var pattern = this.processPattern(gettext('$KFEEDBACK_ITEM_USER_PATTERN'));
				this.user.appendChild(window.document.createTextNode(pattern));
			}
			
			return true;
		}
		return false;
	};
	
	KFeedbackItem.prototype.go = function() {
		this.gone = true;
		this.config.type = this.config.type.substr(0, 1).toUpperCase() + this.config.type.substr(1);
		this.draw();
	};
	
	KFeedbackItem.prototype.draw = function() {
		var hasLinkTitle = false;
		
		if (this.setOwner()) {
			this.container.appendChild(this.user);
		}
		
		if (this.setTitle()) {
			this.container.appendChild(this.link);
			hasLinkTitle = true;
		}
		
		if (this.setLink()) {
			if (!hasLinkTitle) {
				this.container.appendChild(this.link);
				this.link.appendChild(window.document.createTextNode(this.config.data.link));
			}
		}
		
		if (this.setPicture()) {
			this.container.appendChild(this.picture);
		}
		
		if (this.setCaption()) {
			this.container.appendChild(this.caption);
		}
		
		if (this.setLead()) {
			this.container.appendChild(this.lead);
		}
		
		if (this.setBody()) {
			this.container.appendChild(this.body);
		}
		
		this.setReportLink();
		
		this.activate();
	};
	
	KFeedbackItem.prototype.processPattern = function(pattern) {
		var str = pattern.replace('\$USER_NAME', this.config.data.owner.name);
		str = str.replace('\$SUBMISSION_DATE', this.config.data.date);
		str = str.replace('\$SUBMISSION_TIME', this.config.data.time);
		str = str.replace('\$SUBMISSION_TIMESTAMP', this.config.data.timestamp);
		return str;
	};
	
	KFeedbackItem.sendReport = function(event) {
		KDOM.getEventWidget(event).reportFeedback();
	};
	KSystem.included('KFeedbackItem');
	
});
function KFeedback(parent) {
	if (parent != null) {
		KLayeredPanel.call(this, parent);
		this.isLeaf = false;
		this.social = window.document.createElement('div');
		{
			this.social.className = ' ' + this.className + 'SocialPanel KFeedbackSocialPanel ';
		}
		this.panel.insertBefore(this.social, this.content);
	}
}

KSystem.include([
	'KLayeredPanel',
	'KFeedbackPanel'
], function() {
	KFeedback.prototype = new KLayeredPanel();
	KFeedback.prototype.constructor = KFeedback;
	
	KFeedback.prototype.hasType = function(type) {
		for ( var t in this.config.types) {
			if (this.config.types[t] == type) {
				return true;
			}
		}
		return false;
	};
	
	KFeedback.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed/' + this.page);
		this.data.feed = elements;
		this.feed_retrieved = true;
		this.children_retrieved = true;
		this.no_children = true;
		var feedback = (!!this.parent.data.links && !!this.parent.data.links.feedback && !!this.parent.data.links.feedback.href ? this.parent.data.links.feedback.href : this.parent.config.href);
		
		if (!elements && !retrieved && !!feedback) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + feedback + '/feedback', {}, headers, 'onFeed');
		}
		else {
			this.data.feed = {};
			this.draw();
		}
	};
	
	KFeedback.prototype.onFeed = function(data, request) {
		this.data.feed = data;
		KCache.commit(this.ref + '/feed', this.data.feed);
		this.draw();
	};
	
	KFeedback.prototype.draw = function() {
		var feedback = (!!this.parent.data.links && !!this.parent.data.links.feedback && !!this.parent.data.links.feedback.href ? this.parent.data.links.feedback.href : this.parent.config.href);
		var config = {
			hash : this.hash,
			href : this.parent.config.href,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackPanel"
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackPanel"
			],
			links : {
				self : {
					href : feedback,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				},
				feed : {
					href : feedback,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		for ( var t in this.data.types) {
			var type = this.data.types[t];
			var result = this.data.feed[type];
			if (!!result) {
				var total = window.document.createElement('div');
				{
					total.className = ' ' + this.className + 'SocialPanel_' + type + ' KFeedbackSocialPanel_' + type + ' ';
					total.innerHTML = '<span class="KFeedbackSocialPanelCountLabel">' + result.size + '</span><span class="KFeedbackSocialPanelCountLabel">' + gettext(result.size == 1 ? '$KFEEDBACK_SOCIAL' + type.toUpperCase() + '_SINGULAR' : '$KFEEDBACK_SOCIAL' + type.toUpperCase()) + '</span>';
				}
				this.social.appendChild(total);
				
				var childConfig = KSystem.clone(config);
				childConfig.hash += '/' + type;
				childConfig.href += '/' + type;
				childConfig.links.self.href += '/' + type;
				childConfig.links.feed.href += '/' + type;
				childConfig.type = type;
				childConfig.http = this.config.http;
				
				var child = KSystem.construct(childConfig, this);
				this.addPanel(child, gettext('$KFEEDBACK_TITLE_' + type.toUpperCase()), this.nChildren == 0);
			}
		}
		
		KLayeredPanel.prototype.draw.call(this);
	};
	
	KSystem.included('KFeedback');
});
function KFeedbackPanel(parent) {
	if (parent != null) {
		KScrolledList.call(this, parent);
		this.isLeaf = true;
		this.add = window.document.createElement('div');
		{
			this.add.className = ' ' + this.className + 'AddPanel KFeedbackPanelAddPanel ';
		}
		this.content.appendChild(this.add);
		
	}
}

KSystem.include([
	'KScrolledList',
	'KFeedbackItem',
	'KTextArea'
], function() {
	KFeedbackPanel.prototype = new KScrolledList();
	KFeedbackPanel.prototype.constructor = KFeedbackPanel;
	
	KFeedbackPanel.prototype.load = function() {
		this.data = {
			template : {
				widget : {
					type : 'KFeedbackItem'
				}
			},
			links : this.config.links
		};
		this.retrieveFeed({});
	};
	
	KFeedbackPanel.prototype.onError = function(data, request) {
		dump(data);
	};
	
	KFeedbackPanel.prototype.onNoContent = function(data, request) {
		this.draw();
	};
	
	KFeedbackPanel.prototype.onCreated = KFeedbackPanel.prototype.onFeedback = function(data, request) {
		this.parent.refresh();
	};
	
	KFeedbackPanel.prototype.sendFeedback = function() {
		var headers = null;
		this.kinky.post(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.links.self.href, {
			body : this.feedback.getValue()
		}, headers, 'onFeedback');
	};
	
	KFeedbackPanel.prototype.onFeed = function(data, request) {
		var config = {
			hash : this.hash,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackItem"
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky.KFeedbackItem"
			],
			links : {
				self : {
					href : this.config.href,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		this.data.feed = {};
		
		this.totalSize = data.size;
		
		for ( var l in data.elements) {
			var childConfig = KSystem.clone(config);
			childConfig.hash += '/' + l;
			childConfig.links.self.href += '/' + l;
			childConfig.type = this.config.type;
			
			childConfig.data = data.elements[l];
			this.data.feed[childConfig.hash] = childConfig;
		}
		
		this.addPage();
		
		if (this.totalSize != 0) {
			this.data.feed.size = this.totalSize;
		}
		
		if (!!request) {
			KCache.commit(this.ref + '/feed/' + this.page, this.data.feed);
		}
		
		this.draw();
	};
	
	KFeedbackPanel.prototype.draw = function() {
		this.feedback = new KTextArea(this, gettext('$KFEEDBACK_' + this.config.type.toUpperCase() + '_TEXTAREA'), 'submit');
		this.appendChild(this.feedback, this.add);
		
		var button = new KButton(this, gettext('$KFEEDBACK_' + this.config.type.toUpperCase() + '_BUTTON'), 'send', function(event) {
			KDOM.getEventWidget(event).parent.sendFeedback();
		});
		this.appendChild(button, this.add);
		
		if (!!this.data.feed) {
			KScrolledList.prototype.draw.call(this);
		}
		else {
			this.feedback.go();
			button.go();
			this.activate();
		}
	};
	
	KSystem.included('KFeedbackPanel');
});
function KFeedItem(parent, config) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.config = config;
		this.data = {};
		
		this.icon = window.document.createElement('img');
		{
			this.addCSSClass('KFeedItemIcon', this.icon);
		}
		
		this.user = window.document.createElement('a');
		{
			this.addCSSClass('KFeedItemUser', this.user);
		}
		
		this.picture = window.document.createElement('img');
		{
			this.addCSSClass('KFeedItemPicture', this.picture);
		}
		
		this.caption = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemCaption', this.caption);
		}
		
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KFeedItemBody', this.body);
		}
		
		this.link = window.document.createElement('a');
		{
			this.link.href = 'javascript:void(0)';
			this.addCSSClass('KFeedItemLink', this.link);
		}
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFeedItem.prototype = new KPanel();
	KFeedItem.prototype.constructor = KFeedItem;
	
	KFeedItem.prototype.setTitle = function() {
		if (!!this.config.title) {
			this.link.appendChild(window.document.createTextNode(this.config.title));
			this.addCSSClass('KFeedItemLink' + this.config.type, this.link);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setCaption = function() {
		if (!!this.config.caption) {
			this.caption.appendChild(window.document.createTextNode(this.config.caption));
			this.addCSSClass('KFeedItemCaption' + this.config.type, this.caption);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setLead = function() {
		if (!!this.config.lead) {
			this.lead.appendChild(window.document.createTextNode(this.config.lead));
			this.addCSSClass('KFeedItemLead' + this.config.type, this.lead);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setBody = function() {
		if (!!this.config.body) {
			this.body.appendChild(window.document.createTextNode(this.config.body));
			this.addCSSClass('KFeedItemBody' + this.config.type, this.body);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setLink = function() {
		if (!!this.config.link) {
			this.link.href = this.config.link;
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setPicture = function() {
		if (!!this.config.picture) {
			this.picture.src = this.config.picture;
			this.addCSSClass('KFeedItemPicture' + this.config.type, this.picture);
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.setOwner = function() {
		if (!!this.config.owner) {
			if (!!this.config.owner.picture) {
				this.icon.src = this.config.owner.picture;
				this.addCSSClass('KFeedItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			else if (!!this.config.owner.avatar) {
				this.icon.src = this.config.owner.avatar;
				this.addCSSClass('KFeedItemIcon' + this.config.type, this.icon);
				this.user.appendChild(this.icon);
			}
			if (!!this.config.name) {
				this.user.appendChild(window.document.createTextNode(this.config.name));
			}
			if (!!this.config.href) {
				this.user.href = this.config.href;
			}
			else {
				this.user.src = 'javascrip:void(0)';
			}
			return true;
		}
		return false;
	};
	
	KFeedItem.prototype.go = function() {
		this.gone = true;
		this.config.type = this.config.type.substr(0, 1).toUpperCase() + this.config.type.substr(1);
		this.draw();
	};
	
	KFeedItem.prototype.draw = function() {
		var hasLinkTitle = false;
		
		if (this.setOwner()) {
			this.container.appendChild(this.user);
		}
		
		if (this.setTitle()) {
			this.container.appendChild(this.link);
			hasLinkTitle = true;
		}
		
		if (this.setLink()) {
			if (!hasLinkTitle) {
				this.container.appendChild(this.link);
				this.link.appendChild(window.document.createTextNode(this.config.link));
			}
		}
		
		if (this.setPicture()) {
			this.container.appendChild(this.picture);
		}
		
		if (this.setCaption()) {
			this.container.appendChild(this.caption);
		}
		
		if (this.setLead()) {
			this.container.appendChild(this.lead);
		}
		
		if (this.setBody()) {
			this.container.appendChild(this.body);
		}
		
		this.activate();
	};
	
	KSystem.included('KFeedItem');
	
});
function KFeed(parent) {
	if (parent != null) {
		KScrolledList.call(this, parent);
		this.autoRefresh = false;
		this.silent = true;
	}
}

KSystem.include([
	'KScrolledList',
	'KFeedItem'
], function() {
	
	KFeed.prototype = new KScrolledList();
	KFeed.prototype.constructor = KFeed;
	
	KFeed.prototype.onLoad = function(data, request) {
		if (!!data.autoRotate && (data.autoRotate != '0')) {
			this.autoRotate = parseInt(data.autoRotate);
		}
		if (!!data.pageSize && (data.pageSize != '0')) {
			if (!this.activated()) {
				this.silence();
				this.pageSize = parseInt(data.pageSize);
				this.data = data;
				
				if (this.pageSize != 0) {
					KCache.commit(this.ref + '/data', KSystem.clone(data));
					this.addPagination(this.header);
					this.addPagination(this.footer);
				}
				else {
					KCache.commit(this.ref + '/data', this.data);
				}
			}
			
			if (!!data.links && !!data.links.feed) {
				var elements = KCache.restore(this.ref + '/data/' + this.page);
				if (!!elements) {
					this.onChildren(elements);
				}
				else {
					var headers = null;
					this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(data.links.feed.href, {
						pageSize : this.pageSize,
						pageStartIndex : this.page * this.pageSize
					}), {}, headers, 'onChildren');
				}
			}
		}
		else {
			KWidget.prototype.onLoad.call(this, data, request);
		}
	};
	
	KFeed.prototype.onChildren = function(data, request) {
		if (!!request) {
			KCache.commit(this.ref + '/data/' + this.page, data);
		}
		this.addPage();
		for ( var e in data.elements) {
			var item = new KFeedItem(this, data.elements[e]);
			this.appendChild(item);
		}
		this.draw();
	};
	
	KSystem.included('KFeed');
});

function KFileDropPanel(parent) {
	if (parent != null) {
		KDropPanel.call(this, parent);
	}
}

KSystem.include([
	'KDropPanel'
], function() {
	KFileDropPanel.prototype = new KDropPanel();
	KFileDropPanel.prototype.constructor = KFileDropPanel;
	
	KFileDropPanel.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
		this.addEventListener('dragover', KFileDropPanel.preventDefault, false);
		this.addEventListener('dragenter', KFileDropPanel.preventDefault, false);
		this.addEventListener('dragleave', KFileDropPanel.preventDefault, false);
		this.addEventListener('drop', KFileDropPanel.droppedFile, false);
	};
	
	KFileDropPanel.prototype.onDrop = function(fileDropped) {
		this.kinky.save(this, KFileDropPanel.BASE_UPLOAD_SERVICE, fileDropped);
	};
	
	KFileDropPanel.prototype.onSave = function(uploadedFile) {
		for ( var file in uploadedFile.files) {
			this.droppedData[uploadedFile.files[file].name] = uploadedFile.files[file];
			
			var clone = window.document.createElement('div');
			clone.className = ' KFileDropped ';
			clone.style.position = 'relative';
			
			var mime = uploadedFile.files[file].mime.split('/');
			var image = window.document.createElement('img');
			image.src = (mime[0] == 'image' ? uploadedFile.files[file].uploadURL : 'images/mime/' + uploadedFile.files[file].mime.replace(/\//g, '-') + '.png');
			image.alt = image.title = uploadedFile.files[file].name;
			clone.appendChild(image);
			
			var removeButton = window.document.createElement('button');
			removeButton.className = ' KFileDropPanelRemoveButton ';
			removeButton.name = uploadedFile.files[file].name;
			removeButton.setAttribute('type', 'button');
			removeButton.appendChild(window.document.createTextNode('\u00d7'));
			clone.style.overflow = 'visible';
			clone.appendChild(removeButton);
			KDOM.addEventListener(removeButton, 'click', KDropPanel.removeElement);
			
			this.childDiv(KWidget.CONTENT_DIV).appendChild(clone);
		}
	};
	
	KFileDropPanel.preventDefault = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.stopPropagation();
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
	};
	
	KFileDropPanel.droppedFile = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		if (mouseEvent.preventDefault) {
			mouseEvent.stopPropagation();
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var widget = KDOM.getEventWidget(event);
		var mouseEvent = KDOM.getEvent(event);
		var params = {
			length : 0,
			files : []
		};
		for ( var i = 0; i != mouseEvent.dataTransfer.files.length; i++) {
			var droppedFile = mouseEvent.dataTransfer.files[i];
			if (!widget.droppedData[droppedFile.fileName]) {
				params.length++;
				KFileDropPanel.addFile(widget, droppedFile, params);
			}
		}
	};
	
	KFileDropPanel.addFile = function(widget, droppedFile, params) {
		var file = {
			name : droppedFile.fileName,
			size : droppedFile.fileSize,
			mime : droppedFile.type
		};
		var reader = new FileReader();
		reader.onload = function(evt) {
			var fileEvent = KDOM.getEvent(evt);
			file.rawData = fileEvent.target.result.split(',')[1];
			params.files.push(file);
			if (params.files.length == params.length) {
				widget.onDrop(params);
			}
		};
		reader.readAsDataURL(droppedFile);
	};
	
	KFileDropPanel.BASE_UPLOAD_SERVICE = null;
	
	KSystem.included('KFileDropPanel');
});
function KFile(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.maxFile = 1;
		this.noPrompt = true;
		this.options = options || { }
		
		this.contentContainer = window.document.createElement('div');
		
		this.fileListContainer = window.document.createElement('div');
		this.fileListContainer.className = 'KFileListContainer';
		this.fileListContainer.id = 'container' + this.id;
		
		this.button = null;
		
		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea KFileErrorArea';
		this.fileref = null;
		this.multiple = false;

		this.aditional_args = (!!this.options.aditional_args ? this.options.aditional_args : undefined);

		this.progresswin = window.document.createElement('div');
		{
			this.progresswin.className = ' KButtonProgress ';
			KCSS.setStyle({
				position: 'fixed',
				zIndex : '9999'
			}, [
			this.progresswin
			]);

			this.progressbar = window.document.createElement('div');
			{
				this.progressbar.className = ' KButtonProgressBar ';
				KCSS.setStyle({
					width : '0'
				}, [
				this.progressbar
				]);
			}
			this.progresswin.appendChild(this.progressbar);

			this.progresstext = window.document.createElement('div');
			{
				this.progresstext.className = ' KButtonProgressText ';
			}
			this.progresswin.appendChild(this.progresstext);

			var label = window.document.createElement('div');
			{
				label.className = ' KButtonProgressLabel ';
				label.innerHTML = gettext('$KUPLOADBUTTON_UPLOADING_TEXT')
			}
			this.progresswin.appendChild(label);
		}
		this.INITIAL_BUTTON_ALPHA = '0.3';
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KFile.prototype = new KAbstractInput();
	KFile.prototype.constructor = KFile;

	KFile.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		/*var otherbutton = this.content.getElementsByTagName('button');
		for (var ob = 0; ob != otherbutton.length; ob++) {
		}*/		
	}

	KFile.prototype.onLoad = function(data, request) {
		this.data = data;
		this.draw();
	};
	
	KFile.prototype.addEventListener = function(eventName, callback, target) {
		if ((!target || (target == KWidget.ROOT_DIV)) && (eventName == 'change')) {
			if (!this.button) {
				this.event_to_add = callback;
			}
			else {		
				KDOM.addEventListener(this.button, 'click', callback);
			}
		}
		else {
			KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KFile.prototype.showErrorMessage = function(params) {
		if (!params || !params.tooltip) {
			KAbstractInput.prototype.showErrorMessage.call(this, params);
		}
		else if (params && params.tooltip) {
			KDOM.addEventListener(this.inputFile, 'mouseover', KFile.onMouseOver);
		}
	};
	
	KFile.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(mouseEvent);
		
		KTooltip.showTooltip(widget.inputFile, {
			text : '* ' + (widget.validationIndex ? widget.getErrorMessage(widget.validationIndex) : ''),
			isHTML : true,
			offsetX : 0,
			offsetY : 15,
			left : KDOM.mouseX(mouseEvent),
			top : (KDOM.mouseY(mouseEvent) + 15),
			cssClass : 'KAbstractInputTooltip'
		});
	};
	
	KFile.prototype.createButton = function() {
		var inputFile = window.document.createElement('input');
		inputFile.setAttribute('type', 'file');
		if (this.multiple) {
			inputFile.setAttribute('multiple', 'multiple');
		}
		inputFile.className = 'KFileButton';
		inputFile.id = this.id + '_input_file';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			opacity : '0'
		}, [
			inputFile
		]);

		KDOM.addEventListener(inputFile, 'change', KFile.changed);
		
		var toReturn = window.document.createElement('button');
		toReturn.setAttribute('type', 'button');
		toReturn.className = 'KFileButton';
		if (!!this.data && !!this.data.buttonText) {
			toReturn.innerHTML = this.data.buttonText;
		}
		else {
			toReturn.innerHTML = gettext('$KFILE_BUTTON_UPLOAD_TEXT');
		}
		toReturn.id = this.id + '_button';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0'
		}, [
			toReturn
		]);
		
		return [toReturn, inputFile];
	};
	
	KFile.prototype.createButtonLabel = function() {
		var toReturn = window.document.createElement('label');
		toReturn.className = 'KFileButtonLabel';
		if (!!this.data && !!this.data.buttonLabelText) {
			toReturn.innerHTML = this.data.buttonLabelText;
		}
		else {
			toReturn.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
		}
		
		return toReturn;
	};
	
	KFile.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		var label = this.createLabel();
		this.content.appendChild(label);
		var help = this.createHelp();
		if (!!help) {
			label.appendChild(help);
		}
		
		var elements = this.createButton();
		this.button = elements[0];
		this.inputFile = elements[1];
		if (!!this.event_to_add) {
			KDOM.addEventListener(this.button, 'click', this.event_to_add);
			this.event_to_add = null;
		}
		
		var buttonContainer = window.document.createElement('div');
		buttonContainer.className = 'KFileButtonContainer';
		KCSS.setStyle({
			position : 'relative'
		}, [
			buttonContainer
		]);
		buttonContainer.appendChild(this.button);
		buttonContainer.appendChild(this.inputFile);
		
		this.buttonLabel = this.createButtonLabel();
		
		this.maxFile = (!!this.data && !!this.data.maxFile ? this.data.maxFile : this.maxFile);
		this.noPrompt = (!!this.data && !!this.data.noPrompt ? this.data.noPrompt : this.noPrompt);
		//this.iframe.src = (!!Kinky.FILE_MANAGER_UPLOAD ? Kinky.FILE_MANAGER_UPLOAD : "/zepp/filemanager/form") + "?id=" + this.id + '&ref=' + (!!this.fileref ? this.fileref : this.id);
		
		this.contentContainer.appendChild(this.fileListContainer);
		this.contentContainer.appendChild(this.buttonLabel);
		this.contentContainer.appendChild(buttonContainer);
		this.contentContainer.appendChild(this.errorArea);
		
		this.content.appendChild(this.contentContainer);
		if (this.className == 'KFile') {
			this.addCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
		}
		
		KDOM.addEventListener(this.inputFile, 'mouseover', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.addCSSClass('KFileButtonOver', widget.button);
		});
		KDOM.addEventListener(this.inputFile, 'mouseout', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.removeCSSClass('KFileButtonOver', widget.button);
		});
		KDOM.addEventListener(this.button, 'click', KFile.changed);
		
		this.activate();
		this.updateValue();
	};
	
	KFile.prototype.setSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.panel,
			this.content,
			this.button,
			this.inputFile,
			this.iframe
		]);
	};
	
	KFile.prototype.setButtonSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.button,
			this.inputFile,
			this.iframe
		]);
	};
	
	KFile.prototype.getFileName = function() {
		var toReturn = new Array();
		var labels = this.fileListContainer.getElementsByTagName('label');
		
		for ( var label = 0; label != labels.length; label++) {
			toReturn.push(labels[label].innerHTML);
		}
		
		return toReturn;
	};
	
	KFile.prototype.setValue = function(value) {
		if (!value) {
			KAbstractInput.prototype.setValue.call(this, value);
		}
		else if (value instanceof Array) {
			for ( var n in value) {
				var name = value[n].substr(value[n].lastIndexOf('/') + 1);
				this.addFile(name, value[n]);
			}
		}
		else {
			var name = value.substr(value.lastIndexOf('/') + 1);
			this.addFile(name, value);
		}
	};
	
	KFile.changed = function(event) {
		var widget = KDOM.getEventWidget(event);
		
		var files = event.target.files; // FileList object
		if (files === undefined) {
			return;
		}

		widget.disable();

		for (var i = 0, f; f = files[i]; i++) {
			widget.progressbar.style.width = '0%';
			widget.progresstext.innerHTML = '0%';
			window.document.body.appendChild(widget.progresswin);

			var reader = new FileReader();
			reader.file = f;
			reader.onprogress = function(e) {
				KUploadButton.progress(e, widget);
			};

			reader.onload = (function(theFile) {
				return function(e) {
					widget.upload(e.target.result, e.target.file, KFile.uploaded);
				};
			})(f);

			reader.readAsDataURL(f);
		}
	};

	KFile.progress = function(evt, widget, upload) {

		if (evt.lengthComputable) {
			var percentLoaded = Math.round((evt.loaded / evt.total) * 100);

			if (percentLoaded < 100) {
				widget.progressbar.style.width = percentLoaded + '%';
				widget.progresstext.innerHTML = percentLoaded + '%';
			}
			else if (!upload) {
				window.document.body.removeChild(widget.progresswin);				
			}
		}
	};

	KFile.prototype.getUploadedData = function() {
		return this.uploaded_data;
	};

	KFile.prototype.upload = function(data, file, callback) {
		var widget = this;
		var channel = null;
		if (window.XMLHttpRequest) { 
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { 
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}

		channel.onreadystatechange = function() {
			if (channel.readyState == 4) {
				if (!!channel.upload) {
					window.document.body.removeChild(widget.progresswin);				
				}
				widget.enable();

				if (channel.status == 0) {
					return;
				}

				if (channel.status == 200) {
					var resp = JSON.parse(channel.responseText);
					widget.uploaded_data = resp;
					widget.addFile(file.name, resp.href);
				}

			}
		};

		var boundary = "----KFILEUpload" + (new Date()).getTime();

		channel.open('POST', (!!this.options.endpoint ? this.options.endpoint : Kinky.FILE_MANAGER_UPLOAD) + (!!this.fileref ? '?ref=' + encodeURIComponent(this.fileref) : '') + (!!this.aditional_args ? (!!this.fileref ? '&' : '?') + this.aditional_args : ''), true);
		channel.setRequestHeader("Authorization", !!this.options.access_token ? 'OAuth2.0 ' + this.options.access_token : KOAuth.getAuthorization().authentication);
		channel.setRequestHeader("X-Requested-With", "XHR");
		channel.setRequestHeader("Pragma", "no-cache");
		channel.setRequestHeader("Cache-Control", "no-store");
		channel.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

		var content = '--' + boundary + '\r\n';
		content += 'Content-Encoding: Base64\r\n';
		content += 'Content-Disposition: form-data; name="addFile"; filename="' + file.name + '"\r\n';
		content += 'Content-Type: ' + file.type + '\r\n\r\n';
		content += data.split(',')[1] + '\r\n';
		content += '--' + boundary + '--'

		try {
			if (!!channel.upload) {
				widget.progressbar.style.width = '0%';
				widget.progresstext.innerHTML = '0%';
				window.document.body.appendChild(widget.progresswin);
				channel.upload.addEventListener("progress", function(e) {
					KUploadButton.progress(e, widget, true);
				}, false);
			}
			channel.send(content);
		}
		catch (e) {
		}

	};
	
	KFile.prototype.updateValue = function() {
		var toReturn = new Array();
		var inputs = this.fileListContainer.getElementsByTagName('input');
		
		for ( var input = 0; input != inputs.length; input++) {
			toReturn.push(inputs[input].value);
		}
		
		if (toReturn.length == 0) {
			this.input.value = '';
		}
		else if (toReturn.length > 1) {
			this.input.value = JSON.stringify(toReturn);
		}
		else {
			this.input.value = JSON.stringify(toReturn[0]);
		}
		if (this.className == 'KFile') {
			if (toReturn.length == 0) {
				this.addCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
				this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
			}
			else {
				this.removeCSSClass(gettext('$KFILE_NO_IMAGE_THUMB'), this.content);
				this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_CHANGE_TEXT');
			}			
		}
	};
	
	KFile.prototype.addFile = function(fileName, filePath) {
		var inputs = this.fileListContainer.getElementsByTagName('input');
		var parent = this.fileListContainer;
		for ( var idx = 0; idx != parent.childNodes.length; idx++) {
			if (parent.childNodes[idx].className.indexOf("KFileDefaultText") != -1) {
				parent.removeChild(parent.childNodes[idx]);
				break;
			}
		}
		
		var newFileContainer = window.document.createElement('div');
		
		var newFileLabel = window.document.createElement('div');
		newFileLabel.appendChild(window.document.createTextNode(fileName));
		newFileContainer.appendChild(newFileLabel);
		
		var newFile = window.document.createElement('input');
		newFile.type = 'hidden';
		newFile.name = this.id + '[]';
		newFile.id = this.id + '_' + inputs.length;
		newFile.value = filePath;
		newFileContainer.appendChild(newFile);
		
		var newFileRemove = window.document.createElement('button');
		newFileRemove.setAttribute('type', 'button');
		newFileContainer.appendChild(newFileRemove);
		
		KDOM.addEventListener(newFileRemove, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			var target = KDOM.getEventTarget(event);
			target.parentNode.parentNode.removeChild(target.parentNode);
			widget.updateValue();
			
		});
		
		newFileContainer.appendChild(window.document.createElement('br'));
		parent.appendChild(newFileContainer);
		if (this.activated()) {
			this.updateValue();
			KDOM.fireEvent(this.button, 'click');
		}
		
	};
	
	KFile.prototype.setDefaultText = function(text) {
		var newFileContainer = window.document.createElement('div');
		KCSS.addCSSClass('KFileDefaultText', newFileContainer);
		
		var newFileLabel = window.document.createElement('label');
		newFileLabel.appendChild(window.document.createTextNode(text));
		newFileContainer.appendChild(newFileLabel);
		
		this.fileListContainer.appendChild(newFileContainer);
	};
	
	KFile.prototype.getFileInput = function() {
		var doc = this.iframe.contentDocument || this.iframe.document;
		var inputs = doc.getElementsByTagName('input');
		
		for ( var i = 0; i != inputs.length; i++) {
			if (inputs[i].type == 'file') {
				return inputs[i];
			}
		}
		
	};
	
	KFile.BUTTON_ELEMENT = 'button';
	
	KSystem.included('KFile');
	
});

function KFlash(parent, swfURL, params) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);

		this.data = params;
		this.swfURL = swfURL;
		this.swfID = null;
		this.staticFlash = false;
		this.content.id = this.id + 'FlashReplace';

	}
}

KSystem.include([
	'KWidget'
], function() {
	KFlash.prototype = new KWidget;
	KFlash.prototype.constructor = KFlash;

	KFlash.prototype.go = function() {
		if (this.staticFlash || (this.swfURL != null)) {
			this.draw();
		}
		else {
			this.load();
		}
	};

	KFlash.prototype.redraw = function() {
		if (this.activated()) {
			this.panel.innerHTML = '';

			this.content = window.document.createElement('div');
			this.content.className = ' KWidgetPanelContent ' + this.className + 'Content ';
			this.content.id = this.id + 'FlashReplace';
			this.panel.appendChild(this.content);
		}
		KWidget.prototype.redraw.call(this);
	};

	KFlash.prototype.load = function() {
		var params = new Object();
		params.contentView = this.data.contentView;
		params.contentID = this.data.contentID;
		this.kinky.get(this, this.data.feService, params);
	};

	KFlash.prototype.onLoad = function(data, request) {
		this.swfURL = data.fileURL;
		this.data = data;
		this.clear();
		this.draw();
	};

	KFlash.prototype.exec = function(callback, params) {
		var func = null;
		if (this.getFlashObject()) {
			func = this.getFlashObject()[callback];
			if (func) {
				try {
					func(params);
				}
				catch (e) {

				}
			}
		}
	};

	KFlash.prototype.reset = function() {
		this.exec('resetFlash');
	};

	KFlash.prototype.stop = function() {
		this.exec('stopFlash');
	};

	KFlash.prototype.play = function() {
		this.exec('playFlash');
	};

	KFlash.prototype.onFlashStart = function() {
		return this.data;
	};

	KFlash.prototype.onFlashLoad = function() {
		this.activate();
	};

	KFlash.prototype.draw = function() {
		if (this.swfURL) {
			this.data.attributes = this.data.attributes || {
				id : this.id,
				name : this.id
			};
			this.data.flashVars.onStart = 'KFlash.onStart';
			this.data.flashVars.onComplete = 'KFlash.onComplete';
			this.data.flashVars.className = this.className;
			this.data.flashVars.widgetID = this.id;
			this.swfID = this.id + 'FlashReplace';
			swfobject.embedSWF(this.swfURL, this.swfID, this.data.width || '100%', this.data.height || '100%', this.data.version || "9.0.124", null, this.data.flashVars, this.data.params, this.data.attributes);

			if (this.data.noHandlers) {
				this.activate();
			}
		}
		else {
			this.activate();
		}
	};

	KFlash.prototype.getFlashObject = function() {
		return window.document.getElementById(this.data.attributes.id);
	};

	KFlash.onStart = function(widgetID) {
		return Kinky.bunnyMan.widgets[widgetID].onFlashStart();
	};

	KFlash.onComplete = function(widgetID) {
		Kinky.bunnyMan.widgets[widgetID].onFlashLoad();
	};

	KFlash.debug = function(object) {
		dump(object);
	};

	KSystem.included('KFlash');
});
function KFlashWidget(parent, url, service) {
	if (parent != null) {
		KShellPart.call(this, parent, url, service);
	}
}

KSystem.include([
	'KWidget', 'KShellPart'
], function() {

	KFlashWidget.prototype = new KWidget();
	KFlashWidget.prototype.constructor = KFlashWidget;

	KFlashWidget.prototype.onShow = function() {
		if (this.display) {
			return;
		}
	};

	KFlashWidget.prototype.onHide = function() {
		if (this.display && !(this instanceof KFlashWidgetDialog) && Kinky.shell.childWidget(this.getHash()) instanceof KFlashWidgetDialog) {
			return;
		}
	};

	KFlashWidget.prototype.draw = function() {
		this.activate();
	};

	KSystem.included('KFlashWidget');
});
function KFloatable(target, params) {
	if (target != null) {
		KPanel.call(this, target.parent);
		this.target = target;
		target.parent = this;
		this.appendChild(target);
		this.data = new Object();
		this.data.params = params;
	}
}

KSystem.include([
	'KPanel'
], function() {
	KFloatable.prototype = new KPanel();
	KFloatable.prototype.constructor = KFloatable;
	
	KFloatable.prototype.blur = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		if (this.data.params.modal) {
			this.getShell().hideOverlay();
			if (Kinky.MODAL_WIDGET.length == 0) {
				window.document.body.style.height = null;
				window.document.body.style.overflow = 'visible';
			}
		}
		KWidget.prototype.blur.call(this);
		this.unload();
	};
	
	KFloatable.prototype.visible = function() {	
		if (!!this.display) {
			return;
		}

		this.setStyle({
			position : 'absolute',
			top : this.top + 'px',
			width : this.width + 'px',
			marginBottom : '2em',
			height : (this.height != -1 ? this.height + 'px' : (this.target.getHeight() != 0 ? this.target.getHeight() + 'px' : 'auto'))
		});
		if (this.right !== undefined) {
			this.setStyle({
				right : this.right + 'px'
			});
		}
		else {
			this.setStyle({
				left : this.left + 'px'
			});			
		}
		if (this.data.params.expandable) {
			this.parentDiv.appendChild(this.panel);
		}
		
		KPanel.prototype.visible.call(this);
		if (this.data.params.expandable) {
			var h = (this.height != -1 ? this.height : (this.target.getHeight() != 0 ? this.target.getHeight() : 100));
			if (h + 20 < KDOM.getBrowserHeight()) {
				h = 0;
			}
			this.margin.style.top = h + 'px';
		}
		this.focus();
	};
	
	KFloatable.prototype.transition_show = function(tween) {
		if (this.data.params.transition_wait_target) {
			return;
		}

		if (tween == -1) {
			tween = null;
		}
		if (!tween && this.data && this.data.params.tween && this.data.params.tween.show) {
			tween = this.data.params.tween.show;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function(w, t) {
					if (!!w.target && !!w.target.onOpen) {
						w.target.onOpen();
					}
				}
			};
		}
		else if ((tween instanceof Array && !tween[0].onStart) || !tween.onStart) {
			(tween instanceof Array ? tween[0] : tween).onStart = function(w, t) {
				if (!!w.target && !!w.target.onOpen) {
					w.target.onOpen();
				}
			};
		}
		KPanel.prototype.transition_show.call(this, tween);
	};
	
	KFloatable.prototype.transition_hide = function(tween) {
		if (tween == -1) {
			tween = null;
		}
		if (!tween && this.data && this.data.params.tween && this.data.params.tween.hide) {
			tween = this.data.params.tween.hide;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (w.data.params.expandable) {
						w.parentDiv.parentNode.removeChild(w.parentDiv);
					}
					w.invisible();
					w.destroy();				
				}
			};
		}
		else if ((tween instanceof Array && !tween[0].onComplete) || !tween.onComplete) {
			(tween instanceof Array ? tween[0] : tween).onComplete = function(w, t) {
				if (w.data.params.expandable) {
					w.parentDiv.parentNode.removeChild(w.parentDiv);
				}					
				w.invisible();
				w.destroy();				
			};
		}
		KPanel.prototype.transition_hide.call(this, tween);
	};
	
	KFloatable.prototype.draw = function() {
		if (!!this.data.params) {
			if (!!this.data.params.className) {
				this.addCSSClass(this.data.params.className);
			}
			
			if (!!this.data.params.buttons) {
				var buttons = this.data.params.buttons.split(/,/);
				for ( var i = buttons.length - 1; i != -1; i--) {
					var button = window.document.createElement('button');
					{
						button.className = 'KFloatableButton' + i;
						KDOM.addEventListener(button, 'click', KFloatable[buttons[i]]);
					}
					this.container.appendChild(button);
				}
			}
		}
		
		if (!!this.data.params.width) {
			this.width = this.data.params.width;
		}
		else if (!!this.data.params.scale && !!this.data.params.scale.width) {
			this.width = Math.round(KDOM.getBrowserWidth() * this.data.params.scale.width);
		}
		else {
			this.data.params.width = this.width = Math.round(KDOM.getBrowserWidth() * 0.9);
		}
		if (!!this.data.params.height) {
			this.height = this.data.params.height;
		}
		else if (!!this.data.params.scale && !!this.data.params.scale.height) {
			this.height = Math.round(KDOM.getBrowserHeight() * this.data.params.scale.height);
		}
		else {
			this.data.params.height = this.height = -1;
		}
		
		if (!!this.data.params.top) {
			this.top = this.data.params.top;
		}
		else {
			this.data.params.top = this.top = Math.max(20, Math.round((KDOM.getBrowserHeight() - this.height) / 2));
		}
		
		if (this.data.params.left !== undefined) {
			this.left = this.data.params.left;
		}
		else if (this.data.params.right !== undefined) {
			this.right = this.data.params.right;
		}
		else {
			this.data.params.left = this.left = Math.round((KDOM.getBrowserWidth() - this.width) / 2);
		}
		
		KPanel.prototype.draw.call(this);
		Kinky.setModal(this);
	};
	
	KFloatable.prototype.reposition = function(top) {
		if (!!top) {
			this.data.params.top = this.top = top;
		}
		else {
			top = this.data.params.top = this.top = Math.max(20, Math.round((KDOM.getBrowserHeight() - this.height) / 2));
		}
		this.setStyle({
			top : top + 'px'
		});
		return top;
	};

	KFloatable.prototype.resize = function(height) {
		this.data.params.height = this.height = height;
		this.setStyle({
			height : height + 'px'
		});
		this.reposition();
		this.transition_show();
		if (this.data.params.expandable) {
			if (height < KDOM.getBrowserHeight()) {
				this.margin.style.top = '0px';			
			}
			else {
				this.margin.style.top = height + 'px';
			}
		}
		return height;
	};

	KFloatable.prototype.go = function() {
	};

	KFloatable.prototype.showMe = function() {
		KPanel.prototype.go.call(this);
	};
	
	KFloatable.prototype.closeMe = function() {
		Kinky.unsetModal(this);
	};
	
	KFloatable.close = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.closeMe();
	};
	
	KFloatable.make = function(target, params) {
		var floatable = new KFloatable(target, params);
		floatable.parent.appendChild(floatable, KWidget.ROOT_DIV);

		if (floatable.data.params.expandable) {
			floatable.parentDiv = window.document.createElement('div');
			floatable.parentDiv.className = ' KFloatableScoller ';
			KCSS.setStyle({
				width : '100%',
				height : '100%',
				position : 'fixed',
				top : '0',
				left : '0',
				zIndex : '901',
				overflowX : 'hidden',
				overflowY : 'auto'
			}, [ floatable.parentDiv ]);
			floatable.parent.childDiv(KWidget.ROOT_DIV).appendChild(floatable.parentDiv);

			floatable.margin = window.document.createElement('p');
			floatable.margin.className = ' KFloatableScollerMargin ';
			floatable.margin.style.position = 'absolute';
			floatable.margin.style.left = '0';
			floatable.margin.style.width = '100%';
			floatable.parentDiv.appendChild(floatable.margin);	
		}
		return floatable;
	};
	
	KFloatable.show = function(floatable) {
		floatable.showMe();
		if (floatable.data.params.modal) {
			floatable.getShell().showOverlay();
			window.document.body.style.height = KDOM.getBrowserHeight() + 'px';
			window.document.body.style.overflow = 'hidden';
		}
		
	};
	
	KFloatable.hide = function(floatable) {
		Kinky.unsetModal(floatable);
	};
	
	KSystem.included('KFloatable');
});
function KForm(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.method = 'post';
		
		this.form = window.document.createElement('form');
		this.form.method = 'post';
		this.content.appendChild(this.form);
		
		this.info = window.document.createElement('div');
		this.info.className = 'KFormInfoArea';
		this.content.appendChild(this.info);
		
		this.originalContent = this.content;
		
		this.content = this.fieldset = window.document.createElement('fieldset');
		this.form.appendChild(this.content);
		
		this.action = null;
		this.stopOnFirstError = false;
		this.inputByID = new Object();
		
	}
}

KSystem.include([
	'KWidget'
], function() {
	KForm.prototype = new KWidget();
	KForm.prototype.constructor = KForm;
	
	KForm.prototype.reset = function() {
		for ( var element in this.inputByID) {
			this.inputByID[element].setValue(null);
		}
	};
	
	KForm.prototype.setDefaultData = function(data) {
		this.entered = data;
	};
	
	KForm.prototype.addInput = function(input, targetDiv) {
		if (input instanceof KAbstractInput) {
			if (input.parent.id == this.id) {
				this.appendChild(input, targetDiv);
			}
			this.inputByID[input.inputID] = input;
			if (!!this.entered) {
				var defaultValue = null;
				eval('defaultValue = this.entered.' + input.inputID + ';');
				if (!!defaultValue) {
					input.setValue(defaultValue);
				}
			}
			input.parentForm = this;
		}
		else {
			throw new KNoExtensionException(this, "addInput");
		}
	};
	
	KForm.prototype.getInput = function(inputID) {
		return this.inputByID[inputID];
	};
	
	KForm.prototype.removeInput = function(inputID) {
		var input = this.childWidget(inputID);
		if (input) {
			delete this.inputByID[inputID];
			this.removeChild(input);
		}
	};
	
	KForm.prototype.setMethod = function(method) {
		this.method = method;
	};
	
	KForm.onEnterPressed = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		if (keyCode != 13) {
			return false;
		}
		return true;
	};
	
	KForm.prototype.showInfo = function(data) {
		this.info.innerHTML = data;
	};
	
	KForm.prototype.onValidate = function(validationText, params, status) {
		throw new KNoExtensionException(this, "onValidate");
	};
	
	KForm.prototype.onSuccess = function(data, request) {
		throw new KNoExtensionException(this, "onSuccess");
	};
	
	KForm.prototype.onError = function(error) {
	};
	
	KForm.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		
		this.childAt(0).focus(this);
		KDOM.addEventListener(this.form, 'submit', function(event) {
			KForm.goSubmit(event);
			return false;
		});
	};
	
	KForm.prototype.submit = function() {
		var params = this.validate();
		if ((params = this.onValidate(this.action, params, this.validation)) != false) {
			this.kinky[this.method.toLowerCase()](this, this.action, params, null, 'onSuccess');
		}
		return false;
	};
	
	KForm.prototype.validate = function() {
		var params = new Object();
		var wrongParams = new Object();
		var isValid = true;
		
		for ( var element in this.inputByID) {
			var widget = this.inputByID[element];
			if (!(widget instanceof KAbstractInput)) {
				continue;
			}
			var validation = widget.validate();
			if (validation == true) {
				var name = widget.inputID.replace(/[\[\]]/g, '');
				var names = name.split('.');
				var current = '';
				for ( var i = 0; i != names.length - 1; i++) {
					current += '.' + names[i];
					var exists = false;
					eval('exists = !!params' + current + '');
					if (!exists) {
						eval('params' + current + ' = {}');
					}
				}
				eval('params.' + name + ' = widget.getValue()');
			}
			else {
				wrongParams[widget.inputID.replace(/[\[\]]/g, '')] = widget;
				isValid = false;
				if (this.stopOnFirstError) {
					break;
				}
			}
		}
		this.validation = isValid;
		return (isValid ? params : wrongParams);
	};
	
	KForm.prototype.showAllErrors = function(errors) {
		for ( var param in errors) {
			errors[param].showErrorMessage();
		}
	};
	
	KForm.goSubmit = function(event) {
		var target = KDOM.getEventTarget(event);
		var form = KDOM.getWidget(target.form || target);
		while (!(form instanceof KForm)) {
			form = form.parent;
		}
		var params = form.validate();
		
		if ((params = form.onValidate(form.action, params, form.validation)) != false) {
			var method = form.method.toLowerCase();
			form.kinky[(method == 'delete' ? 'remove' : method)](form, form.action, params, null, 'onSuccess');
		}
		
		return false;
	};
	
	KForm.FORM_ELEMENT = 'form';
	
	KSystem.included('KForm');
});
function KGallery(parent) {
	if (parent != null) {
		KList.call(this, parent);
		this.addActionListener(KGallery.popup, [
			'/gallery/view-image/*'
		]);
		this.nImage = [
			0
		];
	}
}

KSystem.include([
	'KList',
], function() {
	KGallery.prototype = new KList();
	KGallery.prototype.constructor = KGallery;
	
	KGallery.prototype.appendChild = function(element, context) {
		if (element instanceof KLink) {
			element.isImageLink = true;
		}
		else if (element instanceof KImage) {
			var a = window.document.createElement('a');
			if (this.paginationDispatchType == 'url') {
				a.href = this.getLink({
					action : '/gallery/view-image/' + this.getImageCount()
				});
			}
			else {
				var imageCount = this.getImageCount();
				a.href = 'javascript:void(0)';
				KDOM.addEventListener(a, 'click', function(event) {
					var widget = KDOM.getEventWidget(event);
					KBreadcrumb.dispatchEvent(widget.parent.id, {
						action : '/gallery/view-image/' + imageCount
					});
				});
			}
			element.maxWidth = this.maxWidth;
			element.maxHeight = this.maxHeight;
			element.content.style.overflow = 'hidden';
			element.content.style.height = this.maxHeight + 'px';
			element.content.style.width = this.maxWidth + 'px';
			element.content.appendChild(a);
			element.content = a;
			element.content.appendChild(element.image);
		}
		this.setImageCount();
		KList.prototype.appendChild.call(this, element, context);
	};
	
	KGallery.prototype.getImageCount = function() {
		var context = this.query + this.nPage;
		if (this.hasPaginationBar && (Math.floor(this.nChildren / this.perPage) != this.nPage)) {
			context = this.query + Math.floor(this.nChildren / this.perPage);
		}
		
		if (this.nImage[context] == null) {
			this.nImage[context] = 0;
		}
		return this.nImage[context];
	};
	
	KGallery.prototype.setImageCount = function() {
		var context = this.query + this.nPage;
		if (this.hasPaginationBar && (Math.floor(this.nChildren / this.perPage) != this.nPage)) {
			context = this.query + Math.floor(this.nChildren / this.perPage);
		}
		
		if (this.nImage[context] == null) {
			this.nImage[context] = 0;
		}
		this.nImage[context]++;
	};
	
	KGallery.popup = function(widget, action) {
		if (widget.activated() && widget.getParent('KShellPart') && (widget.getParent('KShellPart').hash == widget.getHash())) {
			var imageID = action.split('/');
			var image = widget.childAt(imageID[3]);
			if (image) {
				var dialog = null;
				if (Kinky.shell.childWidget('/gallery/popup')) {
					dialog = Kinky.shell.childWidget('/gallery/popup');
				}
				else {
					Kinky.shell.showOverlay();
					
					dialog = new KDialog(Kinky.shell);
					dialog.type = KDialog.MODAL;
					dialog.addCSSClass('KGalleryDialog');
					dialog.hash = '/gallery/popup';
					dialog.nextCallback = function(event) {
						var nImage = parseInt(widget.getAction().split('/')[3]);
						var nextImage = widget.childAt(++nImage);
						if (nextImage) {
							KEffects.addEffect(dialog, {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.overlayWidgets--;
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.dispatchURL({
											action : '/gallery/view-image/' + nImage
										});
									}
									else {
										KBreadcrumb.dispatchEvent({
											action : '/gallery/view-image/' + nImage
										});
									}
								}
							});
						}
					};
					dialog.previousCallback = function(event) {
						var nImage = parseInt(widget.getAction().split('/')[3]);
						var prevImage = widget.childAt(--nImage);
						if (prevImage) {
							KEffects.addEffect(dialog, {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.overlayWidgets--;
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.dispatchURL({
											action : '/gallery/view-image/' + nImage
										});
									}
									else {
										KBreadcrumb.dispatchEvent({
											action : '/gallery/view-image/' + nImage
										});
									}
								}
							});
						}
					};
					dialog.closeCallback = function(event) {
						Kinky.shell.removeChild('/gallery/popup');
						KDialog.close(event);
						Kinky.shell.hideOverlay(true);
					};
					dialog.buttons = KWidget.CLOSE | KWidget.NEXT | KWidget.PREVIOUS;
					Kinky.shell.appendChild(dialog);
					Kinky.shell.panel.appendChild(dialog.panel);
				}
				
				if (image.data.popupURL) {
					image.onLoadPopup = function() {
						dialog.setStyle({
							width : this.data.popupWidth + 'px',
							marginTop : Math.round((KDOM.getBrowserHeight() - this.data.popupHeight) / 2) + 'px',
							height : '0px',
							left : Math.round(KDOM.getBrowserWidth() / 2 - this.data.popupWidth / 2) + 'px',
							top : '0px'
						});
						
						dialog.effects = {
							enter : {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : this.data.popupHeight
								},
								lock : {
									width : true
								}
							},
							exit : {
								f : KEffects.easeOutExpo,
								type : 'resize',
								duration : 200,
								go : {
									height : 0
								},
								lock : {
									width : true
								},
								onComplete : function(wDialog, tween) {
									Kinky.shell.removeChild(wDialog);
									Kinky.shell.hideOverlay();
									if (widget.paginationDispatchType == 'url') {
										KBreadcrumb.back();
									}
								}
							}
						};
						
						KDialog.show(dialog, {
							title : '',
							content : '<img src="' + this.data.popupURL + '"></img>'
						});
					};
					image.loadPopupImage();
				}
				else {
					dialog.setStyle({
						width : image.data.width + 'px',
						marginTop : Math.round((KDOM.getBrowserHeight() - image.data.height) / 2) + 'px',
						height : '0px',
						left : Math.round(KDOM.getBrowserWidth() / 2 - image.data.width / 2) + 'px',
						top : '0px'
					});
					
					dialog.effects = {
						enter : {
							f : KEffects.easeOutExpo,
							type : 'resize',
							duration : 200,
							go : {
								height : image.data.height
							},
							lock : {
								width : true
							}
						},
						exit : {
							f : KEffects.easeOutExpo,
							type : 'resize',
							duration : 200,
							go : {
								height : 0
							},
							lock : {
								width : true
							},
							onComplete : function(wDialog, tween) {
								Kinky.shell.removeChild(wDialog);
								Kinky.shell.hideOverlay();
								if (widget.paginationDispatchType == 'url') {
									KBreadcrumb.back();
								}
							}
						}
					};
					
					KDialog.show(dialog, {
						title : '',
						content : '<img src="' + image.data.url + '"></img>'
					});
				}
			}
		}
	};
	
	KSystem.included('KGallery');
});
function KHidden(parent, id) {
	if (/^widget/.test(id)) {
		throw Error('KHidden: input id cannot start with "widget".');
	}
	if (parent != null) {
		KWidget.call(this, parent);
		this.inputID = id;
		this.parentForm = null;
		this.validations = new Array();
		this.errorArea = {};
		
		this.input = window.document.createElement('input');
		{
			this.input.setAttribute('type', 'hidden');
			this.input.id = this.input.name = this.inputID + "Value";
		}
		this.panel.appendChild(this.input);
	}
}

KSystem.include([
	'KWidget'
], function() {
	KHidden.prototype = new KWidget();
	KHidden.prototype.constructor = KHidden;
	
	KHidden.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			value = !!value ? value : '';
			if ((typeof value == 'object') || (value instanceof Array)) {
				value = JSON.stringify(value);
			}
			for ( var index in this.validations) {
				if (!this.validations[index].regex.test(value)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}				
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KHidden.prototype.addValidator = function(test, message, options) {
		if (!(test instanceof RegExp)) {
			if (typeof test != 'string') {
				return;
			}
			test = new RegExp(test);
		}
		this.validations.push({
			regex : test,
			message : message,
			options : options
		});
	};
	
	KHidden.prototype.removeValidator = function(test) {
	};
	
	KHidden.prototype.addEventListener = function(eventName, callback, target) {
	};
	
	KHidden.prototype.focus = function(dispatcher) {
	};
	
	KHidden.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KHidden.prototype.getInput = function() {
		return this.childDiv(this.INPUT_ELEMENT);
	};
	
	KHidden.prototype.setLabel = function(text) {
	};

	KHidden.prototype.selectNextWidget = function(up) {
		var index = this.parent.indexOf(this);
		var next = undefined;

		if (up) {
			if (index == 0) {
				next = this.parent.childAt(this.parent.nChildren - 1);
			}
			else {
				next = this.parent.childAt(index - 1);
			}
		}
		else {
			if (index < this.parent.nChildren - 1) {
				next = this.parent.childAt(index + 1);
			}
			else {
				next = this.parent.childAt(0);
			}
		}

		if (next instanceof KFile) {
			return next.selectNextWidget(up);
		}
		if (next instanceof KHidden) {
			return next.selectNextWidget(up);
		}
		return next;
	};

	
	KHidden.prototype.setInnerLabel = function(text) {
	};
	
	KHidden.prototype.getErrorMessage = function(index) {
		return "";
	};
	
	KHidden.prototype.showErrorMessage = function(params) {
	};
	
	KHidden.prototype.go = function() {
		this.draw();
	};
	
	KHidden.prototype.clear = function() {
		this.input.value = '';
		KWidget.prototype.clear.call(this);
	};
	
	KHidden.prototype.createInput = function() {
		return null;
	};
	
	KHidden.prototype.createLabel = function() {
		return null;
	};
	
	KHidden.prototype.draw = function() {
		this.activate();
	};
	
	KHidden.prototype.setHelp = function(text, baloon) {
	};
	
	KHidden.prototype.getValue = function(rawValue) {
		if (this.input.value != '') {
			return JSON.parse(this.input.value);
		}
		else {
			return '';
		}
	};
	
	KHidden.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = JSON.stringify(value);
		}
	};
	
	KHidden.prototype.INPUT_ELEMENT = 'input';
	
	KSystem.included('KHidden');
});
function KHighlight(parent) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.link = window.document.createElement('a');
		this.link.target = '_self';
		this.link.href = 'javascript:void(0)';
		this.title = window.document.createElement('div');
		this.text = window.document.createElement('div');
		this.image = window.document.createElement('img');
	}
}

KSystem.include([
	'KTooltip',
	'KWidget'
], function() {
	KHighlight.prototype = new KWidget;
	KHighlight.prototype.constructor = KHighlight;
	
	KHighlight.prototype.setHighlightTitle = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.title.innerHTML = text;
			}
			else {
				this.title.innerHTML = '';
				this.title.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KHighlight.prototype.setHighlightText = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.text.innerHTML = text;
			}
			else {
				this.text.innerHTML = '';
				this.text.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KHighlight.prototype.setHighlightImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KHighlight.prototype.setHighlightURL = function(newURL) {
		if (newURL) {
			this.link.href = newURL;
		}
	};
	
	KHighlight.prototype.setHighlightTarget = function(target) {
		if (target) {
			this.link.target = target;
		}
	};
	
	KHighlight.prototype.setTooltip = function(tooltip) {
		if (tooltip) {
			this.data.tooltip = tooltip;
		}
	};
	
	KHighlight.prototype.initHighlight = function() {
		if (!!this.data.url) {
			this.setHighlightURL(this.data.url);
			
			if (!!this.data.targetWindow) {
				this.setHighlightTarget(this.data.targetWindow);
			}
		}
		
		if (!!this.data.tooltip) {
			this.setTooltip(this.data.tooltip);
		}
		
		if (!!this.data.title) {
			this.setHighlightTitle(this.data.title);
		}
		
		if (!!this.data.description) {
			this.setHighlightText(this.data.description, true);
		}
		
		if (!!this.data.image) {
			this.setHighlightImage(this.data.image);
		}
	};
	
	KHighlight.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!!this.data) {
			this.initHighlight();
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		this.title.className = ' KHighlightTitle ';
		this.text.className = ' KHighlightText ';
		this.image.className = ' KHighlightImage ';
		
		if ((!!this.data && !!this.data.image) || (this.image.src != '')) {
			this.link.appendChild(this.image);
		}
		
		if ((this.title.innerHTML.length == 0) && (!!this.data && !!this.data.title)) {
			this.setHighlightTitle(this.data.title, true);
		}
		this.link.appendChild(this.title);
		
		if ((this.text.innerHTML.length == 0) && (!!this.data && !!this.data.description)) {
			this.setHighlightText(this.data.description, true);
		}
		this.link.appendChild(this.text);
		
		if (!!this.data && !!this.data.tooltip) {
			this.hasTooltip = true;
			// KDOM.addEventListener(this.link, 'mouseover', KHighlight.onMouseOver);
		}
		
		this.content.appendChild(this.link);
		
		if (!!this.data && !!this.data.actions) {
			var ac;
			
			if (!!this.data.actions.length) {
				ac = this.data.actions;
			}
			else {
				ac = [
					this.data.actions
				];
			}
			
			for ( var link_data_index in ac) {
				var link_data = ac[link_data_index];
				
				var link = new KLink(this);
				var url = (!!link_data.url ? link_data.url : link_data.hash);
				if (!!url) {
					link.setLinkURL(url);
					
					if (!!link_data.title) {
						link.setLinkText(link_data.title);
					}
					
					this.appendChild(link);
					
					link.go();
				}
			}
		}
		
		this.activate();
	};
	
	KHighlight.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		
		if (widget.hasTooltip) {
			KTooltip.showTooltip(widget.link, {
				text : widget.data.tooltip,
				isHTML : true,
				offsetX : 15,
				offsetY : 15,
				left : KDOM.mouseX(mouseEvent),
				top : (KDOM.mouseY(mouseEvent) + 15),
				cssClass : 'KHighlightTooltip'
			});
		}
	};
	
	KHighlight.LINK_ELEMENT = 'link';
	KHighlight.IMAGE_ELEMENT = 'image';
	KHighlight.TITLE_ELEMENT = 'title';
	KHighlight.TEXT_ELEMENT = 'text';
	
	KSystem.included('KHighlight');
});
function KImage(parent, imageURL, popupURL) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.image = window.document.createElement('img');
		this.image.onload = function() {
			var widget = KDOM.getWidget(this);
			if (!widget) {
				return;
			}
			widget.width = this.width;
			widget.height = this.height;
		};
		this.content.appendChild(this.image);
		
		if (imageURL != null) {
			this.imageURL = imageURL;
		}
		if (popupURL != null) {
			this.popupURL = popupURL;
		}
	}
}

KSystem.include([
	'KWidget'
], function() {
	KImage.prototype = new KWidget();
	KImage.prototype.constructor = KImage;
	
	KImage.prototype.removeEventListener = function(eventName, callback, target) {
		if (eventName == 'load') {
			KDOM.removeEventListener(this.image, 'click', KImage.changeListener);
		}
		else {
			KWidget.prototype.removeEventListener.call(this, eventName, callback, target);
		}
	};
	
	KImage.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'load') {
			this.callback = callback;
			KDOM.addEventListener(this.image, 'click', KImage.changeListener);
		}
		else {
			KWidget.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};
	
	KImage.prototype.go = function() {
		if (!!this.imageURL) {
			this.draw();
		}
		else {
			KWidget.prototype.go.call(this);
		}
	};
	
	KImage.prototype.onLoad = function(data, request) {
		if (data.image) {
			this.imageURL = data.image;
		}
		if (data.description) {
			this.imageDesc = data.description;
		}
		if (data.imageScaled) {
			this.popupURL = data.imageScaled;
		}
		KWidget.prototype.onLoad.call(this, data);
	};
	
	KImage.prototype.setImage = function(imageURL) {
		this.imageURL = imageURL;
		if (this.activated()) {
			this.content.removeChild(this.image);
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.imageURL) : this.imageURL;
			this.content.appendChild(this.image);
		}
	};
	
	KImage.prototype.setPopupImage = function(popupURL) {
		this.popupURL = popupURL;
	};
	
	KImage.prototype.addMap = function(params) {
		if (this.map == null) {
			this.map = window.document.createElement('map');
			this.map.name = this.map.id = 'map' + this.id;
		}
		
		for ( var index in params.map) {
			var area = window.document.createElement('area');
			area.shape = (!!params.map[index].shape ? params.map[index].shape : 'poly');
			area.coords = params.map[index].coords;
			area.href = params.map[index].url;
			area.target = (!!params.map[index].target ? params.map[index].target : '_self');
			if (params.map[index].onOver) {
				KDOM.addEventListener(area, 'mouseover', params.map[index].onOver);
			}
			if (params.map[index].onOut) {
				KDOM.addEventListener(area, 'mouseout', params.map[index].onOut);
			}
			if (params.map[index].onClick) {
				KDOM.addEventListener(area, 'click', params.map[index].onClick);
			}
			this.map.appendChild(area);
		}
	};
	
	KImage.prototype.setTitle = function(text, isHTML) {
		if (isHTML) {
			this.image.title = this.image.alt = HTMLEntities.decode(text);
		}
		else {
			this.image.title = this.image.alt = text;
		}
	};
	
	KImage.prototype.setDescription = function(text, isHTML) {
		if (!this.p) {
			this.p = window.document.createElement('p');
			this.content.appendChild(this.p);
		}
		if (isHTML) {
			this.p.innerHTML = text;
		}
		else {
			this.p.appendChild(window.document.createTextNode(text));
		}
	};
	
	KImage.prototype.setImageName = function(text) {
		this.imageDesc = text;
	};
	
	KImage.prototype.setDefaultImage = function(imageURL) {
		this.defaultImage = imageURL;
	};
	
	KImage.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (this.imageDesc) {
			KDOM.addEventListener(this.image, 'mouseover', KImage.onMouseOver);
		}
		
		if (!!this.data && !!this.data.title) {
			this.setTitle(this.data.title);
		}
		
		if (!!this.data && !!this.data.description) {
			this.setDescription(this.data.title);
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		if (this.map) {
			this.image.useMap = '#' + this.map.id;
			this.content.appendChild(this.map);
		}
		
		this.activate();
		if (this.popupURL) {
			if ((KBrowserDetect.browser == 2) && (KBrowserDetect.version < 9)) {
				this.popup = new Image();
			}
			else {
				this.popup = window.document.createElement('img');
			}
			this.popup.src = this.popupURL;
		}
		this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.imageURL) : this.imageURL;
		KDOM.fireEvent(this.image, 'click');
	};
	
	KImage.onMouseOver = function(event) {
		
	};

	KImage.changeListener = function(e) {
		var w = KDOM.getEventWidget(e);
		w.changeTimer = KSystem.addTimer(function() {
			if (!w.image.width) {
				return;
			}

			KSystem.removeTimer(w.changeTimer);
			delete w.changeTimer;
			w.callback(e);
		}, 500, true);
	};

	
	KImage.IMAGE_ELEMENT = 'image';
	
	KSystem.included('KImage');
});

function KImageUpload(parent, label, id, options) {
	if (parent != null) {
		KFile.call(this, parent, label, id, options);
		this.maxFile = 1;
		this.noPrompt = true;
		this.metadata = undefined;
		this.onChangeCallback = [];
	}
}

KSystem.include([
	'KFile'
], function() {
	
	KImageUpload.prototype = new KFile();
	KImageUpload.prototype.constructor = KImageUpload;
	
	KImageUpload.prototype.visible = function() {
		KFile.prototype.visible.call(this);
		this.updateImage();
	};

	KImageUpload.prototype.removeEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			if (this.onChangeCallback.length != 0) {
				for (var c = 0; c != this.onChangeCallback.length; c++) {
					if (this.onChangeCallback[c].toString() == callback.toString()) {
						this.onChangeCallback.splice(c, 1);
						break;		
					}
				}
				if (this.onChangeCallback.length == 0) {
					KDOM.removeEventListener(this.button, 'click', KImageUpload.onChangeCallback);
				}
			}
		}
		else if (eventName == 'imageload') {
			KFile.prototype.removeEventListener.call(this, 'propertychange', callback, this.button)
		}
		else {
			KFile.prototype.removeEventListener.call(this, eventName, callback, target);
		}
	};

	KImageUpload.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			if (this.onChangeCallback.length == 0) {
				KFile.prototype.addEventListener.call(this, eventName, KImageUpload.onChangeCallback, target);
			}
			this.onChangeCallback.push(callback);
		}
		else if (eventName == 'imageload') {
			KFile.prototype.addEventListener.call(this, 'propertychange', callback, this.button)
		}
		else {
			KFile.prototype.addEventListener.call(this, eventName, callback, target);
		}
	};

	KImageUpload.prototype.setMetadata = function(metadata) {
		this.metadata = metadata;
	};
	
	KImageUpload.prototype.getMetadata = function() {
		return this.metadata;
	};
	
	KImageUpload.prototype.draw = function() {		
		this.addCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
		KFile.prototype.draw.call(this);
		
		this.button.innerHTML = gettext('$KIMAGEUPLOAD_BUTTON_UPLOAD_TEXT');
		
	};
	
	KImageUpload.prototype.updateValue = function() {
		KFile.prototype.updateValue.call(this);

		if (!this.getValue() || (this.getValue() == '')) {
			this.addCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
			this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_ADD_TEXT');
		}
		else {
			this.removeCSSClass(gettext('$KIMAGEUPLOAD_NO_IMAGE_THUMB'), this.content);
			this.buttonLabel.innerHTML = gettext('$KFILE_BUTTON_LABEL_CHANGE_TEXT');
		}
		this.updateImage();
	};

	KImageUpload.prototype.updateImage = function() {
		var w = this;
		this.width = 0;
		if (!!this.loadTimer) {
			KSystem.removeTimer(this.loadTimer);
			delete this.loadTimer;
		}
		this.loadTimer = KSystem.addTimer(function() {
			if (!w.fileListContainer) {
				if (!!w.contentContainer) {
					w.contentContainer.style.width = 'auto';
				}
				return;
			}

			var imgs = w.fileListContainer.getElementsByTagName('img');
			for (var i = 0; i != imgs.length; i++) {
				var s = KCSS.getComputedStyle(imgs[i]);
				var width = Math.round(KDOM.normalizePixelValue(s.width));
				var height = Math.round(KDOM.normalizePixelValue(s.height));
				if (!width || !height || width <= 0 || height <= 0) {
					return;
				}
				w.width = Math.max(w.width, width);
			}

			KSystem.removeTimer(w.loadTimer);
			delete w.loadTimer;
			w.contentContainer.style.width = (w.width != 0 ? w.width + 'px' : 'auto');
			KDOM.fireEvent(w.button, 'propertychange');
		}, 500, true);
	};
	
	KImageUpload.prototype.addFile = function(fileName, filePath) {
		var inputs = this.fileListContainer.getElementsByTagName('input');
		var parent = this.fileListContainer;
		for (; parent.childNodes.length != 0;) {
			parent.removeChild(parent.childNodes[0]);
		}
		
		var newFileContainer = window.document.createElement('div');
		
		var newFileLabel = window.document.createElement('div');
		{
			var img = window.document.createElement('img');
			img.src = filePath + '?etag=' + (new Date()).getTime();
			newFileLabel.appendChild(img);
		}
		newFileContainer.appendChild(newFileLabel);
		
		var newFile = window.document.createElement('input');
		{
			newFile.type = 'hidden';
			newFile.name = this.id + '[]';
			newFile.id = this.id + '_' + inputs.length;
			newFile.value = filePath;
		}
		newFileContainer.appendChild(newFile);
		
		var newFileRemove = window.document.createElement('button');
		{
			newFileRemove.setAttribute('type', 'button');
		}
		newFileContainer.appendChild(newFileRemove);
		
		KDOM.addEventListener(newFileRemove, 'click', function(event) {
			var widget = KDOM.getEventWidget(event);
			var target = KDOM.getEventTarget(event);
			target.parentNode.parentNode.removeChild(target.parentNode);
			widget.setValue(undefined);
			widget.updateValue();
		});
		
		newFileContainer.appendChild(window.document.createElement('br'));
		parent.appendChild(newFileContainer);
		if (this.activated()) {
			this.updateValue();
			KDOM.fireEvent(this.button, 'click');
		}
	};

	KImageUpload.onChangeCallback = function(e) {
		var self = KDOM.getEventWidget(e);
		for (var c in self.onChangeCallback) {
			self.onChangeCallback[c].call(null, e);
		}
	};

	KSystem.included('KImageUpload');	
});

function Kinky() {
	this.resizeListeners = {};
	this.widgetResizeListeners = {};
	this.lastWidth = 0;
	this.lastHeight = 0;
	
	this.widgets = new Object();
	this.widgetCount = 0;
	
	this.request = null;
	this.loggedUser = null;
	this.loggedFB = null;
	this.currentState = [
		'default'
	];
}
{
	Kinky.prototype.constructor = Kinky;
	
	Kinky.getInstance = function() {
		return Kinky.bunnyMan;
	};
	
	Kinky.config = function() {
		if (KSystem.hasPendingImports()) {
			KSystem.addTimer(Kinky.config, 500);
			return;
		}

		if (Kinky.bunnyMan.started) {
			return;
		}
		Kinky.bunnyMan.started = true;
		
		for ( var shell in Kinky.SHELL_CONFIG) {
			Kinky.SHELL_CONFIG[shell].id = shell;
		}
		
		Kinky.bunnyMan.widgets['proxy'] = Kinky.bunnyMan;
		Kinky.bunnyMan.widgets['picker'] = KOAuth.lockPicker;
		
		KDOM.addEventListener(window.document.body, 'click', function(event) {
			var mouseEvent = KDOM.getEvent(event);
			Kinky.BROWSER_CLICK = (!!Kinky.BROWSER_CLICK ? Kinky.BROWSER_CLICK : {});
			Kinky.BROWSER_CLICK.x = mouseEvent.clientX;
			Kinky.BROWSER_CLICK.y = mouseEvent.clientY;
			Kinky.BROWSER_CLICK.w = KDOM.getEventWidget(event);
			
			Kinky.releaseModal(event, true);
		});
		
		if (window.addEventListener) {
			window.addEventListener('DOMMouseScroll', Kinky.releaseModal, false);
		}
		window.document.onmousewheel = window.onmousewheel = Kinky.releaseModal;
		
		Kinky.init();
		window.kinky = Kinky.bunnyMan;
	};
	
	Kinky.init = function() {
		if (Kinky.ALLOW_EFFECTS) {
			KEffects.loadTimmingFunctions();
		}
		
		KBreadcrumb.sparrow = new KBreadcrumb();
		KSystem.addTimer(KBreadcrumb.onLocationChange, 0);
		KSystem.addTimer(Kinky.onResize, 0);
		KSystem.addTimer(Kinky.onResizeWidget, 0);
		
		var index = -1;
		if ((index = window.document.location.href.indexOf('access_token=')) != -1) {
			var indexOfE = window.document.location.href.indexOf('&', index);
			var accessToken = null;
			if (indexOfE == -1) {
				accessToken = window.document.location.href.substr(index + 13);
			}
			else {
				accessToken = window.document.location.href.substring(index + 13, indexOfE);
			}
			if (window.document.location.href.indexOf('#access_token=') != -1) {
				// REMOVED DUE TO PROBLEMS WHEN CONNECTING TO SEVERAL BACKENDS -> DO NOT UNCOMMENT
				/*KOAuth.session({
					authentication : 'OAuth2.0 ' + accessToken
				}, {}, 'GLOBAL');*/
				KBreadcrumb.dispatchURL({
					hash : ''
				});
			}
		}
		
		if (!!Kinky.SHELL_CONFIG) {
			for ( var shell in Kinky.SHELL_CONFIG) {
				if (!!Kinky.SHELL_CONFIG[shell].auth && !!Kinky.SHELL_CONFIG[shell].auth.service) {
					KOAuth.start(shell, !!Kinky.SHELL_CONFIG[shell].href);
				}
				else {
					if (!!Kinky.SHELL_CONFIG[shell].href) {
						Kinky.loadShell(Kinky.SHELL_CONFIG[shell]);
					}
					else {
						Kinky.SHELL_CONFIG[shell].id = shell;
						Kinky.SHELL_CONFIG[shell].href = shell;
						Kinky.SHELL_CONFIG[shell].requestTypes = [
							'application/vnd.kinky.' + Kinky.SHELL_CONFIG[shell].className
						];
						Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[shell]);
					}
				}
			}
		}
	};
	
	Kinky.loadShell = function(shellConfig, callbacks) {
		var cbs = [
			'Kinky.bunnyMan.start'
		];
		if (!!callbacks) {
			if (typeof callbacks == 'string') {
				cbs.push('Kinky.bunnyMan.' + callbacks);
			}
			else {
				for ( var cb in callbacks) {
					cbs.push('Kinky.getWidget(\'' + cb + '\').' + callbacks[cb]);
				}
			}
		}
		var service = shellConfig.preferredProvider + ':' + shellConfig.href;
		var request = {
			type : service.substring(0, service.indexOf(':')),
			namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
			service : service.substring(service.lastIndexOf(':') + 1),
			action : 'get',
			id : 'proxy',
			callback : cbs
		};
		
		try {
			KConnectionsPool.getConnection(request.type, shellConfig.id).send(request, null, shellConfig.headers);
		}
		catch (e) {
			KSystem.include(KConnectionsPool.connectors[request.type], function() {
				KConnectionsPool.getConnection(request.type, shellConfig.id).send(request, null, shellConfig.headers);
			});
		}
	};
	
	Kinky.setModal = function(widget, withScroll) {
		Kinky.MODAL_WIDGET.unshift({
			w : widget,
			s : !!withScroll
		});
	};
	
	Kinky.releaseModal = function(event, notWithScroll) {
		if (Kinky.MODAL_WIDGET.length != 0) {
			var widget = KDOM.getEventWidget(event);
			if (!widget || (widget.className == 'KFloatable')) {
				return;
			}
			
			var parent = KDOM.getEventWidget(event, Kinky.MODAL_WIDGET[0].w.className);
			if ((!parent || ((!!parent && (parent.id != Kinky.MODAL_WIDGET[0].w.id)))) && (notWithScroll || (!notWithScroll == Kinky.MODAL_WIDGET[0].s))) {
				var modal = Kinky.MODAL_WIDGET[0].w;
				if (!!modal.data && !!modal.data.params && !modal.data.params.modal) {
					return;
				}
				Kinky.MODAL_WIDGET.shift();
				modal.blur();
			}
		}
	};
	
	Kinky.getActiveModalWidget = function() {
		if (Kinky.MODAL_WIDGET.length == 0) {
			return null;
		}
		return Kinky.MODAL_WIDGET[0].w;
	};
	
	Kinky.unsetModal = function(widget) {
		if ((Kinky.MODAL_WIDGET.length != 0) && ((!widget) || (widget.id == Kinky.MODAL_WIDGET[0].w.id))) {
			var modal = Kinky.MODAL_WIDGET[0].w;
			Kinky.MODAL_WIDGET.shift();
			modal.blur();
			if (Kinky.MODAL_WIDGET.length != 0) {
				Kinky.MODAL_WIDGET[0].w.focus();
			}
		}
	};
	
	Kinky.getShell = function(shellName) {
		if (!!shellName) {
			return Kinky.shell[shellName];
		}
		else {
			if (KSystem.isIncluded('KShell')) {
				for ( var w in Kinky.bunnyMan.widgets) {
					if (Kinky.bunnyMan.widgets[w] instanceof KShell) {
						return Kinky.bunnyMan.widgets[w];
					}
				}
			}
			return null;
		}
	};
	
	Kinky.getDefaultShell = function() {
		if (!Kinky.SHELL_CONFIG || !Kinky.shell) {
			return undefined;
		}
		for (var shellName in Kinky.SHELL_CONFIG) {
			return Kinky.shell[shellName];
		}
	};
	
	Kinky.getShellConfig = function(shellName) {
		return Kinky.SHELL_CONFIG[shellName];
	};
	
	Kinky.dispatchState = function(stateName) {
		Kinky.bunnyMan.currentState.push(stateName);
		for ( var widget in Kinky.bunnyMan.widgets) {
			if (/widget([0-9]+)/.test(widget)) {
				Kinky.bunnyMan.widgets[widget].dispatchState(stateName);
			}
		}
		
	};
	
	Kinky.getWidget = function(widgetID) {
		return Kinky.bunnyMan.getWidget(widgetID);
	};
	
	Kinky.clearWidgets = function(shell) {
		return Kinky.bunnyMan.clearWidgets(shell);
	};
	
	Kinky.getLoggedUser = function() {
		return Kinky.bunnyMan.loggedUser;
	};
	
	Kinky.setLoggedFB = function(data) {
		Kinky.bunnyMan.loggedFB = data;
	};
	
	Kinky.getLoggedFB = function() {
		return Kinky.bunnyMan.loggedFB;
	};
	
	Kinky.clearSession = function(shellName) {
		delete Kinky.bunnyMan.loggedUser;
		delete Kinky.bunnyMan.loggedFB;
		KOAuth.destroy();
		if (!!shellName) {
			KOAuth.destroy(shellName);
		}
	};
	
	Kinky.startSession = function(userData) {
		if (userData) {
			Kinky.bunnyMan.loggedUser = userData;
		}
	};
	
	Kinky.onResize = function() {
		try {
			Kinky.bunnyMan.dispatchResize();
		}
		catch (e) {
		}
	};
	
	Kinky.onResizeWidget = function() {
		try {
			Kinky.bunnyMan.dispatchResizeWidget();
		}
		catch (e) {
			throw e;
		}
	};
	
	Kinky.prototype.start = function(config) {
		if (!Kinky.shell) {
			Kinky.shell = {};
		}
		var feClass = KSystem.getWRMLClass(config);
		KSystem.include(feClass, function() {
			var shell = KSystem.construct(config, -1);
			shell.parent = document.getElementById(Kinky.SHELL_CONFIG[config.id].parent);
			shell.data = config;
			
			var auth = KOAuth.getAuthorization();
			// var auth = KOAuth.getAuthorization(config.href);
			if (!auth) {
				auth = KOAuth.getAuthorization(config.id);
				// auth = KOAuth.getAuthorization();
			}
			if (!!auth) {
				if (!Kinky.SHELL_CONFIG[config.id].headers) {
					Kinky.SHELL_CONFIG[config.id].headers = {};
				}
				Kinky.SHELL_CONFIG[config.id].headers['Authorization'] = auth.authentication;
			}
			
			shell.start(Kinky.SHELL_CONFIG[config.id].startPage);
			Kinky.shell[config.id] = shell;
		}, (!!config.includeroot ? config.includeroot : null));
	};
	
	Kinky.prototype.dispatchResize = function() {
		var width = KDOM.getBrowserWidth();
		var height = KDOM.getBrowserHeight();
		if ((width != this.lastWidth) || (height != this.lastHeight)) {
			this.lastHeight = height;
			this.lastWidth = width;
			for ( var widget in this.resizeListeners) {
				for ( var index in this.resizeListeners[widget]) {
					this.resizeListeners[widget][index].call(null, this.widgets[widget], {
						width : width,
						height : height
					});
				}
			}
		}
		KSystem.addTimer(Kinky.onResize, 200);
	};
	
	Kinky.prototype.dispatchResizeWidget = function() {
		for ( var widget in this.widgetResizeListeners) {
			if (!this.widgets[widget].activated() || !this.widgets[widget].display) {
				continue;
			}
			var width = this.widgets[widget].getWidth();
			var height = this.widgets[widget].getHeight();
			
			if ((width != this.widgets[widget].lastWidth) || (height != this.widgets[widget].lastHeight)) {
				this.widgets[widget].lastHeight = height;
				this.widgets[widget].lastWidth = width;
				for ( var index in this.widgetResizeListeners[widget]) {
					this.widgetResizeListeners[widget][index].call(null, this.widgets[widget], {
						width : width,
						height : height
					});
				}
			}
		}
		KSystem.addTimer(Kinky.onResizeWidget, 200);
	};
	
	Kinky.prototype.addWindowResizeListener = function(widget, callback) {
		if (this.resizeListeners[widget.id]) {
			this.resizeListeners[widget.id].push(callback);
		}
		else {
			this.resizeListeners[widget.id] = new Array();
			this.resizeListeners[widget.id].push(callback);
		}
	};
	
	Kinky.prototype.addResizeListener = function(widget, callback) {
		if (this.widgetResizeListeners[widget.id]) {
			this.widgetResizeListeners[widget.id].push(callback);
		}
		else {
			this.widgetResizeListeners[widget.id] = new Array();
			this.widgetResizeListeners[widget.id].push(callback);
		}
	};
	
	// >>>
	// HTTP response handlers
	Kinky.prototype.onError = function(data, request) {
		console.log(data);
	};
	
	Kinky.prototype.onOk = function(data, request) {
	};
	
	Kinky.prototype.onHead = function(data, request) {
	};
	
	Kinky.prototype.onCreated = function(data, request) {
	};
	
	Kinky.prototype.onAccepted = function(data, request) {
	};
	
	Kinky.prototype.onNoContent = function(data, request) {
	};
	
	Kinky.prototype.onBadRequest = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onUnauthorized = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onForbidden = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onNotFound = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onPreConditionFailed = function(data, request) {
		this.onError(data);
	};
	
	Kinky.prototype.onInternalError = function(data, request) {
		this.onError(data);
	};
	// <<<
	
	Kinky.prototype.addWidget = function(widget, prefix) {
		var widgetID = (!!prefix ? prefix : 'widget') + (++this.widgetCount);
		this.widgets[widgetID] = widget;
		return widgetID;
	};
	
	Kinky.prototype.removeWidget = function(widgetID) {
		delete KBreadcrumb.sparrow.byWidget.location[widgetID];
		delete KBreadcrumb.sparrow.byWidget.query[widgetID];
		delete KBreadcrumb.sparrow.byWidget.action[widgetID];
		delete this.widgetResizeListeners[widgetID];
		delete this.resizeListeners[widgetID];
		delete this.widgets[widgetID];
	};
	
	Kinky.prototype.getWidget = function(widgetID) {
		return this.widgets[widgetID];
	};
	
	Kinky.prototype.clearWidgets = function(shell) {
		for ( var c in this.widgets) {
			if (!!this.widgets[c] && (!shell || (!!shell && (this.widgets[c].shell == shell)))) {
				this.widgets[c].unload();
			}
		}
	};
	
	Kinky.prototype.perform = function(widget, performative, service, content, headers, callback) {
		var type = service.substring(0, service.indexOf(':'));
		var callbacks = [];
		
		if (typeof callback == 'string') {
			callbacks.push('Kinky.getWidget(\'' + widget.id + '\').' + callback);
		}
		else {
			for ( var cb in callback) {
				callbacks.push('Kinky.getWidget(\'' + cb + '\').' + callback[cb]);
			}
		}
		
		if (KSystem.isIncluded(KConnectionsPool.connectors[type])) {
			var request = {
				type : service.substring(0, service.indexOf(':')),
				namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
				service : service.substring(service.lastIndexOf(':') + 1),
				action : performative,
				id : widget.id,
				callback : callbacks
			};
			if (widget instanceof KWidget) {
				widget.wait();
			}
			widget.getConnection(request.type).send(request, content, headers);
		}
		else {
			KSystem.include(KConnectionsPool.connectors[type], function() {
				var request = {
					type : service.substring(0, service.indexOf(':')),
					namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
					service : service.substring(service.lastIndexOf(':') + 1),
					action : performative,
					id : widget.id,
					callback : callbacks
				};
				if (widget instanceof KWidget) {
					widget.wait();
				}
				widget.getConnection(request.type).send(request, content, headers);
			});
		}
	};
	
	Kinky.prototype.get = function(widget, service, content, headers, callback) {
		this.perform(widget, 'get', service, content, headers, callback || 'onLoad');
	};
	
	Kinky.prototype.save = function(widget, service, content, headers, callback) {
		this.perform(widget, 'post', service, content, headers, callback || 'onSave');
	};
	
	Kinky.prototype.remove = function(widget, service, content, headers, callback) {
		this.perform(widget, 'delete', service, content, headers, callback || 'onRemove');
	};
	
	Kinky.prototype.put = function(widget, service, content, headers, callback) {
		this.perform(widget, 'put', service, content, headers, callback || 'onPut');
	};
	
	Kinky.prototype.post = function(widget, service, content, headers, callback) {
		this.perform(widget, 'post', service, content, headers, callback || 'onPost');
	};
	
	Kinky.prototype.head = function(widget, service, content, headers, callback) {
		this.perform(widget, 'head', service, content, headers, callback || 'onHead');
	};
	
	Kinky.MODAL_WIDGET = [];
	Kinky.INCLUDER_URL = null;
	Kinky.BASE_TEMP_URL = null;
	Kinky.BASE_TEMP_DIR = null;
	Kinky.SHELL_CONFIG = null;
	Kinky.ENTITIES = false;
	Kinky.URL_ENCODED = false;
	Kinky.SHOW_INSTANTIATIONS_ERRORS = false;
	Kinky.ALLOW_EFFECTS = false;
	Kinky.PERSISTENT_SESSION = false;
	Kinky.FB_APP_ID = null;
	Kinky.FB_APP_COOKIE_NAME = null;
	Kinky.FB_APP_URL = null;
	Kinky.BROWSER_CLICK = {
		w : null,
		x : 0,
		y : 0
	};
	Kinky.TITLE_CLEAR_BOTH = false;
	Kinky.DEV_MODE = true;
	
	Kinky.shell = null;
	Kinky.bunnyMan = new Kinky();
	Kinky.HTML_READY = 'html4';
	Kinky.CSS_READY = 'css2';
	Kinky.DUMP_MODE = 'console';
	Kinky.IMAGE_PROXY = null;
	Kinky.LOADER_IMAGE = '/images/loader.gif';
	
	KSystem.included('Kinky');
}
function KInputAction(parent, callback, label) {
	if (parent != null) {
		KAddOn.call(this, parent);
		this.callback = callback;
		this.label = label;
		this.parent.addCSSClass('KInputActionParent');
	}
}

KSystem.include([
	'KAddOn',
	'KAbstractInput'
], function() {
	KInputAction.prototype = new KAddOn();
	KInputAction.prototype.constructor = KInputAction;
	
	KInputAction.prototype.setDialogProps = function(props) {
		this.dialogProps = props;
	};
	
	KInputAction.prototype.draw = function() {
		var kmlID = this.id;
		var actionButton = window.document.createElement('button');
		actionButton.className = ' KInputActionButton ';
		actionButton.innerHTML = this.label || gettext('$KMEDIALIST_OPEN');
		KDOM.addEventListener(actionButton, 'click', this.callback || this.default_callback);
		var self = this;
		KDOM.addEventListener(this.parent, 'keypress', function(event) {
			event = KDOM.getEvent(event);
			if (event.keyCode == 13) {
				KDOM.stopEvent(event);
				var callback = self.callback || self.default_callback;
				callback(event);
			}			
		});
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(actionButton);
		}
		else {
			this.parent.container.appendChild(actionButton);
		}
	};

	KInput.prototype.default_callback = function(event) {
		KDOM.stopEvent(event);
		var input = KDOM.getEventWidget(event);
		if (input instanceof KAbstractInput) {
			input.validate();
		}
	};
	
	KSystem.registerAddOn('KInputAction');
	KSystem.included('KInputAction');
}, Kinky.ZEPP_INCLUDER_URL);
function KInput(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.panel.removeChild(this.input);
		delete this.input;
		
		this.input = window.document.createElement('input');
		this.input.setAttribute('type', 'text');
		this.input.name = id;
		this.input.id = id;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KInput.prototype = new KAbstractInput();
	KInput.prototype.constructor = KInput;
	
	KInput.prototype.setLength = function(length) {
		this.input.size = this.input.maxLength = length;
		this.addCSSClass('KInput' + (length < 5 ? 'Small' : (length < 9 ? 'Medium' : 'Big')) + 'Length');
	};
	
	KInput.prototype.createInput = function() {
		return this.input;
	};
	
	KInput.prototype.getValue = function() {
		return this.input.value;
	};
	
	KInput.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = value;
		}
	};
	
	KInput.prototype.INPUT_ELEMENT = 'input';
	KInput.LABEL_CONTAINER = 'label';
	KInput.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KInput');
});
function KJSONText(parent, label, id) {
	if (!!parent) {
		KTextArea.call(this, parent, label, id);
	}
}

KSystem.include([
	'KTextArea',
	'KAbstractInput'
], function() {
	
	KJSONText.prototype = new KTextArea();
	KJSONText.prototype.constructor = KJSONText;
	
	KJSONText.prototype.getValue = function() {
		return JSON.parse(this.textarea.value);
	};
	
	KJSONText.prototype.setValue = function(value) {
		if (!value) {
			this.textarea.value = '';
		}
		else {
			this.textarea.value = JSON.stringify(value);
		}
		KAbstractInput.prototype.setValue.call(this, value);
	};
	
	KSystem.included('KJSONText');
});
function KLayeredPanel(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.layout = KLayeredPanel.LINEAR;
		this.selected = null;
		this.addActionListener(KLayeredPanel.actions);
		
		this.title = window.document.createElement('h2');
		this.panel.insertBefore(this.title, this.content);
		
		this.nPanels = 0;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KLayeredPanel.prototype = new KWidget();
	KLayeredPanel.prototype.constructor = KLayeredPanel;
	
	KLayeredPanel.prototype.setTitle = function(title, isHTML) {
		this.title.innerHTML = title;
	};
	
	KLayeredPanel.prototype.appendTitleText = function(title, isHTML) {
		this.title.innerHTML += title;
	};
	
	KLayeredPanel.prototype.focus = function(dispatcher) {
		this.onChangeTab(dispatcher.hash);
		KWidget.prototype.focus.call(this, this);
	};
	
	KLayeredPanel.prototype.visible = function() {
		KWidget.prototype.visible.call(this);
		if (!!this.def) {
			var _this = this;
			KSystem.addTimer(function()  {
				KBreadcrumb.dispatchEvent(_this.id, {
					action : _this.def
				});
				_this.def = null;
			}, (!!this.getParent('KLayeredPanel') ? 300 : 100));
		}
	};

	KLayeredPanel.prototype.visibleTab = function(child) {
	};	
	
	KLayeredPanel.prototype.invisibleTab = function(child) {
	};	
	
	KLayeredPanel.prototype.draw = function() {
		if (!!this.header) {
			var width = 0;
			for ( var n = 0; n != this.header.childNodes.length; n++) {
				width += this.header.childNodes[n].offsetWidth;
			}
			var computed = this.header.offsetWidth;
			var offset = computed - width;
			
			if (offset < 0) {
				offset -= 40;
				var height = '0px';
				
				this.headerclipper = window.document.createElement('div');
				{
					KCSS.setStyle({
						position : 'relative',
						width : width + 'px',
						top : '0',
						left : '0px'
					}, [
						this.headerclipper
					]);
					height = this.header.offsetHeight + 'px';
					for (; this.header.childNodes.length != 0;) {
						this.headerclipper.appendChild(this.header.childNodes[0]);
					}
				}
				
				var outter = window.document.createElement('div');
				{
					KCSS.setStyle({
						position : 'absolute',
						overflow : 'hidden',
						top : '0',
						left : '20px',
						width : (this.header.offsetWidth - 40) + 'px',
						height : height
					}, [
						outter
					]);
				}
				
				this.header.appendChild(outter);
				outter.appendChild(this.headerclipper);
				
				KCSS.setStyle({
					height : height
				}, [
					this.header
				]);
				
				var back = window.document.createElement('button');
				{
					back.className = 'KLayeredPantlTabNavBack';
					back.innerHTML = '&lt;';
					KCSS.setStyle({
						width : '20px',
						height : height,
						position : 'absolute',
						top : '0',
						left : '0'
					}, [
						back
					]);
					var backtimer = 0;
					KDOM.addEventListener(back, 'mousedown', function(event) {
						var clipper = KDOM.getEventWidget(event).headerclipper;
						backtimer = KSystem.addTimer(function() {
							var left = KDOM.normalizePixelValue(clipper.style.left);
							if (left < 0) {
								clipper.style.left = (Math.abs(left) > 5 ? left + 5 : 0) + 'px';
							}
							else {
								KSystem.removeTimer(forwardimer);
							}
						}, 10, true);
					});
					KDOM.addEventListener(back, 'mouseup', function(event) {
						KSystem.removeTimer(backtimer);
					});
				}
				this.header.appendChild(back);
				
				var forward = window.document.createElement('button');
				{
					forward.className = 'KLayeredPantlTabNavForward';
					forward.innerHTML = '&gt;';
					KCSS.setStyle({
						width : '20px',
						height : height,
						position : 'absolute',
						top : '0',
						right : '0'
					}, [
						forward
					]);
					var forwardimer = 0;
					KDOM.addEventListener(forward, 'mousedown', function(event) {
						var clipper = KDOM.getEventWidget(event).headerclipper;
						forwardimer = KSystem.addTimer(function() {
							var left = KDOM.normalizePixelValue(clipper.style.left);
							if (left > offset) {
								clipper.style.left = (Math.abs(left - offset) > 5 ? left - 5 : left - (left - offset)) + 'px';
							}
							else {
								KSystem.removeTimer(backtimer);
							}
						}, 10, true);
					});
					KDOM.addEventListener(forward, 'mouseup', function(event) {
						KSystem.removeTimer(forwardimer);
					});
				}
				this.header.appendChild(forward);
				
				KCSS.setStyle({
					position : 'relative'
				}, [
					this.header
				]);
				
			}
		}
		
		KWidget.prototype.draw.call(this);
	};
	
	KLayeredPanel.prototype.addHeaderSeparator = function(content, css) {
		var sep = window.document.createElement('i');
		sep.className = ' KLayeredPanelTabNavigationSeparator ' + this.className + 'TabNavigationSeparator ' + (!!css ? css + ' ' : '');
		if (!!content) {
			sep.innerHTML = content;
		}
		if (this.activated() && !!this.headerclipper) {
			this.headerclipper.appendChild(sep);
		}
		else {
			this.header.appendChild(sep);
		}
	};

	KLayeredPanel.prototype.addPanel = function(element, title, isDefault) {
		var activator = null;
		element.opened = KLayeredPanel.opened;
		
		if (this.layout == KLayeredPanel.TABS) {
			if (!this.header) {
				this.header = window.document.createElement(this.tagNames[Kinky.HTML_READY][KLayeredPanel.HEADER_DIV]);
				this.header.className = ' KLayeredPanelTabNavigation ';
				this.panel.insertBefore(this.header, this.content);
			}
			
			activator = window.document.createElement('a');
			{
				activator.href = 'javascript:void(0)';
				KDOM.addEventListener(activator, 'click', function(event) {
					var widget = KDOM.getEventWidget(event, 'KLayeredPanel');
					KBreadcrumb.dispatchEvent(widget.id, {
						action : element.hash
					});
				});
				activator.style.cursor = 'pointer';
				activator.innerHTML = title;
				this.addCSSClass('KLayeredPanelTabTitle', activator);
				element.selector = activator;
				element.setStyle({
					display : 'none'
				});
			}
			if (this.activated() && !!this.headerclipper) {
				this.headerclipper.appendChild(activator);
			}
			else {
				this.header.appendChild(activator);
			}
			element.addCSSClass('KLayeredPanelItemContent');
		}
		else {
			activator = window.document.createElement('a');
			{
				activator.href = 'javascript:void(0)';
				KDOM.addEventListener(activator, 'click', function(event) {
					var widget = KDOM.getEventWidget(event, 'KLayeredPanel');
					KBreadcrumb.dispatchEvent(widget.id, {
						action : element.hash
					});
				});
				activator.style.cursor = 'pointer';
				activator.innerHTML = title;
				this.addCSSClass('KLayeredPanelItemTitle', activator);
				element.selector = activator;
			}
			if (!!this.container) {
				this.container.appendChild(activator);
			}
			else {
				this.content.appendChild(activator);
			}
			element.setStyle({
				overflow : 'hidden',
				height : '0px'
			});
			
			element.addCSSClass('KLayeredPanelTabContent');
		}
		element.layeredPanePosition = this.nChildren;
		this.nPanels++;
		
		element.abort = function() {
			if (this.parent.layout == KLayeredPanel.LINEAR) {
				if (!!this.parent.container) {
					this.parent.container.removeChild(this.selector);
				}
				else {
					this.parent.content.removeChild(this.selector);
				}
			}
			else {
				if (!!this.parent.headerclipper) {
					this.parent.headerclipper.removeChild(this.selector);
				}
				else {
					this.parent.header.removeChild(this.selector);
				}
			}
			eval(this.className + '.prototype.abort.call(this);');
		};
		
		this.appendChild(element);
		if (this.activated()) {
			element.go();
		}
		else {
			if (isDefault) {
				this.def = element.hash;
			}
		}
	};
	
	KLayeredPanel.prototype.transition_tab_show = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.tab_show) {
			tween = this.data.tween.tab_show;
		}
		if (this.layout == KLayeredPanel.TABS) {
			if (!direction) {
				direction = 1;
			}
			var width = this.getWidth();
			if (!tween) {
				element.setStyle({
					left : (-width * direction) + 'px'
				});
				tween = [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 400,
					lock : {
						y : true
					},
					from : {
						x : -width * direction
					},
					go : {
						x : 0
					},
					onComplete : function(w, tween) {
						w.parent.visibleTab(w);
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 800,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}];
			}
			element.setStyle({
				display : 'block'
			});
			KEffects.addEffect(element, tween);
		}
		else {
			var height = element.content.offsetHeight;
			if (!tween) {
				element.setStyle({
					height : '0px'
				});
				tween = {
					f : KEffects.easeOutExpo,
					type : 'resize',
					duration : 300,
					lock : {
						width : true
					},
					from : {
						height : 0
					},
					go : {
						height : height
					},
					onComplete : function(w, tween) {
						w.parent.visibleTab(w);
					}
				};
			}
			KEffects.addEffect(element, tween);
			
		}
	};
	
	KLayeredPanel.prototype.transition_tab_hide = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.tab_hide) {
			tween = this.data.tween.tab_hide;
		}
		if (this.layout == KLayeredPanel.TABS) {
			if (!direction) {
				direction = 1;
			}
			var width = this.getWidth();
			if (!tween) {
				tween = [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 400,
					lock : {
						y : true
					},
					from : {
						x : 0
					},
					go : {
						x : width * direction
					},
					onComplete : function(widget, t) {
						widget.setStyle({
							display : 'none'
						});
						widget.invisible();
						widget.parent.invisibleTab(widget);
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 200,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}];
			}
			KEffects.addEffect(element, tween);
		}
		else {
			var height = element.content.offsetHeight;
			if (!tween) {
				tween = {
					f : KEffects.easeOutExpo,
					type : 'resize',
					duration : 300,
					lock : {
						width : true
					},
					from : {
						height : height
					},
					go : {
						height : 0
					},
					onComplete : function(widget, t) {
						widget.invisible();
						widget.parent.invisibleTab(widget);
					}
				};
			}
			KEffects.addEffect(element, tween);
			
		}
	};
	
	KLayeredPanel.prototype.dispatchPrevious = function() {		
		if (!this.selected) {
			return;
		}
		var index = this.indexOf(this.childWidget(this.selected));
		var prev = undefined;
		while(index != -1 && !((prev = this.childAt(--index)) instanceof KPanel)) {
			prev = undefined;
		}
		if (!prev) {
			return;
		}

		KBreadcrumb.dispatchEvent(this.id, {
			action : prev.hash
		});
	};

	KLayeredPanel.prototype.dispatchNext = function() {		
		if (!this.selected) {
			return;
		}
		var index = this.indexOf(this.childWidget(this.selected));
		var next = undefined;
		while(index != this.nChildren && !((next = this.childAt(++index)) instanceof KPanel)) {
			next = undefined;
		}
		if (!next) {
			return;
		}

		KBreadcrumb.dispatchEvent(this.id, {
			action : next.hash
		});
	};

	KLayeredPanel.prototype.onChangeTab = function(action) {
		var child = this.childWidget(action);
		if (!!child) {
			if (this.selected == action) {
				if (this.layout == KLayeredPanel.TABS) {
					return;
				}
				
				child.removeCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), child.selector);
				this.transition_tab_hide(child);
				this.selected = null;
			}
			else {
				var direction = 0;
				
				var last = this.childWidget(this.selected);
				this.selected = action;
				
				child.addCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), child.selector);
				
				if (!!last) {
					last.removeCSSClass((this.layout == KLayeredPanel.TABS ? 'KLayeredPanelTabTitleSelected' : 'KLayeredPanelItemTitleSelected'), last.selector);
					direction = (this.indexOf(last) - this.indexOf(child) > 0 ? 1 : -1);
					
					this.transition_tab_hide(last, null, direction);
				}
				else {
					direction = 1;
				}
				this.transition_tab_show(child, null, direction);
			}
		}
	};
	
	KLayeredPanel.opened = function() {
		var lp = this.getParent('KLayeredPanel');
		return lp.selected == this.hash;
	};

	KLayeredPanel.actions = function(widget, action) {
		if (widget.childWidget(action)) {
			widget.onChangeTab(action);
		}
	};
	
	KLayeredPanel.closePanel = function(event) {
		var div = KDOM.getEventTarget(event).parentNode;
		var id = div.alt;
		var pane = KDOM.getEventWidget(event);
		pane.removeChild(pane.childWidget(id));
		div.parentNode.removeChild(div);
	};
	
	KLayeredPanel.TABS = 1;
	KLayeredPanel.LINEAR = 2;
	KLayeredPanel.HEADER_DIV = 'header';
	
	KLayeredPanel.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div',
			header : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div',
			header : 'header'
		}
	};
	
	KSystem.included('KLayeredPanel');
});
function KLayout(parent) {
	if (!!parent) {
		this.prefix = 'layout';
		KAddOn.call(this, parent);
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KLayout.prototype = new KAddOn();
	KLayout.prototype.constructor = KLayout;

	KLayout.prototype.addTarget = function(targetName) {
		if (!this.config) {
			this.config = {
				targets : new Object()
			};
		}
		else if (!this.config.targets) {
			this.config.targets = new Object();
		}
		this.config.targets[targetName] = {
			name : targetName
		};
	};

	KLayout.prototype.draw = function() {
		this.parent.addCSSClass(this.config.title, KWidget.CONTENT_DIV);
		this.parent.addCSSClass(this.config.title, this.parent.container);
		if (this.parent.activated()) {
			this.parent[this.config.title] = this.parent.content;
			this.addTargets(this.config.targets, this.parent.content);
		}
		else {
			this.parent[this.config.title] = this.parent.container;
			this.addTargets(this.config.targets, this.parent.container);
		}
	};

	KLayout.prototype.addTargets = function(targets, parentDiv) {
		for ( var target in targets) {
			this.parent[target] = window.document.createElement('div');
			{
				this.parent[target].style.position = 'relative';
				this.parent[target].className = ' ' + targets[target].name + ' ';
				this.parent[target].id = this.parent[target].alt = this.parent[target].name = this.id;
				parentDiv.appendChild(this.parent[target]);
				if (!!targets[target].targets) {
					this.addTargets(targets[target].targets, this.parent[target]);
				}
			}
		}
	};

	KLayout.getEventLayout = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		while ((element != null) && !/^layout([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}

		return element;
	};

	KLayout.getLayout = function(domElement) {
		var element = domElement;
		while ((element != null) && !/^layout([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		return element;
	};

	KSystem.registerAddOn('KLayout');
	KSystem.included('KLayout');
});

function KLink(parent) {
	if (parent != null) {
		this.isLeaf = true;
		KWidget.call(this, parent);
		
		this.link = window.document.createElement('a');
		this.link.target = '_self';
		this.link.href = 'javascript:void(0)';
		this.text = window.document.createElement('div');
		this.image = window.document.createElement('img');
	}
}

KSystem.include([
	'KTooltip',
	'KBaloon',
	'KWidget'
], function() {
	KLink.prototype = new KWidget;
	KLink.prototype.constructor = KLink;
	
	KLink.prototype.setLinkText = function(text, isHTML) {
		if (text) {
			if (isHTML) {
				this.text.innerHTML = text;
			}
			else {
				this.text.innerHTML = '';
				this.text.appendChild(window.document.createTextNode(text));
			}
		}
	};
	
	KLink.prototype.setLinkImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KLink.prototype.setLinkOverImage = function(imageURL) {
		if (imageURL) {
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(imageURL) : imageURL;
		}
	};
	
	KLink.prototype.setTooltip = function(tooltip) {
		if (tooltip) {
			this.data.tooltip = tooltip;
		}
	};
	
	KLink.prototype.setLinkURL = function(newURL) {
		if (newURL) {
			this.link.href = newURL;
		}
	};
	
	KLink.prototype.initLink = function() {
		this.setLinkURL(this.data.url);
		this.setTooltip(this.data.tooltip);
		this.setLinkImage(this.data.image);
		this.setLinkText((!!this.data.description ? this.data.description : this.data.title));
	};
	
	KLink.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!!this.data && !!this.data.url) {
			this.initLink();
		}
		
		if (!!this.data && !!this.data.graphics && !!this.data.graphics.background) {
			this.setBackground(this.data.graphics.background);
		}
		
		if (!!this.data && !!this.data.targetWindow) {
			this.link.target = this.data.targetWindow;
		}
		
		this.text.className = ' KLinkText ';
		this.image.className = ' KLinkImage ';
		
		if (!!this.data && !!this.data.image) {
			this.link.appendChild(this.image);
			this.image.src = Kinky.IMAGE_PROXY ? Kinky.IMAGE_PROXY + encodeURIComponent(this.data.image) : this.data.image;
			this.isImageLink = true;
		}
		
		if ((this.text.innerHTML.length == 0) && (!!this.data && !!this.data.description)) {
			this.setLinkText(this.data.description, true);
		}
		this.link.appendChild(this.text);
		
		if (!!this.data && !!this.data.tooltip) {
			KBaloon.make(this, {
				text : this.data.tooltip
			});
		}
		
		this.content.appendChild(this.link);
		
		this.activate();
	};
	
	KLink.onMouseOver = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var widget = KDOM.getEventWidget(event);
		
		if (widget.hasTooltip) {
			KTooltip.showTooltip(widget.link, {
				text : widget.data.tooltip,
				isHTML : true,
				offsetX : 15,
				offsetY : 15,
				left : KDOM.mouseX(mouseEvent),
				top : (KDOM.mouseY(mouseEvent) + 15),
				cssClass : 'KLinkImageTooltip'
			});
		}
	};
	
	KLink.LINK_ELEMENT = 'link';
	KLink.IMAGE_ELEMENT = 'image';
	KLink.TEXT_ELEMENT = 'text';
	
	KSystem.included('KLink');
});
function KList(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.addActionListener(KList.actions, [
			'/page/*',
			'/per-page/*'
		]);
		this.isLeaf = false;
		this.pages = {};
		this.page = 0;
		this.lastpage = -1;
		this.pageSize = 0;
		this.totalSize = null;
		this.autoRotate = 0;
		this.autoRotateTimer = null;
		this.noMorePages = false;
		
		this.header = window.document.createElement('div');
		{
			this.header.className = ' ' + this.className + 'ListHeader ';
		}
		this.panel.insertBefore(this.header, this.content);
		
		this.footer = window.document.createElement('div');
		{
			this.footer.className = ' ' + this.className + 'ListFooter ';
		}
		this.panel.appendChild(this.footer);
	}
}

KSystem.include([
	'KWidget',
	'KCombo'
], function() {
	KList.prototype = new KWidget();
	KList.prototype.constructor = KList;
	
	KList.prototype.load = function() {
		if (!!this.config && !!this.config.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.config.href, {}, headers);
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.onNoContent = function(data, request) {
		if (!this.feed_retrieved && !this.no_feed) {
			this.retrieveFeed(data, request);
			return;
		}
		else {
			this.no_feed = !this.data || !this.data.feed || ((this.data.feed.size == 0));
		}
		
		if (this.feed_retrieved && !this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
			return;
		}
		else {
			this.no_children = !this.data || !this.data.children || ((this.data.children.size == 0));
		}
		
		if (!this.data.children && !this.data.feed) {
			if (this.activated()) {
				this.afterNext();
			}
			else {
				this.abort();
			}
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.onLoad = function(data, request) {
		if (!!this.data && !!this.data.children && !!this.data.children.size) {
			this.totalSize = this.data.children.size;
		}
		if (!!data.autoRotate && (((data.autoRotate != '0')))) {
			this.autoRotate = parseInt(data.autoRotate);
		}
		if (!!data.pageSize && (((data.pageSize != '0')))) {
			if (!this.activated()) {
				this.silence();
				this.pageSize = parseInt(data.pageSize);
				this.data = data;
				
				KCache.commit(this.ref + '/data', data);
				if (this.pageSize != 0) {
					this.addPagination(this.header, this.totalSize);
					this.addPagination(this.footer, this.totalSize);
				}
			}
			if (!this.no_children && !this.children_retrieved) {
				this.retrieveChildren(data, request);
			}
			else if (!this.no_feed && !this.feed_retrieved) {
				this.retrieveFeed(data, request);
			}
		}
		else {
			KWidget.prototype.onLoad.call(this, data, request);
		}
	};
	
	KList.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed/' + this.page);
		this.data.feed = elements;
		this.feed_retrieved = true;
		
		if (!elements && !retrieved && !!this.data.template && !!this.data.template.widget && !!this.data.links && !!this.data.links.feed && !!this.data.links.feed.href) {
			if (!KSystem.isIncluded(this.data.template.widget.type)) {
				this.feed_retrieved = false;
				var id = this.id;
				KSystem.include([
					this.data.template.widget.type
				], function() {
					if (!!Kinky.getWidget(id)) {
						Kinky.getWidget(id).retrieveFeed();
					}
				});
				return;
			}
			var headers = null;
			var args = null;
			if (this.pageSize != 0) {
				args = {
					fields : 'elements,size',
					pageSize : this.pageSize,
					pageStartIndex : this.page * this.pageSize
				};
			}
			else {
				args = {
					fields : 'elements,size'
				};
			}
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.feed.href, args), {}, headers, 'onFeed');
		}
		else if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			if (!!elements) {
				this.totalSize = elements.size;
			}
			this.draw();
		}
		
	};
	
	KList.prototype.retrieveChildren = function(config_data, config_request) {
		var retrieved = this.children_retrieved;
		var elements = KCache.restore(this.ref + '/children/' + this.page);
		this.data.children = elements;
		this.children_retrieved = true;
		
		if (!elements && !this.isLeaf && !retrieved && !!this.data.links && !!this.data.links.children && !!this.data.links.children.href) {
			var headers = null;
			var args = null;
			if (this.pageSize != 0) {
				args = {
					fields : 'elements,size',
					pageSize : this.pageSize,
					pageStartIndex : this.page * this.pageSize
				};
			}
			else {
				args = {
					fields : 'elements,size'
				};
			}
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.children.href, args), {}, headers, 'onChildren');
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			if (!!elements) {
				this.totalSize = elements.size;
			}
			this.draw();
		}
	};
	
	KList.prototype.onChildren = function(data, request) {
		this.totalSize = data.size;
		this.addPage();
		KWidget.prototype.onChildren.call(this, data, request);
		if (!!request) {
			this.data.children.size = this.totalSize;
			KCache.commit(this.ref + '/children/' + this.page, this.data.children);
		}
	};
	
	KList.prototype.onFeed = function(data, request) {
		this.totalSize = data.size;
		this.addPage();
		this.data.feed = {};
		
		var config = {
			hash : this.hash,
			rel : "\/wrml-relations\/widgets\/{widget_id}",
			requestTypes : [
				"application\/json",
				"application\/vnd.kinky." + this.data.template.widget.type
			],
			responseTypes : [
				"application\/json",
				"application\/vnd.kinky." + this.data.template.widget.type
			],
			links : {
				self : {
					href : this.config.href,
					rel : "\/wrml-relations\/widgets\/{widget_id}"
				}
			}
		};
		
		for ( var l in data.elements) {
			var childConfig = KSystem.clone(config);
			var id = (!!this.pageSize && ((this.pageSize != 0)) ? this.page * this.pageSize + parseInt(l) : l);
			childConfig.hash += '/' + id;
			childConfig.links.self.href += '/' + id;
			
			childConfig.data = new Object();
			childConfig.data.feeditem = data.elements[l];
			for ( var t in this.data.template.translationTable) {
				var name = this.data.template.translationTable[t].srcField;
				var value = data.elements[l][name];
				childConfig.data[this.data.template.translationTable[t].dstField] = value;
				if (!value && ((name == 'id')) && !!data.elements[l]._id) {
					childConfig.data[this.data.template.translationTable[t].dstField] = data.elements[l]._id;
				}
			}
			this.data.feed[childConfig.hash] = childConfig;
		}
		if (!!request) {
			this.data.feed.size = this.totalSize;
			KCache.commit(this.ref + '/feed/' + this.page, this.data.feed);
		}
		
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};
	
	KList.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		this.pageVisible(this.page);
	};
	
	KList.prototype.destroy = function() {
		if (!!this.autoRotateTimer) {
			KSystem.removeTimer(this.autoRotateTimer);
		}
		KWidget.prototype.destroy.call(this);
	};
	
	KList.prototype.addPage = function(n_page) {
		if (n_page == null) {
			n_page = this.page;
		}
		
		var id = 'page' + n_page;
		if (!!this.pages[id]) {
			return;
		}
		
		this.pages[id] = window.document.createElement('div');
		{
			KCSS.addCSSClass('KListPage ' + this.className + 'Page ' + this.className + 'Page' + this.page, this.pages[id]);
			KCSS.setStyle({
				display : 'none',
				position : 'relative'
			}, [
				this.pages[id]
			]);
		}
		if (this.activated()) {
			this.content.appendChild(this.pages[id]);
		}
		else {
			this.container.appendChild(this.pages[id]);
		}
		
		if (!!this.totalSize && ((this.totalSize != 0)) && ((((this.totalSize == this.pageSize)) || ((Math.floor(this.totalSize / this.pageSize) <= n_page))))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
		
	};
	
	KList.prototype.transition_page_show = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.page_show) {
			tween = this.data.tween.page_show;
		}
		if (!direction) {
			direction = 1;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 0
				},
				go : {
					alpha : 1
				},
				onStart : function(w, t) {
					KCSS.setStyle({
						display : 'inline'
					}, [
						element
					]);
				},
				applyToElement : true
			};
		}
		
		KEffects.addEffect(element, tween);
	};
	
	KList.prototype.transition_page_hide = function(element, tween, direction) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.page_hide) {
			tween = this.data.tween.page_hide;
		}
		if (!direction) {
			direction = 1;
		}
		if (!tween) {
			tween = {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 1
				},
				go : {
					alpha : 0
				},
				onStart : function(w, t) {
					KCSS.setStyle({
						display : 'none'
					}, [
						element
					]);
				},
				applyToElement : true
			};
		}
		KEffects.addEffect(element, tween);
	};
	
	KList.prototype.pageVisible = function(n_page) {
		if (this.header.childNodes.length > 1) {
			this.header.childNodes[1].childNodes[0].value = n_page + 1;
			this.footer.childNodes[1].childNodes[0].value = n_page + 1;
			
			this.header.childNodes[1].childNodes[2].innerHTML = Math.ceil(this.totalSize / this.pageSize);
			this.footer.childNodes[1].childNodes[2].innerHTML = Math.ceil(this.totalSize / this.pageSize);
		}
		
		if (!!this.totalSize && (this.totalSize != 0) && ((this.totalSize == this.pageSize) || (Math.floor(this.totalSize / this.pageSize) <= n_page))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
		
		if (this.lastpage >= 0) {
			this.transition_page_hide(this.pages['page' + this.lastpage], null, this.lastpage > this.page ? 1 : -1);
			this.transition_page_show(this.pages['page' + n_page], null, this.lastpage > this.page ? 1 : -1);
		}
		else {
			KCSS.setStyle({
				display : 'inline'
			}, [
				this.pages['page' + n_page]
			]);
		}
		this.lastpage = n_page;
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
		
	};
	
	KList.prototype.appendChild = function(element, childDiv) {
		if (!!childDiv) {
			KWidget.prototype.appendChild.call(this, element, childDiv);
			return;
		}

		var n_page = 0;
		if (this.pageSize != 0 && !this.children_retrieved && !this.feed_retrieved) {
			n_page = Math.floor(this.nChildren / this.pageSize);
		}
		else {
			n_page = this.page;
		}
		this.addPage(n_page);
		
		var appended = KWidget.prototype.appendChild.call(this, element, this.pages['page' + n_page]);
		if (!!appended) {
			element.parentPage = n_page;
		}
	};
	
	KList.prototype.addPagination = function(parent, totalSize) {
		var prevButton = window.document.createElement('button');
		{
			prevButton.setAttribute("type", "button");
			prevButton.className = ' KListPaginationPrev ' + (this.className != 'KList' ? this.className + 'PaginationPrev ' : '');
			prevButton.appendChild(window.document.createTextNode('<'));
			KDOM.addEventListener(prevButton, 'click', KList.dispatchPrevious);
		}
		parent.appendChild(prevButton);
		
		var pagination = window.document.createElement('div');
		{
			pagination.className = ' KListPaginationPages ' + (this.className != 'KList' ? this.className + 'PaginationPages ' : '');
			var paginationNrPage = window.document.createElement('input');
			{
				paginationNrPage.setAttribute("type", "text");
				paginationNrPage.value = (this.page + 1);
				KDOM.addEventListener(paginationNrPage, "focus", function(event) {
					KDOM.getEventTarget(event).select();
				});
				KDOM.addEventListener(paginationNrPage, "keypress", KList.dispatchPage);
			}
			pagination.appendChild(paginationNrPage);
			
			pagination.appendChild(window.document.createTextNode(' / '));
			
			var paginationTotalPages = window.document.createElement('span');
			paginationTotalPages.appendChild(window.document.createTextNode(Math.ceil(this.totalSize / this.pageSize)));
			pagination.appendChild(paginationTotalPages);
		}
		parent.appendChild(pagination);
		
		var nextButton = window.document.createElement('button');
		{
			nextButton.setAttribute("type", "button");
			nextButton.className = ' KListPaginationNext ' + (this.className != 'KList' ? this.className + 'PaginationNext ' : '');
			nextButton.appendChild(window.document.createTextNode('>'));
			KDOM.addEventListener(nextButton, 'click', KList.dispatchNext);
		}
		parent.appendChild(nextButton);
		
		var pageSizeSelection = new KCombo(this, '', this.id + 'PageSize');
		{
			pageSizeSelection.addCSSClass('KListPageSize ' + (this.className != 'KList' ? this.className + 'PageSize' : ''));
			pageSizeSelection.addEventListener('change', KList.dispatchPageSize);
			for ( var index = 1; index != 6; index++) {
				pageSizeSelection.addOption(this.pageSize * index, this.pageSize * index);
			}
		}
		parent.appendChild(pageSizeSelection.panel);
		pageSizeSelection.go();
	};
	
	KList.prototype.gotoPage = function(n_page) {
		
		if (this.page == n_page) {
			return;
		}
		else if (!!this.totalSize && (this.totalSize != 0) && (Math.floor(this.totalSize / this.pageSize) < n_page)) {
			this.afterNext();
			return;
		}
		else if (n_page < 0) {
			this.beforePrevious();
			return;
		}
		
		this.children_retrieved = false;
		this.feed_retrieved = false;
		
		this.page = n_page;
		if (!!this.pages['page' + n_page]) {
			this.pageVisible(this.page);
		}
		else if (!this.noMorePages) {
			this.onLoad(KCache.restore(this.ref + '/data'));
		}
		else {
			this.afterNext();
		}
	};
	
	KList.prototype.onNextPage = function() {
	};
	
	KList.prototype.onPreviousPage = function() {
	};
	
	KList.prototype.afterNext = function() {
	};
	
	KList.prototype.beforePrevious = function() {
	};
	
	KList.prototype.destroy = function() {
		if (!!this.autoRotateTimer) {
			KSystem.removeTimer(this.autoRotateTimer);
		}
		KWidget.prototype.destroy.call(this);
	};
	
	KList.dispatchNext = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event, 'KList');
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		if (widget.noMorePages || widget.page + 1 == Math.ceil(widget.totalSize / widget.pageSize)) {
			widget.afterNext();
		}
		else {
			var dispatchPage = widget.page + 1;
			KBreadcrumb.dispatchEvent(widget.id, {
				action : '/page/' + dispatchPage
			});
		}
	};
	
	KList.dispatchPrevious = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		if (widget.page == 0) {
			try {
				widget.beforePrevious();
				return;
			}
			catch (e) {
				return;
			}
		}
		var dispatchPage = widget.page - 1;
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/' + dispatchPage
		});
	};
	
	KList.dispatchLast = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event, 'KList');
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}

		var n_pages = Math.ceil(widget.totalSize / widget.pageSize);
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/' + (n_pages - 1)
		});
	};
	
	KList.dispatchFirst = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}

		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/page/0'
		});
	};

	KList.dispatchPageSize = function(event, widget) {
		if (widget == null) {
			widget = KDOM.getEventWidget(event);
		}
		if (!!widget.autoRotateTimer) {
			KSystem.removeTimer(widget.autoRotateTimer);
		}
		var select = KDOM.getEventTarget(event);
		KBreadcrumb.dispatchEvent(widget.id, {
			action : '/per-page/' + select.options[select.selectedIndex].value
		});
	};
	
	KList.dispatchPage = function(event) {
		var keyCode = event.keyCode;
		if (!keyCode || (keyCode == 13)) {
			var widget = KDOM.getEventWidget(event, 'KList');
			if (!!widget.autoRotateTimer) {
				KSystem.removeTimer(widget.autoRotateTimer);
			}
			var page = (!!KDOM.getEventTarget(event).value ? KDOM.getEventTarget(event).value : KDOM.getEventTarget(event).innerHTML);
			widget.gotoPage((parseInt(page) - 1));
		}
	};
	
	KList.rotate = function(id) {
		var widget = Kinky.getWidget(id);
		widget.gotoPage(widget.page + 1);
	};
	
	KList.actions = function(widget, action) {
		var baseAction = '/' + action.split('/')[1];
		
		switch (baseAction) {
			case '/per-page': {
				var pageSize = parseInt(action.split('/')[2]);
				widget.page = 0;
				widget.pageSize = pageSize;
				widget.refresh();
				break;
			}
			case '/page': {
				var n_page = parseInt(action.split('/')[2]);
				widget.gotoPage(n_page);
				break;
			}
		}
		return;
	};
	
	KList.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div',
			header : 'div',
			footer : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div',
			header : 'header',
			footer : 'footer'
		}
	};
	
	KSystem.included('KList');
});
function KLocale() {
	this.translations = {};
	this.failures = '';
}
{
	KLocale.prototype = {};
	KLocale.prototype.constructor = KLocale;
	
	KLocale.set = function(lang, key, value) {
		if (!KLocale.speeky.translations[lang]) {
			KLocale.speeky.translations[lang] = {};
		}
		KLocale.speeky.translations[lang][key] = value;
	};
	
	KLocale.get = function(lang, key) {
		if (!KLocale.speeky.translations[lang] || !KLocale.speeky.translations[lang][key]) {
			KLocale.speeky.failures += 'settext(\'' + key + '\', \'\', \'' + lang + '\');\n';
			return key;
		}
		return KLocale.speeky.translations[lang][key];
	};
	
	KLocale.speeky = new KLocale();
	KLocale.LANG = 'pt';
	KLocale.LOCALE = 'pt_PT';
	KLocale.DATE_FORMAT = 'Y-M-d H:i';

	if (!!window.top && !!window.top.document && !!window.top.document.location) {
		var uriparts = window.top.document.location.href.split('/');
		for(var index = uriparts.length - 1; index != 0; index--) {
			if (uriparts[index].length == 2) {
				KLocale.LANG = uriparts[index];
			}
		}
	}

	KSystem.included('KLocale');
}

function gettext(key, lang) {
	if (!lang) {
		lang = KLocale.LANG;
	}
	return KLocale.get(lang, key);
}

function settext(key, value, lang) {
	return KLocale.set(lang, key, value);
}

function getlocale(obj, field, locale) {
	if (!locale) {
		locale = KLocale.LOCALE;
	}
	if (!!obj.locales) {
		var value = undefined;
		try {
			eval('value = obj.locales[locale]' + (!!field ? '.' + field : ''));
		}
		catch(e) {}
		
		return value;
	}
	return undefined;
}


function KMap(parent, label, id) {
	if (parent) {
		this.isLeaf = true;

		KWidget.call(this, parent);

		this.map = null;

		this.mapMarkers = {};
		this.mapMarkersCount = 0;

		this.showInfoWindow = true;

		this.latlng = new google.maps.LatLng(38.708324327636795, -9.135032854173915);
		this.mapType = google.maps.MapTypeId.ROADMAP;
		this.initialZoom = 13;
		this.doubleClickZoom = true;
		this.draggable = true;
		this.streetView = false;

		this.draggableMarkers = false;

		this.infowindow = new google.maps.InfoWindow();

		this.linkContainer = document.createElement('div');
		this.mapContainer = document.createElement('div');
	}
}

KSystem.include([ 'KWidget', 'KHighlight' ], function() {
	KMap.prototype = new KWidget();
	KMap.prototype.constructor = KMap;

	KMap.prototype.draw = function() {
		if (!!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}

		this.mapContainer.className = 'KMapParent';
		this.content.appendChild(this.mapContainer);

		this.linkContainer.className = 'KMapLinks';
		this.content.appendChild(this.linkContainer);

		this.initializeMap();

		this.activate();
	};

	KMap.prototype.initializeMap = function() {
		if (!!this.data) {
			if (this.data.latitude != null && this.data.longitude != null) {
				eval('this.latlng = new google.maps.LatLng(' + this.data.latitude + ',' + this.data.longitude + ');');
			}

			if (this.data.mapType != null) {
				eval('this.mapType = ' + this.data.mapType + ';');
			}

			if (this.data.initialZoom != null) {
				this.initialZoom = this.data.initialZoom;
			}

			if (this.data.doubleClickZoom != null) {
				this.doubleClickZoom = this.data.doubleClickZoom && this.data.doubleClickZoom != "false";
			}

			if (this.data.draggable != null) {
				this.draggable = this.data.draggable && this.data.draggable != "false";
			}

			if (this.data.streetView != null) {
				this.streetView = this.data.streetView && this.data.streetView != "false";
			}

			if (this.data.draggableMarkers != null) {
				this.draggableMarkers = this.data.draggableMarkers && this.data.draggableMarkers != "false";
			}
		}

		var myOptions = {
			center : this.latlng,
			mapTypeId : this.mapType,
			zoom : this.initialZoom,
			disableDoubleClickZoom : !this.doubleClickZoom,
			draggable : this.draggable,
			streetViewControl : this.streetView,
			mapTypeControlOptions : {
				style : google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			navigationControlOptions : {
				style : google.maps.NavigationControlStyle.SMALL
			}
		};

		this.map = new google.maps.Map(this.mapContainer, myOptions);

		if (!!this.data.markers) {
			var ac;

			if (!!this.data.markers.length) {
				ac = this.data.markers;
			} else {
				ac = [ this.data.markers ];
			}

			for ( var index in ac) {
				if (ac[index].latitude != null && ac[index].longitude != null) {
					var latLng = null;
					eval('latLng = new google.maps.LatLng(' + ac[index].latitude + ',' + ac[index].longitude + ');');

					var marker = this.placeMarker(latLng, ac[index]);

					var link = new KHighlight(this);
					link.marker = marker;

					if(ac[index].isDefault) {

						this.activeHighlight = link;
						this.activeHighlight.addCSSClass('KHighlightSelected');
						this.map.setCenter(link.marker.position);
					}

					if (!!ac[index].title)
						link.setHighlightTitle(ac[index].title);

					if (!!ac[index].description)
						link.setHighlightText(ac[index].description, true);

					link.addEventListener('click', function(event) {
						var widget = KDOM.getEventWidget(event);
						var topWidget = widget.getParent('KMap');

						topWidget.map.setCenter(widget.marker.position);

						if (topWidget.activeHighlight != null) {
							topWidget.activeHighlight.removeCSSClass('KHighlightSelected');
						}

						topWidget.activeHighlight = widget;
						topWidget.activeHighlight.addCSSClass('KHighlightSelected');

						if (topWidget.showInfoWindow) {
							if (!topWidget.infowindow)
								topWidget.infowindow = new google.maps.InfoWindow();

							topWidget.infowindow.open(topWidget.map, widget.marker);
						}
					}, KHighlight.LINK_ELEMENT);

					this.appendChild(link, 'linkContainer');

					link.go();
				}
			}
		}
	};

	KMap.prototype.placeMarker = function(latLng, pointObject) {
		var marker = new google.maps.Marker({
			map : this.map,
			position : latLng,
			title : pointObject.title,
			clickable : true,
			draggable : this.draggableMarkers && (pointObject.draggable == null || pointObject.draggable != 0)
		});

		marker.index = this.mapMarkersCount++;
		marker.name = 'marker' + marker.index;

		if (!!pointObject && !!pointObject.icon && pointObject.icon != '')
			marker.setIcon(pointObject.icon);
		else if (!!this.data && !!this.data.icon && this.data.icon != '')
			marker.setIcon(this.data.icon);

		marker.ballon = !!pointObject.ballon ? pointObject.ballon : pointObject.description;

		this.mapMarkers[marker.name] = marker;

		var topWidget = this;
		google.maps.event.addListener(marker, 'click', function() {
			if (topWidget.showInfoWindow) {
				if (!topWidget.infowindow)
					topWidget.infowindow = new google.maps.InfoWindow();

				topWidget.infowindow.setContent(this.ballon);
				topWidget.infowindow.open(topWidget.map, this);
			}
		});

		if (marker.getDraggable()) {
			marker.dragging = false;

			google.maps.event.addListener(marker, 'dragstart', function() {
				marker.dragging = true;
				topWidget.infowindow.close();
			});

			google.maps.event.addListener(marker, 'dragend', function() {
				this.dragging = false;
				topWidget.setCenter(this.position);
			});
		}

		return marker;
	};

	KMap.prototype.clearMarkers = function() {
		this.infowindow.close();

		for ( var i = 0; i < this.mapMarkersCount; i++) {
			this.mapMarkers['marker' + i].setMap(null);
			delete this.mapMarkers['marker' + i];
		}

		this.mapMarkers = new Object();
		this.mapMarkersCount = 0;
	};

	KMap.prototype.visible = function() {
		KWidget.prototype.visible.call(this);

		var center = this.map.getCenter();
		google.maps.event.trigger(this.map, 'resize');
		this.map.setZoom(this.map.getZoom());
		this.map.setCenter(center);
	};

	KMap.LINK_CONTAINER = 'linkContainer';
	KMap.MAP_CONTAINER = 'mapContainer';

	KSystem.included('KMap');
});
function KMediaList(parent, params) {
	if (parent != null) {
		KAddOn.call(this, parent);
		KMediaList.selectedServer = '';
		KMediaList.selectedRoot = '';
		this.dialogProps = {
			modal : true,
			width : 800,
			scale : {
				height : 0.8
			},
			buttons : 'close'
		};
		this.params = params;
	}
}

KSystem.include([
	'KPanel',
	'KList',
	'KConfirmDialog',
	'KLink'
], function() {
	KMediaList.prototype = new KAddOn();
	KMediaList.prototype.constructor = KMediaList;
	
	KMediaList.prototype.setDialogProps = function(props) {
		this.dialogProps = props;
	};
	
	KMediaList.prototype.draw = function() {
		var kmlID = this.id;
		var mediaButton = window.document.createElement('button');
		mediaButton.className = 'KMediaListButton';
		mediaButton.innerHTML = gettext('$KMEDIALIST_OPEN');
		KDOM.addEventListener(mediaButton, 'click', function(event) {
			KDOM.stopEvent(event);
			Kinky.getWidget(kmlID).drawDialog();
		});
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(mediaButton);
		}
		else {
			this.parent.container.appendChild(mediaButton);
		}
	};
	
	KMediaList.prototype.drawDialog = function() {
		var kmlID = this.id;
		
		var panel = new KPanel(this.getShell());
		panel.parentID = kmlID;
		panel.addCSSClass('KMediaList');
		
		panel.breadcrumbPanel = window.document.createElement('div');
		{
			panel.breadcrumbPanel.className = ' KMediaListHeader ';
		}
		panel.content.appendChild(panel.breadcrumbPanel);
		
		panel.actions = new KPanel(panel);
		panel.appendChild(panel.actions);
		this.createActionsPanel(panel.actions);
		
		panel.fileOptions = new KPanel(panel);
		panel.appendChild(panel.fileOptions);
		this.createFileOptionsPanel(panel.fileOptions);
		
		var ypURL = this.params.ypURL;
		var serverList = new KList(panel);
		serverList.setTitle(gettext('$KMEDIALIST_TITLE'));
		serverList.load = function() {
			this.kinky.get(this, ypURL, {});
		};
		
		serverList.onLoad = function(data, request) {
			this.data = {
				serverData : data
			};
			this.draw();
		};
		
		serverList.draw = function() {
			for ( var index in this.data.serverData) {
				if (index == 'http') {
					continue;
				}
				var serverLink = new KLink(this);
				serverLink.setLinkText(this.data.serverData[index]);
				serverLink.data = {
					serverIndex : index
				};
				serverLink.addEventListener('click', function(e) {
					var widget = KDOM.getEventWidget(e);
					var parentWidget = KDOM.getEventWidget(e, 'KPanel');
					
					if (KMediaList.selectedServer != widget.data.serverIndex) {
						KMediaList.selectedServer = widget.data.serverIndex;
						if (widget.parent.selectedServer) {
							widget.parent.selectedServer.removeCSSClass('Selected');
						}
						parentWidget.actions.setStyle({
							display : 'block'
						});
						widget.parent.selectedServer = widget;
						widget.addCSSClass('Selected');
						KMediaList.selectedRoot = '';
						parentWidget.fileList = parentWidget.fileList.refresh();
						parentWidget.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
					}
				});
				this.appendChild(serverLink);
			}
			KList.prototype.draw.call(this);
		};
		panel.appendChild(serverList);
		
		panel.invisible = function() {
			KMediaList.selectedServer = '';
			KMediaList.selectedRoot = '';
			if (!this.display) {
				return;
			}
			this.display = false;
		};
		
		panel.fileList = new KMediaFileList(panel);
		panel.fileList.config = {
			jsClass : 'KMediaFileList'
		};
		panel.appendChild(panel.fileList);
		
		KMediaList.parent = this.parent;
		var dialog = KFloatable.make(panel, this.dialogProps);
		{
			dialog.addCSSClass('KMediaFloatableDialog');
		}
		KFloatable.show(dialog);
	};
	
	KMediaList.prototype.createActionsPanel = function(container) {
		var widget = this;
		var kmlID = this.id;
		
		container.setStyle({
			display : 'none'
		});
		container.addCSSClass('KMediaListActions');
		{
			container.addFile = new KFile(container, gettext('$KMEDIALIST_NEWFILE'), 'newFile');
			container.addFile.setSize(206, 40);
			container.addFile.addFile = function(fileName, filePath) {
				KFile.prototype.addFile.call(this, fileName, filePath);
				widget.refreshFileList();
			}
			container.appendChild(container.addFile);
			
			var addFolderInput = new KInput(container, gettext('$KMEDIALIST_NEWFOLDER'), 'newFolder');
			var addFolderBt = window.document.createElement('button');
			addFolderBt.innerHTML = gettext('$KMEDIALIST_CREATEFOLDER');
			KDOM.addEventListener(addFolderBt, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				if (widget.getValue() != '') {
					widget.removeCSSClass('KAbstractInputError');
					Kinky.getWidget(kmlID).createFolder(widget.getValue());
					widget.setValue('');
				}
				else {
					widget.addCSSClass('KAbstractInputError');
				}
			});
			container.appendChild(addFolderInput);
			addFolderInput.content.appendChild(addFolderBt);
		}
	};
	
	KMediaList.prototype.createFileOptionsPanel = function(container) {
		
		var kmlID = this.id;
		
		container.setTitle(gettext('$KMEDIALIST_SELECTEDFILES_OPTIONS'));
		container.setStyle({
			display : 'none'
		});
		container.addCSSClass('KMediaListFileOptions');
		{
			var removeBT = window.document.createElement('button');
			removeBT.className = 'KMediaListFileOptionsButton';
			removeBT.innerHTML = gettext('$KMEDIALIST_REMOVE');
			KDOM.addEventListener(removeBT, 'click', function(event) {
				var fileList = KDOM.getEventWidget(event, 'KPanel').parent.fileList;
				KDOM.stopEvent(event);
				Kinky.getWidget(kmlID).deleteFiles(fileList.elementsSelected);
			});
			container.content.appendChild(removeBT);
			
			container.externalBT = window.document.createElement('button');
			container.externalBT.className = 'KMediaListFileOptionsButton';
			container.externalBT.innerHTML = gettext('$KMEDIALIST_EXTERNAL');
			KDOM.addEventListener(container.externalBT, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widget = KDOM.getEventWidget(event);
				
				KDOM.stopEvent(event);
				
				var externalLink = new KInput(widget.getShell(), gettext('$KMEDIALIST_EXTERNAL_TEXT'), 'external');
				externalLink.addCSSClass('ExternalLink');
				externalLink.setValue(element.name);
				externalLink.input.readOnly = true;
				externalLink.addEventListener('click', function(ev) {
					var widget = KDOM.getEventWidget(ev);
					widget.input.select();
				});
				
				var dialog = KFloatable.make(externalLink, {
					modal : true,
					width : 300,
					scale : {
						height : 0.95
					}
				});
				{
					dialog.addCSSClass('KMediaListFloatableOptions');
				}
				KFloatable.show(dialog);
			});
			container.content.appendChild(container.externalBT);
			
			container.renameBT = window.document.createElement('button');
			container.renameBT.className = 'KMediaListFileOptionsButton';
			container.renameBT.innerHTML = gettext('$KMEDIALIST_RENAME');
			KDOM.addEventListener(container.renameBT, 'click', function(event) {
				var parentWidget = KDOM.getEventWidget(event);
				var element = KDOM.getEventTarget(event);
				element.style.display = 'none';
				parentWidget.childWidget('/media-actions-rename').setStyle({
					display : 'block'
				});
			});
			container.content.appendChild(container.renameBT);
			
			var renameInput = new KInput(container, gettext('$KMEDIALIST_RENAME'), 'rename');
			renameInput.hash = '/media-actions-rename';
			var renameInputBt = window.document.createElement('button');
			renameInputBt.innerHTML = gettext('$KMEDIALIST_RENAME_BT');
			KDOM.addEventListener(renameInputBt, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				var fileList = KDOM.getEventWidget(event, 'KPanel').parent.fileList;
				var fileToRename = '';
				for ( var index in fileList.elementsSelected) {
					fileToRename = fileList.elementsSelected[index];
				}
				if (widget.getValue() != fileToRename) {
					Kinky.getWidget(kmlID).renameFile(fileToRename, widget.getValue());
				}
			});
			container.appendChild(renameInput);
			renameInput.content.appendChild(renameInputBt);
			renameInput.setStyle({
				display : 'none'
			});
		}
	};
	
	KMediaList.prototype.createFolder = function(name) {
		var params = {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot,
			folderName : name
		};
		Kinky.bunnyMan.post(this, 'rest:' + (!!Kinky.FILE_MANAGER_NEWFOLDER ? Kinky.FILE_MANAGER_NEWFOLDER : '/zepp/filemanager/newfolder'), params, null, 'refreshFileList');
	};
	
	KMediaList.prototype.deleteFiles = function(filesToDelete) {
		var self = this;

		KConfirmDialog.confirm({
			question : gettext('$KMEDIALIST_DELETE_CONFIRM'),
			callback : function() {
				var params = {
					serverIndex : KMediaList.selectedServer,
					fileRoot : KMediaList.selectedRoot,
					files : filesToDelete
				};
				Kinky.bunnyMan.post(self, 'rest:' + (!!Kinky.FILE_MANAGER_DELETE ? Kinky.FILE_MANAGER_DELETE : '/zepp/filemanager/deletefile'), params, null, 'refreshFileList');
			},
			shell : self.shell,
			modal : true,
			width : 400
		});
	};
	
	KMediaList.prototype.renameFile = function(fileName, newName) {
		var params = {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot,
			fileName : fileName,
			newName : newName
		};
		Kinky.bunnyMan.post(this, 'rest:' + (!!Kinky.FILE_MANAGER_RENAME ? Kinky.FILE_MANAGER_RENAME : '/zepp/filemanager/renamefile'), params, null, 'refreshFileList');
	};
	
	KMediaList.prototype.refreshFileList = function(data) {
		KBreadcrumb.dispatchEvent(null, {
			action : '/refresh-list'
		});
	};
	
	KSystem.registerAddOn('KMediaList');
	KSystem.included('KMediaList');
}, Kinky.ZEPP_INCLUDER_URL);

function KMediaFileList(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.addActionListener(KMediaFileList.refreshList, [
			'/refresh-list'
		]);
		this.elementsSelected = {};
	}
}

KSystem.include([
	'KPanel',
	'KButton'
], function() {
	KMediaFileList.prototype = new KPanel();
	KMediaFileList.prototype.constructor = KMediaFileList;
	
	KMediaFileList.refreshList = function(widget) {
		widget.parent.fileList = widget.refresh();
	};
	
	KMediaFileList.prototype.load = function() {
		this.fileListData = [];
		if (!KMediaList.selectedServer || (!KMediaList.selectedServer != '')) {
			this.draw();
			return;
		}
		this.kinky.get(this, 'rest:' + (!!Kinky.FILE_MANAGER_LIST ? Kinky.FILE_MANAGER_LIST : '/zepp/filemanager/filelist'), {
			serverIndex : KMediaList.selectedServer,
			fileRoot : KMediaList.selectedRoot
		});
	};
	
	KMediaFileList.prototype.refresh = function() {
		var widget = KWidget.prototype.refresh.call(this);
		widget.content.style.visibility = 'hidden';
		widget.container.style.visibility = 'hidden';
		widget.manageSelected(null, false);
		return widget;
	};
	
	KMediaFileList.prototype.onLoad = function(data) {
		if (!!data) {
			this.fileListData = data;
		}
		this.draw();
	};
	
	KMediaFileList.prototype.draw = function() {
		this.content.style.visibility = 'visible';
		this.container.style.visibility = 'visible';
		
		if (!KMediaList.selectedServer) {
			var noItems = window.document.createElement('div');
			noItems.className = ' ZeppListNoItems ';
			noItems.appendChild(window.document.createTextNode(gettext('$KMEDIALIST_NO_SERVER')));
			this.content.appendChild(noItems);
		}
		else {
			this.refreshBreadcrumb();
			
			if (this.fileListData.length > 0) {
				for ( var index in this.fileListData) {
					if (!!this.fileListData[index].mime && /directory/.test(this.fileListData[index].mime)) {
						var element = new KButton(this, '', 'filesystem' + index, function(event) {
							var widget = KDOM.getEventWidget(event);
							widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
						});
						element.setText('<i class="directory fa fa-folder-open-o"></i> ' + this.fileListData[index].title, true);
						
						element.addEventListener('dblclick', function(event) {
							var widget = KDOM.getEventWidget(event);
							widget.parent.changeRoot(widget.data.fileURL);
						});
						element.data = {
							fileURL : this.fileListData[index].url
						};
						element.hash = '/list-element/' + element.id;
						this.appendChild(element);
					}
				}
				for ( var index in this.fileListData) {
					if (!!this.fileListData[index].mime && !(/directory/.test(this.fileListData[index].mime))) {
						var element = null;
						var category = this.fileListData[index].mime.split(';')[0].split('/');
						var type = category[1];
						category = category[0];
						
						switch (category) {
							case 'image': {
								element = new KImage(this);
								element.setDescription(this.fileListData[index].title);
								element.setImage(this.fileListData[index].url);
								element.addEventListener('click', function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								break;
							}
							case 'audio': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="audio fa-music"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'video': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="video fa fa-film"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'text': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="office fa fa-file-o"></i> ' + this.fileListData[index].title, true);
								break;
							}
							case 'application': {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								switch (type) {
									case 'msword' :
									case 'vnd.ms-powerpoint' :
									case 'vnd.ms-excel' :
									case 'pdf' : {
										element.setText('<i class="office fa fa-file-o"></i> ' + this.fileListData[index].title, true);
										break;
									}
									default : {
										element.setText('<i class="general fa fa-file"></i> ' + this.fileListData[index].title, true);
									}
								}
								break;
							}
							default: {
								var element = new KButton(this, '', 'filesystem' + index, function(event) {
									var widget = KDOM.getEventWidget(event);
									widget.getParent('KMediaFileList').manageSelected(widget, !!event.ctrlKey);
								});
								element.setText('<i class="general fa fa-file"></i> ' + this.fileListData[index].title, true);
								break;
							}
						}
						element.addEventListener('dblclick', function(event) {
							KDOM.stopEvent(event);
							var widget = KDOM.getEventWidget(event);
							widget.parent.selectFile(widget.data.fileURL);
						});
						element.data = {
							fileURL : this.fileListData[index].url
						};
						element.hash = '/list-element/' + element.id;
						this.appendChild(element);
					}
				}
			}
			else {
				var noItems = window.document.createElement('div');
				noItems.className = ' ZeppListNoItems ';
				noItems.appendChild(window.document.createTextNode(gettext('$ZEPP_LIST_NO_ITEMS')));
				this.content.appendChild(noItems);
			}
		}
		
		KPanel.prototype.draw.call(this);
	};
	
	KMediaFileList.prototype.selectFile = function(fileURL) {
		KMediaList.parent.setValue(fileURL);
	};
	
	KMediaFileList.prototype.breadcrumbNavigation = function(place) {
		var newRoot = '';
		var splittedBC = KMediaList.selectedRoot.split('/');
		for ( var index in splittedBC) {
			if (index > 0) {
				newRoot += '/' + splittedBC[index];
			}
			if (splittedBC[index] == place) {
				KMediaList.selectedRoot = newRoot;
				this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
				this.parent.fileList = this.refresh();
				break;
			}
		}
	};
	
	KMediaFileList.prototype.upInRoot = function() {
		var newRoot = KMediaList.selectedRoot.substring(0, KMediaList.selectedRoot.lastIndexOf('/'));
		if (KMediaList.selectedRoot != '') {
			this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
			KMediaList.selectedRoot = newRoot;
			this.parent.fileList = this.refresh();
		}
	};
	
	KMediaFileList.prototype.changeRoot = function(root) {
		KMediaList.selectedRoot += root.substring(root.lastIndexOf('/'), root.length);
		this.parent.actions.addFile.aditional_args = 'mediaServer=' + KMediaList.selectedServer + '&folderPath=' + KMediaList.selectedRoot;
		this.parent.fileList = this.refresh();
	};
	
	KMediaFileList.prototype.refreshBreadcrumb = function() {
		var parent = this.parent;
		parent.breadcrumbPanel.innerHTML = '';
		var upInRoot = window.document.createElement('button');
		upInRoot.innerHTML = gettext('$KMEDIALIST_BACK');
		KDOM.addEventListener(upInRoot, 'click', function(event) {
			var parentWidget = KDOM.getEventWidget(event, 'KPanel');
			parentWidget.fileList.upInRoot();
		});
		parent.breadcrumbPanel.appendChild(upInRoot);
		var splittedBC = KMediaList.selectedRoot.split('/');
		
		var breadcrumbDiv = window.document.createElement('div');
		for ( var index in splittedBC) {
			var link = window.document.createElement('a');
			link.innerHTML = '/' + (index == 0 ? gettext('$KMEDIALIST_ROOT') : splittedBC[index]);
			link.alt = (index == 0 ? '' : splittedBC[index]);
			KDOM.addEventListener(link, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widgetParent = KDOM.getEventWidget(event, 'KPanel');
				widgetParent.fileList.breadcrumbNavigation(element.alt);
			});
			breadcrumbDiv.appendChild(link);
		}
		parent.breadcrumbPanel.appendChild(breadcrumbDiv);
	};
	
	KMediaFileList.prototype.manageSelected = function(widget, controlFlag) {
		var count = 0;
		for ( var selected in this.elementsSelected) {
			if (!!this.elementsSelected[selected]) {
				count++;
			}
		}
		
		if (!!widget) {
			if (this.elementsSelected[widget.hash]) {
				if (!controlFlag) {
					this.elementsSelected = {};
					if (count != 1) {
						var fileName = widget.data.fileURL.substring(widget.data.fileURL.lastIndexOf('/') + 1, widget.data.fileURL.length);
						this.elementsSelected[widget.hash] = fileName;
					}
				}
				else {
					delete this.elementsSelected[widget.hash];
				}
			}
			else {
				if (!controlFlag) {
					this.elementsSelected = {};
				}
				var fileName = widget.data.fileURL.substring(widget.data.fileURL.lastIndexOf('/') + 1, widget.data.fileURL.length);
				this.elementsSelected[widget.hash] = fileName;
			}
			var siblling = widget.parent.childWidgets();
			for ( var index in siblling) {
				if (this.elementsSelected[siblling[index].hash]) {
					siblling[index].addCSSClass(siblling[index].className + 'Selected');
				}
				else {
					siblling[index].removeCSSClass(siblling[index].className + 'Selected');
				}
			}
			count = 0;
			for ( var selected in this.elementsSelected) {
				if (!!this.elementsSelected[selected]) {
					count++;
				}
			}
		}
		
		if (count > 1) {
			this.parent.fileOptions.setStyle({
				display : 'block'
			});
			this.parent.fileOptions.externalBT.style.display = 'none';
			this.parent.fileOptions.renameBT.style.display = 'none';
		}
		else if (count == 1) {
			this.parent.fileOptions.setStyle({
				display : 'block'
			});
			
			for ( var index in this.elementsSelected) {
				var externalLink = widget.parent.childWidget(index).data.fileURL;
				this.parent.fileOptions.externalBT.name = externalLink;
				
				var fileName = this.elementsSelected[index];
				this.parent.fileOptions.childWidget('/media-actions-rename').setValue(fileName);
				break;
			}
			this.parent.fileOptions.renameBT.style.display = 'block';
			this.parent.fileOptions.externalBT.style.display = 'block';
			this.parent.fileOptions.childWidget('/media-actions-rename').setStyle({
				display : 'none'
			});
		}
		else {
			this.parent.fileOptions.setStyle({
				display : 'none'
			});
		}
	};
	
	KSystem.included('KMediaFileList');
}, Kinky.ZEPP_INCLUDER_URL);
function KMenu(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.isLeaf = false;
		this.menu = window.document.createElement('ul');
		{
			this.menu.className = 'KMenuLevel1';
			this.menu.id = this.hash;
		}
		
		this.marked = [];
		this.addActionListener(KMenu.markSelected, [
			'/kmenu/mark/item'
		]);
	}
}

KSystem.include([
	'KWidget'
], function() {
	KMenu.prototype = new KWidget();
	KMenu.prototype.constructor = KMenu;
	
	KMenu.prototype.mark = function(hash) {
		if (!hash) {
			return;
		}
		
		var paths = hash.split('/');
		paths.splice(0, 1);
		
		for ( var level in this.marked) {
			this.removeCSSClass('KMenuMarked', this.marked[level]);
		}
		
		delete this.marked;
		this.marked = [];
		
		var ul = this.menu;
		var cur = '';
		for ( var path in paths) {
			for ( var i = 0; i != ul.childNodes.length; i++) {
				if ((ul.childNodes[i].name == cur + '/' + paths[path]) || (ul.childNodes[i].name == '/' + paths[path])) {
					this.marked.push(ul.childNodes[i]);
					this.addCSSClass('KMenuMarked', ul.childNodes[i]);
					var newUL = ul.childNodes[i].getElementsByTagName('ul');
					if (newUL.length != 0) {
						ul = newUL[0];
					}
					cur += '/' + paths[path];
					break;
				}
			}
		}
	};
	
	KMenu.prototype.loadIndex = function(page, level, topUL, parent) {
		var ul = null;
		if (level == null) {
			ul = topUL = this.menu;
			level = 0;
		}
		else {
			var li = window.document.createElement('li');
			li.name = page.hash;
			try {
				li.className = 'KMenuLevelItem' + level + ' KMenu' + page.hash.replace(/\//g, '_').toUpperCase();
				li.id = page.hash.replace(/\//g, '_').toUpperCase();
			}
			catch (e) {
			}
			
			var a = window.document.createElement('a');
			
			if (page.graphicStyle && page.graphicStyle.textImages && page.graphicStyle.textImages.pageMenuTitle) {
				a.appendChild(KCSS.img(page.graphicStyle.textImages.pageMenuTitle.base, page.menuText, page.titleText));
			}
			else {
				a.appendChild(window.document.createTextNode(page.title));
			}
			a.href = (/http:/.test(page.hash) ? '' : '#') + page.href;
			li.appendChild(a);
			
			ul = window.document.createElement('ul');
			ul.className = 'KMenuLevel' + (level + 1);
			li.appendChild(ul);
			
			topUL.appendChild(li);
		}
		for ( var index in page.pages) {
			this.loadIndex(page.pages[index], level + 1, ul, page);
		}
	};
	
	KMenu.markSelected = function(widget, hash) {
		if (widget.activated()) {
			widget.mark(KBreadcrumb.getHash());
		}
	};
	
	KMenu.prototype.draw = function() {
		KWidget.prototype.draw.call(this);
		this.loadIndex(this.data);
		this.content.appendChild(this.menu);
	};
	
	KMenu.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'aside',
			content : 'nav',
			background : 'div'
		}
	};
	
	KSystem.included('KMenu');
});
function KMessageDialog(target, params) {
	if (target != null) {
		KFloatable.call(this, target, params);
	}
}
function KMessagePanel(params) {
	if (params != null) {
		KPanel.call(this, Kinky.getShell(params.shell));
		this.config = params;
	}
}

KSystem.include([
	'KFloatable',
	'KButton',
	'KPanel'
], function() {
	KMessageDialog.prototype = new KFloatable();
	KMessageDialog.prototype.constructor = KMessageDialog;
	
	KMessageDialog.alert = function(params) {
		params.modal = false;
		
		var target = new KMessagePanel(params);
		var dialog = KFloatable.make(target, params);
		dialog.addCSSClass('KMessageDialog');
		KMessageDialog.showOverlay();
		KFloatable.show(dialog);
	};
	
	KSystem.included('KMessageDialog');
	
	KMessagePanel.prototype = new KPanel();
	KMessagePanel.prototype.constructor = KMessagePanel;

	KMessagePanel.prototype.onOpen = function() {
		this.childWidget('/buttons/ok').focus();
	};

	KMessagePanel.prototype.invisible = function() {
		if (!!this.config.onClose) {
			this.config.onClose();
		}
		KPanel.prototype.invisible.call(this);
		KMessageDialog.hideOverlay();
	};
	
	KMessagePanel.prototype.load = function() {
		this.draw();
	};
	
	KMessagePanel.prototype.draw = function() {
		var text = window.document.createElement('div');
		{
			if (!!this.config.html) {
				text.innerHTML = this.config.message;
			}
			else {
				text.appendChild(window.document.createTextNode(this.config.message));
			}
			text.className = ' KMessageDialogMessageText ';
		}
		this.container.appendChild(text);
		
		var ok = new KButton(this, gettext('$KMESSAGEDIALOG_OK_BUTTON_LABEL'), 'ok', function(event) {
			KDOM.stopEvent(event);
			
			KDOM.getEventWidget(event).parent.parent.closeMe();
		});
		{
			ok.hash = '/buttons/ok';
			ok.addCSSClass('KMessageDialogOKButton');
		}
		this.appendChild(ok);
		
		KPanel.prototype.draw.call(this);
	};
	

	KMessageDialog.showOverlay = function() {
		if (!KMessageDialog.overlay.parentNode) {
			Kinky.getShell().panel.appendChild(KMessageDialog.overlay);
		}
		KMessageDialog.overlay.style.display = 'block';
	};
	
	KMessageDialog.hideOverlay = function(force) {
		KMessageDialog.overlay.style.display = 'none';	
	};

	KMessageDialog.overlay = window.document.createElement('div');
	KMessageDialog.overlay.appendChild(window.document.createTextNode(' '));
	KMessageDialog.overlay.className = ' KShellOverlay KMessageDialogOverlay ';
	KCSS.setStyle({
		width : '100%',
		height : '100%',
		position : 'fixed',
		opacity : '1',
		top : '0',
		left : '0',
		display : 'none'
	}, [ KMessageDialog.overlay ]);

	KSystem.included('KMessagePanel');
});
function KMinimalScroll(parent, div) {
	if (!!parent) {
		KScroll.call(this, parent, div);
	}
}
KSystem.include([
	'KScroll'
], function() {
	KMinimalScroll.prototype = new KScroll();
	KMinimalScroll.prototype.constructor = KMinimalScroll;
	
	KMinimalScroll.prototype.draw = function() {
		this.parent.addResizeListener(KMinimalScroll.onWidgetResize);
		var root = this.parent[this.divTarget];
		
		this.barV = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'VerticalBar', this.barV);
			this.barV.id = this.id;
			
			this.scrollerV = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'VerticalScroller', this.scrollerV);
			}
			this.barV.appendChild(this.scrollerV);
			
			var buttonDiv = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'VerticalButtons', buttonDiv);
				
				this.upperV = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'VerticalUpper', this.upperV);
					this.upperV.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.upperV, 'click', KScroll.onUp);
				}
				buttonDiv.appendChild(this.upperV);
				
				this.downerV = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'VerticalDowner', this.downerV);
					this.downerV.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.downerV, 'click', KScroll.onDown);
				}
				buttonDiv.appendChild(this.downerV);
				
				buttonDiv.style.display = 'none';
			}
			this.scrollerV.appendChild(buttonDiv);
		}
		root.appendChild(this.barV);
		
		this.barH = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'HorizontalBar', this.barH);
			this.barH.id = this.id;
			
			this.scrollerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalScroller', this.scrollerH);
				this.scrollerH.innerHTML = '&nbsp;';
			}
			this.barH.appendChild(this.scrollerH);
			
			var buttonDiv = window.document.createElement('div');
			{
				KCSS.addCSSClass(this.className + 'HorizontalButtons', buttonDiv);
				
				this.upperH = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'HorizontalUpper', this.upperH);
					this.upperH.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.upperH, 'click', KScroll.onLeft);
				}
				buttonDiv.appendChild(this.upperH);
				
				this.downerH = window.document.createElement('button');
				{
					KCSS.addCSSClass(this.className + 'HorizontalDowner', this.downerH);
					this.downerH.innerHTML = '&nbsp;';
					KDOM.addEventListener(this.downerH, 'click', KScroll.onRight);
				}
				buttonDiv.appendChild(this.downerH);
				
				buttonDiv.style.display = 'none';
			}
			this.barH.appendChild(buttonDiv);
		}
		root.appendChild(this.barH);
		
		var rootH = root.style.height;
		if (!rootH) {
			rootH = KDOM.getBrowserHeight() + 'px';
		}
		KCSS.setStyle({
			overflow : 'hidden',
			height : rootH
		}, [
			root
		]);
		
		this.offsetV = this.parent.content.offsetTop;
		this.offsetH = this.parent.content.offsetLeft;
		this.position.pv = this.topbH = 0;
		this.bottombH = 0;
		this.position.ph = this.leftbW = 0;
		this.rightbW = 0;
		
		KDOM.addEventListener(this.scrollerV, 'mouseover', KMinimalScroll.showVertical);
		KDOM.addEventListener(this.barV, 'mouseout', KMinimalScroll.hide);
		KDOM.addEventListener(this.scrollerH, 'mouseover', KMinimalScroll.showHorizontal);
		KDOM.addEventListener(this.barH, 'mouseout', KMinimalScroll.hide);
	};
	
	KMinimalScroll.onWidgetResize = function(scrolledWidget, size) {
		var scroll = KScroll.getScrollFromWidget(scrolledWidget);
		
		scroll.viewportH = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).height);
		scroll.viewportW = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).width);
		scroll.proportionV = (scroll.viewportH - scroll.topbH - scroll.bottombH - scroll.offsetV) / (scroll.parent.content.offsetHeight + 10);
		scroll.proportionH = (scroll.viewportW - scroll.leftbW - scroll.rightbW - scroll.offsetH) / (scroll.parent.content.offsetWidth + 10);
		
		if (scroll.viewportW < scroll.parent.content.offsetWidth) {
			var width = Math.round(scroll.proportionH * scroll.viewportW);
			var scrollerHX = KDOM.normalizePixelValue(scroll.scrollerH.style.left);
			
			if (isNaN(scrollerHX)) {
				scrollerHX = scroll.leftbW;
			}
			else {
				var contentX = KDOM.normalizePixelValue(scroll.parent.content.style.left);
				if (isNaN(contentX)) {
					contentX = 0;
				}
				scrollerHX = Math.round(-contentX * scroll.proportionH) + scroll.leftbW;
			}
			scroll.position.ph = scrollerHX;
			
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'block'
			}, [
				scroll.barH
			]);
			
			KCSS.setStyle({
				width : width + 'px',
			}, [
				scroll.scrollerH
			]);
			
			KEffects.addEffect(scroll.scrollerH, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					0,
					scroll.viewportW - width - scroll.rightbW,
					0,
					scroll.leftbW
				],
				from : {
					x : scrollerHX
				},
				lock : {
					y : true
				},
				onStart : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					var buttonsH = scroll.scrollerH.childNodes[0];
					scroll.dragx = KDOM.normalizePixelValue(buttonsH.style.left) - KDOM.normalizePixelValue(scroll.scrollerH.style.left);
				},
				onComplete : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = false;
				},
				onAnimate : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dispatchScroll();
					scroll.dragging = true;
					scroll.blockclick = true;
					var buttonsH = scroll.scrollerH.childNodes[0];
					buttonsH.style.left = (KDOM.normalizePixelValue(scroll.scrollerH.style.left) + scroll.dragx) + 'px';
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'none'
			}, [
				scroll.barH
			]);
		}
		
		if (scroll.viewportH < scroll.parent.content.offsetHeight) {
			var height = Math.round(scroll.proportionV * scroll.viewportH);
			var scrollerVY = KDOM.normalizePixelValue(scroll.scrollerV.style.top);
			
			if (isNaN(scrollerVY)) {
				scrollerVY = scroll.topbH;
			}
			else {
				var contentY = KDOM.normalizePixelValue(scroll.parent.content.style.top);
				if (isNaN(contentY)) {
					contentY = 0;
				}
				scrollerVY = Math.round(-contentY * scroll.proportionV) + scroll.topbH;
			}
			
			scroll.position.pv = scrollerVY;
			
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'block'
			}, [
				scroll.barV
			]);
			KCSS.setStyle({
				height : height + 'px',
				top : scrollerVY + 'px'
			}, [
				scroll.scrollerV
			]);
			KEffects.addEffect(scroll.scrollerV, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					scroll.topbH,
					0,
					scroll.viewportH - height - scroll.bottombH,
					0
				],
				from : {
					y : scrollerVY
				},
				lock : {
					x : true
				},
				onStart : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					var buttonsV = scroll.scrollerV.childNodes[0];
					scroll.dragy = KDOM.normalizePixelValue(buttonsV.style.top) - KDOM.normalizePixelValue(scroll.scrollerV.style.top);
				},
				onComplete : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = false;
				},
				onAnimate : function(wx, tween) {
					var scroll = KScroll.getScrollFromWidget(wx);
					scroll.dragging = true;
					scroll.blockclick = true;
					scroll.dispatchScroll();
					var buttonsV = scroll.scrollerV.childNodes[0];
					buttonsV.style.top = (KDOM.normalizePixelValue(scroll.scrollerV.style.top) + scroll.dragy) + 'px';
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'none'
			}, [
				scroll.barV
			]);
		}
	};
	
	KMinimalScroll.showVertical = function(event) {
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.dragging) {
			return;
		}
		var mouseEvent = KDOM.getEvent(event);
		KDOM.stopEvent(event);
		var scroller = KDOM.getEventCurrentTarget(event);
		var target = KDOM.getEventTarget(event);
		
		if (scroller == target) {
			var buttons = scroller.childNodes[0];
			buttons.style.position = 'fixed';
			buttons.style.visibility = 'hidden';
			buttons.style.display = 'block';
			var top = (mouseEvent.clientY - Math.round(buttons.offsetHeight / 2));
			if (top < 0) {
				top = 0;
			}
			else if (top > KDOM.getBrowserHeight()) {
				top = KDOM.getBrowserHeight() - buttons.offsetHeight;
			}
			buttons.style.top = top + 'px';
			
			var left = mouseEvent.clientX - buttons.offsetWidth;
			buttons.style.left = left + 'px';
			
			KEffects.addEffect(buttons, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function() {
					buttons.style.visibility = 'visible';
				},
				applyToElement : true
			});
		}
	};
	
	KMinimalScroll.hide = function(event) {
		if (KScroll.getScrollFromEvent(event).dragging) {
			return;
		}
		var target = KDOM.getEventTarget(event);
		var scrollDiv = KScroll.getScrollDiv(target);
		var relTarget = KDOM.getEventRelatedTarget(event, true);
		var testScrollDiv = KScroll.getScrollDiv(relTarget);
		
		if (!testScrollDiv || (scrollDiv.id != testScrollDiv.id)) {
			KEffects.addEffect(scrollDiv.childNodes[0].childNodes[0], {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function() {
					scrollDiv.childNodes[0].childNodes[0].style.display = 'none';
				},
				applyToElement : true
			});
		}
	};
	
	KMinimalScroll.showHorizontal = function(event) {
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.dragging) {
			return;
		}
		var mouseEvent = KDOM.getEvent(event);
		KDOM.stopEvent(event);
		var scroller = KDOM.getEventCurrentTarget(event);
		var target = KDOM.getEventTarget(event);
		
		if (scroller == target) {
			var buttons = scroller.childNodes[0];
			buttons.style.position = 'fixed';
			buttons.style.visibility = 'hidden';
			buttons.style.display = 'block';
			var left = (mouseEvent.pageX - Math.round(buttons.offsetWidth / 2));
			if (left < 0) {
				left = 0;
			}
			else if (left > scroll.viewportW - buttons.offsetWidth) {
				left = scroll.viewportW - buttons.offsetWidth;
			}
			buttons.style.left = left + 'px';
			KEffects.addEffect(buttons, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 300,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onStart : function() {
					buttons.style.visibility = 'visible';
				},
				applyToElement : true
			});
		}
	};
	
	KSystem.registerAddOn('KMinimalScroll');
	KSystem.included('KMinimalScroll');
});

function KOAuthForm(parent, config) {
	if (parent != null) {
		KForm.call(this, parent);
		this.oauth = config;
		if (!this.oauth) {
			this.oauth = new Object();
		}
		if (!this.oauth.response_type) {
			this.oauth.response_type = 'token';
		}
		this.oauth.client_id = window.document.location.host;
	}
}

KSystem.include([
	'KForm',
	'KInput',
	'KPassword',
	'KButton'
], function() {
	KOAuthForm.prototype = new KForm();
	KOAuthForm.prototype.constructor = KOAuthForm;
	
	KOAuthForm.prototype.draw = function() {
		this.user = new KInput(this, gettext('$LOGIN_FORM_USER'), 'id');
		this.user.addCSSClass('KOAuthFormUserInput');
		this.user.addValidator(/(.+)/, gettext('$LOGIN_FORM_USER_ERR_MSG'));
		this.addInput(this.user);
		
		this.password = new KPassword(this, gettext('$LOGIN_FORM_PASS'), 'password');
		this.user.addCSSClass('KOAuthFormPasswordInput');
		this.user.addValidator(/(.+)/, gettext('$LOGIN_FORM_PASS_ERR_MSG'));
		this.addInput(this.password);
		
		this.submit = new KButton(this, gettext('$LOGIN_FORM_SUBMIT'), 'submit', KForm.goSubmit);
		this.user.addCSSClass('KOAuthFormSubmitButton');
		this.appendChild(this.submit);
		
		KForm.prototype.draw.call(this);
	};
	
	KOAuthForm.prototype.onSuccess = function(data, request) {
		
		this.oauth.access_token = KOAuth.getToken(this.shell);
		
		var vars = '';
		for ( var att in this.oauth) {
			if (vars.length == 0) {
				vars += '?';
			}
			else {
				vars += '&';
			}
			vars += att + '=' + encodeURIComponent(this.oauth[att]);
		}
		var shell = this.getShellConfig();
		window.document.location = shell.providers.rest.services + 'oauth/authorize' + vars;
	};
	
	KOAuthForm.prototype.onValidate = function(action, params, status) {
		if (status) {
			this.action = 'rest:/oauth/' + this.user.getValue() + '/login';
			return {
				password : this.password.getValue()
			};
		}
		else {
			this.showAllErrors(params);
		}
		return false;
	};
	
	KOAuthForm.FORM_ELEMENT = 'form';
	
	KSystem.included('KOAuthForm');
});
function KOAuth() {
}
{
	KOAuth.prototype = {};
	KOAuth.prototype.constructor = KOAuth;
	
	KOAuth.start = function(shellName, loadShell) {
		var service = Kinky.SHELL_CONFIG[shellName].auth.service;
		var href = (!!Kinky.SHELL_CONFIG[shellName].id ? Kinky.SHELL_CONFIG[shellName].id : shellName);
		
		var authHeader = KOAuth.getHTTPAuthorizationHeader();
		if (!authHeader) {
			authHeader = KOAuth.getHTTPAuthorizationHeader(shellName);
		}
		var request = {
			type : service.substring(0, service.indexOf(':')),
			namespace : service.substring(service.indexOf(':') + 1, service.lastIndexOf(':')),
			service : service.substring(service.lastIndexOf(':') + 1),
			action : 'get',
			id : 'picker',
			flag : loadShell,
			shell : shellName,
			callback : [
				'KOAuth.lockPicker.onNegotiate'
			]
		};
		try {
			KConnectionsPool.getConnection(request.type, href).send(request, null, authHeader);
		}
		catch (e) {
			KSystem.include(KConnectionsPool.connectors[request.type], function() {
				KConnectionsPool.getConnection(request.type, href).send(request, null, authHeader);
			});
		}
	};
	
	KOAuth.destroy = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		if(typeof(Storage)!=="undefined") {
			delete localStorage[type + '_' + shellName + '_AUTH'];
			delete localStorage[type + '_' + shellName + '_CREDENTIALS'];
		}
		else {
			KSystem.removeCookie(type + '_' + shellName + '_AUTH');
			KSystem.removeCookie(type + '_' + shellName + '_CREDENTIALS');
		}
	};
	
	KOAuth.session = function(authData, request, shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		if(typeof(Storage)!=="undefined") {
			localStorage[type + '_' + shellName + '_AUTH'] = JSON.stringify(authData);
			localStorage[type + '_' + shellName + '_CREDENTIALS'] = JSON.stringify(request);
		}
		else {
			KSystem.setCookie(type + '_' + shellName + '_AUTH', authData);
			KSystem.setCookie(type + '_' + shellName + '_CREDENTIALS', request);
		}
	};
	
	KOAuth.getAuthorization = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		var authCookie = null;
		if(typeof(Storage)!=="undefined") {
			try {
				authCookie = JSON.parse(localStorage[type + '_' + shellName + '_AUTH']);
			}
			catch(e) {
				authCookie = null;
			}
		}
		else {
			authCookie = KSystem.getCookie(type + '_' + shellName + '_AUTH');
		}
		if (!authCookie || !authCookie.authentication) {
			return null;
		}
		return authCookie;
	};
	
	KOAuth.getToken = function(shellName, type) {
		var auth = this.getAuthorization(shellName, type);
		if (!auth || !auth.authentication) {
			return null;
		}
		return auth.authentication.replace('OAuth2.0 ', '');
	};
	
	KOAuth.getHTTPAuthorizationHeader = function(shellName, type) {
		if (!type) {
			type = 'KINKY';
		}
		if (!shellName) {
			shellName = 'GLOBAL';
		}
		else {
			shellName = shellName.replace(/\//g, '_');
		}
		var headers = null;
		var authCookie = KOAuth.getAuthorization(shellName, type);
		if (!!authCookie) {
			headers = {
				"Authorization" : authCookie.authentication
			};
		}
		return headers;
	};
	
	KOAuth.prototype.onLoad = function(data, request) {
	};
	
	KOAuth.prototype.onSave = function(data, request) {
	};
	
	KOAuth.prototype.onRemove = function(data, request) {
	};
	
	KOAuth.prototype.onPost = function(data, request) {
	};
	
	KOAuth.prototype.onPut = function(data, request) {
	};
	
	KOAuth.prototype.onCreated = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onAccepted = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onNoContent = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onBadRequest = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onUnauthorized = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onForbidden = function(data, request) {
		this.onNegotiate(data, request);
	};
	
	KOAuth.prototype.onNotFound = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onInternalError = function(data, request) {
		this.onError(data, request);
	};
	
	KOAuth.prototype.onError = function(data, request) {
		if (!!Kinky.SHELL_CONFIG[request.shell].auth && !!Kinky.SHELL_CONFIG[request.shell].auth.callback && !!Kinky.SHELL_CONFIG[request.shell].auth.callback.fail) {
			Kinky.SHELL_CONFIG[request.shell].auth.callback.fail.call(data);
		}
		
		if (request.flag) {
			Kinky.loadShell(Kinky.SHELL_CONFIG[request.shell]);
		}
		else {
			Kinky.SHELL_CONFIG[request.shell].href = request.shell;
			Kinky.SHELL_CONFIG[request.shell].requestTypes = [
				'application/vnd.kinky.' + Kinky.SHELL_CONFIG[request.shell].className
			];
			Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[request.shell]);
		}
	};
	
	KOAuth.prototype.onNegotiate = function(data, request) {
		Kinky.startSession(data);
		if (!!Kinky.SHELL_CONFIG[request.shell].auth && !!Kinky.SHELL_CONFIG[request.shell].auth.callback && !!Kinky.SHELL_CONFIG[request.shell].auth.callback.success) {
			Kinky.SHELL_CONFIG[request.shell].auth.callback.success.call(data);
		}
		
		if (request.flag) {
			Kinky.loadShell(Kinky.SHELL_CONFIG[request.shell]);
		}
		else {
			Kinky.SHELL_CONFIG[request.shell].id = request.shell;
			Kinky.SHELL_CONFIG[request.shell].requestTypes = [
				'application/vnd.kinky.' + Kinky.SHELL_CONFIG[request.shell].className
			];
			Kinky.bunnyMan.start(Kinky.SHELL_CONFIG[request.shell]);
		}
	};
	
	KOAuth.lockPicker = new KOAuth();
	KOAuth.PROTOCOL_VERSION = '2.0';
	KOAuth.ACCESS_TOKEN_AS_VAR = false;
	
	KSystem.included('KOAuth');
}

function KOrderedList(parent, params) {
	if (parent != null) {
		KList.call(this, parent, params);
		this.onCompare = KOrderedList.defaulCompare;
		this.orderedChildren = {};
	}
}

KSystem.include( [ 'KList' ], function() {

	KOrderedList.prototype = new KList();
	KOrderedList.prototype.constructor = KOrderedList;

	KOrderedList.prototype.hasPaginationBar = true;

	KOrderedList.prototype.appendChild = function(element, context, childDiv) {
		context = context || (this.query + this.nPage);
		if (!this.orderedChildren[context]) {
			this.orderedChildren[context] = [];
		}
		element.orderedIndex = this.orderedChildren[context].push(element) - 1;
		KList.prototype.appendChild.call(this, element, context, childDiv);
	};

	KOrderedList.prototype.order = function() {
		var context = this.query + this.nPage;
		if (this.orderedChildren[context]) {
			this.orderedChildren[context] = Quicksort.quicksort(this.orderedChildren[context], this.onCompare);

			for ( var index in this.orderedChildren[context]) {
				this.orderedChildren[context][index].orderedIndex = index;
				this.content.appendChild(this.orderedChildren[context][index].panel);
			}
		}
	};

	KOrderedList.prototype.draw = function() {
		KList.prototype.draw.call(this);
		this.order();
	};

	KOrderedList.prototype.removeAllChildren = function(context) {
		context = context || (this.query + this.nPage);
		KList.prototype.removeAllChildren.call(this, context);
		delete this.orderedChildren[context];
	};

	KOrderedList.prototype.removeChild = function(element, context, notDeleteIt) {
		context = context || (this.query + this.nPage);
		KList.prototype.removeChild.call(this, element, context, notDeleteIt);
		this.orderedChildren[context].splice(element.orderedIndex, 1);
	};

	KOrderedList.defaulCompare = function(widget1, widget2) {
		return parseInt(widget1.id.replace('widget', '')) - parseInt(widget2.id.replace('widget', ''));
	};

	KSystem.included('KOrderedList');
});
function KOrkutShare(parent, params) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.hash = '/orkut-share' + params.tweetPageHash;
		this.data = {
			hash : params.sharePageHash,
			url : params.siteURL,
			size : params.size,
			title : params.title ? params.title : null,
			body : params.body ? params.body : null,
			thumbnail : params.thumbnail ? params.thumbnail : null
		};
		this.isStatic = false;
	}
}

KSystem.include([
	'KPanel'
], function() {
	KOrkutShare.prototype = new KPanel();
	KOrkutShare.prototype.constructor = KOrkutShare;

	KOrkutShare.prototype.draw = function() {
		if (!this.isStatic){
			this.addLocationListener(KOrkutShare.shareMe);
		}

		this.onChangeURL(this.data.hash);

		this.setStyle({
			width : this.data.size.width + 'px',
			height : this.data.size.height + 'px',
			overflow : 'hidden'
		});
		KPanel.prototype.draw.call(this);
	};

	KOrkutShare.prototype.onChangeURL = function(hash) {

		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.async = false;
		script.src = 'http://www.google.com/jsapi';
		head.appendChild(script);

		this.content.innerHTML = '<a href="http://promote.orkut.com/preview?nt=orkut.com&tt='+ this.data.title +'&du=' + encodeURIComponent(this.data.url + this.data.hash.substring(1)) + (this.data.body != null ? '&cn=' + encodeURIComponent(this.data.body) : '') + '" target="_blank"><img src="' + this.data.thumbnail + '" border=0></a>';

	};

	KOrkutShare.shareMe = function(widget, hash) {
		if (widget.activated()) {
			widget.onChangeURL(hash);
		}
	};

	KSystem.included('KOrkutShare');
});

function KPagedList(parent) {
	if (parent != null) {
		KList.call(this, parent);
	}
}

KSystem.include([
	'KList'
], function() {
	KPagedList.prototype = new KList();
	KPagedList.prototype.constructor = KPagedList;
	
	KPagedList.prototype.pageVisible = function(n_page) {
		if ((this.header.childNodes.length == 0) && (this.pageSize != 0)) {
			this.addPagination(this.header, this.totalSize);
			this.addPagination(this.footer, this.totalSize);
		}
		
		if ((this.header.childNodes.length != 0) && (this.pageSize != 0)) {
			this.header.childNodes[0].value = '' + (n_page + 1);
		}
		if ((this.footer.childNodes.length != 0) && (this.pageSize != 0)) {
			this.footer.childNodes[0].value = '' + (n_page + 1);
		}

		if (this.lastpage >= 0) {
			this.transition_page_hide(this.pages['page' + this.lastpage], null, this.lastpage > this.page ? 1 : -1);
			this.transition_page_show(this.pages['page' + n_page], null, this.lastpage > this.page ? 1 : -1);
		}
		else {
			if (!!this.pages['page' + n_page]) {
				this.transition_page_show(this.pages['page' + n_page], null, 1);
			}
		}
		this.lastpage = n_page;
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
	};
	
	KPagedList.prototype.addPagination = function(parent, totalSize) {
		if (!!totalSize) {
			var n_pages = Math.ceil(this.totalSize / this.pageSize);

			var pageNr = window.document.createElement('input');
			{
				pageNr.className = ' KPagedListPages ' + (this.className != 'KPagedList' ? this.className + 'ListPages ' : '');
				pageNr.setAttribute('type', 'text');
				KDOM.addEventListener(pageNr, 'focus', function(event) {
					KDOM.getEventTarget(event).select();					
				});
				KDOM.addEventListener(pageNr, 'keyup', KPagedList.dispatchPage);
			}
			parent.appendChild(pageNr);

			var pagesTotal = window.document.createElement('span');
			{
				pagesTotal.className = ' KPagedListPages ' + (this.className != 'KPagedList' ? this.className + 'ListPages ' : '');
				pagesTotal.appendChild(window.document.createTextNode(' / ' + n_pages));
			}
			parent.appendChild(pagesTotal);

			var prev = window.document.createElement('a');
			{
				prev.className = ' KPagedListPagePrev ' + (this.className != 'KPagedList' ? this.className + 'ListPagePrev ' : '');
				prev.href = 'javascript:void(0)';
				prev.innerHTML = gettext('$KPAGEDLIST_PREV_BUTTON');
				KDOM.addEventListener(prev, 'click', KList.dispatchPrevious);
			}
			parent.appendChild(prev);

			var first = window.document.createElement('a');
			{
				first.className = ' KPagedListPagePrev ' + (this.className != 'KPagedList' ? this.className + 'ListPagePrev ' : '');
				first.href = 'javascript:void(0)';
				first.innerHTML = gettext('$KPAGEDLIST_FIRST_BUTTON');
				KDOM.addEventListener(first, 'click', KList.dispatchFirst);
			}
			parent.appendChild(first);

			var last = window.document.createElement('a');
			{
				last.className = ' KPagedListPageNext ' + (this.className != 'KPagedList' ? this.className + 'ListPageNext ' : '');
				last.href = 'javascript:void(0)';
				last.innerHTML = gettext('$KPAGEDLIST_LAST_BUTTON');
				KDOM.addEventListener(last, 'click', KList.dispatchLast);
			}
			parent.appendChild(last);

			var next = window.document.createElement('a');
			{
				next.className = ' KPagedListPageNext ' + (this.className != 'KPagedList' ? this.className + 'ListPageNext ' : '');
				next.href = 'javascript:void(0)';
				next.innerHTML = gettext('$KPAGEDLIST_NEXT_BUTTON');
				KDOM.addEventListener(next, 'click', KList.dispatchNext);
			}
			parent.appendChild(next);

		}
	};

	KPagedList.dispatchPage = function(event) {
		var keyCode = event.keyCode;
		if (!keyCode || (keyCode == 13)) {
			var pageNr = KDOM.getEventTarget(event);
			var widget = KDOM.getEventWidget(event, 'KPagedList');
			var page = (!!pageNr.value ? pageNr.value : pageNr.innerHTML);

			var n_pages = Math.ceil(widget.totalSize / widget.pageSize);
			var n_page = parseInt(page);
			if (!isNaN(n_page)) {
				n_page--;
				if (n_page >= 0 && n_page < n_pages) {
					widget.gotoPage(n_page);
					pageNr.blur();
					return;
				}
			}

			pageNr.select();
		}
		else if (keyCode == 27) {
			var pageNr = KDOM.getEventTarget(event);
			var widget = KDOM.getEventWidget(event, 'KPagedList');
			pageNr.value = '' + (widget.lastpage + 1);
			pageNr.blur();
		}
	};
		
	KSystem.included('KPagedList');
});
function KPanel(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.isLeaf = false;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KPanel.prototype = new KWidget();
	KPanel.prototype.constructor = KPanel;
	
	KPanel.prototype.addElement = function(element) {
		if (element instanceof KWidget) {
			this.appendChild(element);
		}
		else {
			throw new KNoExtensionException(this, "addElement");
		}
	};
	
	KSystem.included('KPanel');
});
function KPassword(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.panel.removeChild(this.input);
		delete this.input;
		
		this.input = window.document.createElement('input');
		this.input.setAttribute('type', 'password');
		this.input.name = id;
		this.input.id = id;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KPassword.prototype = new KAbstractInput();
	KPassword.prototype.constructor = KPassword;
	
	KPassword.prototype.createInput = function() {
		return this.input;
	};
	
	KPassword.prototype.getValue = function() {
		if (!this.input.value || (this.input.value == '')) {
			return '';
		}
		else {
			return SHA1(this.input.value);
		}
	};
	
	KPassword.prototype.getRawValue = function() {
		if (!this.input.value || (this.input.value == '')) {
			return '';
		}
		else {
			return this.input.value;
		}
	};
	
	KPassword.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = '';
		}
		else {
			this.input.value = value;
		}
	};
	
	KPassword.prototype.INPUT_ELEMENT = 'input';
	KPassword.LABEL_CONTAINER = 'label';
	KPassword.INPUT_ERROR_CONTAINER = 'errorArea';
	
	KSystem.included('KPassword');
});
function KProgressBar(parent, label, id) {
	if (parent != null) {
		this.isLeaf = true;
		KAbstractInput.call(this, parent, 'text', label, id);
		this.editable = false;
		this.totalWidth = 100;
		this.percentWidth = 1;
		this.percentValue = 0.1;
		this.reversed = false;
	}
}

KSystem.include( [ 'KAbstractInput' ], function() {
	KProgressBar.prototype = new KAbstractInput();
	KProgressBar.prototype.constructor = KProgressBar;

	KProgressBar.prototype.setTotalWidth = function(width) {
		if (this.activated()) {
			this.setStyle( {
				width : width + 'px'
			}, [ KAbstractInput.INPUT_CONTAINER ]);
		}
		this.totalWidth = width;
	};

	KProgressBar.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		this.setStyle( {
			width : this.totalWidth + 'px'
		}, [ KAbstractInput.INPUT_CONTAINER ]);
		this.setStyle( {
			width : (this.percentWidth > this.totalWidth ? this.totalWidth : this.percentWidth) + 'px'
		}, KAbstractInput.INPUT_ELEMENT);
		this.paint(this.percentValue);
	};

	KProgressBar.prototype.setValue = function(value, index) {
		KAbstractInput.prototype.setValue.call(this, (Math.round(value * 10000) / 100) + '%', index);

		this.percentValue = value;
		this.percentWidth = Math.round(value * this.totalWidth);

		if (this.activated()) {
			this.setStyle( {
				width : (this.percentWidth > this.totalWidth ? this.totalWidth : this.percentWidth) + 'px'
			}, KAbstractInput.INPUT_ELEMENT);
			this.paint(this.percentValue);
		}
	};

	KProgressBar.prototype.paint = function(value) {
		var colors = null;
		if (this.reversed) {
			colors = [].concat(KProgressBar.colors);
			colors.reverse();
		} else {
			colors = KProgressBar.colors;
		}
		if (value <= 0.25) {
			this.setStyle( {
				backgroundColor : colors[0].back,
				color : colors[0].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 0.5) {
			this.setStyle( {
				backgroundColor : colors[1].back,
				color : colors[1].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 0.75) {
			this.setStyle( {
				backgroundColor : colors[2].back,
				color : colors[2].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value <= 1) {
			this.setStyle( {
				backgroundColor : colors[3].back,
				color : colors[3].fore
			}, KAbstractInput.INPUT_ELEMENT);
		} else if (value > 1) {
			this.setStyle( {
				backgroundColor : KProgressBar.overflowColor.back,
				color : KProgressBar.overflowColor.fore
			}, KAbstractInput.INPUT_ELEMENT);
		}
	};

	KProgressBar.prototype.getValue = function(rawValue) {
		return this.percentValue;
	};

	KProgressBar.colors = [ {
		back : '#CC0000',
		fore : '#FFFFFF'
	}, {
		back : '#EDD200',
		fore : '#000000'
	}, {
		back : '#40AFFF',
		fore : '#000000'
	}, {
		back : '#009C03',
		fore : '#FFFFFF'
	} ];

	KProgressBar.overflowColor = {
		back : '#FF0000',
		fore : '#FFFFFF'
	};

	KSystem.included('KProgressBar');
});

function KRadioButton(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.selectedOption = null;
		
		this.radiobutton = window.document.createElement('a');
		this.radiobutton.href = 'javascript:void(0)';
		this.radiobutton.style.cursor = 'normal';
		this.radiobutton.className = ' KRadioButtonOptionsContent ';
		this.radiobutton.style.overflow = 'visible';
		this.radiobutton.style.position = 'relative';
		
		this.INPUT_ELEMENT = 'radiobutton';
		
		this.lastElement = null;
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KRadioButton.prototype = new KAbstractInput();
	KRadioButton.prototype.constructor = KRadioButton;
	
	KRadioButton.prototype.clear = function() {
		this.selectedOption = null;
		this.setValue(null);
		this.radiobutton.innerHTML = '';
		KAbstractInput.prototype.clear.call(this);
	};
	
	KRadioButton.prototype.addOption = function(value, caption, selected) {
		var checkBoxItemContainer = window.document.createElement('div');
		checkBoxItemContainer.className = ' KRadioButtonOption KRadioButtonClickableOption ';
		
		var check = window.document.createElement('button');
		check.src = selected ? this.imageOn : this.image;
		check.innerHTML = '&nbsp;';
		check.setAttribute('type', 'button');
		check.value = JSON.stringify(value);
		check.id = this.inputID + this.radiobutton.childNodes.length;
		checkBoxItemContainer.appendChild(check);
		
		var label = window.document.createElement('label');
		label.setAttribute('for', check.id);
		label.alt = check.value;
		label.name = check.id;
		label.innerHTML = caption;
		
		KDOM.addEventListener(checkBoxItemContainer, 'click', KRadioButton.selectOption);
		
		KDOM.addEventListener(checkBoxItemContainer, 'mouseover', KRadioButton.mouseOver);
		KDOM.addEventListener(checkBoxItemContainer, 'mouseout', KRadioButton.mouseOut);
		checkBoxItemContainer.appendChild(label);
		this.radiobutton.appendChild(checkBoxItemContainer);
		
		if (selected) {
			this.setValue(value);
		}
		
		return true;
	};
	
	KRadioButton.prototype.addEventListener = function(eventName, callback, target) {
		if (eventName == 'change') {
			eventName = 'propertychange';
		}
		KAbstractInput.prototype.addEventListener.call(this, eventName, callback, target);
	};

	KRadioButton.prototype.createInput = function() {
		return this.radiobutton;
	};
	
	KRadioButton.prototype.check = function(idx) {
		KDOM.fireEvent(this.radiobutton.childNodes[idx], 'click');
	};
	
	KRadioButton.prototype.uncheck = function(idx) {
		KDOM.fireEvent(this.radiobutton.childNodes[idx], 'click');
	};
	
	KRadioButton.prototype.select = function(element, force) {
		if (!element) {
			this.selectedOption = null;
			this.input.value = '';
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				child.childNodes[0].innerHTML = '&nbsp;';
			}
			return;
		}
		
		if (element.tagName.toLowerCase() == 'label') {
			element = element.previousSibling;
		}
		if (element.tagName.toLowerCase() == 'div') {
			element = element.childNodes[0];
		}
		
		if (element.parentNode.className.indexOf('KRadioButtonOptionSelected') != -1) {
			if (!force) {
				this.selectedOption = null;
				KCSS.removeCSSClass('KRadioButtonOptionSelected', element.parentNode);
				element.innerHTML = '&nbsp;';
			}
		}
		else {
			for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
				var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
				KCSS.removeCSSClass('KRadioButtonOptionSelected', child);
			}
			KCSS.addCSSClass('KRadioButtonOptionSelected', element.parentNode);
			this.selectedOption = JSON.parse(element.value);
			element.innerHTML = '&nbsp;';
			
		}
		
		KDOM.fireEvent(this.childDiv(this.INPUT_ELEMENT), 'propertychange');
	};
	
	KRadioButton.prototype.setValue = function(value) {
		if (!value) {
			delete this.selectedOption;
			this.selectedOption = null;
		}
		else {
			this.selectedOption = value;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.childDiv(this.INPUT_ELEMENT).childNodes.length; i++) {
			var child = this.childDiv(this.INPUT_ELEMENT).childNodes[i];
			if (child.childNodes[0].value == this.input.value) {
				this.select(child.childNodes[0], true);
				break;
			}
		}
	};
	
	KRadioButton.prototype.getValue = function() {
		if (!!this.selectedOption) {
			return this.selectedOption;
		}
		else {
			return '';
		}
	};
	
	KRadioButton.mouseOver = function(event) {
		var element = KDOM.getEventTarget(event);
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}
		if (element.className.indexOf('KRadioButtonOptionSelected') == -1) {
			KCSS.addCSSClass('KRadioButtonOptionOver', element);
		}
	};
	
	KRadioButton.mouseOut = function(event) {
		var element = KDOM.getEventTarget(event);		
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}
		KCSS.removeCSSClass('KRadioButtonOptionOver', element);
	};
	
	KRadioButton.selectOption = function(event) {
		KDOM.stopEvent(event);

		var element = KDOM.getEventTarget(event);		
		while(element.className.indexOf('KRadioButtonClickableOption') == -1) {
			element = element.parentNode;
		}		
		if (!element) {
			return;
		}

		var widget = KDOM.getEventWidget(event);
		widget.select(element);
	};
	
	KRadioButton.updateValue = function(event) {
		var widget = KDOM.getEventWidget(event);
		widget.onKeyPressed();
	};
	
	KRadioButton.OPTION_CONTAINER_DIV = 'radiobutton';
	KRadioButton.OPENED = null;
	
	KSystem.included('KRadioButton');
});
function KRichText(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		
		this.editorInstance = null;
		this.waitForVisibility = -1;
		this.configs = undefined;

		this.textarea = window.document.createElement('div');
		this.textarea.name = this.id + '_textarea';
		this.textarea.id = this.id + '_textarea';
		this.textarea.setAttribute('contenteditable', 'true');
		this.addCSSClass(this.className + 'ContentEditable KRichTextContentEditable', this.textarea);
		this.height = 200;
		
		this.INPUT_ELEMENT = 'textarea';

		this.content.appendChild(this.textarea);
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KRichText.prototype = new KAbstractInput();
	KRichText.prototype.constructor = KRichText;
	
	/*KRichText.prototype.focus = function() {
		KAbstractInput.prototype.focus.call(this);
		this.textarea.focus();
	};*/

	KRichText.prototype.createInput = function() {
		return this.textarea;
	};
	
	KRichText.prototype.getValue = function() {
		return this.textarea.innerHTML;
	};
	
	KRichText.prototype.setValue = function(value) {
		this.textarea.innerHTML = value;
	};
	
	KRichText.prototype.setConfig = function(config) {
		this.configs = config;
	};

	KRichText.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		this.waitForVisibility = KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').editor()', KRichText.CKE_DELAY);
	};
	
	KRichText.prototype.editor = function() {
		var p = this.panel;
		while (p.tagName.toLowerCase() != 'body') {
			if (p.style.display == 'none' || p.style.visibility == 'hidden') {
				this.waitForVisibility = KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').editor()', KRichText.CKE_DELAY);
				return;
			}
			p = p.parentNode;
		}
		
		this.waitForVisibility = -1;
		if (window.CKEDITOR) {
			this.editorInstance = CKEDITOR.inline(this.textarea, this.configs || {
				
				language : 'pt',
				coreStyles_bold : {
					element : 'b'
				},
				coreStyles_italic : {
					element : 'i'
				},
				fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
				fontSize_style : {
					element : 'font',
					attributes : {
						'size' : '#(size)'
					}
				},
				height : this.height + 'px',
				toolbar : [
					[
						'Bold',
						'Italic',
						'Underline'
					],
					[
						'NumberedList',
						'BulletedList',
						'Outdent', 
						'Indent',
						'-',
						'Blockquote'
					],
					[
						'Source',
						'-',
						'Undo',
						'Redo'
					],
					[
						'Link',
						'Unlink',
						'HorizontalRule', 
						'SpecialChar',
						'-',
						'RemoveFormat',
						'FontSize',
						'Format'
					],
					[
						'JustifyLeft',
						'JustifyCenter',
						'JustifyBlock',
						'JustifyRight'
					]
				]
			});
		}
	};
	
	KRichText.prototype.destroy = function() {
		if (this.waitForVisibility != -1) {
			KSystem.removeTimer(this.waitForVisibility);
		}
		if (!!this.editorInstance) {
			this.editorInstance.destroy(true);
		}
		KAbstractInput.prototype.destroy.call(this);
	};
	
	KRichText.prototype.actOnEnter = function() {
		return false;
	};	

	KRichText.prototype.INPUT_ELEMENT = 'input';
	KRichText.LABEL_CONTAINER = 'label';
	KRichText.INPUT_ERROR_CONTAINER = 'errorArea';
	KRichText.CKE_DELAY = 750;
	
	KSystem.included('KRichText');
});
function KRow(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.editMode = false;
		this.isDrawn = false;
		this.data = null;
		this.children = null;
		this.requestedPage = this.nPage = 0;
		this.nChildren = 0;
		this.perPage = 10;
		this.totalCount = null;
		this.display = false;
		
		this.kinky = Kinky.bunnyMan;
		this.breadcrumb = '';
		
		this.id = this.id || this.kinky.addWidget(this);
		this.hash = null;
		
		this.panel = this.content = window.document.createElement('tr');
		this.panel.id = this.id;
		this.panel.className = ' KWidgetPanel ' + this.className + ' ';
		this.panel.style.overflow = this.overflow;
		this.panel.style.position = 'relative';
		// this.panel.style.clear = 'both';
		
		this.window = false;
		this.closeButton = null;
		this.minimizeButton = null;
		this.maximizeButton = null;
		this.buttons = 0;
		this.movable = false;
		this.frame = false;
		this.effects = new Object();
		this.waiting = false;
		
		this.error = null;
		this.request = null;
		this.response = null;
		this.needAuthentication = false;
		
		this.fromListener = false;
		this.lastWidth = 0;
		this.lastHeight = 0;
		this.scroll = {};
	}
}

KSystem.include([
	'KWidget',
	'KCell'
], function() {
	
	KRow.prototype = new KWidget();
	KRow.prototype.constructor = KRow;
	
	KRow.prototype.go = function() {
		this.draw();
	};
	
	KRow.prototype.draw = function() {
		this.panel.style.overflow = 'visible';
		
		for ( var element in this.childWidgets()) {
			this.childWidget(element).go();
		}
		this.activate();
	};
	
	KRow.prototype.appendChild = function(element) {
		if (element instanceof KCell) {
			KWidget.prototype.appendChild.call(this, element);
		}
	};
	
	KSystem.included('KRow');
});
KSystem.include([
	'KScroll',
	'KMinimalScroll'
], function() {
	KSystem.included('KScrollBars');
});

function KScrolledList(parent) {
	if (parent) {
		KList.call(this, parent);
		this.nextButton = null;
	}
}

KSystem.include([
	'KList'
], function() {
	KScrolledList.prototype = new KList();
	KScrolledList.prototype.constructor = KScrolledList;
	
	KScrolledList.prototype.afterNext = function() {
		this.nextButton.style.display = 'none';
	};
	
	KScrolledList.prototype.pageVisible = function(n_page) {
		if ((this.footer.childNodes.length == 0) && (this.pageSize != 0)) {
			this.addPagination(this.footer, this.totalSize);
		}
		
		KCSS.setStyle({
			display : 'inline'
		}, [
			this.pages['page' + n_page]
		]);
		
		if (this.autoRotate != 0) {
			this.autoRotateTimer = KSystem.addTimer('KList.rotate(\'' + this.id + '\')', this.autoRotate);
		}
		
		if (!!this.totalSize && (this.totalSize != 0) && ((this.totalSize == this.pageSize) || (Math.floor(this.totalSize / this.pageSize) <= n_page))) {
			this.afterNext();
		}
		else if (n_page < 0) {
			this.beforePrevious();
		}
	};
	
	KScrolledList.prototype.addPagination = function(parent, totalSize) {
		if (parent === this.header) {
			return;
		}
		this.nextButton = window.document.createElement('button');
		{
			this.nextButton.setAttribute("type", "button");
			this.nextButton.className = ' KListPaginationNext ' + (this.className != 'KList' ? this.className + 'PaginationNext ' : '');
			this.nextButton.appendChild(window.document.createTextNode('ver mais'));
			KDOM.addEventListener(this.nextButton, 'click', KList.dispatchNext);
		}
		parent.appendChild(this.nextButton);
	};
	
	KSystem.included('KScrolledList');
});
function KScroll(parent, div) {
	if (!!parent) {
		this.prefix = 'scroll';
		this.divTarget = (!!div ? div : 'panel');
		KAddOn.call(this, parent);
		this.listeners = new Array();
		this.enabled = false;
		this.position = new Object();
		this.position.ph = 0;
		this.position.pv = 0;
		this.position.dh = 0;
		this.position.dv = 0;
		this.buttonclick = false;
		KScroll.deactiveScrollKeys = false;

		if (window.attachEvent) {
			parent.panel.attachEvent('onmousewheel', KScroll.mouseWheelActive);
			parent.panel.attachEvent('onkeydown', KScroll.downKey);
		}
		else {
			if (KBrowserDetect.browser == 1) {
				parent.panel.addEventListener('DOMMouseScroll', KScroll.mouseWheelActive, false);
			}
			else {
				parent.panel.addEventListener('mousewheel', KScroll.mouseWheelActive, false);
			}
			parent.panel.addEventListener('keydown', KScroll.downKey, false);
		}

	}
}
KSystem.include([
	'KAddOn'
], function() {
	KScroll.prototype = new KAddOn();
	KScroll.prototype.constructor = KScroll;
	
	KScroll.prototype.addScrollListener = function(callback, widget) {
		this.listeners.push({
			widget : widget || this.parent,
			callback : callback
		});
	};
	
	KScroll.prototype.removeScrollListener = function(widget) {
		for ( var listener in this.listeners) {
			if (this.listeners[listener].widget.id == widget.id) {
				this.listeners.splice(listener, 1);
				break;
			}
		}
	};
	
	KScroll.prototype.removeScrollListeners = function(callback, widget) {
		delete this.listeners;
		this.listeners = new Array();
	};
	
	KScroll.prototype.draw = function() {
		this.parent.addResizeListener(KScroll.onWidgetResize);
		var root = this.parent[this.divTarget];
		
		this.barV = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'VerticalBar', this.barV);
			this.barV.id = this.id;
			
			this.upperV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalUpper', this.upperV);
				this.upperV.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.upperV, 'click', KScroll.onUp);
			}
			this.barV.appendChild(this.upperV);
			
			this.scrollerV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalScroller', this.scrollerV);
				this.scrollerV.innerHTML = '&nbsp;';
			}
			this.barV.appendChild(this.scrollerV);
			
			this.downerV = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'VerticalDowner', this.downerV);
				this.downerV.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.downerV, 'click', KScroll.onDown);
			}
			this.barV.appendChild(this.downerV);
		}
		root.appendChild(this.barV);
		
		this.barH = window.document.createElement('div');
		{
			KCSS.addCSSClass(this.className + 'HorizontalBar', this.barH);
			this.barH.id = this.id;
			
			this.upperH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalUpper', this.upperH);
				this.upperH.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.upperH, 'click', KScroll.onLeft);
			}
			this.barH.appendChild(this.upperH);
			
			this.scrollerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalScroller', this.scrollerH);
				this.scrollerH.innerHTML = '&nbsp;';
			}
			this.barH.appendChild(this.scrollerH);
			
			this.downerH = window.document.createElement('button');
			{
				KCSS.addCSSClass(this.className + 'HorizontalDowner', this.downerH);
				this.downerH.innerHTML = '&nbsp;';
				KDOM.addEventListener(this.downerH, 'click', KScroll.onRight);
			}
			this.barH.appendChild(this.downerH);
		}
		root.appendChild(this.barH);
		
		var rootH = KCSS.getComputedStyle(root).height;
		if (!rootH) {
			rootH = KDOM.getBrowserHeight() + 'px';
		}
		KCSS.setStyle({
			overflow : 'hidden',
			height : rootH
		}, [
			root
		]);
		
		this.offsetV = this.parent.content.offsetTop;
		this.offsetH = this.parent.content.offsetLeft;
		this.bottombH = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.downerV).height);
		this.rightbW = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.upperH).width);
		this.position.pv = this.topbH = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.upperV).height);
		if (isNaN(this.position.pv)) {
			this.position.pv = 0;
		}
		this.position.ph = this.leftbW = KDOM.normalizePixelValue(KCSS.getComputedStyle(this.downerH).width);
		if (isNaN(this.position.ph)) {
			this.position.ph = 0;
		}
		
	};
	
	KScroll.prototype.scrollTo = function(pos) {
		this.dispatchScroll({
			h : pos.x || this.position.h,
			v : pos.y || this.position.v
		});
	};
	
	KScroll.prototype.dispatchScroll = function(currentPos) {
		if (!!currentPos) {
			if (!!currentPos.h && (((currentPos.h > this.leftbW)))) {
				if (currentPos.h > this.viewportW - KDOM.normalizePixelValue(this.scrollerH.style.width) - this.rightbW) {
					currentPos.h = (this.viewportW - KDOM.normalizePixelValue(this.scrollerH.style.width) - this.rightbW);
				}
			}
			else {
				currentPos.h = this.leftbW;
			}
			if (!!currentPos.v && (((currentPos.v > this.topbH)))) {
				if (currentPos.v > this.viewportH - KDOM.normalizePixelValue(this.scrollerV.style.height) - this.bottombH) {
					currentPos.v = (this.viewportH - KDOM.normalizePixelValue(this.scrollerV.style.height) - this.bottombH);
				}
			}
			else {
				currentPos.v = this.topbH;
			}
		}
		else {
			currentPos = {
				h : KDOM.normalizePixelValue(this.scrollerH.style.left),
				v : KDOM.normalizePixelValue(this.scrollerV.style.top)
			};
		}
		if (isNaN(currentPos.v)) {
			currentPos.v = 0;
		}
		if (isNaN(currentPos.h)) {
			currentPos.h = 0;
		}
		
		var hasDeltaV = currentPos.v != this.position.pv;
		var hasDeltaH = currentPos.h != this.position.ph;
		if (hasDeltaV || hasDeltaH) {
			
			this.position.dh = currentPos.h - this.position.ph;
			this.position.dv = currentPos.v - this.position.pv;
			this.position.ph = currentPos.h;
			this.position.pv = currentPos.v;
			
			if (this.buttonclick) {
				this.buttonclick = false;
				var scroll = this;
				if (hasDeltaV) {
					KEffects.addEffect(this.scrollerV, {
						f : KEffects.easeOutSine,
						type : 'move',
						duration : 300,
						lock : {
							x : true
						},
						go : {
							y : currentPos.v
						},
						onAnimate : function(widget, tween) {
							scroll.parent.scroll({
								dh : scroll.position.dh,
								dv : scroll.position.dv,
								ph : scroll.position.ph,
								pv : tween.y
							}, scroll);
						}
					
					});
				}
				if (hasDeltaH) {
					KEffects.addEffect(this.scrollerH, {
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 300,
						lock : {
							y : true
						},
						go : {
							x : currentPos.h
						},
						onAnimate : function(widget, tween) {
							scroll.parent.scroll({
								dh : scroll.position.dh,
								dv : scroll.position.dv,
								pv : scroll.position.pv,
								ph : tween.x
							}, scroll);
						}
					
					});
				}
			}
			else {
				this.parent.scroll(this.position, this);
			}
			
			for ( var listener in this.listeners) {
				KSystem.addTimer('Kinky.getWidget(\'' + this.id + '\').listeners[' + listener + '].callback(Kinky.getWidget(\'' + this.id + '\').listeners[' + listener + '].widget, Kinky.getWidget(\'' + this.id + '\'), ' + JSON.stringify(this.position) + ')', 0);
			}
		}
	};
	
	KScroll.prototype.handleMouseWheelScroll = function(scrollDelta) {
		if (this.viewportH >= this.parent.content.offsetHeight) {
			return;
		}
		this.buttonclick = true;
		if (scrollDelta < 0) {
			this.down();
		}
		else {
			this.up();
		}
	};
	
	KScroll.prototype.handleUpButton = function() {
		this.buttonclick = true;
		this.up(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleDownButton = function() {
		this.buttonclick = true;
		this.down(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleLeftButton = function() {
		this.buttonclick = true;
		this.left(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.handleRightButton = function() {
		this.buttonclick = true;
		this.right(KScroll.PAGE_RATIO);
	};
	
	KScroll.prototype.up = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph,
			v : this.position.pv - Math.round(ratio * this.viewportH * this.proportionV)
		});
	};
	
	KScroll.prototype.down = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph,
			v : this.position.pv + Math.round(ratio * this.viewportH * this.proportionV)
		});
	};
	
	KScroll.prototype.left = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph - Math.round(ratio * this.viewportW * this.proportionH),
			v : this.position.pv
		});
	};
	
	KScroll.prototype.right = function(ratio) {
		ratio = (!ratio ? KScroll.WHEEL_RATIO : ratio);
		this.dispatchScroll({
			h : this.position.ph + Math.round(ratio * this.viewportW * this.proportionH),
			v : this.position.pv
		});
	};
	
	KScroll.getScrollDiv = function(element) {
		var toReturn = element;
		
		try {
			while (!/^widget([0-9]+)$/.test(toReturn.id) && !/^scroll([0-9]+)$/.test(toReturn.id)) {
				toReturn = toReturn.parentNode;
			}
			if (!/^scroll([0-9]+)$/.test(toReturn.id)) {
				return null;
			}
		}
		catch (e) {
			return null;
		}
		return toReturn;
	};
	
	KScroll.getScrollFromEvent = function(event) {
		return KScroll.getScrollInWidgets(KDOM.getEventWidget(event));
	};
	
	KScroll.getScrollInWidgets = function(scrolledWidget) {
		if (!scrolledWidget || !(scrolledWidget instanceof KWidget)) {
			return null;
		}
		for ( var a in scrolledWidget.addons) {
			if (a.indexOf('scroll') != -1) {
				return scrolledWidget.addons[a];
			}
		}
		return KScroll.getScrollInWidgets(scrolledWidget.parent);
	};
	
	KScroll.getScrollFromWidget = function(scrolledWidget) {
		for ( var a in scrolledWidget.addons) {
			if (a.indexOf('scroll') != -1) {
				return scrolledWidget.addons[a];
			}
		}
		return null;
	};
	
	KScroll.onWidgetResize = function(scrolledWidget, size) {
		var scroll = KScroll.getScrollFromWidget(scrolledWidget);
		
		scroll.viewportH = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).height);
		scroll.viewportW = KDOM.normalizePixelValue(KCSS.getComputedStyle(scroll.parent.panel).width);
		scroll.proportionV = (scroll.viewportH - scroll.topbH - scroll.bottombH - scroll.offsetV) / scroll.parent.content.offsetHeight;
		scroll.proportionH = (scroll.viewportW - scroll.leftbW - scroll.rightbW - scroll.offsetH) / scroll.parent.content.offsetWidth;
		
		if (scroll.viewportW < scroll.parent.content.offsetWidth) {
			var width = Math.round(scroll.proportionH * scroll.viewportW);
			var scrollerHX = KDOM.normalizePixelValue(scroll.scrollerH.style.left);
			
			if (isNaN(scrollerHX)) {
				scrollerHX = scroll.leftbW;
			}
			else {
				var contentX = KDOM.normalizePixelValue(scroll.parent.content.style.left);
				if (isNaN(contentX)) {
					contentX = 0;
				}
				scrollerHX = Math.round(-contentX * scroll.proportionH) + scroll.leftbW;
			}
			scroll.position.ph = scrollerHX;
			
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'block'
			}, [
				scroll.barH
			]);
			
			KCSS.setStyle({
				width : width + 'px',
			}, [
				scroll.scrollerH
			]);
			
			KEffects.addEffect(scroll.scrollerH, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					0,
					scroll.viewportW - width - scroll.rightbW,
					0,
					scroll.leftbW
				],
				from : {
					x : scrollerHX
				},
				lock : {
					y : true
				},
				onAnimate : function(wx, tween) {
					scroll.dragging = true;
					scroll.blockclick = true;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				onComplete : function(wx, tween) {
					scroll.dragging = false;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				width : scroll.viewportW + 'px',
				display : 'none'
			}, [
				scroll.barH
			]);
		}
		
		if (scroll.viewportH < scroll.parent.content.offsetHeight) {
			var height = Math.round(scroll.proportionV * scroll.viewportH);
			var scrollerVY = KDOM.normalizePixelValue(scroll.scrollerV.style.top);
			
			if (isNaN(scrollerVY)) {
				scrollerVY = scroll.topbH;
			}
			else {
				var contentY = KDOM.normalizePixelValue(scroll.parent.content.style.top);
				if (isNaN(contentY)) {
					contentY = 0;
				}
				scrollerVY = Math.round(-contentY * scroll.proportionV) + scroll.topbH;
			}
			
			scroll.position.pv = scrollerVY;
			
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'block'
			}, [
				scroll.barV
			]);
			KCSS.setStyle({
				height : height + 'px',
				top : scrollerVY + 'px'
			}, [
				scroll.scrollerV
			]);
			KEffects.addEffect(scroll.scrollerV, {
				f : KEffects.linear,
				type : 'drag',
				rectangle : [
					scroll.topbH,
					0,
					scroll.viewportH - height - scroll.bottombH,
					0
				],
				from : {
					y : scrollerVY
				},
				lock : {
					x : true
				},
				onAnimate : function(wx, tween) {
					scroll.dragging = true;
					scroll.blockclick = true;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				onComplete : function(wx, tween) {
					scroll.dragging = false;
					KScroll.getScrollFromWidget(wx).dispatchScroll();
				},
				applyToElement : true
			});
		}
		else {
			KCSS.setStyle({
				height : scroll.viewportH + 'px',
				display : 'none'
			}, [
				scroll.barV
			]);
		}
	};
	
	KScroll.activateScroll = function(event) {
		var widget = null;
		try {
			widget = KDOM.getEventWidget(event);
		}
		catch (e) {
		}
		
		if (!widget) {
			widget = KCombo.getEventWidget(event);
		}
		
		if (widget) {
			var scroll = KScroll.getScroll(widget);
			if (scroll) {
				KScroll.activeScroll = scroll.panel.style.display == 'block' ? scroll : null;
			}
		}
	};
	
	KScroll.deactivateScroll = function(event) {
		var widget = null;
		try {
			widget = KDOM.getEventWidget(event);
		}
		catch (e) {
		}
		
		if (!widget) {
			widget = KCombo.getEventWidget(event);
		}
		
		if (widget) {
			var scroll = KScroll.getScroll(widget);
			if (scroll) {
				KScroll.activeScroll = null;
			}
		}
	};
	
	KScroll.onUp = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleUpButton();
	};
	
	KScroll.onDown = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleDownButton();
	};
	
	KScroll.onLeft = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleLeftButton();
	};
	
	KScroll.onRight = function(event) {
		event = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);
		if (scroll.blockclick) {
			scroll.blockclick = false;
			return;
		}
		scroll.handleRightButton();
	};
	
	KScroll.downKey = function(event) {
		if (!KScroll.deactiveScrollKeys) {
			var shell = Kinky.getShell();
			if (!shell) {
				return;
			}
			
			var scroll = KScroll.getScrollFromWidget(shell);
			
			if (!scroll) {
				return;
			}
			
			switch (event.keyCode) {
				case 34: { // PAGE DOWN
					scroll.buttonclick = true;
					scroll.down(KScroll.PAGE_RATIO);
					break;
				}
				case 40: { // DOWN
					scroll.buttonclick = true;
					scroll.down(KScroll.LINE_RATIO);
					break;
				}
				case 33: { // PAGE UP
					scroll.buttonclick = true;
					scroll.up(KScroll.PAGE_RATIO);
					break;
				}
				case 38: { // UP
					scroll.buttonclick = true;
					scroll.up(KScroll.LINE_RATIO);
					break;
				}
			}
		}
	};
	
	KScroll.disableKeys = function() {
		KScroll.deactiveScrollKeys = true;
	};
	
	KScroll.enableKeys = function() {
		KScroll.deactiveScrollKeys = false;
	};
	
	KScroll.mouseWheelActive = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var scroll = KScroll.getScrollFromEvent(event);

		if (!scroll) {
			return;
		}
		var widget = KDOM.getEventWidget(event);
		if (widget.id != scroll.parent.id && widget instanceof KCombo) {
			if (KDOM.getEventTarget(event).className.indexOf('Option') != -1) {
				return;
			}
		}

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		mouseEvent.returnValue = false;
		
		var delta = 0;
		if (mouseEvent.wheelDelta) {
			delta = mouseEvent.wheelDelta;
		}
		else if (mouseEvent.detail) {
			delta = -mouseEvent.detail;
		}
		
		if (delta) {
			scroll.handleMouseWheelScroll(delta);
		}
	};
	
	KScroll.make = function(parent, selector) {
		var scrollindex = selector.indexOf('Scroll');
		var spaceindex = selector.lastIndexOf('.', scrollindex);
		var scrollClass = selector.substring(spaceindex + 1, scrollindex) + 'Scroll';

		if (KSystem.isAddOn(scrollClass)) {
			var scroll = null;
			eval('scroll = new ' + scrollClass + '(parent)');
			scroll.go();
		}
	};

	KScroll.WHEEL_RATIO = 0.07;
	KScroll.PAGE_RATIO = 0.2;
	KScroll.LINE_RATIO = 0.05;
	
	KScroll.deactiveScrollKeys = true;
	
	KSystem.registerAddOn('KScroll');
	KSystem.included('KScroll');
});

function KSelect(parent, label, id) {
	if (parent != null) {
		KCombo.call(this, parent, label, id);
		this.selectedOption = {};
	}
}

KSystem.include([
	'KCombo'
], function() {
	
	KSelect.prototype = new KCombo();
	KSelect.prototype.constructor = KSelect;
	
	KSelect.prototype.blur = function() {
		KCSS.removeCSSClass('KComboOpened', this.content);
		this.childDiv(KWidget.ROOT_DIV).removeChild(this.optionsPanel);
		this.optionsToggled = false;
	};
	
	KSelect.prototype.validate = function() {
		if (this.validations.length != 0) {
			var value = this.getValue(true);
			for ( var index in this.validations) {
				if (((this.validations[index].regex.source == '(.+)') || (this.validations[index].regex.source == '.+')) && (value.length == 0)) {
					this.validationIndex = index;
					this.addCSSClass('KAbstractInputError');
					return this.validations[index].message;
				}
				for ( var k in value) {
					if (!this.validations[index].regex.test(value[k])) {
						this.validationIndex = index;
						this.addCSSClass('KAbstractInputError');
						return this.validations[index].message;
					}
				}
				
				this.errorArea.innerHTML = '';
				this.removeCSSClass('KAbstractInputError');
			}
		}
		return true;
	};
	
	KSelect.prototype.onKeyPressed = function(event) {
		switch (event.keyCode) {
			case 40: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markNext();
				break;
			}
			case 38: {
				KDOM.stopEvent(event);
				if (!this.optionsToggled) {
					KCombo.toggleOptions(null, this);
				}
				this.markPrevious();
				break;
			}
			case 8: {
				KDOM.stopEvent(event);
				if (this.comboText.innerHTML.length != 0) {
					this.comboText.innerHTML = this.comboText.innerHTML.substr(0, this.comboText.innerHTML.length - 1);
					this.filter();
				}
				break;
			}
			case 27: {
				KDOM.stopEvent(event);
				this.innerLabel.innerHTML = '';
				this.filter();
				KCombo.toggleOptions(null, this, true);
				break;
			}
			default: {
				var key = (event.charCode != 0 ? event.charCode : event.keyCode);
				if (key != 0) {
					if (!this.optionsToggled) {
						KCombo.toggleOptions(null, this);
					}
					this.innerLabel.innerHTML += String.fromCharCode(key);
					this.filter();
				}
			}
		}
	};
	
	KSelect.prototype.filter = function() {
		var regex = new RegExp(this.comboText.innerHTML, 'i');
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].tagName.toLowerCase() == 'label') {
				if (!regex.test(this.optionsContent.childNodes[i].innerHTML.replace(/\$/, '\\$'))) {
					this.optionsContent.childNodes[i].style.display = 'none';
				}
				else {
					this.optionsContent.childNodes[i].style.display = 'block';
				}
			}
		}
	};
	
	KSelect.prototype.select = function(element) {
		if (!!element && !!this.selectedOption[element.alt]) {
			this.innerLabel.innerHTML = '';
			return;
		}
		
		if (!!element) {
			this.selectedOption[element.alt] = true;
			
			var selected = window.document.createElement('div');
			{
				selected.alt = element.alt;
				selected.innerHTML = element.innerHTML;
				var removeButton = window.document.createElement('button');
				{
					removeButton.innerHTML = 'x';
					this.addEventListener('click', KSelect.removeSelection, removeButton);
				}
				selected.appendChild(removeButton);
			}
			this.comboText.appendChild(selected);
		}
		else {
			delete this.selectedOption;
			this.selectedOption = new Object();
			this.comboText.innerHTML = '';
		}
		this.innerLabel.innerHTML = '';
		
		KCombo.toggleOptions(null, this, true);
		
		this.currentIndex = -1;
		KDOM.fireEvent(this.combo, 'change');
	};
	
	KSelect.prototype.getValue = function() {
		var toReturn = new Array();
		
		for ( var index in this.selectedOption) {
			toReturn.push(JSON.parse(index));
		}
		
		return toReturn;
	};
	
	KSelect.prototype.setValue = function(value) {
		if (!value) {
			this.select();
			return;
		}
		KAbstractInput.prototype.setValue.call(this, value);
		
		for ( var i = 0; i != this.optionsContent.childNodes.length; i++) {
			if (this.optionsContent.childNodes[i].alt == this.input.value) {
				this.select(this.optionsContent.childNodes[i]);
			}
		}
	};
	
	KSelect.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this);
		
		this.inputContainer.appendChild(this.dropButton);
		if (KSystem.isIncluded('KScroll') && ((this.scrollOps || KCombo.SCROLL_OPS))) {
			try {
				this.addScroll(KScroll.VSCROLL, this.scrollOps || KCombo.SCROLL_OPS, KCombo.OPTIONS_CONTENT_DIV, KCombo.OPTIONS_CONTAINER_DIV, KCombo.OPTIONS_DIV);
			}
			catch (e) {
			}
		}
		else {
			this.optionsPanel.style.overflow = 'auto';
			this.optionsContainer.style.overflow = 'visible';
			this.optionsContent.style.position = 'relative';
		}
		
		this.addEventListener('keypress', KCombo.keyPressed, this.INPUT_ELEMENT);
	};
	
	KSelect.removeSelection = function(event) {
		var button = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);
		delete widget.selectedOption[button.parentNode.alt];
		button.parentNode.parentNode.removeChild(button.parentNode);
	};
	
	KSystem.included('KSelect');
});
function KShare(parent) {
	if (!!parent) {
		this.prefix = 'share';
		KAddOn.call(this, parent);
		this.buzz = new Object();
		
		this.panel = window.document.createElement(this.isAside ? 'aside' : this.tagNames[Kinky.HTML_READY][KWidget.ROOT_DIV]);
		{
			this.panel.id = this.id;
			this.panel.className = ' KWidgetPanel ' + this.className + ' ';
			this.panel.style.position = 'relative';
		}
		
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KShare.prototype = new KAddOn();
	KShare.prototype.constructor = KShare;
	
	KShare.prototype.addSocialLink = function(link) {
		if (!this.config) {
			this.config = {
				links : new Array()
			};
		}
		else if (!this.config.links) {
			this.config.links = new Array();
		}
		this.config.links.push(link);
	};
	
	KShare.prototype.onBuzzFacebook = function(data) {
		if (!!data.shares) {
			if (data.shares > 999999) {
				data.shares = KSystem.formatDouble(data.shares / 1000000, 1) + 'm';
			}
			else if (data.shares > 999) {
				data.shares = KSystem.formatDouble(data.shares / 1000, 1) + 'k';
			}
		}
		else {
			data.shares = 0;
		}
		this.buzz['facebook'].count = data.shares;
		this.buzz['facebook'].element.innerHTML = data.shares;
		
		window.document.getElementsByTagName('head')[0].removeChild(this.facebookp);
	};
	
	KShare.prototype.onBuzzTwitter = function(data) {
		if (!!data.count) {
			if (data.count > 999999) {
				data.count = KSystem.formatDouble(data.count / 1000000, 1) + 'm';
			}
			else if (data.count > 999) {
				data.count = KSystem.formatDouble(data.count / 1000, 1) + 'k';
			}
		}
		else {
			data.count = 0;
		}
		this.buzz['twitter'].count = data.count;
		this.buzz['twitter'].element.innerHTML = data.count;
		window.document.getElementsByTagName('head')[0].removeChild(this.twitterp);
	};
	
	KShare.prototype.draw = function() {
		if (!this.config || !this.config.links) {
			return;
		}
		
		var shellConf = Kinky.getShellConfig(this.shell);
		var lang = (!!shellConf.headers && !!shellConf.headers['Accept-Language'] ? shellConf.headers['Accept-Language'] : (!!KLocale.LANG ? KLocale.LANG : 'pt'));
		
		for ( var l in this.config.links) {
			this.buzz[this.config.links[l].rel] = new Object();
			this.buzz[this.config.links[l].rel].count = 0;
			var button = window.document.createElement(this.config.links[l].rel != 'facebook_like' ? 'a' : 'div');
			{
				button.innerHTML = this.config.links[l].title;
				KCSS.addCSSClass('KShareLink KShareLink_' + this.config.links[l].rel, button);
				if (!!this.config.links[l].href) {
					switch (this.config.links[l].rel) {
						case 'facebook': {
							button.href = 'https://www.facebook.com/sharer.php?u=' + encodeURIComponent(this.config.links[l].href);
							break;
						}
						case 'twitter': {
							button.href = 'http://twitter.com/home?status=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description + ' ' : '') + this.config.links[l].href);
							break;
						}
						case 'orkut': {
							button.href = 'http://promote.orkut.com/preview?nt=orkut.com&tt=' + encodeURIComponent(this.config.links[l].href) + '&cn=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description : '')) + '&du=' + encodeURIComponent(this.config.links[l].href);
							break;
						}
						case 'facebook_like': {
							button.className = ' KShareItem KShareItem_facebook_like ';
							button.innerHTML = '<iframe src="http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent(this.config.links[l].href) + '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>';
							break;
						}
						default: {
							
						}
					}
				}
				else {
					switch (this.config.links[l].rel) {
						case 'facebook': {
							button.href = 'https://www.facebook.com/sharer.php?u=' + encodeURIComponent(shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + '?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href)));
							break;
						}
						case 'twitter': {
							button.href = 'http://twitter.com/home?status=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description + ' ' : '') + (!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href));
							break;
						}
						case 'orkut': {
							button.href = 'http://promote.orkut.com/preview?nt=orkut.com&tt=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.title ? this.parent.data.metadata.title : '')) + '&cn=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.description ? this.parent.data.metadata.description : '')) + '&du=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href));
							break;
						}
						case 'facebook_like': {
							button.className = ' KShareItem KShareItem_facebook_like ';
							button.innerHTML = '<iframe src="http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent(shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + '?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>';
							break;
						}
						default: {
							
						}
					}
				}
				if (this.config.links[l].rel != 'facebook_like') {
					button.target = '_blank';
					
					var link = button;
					
					button = window.document.createElement('div');
					KCSS.addCSSClass('KShareItem KShareItem_' + this.config.links[l].rel, button);
					
					button.appendChild(link);
					
					var count = window.document.createElement('div');
					count.className = ' KShareCount KShareCount_' + this.config.links[l].rel + ' ';
					button.appendChild(count);
					
					var pointers = window.document.createElement('s');
					count.appendChild(pointers);
					
					var pointeri = window.document.createElement('i');
					count.appendChild(pointeri);
					
					var number = window.document.createElement('span');
					number.innerHTML = '0';
					count.appendChild(number);
					
					this.buzz[this.config.links[l].rel].element = number;
					
				}
				
			}
			this.panel.appendChild(button);
		}
		
		this.retrieveBuzz();
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(this.panel);
		}
		else {
			this.parent.container.appendChild(this.panel);
		}
	};
	
	KShare.prototype.retrieveBuzz = function() {
		for ( var l in this.config.links) {
			if (!!this.config.links[l].href) {
				switch (this.config.links[l].rel) {
					case 'facebook': {
						this.facebookp = window.document.createElement('script');
						this.facebookp.charset = 'utf-8';
						this.facebookp.src = 'https://graph.facebook.com/?id=' + encodeURIComponent(this.config.links[l].href) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzFacebook';
						window.document.getElementsByTagName('head')[0].appendChild(this.facebookp);
						break;
					}
					case 'twitter': {
						this.twitterp = window.document.createElement('script');
						this.twitterp.charset = 'utf-8';
						this.twitterp.src = 'http://urls.api.twitter.com/1/urls/count.json?url=' + encodeURIComponent(this.config.links[l].href) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzTwitter';
						window.document.getElementsByTagName('head')[0].appendChild(this.twitterp);
						break;
					}
					case 'orkut': {
						break;
					}
					default: {
						
					}
				}
			}
			else {
				switch (this.config.links[l].rel) {
					case 'facebook': {
						this.facebookp = window.document.createElement('script');
						this.facebookp.charset = 'utf-8';
						this.facebookp.src = 'https://graph.facebook.com/?id=' + shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + encodeURIComponent('?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzFacebook';
						window.document.getElementsByTagName('head')[0].appendChild(this.facebookp);
						break;
					}
					case 'twitter': {
						this.twitterp = window.document.createElement('script');
						this.twitterp.charset = 'utf-8';
						this.twittwep.src = 'http://urls.api.twitter.com/1/urls/count.json?url=' + shellConf.providers[shellConf.preferredProvider].services + 'opengraph' + this.parent.config.href + encodeURIComponent('?lang=' + lang + '&url=' + encodeURIComponent((!!this.parent.data && !!this.parent.data.metadata && !!this.parent.data.metadata.url ? this.parent.data.metadata.url : window.document.location.href))) + '&callback=Kinky.bunnyMan.widgets.' + this.id + '.onBuzzTwitter';
						window.document.getElementsByTagName('head')[0].appendChild(this.twitterp);
						break;
					}
					case 'orkut': {
						break;
					}
					default: {
						
					}
				}
			}
		}
	};
	
	KShare.getEventShare = function(event) {
		event = event || window.event;
		if (event == null) {
			return null;
		}
		var element = event.target || event.srcElement;
		while ((element != null) && !/^share([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		
		return element;
	};
	
	KShare.getShare = function(domElement) {
		var element = domElement;
		while ((element != null) && !/^share([0-9]+)$/.test(element.id)) {
			element = element.parentNode;
		}
		return element;
	};
	
	KShare.prototype.tagNames = {
		html4 : {
			panel : 'div',
		},
		html5 : {
			panel : 'section',
		}
	};
	
	KSystem.registerAddOn('KShare');
	KSystem.included('KShare');
});

function KShell(parent) {
	if (parent != null) {
		this.isLeaf = false;
		KWidget.call(this, parent);
		
		this.homepageURL = null;
		this.autoLoad = false;
		this.elements = {};
		this.parentPanel = null;
		this.usingLoader = 0;
		this.loader = null;
		this.loaderOnTop = true;
		
		this.overlay = window.document.createElement('div');
		{
			this.overlay.appendChild(window.document.createTextNode(' '));
			this.overlay.className = ' KShellOverlay ' + this.className + 'Overlay ';
			this.setStyle({
				width : KDOM.getBrowserWidth() + 'px',
				height : KDOM.getBrowserHeight() + 'px',
				position : 'fixed',
				top : '0',
				left : '0',
				display : 'none'
			}, this.overlay);
		}
		this.overlayWidgets = 0;
		
		this.addLocationListener(KShell.go);
		this.addWindowResizeListener(KShell.resize);
		
		this.center = {
			loader : {
				width : true,
				height : true
			}
		};
		
		this.background = window.document.createElement('div');
		this.background.className = ' KWidgetPanelBackground ' + this.className + 'Background ';
		this.background.style.position = 'absolute';
		this.background.style.top = '0px';
		this.background.style.left = '0px';
		this.panel.insertBefore(this.background, this.content);
		
		this.loader = window.document.createElement('div');
		{
			this.loader.className = ' KWidgetLoader ';
			
			var img = window.document.createElement('img');
			img.src = Kinky.LOADER_IMAGE;
			this.loader.appendChild(img);
			
			KCSS.setStyle({
				display : 'none'
			}, [
				this.loader
			]);
		}
		this.panel.appendChild(this.loader);
	}
}

KSystem.include([
	'KShellPart',
	'KWidget'
], function() {
	KShell.prototype = new KWidget();
	KShell.prototype.constructor = KShell;
	
	KShell.prototype.focus = function(dispatcher) {
	};
	
	KShell.prototype.onChildLoaded = function(childWidget) {
		if ((this.size() <= this.loadedChildren)) {
			if (this.activated()) {
				var div = childWidget.childDiv(KWidget.ROOT_DIV);
				var rootDiv = this.childDiv(childWidget.shellDiv);
				rootDiv.appendChild(div);
			}
			else {
				this.activate();
			}
		}
	};
	
	KShell.prototype.insertBefore = function(element, before, childDiv) {
		if (this.activated() && element instanceof KShellPart) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			return KWidget.prototype.insertBefore.call(this, element, before, this.container);
			return element;
		}
		else {
			return KWidget.prototype.insertBefore.call(this, element, before, childDiv);
		}
	};
	
	KShell.prototype.insertAfter = function(element, after, childDiv) {
		if (this.activated() && element instanceof KShellPart) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			return KWidget.prototype.insertAfter.call(this, element, after, this.container);
			return element;
		}
		else {
			return KWidget.prototype.insertAfter.call(this, element, after, childDiv);
		}
	};
	
	KShell.prototype.appendChild = function(element, childDiv) {
		if (this.activated()) {
			element.shellDiv = (!!childDiv ? childDiv : 'content');
			KWidget.prototype.appendChild.call(this, element, this.container);
			return element;
		}
		else {
			return KWidget.prototype.appendChild.call(this, element, childDiv);
		}
	};
	
	KShell.prototype.visible = function() {
		this.display = true;
		for (; this.container.childNodes.length != 0;) {
			var div = this.container.childNodes[0];
			this.container.removeChild(div);
			this.content.appendChild(div);
		}
		this.computeScrollbars(this);
	};
	
	KShell.prototype.invisible = function() {
		this.display = false;
		for (; this.content.childNodes.length != 0;) {
			var div = this.content.childNodes[0];
			this.content.removeChild(div);
			this.container.appendChild(div);
		}
	};
	
	KShell.prototype.onLoadChild = function(data, request) {
		var child = KSystem.construct(data, this);
		if (child == null) {
			return;
		}
		this.appendChild(child, (!!data.target ? data.target : null));
	};
	
	KShell.prototype.refresh = function() {
		var config = KSystem.clone(Kinky.getShellConfig(this.shell));
		this.cleanCache();
		this.parent.removeChild(this.panel);
		this.parent = null;
		this.unload();
		Kinky.clearWidgets(this.shell);
		Kinky.loadShell(config);
	};
	
	KShell.prototype.start = function(homeURL) {
		this.shell = this.config.id;
		this.homepageURL = homeURL;
		this.parentPanel = this.parent;
		
		if (!!this.data && !!this.data.links) {
			this.onLoad(this.data);
		}
		else {
			this.load();
		}
		this.parentPanel.innerHTML = '';
		this.parentPanel.appendChild(this.panel);
	};
	
	KShell.prototype.draw = function() {
		if (!!this.data) {
			if (!!this.data && !!this.data['class']) {
				this.addCSSClass(this.data['class']);
			}
			
			if (!!this.data.title) {
				this.setTitle(this.data.title);
			}
			
			if (!!this.data.graphics && !!this.data.graphics.background) {
				this.setBackground(this.data.graphics.background);
			}
		}
		
		if (this.content.nextSibling) {
			this.panel.insertBefore(this.overlay, this.content.nextSibling);
		}
		else {
			this.panel.appendChild(this.overlay);
		}
		
		this.shellparts = {};
		if (!!this.data.children) {
			for ( var link in this.data.children.elements) {
				var jsClass = KSystem.getWRMLClass(this.data.children.elements[link], 'application/vnd.kinky.');
				if (!!jsClass && !KSystem.isAddOn(jsClass)) {
					var child = null;
					try {
						child = KSystem.construct(this.data.children.elements[link], this);
					}
					catch (e) {
						continue;
					}
					if (!(child instanceof KShellPart)) {
						this.appendChild(child, (!!this.data.children.elements[link].target ? this.data.children.elements[link].target : null));
					}
					else {
						this.shellparts[child.hash] = this.data.children.elements[link];
						child.destroy();
						delete child;
					}
				}
			}
		}
		
		for ( var element in this.childWidgets()) {
			var w = this.childWidget(element);
			if (w.silent) {
				this.loadedChildren++;
				this.onChildLoaded(w);
			}
			if (!(w instanceof KShellPart) && !w.activated()) {
				this.childWidget(element).go();
			}
		}
		
		var page = null;
		if (window.document.location.href.indexOf('#') != -1) {
			page = window.document.location.href.split('#')[1];
		}
		if (this.activated()) {
			if (!this.childWidget(page)) {
				this.onHashChange(page);
			}
		}
		else {
			if (this.nChildren == 0) {
				this.activate();
				if (!!page && !this.childWidget(page)) {
					this.onHashChange(page);
					return;
				}
			}
			if (!page) {
				if (!!Kinky.SHELL_CONFIG[this.shell].startPage) {
					KBreadcrumb.dispatchURL({
						hash : Kinky.SHELL_CONFIG[this.shell].startPage
					});
				}
			}
		}
		if (!this.loaderOnTop && this.background) {
			this.background.appendChild(this.loader);
		}
		if (this.center.loader.width) {
			KCSS.setStyle({
				position : 'fixed',
				left : Math.round(KDOM.getBrowserWidth() / 2 - this.loader.offsetWidth / 2) + 'px'
			}, [
				this.loader
			]);
		}
		if (this.center.loader.height) {
			KCSS.setStyle({
				position : 'fixed',
				top : Math.round(KDOM.getBrowserHeight() / 2 - this.loader.offsetHeight / 2) + 'px'
			}, [
				this.loader
			]);
		}
		
	};
	
	KShell.prototype.childShellPart = function(hash) {
		var data = null;
		data = this.shellparts[hash];
		if (!data) {
			for ( var c in this.shellparts) {
				var regex = new RegExp(c + '/(.+)');
				if (regex.test(hash)) {
					if ((!!this.shellparts[c].hash && !(data)) || (!!data && (data.hash.length < c.length))) {
						data = this.shellparts[c];
					}
				}
			}
		}
		return data;
	};
	
	KShell.prototype.showLoading = function() {
		if (this.loader_t === undefined) {
			var self = this;
			this.loader_t = KSystem.addTimer(function() {
				KCSS.setStyle({
					display : 'block',
					opacity : '0'
				}, [
				self.loader
				]);
				KCSS.setStyle({
					opacity : '1',
					left : Math.round(KDOM.getBrowserWidth() / 2 - self.loader.offsetWidth / 2) + 'px',
					top : Math.round(KDOM.getBrowserHeight() / 2 - self.loader.offsetHeight / 2) + 'px'
				}, [
				self.loader
				]);
			}, 350);
		}
		this.usingLoader++;
	};

	KShell.prototype.hideLoading = function() {
		this.usingLoader--;
		if (this.usingLoader <= 0) {
			this.usingLoader = 0;
			if (this.loader_t !== undefined) {
				KSystem.removeTimer(this.loader_t);
				this.loader_t = undefined;
			}
			KCSS.setStyle({
				opacity : '0'
			}, [
				this.loader
			]);
			var self = this;
			KSystem.addTimer(function(){
				KCSS.setStyle({
					display : 'none'
				}, [
				self.loader
				]);
			}, 200);
		}
	};

	KShell.prototype.showOverlay = function() {
		this.overlayWidgets++;
		this.setStyle({
			display : 'block'
		}, this.overlay);
		this.setStyle({
			opacity : '1'
		}, this.overlay);
	};
	
	KShell.prototype.hideOverlay = function(force) {
		this.overlayWidgets--;
		if ((this.overlayWidgets <= 0) || force) {
			this.overlayWidgets = 0;

			KCSS.setStyle({
				opacity : '1'
			}, [
				this.overlay
			]);
			var self = this;
			KSystem.addTimer(function(){
				KCSS.setStyle({
					display : 'none'
				}, [
				self.overlay
				]);
			}, 200);
		}
	};
	
	KShell.prototype.onHashChange = function(hash) {
		var data = this.childShellPart(hash);
		if (!data) {
			KBreadcrumb.dispatchEvent(null, {
				action : '/not-found'
			});
			return;
		}
		
		KBreadcrumb.drilldown = hash.replace(data.hash, '');
		if (KBreadcrumb.drilldown == '') {
			KBreadcrumb.drilldown = null;
		}
		
		var child = this.childWidget(data.hash);
		if (!!child) {
			child.go();
		}
		else {
			this.onLoadChild(data, hash);
		}
	};

	KShell.prototype.popupBlocked = function() {
		alert('Grrrrrrrrrr, a popup blocker is preventing you from continuing your interaction with this page.\nPlease, add this domain has an exception.')
	};

	KShell.go = function(widget, hash) {
		if (widget.activated()) {
			widget.onHashChange(hash);
		}
	};
	
	KShell.resize = function(site) {
		site.setStyle({
			width : KDOM.getBrowserWidth() + 'px',
			height : KDOM.getBrowserHeight() + 'px'
		}, site.overlay);
		
	};
	
	KShell.setWindowTitle = function(text) {
		window.document.title = text;
	};
	
	KShell.current = null;
	KShell.OVERLAY_DIV = 'overlay';
	KShell.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};
	
	KSystem.included('KShell');
});

function KShellPart(parent, hash, service) {
	if (parent != null) {
		this.isLeaf = false;
		KWidget.call(this, parent);
		
		this.height = null;
		this.hash = hash;
		this.really = false;
	}
}

KSystem.include([
	'KWidget'
], function() {
	KShellPart.prototype = new KWidget();
	KShellPart.prototype.constructor = KShellPart;
	
	KShellPart.prototype.go = function() {
		if (this.gone) {
			if (!!KBreadcrumb.drilldown) {
				KBreadcrumb.dispatchEvent(null, {
					query : KBreadcrumb.drilldown
				});
			}
		}
		KWidget.prototype.go.call(this);
	};
	
	KShellPart.prototype.transition_hide = function(tween) {
		if (!!tween) {
			tween.onComplete = function(widget, t) {
				KShell.current.w.transition_show(KShell.current.t);
				widget.invisible();
				widget.destroy();
			};
		}
		else {
			KShell.current.w.transition_show(KShell.current.t);
		}
		KWidget.prototype.transition_hide.call(this, tween);
	};
	
	KShellPart.prototype.transition_show = function(tween) {
		if (!!KShell.current && (KShell.current.w.hash != this.hash)) {
			var last = KShell.current.w;
			delete KShell.current;
			KShell.current = {
				w : this,
				t : tween
			};
			last.unload();
			return;
		}
		
		KShell.current = {
			w : this,
			t : tween
		};
		KWidget.prototype.transition_show.call(this, tween);
	};
	
	KShellPart.prototype.visible = function() {
		if (!!this.parent && !!this.parent.config && !!this.config) {
			KShell.setWindowTitle(this.parent.config.title + ': ' + this.config.title);
			KBreadcrumb.setHistoryTitle(this.config.title);
		}
		
		KBreadcrumb.dispatchEvent(null, {
			action : '/kmenu/mark/item'
		});
		KWidget.prototype.visible.call(this);
		if (!!KBreadcrumb.drilldown) {
			KBreadcrumb.dispatchEvent(null, {
				query : KBreadcrumb.drilldown
			});
		}
	};
	
	KShellPart.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};
	
	KSystem.included('KShellPart');
});
function KSlider(parent, label, id, options) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.options = options || {};

		this.draggable = window.document.createElement('i');
		this.draggable.className = ' ' + this.className + 'SliderDrag KSliderDrag fa fa-circle ';
		this.setStyle({
			cursor : 'move',
			display : 'block',
			position: 'absolute',
			top : '0',
			left : '0'
		}, this.draggable);

		this.legend = window.document.createElement('p');
		this.legend.className = ' ' + this.className + 'SliderLegend KSliderLegend ';

		this.min = 0;
		this.max = (!!this.options.percentage ? 100 : 1);
		if (!!this.options.limits && this.options.limits.length == 2) {
			this.min = this.options.limits[0];
			this.max = this.options.limits[1];
		}

	}
}

KSystem.include( [ 
	'KEffects',
	'KAbstractInput' 
], function() {
	KSlider.prototype = new KAbstractInput;
	KSlider.prototype.constructor = KSlider;
		
	KSlider.prototype.createInput = function() {
		return this.input;
	};

	KSlider.prototype.getValue = function() {
		if (this.options.percentage) {
			return Math.round(parseFloat(this.input.value) * 100) + '%';
		}
		else {
			return this.input.value;
		}
	};
	
	KSlider.prototype.getRawValue = function() {
		return parseFloat(this.input.value);
	};

	KSlider.prototype.setValue = function(value) {
		if (!value) {
			this.input.value = 0;
		}
		else {
			if (this.options.percentage) {
				this.input.value = typeof value == 'string' ? parseInt(value.replace('%', '')) / 100 : value;
			}
			else {
				this.input.value = typeof value == 'string' ? parseFloat(value).toFixed(2) : value.toFixed(2);
			}
		}

		if (this.options.percentage) {
			this.legend.innerHTML = Math.round(parseFloat(this.input.value) * 100) + '%';
		}
		else {
			this.legend.innerHTML = parseFloat(this.input.value);
		}

	};

	KSlider.prototype.visible = function() {
		KAbstractInput.prototype.visible.call(this);
		var self = this;
		this.readyTimer = KSystem.addTimer(function() {
			var width = KDOM.normalizePixelValue(KCSS.getComputedStyle(self.inputContainer).width);
			if (isNaN(width) || width == 0) {
				return;
			}
			KSystem.removeTimer(self.readyTimer);
			var value = self.getRawValue();
			value = (value - self.min) / (self.max - self.min);

			self.draggable.style.left = (Math.round(value * width) - Math.round(self.draggable.offsetWidth / 2) + KSlider.getElementCoords(self.inputContainer).x) + 'px';
		}, 100, true);
	};

	KSlider.prototype.draw = function() {
		KAbstractInput.prototype.draw.call(this); 

		this.inputContainer.appendChild(this.legend);

		this.inputContainer.appendChild(window.document.createElement('hr'));

		this.inputContainer.style.position = 'relative';
		this.inputContainer.appendChild(this.draggable);

		KDOM.addEventListener(this.draggable, 'mousedown', KSlider.startDrag);
	};

	KSlider.startDrag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		KDOM.addEventListener(w.content, 'mouseup', KSlider.stopDrag);
		KDOM.addEventListener(w.content, 'mousemove', KSlider.drag);

		w.width = KDOM.normalizePixelValue(KCSS.getComputedStyle(w.inputContainer).width);
		w.offsetx = KSlider.getElementCoords(w.inputContainer).x;
	};
	
	KSlider.drag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var x = mouseEvent.clientX - w.offsetx;
		if (x < 0 || x > w.width) {
			return;
		}
		w.x = x;
		w.draggable.style.left = (w.x - Math.round(w.draggable.offsetWidth / 2)) + 'px';
		w.setValue(w.min + ((w.x / w.width) * (w.max - w.min)));
	};
	
	KSlider.stopDrag = function(event) {
		var w = KDOM.getEventWidget(event);
		w.offsety = 0;
		w.offsetx = 0;
		KDOM.removeEventListener(w.content, 'mouseup', KSlider.stopDrag);
		KDOM.removeEventListener(w.content, 'mousemove', KSlider.drag);
	};

	KSlider.getElementCoords = function(element) {
		var e = element;
		var ex = 0;
		var ey = 0;
		do {
			ex += e.offsetLeft;
			ey += e.offsetTop;
		} while (e = e.offsetParent);

		return { x : ex, y : ey };
	};

	KSlider.prototype.INPUT_ELEMENT = 'input';
	KSlider.LABEL_CONTAINER = 'label';
	KSlider.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KSlider');
});
function KSocialAuthClient(parent) {
	if (parent != null) {
		KShell.call(this, parent);
	}
}

KSystem.include([
	'KConnectionREST',
	'KShell',
	'Base64'
], function() {
	KSocialAuthClient.prototype = new KShell();
	KSocialAuthClient.prototype.constructor = KSocialAuthClient;
	
	KSocialAuthClient.prototype.load = function() {
		this.draw();
	};
	
	KSocialAuthClient.prototype.onLoad = function() {
		this.draw();
	};
	
	KSocialAuthClient.prototype.draw = function() {
		var params = new Object();
		
		var return_service = '';
		
		var query_string = location.search.substr(1).split("&");
		for ( var i = 0; i != query_string.length; ++i) {
			var index = query_string[i].indexOf('=');
			
			var key = query_string[i].substring(0, index);
			var value = query_string[i].substring(index + 1);
			
			if (key == 'return_service') {
				return_service = unescape(value);
				
				// http://stackoverflow.com/questions/4386691/facebook-error-error-validating-verification-code
				// "... you cannot use many special characters in the redirect_url ..."
				if (return_service.indexOf("/") == -1) {
					return_service = Base64.decode(return_service);
				}
			}
			else {
				params[key] = value;
			}
		}
		
		this.kinky.get(this, 'rest:' + return_service, params, 'onSuccess');
		
		KShell.prototype.draw.call(this);
	};
	
	KSocialAuthClient.prototype.onSuccess = function(data) {
		window.opener.KOAuth.setAccessToken(KOAuth.getAccessToken(), false);
		window.opener.Kinky.bunnyMan.loggedUser = Kinky.bunnyMan.loggedUser;
		
		window.opener.KBreadcrumb.dispatchURL({
			action : '/logged-in'
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onCreated = function(data) {
		window.opener.KBreadcrumb.dispatchURL({
			action : '/associated' + data.redirectURL
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onError = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onBadRequest = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onUnauthorized = function(data) {
		var action = '/unauthorized';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSocialAuthClient.prototype.onMethodNotAllowed = function(data) {
		var action = '/bad-request';
		
		if (data.message.indexOf('@') == 0) {
			action = data.message.substring(1);
		}
		
		window.opener.KBreadcrumb.dispatchURL({
			action : action
		});
		
		window.close();
	};
	
	KSystem.included('KSocialAuthClient');
});
function KStatic(parent, label, id) {
	if (parent != null) {
		KAbstractInput.call(this, parent, label, id);
		this.statici = this.createInput();
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	KStatic.prototype = new KAbstractInput();
	KStatic.prototype.constructor = KStatic;

	KStatic.prototype.setInnerLabel = function(text) {
		this.innerLabel.innerHTML = text;
		this.innerLabel.className = 'InnerLabel';
	};

	KStatic.prototype.setValue = function(value) {
		KAbstractInput.prototype.setValue.call(this, value);
		this.statici.innerHTML = value;
	};

	KStatic.prototype.getValue = function() {
		var ret = KAbstractInput.prototype.getValue.call(this);
		return ret;
	};

	KStatic.prototype.createInput = function() {
		var input = window.document.createElement('div');
		{
			input.className = ' ' + this.className + 'InputElement ';
			input.name = (!!this.inputID ? input.inputID : input.hash);
		}
		return input;
	};

	KStatic.prototype.draw = function() {
		this.inputContainer = window.document.createElement('div');

		var label = this.createLabel();

		this.inputContainer.appendChild(this.statici);

		this.content.appendChild(label);
		this.content.appendChild(this.inputContainer);

		this.errorArea = window.document.createElement('div');
		this.errorArea.className = 'KAbstractInputErrorArea ' + this.className + 'ErrorArea';
		this.content.appendChild(this.errorArea);

		this.activate();
	};

	KStatic.prototype.INPUT_ELEMENT = 'input';
	KStatic.LABEL_CONTAINER = 'label';
	KStatic.INPUT_ERROR_CONTAINER = 'errorArea';

	KSystem.included('KStatic');
});
function KSushibar(parent) {
	if (parent != null) {
		this.hasPaginationBar = true;
		KGallery.call(this, parent);
		this.autoScroll = false;
		this.timeStep = 5000;
		this.perPage = 4;
		this.transitionTimer = null;
		
	}
}

KSystem.include([
	'KGallery'
], function() {
	
	KSushibar.prototype = new KGallery();
	KSushibar.prototype.constructor = KSushibar;
	
	KSushibar.prototype.draw = function() {
		var index = 1;
		for ( var element in this.childWidgets()) {
			var child = this.childWidget(element);
			child.addCSSClass('KSushibarItem' + index++);
			child.go();
		}
		
		if (this.autoScroll && (this.transitionTimer == null)) {
			this.transitionTimer = KSystem.addTimer('KSushibar.nextSlide(\'' + this.id + '\')', this.timeStep, true);
		}
		this.activate();
	};
	
	KSushibar.prototype.stopSlideshow = function() {
		KSystem.removeTimer(this.transitionTimer);
	};
	
	KSushibar.nextSlide = function(id) {
		var widget = null;
		widget = Kinky.bunnyMan.widgets['\'' + id + '\''];
		var parent = widget.getParent('KShellPart');
		if (parent.hash == KBreadcrumb.getHash()) {
			KWidget.dispatchNext(null, widget);
		}
	};
	
	KSushibar.prototype.continueSlideshow = function() {
		if (this.autoScroll) {
			this.transitionTimer = KSystem.addTimer('KSushibar.nextSlide(\'' + this.id + '\')', this.timeStep, true);
		}
	};
	
	KSystem.included('KSushibar');
});
function KTable(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);
		
		this.parent = parent;
		this.editMode = false;
		this.isDrawn = false;
		this.data = null;
		this.children = null;
		this.requestedPage = this.nPage = 0;
		this.nChildren = 0;
		this.perPage = 10;
		this.totalCount = null;
		this.display = false;
		
		this.kinky = Kinky.bunnyMan;
		this.breadcrumb = '';
		
		this.id = this.id || this.kinky.addWidget(this);
		this.hash = null;
		
		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KWidgetPanel ' + this.className + ' ';
		this.panel.style.overflow = this.overflow;
		this.panel.style.position = 'relative';
		// this.panel.style.clear = 'both';
		
		if (!this.isLeaf) {
			this.topMarker = window.document.createElement('div');
			this.topMarker.style.position = 'relative';
			this.panel.appendChild(this.topMarker);
			
			this.background = window.document.createElement('div');
			this.background.className = ' KWidgetPanelBackground ' + this.className + 'Background ';
			this.background.style.position = 'absolute';
			this.background.style.top = '0px';
			this.background.style.left = '0px';
			this.panel.appendChild(this.background);
			
			this.contentContainer = window.document.createElement('table');
			this.contentContainer.className = ' KWidgetPanelContentContainer ' + this.className + 'ContentContainer ';
			this.contentContainer.style.clear = 'both';
			this.contentContainer.style.position = 'relative';
			this.panel.appendChild(this.contentContainer);
			
			this.widgetTitle = window.document.createElement('thead');
			this.widgetTitle.className = ' KWidgetPanelTitle ' + this.className + 'Title ';
			this.widgetTitle.style.position = 'relative';
			this.contentContainer.appendChild(this.widgetTitle);
			if (Kinky.TITLE_CLEAR_BOTH || this.hasClearOnTitle) {
				this.contentContainer.appendChild(KCSS.clearBoth(true));
			}
		}
		
		this.content = window.document.createElement('tbody');
		this.content.className = ' KWidgetPanelContent ' + this.className + 'Content ';
		if (!this.isLeaf) {
			this.contentContainer.appendChild(this.content);
		}
		else {
			this.panel.appendChild(this.content);
		}
		
		if (!this.isLeaf) {
			this.titleBottomMarker = window.document.createElement('div');
			this.titleBottomMarker.style.position = 'absolute';
			this.titleBottomMarker.style.bottom = '0';
			this.titleBottomMarker.style.clear = 'both';
			this.widgetTitle.appendChild(this.titleBottomMarker);
			
			this.bottomMarker = window.document.createElement('div');
			this.bottomMarker.style.position = 'absolute';
			this.bottomMarker.style.bottom = '0';
			this.bottomMarker.style.clear = 'both';
			this.content.appendChild(this.bottomMarker);
		}
		
		this.window = false;
		this.closeButton = null;
		this.minimizeButton = null;
		this.maximizeButton = null;
		this.buttons = 0;
		this.movable = false;
		this.frame = false;
		this.effects = new Object();
		this.waiting = false;
		
		this.error = null;
		this.request = null;
		this.response = null;
		this.needAuthentication = false;
		
		this.fromListener = false;
		this.lastWidth = 0;
		this.lastHeight = 0;
		this.scroll = {};
	}
}

KSystem.include([
	'KWidget',
	'KCell',
	'KRow'
], function() {
	
	KTable.prototype = new KWidget();
	KTable.prototype.constructor = KTable;
	
	KTable.prototype.go = function() {
		this.draw();
	};
	
	KTable.prototype.draw = function() {
		this.panel.style.overflow = 'visible';
		
		for ( var element in this.childWidgets()) {
			this.childWidget(element).go();
		}
		this.activate();
	};
	
	KTable.prototype.appendChild = function(element) {
		if (element instanceof KCell || element instanceof KRow) {
			KWidget.prototype.appendChild.call(this, element);
		}
	};
	
	KTable.prototype.load = function() {
		var params = new Object();
		if (this.data.feServiceArgs) {
			KSystem.merge(params, this.data.feServiceArgs);
		}
		params.contentView = this.data.contentView;
		params.contentID = this.data.contentID;
		params.offset = this.nPage * this.perPage;
		params.limit = this.perPage;
		this.kinky.get(this, this.data.feService, params);
	};
	
	KSystem.included('KTable');
});
function KTextArea(parent, label, id) {
	if (!!parent) {
		KAbstractInput.call(this, parent, label, id);
		this.onEnterSubmit = false;
		
		this.textarea = window.document.createElement('textarea');
		this.textarea.name = this.textarea.id = id;
		this.INPUT_ELEMENT = 'textarea';
	}
}

KSystem.include([
	'KAbstractInput'
], function() {
	
	KTextArea.prototype = new KAbstractInput;
	KTextArea.prototype.constructor = KTextArea;
	
	KTextArea.prototype.createInput = function() {
		if (!!this.height) {
			this.textarea.style.height = this.textarea.style.maxHeight = this.height + 'px';
		}
		return this.textarea;
	};
	
	KTextArea.prototype.getValue = function() {
		return this.textarea.value;
	};
	
	KTextArea.prototype.setValue = function(value) {
		this.textarea.value = '';
		this.textarea.innerHTML = this.textarea.value = value;
		KAbstractInput.prototype.setValue.call(this, value);
	};
	
	KSystem.included('KTextArea');
});
function KText(parent) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.tagName = 'div';
		this.lead = window.document.createElement('div');
		{
			this.addCSSClass('KTextLead', this.lead);
		}
		
		this.body = window.document.createElement('div');
		{
			this.addCSSClass('KTextBody', this.body);
		}
		
	}
}

KSystem.include([
	'KWidget'
], function() {
	KText.prototype = new KWidget;
	KText.prototype.constructor = KText;
	
	KText.prototype.getText = function() {
		return this.body.innerHTML;
	};
	
	KText.prototype.setText = function(text) {
		this.body.innerHTML = text;
	};
	
	KText.prototype.appendText = function(text) {
		this.body.innerHTML += text;
	};
	
	KText.prototype.getLead = function() {
		return this.lead.innerHTML;
	};
	
	KText.prototype.setLead = function(text) {
		this.lead.innerHTML = text;
	};
	
	KText.prototype.appendLead = function(text) {
		this.lead.innerHTML += text;
	};
	
	KText.prototype.draw = function() {
		if (!!this.data && !!this.data.title) {
			this.setTitle(this.data.title);
		}
		if (!!this.data && !!this.data.description) {
			this.setLead(this.data.description);
		}
		if (!!this.data && !!this.data.body) {
			this.setText(this.data.body);
		}
		if (!!this.data && !!this.data && !!this.data['class']) {
			this.addCSSClass(this.data['class']);
		}
		
		if (!this.activated()) {
			this.container.appendChild(this.lead);
			this.container.appendChild(this.body);
		}
		else {
			this.content.appendChild(this.lead);
			this.content.appendChild(this.body);
		}
		this.activate();
	};
	
	KSystem.included('KText');
});

function KTooltip(parent, params) {
	if (!!parent) {
		this.prefix = 'tooltip';
		KAddOn.call(this, parent);
		this.params = params;

		this.parentRootDiv = this.parent.childDiv(KWidget.ROOT_DIV);

		this.panel = window.document.createElement('div');
		this.panel.id = this.id;
		this.panel.className = ' KTooltip ' + this.parent.className + 'Tooltip ';
		this.panel.innerHTML = this.params.text;
		this.panel.style.display = 'none';
		this.panel.style.opacity = 0;

		this.go();
	}
}
KSystem.include([
	'KAddOn'
], function() {
	KTooltip.prototype = new KAddOn();
	KTooltip.prototype.constructor = KTooltip;

	KTooltip.prototype.go = function() {
		var offsetX = (!!this.params.offsetX ? this.params.offsetX : 0);
		var offsetY = (!!this.params.offsetY ? this.params.offsetY : 0);

		var delay = this.params.delay;
		var timer = -1;
		var widget = this;
		KDOM.addEventListener(this.parentRootDiv, 'mouseover', function(event) {
			var targetWidget = KDOM.getEventWidget(event, widget.parent.className);
			if (!!targetWidget && (targetWidget.id == widget.parent.id) && (widget.panel.style.display == 'block')) {
				return;
			}

			if (!!delay) {
				timer = KSystem.addTimer(function() {
					timer = -1;
					KTooltip.show(widget, (KDOM.mouseX(event) + offsetX), (KDOM.mouseY(event) + offsetY));
				}, delay);
				return;
			}
			KTooltip.show(widget, (KDOM.mouseX(event) + offsetX), (KDOM.mouseY(event) + offsetY));
		});
		KDOM.addEventListener(this.parentRootDiv, 'mouseout', function(event) {
			var targetWidget = KDOM.getWidget(KDOM.getEventRelatedTarget(event, true), widget.parent.className);
			if (!!targetWidget && (targetWidget.id == widget.parent.id)) {
				return;
			}

			if (timer != -1) {
				KSystem.removeTimer(timer);
				return;
			}
			widget.transition_hide();
		});
		KDOM.addEventListener(this.parentRootDiv, 'mousemove', function(event) {
			event = KDOM.getEvent(event);
			widget.panel.style.left = (KDOM.mouseX(event) + offsetX) + 'px';
			widget.panel.style.top = (KDOM.mouseY(event) + offsetY) + 'px';
		});

		var func = this.parent.destroy;
		this.parent.destroy = function() {
			widget.panel.style.display = 'none';
			widget.parentRootDiv.appendChild(widget.panel);
			widget.panel.style.position = 'relative';
			func.call(this);
		};

		this.parentRootDiv.appendChild(this.panel);
	};

	KTooltip.show = function(widget, positionX, positionY) {

		widget.panel.style.position = 'absolute';
		window.document.body.appendChild(widget.panel);

		widget.panel.style.left = positionX + 'px';
		widget.panel.style.top = positionY + 'px';

		widget.panel.style.display = 'block';

		widget.transition_show();
	};

	KTooltip.prototype.hide = function() {
		this.panel.style.display = 'none';
		this.parentRootDiv.appendChild(this.panel);
		this.panel.style.position = 'relative';
	};

	KTooltip.prototype.transition_show = function() {
		var defaultEnterTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			}
		};
		KEffects.addEffect(this, (!!this.params.enterTween ? this.params.enterTween : defaultEnterTween));
	};

	KTooltip.prototype.transition_hide = function() {
		var widget = this;
		var defaultExitTween = {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 1000,
			go : {
				alpha : 0
			},
			from : {
				alpha : 1
			},
			onComplete : function() {
				// caso seja subcarregado este metodo NAO ESQUECER
				widget.hide();
			}
		};
		KEffects.addEffect(this, (!!this.params.exitTween ? this.params.exitTween : defaultExitTween));
	};

	KTooltip.make = function(target, params) {
		var tooltip = new KTooltip(target, params);
		return tooltip;
	};

	KSystem.registerAddOn('KTooltip');
	KSystem.included('KTooltip');
});

function KTree(parent) {
	if (parent) {
		KPanel.call(this, parent);
		this.addCSSClass('KTree');
		this.toggleImgClose = null;
		this.toggleImgOpen = null;
	}
}

KSystem.include( [ 'KPanel' ], function() {
	KTree.prototype = new KPanel();
	KTree.prototype.constructor = KTree;

	KTree.prototype.draw = function() {
		KPanel.prototype.draw.call(this);
	};

	KTree.prototype.createPageTree = function(subPages, level) {

		var toReturn = document.createElement('div');
		toReturn.style.display = (level > 1 ? 'none' : 'block');

		if (subPages.length > 0) {

			for ( var indexSubPages in subPages) {
				var elementUl = window.document.createElement('ul');
				var elementLI = window.document.createElement('li');

				if (subPages[indexSubPages].pages.length > 0) {
					var toggleImg = window.document.createElement('img');
					toggleImg.src = this.toggleImgClose;
					toggleImg.addEventListener('click', function(e) {
						KTree.toggleDiv(e);
					}, false);
					elementLI.appendChild(toggleImg);
				}

				var elementParentLink = window.document.createElement('a');
				elementParentLink.href = '#' + subPages[indexSubPages].urlHashText;
				elementParentLink.appendChild(document.createTextNode(subPages[indexSubPages].titleText));
				elementLI.appendChild(elementParentLink);

				elementUl.appendChild(elementLI);

				if (subPages[indexSubPages].pages.length > 0) {
					elementUl.appendChild(this.createPageTree(subPages[indexSubPages].pages, level + 1));
				}

				toReturn.appendChild(elementUl);
			}
		}
		return toReturn;
	};

	KTree.toggleDiv = function(event) {
		var element = KDOM.getEventTarget(event);
		var widget = KDOM.getEventWidget(event);

		if (element.parentNode.parentNode.getElementsByTagName('div')[0].style.display == 'block') {
			element.parentNode.getElementsByTagName('img')[0].src = widget.toggleImgClose;
			element.parentNode.parentNode.getElementsByTagName('div')[0].style.display = 'none';
		} else {
			element.parentNode.getElementsByTagName('img')[0].src = widget.toggleImgOpen;
			element.parentNode.parentNode.getElementsByTagName('div')[0].style.display = 'block';
		}
	};

	KSystem.included('KTree');
});
function KUploadButton(parent, label, id, callback, options) {
	if (parent != null) {
		KWidget.call(this, parent);
		
		this.callback = callback;
		this.label = label;
		this.options = options || { };

		this.addCSSClass('KButton', this.panel);
		this.addCSSClass('KButton', this.content);
		this.addCSSClass('KButton', this.container);

		this.input = window.document.createElement('button');
		this.input.className = ' KButtonInput ';
		this.input.setAttribute('type', 'button');
		KCSS.setStyle({
			top : '0',
			left : '0'
		}, [
			this.input
		]);
		KDOM.addEventListener(this.input, 'click', callback);
		this.inputID = this.input.id = id;

		this.uploader = window.document.createElement('input');
		this.uploader.setAttribute('type', 'file');
		if (this.multiple) {
			this.uploader.setAttribute('multiple', 'multiple');
		}
		this.uploader.id = this.id + '_uploader';
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			opacity : '0'
		}, [
			this.uploader
		]);

		KDOM.addEventListener(this.uploader, 'change', KUploadButton.changed);
		KDOM.addEventListener(this.uploader, 'mouseover', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.addCSSClass('KButtonOver', widget.input);
		});
		KDOM.addEventListener(this.uploader, 'mouseout', function(event) {
			var widget = KDOM.getEventWidget(event);
			widget.removeCSSClass('KButtonOver', widget.input);
		});
		
		var labelDiv = window.document.createElement('div');
		labelDiv.className = ' KButtonInputLabel ';
		labelDiv.appendChild(window.document.createTextNode(label));
		this.input.appendChild(labelDiv);

		this.progresswin = window.document.createElement('div');
		{
			this.progresswin.className = ' KButtonProgress ';
			KCSS.setStyle({
				position: 'fixed',
				zIndex : '9999'
			}, [
			this.progresswin
			]);

			this.progressbar = window.document.createElement('div');
			{
				this.progressbar.className = ' KButtonProgressBar ';
				KCSS.setStyle({
					width : '0'
				}, [
				this.progressbar
				]);
			}
			this.progresswin.appendChild(this.progressbar);

			this.progresstext = window.document.createElement('div');
			{
				this.progresstext.className = ' KButtonProgressText ';
			}
			this.progresswin.appendChild(this.progresstext);

			var label = window.document.createElement('div');
			{
				label.className = ' KButtonProgressLabel ';
				label.innerHTML = gettext('$KUPLOADBUTTON_UPLOADING_TEXT')
			}
			this.progresswin.appendChild(label);
		}
		
		this.INPUT_ELEMENT = 'input';
	}
}

KSystem.include([
	'KWidget'
], function() {
	KUploadButton.prototype = new KWidget();
	KUploadButton.prototype.constructor = KUploadButton;
	KUploadButton.prototype.isLeaf = true;
	
	KUploadButton.prototype.getInputID = function() {
		return this.inputID;
	};
	
	KUploadButton.prototype.enable = function() {
		this.input.disabled = '';
	};
	
	KUploadButton.prototype.disable = function() {
		this.input.disabled = 'disabled';
	};
	
	KUploadButton.prototype.setSize = function(width, height) {
		KCSS.setStyle({
			width : width + 'px',
			height : height + 'px'
		}, [
			this.panel,
			this.content,
			this.input,
			this.uploader
		]);
	};

	KUploadButton.prototype.go = function() {
		this.gone = true;
		if (!this.activated()) {
			this.draw();
		}
		else {
			this.redraw();
		}
	};
	
	KUploadButton.prototype.setCallback = function(callback) {
		KDOM.removeEventListener(this.input, 'click', this.callback);
		KDOM.addEventListener(this.input, 'click', callback);
		this.callback = callback;
	};
	
	KUploadButton.prototype.setText = function(text, html) {
		var labelDiv = this.input.childNodes[0];
		labelDiv.innerHTML = '';
		if (html) {
			labelDiv.innerHTML = text;
		}
		else {
			labelDiv.appendChild(window.document.createTextNode(text));
		}
	};
	
	KUploadButton.prototype.getText = function(html) {
		if (html) {
			return this.input.childNodes[0].innerHTML;
		}
		else {
			return this.input.childNodes[0].textContent;
		}
	};
	
	KUploadButton.prototype.setHelpText = function(text) {
		this.helpText = text;
	};
	
	KUploadButton.prototype.draw = function() {
		this.content.appendChild(this.input);
		this.content.appendChild(this.uploader);
		this.activate();
	};
	
	KUploadButton.prototype.upload = function(data, file, callback) {
		var widget = this;
		var channel = null;
		if (window.XMLHttpRequest) { 
			channel = new window.XMLHttpRequest();
		}
		else if (window.ActiveXObject) { 
			try {
				channel = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					channel = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
				}
			}
		}

		channel.onreadystatechange = function() {
			if (channel.readyState == 4) {
				if (!!channel.upload) {
					window.document.body.removeChild(widget.progresswin);				
				}
				widget.enable();

				if (channel.status == 0) {
					return;
				}

				if (channel.status == 200) {
					var resp = JSON.parse(channel.responseText);
					widget.uploaded_data = resp;
					KDOM.fireEvent(widget.input, 'click');
				}
			}
		};

		var boundary = "----KFILEUpload" + (new Date()).getTime();

		channel.open('POST', (!!this.options.endpoint ? this.options.endpoint : Kinky.FILE_MANAGER_UPLOAD) + '?ref=' + encodeURIComponent(this.fileref) + (!!this.options.aditional_args ? '&' + this.options.aditional_args : ''), true);
		channel.setRequestHeader("Authorization", !!this.options.access_token ? 'OAuth2.0 ' + this.options.access_token : KOAuth.getAuthorization().authentication);
		channel.setRequestHeader("X-Requested-With", "XHR");
		channel.setRequestHeader("Pragma", "no-cache");
		channel.setRequestHeader("Cache-Control", "no-store");
		channel.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

		var content = '--' + boundary + '\r\n';
		content += 'Content-Encoding: Base64\r\n';
		content += 'Content-Disposition: form-data; name="addFile"; filename="' + file.name + '"\r\n';
		content += 'Content-Type: ' + file.type + '\r\n\r\n';
		content += data.split(',')[1] + '\r\n';
		content += '--' + boundary + '--'

		try {
			if (!!channel.upload) {
				widget.progressbar.style.width = '0%';
				widget.progresstext.innerHTML = '0%';
				window.document.body.appendChild(widget.progresswin);
				channel.upload.addEventListener("progress", function(e) {
					KUploadButton.progress(e, widget, true);
				}, false);
			}
			channel.send(content);
		}
		catch (e) {
		}

	};
	
	KUploadButton.progress = function(evt, widget, upload) {

		if (evt.lengthComputable) {
			var percentLoaded = Math.round((evt.loaded / evt.total) * 100);

			if (percentLoaded < 100) {
				widget.progressbar.style.width = percentLoaded + '%';
				widget.progresstext.innerHTML = percentLoaded + '%';
			}
			else if (!upload) {
				window.document.body.removeChild(widget.progresswin);				
			}
		}
	};

	KUploadButton.changed = function(event) {
		var widget = KDOM.getEventWidget(event);
		
		var files = event.target.files; // FileList object
		if (files === undefined) {
			return;
		}

		widget.disable();

		for (var i = 0, f; f = files[i]; i++) {
			widget.progressbar.style.width = '0%';
			widget.progresstext.innerHTML = '0%';
			window.document.body.appendChild(widget.progresswin);
			
			var reader = new FileReader();
			reader.file = f;
			reader.onprogress = function(e) {
				KUploadButton.progress(e, widget);
			};

			reader.onload = (function(theFile) {
				return function(e) {
					widget.upload(e.target.result, e.target.file, KFile.uploaded);
				};
			})(f);

			reader.readAsDataURL(f);
		}
	};

	KUploadButton.BUTTON_ELEMENT = 'input';
	
	KSystem.included('KUploadButton');
});
function KWidget(parent) {
	if ((parent != null) || (parent == -1)) {
		this.className = KSystem.getObjectClass(this);

		this.parent = parent;
		this.parentDiv = null;
		this.isDrawn = false;
		this.data = null;
		this.config = null;
		this.children = new Object();
		this.childrenid = 0;
		this.nChildren = 0;
		this.loadedChildren = 0;
		this.display = false;
		this.gone = false;
		this.shell = (parent != -1 ? this.parent.shell : null);
		this.addons = {};

		this.kinky = Kinky.bunnyMan;

		this.id = (!!this.id ? this.id : this.kinky.addWidget(this));
		this.hash = null;
		this.ref = null;

		if (!this.tagNames) {
			alert(this.className);
		}
		this.panel = window.document.createElement(this.isAside ? 'aside' : this.tagNames[Kinky.HTML_READY][KWidget.ROOT_DIV]);
		{
			this.panel.id = this.id;
			this.panel.className = ' KWidgetPanel ' + this.className + ' ';
			this.panel.style.position = 'relative';
		}

		this.content = window.document.createElement(this.tagNames[Kinky.HTML_READY][KWidget.CONTENT_DIV]);
		{
			this.content.className = ' KWidgetPanelContent ' + this.className + 'Content ';
		}
		this.panel.appendChild(this.content);

		this.container = window.document.createElement('div');
		{
			this.setStyle({
				width : '0px',
				height : '0px',
				overflow : 'hidden',
				display : 'block'
			}, this.container);
			this.container.className = this.content.className;
		}
		this.panel.appendChild(this.container);

		this.waiting = 0;

		this.fromListener = false;
		this.width = 0;
		this.height = 0;
		this.top = 0;
		this.left = 0;
		this.children_retrieved = false;
		this.feed_retrieved = false;
		this.no_feed = false;
		this.no_children = false;

		if (Kinky.bunnyMan.currentState.length != 1) {
			for ( var i in Kinky.bunnyMan.currentState) {
				this.dispatchState(Kinky.bunnyMan.currentState[i]);
			}
		}

	}
}
KSystem.include([
	'KAddOn',
	'KLayout',
	'KScrollBars'
], function() {
	KWidget.prototype.className = null;
	KWidget.prototype.parent = null;
	KWidget.prototype.isDrawn = false;
	KWidget.prototype.data = null;
	KWidget.prototype.children = null;
	KWidget.prototype.kinky = null;
	KWidget.prototype.id = false;
	KWidget.prototype.hash = null;
	KWidget.prototype.isLeaf = true;
	KWidget.prototype.isAside = false;
	KWidget.prototype.silent = false;

	// >>>
	// Widget action handlers
	KWidget.prototype.onLoad = function(data, request) {
		this.data = data;
		KCache.commit(this.ref + '/data', this.data);

		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed(data, request);
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.retrieveFeed = function(config_data, config_request) {
		var retrieved = this.feed_retrieved;
		var elements = KCache.restore(this.ref + '/feed');
		this.feed_retrieved = true;

		if (!elements && !!this.data.feed) {
			this.onFeed(this.data.feed, config_request);
		}
		else if (!elements && !retrieved && !!this.data.template && !!this.data.template.widget && !!this.data.links && !!this.data.links.feed && !!this.data.links.feed.href) {
			if (!KSystem.isIncluded(this.data.template.widget.type)) {
				this.feed_retrieved = false;
				var id = this.id;
				KSystem.include([
					this.data.template.widget.type
				], function() {
					if (!!Kinky.getWidget(id)) {
						Kinky.getWidget(id).retrieveFeed();
					}
				});
				return;
			}
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + this.data.links.feed.href, {}, headers, 'onFeed');
		}
		else if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.retrieveChildren = function(config_data, config_request) {
		var retrieved = this.children_retrieved;
		var elements = KCache.restore(this.ref + '/children');
		this.children_retrieved = true;

		if (!elements && !!this.data.children) {
			this.onChildren(this.data.children, config_request);
		}
		else if (!elements && !this.isLeaf && !retrieved && !!this.data.links && !!this.data.links.children && !!this.data.links.children.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.data.links.children.href, {
				fields : 'elements'
			}), {}, headers, 'onChildren');
		}
		else if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onFeed = function(data, request) {
		if (!!this.data.template && !!this.data.template.widget && !!this.data.template.translationTable) {
			this.data.feed = {};

			var config = {
				hash : this.hash,
				rel : "\/wrml-relations\/widgets\/{widget_id}",
				requestTypes : [
					"application\/json",
					"application\/vnd.kinky." + this.data.template.widget.type
				],
				responseTypes : [
					"application\/json",
					"application\/vnd.kinky." + this.data.template.widget.type
				],
				links : {
					self : {
						href : this.config.href,
						rel : "\/wrml-relations\/widgets\/{widget_id}"
					}
				}
			};

			for ( var l in data.elements) {
				var childConfig = KSystem.clone(config);
				childConfig.hash += '/' + l;
				childConfig.links.self.href += '/' + l;

				childConfig.data = new Object();
				childConfig.data.feeditem = data.elements[l];
				for ( var t in this.data.template.translationTable) {
					var name = this.data.template.translationTable[t].srcField;
					var value = data.elements[l][name];
					childConfig.data[this.data.template.translationTable[t].dstField] = value;
					if (!value && ((name == 'id')) && !!data.elements[l]._id) {
						childConfig.data[this.data.template.translationTable[t].dstField] = data.elements[l]._id;
					}
				}
				this.data.feed[childConfig.hash] = childConfig;
			}
			this.data.feed.size = data.size;
		}
		else {
			this.data.feed = data;
		}
		KCache.commit(this.ref + '/feed', this.data.feed);
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onChildren = function(data, request) {
		var includes = [];
		var size = 0;
		if (!this.data.children) {
			this.data.children = data;
		}
		this.addonsdata = [];

		for ( var link in data.elements) {
			size++;
			var jsClass = KSystem.getWRMLClass(data.elements[link], 'vnd.kinky.');
			if (!!jsClass) {
				if (KSystem.isAddOn(jsClass)) {
					try {
						var child = null;
						try {
							child = KSystem.construct(data.elements[link], this);
							this.appendChild(child);
						}
						catch (e) {
							continue;
						}
						this.addonsdata.push(data.elements[link]);
					}
					catch (e) {
					}
				}
				else {
					includes.push(jsClass);
				}
			}
		}
		this.data.children.size = size;
		KCache.commit(this.ref + '/addons', this.addonsdata);
		KCache.commit(this.ref + '/children', this.data.children);

		if (includes.length != 0) {
			var id = this.id;
			KSystem.include(includes, function() {
				var widget = Kinky.getWidget(id);
				if (!widget.no_feed && !widget.feed_retrieved) {
					widget.retrieveFeed();
				}
				else {
					widget.draw();
				}
			});
			return;
		}

		if (!this.no_feed && !this.feed_retrieved) {
			this.retrieveFeed();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.onSave = function(data, request) {
	};

	KWidget.prototype.onRemove = function(data, request) {
	};

	KWidget.prototype.onPost = function(data, request) {
	};

	KWidget.prototype.onPut = function(data, request) {
	};

	KWidget.prototype.onDestroy = function() {
	};

	KWidget.prototype.onError = function(data, request) {
		this.abort();
	};
	// <<<

	// >>>
	// HTTP response handlers
	KWidget.prototype.onOk = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onHead = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onCreated = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onAccepted = function(data, request) {
		this.onLoad(data, request);
	};

	KWidget.prototype.onNoContent = function(data, request) {
		if (!this.no_children && !this.children_retrieved) {
			this.retrieveChildren(data, request);
			return;
		}
		else {
			this.no_children = !this.data || !this.data.children || ((this.data.children.size == 0));
		}

		if (this.children_retrieved && !this.feed_retrieved && !this.no_feed) {
			this.retrieveFeed(data, request);
			return;
		}
		else {
			this.no_feed = !this.data || !this.data.feed || ((this.data.feed.size == 0));
		}

		this.onLoad(data, request);
	};

	KWidget.prototype.onBadRequest = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onUnauthorized = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onForbidden = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onNotFound = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onMethodNotAllowed = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onPreConditionFailed = function(data, request) {
		this.onError(data, request);
	};

	KWidget.prototype.onInternalError = function(data, request) {
		this.onError(data, request);
	};
	// <<<

	KWidget.prototype.dispatchState = function(stateName) {
		var refresh = false;
		for ( var func in KWidget.STATE_FUNCTIONS) {
			var exists = false;
			eval('exists = !!' + this.className + '.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			refresh |= exists;
			if (exists) {
				eval('this.' + KWidget.STATE_FUNCTIONS[func] + ' = ' + this.className + '.prototype.' + stateName + '_' + KWidget.STATE_FUNCTIONS[func] + ';');
			}
		}
		if (refresh && this.activated()) {
			this.refresh();
		}
	};

	KWidget.prototype.size = function() {
		return this.nChildren;
	};

	KWidget.prototype.insertBefore = function(element, before, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' insertBefore: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).insertBefore(element.panel, before.panel);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.insertAfter = function(element, after, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' insertAfter: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).insertBefore(element.panel, after.panel.nextSibling);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.appendChild = function(element, childDiv) {
		if (!element) {
			return;
		}

		if (!!childDiv && !this.hasChildDiv(childDiv)) {
			throw new KException(this, ' appendChild: first add ' + childDiv);
		}

		if (element instanceof KAddOn) {
			element.parent = this;
			element.go();
			return false;
		}

		var elementKey = element.key = (!!element.hash ? element.hash : element.id);
		if (!!this.children[elementKey]) {
			return;
		}
		this.children[elementKey] = element;

		element.parentDiv = (!!childDiv ? this.childDiv(childDiv) : this.content);
		if (!childDiv) {
			childDiv = (!this.activated() ? 'container' : 'content');
		}

		this.childDiv(childDiv).appendChild(element.panel);
		this.nChildren++;
		if (this.activated() && !element.gone) {
			element.go();
		}

		return element;
	};

	KWidget.prototype.hasChildDiv = function(target) {
		if (typeof target == 'string') {
			return !!this[target];
		}
		else {
			return target;
		}
	};

	KWidget.prototype.childDiv = function(target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!!this[target]) {
					domElement = this[target];
				}
			}
			else {
				domElement = target;
			}
		}

		return domElement;
	};

	KWidget.prototype.childWidget = function(id) {
		return this.children[id];
	};

	KWidget.prototype.childAt = function(index) {
		if (((index >= 0)) && ((index < this.nChildren))) {
			var pos = 0;
			for ( var child in this.childWidgets()) {
				if (pos == index) {
					return this.childWidget(child);
				}
				pos++;
			}
		}
		return null;
	};

	KWidget.prototype.indexOf = function(widget) {
		try {
			var pos = 0;
			for ( var child in this.childWidgets()) {
				if (widget.id == this.childWidget(child).id) {
					return pos;
				}
				pos++;
			}
		}
		catch (e) {
		}
		return -1;
	};

	KWidget.prototype.childWidgets = function() {
		return this.children;
	};

	KWidget.prototype.focus = function(dispatcher) {
		this.parent.focus(this);
	};

	KWidget.prototype.blur = function(dispatcher) {
		this.parent.blur(this);
	};

	KWidget.prototype.unload = function() {
		if (this.display) {
			this.onHide();
			return;
		}
		this.destroy();
	};

	KWidget.prototype.abort = function() {
		var parent = this.parent;
		this.destroy();
		if (parent && (parent instanceof KWidget)) {
			parent.onChildLoaded();
		}
	};

	KWidget.prototype.destroy = function() {
		this.onDestroy();
		var id = this.id;

		this.removeAllChildren();

		if (!!this.parent) {
			this.parent.removeChild(this);
		}

		for ( var att in this.addons) {
			if (this.addons[att]) {
				this.addons[att].destroy();
			}
		}

		for ( var att in this) {
			if (this[att] && (att != 'data') && (att != 'config')) {
				delete this[att];
			}
		}
		Kinky.bunnyMan.removeWidget(id);
	};

	KWidget.prototype.removeChild = function(element) {
		var elementKey = element.hash || element.id;
		if (!!this.children[elementKey]) {
			if (element.panel.parentNode) {
				element.panel.parentNode.removeChild(element.panel);
			}
			element.parent = undefined;
			element.unload();
			delete this.children[elementKey];
			this.nChildren--;
			return true;
		}

		return false;
	};

	KWidget.prototype.removeAllChildren = function() {
		for ( var elementKey in this.childWidgets()) {
			this.childWidget(elementKey).unload();
		}
		delete this.children;
		this.children = new Object();

		this.nChildren = 0;
		this.loadedChildren = 0;
	};

	KWidget.prototype.disable = function() {
		if (!this.w_overlay) {
			this.w_overlay = window.document.createElement('div');
			this.w_overlay.className = this.className + "Overlay KWidgetDisableOverlay";
		}
		var width = Math.max(this.panel.offsetWidth, this.content.offsetWidth);
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			width : (isNaN(width) ? KDOM.getBrowserWidth() : width) + 'px',
			height : Math.max(this.panel.offsetHeight, this.content.offsetHeight) + 'px'
		}, [
		this.w_overlay
		]);
		this.panel.appendChild(this.w_overlay);
		KCSS.setStyle({
			opacity : '1'
		}, [
		this.w_overlay
		]);
	};

	KWidget.prototype.enable = function() {
		if (!!this.w_overlay && !!this.w_overlay.parentNode) {
			KCSS.setStyle({
				opacity : '0'
			}, [
			this.w_overlay
			]);
			var self = this;
			KSystem.addTimer(function(){
				self.panel.removeChild(self.w_overlay);
			}, 200);
		}
	};

	KWidget.prototype.getShell = function() {
		return Kinky.getShell(this.shell);
	};

	KWidget.prototype.getShellConfig = function() {
		return Kinky.getShellConfig(this.shell);
	};

	KWidget.prototype.getConnection = function(type) {
		return KConnectionsPool.getConnection(type, this.shell);
	};

	KWidget.prototype.getHeight = function() {
		if (!this.activated() && !!this.container) {
			return this.container.offsetHeight;
		}
		return this.content.offsetHeight;
	};

	KWidget.prototype.getWidth = function() {
		if (!this.activated() && !!this.container) {
			return this.container.offsetWidth;
		}
		return this.content.offsetWidth;
	};

	KWidget.prototype.getRootHeight = function() {
		return this.panel.offsetHeight;
	};

	KWidget.prototype.getRootWidth = function() {
		return this.panel.offsetWidth;
	};

	KWidget.prototype.getComputedStyle = function(childDiv) {
		var domElement = this.childDiv(childDiv);
		if (!domElement) {
			if (!!this.container) {
				domElement = this.container;
			}
			else {
				domElement = this.content;
			}
		}
		return KCSS.getComputedStyle(domElement);
	};

	KWidget.prototype.scrollTop = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			if ((parent.nodeName.toLowerCase() != 'html') && parent.scrollTop !== undefined) {
				toReturn += parent.scrollTop;
			}
			parent = parent.parentNode;
		}
		if (!!window.document.body.style.overflow && ((window.document.body.style.overflow == 'hidden'))) {
			return toReturn - (!!window.pageYOffset ? window.pageYOffset : 0);
		}
		return toReturn;
	};

	KWidget.prototype.offsetTop = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetTop;
			parent = parent.offsetParent;
		}
		return toReturn;
	};

	KWidget.prototype.offsetLeft = function() {
		var parent = this.panel;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetLeft;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parent.offsetParent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KWidget.prototype.offsetRight = function() {
		var parent = this.panel;
		var parentWidget = this;
		var toReturn = 0;
		while (parent != null) {
			toReturn += parent.offsetRight;
			if (((KBrowserDetect.browser == 2)) && ((KBrowserDetect.version == 7))) {
				parent = parentWidget.panel;
				parentWidget = parentWidget.parent;
			}
			else {
				parent = parent.offsetParent;
			}
		}
		return toReturn;
	};

	KWidget.prototype.setBackground = function(bg) {
		KCSS.setStyle({
			backgroundImage : 'url("' + bg + '")'
		}, [
			this.content
		]);
	};

	KWidget.prototype.setTitle = function(title, isHTML) {
		var div = (this.activated() || !this.container ? this.content : this.container);

		if (div.getElementsByTagName('h2').length == 0) {
			var h2 = window.document.createElement('h2');
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			if (div.childNodes.length > 0) {
				div.insertBefore(h2, div.childNodes[0]);
			}
			else {
				div.appendChild(h2);
			}
			h2.className = ' ' + this.className + 'Title ';
			this.title = h2;
			return h2;
		}
		else {
			var h2 = div.getElementsByTagName('h2')[0];
			h2.innerHTML = '';
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			return h2;
		}
	};

	KWidget.prototype.appendTitleText = function(title, isHTML) {
		var div = (this.activated() || !this.container ? this.content : this.container);

		if (div.getElementsByTagName('h2').length == 0) {
			var h2 = window.document.createElement('h2');
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML = title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			if (div.childNodes.length > 0) {
				div.insertBefore(h2, div.childNodes[0]);
			}
			else {
				div.appendChild(h2);
			}
			h2.className = ' ' + this.className + 'Title ';
			return h2;
		}
		else {
			var h2 = div.getElementsByTagName('h2')[0];
			if (isHTML) {
				if (typeof title == 'string') {
					h2.innerHTML += title;
				}
				else {
					h2.appendChild(title);
				}
			}
			else {
				h2.appendChild(window.document.createTextNode(title));
			}
			return h2;
		}
	};

	KWidget.prototype.addCSSClass = function(cssClass, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addCSSClass: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}
		domElement.className += ' ' + cssClass + ' ';
	};

	KWidget.prototype.removeCSSClass = function(cssClass, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' removeCSSClass: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}
		if (domElement.className) {
			var regex = new RegExp(' ' + cssClass + ' ', 'g');
			domElement.className = domElement.className.replace(regex, '');
		}
	};

	KWidget.prototype.setStyle = function(cssStyle, target) {
		var domElements = null;
		if (target) {
			if (target instanceof Array) {
				domElements = new Array();
				for ( var index in target) {
					if (typeof target[index] == 'string') {
						if (!this[target[index]]) {
							throw new KException(this, ' setStyle: first add ' + target[index]);
						}
						domElements.push(this[target[index]]);
					}
					else {
						domElements.push(target);
					}
				}
			}
			else if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' setStyle: first add ' + target);
				}
				domElements = [
					this[target]
				];
			}
			else {
				domElements = [
					target
				];
			}
		}
		else {
			domElements = [
				this.panel
			];
		}
		KCSS.setStyle(cssStyle, domElements);
	};

	KWidget.prototype.clearBoth = function(target) {
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' clearBoth: first add ' + target);
				}
				target = this[target];
			}
		}
		else {
			target = (this.activated() || !this.container ? this.content : this.container);
		}
		target.appendChild(KCSS.clearBoth());
	};

	KWidget.prototype.addWindowResizeListener = function(callback) {
		this.kinky.addWindowResizeListener(this, callback);
	};

	KWidget.prototype.addResizeListener = function(callback) {
		this.kinky.addResizeListener(this, callback);
	};

	KWidget.prototype.addLocationListener = function(callback) {
		KBreadcrumb.addLocationListener(this, callback);
	};

	KWidget.prototype.addQueryListener = function(callback, queries) {
		KBreadcrumb.addQueryListener(this, callback, queries);
	};

	KWidget.prototype.addActionListener = function(callback, actions) {
		KBreadcrumb.addActionListener(this, callback, actions);
	};

	KWidget.prototype.addEventListener = function(eventName, callback, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addEventListener: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}

		KDOM.addEventListener(domElement, eventName, callback);
	};

	KWidget.prototype.removeEventListener = function(eventName, callback, target) {
		var domElement = null;
		if (target) {
			if (typeof target == 'string') {
				if (!this[target]) {
					throw new KException(this, ' addEventListener: first add ' + target);
				}
				domElement = this[target];
			}
			else {
				domElement = target;
			}
		}
		else {
			domElement = this.panel;
		}

		KDOM.removeEventListener(domElement, eventName, callback);
	};

	KWidget.prototype.getLink = function(params) {
		return KBreadcrumb.getLink(params);
	};

	KWidget.prototype.load = function() {
		if (!!this.config && !!this.config.href) {
			var headers = null;
			this.kinky.get(this, Kinky.SHELL_CONFIG[this.shell].preferredProvider + ':' + KSystem.urlify(this.config.href, {
				embed : 'children'
			}), {}, headers);
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.go = function() {
		if (this.gone) {
			return;
		}
		this.gone = true;
		this.ref = (!!this.config && !!this.config.href ? this.config.href : (!!this.hash ? this.hash : '/' + this.id));

		this.addonsdata = KCache.restore(this.ref + '/addons');
		if (!!this.addonsdata) {
			for ( var link in this.addonsdata) {
				var jsClass = KSystem.getWRMLClass(this.addonsdata[link], 'application/vnd.kinky.');
				if (!!jsClass) {
					var child = null;
					try {
						child = KSystem.construct(this.addonsdata[link], this);
						this.appendChild(child);
					}
					catch (e) {
						continue;
					}
				}
			}
		}

		var storedConfig = KCache.restore(this.ref + '/config');
		if (!storedConfig && !!this.config) {
			KCache.commit(this.ref + '/config', this.config);
		}
		this.config = (!!this.config ? this.config : storedConfig);
		var data = (!!this.data && !!this.data.embedded ? this.data : KCache.restore(this.ref + '/data'));

		if (!!this.config && !!data) {
			this.onLoad(data, {});
		}
		else if (!this.activated()) {
			this.load();
		}
		else {
			this.draw();
		}
	};

	KWidget.prototype.resume = function(noDisplay) {
		if (this.waiting != 0) {
			this.waiting--;
			if (!!this.getShell()) {
				this.getShell().hideLoading();
			}
		}
	};

	KWidget.prototype.wait = function() {
		if (!!this.getShell()) {
			this.getShell().showLoading();
		}
		this.waiting++;
	};

	KWidget.prototype.deactivate = function() {
		this.isDrawn = false;
	};

	KWidget.prototype.activate = function() {
		if (!this.activated()) {
			this.isDrawn = true;
			KBreadcrumb.dispatchEvent(this.id);
		}
		this.isDrawn = true;
		this.resume();
		if (!this.display) {
			this.onShow();
		}
	};

	KWidget.prototype.activated = function() {
		return this.isDrawn;
	};

	KWidget.prototype.clear = function() {
		for (; this.content.childNodes.length != 0;) {
			this.content.removeChild(this.content.childNodes[0]);
		}
	};

	KWidget.prototype.silence = function() {
		this.silent = true;
		this.parent.loadedChildren++;
		this.parent.onChildLoaded(this);
	};

	KWidget.prototype.onChildLoaded = function(childWidget) {
		if (((this.size() <= this.loadedChildren)) && !this.display) {
			this.activate();
		}
	};

	KWidget.prototype.visible = function() {
		if (this.display) {
			return;
		}

		this.display = true;
		if (!this.container) {
			return;
		}

		for (; this.container.childNodes.length != 0;) {
			var div = this.container.childNodes[0];
			this.content.appendChild(div);
		}

		this.panel.removeChild(this.container);
		delete this.container;
		this.computeScrollbars();
	};

	KWidget.prototype.invisible = function() {
		if (!this.display) {
			return;
		}
		this.display = false;
	};

	KWidget.prototype.onShow = function() {
		if (this.parent && (this.parent instanceof KWidget) && !this.silent) {
			this.parent.loadedChildren++;
			this.parent.onChildLoaded(this);
		}

		this.transition_show();
		return true;
	};

	KWidget.prototype.onHide = function() {
		this.transition_hide();
		return false;
	};

	KWidget.prototype.transition_show = function(tween) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.show) {
			tween = this.data.tween.show;
		}
		if (!!tween) {
			KEffects.addEffect((!!tween.target ? tween.target : this), tween);
		}
		else {
			this.visible();
		}
	};

	KWidget.prototype.transition_hide = function(tween) {
		if (!tween && !!this.data && !!this.data.tween && !!this.data.tween.hide) {
			tween = this.data.tween.hide;
		}
		if (!!tween) {
			var toAdd = null;
			if (tween instanceof Array) {
				toAdd = tween[0];
			}
			else {
				toAdd = tween;
			}
			if (!toAdd.onComplete) {
				toAdd.onComplete = function(widget, t) {
					widget.invisible();
					widget.destroy();
				};
			}
			KEffects.addEffect((!!tween.target ? tween.target : this), tween);
		}
		else {
			this.invisible();
			this.unload();
		}
	};


	KWidget.prototype.computeScrollbars = function() {
		var style = this.getComputedStyle(this.panel);
		if (style.overflow == 'auto' || style.overflow == 'scroll' || style.overflowY == 'auto' || style.overflowY == 'scroll' || style.overflowX == 'auto' || style.overflowX == 'scroll') {
			var r = this.panel.className.trim().replace(/([a-zA-Z]+)/g, '\\.$1(.*)\\.((.*)Scroll)').replace(/ /g, '|').replace(/\|\|/g, '|');
			var regex = new RegExp(r);
			var rule = KCSS.getCSS(regex);
			if (!!rule) {
				this.content.style.position = 'relative';
				KScroll.make(this, rule.selectorText);
			}
		}
	};

	KWidget.prototype.scrollIntoView = function() {
		var scrolled = KScroll.getScrollInWidgets(this);
		if (!scrolled) {
			this.panel.scrollIntoView(false);
			return;
		}
		else {
			scrolled.parent.content.offsetScroll = 0;
			scrolled.parent.content.scrollTop = 0;
			if (this.panel.offsetTop - scrolled.position.v > scrolled.viewportH) {
				scrolled.scrollTo({ y : this.panel.offsetTop });
			}
		}
	};

	KWidget.prototype.scroll = function(barpos, scroll) {
		var top = KDOM.normalizePixelValue(this.content.style.top);
		if (isNaN(top)) {
			top = 0;
		}
		var left = KDOM.normalizePixelValue(this.content.style.left);
		if (isNaN(left)) {
			left = 0;
		}

		top = -Math.round((barpos.pv - scroll.topbH) / scroll.proportionV);
		left = -Math.round((barpos.ph - scroll.leftbW) / scroll.proportionH);

		this.content.style.top = top + 'px';
		this.content.style.left = left + 'px';

	};

	KWidget.prototype.draw = function() {
		if (!!this.data) {
			if (!!this.data && !!this.data['class']) {
				this.addCSSClass(this.data['class']);
			}

			if (!!this.data.graphics && !!this.data.graphics.background) {
				this.setBackground(this.data.graphics.background);
			}

			if (!!this.data.feed && !!this.data.template && !!this.data.template.widget && !this.no_feed) {
				for ( var link in this.data.feed) {
					var jsClass = KSystem.getWRMLClass(this.data.feed[link], 'application/vnd.kinky.');
					if (!!jsClass) {
						var child = null;
						try {
							child = KSystem.construct(this.data.feed[link], this);
						}
						catch (e) {
							continue;
						}
						if (!!this.data.feed[link].data) {
							child.data = this.data.feed[link].data;
						}
						this.appendChild(child, (!!this.data.feed[link].target ? this.data.feed[link].target : null));
					}
				}
			}
			if (!!this.data.children && !this.no_children) {
				for ( var link in this.data.children.elements) {
					var jsClass = KSystem.getWRMLClass(this.data.children.elements[link], 'application/vnd.kinky.');
					if (!!jsClass && !KSystem.isAddOn(jsClass)) {
						var child = null;
						try {
							child = KSystem.construct(this.data.children.elements[link], this);
						}
						catch (e) {
							continue;
						}
						if (!!this.data.children.elements[link].embedded) {
							child.data = this.data.children.elements[link];
						}
						this.appendChild(child, (!!this.data.children.elements[link].target ? this.data.children.elements[link].target : null));
					}
				}
			}
		}
		for ( var child in this.childWidgets()) {
			var w = this.childWidget(child);
			if (w.silent) {
				this.loadedChildren++;
				this.onChildLoaded(w);
			}
			if (!w.gone) {
				w.go();
			}
		}
		if (this.nChildren == 0) {
			this.activate();
		}
	};

	KWidget.prototype.cleanCache = function() {
		KCache.find(new RegExp(this.ref + '/children(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/feed(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/data(.*)'), KCache.clear);
		KCache.find(new RegExp(this.ref + '/config(.*)'), KCache.clear);
		KCache.clear(this.ref + '/children');
		KCache.clear(this.ref + '/feed');
		KCache.clear(this.ref + '/data');
		KCache.clear(this.ref + '/config');
		delete this.data;

		delete this.config;
		for ( var w in this.children) {
			this.children[w].cleanCache();
		}
	};

	KWidget.prototype.redraw = function() {
		this.clear();
		for ( var element in this.childWidgets()) {
			this.content.appendChild(this.childWidget(element).panel);
		}
		this.draw();
	};

	KWidget.prototype.refresh = function() {
		/*var config = this.config;
		var parent = this.parent;
		var sibling = this.parent.childAt(this.parent.indexOf(this) + 1);

		this.cleanCache();
		this.unload();
		return KWidget.cloneWidget(config, parent, sibling);*/
		throw new Error('kinky warning: widget "' + this.className + '" must implement the refresh() function.');
	};

	KWidget.prototype.getParent = function(parentClass) {
		if (parentClass) {
			if (this.parent == null) {
				return null;
			}
			else if (this.parent.className) {
				var isParent = false;
				eval('isParent = this.parent instanceof ' + parentClass + ';');
				if (isParent) {
					return this.parent;
				}
				else {
					return this.parent.getParent(parentClass);
				}
			}
			else {
				return undefined;
			}
		}
		else {
			return this.parent;
		}
	};

	KWidget.prototype.getURL = function() {
		return KBreadcrumb.getURL();
	};

	KWidget.prototype.getHash = function() {
		return KBreadcrumb.getHash();
	};

	KWidget.prototype.getQuery = function() {
		return KBreadcrumb.getQuery();
	};

	KWidget.prototype.getAction = function() {
		return KBreadcrumb.getAction();
	};

	KWidget.prototype.retain = function(args) {
		this._original = args;
	};

	KWidget.hashToRegex = function(hash) {
		var regex = hash.split(/\{([^\}]+)\}/);
		var result = '';
		for ( var part in regex) {
			if (part % 2 == 0) {
				result += regex[part];
			}
			else {
				result += '([^/]+)';
			}
		}
		return result;
	};


	KWidget.prototype.clone = function() {
		if (!this._original) {
			throw new Error('kinky warning: widget must invoke "this.retain(arguments)" method in the constructor, in order to use the "this.clone()" method.');
		}

		var child = null;
		var toeval = 'child = new ' + this.className + '(this.parent';
		for (var i in this._original) {
			toeval += ', this._original[' + i + ']';
		}
		toeval += ')';

		/*
		var sibling = this.parent.childAt(this.parent.indexOf(this) + 1);
		if (!sibling) {
			parent.appendChild(child, (!!config.target ? config.target : null));
		}
		else {
			parent.insertBefore(child, sibling, (!!config.target ? config.target : null));
		}
		child.go();*/
		eval(toeval);
		return child;
	};

	KWidget.replace = function(self, other) {
		var sibling = self.parent.childAt(self.parent.indexOf(self) + 1);
		if (!sibling) {
			self.parent.appendChild(other, (!!other.config && !!other.config.target ? other.config.target : null));
		}
		else {
			self.parent.insertBefore(other, sibling, (!!other.config && !!config.target ? other.config.target : null));
		}
		self.destroy();
		other.go();
	}

	KWidget.ROOT_DIV = 'panel';
	KWidget.CONTENT_DIV = 'content';

	KWidget.STATE_FUNCTIONS = [
		"go",
		"load",
		"draw",
		"focus",
		"blur",
		"enable",
		"disable",
		"visible",
		"invisible",
		"abort",
		"onLoad",
		"onSave",
		"onRemove",
		"onPost",
		"onPut",
		"onError",
		"onOk",
		"onHead",
		"onCreated",
		"onAccepted",
		"onNoContent",
		"onBadRequest",
		"onUnauthorized",
		"onForbidden",
		"onNotFound",
		"onMethodNotAllowed",
		"onPreConditionFailed",
		"onInternalError"
	];

	KWidget.prototype.tagNames = {
		html4 : {
			panel : 'div',
			content : 'div',
			background : 'div'
		},
		html5 : {
			panel : 'section',
			content : 'div',
			background : 'div'
		}
	};

	KSystem.included('KWidget');
});
KWidgetNotFoundException.prototype = new KException();
KWidgetNotFoundException.prototype.constructor = KWidgetNotFoundException;

function KWidgetNotFoundException(invoker, message) {
	KException.call(invoker, message);
}

var MD5 = function(string) {
	
	function RotateLeft(lValue, iShiftBits) {
		return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
	}
	
	function AddUnsigned(lX, lY) {
		var lX4, lY4, lX8, lY8, lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			}
			else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		}
		else {
			return (lResult ^ lX8 ^ lY8);
		}
	}
	
	function F(x1, y, z) {
		return (x1 & y) | ((~x1) & z);
	}
	function G(x1, y, z) {
		return (x1 & z) | (y & (~z));
	}
	function H(x1, y, z) {
		return (x1 ^ y ^ z);
	}
	function I(x1, y, z) {
		return (y ^ (x1 | (~z)));
	}
	
	function FF(a1, b1, c1, d1, x1, s, ac) {
		a1 = AddUnsigned(a1, AddUnsigned(AddUnsigned(F(b1, c1, d1), x1), ac));
		return AddUnsigned(RotateLeft(a1, s), b1);
	}
	
	function GG(a1, b1, c1, d1, x1, s, ac) {
		a1 = AddUnsigned(a1, AddUnsigned(AddUnsigned(G(b1, c1, d1), x1), ac));
		return AddUnsigned(RotateLeft(a1, s), b1);
	}
	
	function HH(a1, b1, c1, d1, x1, s, ac) {
		a1 = AddUnsigned(a1, AddUnsigned(AddUnsigned(H(b1, c1, d1), x1), ac));
		return AddUnsigned(RotateLeft(a1, s), b1);
	}
	
	function II(a1, b1, c1, d1, x1, s, ac) {
		a1 = AddUnsigned(a1, AddUnsigned(AddUnsigned(I(b1, c1, d1), x1), ac));
		return AddUnsigned(RotateLeft(a1, s), b1);
	}
	
	function ConvertToWordArray(string1) {
		var lWordCount;
		var lMessageLength = string1.length;
		var lNumberOfWords_temp1 = lMessageLength + 8;
		var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
		var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
		var lWordArray = Array(lNumberOfWords - 1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while (lByteCount < lMessageLength) {
			lWordCount = (lByteCount - (lByteCount % 4)) / 4;
			lBytePosition = (lByteCount % 4) * 8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string1.charCodeAt(lByteCount) << lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount - (lByteCount % 4)) / 4;
		lBytePosition = (lByteCount % 4) * 8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
		lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
		lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
		return lWordArray;
	}
	
	function WordToHex(lValue) {
		var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
		for (lCount = 0; lCount <= 3; lCount++) {
			lByte = (lValue >>> (lCount * 8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
		}
		return WordToHexValue;
	}
	
	function Utf8Encode(string1) {
		string1 = string1.replace(/\r\n/g, "\n");
		var utftext = "";
		
		for ( var n = 0; n < string1.length; n++) {
			
			var c = string1.charCodeAt(n);
			
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			
		}
		
		return utftext;
	}
	
	var x = Array();
	var k, AA, BB, CC, DD, a, b, c, d;
	var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
	var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
	var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
	var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
	
	string = Utf8Encode(string);
	
	x = ConvertToWordArray(string);
	
	a = 0x67452301;
	b = 0xEFCDAB89;
	c = 0x98BADCFE;
	d = 0x10325476;
	
	for (k = 0; k < x.length; k += 16) {
		AA = a;
		BB = b;
		CC = c;
		DD = d;
		a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
		d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
		c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
		b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
		a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
		d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
		c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
		b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
		a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
		d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
		c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
		b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
		a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
		d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
		c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
		b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
		a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
		d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
		c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
		b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
		a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
		d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
		c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
		b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
		a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
		d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
		c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
		b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
		a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
		d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
		c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
		b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
		a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
		d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
		c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
		b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
		a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
		d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
		c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
		b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
		a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
		d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
		c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
		b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
		a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
		d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
		c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
		b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
		a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
		d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
		c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
		b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
		a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
		d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
		c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
		b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
		a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
		d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
		c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
		b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
		a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
		d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
		c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
		b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
		a = AddUnsigned(a, AA);
		b = AddUnsigned(b, BB);
		c = AddUnsigned(c, CC);
		d = AddUnsigned(d, DD);
	}
	
	var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
	
	return temp.toLowerCase();
};
KSystem.included('MD5');
function Quicksort() {
}

{
	Quicksort.prototype = {};
	Quicksort.prototype.constructor = Quicksort;
	
	Quicksort.quicksort = function(list, compareCallback) {
		if (list.length <= 1) {
			return list;
		}
		var middle = Math.floor(list.length / 2);
		var pivot = list.splice(middle, 1);
		
		var less = [];
		var greater = [];
		
		for ( var index in list) {
			if (compareCallback(list[index], pivot[0]) <= 0) {
				less.push(list[index]);
			}
			else {
				greater.push(list[index]);
			}
		}
		
		return [].concat(Quicksort.quicksort(less, compareCallback), pivot, Quicksort.quicksort(greater, compareCallback));
	};
	
	Quicksort.quicksortKeys = function(object, compareCallback) {
		var realList = [];
		for ( var index in object) {
			realList.push(index);
		}
		
		if (realList.length <= 1) {
			return realList;
		}
		
		return Quicksort.quicksort(realList, compareCallback);
	};
	
	KSystem.included('Quicksort');
}
/**
 * 
 * Secure Hash Algorithm (SHA1) http://www.webtoolkit.info/
 * 
 */

function SHA1(msg) {
	
	function rotate_left(n, s) {
		var t4 = (n << s) | (n >>> (32 - s));
		return t4;
	}
	
	function lsb_hex(val) {
		var str = "";
		var i;
		var vh;
		var vl;
		
		for (i = 0; i <= 6; i += 2) {
			vh = (val >>> (i * 4 + 4)) & 0x0f;
			vl = (val >>> (i * 4)) & 0x0f;
			str += vh.toString(16) + vl.toString(16);
		}
		return str;
	}
	
	function cvt_hex(val) {
		var str = "";
		var i;
		var v;
		
		for (i = 7; i >= 0; i--) {
			v = (val >>> (i * 4)) & 0x0f;
			str += v.toString(16);
		}
		return str;
	}
	
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";
		
		for ( var n = 0; n < string.length; n++) {
			
			var c = string.charCodeAt(n);
			
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			
		}
		
		return utftext;
	}
	
	var blockstart;
	var i, j;
	var W = new Array(80);
	var H0 = 0x67452301;
	var H1 = 0xEFCDAB89;
	var H2 = 0x98BADCFE;
	var H3 = 0x10325476;
	var H4 = 0xC3D2E1F0;
	var A, B, C, D, E;
	var temp;
	
	msg = Utf8Encode(msg);
	
	var msg_len = msg.length;
	
	var word_array = new Array();
	for (i = 0; i < msg_len - 3; i += 4) {
		j = msg.charCodeAt(i) << 24 | msg.charCodeAt(i + 1) << 16 | msg.charCodeAt(i + 2) << 8 | msg.charCodeAt(i + 3);
		word_array.push(j);
	}
	
	switch (msg_len % 4) {
		case 0:
			i = 0x080000000;
			break;
		case 1:
			i = msg.charCodeAt(msg_len - 1) << 24 | 0x0800000;
			break;
		
		case 2:
			i = msg.charCodeAt(msg_len - 2) << 24 | msg.charCodeAt(msg_len - 1) << 16 | 0x08000;
			break;
		
		case 3:
			i = msg.charCodeAt(msg_len - 3) << 24 | msg.charCodeAt(msg_len - 2) << 16 | msg.charCodeAt(msg_len - 1) << 8 | 0x80;
			break;
	}
	
	word_array.push(i);
	
	while ((word_array.length % 16) != 14) {
		word_array.push(0);
	}
	
	word_array.push(msg_len >>> 29);
	word_array.push((msg_len << 3) & 0x0ffffffff);
	
	for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
		
		for (i = 0; i < 16; i++) {
			W[i] = word_array[blockstart + i];
		}
		for (i = 16; i <= 79; i++) {
			W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
		}
		
		A = H0;
		B = H1;
		C = H2;
		D = H3;
		E = H4;
		
		for (i = 0; i <= 19; i++) {
			temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}
		
		for (i = 20; i <= 39; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}
		
		for (i = 40; i <= 59; i++) {
			temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}
		
		for (i = 60; i <= 79; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}
		
		H0 = (H0 + A) & 0x0ffffffff;
		H1 = (H1 + B) & 0x0ffffffff;
		H2 = (H2 + C) & 0x0ffffffff;
		H3 = (H3 + D) & 0x0ffffffff;
		H4 = (H4 + E) & 0x0ffffffff;
		
	}
	
	var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
	
	return temp.toLowerCase();
	
}

KSystem.included('SHA1');
var UTF8 = {
	
	// public method for url encoding
	encode : function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";
		
		for ( var n = 0; n < string.length; n++) {
			
			var c = string.charCodeAt(n);
			
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			
		}
		
		return utftext;
	},
	
	// public method for url decoding
	decode : function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
		
		while (i < utftext.length) {
			
			c = utftext.charCodeAt(i);
			
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
			
		}
		
		return string;
	}

};

KSystem.included('UTF8');
function CODAArrayElementForm(parent, list, data){
	if (parent != null) {
		CODAForm.call(this, parent, list.parent.data.href, null, null);
		this.nillable = true;
		this.list = list;
		this.option = data;
		this.single = true;
	}
}

KSystem.include([
	'CODAForm',
	'KInput'
], function() {
	CODAArrayElementForm.prototype = new CODAForm();
	CODAArrayElementForm.prototype.constructor = CODAArrayElementForm;

	CODAArrayElementForm.prototype.load = function(){
		this.draw();
	};

	CODAArrayElementForm.prototype.addElementInputs = function(panel) {
		for (var o in this.option) {
			var code = null;
			eval('code = new ' + CODAArrayElementForm.getInputType(o) + '(panel, gettext(\'$\' + this.list.parent.className.toUpperCase() + \'_ARRAY_ELEMENT_\' + o.toUpperCase()) + \'*\', o)');
			{
				if (!!this.option && !!this.option[o]){
					code.setValue(this.option[o]);
				}
				code.setHelp(gettext('$' + this.list.parent.className.toUpperCase() + '_ARRAY_ELEMENT_' + o.toUpperCase() + '_HELP'), CODAForm.getHelpOptions());
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				var addon = CODAArrayElementForm.getAddOnType(o);
				if (!!addon) {
					var addonwidget = null;
					eval('addonwidget = new ' + addon + '(code, \'*/*\')');
					code.appendChild(addonwidget);						
				}
			}
			panel.appendChild(code);
		}
	};

	CODAArrayElementForm.addElementInputsWizard = function(panel) {
		for (var o in this.option) {
			if (this.option[o] !== undefined) {
				var code = new KHidden(panel, o);
				{
					code.setValue(this.option[o]);
					code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
				panel.appendChild(code);
			}
			else {
				var code = null;
				eval('code = new ' + CODAArrayElementForm.getInputType(o) + '(panel, gettext(\'$\' + this.parentClassName.toUpperCase() + \'_ARRAY_ELEMENT_\' + o.toUpperCase()) + \'*\', o)');
				{
					code.setHelp(gettext('$' + this.parentClassName.toUpperCase() + '_ARRAY_ELEMENT_' + o.toUpperCase() + '_HELP'), CODAForm.getHelpOptions());
					code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
					var addon = CODAArrayElementForm.getAddOnType(o);
					if (!!addon) {
						var addonwidget = null;
						eval('addonwidget = new ' + addon + '(code, \'*/*\')');
						code.appendChild(addonwidget);						
					}
				}
				panel.appendChild(code);
			}
		}
	};

	CODAArrayElementForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAArrayElementForm.prototype.addMinimizedPanel = function() {
		this.setTitle(gettext('$' + this.list.parent.className.toUpperCase() + '_EDIT_ARRAY_ELEMENT_PROPERTIES'));

		var firstPanel = new KPanel(this);
		{
			firstPanel.hash = '/general';
			this.addElementInputs(firstPanel);
		}

		this.addPanel(firstPanel, gettext('$FORM_GENERALINFO_PANEL'), true);
	};

	CODAArrayElementForm.prototype.save = function(){
		this.instantiateParams(this.option);
		CODAArrayList.OPENED.list.refreshPages();
		CODAArrayList.OPENED.parent.save(CODAArrayList.OPENED.parent.data);
		this.enable();
	};

	CODAArrayElementForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$' + parent.parentClassName.toUpperCase() + '_ADD_ARRAY_ELEMENT_PANEL_TITLE');
			firstPanel.description = gettext('$' + parent.parentClassName.toUpperCase() + '_ADD_ARRAY_ELEMENT_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-bars';

			parent.addElementInputs(firstPanel);

		}
		return [firstPanel];
	};

	CODAArrayElementForm.getInputType = function(name) {
		var type = CODAArrayElementForm.types[name];
		if (!type) {
			return 'KInput';
		}
		return type.split('+')[0];
	};

	CODAArrayElementForm.getAddOnType = function(name) {
		var type = CODAArrayElementForm.types[name];
		if (!type) {
			return undefined;
		}
		var splited = type.split('+');
		if (splited.length > 1) {
			return splited[1];
		}
		return undefined;
	};

	CODAArrayElementForm.types = {
		'description' : 'KTextArea',
		'url' : 'KInput+CODAMediaList',
		'icon' : 'KImageUpload',
		'image' : 'KImageUpload'
	};

	KSystem.included('CODAArrayElementForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAArrayList(parent, elements, fieldID, pattern, options) {
	if(parent != null){
		CODAList.call(this, parent, {}, '', options);
		this.pageSize = 0;
		this.elements = elements;
		this.fieldID = fieldID;
		this.pattern = pattern;
		this.table = {};

		for (var o in this.pattern) {
			if (this.pattern[o] !== undefined) {
				continue;
			}
			this.table[o] = {
				label : gettext('$' + parent.className.toUpperCase() + '_ARRAY_ELEMENT_' + o.toUpperCase())
			};
		}

		this.addLocationListener(CODAArrayList.actions);
	}
}

KSystem.include([
	'CODAList',
	'CODAArrayElementForm'
], function(){
	CODAArrayList.prototype = new CODAList();
	CODAArrayList.prototype.constructor = CODAArrayList;

	CODAArrayList.prototype.load = function() {
		this.draw();
	};

	CODAArrayList.prototype.addItems = function(list) {
		for(var index in this.elements) {
			var listItem = new CODAListItem(list);
			listItem.data = this.elements[index];
			listItem.onDrop = this.onDropOption;
			listItem.remove = this.removeOption;
			listItem.showDetails = this.showOption;
			listItem.move = this.moveOption;
			list.appendChild(listItem);
		}
	};

	CODAArrayList.prototype.editElement = function(item) {
		var configForm = new CODAArrayElementForm(this.getShell(), this, item.data);
		configForm.hash = this.hash + '/edit-element';
		if (!!this.getParent('CODAFormIconDashboard')) {
			this.getShell().makeMiddleDialog(configForm, {minimized : false, modal : true});
		}
		else if (!!this.getParent('CODAForm')) {
			this.getShell().makeDialog(configForm, {minimized : true, modal : false});		
		}
		else {
			this.getShell().makeMiddleDialog(configForm, {minimized : false, modal : true});
		}
	};

	CODAArrayList.prototype.addElement = function() {
		var screens = [ 'CODAArrayElementForm'];
		var wiz = new CODAWizard(this.getShell(), screens);
		wiz.setTitle(gettext('$' + this.parent.className.toUpperCase() + '_ARRAY_ELEMENT_WIZARD'));
		wiz.hash = this.hash + '/add-wizard';
		wiz.option = this.pattern;
		wiz.parentClassName = this.parent.className;

		wiz.addElementInputs = CODAArrayElementForm.addElementInputsWizard;
		wiz.send = function(params) {
			if (!CODAArrayList.OPENED.elements) {
				CODAArrayList.OPENED.elements = [];
			}
			CODAArrayList.OPENED.elements.push(params);
			CODAArrayList.OPENED.list.refreshPages();
			CODAArrayList.OPENED.parent.save();
			this.parent.closeMe();
		};

		this.getShell().makeWizard(wiz);
	};

	CODAArrayList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : this.hash + '/add-element'
		});
	};

	CODAArrayList.prototype.getRequest = function() {
		return this.elements;
	};

	CODAArrayList.prototype.onDropOption = function(w) {
		var self = this.getParent('CODAArrayList');
		w.move(self.elements.indexOf(this.data));
	};

	CODAArrayList.prototype.moveOption = function(order) {
		var self = this.getParent('CODAArrayList');
		var d = this.data;
		var index = self.elements.indexOf(d);

		if (order > index) {
			for (var k = index; k != order; k++) {
				self.elements[k] = self.elements[k + 1];
			}
			self.elements[order] = d;
		}
		else{
			for (var k = index; k != order; k--) {
				self.elements[k] = self.elements[k - 1];
			}
			self.elements[order] = d;
		}
		self.parent.save();
		self.list.refreshPages();
	};

	CODAArrayList.prototype.showOption = function() {
		if (!this.selected) {
			CODAListItem.selectElement(this);
		}
		var list = this.getParent('CODAArrayList');
		list.editing = this;
		KBreadcrumb.dispatchURL({
			hash : list.hash + '/edit-element'
		});
	};

	CODAArrayList.prototype.removeOption = function() {
		var self = this.getParent('CODAArrayList');
		self.elements.splice(self.elements.indexOf(this.data), 1);
		self.parent.save();
		self.list.refreshPages();
	};

	CODAArrayList.prototype.draw = function() {
		CODAList.prototype.draw.call(this);
	};

	CODAArrayList.prototype.invisible = function() {
		if (CODAArrayList.OPENED.id == this.id) {
			CODAArrayList.OPENED = undefined;
		}
		CODAList.prototype.invisible.call(this);
	};

	CODAArrayList.prototype.visible = function() {
		var className = this.className;
		this.className = this.parent.className + this.hash.toUpperCase().replace(/\//g, '_') + 'ArrayList';
		CODAList.prototype.visible.call(this);
		this.className = className;
		CODAArrayList.OPENED = this;
	};
	
	CODAArrayList.prototype.onVisibleTab = function() {
		CODAArrayList.OPENED = this;
	};

	CODAArrayList.actions = function(widget, hash) {
		var parts = hash.split('/');
		if ('/' + parts[1] + '/' + parts[2] != widget.hash) {
			return;
		}

		switch (parts[3]) {
			case 'add-element': {
				widget.addElement();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-element': {
				widget.editElement(widget.editing);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	CODAArrayList.OPENED = undefined;

	KSystem.included('CODAArrayList');
}, Kinky.CODA_INCLUDER_URL);
function CODAAuthShell(parent) {
	if (!!parent) {
		KShell.call(this, parent);
		this.userInfo = null;
	}
}

KSystem.include([
	'KShell',
	'KPanel',
	'KForm',
	'KInput',
	'KPassword',
	'KText',
	'KLink',
	'KButton',
	'KImage',
	'KList',
	'KConnectionREST',
// 'CODATooltip'
], function() {
	
	CODAAuthShell.prototype = new KShell();
	CODAAuthShell.prototype.construtor = CODAAuthShell;
	
	CODAAuthShell.prototype.load = function() {
		Kinky.bunnyMan.get(this, 'rest:/admins/me', {}, [], 'onUserData');
	};
	
	CODAAuthShell.prototype.onUserData = function(response_data) {
		this.userInfo = response_data;
		this.draw();
	};
	
	CODAAuthShell.prototype.draw = function() {
		var client_id = this.data.client_id;
		
		var mainPanel = new KPanel(this);
		mainPanel.addCSSClass('MainPanel');
		this.appendChild(mainPanel);
		
		{
			
			var logotipo = new KImage(mainPanel, '../images/auth/logtipo.png');
			logotipo.addCSSClass('Logotipo');
			mainPanel.appendChild(logotipo);
			
			var optionPanel = new KPanel(mainPanel);
			optionPanel.addCSSClass('OptionPanel');
			mainPanel.appendChild(optionPanel);
			
			{
				var userContainer = new KText(optionPanel);
				userContainer.addCSSClass('UserContainer');
				userContainer.setText("Bem-vindo, " + this.userInfo.name);
				optionPanel.appendChild(userContainer);
				
				var helpContainer = new KPanel(optionPanel);
				helpContainer.addCSSClass('HelpContainer');
				optionPanel.appendChild(helpContainer);
				{
					var titleContainer = new KText(helpContainer);
					titleContainer.addCSSClass('TitleContainer');
					titleContainer.setText('Escolha o projecto');
					helpContainer.appendChild(titleContainer);
					
					var questionIcon = new KImage(helpContainer, '../images/auth/question_icon.png');
					questionIcon.addCSSClass('QuestionIcon');
					helpContainer.appendChild(questionIcon);
					questionIcon.addEventListener('mouseover', CODAAuthShell.showTooltip);
				}
				
				var projectList = new KList(optionPanel);
				projectList.hash = '/project-list';
				projectList.addCSSClass('ProjectList');
				optionPanel.appendChild(projectList);
				projectList.selectedProject = null;
				
				projectList.load = function() {
					Kinky.bunnyMan.get(projectList, 'rest:/clients/' + client_id + '/projects', {}, []);
				};
				
				projectList.onLoad = function(data) {
					for ( var index in data.elements) {
						var projectInfo = data.elements[index];
						
						var projectItem = new KText(projectList);
						projectItem.addCSSClass('ProjectItem');
						projectItem.setText(projectInfo.name);
						projectItem.hash = projectInfo.id;
						projectItem.urlrest = projectInfo.urlrest;
						projectList.appendChild(projectItem);
						
						projectItem.addEventListener('click', CODAAuthShell.toggleSelectedProject);
					}
					
					this.draw();
				};
				
				var editButton = new KButton(optionPanel, 'editar', 'edit', CODAAuthShell.editProject);
				optionPanel.appendChild(editButton);
			}
		}
		
		KShell.prototype.draw.call(this);
	};
	
	CODAAuthShell.showTooltip = function(event) {
		/*
		 * var mouseEvent = KDOM.getEvent(event); var widget = KDOM.getEventWidget(event);
		 * 
		 * CODATooltip.make(widget, { text : 'textofghdjtytooltiplpkjrnfvirnvurnfurnvurhkdsjgaoiprewcvm,esauinverretuhklvzreiuolrdjfhbrvkrezlwahkzhnypothmb<a�rjb.timrjo', isHTML : true, offsetX : 0, offsetY : -25, left : KDOM.mouseX(mouseEvent) + 15, top : (KDOM.mouseY(mouseEvent) + 15) });
		 */
	};
	
	CODAAuthShell.toggleSelectedProject = function(event) {
		var projectWidget = KDOM.getEventWidget(event);
		var list = projectWidget.parent;
		
		if (list.selectedProject != null) {
			if (list.selectedProject == projectWidget.hash) {
				projectWidget.removeCSSClass('Selected');
				list.selectedProject = null;
			}
			else {
				list.childWidget(list.selectedProject).removeCSSClass('Selected');
				projectWidget.addCSSClass('Selected');
				list.selectedProject = projectWidget.hash;
			}
		}
		else {
			list.selectedProject = projectWidget.hash;
			projectWidget.addCSSClass('Selected');
		}
	};
	
	CODAAuthShell.editProject = function(event) {
		
		var optionsPanel = KDOM.getEventWidget(event, 'KPanel');
		var list = optionsPanel.childWidget('/project-list');
		var topWidget = list.getParent('CODAAuthShell');
		var access_token = encodeURIComponent(KOAuth.getAuthorization(topWidget.shell).authentication.replace(/OAuth2.0 /, ''));
		
		if (list.selectedProject == null) {
			debug("nao ha nenhum seleccionado");
		}
		else {
			window.location = topWidget.data.authorize_url + "?" + (topWidget.data.response_type ? "response_type=" + topWidget.data.response_type + "&" : "") + (topWidget.data.client_id ? "client_id=" + topWidget.data.client_id + "&" : "") + (topWidget.data.redirect_uri ? "redirect_uri=" + escape(topWidget.data.redirect_uri) + "&" : "") + "scope=@" + list.selectedProject + ":" + (!!Kinky.SHELL_CONFIG['/CODA/auth'].scope ? Kinky.SHELL_CONFIG['/CODA/auth'].scope : "admin") + "&" + (topWidget.data.state ? "state=" + topWidget.data.state + "&" : "") + (topWidget.data.display ? "display=" + topWidget.data.display + "&" : "") + "access_token=" + access_token;
		}
	};
	
	KSystem.included('CODAAuthShell');
});
function CODACloudBoxList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/box/folders/0' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudBoxList.prototype = new CODACloudList();
	CODACloudBoxList.prototype.constructor = CODACloudBoxList;

	CODACloudBoxList.prototype.download = function(element, widget) {
		if (!!element.uri) {
			var pup = window.open(element.uri, "width=300,height=200,status=0,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());
		}
		else {
			this.getFileInfo('download', element);
		}
	};

	CODACloudBoxList.prototype.use = function(element, widget) {
		if (!!element.uri) {
			this.getParent('CODACloudBrowser').select(element);
		}
		else {
			this.getFileInfo('use', element);
		}
	};

	CODACloudBoxList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudBoxList.prototype.mapper = function(element) {
		if (element.type == 'folder') {
			element.mimetype = 'inode/directory';
			element.uri = '/box/folders/' + element.id;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			element.mimetype = this.getMimeType(element.name);
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.uri = (!!element.shared_link ? element.shared_link.url : undefined);
			element.download_uri = (!!element.shared_link ? element.shared_link.download_url : undefined);
			element.href = '/file/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudBoxList.prototype.populate = function() {
		if (!!this.elements.item_collection && !!this.elements.item_collection.entries) {
			return this.elements.item_collection.entries;
		}
		return this.elements;
	};

	CODACloudBoxList.prototype.createSharedLInk = function(callback, element) {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_CLOUD_BROWSER_CREATE_SHARED'),
			callback : function() {
				self.sharedCallback = callback;
				self.sharedElement = element;
				self.kinky.put(self, 'oauth:/box/files/' + element.id, { shared_link : { access : "open"} }, this.headers, 'onSharedLinkCreated');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	}

	CODACloudBoxList.prototype.onSharedLinkCreated = function(data, request) {
		this.sharedElement.shared_link = data.shared_link;
		this.sharedElement.uri = this.sharedElement.shared_link.url;
		this.sharedElement.download_uri = this.sharedElement.shared_link.download_url;

		if (this.sharedCallback == 'use') {
			this.use(this.sharedElement);
		}
		else {
			this.download(this.sharedElement);			
		}
		this.sharedElement = undefined;
		this.sharedCallback = undefined;
	};

	CODACloudBoxList.prototype.getFileInfo = function(callback, element) {
		this.sharedCallback = callback;
		this.sharedElement = element;
		this.kinky.get(this, 'oauth:/box/files/' + element.id, {  }, this.headers, 'onFileGetInfo');
	}

	CODACloudBoxList.prototype.onFileGetInfo = function(data, request) {
		if (!!data.shared_link) {
			this.sharedElement.shared_link = data.shared_link;
			this.sharedElement.uri = this.sharedElement.shared_link.url;
			this.sharedElement.download_uri = this.sharedElement.shared_link.download_url;

			if (this.sharedCallback == 'use') {
				this.use(this.sharedElement);
			}
			else {
				this.download(this.sharedElement);
			}
			this.sharedElement = undefined;
			this.sharedCallback = undefined;
		}
		else {
			this.createSharedLInk(this.sharedCallback, this.sharedElement);
		}
	};


	KSystem.included('CODACloudBoxList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudBrowser(parent, target, filter) {
	if (parent != null) {
		CODAForm.call(this, parent, 'oauth:/users/me?embed=accounts,scopes', null, null);
		this.headers = {
			'Authorization' : 'OAuth2.0 ' + decodeURIComponent(this.getShell().data.module_token)
		}
		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this._target = target;
		if (typeof filter == 'string') {
			this.filter = {
				mimetype : filter
			}
		}
		else {
			this.filter = filter;
		}

		this._target._filedata = undefined;
	}
}

KSystem.include([
	'CODAForm',
	'CODACloudStorageList',
	'CODACloudInstagramList',
	'CODACloudFacebookList',
	'CODACloudFlickrList',
	'CODACloudYoutubeList',
	'CODACloudTumblrList',
	'CODACloudBoxList',
	'CODACloudDropboxList',
	'CODACloudCopyList',
	'CODACloudList'
], function() {
	CODACloudBrowser.prototype = new CODAForm();
	CODACloudBrowser.prototype.constructor = CODACloudBrowser;

	CODACloudBrowser.prototype.refresh = function() {
		var target = this._target;
		var filter = this.filter;
		var shell = this.getShell();

		this.getParent('KFloatable').closeMe();
		KSystem.addTimer(function() {
			shell.showCloudBrowser(target, filter);
		}, 750);

	};

	CODACloudBrowser.prototype.select = function(element) {
		if (!!this.filter) {
			if (!!this.filter.mimetype) {
				var mimefamily = element.mimetype.split('/')[0];
				var mimespec = element.mimetype.split('/')[1];
				var filterfamily = this.filter.mimetype.split('/')[0];
				var filterspec = this.filter.mimetype.split('/')[1];

				var invalid = false;
				if (filterfamily != '*' && filterfamily != mimefamily) {
					invalid = true;
				}
				else {
					if (filterspec != '*' && filterspec != mimespec) {
						invalid = true;
					}
				}

				if (invalid) {
					KMessageDialog.alert({
						message : gettext('$CODA_CLOUD_BROWSER_MIME_NOTACCEPTED').replace('$MIMETYPE', this.filter.mimetype),
						shell : this.shell,
						modal : true,
						width : 400,
						height : 300
					});
					return;
				}

				if (!!this.filter.size && /image\//.test(this.filter.mimetype)) {
					this._selected_element = element;
					this.kinky.get(this, 'oauth:/filemanager/dimensions?uri=' + encodeURIComponent(element.uri), {}, { 'E-Tag' : '' + (new Date()).getTime()}, 'onImageSize');
					return;					
				}
			}
		}
		this._target.setValue(element.uri);
		this.getParent('KFloatable').closeMe();
	};

	CODACloudBrowser.prototype.onImageSize = function(data, request) {
		var invalid = false;
		var text = undefined;

		if (!!this.filter.size.min && (this.filter.size.min.width > data.width || this.filter.size.min.height > data.height)) {
			text = gettext('$CODA_CLOUD_BROWSER_MINSIZE_NOTACCEPTED').replace('$SIZE', this.filter.size.min.width + 'x' + this.filter.size.min.height);
		}

		if (!!this.filter.size.max && (this.filter.size.max.width > data.width || this.filter.size.max.height > data.height)) {
			text = gettext('$CODA_CLOUD_BROWSER_MAXSIZE_NOTACCEPTED').replace('$SIZE', this.filter.size.max.width + 'x' + this.filter.size.max.height);
		}

		if (!!this.filter.size.bytes && this.filter.size.bytes > data.size) {
			text = gettext('$CODA_CLOUD_BROWSER_BYTES_NOTACCEPTED').replace('$SIZE', this.filter.size.bytes);
		}

		if (invalid) {
			KMessageDialog.alert({
				message : text,
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
			this._selected_element = undefined;
			delete this._selected_element;
			return;
		}

 		this._target._filedata = data;
		this._target.setValue(this._selected_element.uri);
		this._selected_element = undefined;
		delete this._selected_element;
		this.getParent('KFloatable').closeMe();
	};

	CODACloudBrowser.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target.id = this.target.href = data._id;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACloudBrowser.prototype.addPanel = function(element, title, isDefault) {
		KLayeredPanel.prototype.addPanel.call(this, element, title, isDefault);

		var usablew = KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.BROWSER_DIALOG_MARGIN;

		if (CODAGenericShell.BROWSER_DIALOG_MAXWIDTH < usablew) {
			element.setStyle({
				width : (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN - (usablew - CODAGenericShell.BROWSER_DIALOG_MAXWIDTH) - 335) + 'px'
			});
		}
		else {
			element.setStyle({
				width : (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN - 335) + 'px'
			});
		}
	};

	CODACloudBrowser.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODACloudBrowser.prototype.addMinimizedPanel = function() {
		var data = this.data;

		var panel = this.createPanel("appstorage", {
			type : 'oauth2',
			resolver : 'CODACloudStorageList',
			icon : 'fa fa-hdd-o'
		}, data);
		this.addPanel(panel, gettext('$CODA_USERS_INFO_APPSTORAGE_LINK'), true);

		this.addHeaderSeparator(gettext('$CODA_USERS_INFO_SOCIALNETWORK_SEPARATOR'));

		var social = {
			"instagram" : {
				type : 'oauth2',
				resolver : 'CODACloudInstagramList',
				width : 820,
				height : 520,
				icon : 'fa fa-instagram'
			},
			"facebook" : {
				type : 'oauth2',
				resolver : 'CODACloudFacebookList',
				width : 820,
				height : 450,
				icon : 'fa fa-facebook-square'
			},
			"flickr" : {
				type : 'oauth1',
				resolver : 'CODACloudFlickrList',
				width : 980,
				height : 700,
				icon : 'fa fa-flickr'
			},
			"tumblr" : {
				type : 'oauth1',
				resolver : 'CODACloudTumblrList',
				width : 820,
				height : 640,
				icon : 'fa fa-tumblr-square'
			},
			"google" : {
				type : 'oauth2',
				resolver : 'CODACloudYoutubeList',
				width : 820,
				height : 570,
				icon : 'fa fa-youtube'
			}
		};

		for (var s in social) {
			var prefix = s.toUpperCase();
			var panel = this.createPanel(s, social[s], data);
			if (s == 'google') {
				this.addPanel(panel, gettext('$CODA_USERS_INFO_YOUTUBE_LINK'), false);
			}
			else {
				this.addPanel(panel, gettext('$CODA_USERS_INFO_' + prefix + '_LINK'), false);
			}
		}

		this.addHeaderSeparator(gettext('$CODA_USERS_INFO_CLOUDSTORAGE_SEPARATOR'));

		var cloud = {
			"box" : {
				type : 'oauth2',
				resolver : 'CODACloudBoxList',
				width : 820,
				height : 640,
				icon : 'rp rp-box-rect'
			},
			"copy" : {
				type : 'oauth1',
				resolver : 'CODACloudCopyList',
				width : 820,
				height : 700,
				icon : 'fa fa-cloud'
			},
			"dropbox" : {
				type : 'oauth2',
				resolver : 'CODACloudDropboxList',
				width : 1000,
				height : 660,
				icon : 'fa fa-dropbox'
			}
		};

		for (var s in cloud) {
			var prefix = s.toUpperCase();
			var panel = this.createPanel(s, cloud[s], data);
			this.addPanel(panel, gettext('$CODA_USERS_INFO_' + prefix + '_LINK'), false);
		}
	};

	CODACloudBrowser.prototype.setDefaults = function(params){
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;

		delete params.accounts;
	};

	CODACloudBrowser.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACloudBrowser.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACloudBrowser.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_USERS_NOT_FOUND'));
	};

	CODACloudBrowser.prototype.onPost = CODACloudBrowser.prototype.onCreated = function(data, request){
		this.getShell().dashboard.refresh();
		this.parent.closeMe();
	};

	CODACloudBrowser.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODACloudBrowser.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODACloudBrowser.prototype.createPanel = function(s, social, data) {
		var prefix = s.toUpperCase();
		var account = CODACloudBrowser.getAccount(data, s);

		if (s != 'appstorage' && !account) {
			var panel = new KPanel(this);
			{
				panel.hash = '/' + s;
				var handler = null;
				if (social.type == 'oauth2') {
					handler = CODACloudBrowser.oauth2Event('/accounts/' + s + '/connect', social.width, social.height, 'connected');
				}
				else {
					handler = CODACloudBrowser.oauth1Event('/accounts/' + s + '/connect', social.width, social.height, 'connected');
				}

				var calltoaction = new KText(panel);
				{
					calltoaction.setLead(gettext('$CODA_USERS_NO_' + prefix));
					calltoaction.setText(gettext('$CODA_USERS_INFO_NOT_CONNECTED_DESC'));
				}
				panel.appendChild(calltoaction);

				var action = new KButton(panel, '', 'link-' + s, handler);
				{
					action.setText(gettext('$CODA_USERS_' + prefix + '_LINK_BUTTON'), true);
				}
				panel.appendChild(action);
			}
			return panel;
		}
		else {
			var panel = null;
			eval('panel = new ' + social.resolver + '(this)');
			panel.hash = '/cloud/server/' + s;
			panel.prefix = s;
			if (social.type == 'oauth1') {
				panel.oauth_handler = CODACloudBrowser.oauth1Event('/accounts/' + s + '/connect', social.width, social.height, 'reconnected');
			}
			else {
				panel.oauth_handler = CODACloudBrowser.oauth2Event('/accounts/' + s + '/connect', social.width, social.height, 'reconnected');
			}
			return panel;
		}
	};

	CODACloudBrowser.getAccount = function(data, type) {
		if (!!data.accounts && !!data.accounts.elements) {
			for (var a in data.accounts.elements) {
				if (data.accounts.elements[a].type.toLowerCase() == type.toLowerCase()) {
					if (!!data.accounts.elements[a].profile) {
						return data.accounts.elements[a].profile;
					}
					else {
						return data.accounts.elements[a];
					}
				}
			}
		}
		return false;
	};

	CODACloudBrowser.oauth1Event = function(url, width, height, prefix) {
		return function(e) {
			var docroot = Kinky.getDefaultShell().data.docroot;
			if (docroot.indexOf('http') == -1) {
				docroot = window.document.location.protocol + docroot;
			}
			var restroot = Kinky.getDefaultShell().data.providers.oauth.services;
			if (restroot.indexOf('http') == -1) {
				restroot = window.document.location.protocol + restroot;
			}

			if (prefix == 'connected') {
				window.CODA_WAITING_AUTH = KDOM.getEventWidget(e, 'CODAForm');
			}

			var reredir = restroot + url + '?redirect_uri=' + encodeURIComponent(docroot + CODA_VERSION + '/social/' + prefix + '.html');
			var pup = window.open(restroot + url + '?display=popup&redirect_uri=' + encodeURIComponent(reredir), "", "width=" + width + ",height=" + height + ",status=1,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());			
		};
	};

	CODACloudBrowser.oauth2Event = function(url, width, height, prefix) {
		return function(e) {
			var docroot = Kinky.getDefaultShell().data.docroot;
			if (docroot.indexOf('http') == -1) {
				docroot = window.document.location.protocol + docroot;
			}
			var restroot = Kinky.getDefaultShell().data.providers.oauth.services;
			if (restroot.indexOf('http') == -1) {
				restroot = window.document.location.protocol + restroot;
			}
			
			if (prefix == 'connected') {
				window.CODA_WAITING_AUTH = KDOM.getEventWidget(e, 'CODAForm');
			}

			var pup = window.open(restroot + url + '?display=popup&redirect_uri=' + encodeURIComponent(docroot + CODA_VERSION + '/social/' + prefix + '.html'), "", "width=" + width + ",height=" + height + ",status=1,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());
		};
	};

	KSystem.included('CODACloudBrowser');
}, Kinky.CODA_INCLUDER_URL);
function CODACloudCopyList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/copy/meta' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudCopyList.prototype = new CODACloudList();
	CODACloudCopyList.prototype.constructor = CODACloudCopyList;

	CODACloudCopyList.prototype.download = function(element, widget) {
		if (!!element.uri) {
			var pup = window.open(element.uri, "width=300,height=200,status=0,location=1,resizable=no,left=200,top=200");
			KSystem.popup.check(pup, Kinky.getDefaultShell());
		}
		else {
			this.createSharedLInk('download', element);
		}
	};

	CODACloudCopyList.prototype.use = function(element, widget) {
		if (!!element.uri) {
			this.getParent('CODACloudBrowser').select(element);
		}
		else {
			this.createSharedLInk('use', element);
		}
	};

	CODACloudCopyList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudCopyList.prototype.mapper = function(element) {
		if (/folder|share|dir|copy|company|inbox/.test(element.type)) {
			element.mimetype = 'inode/directory';
			element.uri = '/copy/meta' + element.id;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			element.mimetype = element.mime_type;
			element.uri = (!!element.links && element.links.length != 0 ? element.links[0].url : undefined);
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/file/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudCopyList.prototype.populate = function() {
		if (!!this.elements.children) {
			return this.elements.children;
		}
		return this.elements;
	};

	CODACloudCopyList.prototype.createSharedLInk = function(callback, element) {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_CLOUD_BROWSER_CREATE_SHARED'),
			callback : function() {
				self.sharedCallback = callback;
				self.sharedElement = element;
				self.kinky.post(self, 'oauth:/copy/links', { "public": true, name: element.name, paths: [ element.path ] }, this.headers, 'onSharedLinkCreated');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	}

	CODACloudCopyList.prototype.onSharedLinkCreated = function(data, request) {
		this.sharedElement.uri = this.sharedElement.url;

		if (this.sharedCallback == 'use') {
			this.use(this.sharedElement);
		}
		else {
			this.download(this.sharedElement);			
		}
		this.sharedElement = undefined;
		this.sharedCallback = undefined;
	};

	KSystem.included('CODACloudCopyList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudDropboxList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/dropbox/metadata/dropbox' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudDropboxList.prototype = new CODACloudList();
	CODACloudDropboxList.prototype.constructor = CODACloudDropboxList;

	CODACloudDropboxList.prototype.download = function(element, widget) {
		if (!!element.uri) {
			var pup = window.open(element.uri, "width=300,height=200,status=0,location=1,resizable=no,left=200,top=200");
 			KSystem.popup.check(pup, Kinky.getDefaultShell());
		}
		else {
			this.createSharedLInk('download', element);
		}
	};

	CODACloudDropboxList.prototype.use = function(element, widget) {
		if (!!element.uri) {
			this.getParent('CODACloudBrowser').select(element);
		}
		else {
			this.createSharedLInk('use', element);
		}
	};

	CODACloudDropboxList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudDropboxList.prototype.mapper = function(element) {
		if (element.is_dir) {
			element.id = element.rev;
			element.name = element.path.substring(element.path.lastIndexOf('/') + 1);
			element.mimetype = 'inode/directory';
			element.uri = '/dropbox/metadata/dropbox' + element.path.replace(/ /g, '%2520');
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			element.id = element.rev;
			element.name = element.path.substring(element.path.lastIndexOf('/') + 1);
			element.mimetype = element.mime_type;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.uri = undefined;
			element.href = '/file/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudDropboxList.prototype.populate = function() {
		if (!!this.elements.contents) {
			return this.elements.contents;
		}
		return this.elements;
	};

	CODACloudDropboxList.prototype.createSharedLInk = function(callback, element) {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_CLOUD_BROWSER_CREATE_SHARED'),
			callback : function() {
				self.sharedCallback = callback;
				self.sharedElement = element;
				self.kinky.post(self, 'oauth:/dropbox/shares/dropbox' + element.path.replace(/ /g, '%2520') + '?short_url=false', { }, this.headers, 'onSharedLinkCreated');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	}

	CODACloudDropboxList.prototype.onSharedLinkCreated = function(data, request) {
		this.sharedElement.uri = data.url;

		if (this.sharedCallback == 'use') {
			this.use(this.sharedElement);
		}
		else {
			this.download(this.sharedElement);	
		}
		this.sharedElement = undefined;
		this.sharedCallback = undefined;
	};

	KSystem.included('CODACloudDropboxList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudFacebookList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/facebook/me/albums?fields=name,cover_photo,type&limit=1000' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudFacebookList.prototype = new CODACloudList();
	CODACloudFacebookList.prototype.constructor = CODACloudFacebookList;

	CODACloudFacebookList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:/facebook/' + element.id + '/photos?fields=images,source,album&limit=1000' }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudFacebookList.prototype.mapper = function(element) {
		if (element.source) {
			element.name = element.source.substring(element.source.lastIndexOf('/') + 1);
			element.uri = element.source;
			element.icon = '<img src="' + element.source + '"></img>';
			element.href = '/image/' + element.id;
			element.mimetype = 'image/' + element.source.substring(element.source.lastIndexOf('.') + 1).toLowerCase();
			element.type = 'file';
		}
		else {
			element.icon = CODACloudList.DEFAULT_FOLDER_ICON;
			element.href = '/folder/' + element.id;
			element.mimetype = 'inode/directory';
			element.type = 'folder';
		}
		return element;
	};

	CODACloudFacebookList.prototype.populate = function() {
		return this.elements.data;
	};

	KSystem.included('CODACloudFacebookList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudFlickrList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/flickr/flickr.photosets.getList' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudFlickrList.prototype = new CODACloudList();
	CODACloudFlickrList.prototype.constructor = CODACloudFlickrList;

	CODACloudFlickrList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudFlickrList.prototype.mapper = function(element) {
		if (element.photos) {
			element.mimetype = 'inode/directory';
			element.uri = '/flickr/flickr.photosets.getPhotos?extras=original_format,date_upload&photoset_id=' + element.id;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.name = element.title._content;
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			element.mimetype = 'image/jpeg';
			element.name = element.title;
			element.uri = 'http://farm' + element.farm + '.staticflickr.com/' + element.server + '/' + element.id + '_' + element.originalsecret + '_o.' + element.originalformat;
			element.icon = this.getIconForMime(element.mimetype, 'http://farm' + element.farm + '.staticflickr.com/' + element.server + '/' + element.id + '_' + element.secret + '_s.jpg');
			element.href = '/image/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudFlickrList.prototype.populate = function() {
		if (!!this.elements.photosets && !!this.elements.photosets.photoset) {
			return this.elements.photosets.photoset;
		}
		if (!!this.elements.photoset && !!this.elements.photoset.photo) {
			return this.elements.photoset.photo;
		}
		return this.elements;
	};

	KSystem.included('CODACloudFlickrList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudInstagramList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/instagram/users/self/media/recent' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudInstagramList.prototype = new CODACloudList();
	CODACloudInstagramList.prototype.constructor = CODACloudInstagramList;

	CODACloudInstagramList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				break;
			}
		}
	};

	CODACloudInstagramList.prototype.mapper = function(element) {
		if (element.type == 'image') {
			element.uri = element.images.standard_resolution.url;
			element.name = element.uri.substring(element.uri.lastIndexOf('/') + 1);
			element.mimetype = 'image/jpeg';
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/image/' + element.id;
			element.type = 'file';
		}
		else {
			element.uri = element.videos.standard_resolution.url;
			element.name = element.uri.substring(element.uri.lastIndexOf('/') + 1);
			element.mimetype = 'video/mp4';
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/video/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudInstagramList.prototype.populate = function() {
		return this.elements.data;
	};

	KSystem.included('CODACloudInstagramList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KUploadButton',
	'KButton',
	'KCheckBox'
], function() {
	CODACloudListItem.prototype = new CODAListItem();
	CODACloudListItem.prototype.constructor = CODACloudListItem;

	CODACloudListItem.prototype.onClick = function() {
		this.getParent('CODACloudList').clicked(this.data, this);
	};

	CODACloudListItem.prototype.download = function() {
		this.getParent('CODACloudList').download(this.data, this);
	};

	CODACloudListItem.prototype.use = function() {
		this.getParent('CODACloudList').use(this.data, this);
	};

	CODACloudListItem.prototype.upload = function(data) {
		this.getParent('CODACloudList').upload(data, this.data, this);
	};

	CODACloudListItem.prototype.remove = function() {
		var widget = this;

		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_FILE_CONFIRM'),
			callback : function() {
				widget.kinky.post(widget, 'oauth:/filemanager/deletefile', {
					serverIndex : widget.data.server_uri,
					fileRoot : widget.data.folder_uri,
					files : [ widget.data.name ]
				}, null, 'onRemove');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODACloudListItem.prototype.draw = function() {
		var delbt = this.getParent('CODAList').options.delete_button;
		if (this.data.type == 'folder' || this.data.type == 'server') {
			var open = new KButton(this, ' ', 'open', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAListItem');
				Kinky.unsetModal(widget);
				widget.onClick();
			});
			{
				open.hasClickOutListener = true;
				open.setText('<i class="fa fa-folder-open"></i> ' + gettext('$CODA_CLOUD_OPENFOLDER_ACTION'), true);
			}
			this.appendChild(open);

			var list = this.getParent('CODACloudList');
			if (!!this.allowupload && (!!list.server || this.data.type == 'server')) {
				var options =  {
					access_token : decodeURIComponent(this.getShell().data.module_token),
					endpoint : this.getShell().data.providers.oauth.services + '/filemanager/upload',
					aditional_args : 'mediaServer=' + encodeURIComponent((this.data.type != 'server' ? list.server : this.data.server)) + '&folderPath=' + encodeURIComponent(this.data.uri)
				};

				var upload = new KUploadButton(this, ' ', 'upload', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAListItem');
					Kinky.unsetModal(widget);
					widget.upload(upload.uploaded_data);
				}, options);
				{

					upload.disable = function() {
						list.disable();
					};
					upload.enable = function() {
						list.enable();
					};
					upload.fileref = this.data.uri;
					upload.hasClickOutListener = true;
					upload.setText('<i class="fa fa-upload"></i> ' + gettext('$CODA_CLOUD_UPLOAD_ACTION'), true);
				}
				this.appendChild(upload);
			}

		}
		else if (this.data.type == 'up-one-level') {

		}
		else {
			var use = new KButton(this, ' ', 'use', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAListItem');
				Kinky.unsetModal(widget);
				widget.use();
			});
			{
				use.hasClickOutListener = true;
				use.setText('<i class="fa fa-check-square-o"></i> ' + gettext('$CODA_CLOUD_USE_ACTION'), true);
			}
			this.appendChild(use);

			var download = new KButton(this, ' ', 'download', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAListItem');
				Kinky.unsetModal(widget);
				widget.download();
			});
			{
				download.hasClickOutListener = true;
				download.setText('<i class="fa fa-arrow-circle-o-down"></i> ' + gettext('$CODA_CLOUD_DOWNLOAD_ACTION'), true);
			}
			this.appendChild(download);

			this.getParent('CODAList').options.delete_button = true;
		}

		CODAListItem.prototype.draw.call(this);
		this.getParent('CODAList').options.delete_button = delbt;
		if (!!this.data.extra) {
			var h2 = window.document.createElement('div');
			h2.innerHTML = this.data.extra;
			h2.className = ' OnlyOnExpand ';
			this.titleContainer.appendChild(h2);
		}
	};

	KSystem.included('CODACloudListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudList(parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, {}, null, {
			one_choice : true,
			drop_panel : false,
			make_draggable : false,
			add_button : false,
			delete_button : false,
			edit_button : false
		});
		this.headers = {
			'Authorization' : 'OAuth2.0 ' + decodeURIComponent(this.getShell().data.module_token)
		}
		this._data = data;
		this.allowupload = data.allowupload;
		this.onechoice = true;
		this.pageSize = 0;
		this.table = {
			"icon" : {
				label : '<i class="fa fa-file"></i>',
				fixed : 20
			},
			"name" : {
				label : "Nome"
			}
		};
		this.fieldslist = undefined;
		this.root = [];
		
		this.LIST_MARGIN = CODAGenericShell.BROWSER_DIALOG_MARGIN + 238;
		var usablew = KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.BROWSER_DIALOG_MARGIN;
		if (CODAGenericShell.BROWSER_DIALOG_MAXWIDTH < usablew) {
			this.LIST_MARGIN += usablew - CODAGenericShell.BROWSER_DIALOG_MAXWIDTH ;
		}

		this.list.onBadRequest = this.list.onUnauthorized = function(data, request) {
			window.CODA_WAITING_AUTH = this;
			this.getParent('CODACloudList').oauth_handler();
		};
	}
}

KSystem.include([
	'CODAList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudList.prototype = new CODAList();
	CODACloudList.prototype.constructor = CODACloudList;

	CODACloudList.prototype.visible = function() {
		if (this.display && !this.data.links) {
			this.display = false;
			this.setData(this._data);
			this.list.refreshPages(true);
		}
		else {
			CODAList.prototype.visible.call(this);
			if (!this.data.links) {
				this.setStyle({
					visibility: 'hidden'
				});
			}
		}
	};

	CODACloudList.prototype.open = function(element) {
	};

	CODACloudList.prototype.upload = function(uploaded, element, widget) {
	};

	CODACloudList.prototype.download = function(element, widget) {
		var pup = window.open(element.uri, "width=300,height=200,status=0,location=1,resizable=no,left=200,top=200");
		KSystem.popup.check(pup, Kinky.getDefaultShell());
	};

	CODACloudList.prototype.use = function(element, widget) {
		this.getParent('CODACloudBrowser').select(element);
	};

	CODACloudList.prototype.clicked = function(element, widget) {
		if (element.type == 'up-one-level') {
			var data = this.root.pop();
			this.setData(data);
			this.lastopened = undefined;
			this.list.refreshPages(true);
			return;
		}
		else if (element.type == 'file') {
			var mimefamily = element.mimetype.split('/')[0];

			if (!!this.lastopened) {
				if (!!this.lastopened.menuToggled) {
					KDOM.fireEvent(this.lastopened.menuActivator, 'click');
				}
				this.lastopened.removeCSSClass('CODAListItemExpanded CODAListItemExpanded_' + this.lastopened.data.mimetype.split('/')[0]);
			}

			if (!!element.extra) {
				widget.addCSSClass('CODAListItemExpanded CODAListItemExpanded_' + mimefamily);
				this.lastopened = widget;
				if (!widget.menuToggled) {
					KSystem.addTimer(function() {
						KDOM.fireEvent(widget.menuActivator, 'click');
					}, 350);
				}
			}
			else {
				this.lastopened = widget;
				KDOM.fireEvent(widget.menuActivator, 'click');
			}
		}
		else {
			this.data.label = element.name;
			this.data.uri = element.uri;
			this.root.push(this.data);
		}
		this.open(element);
	};


	CODACloudList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODACloudList.prototype.mapper = function(element) {
		return element;
	};

	CODACloudList.prototype.map = function(element) {
		var folders = [];
		var files = [];
		for (var e in this.elements) {
			var d = this.mapper(this.elements[e]);
			d.extra = d.extra || this.getExtraForMime(d.mimetype, d.uri);
			
			if (d.type == 'file') {
				files.push(d);	
			}
			else {
				folders.push(d);
			}
		}
		return folders.concat(files);
	};

	CODACloudList.prototype.addItems = function(list) {
		this.tableHeaderTotal.style.display = 'none';

		this.lastopened = undefined;
		if (this.root.length != 0) {
			var listItem = new CODACloudListItem(list);
			listItem.index = -1;
			listItem.data = {
				id : 'up',
				name : '<span style="font-style: italic; opacity: 0.65;">' + this.root[this.root.length - 1].label + '</span>',
				icon : CODACloudList.DEFAULT_LEVELUP_ICON,
				type : 'up-one-level'
			};
			list.appendChild(listItem);
		}
		this.elements = this.populate();
		this.elements = this.map();

		for (var e in this.elements) {
			var listItem = new CODACloudListItem(list);
			{
				listItem.index = e;
				listItem.allowupload = this.allowupload;
				listItem.data = this.elements[e];
			}
			list.appendChild(listItem);
		}

		this.setStyle({
			visibility: 'visible'
		});

	};

	CODACloudList.prototype.populate = function() {
		return this.elements;
	};

	CODACloudList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add'
		});
	};

	CODACloudList.prototype.getIconForMime = function(mimetype, url) {
		var mimefamily = mimetype.split('/')[0];
		var mimespec = mimetype.split('/')[1];

		switch(mimefamily) {
			case 'inode' : {
				return CODACloudList.DEFAULT_FOLDER_ICON;
			}
			case 'image' : {
				if (!!url) {
					switch(mimespec) {
						case 'png' : 
						case 'jpg' : 
						case 'jpeg' : 
						case 'gif' : 
						case 'tif' : 
						case 'tiff' : 
						case 'svg+xml' : 
						case 'svg' : {
							return '<img src="' + url + '"></img>';
						}
					}					
				}
				return CODACloudList.DEFAULT_IMAGE_ICON;
			}
			case 'text' : {
				return CODACloudList.DEFAULT_TEXT_ICON;
			}
			case 'audio' : {
				return CODACloudList.DEFAULT_AUDIO_ICON;
			}
			case 'video' : {
				return CODACloudList.DEFAULT_VIDEO_ICON;
			}
			case 'application' : {
				switch(mimespec) {
					case 'json' : 
					case 'xml' : 
					case 'pdf' : 
					case 'xls' : 
					case 'xlsx' : 
					case 'ppt' : 
					case 'pptx' : 
					case 'docx' : 
					case 'doc' : {
						return CODACloudList.DEFAULT_TEXT_ICON;
					}
					case 'zip' : 
					case 'rar' : 
					case 'x-rar-compressed' : 
					case 'tar' : 
					case 'x-tar' : 
					case 'compress' : 
					case 'compressed' : 
					case 'bz' : 
					case 'bzip' : 
					case 'bz2' : 
					case 'bzip2' : 
					case 'gzip' : 
					case 'gz' : {
						return CODACloudList.DEFAULT_PACKAGE_ICON;
					}				
				}
				return CODACloudList.DEFAULT_APPLICATION_ICON;
			}
			default : {
				return CODACloudList.DEFAULT_FILE_ICON;
			}
		}
	};

	CODACloudList.prototype.getExtraForMime = function(mimetype, url) {
		if (!mimetype) {
			return undefined;
		}
		var mimefamily = mimetype.split('/')[0];
		var mimespec = mimetype.split('/')[1];

		switch(mimefamily) {
			case 'image' : {
				if (!!url) {
					return '<img class="OnlyOnExpand" src="' + url + '"></img>';
				}
				return undefined;
			}
			case 'audio' : {
				if (!!url) {
					return '<audio class="OnlyOnExpand" preload="metadata" controls><source src="' + url + '" type="'+ mimetype + '"></audio>';
				}
				return undefined;
			}
			case 'video' : {
				if (!!url) {
					if (mimespec == 'youtube') {
						return '<iframe class="OnlyOnExpand" width="267" height="200" src="' + url + '" frameborder="0" allowfullscreen></iframe>';
					}
					else {
						return '<video class="OnlyOnExpand" preload="metadata" width="267" height="200" controls><source src="' + url + '" type="'+ mimetype + '"></video>';
					}
				}
				return undefined;
			}
			default : {
				return undefined;
			}
		}
	};


	CODACloudList.prototype.getMimeType = function(name) {
		if (name.lastIndexOf('.') == -1) {
			return "application/*";
		}
		var extension = name.substring(name.lastIndexOf('.') + 1);
		switch(extension) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'gif':
			case 'svg':
			case 'tif':
			case 'tiff': {
				return 'image/' + extension;
			}
			case 'mp4':
			case 'mpg':
			case 'mpeg':
			case 'ogv':
			case 'avi':
			case 'mkv':
			case 'mkv':
			case 'webm': {
				return 'video/' + extension;
			}
			case 'mp3':
			case 'ogg':
			case 'wav': {
				return 'audio/' + extension;
			}
			case 'txt': {
				return 'text/plain';
			}
			case 'rtf': {
				return 'text/richtext';
			}
			case 'html': {
				return 'text/' + extension;
			}
			default : {
				return 'application/' + extension;
			}
		}
	};

	CODACloudList.DEFAULT_FOLDER_ICON = '<i class="fa fa-folder" style="color: rgb(180, 76, 0) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_FILE_ICON = '<i class="fa fa-file-o" style="color: rgb(32, 32, 32) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_IMAGE_ICON = '<i class="fa fa-camera" style="color: rgb(0, 117, 16) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_TEXT_ICON = '<i class="fa fa-file-text-o" style="color: rgb(177, 0, 117) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_AUDIO_ICON = '<i class="fa fa-music" style="color: rgb(0, 16, 117) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_VIDEO_ICON = '<i class="fa fa-video-camera" style="color: rgb(117, 16, 0) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_APPLICATION_ICON = '<i class="fa fa-cog" style="color: rgb(32, 32, 32) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_LEVELUP_ICON = '<i class="fa fa-chevron-left" style="color: rgb(180, 76, 0) !important; font-size: 18px !important;"></i>';
	CODACloudList.DEFAULT_PACKAGE_ICON = '<i class="fa fa-suitcase" style="color: rgb(117, 117, 0) !important; font-size: 18px !important;"></i>';

	KSystem.included('CODACloudList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudStorageList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/filemanager/serverlist' }
			},
			allowupload : true
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudStorageList.prototype = new CODACloudList();
	CODACloudStorageList.prototype.constructor = CODACloudStorageList;

	CODACloudStorageList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:/filemanager/filelist?serverIndex=' + this.server + '&fileRoot=' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
			case 'server' : {
				this.server = element.server;
				this.setData({
					links : {
						feed : { href :  'oauth:/filemanager/filelist?serverIndex=' + element.server + '&fileRoot=' +  element.uri}
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudStorageList.prototype.mapper = function(element) {
		if (!!element.type && element.type == 'server') {
			element.mimetype = 'inode/directory';
			element.uri = (element.name == 'User Storage' ? Kinky.getLoggedUser()._id + '/' : (element.name == 'App Storage' ? '/' + this.getShell().data.app_id.charAt(0) + '/' + this.getShell().data.app_id + '/' : '/'));
			element.icon = this.getIconForMime(element.mimetype);
			element.href = '/server/' + element.id;
			element.type = 'server';
		}
		else {
			element.mimetype = element.mime.split(';')[0];
			var mimefamily = element.mimetype.split('/')[0];

			switch (element.mimetype) {
				case 'inode/directory' : {
					element.name = element.title;
					element.server_uri = this.server;
					element.uri = (this.root.length != 0 ? this.root[this.root.length - 1].uri : '') + element.title + '/';
					element.icon = this.getIconForMime(element.mimetype);
					element.href = '/folder/' + element.id;
					element.type = 'folder';
					break;
				}
				default : {
					element.name = element.title;
					element.server_uri = this.server;
					element.folder_uri = this.root[this.root.length - 1].uri;
					element.uri = element.url;
					element.icon = this.getIconForMime(element.mimetype, element.url);
					element.href = '/file/' + element.id;
					element.type = 'file';
					break;
				}
			}
		}
		return element;
	};

	CODACloudStorageList.prototype.populate = function() {
		for (var f in this.elements) {
			if (typeof f  == 'string') {
				if (f.indexOf('media_url') == 0) {
					var elements = [];
					for ( var index in this.elements) {
						var name = this.elements[index];
						if (name == 'App Storage' && !this.getShell().data.app_id) {
							continue;
						}
						elements.push({
							name : name,
							type : 'server',
							server : index,
							uri : index,
							id : index
						});
					}
					return elements;
				}
				else {
					for ( var index in this.elements) {
						this.elements[index].id = index;
					}
					return this.elements;
				}
			}
			else {
				return this.elements;
			}
		}
	};

	KSystem.included('CODACloudStorageList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudTumblrList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/tumblr/user/info' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudTumblrList.prototype = new CODACloudList();
	CODACloudTumblrList.prototype.constructor = CODACloudTumblrList;

	CODACloudTumblrList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudTumblrList.prototype.mapper = function(element) {
		if (element.facebook_opengraph_enabled) {
			element.mimetype = 'inode/directory';
			element.uri = '/tumblr/blog/' + element.name + '.tumblr.com/posts?limit=100';
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			switch (element.type) {
				case 'photo' : {
					element.mimetype = 'image/*';
					element.uri = element.photos[0].original_size.url.replace(/http:/, window.document.location.protocol);
					element.name = element.slug || element.reblog_key;
					break;
				}
				case 'audio' : {
					element.mimetype = 'audio/*';
					element.uri = element.short_url.replace(/http:/, window.document.location.protocol);
					element.name = element.slug || element.reblog_key;
					element.extra = element.embed.replace('<iframe class="', '<iframe class="OnlyOnExpand ');
					break;
				}
				case 'video' : {
					element.mimetype = 'video/*';
					element.uri = element.permalink_url || element.short_url;
					element.uri = element.uri.replace(/http:/, window.document.location.protocol);
					element.name = element.slug || element.reblog_key;
					element.extra = element.player[0].embed_code.replace('<iframe class="', '<iframe class="OnlyOnExpand ');
					break;
				}
				default : {
					element.mimetype = 'aplication/*';
					element.uri = element.short_url.replace(/http:/, '');
					element.name = element.slug || element.reblog_key;
				}
			}
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.href = '/file/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudTumblrList.prototype.populate = function() {
		if (!!this.elements.response && !!this.elements.response.user && !!this.elements.response.user.blogs) {
			return this.elements.response.user.blogs;
		}
		if (!!this.elements.response && !!this.elements.response.posts) {
			var elements = this.elements.response.posts;

			for (var e = 0; e != elements.length; e++) {
				switch (elements[e].type) {
					case 'photo' : 
					case 'audio' : 
					case 'video' : {
						break;
					}
					default : {
						elements.splice(e, 1);
						e--;
					}
				}
			}
			return elements;
		}
		return this.elements;
	};

	KSystem.included('CODACloudTumblrList');
}, Kinky.CODA_INCLUDER_URL);

function CODACloudYoutubeList(parent) {
	if (parent != null) {
		CODACloudList.call(this, parent, {
			links : {
				feed : { href :  'oauth:/youtube/channels?part=id,snippet,contentDetails&mine=true' }
			}
		});
	}
}

KSystem.include([
	'CODACloudList',
	'CODACloudListItem',
	'KLink'
], function() {
	CODACloudYoutubeList.prototype = new CODACloudList();
	CODACloudYoutubeList.prototype.constructor = CODACloudYoutubeList;

	CODACloudYoutubeList.prototype.open = function(element) {
		switch(element.type) {
			case 'folder' : {
				this.setData({
					links : {
						feed : { href :  'oauth:' + element.uri }
					}
				});
				this.list.refreshPages(true);
				break;
			}
		}
	};

	CODACloudYoutubeList.prototype.mapper = function(element) {
		if (element.kind == 'youtube#channel') {
			element.mimetype = 'inode/directory';
			element.uri = '/youtube/playlistItems?part=id,snippet,status,contentDetails&playlistId=' + element.contentDetails.relatedPlaylists.uploads;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.name = element.snippet.title;
			element.href = '/folder/' + element.id;
			element.type = 'folder';
		}
		else {
			element.mimetype = 'video/youtube';
			element.uri = 'http://www.youtube.com/v/' + element.contentDetails.videoId;
			element.icon = this.getIconForMime(element.mimetype, element.uri);
			element.name = element.snippet.title;
			element.href = '/image/' + element.id;
			element.type = 'file';
		}
		return element;
	};

	CODACloudYoutubeList.prototype.populate = function() {
		return this.elements.items;
	};

	KSystem.included('CODACloudYoutubeList');
}, Kinky.CODA_INCLUDER_URL);

function CODADashboard(parent, options) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.options = KSystem.merge(KSystem.clone(CODADashboard.OPTIONS), options || {});
		this.nside = 0;
		this.addCSSClass('CODADashboard');
		this.addCSSClass('CODADashboardPanelNoSide');

		this.noFadeTransition = false;

		var img = window.document.createElement('img');
		img.src = '/imgs/dashboard.svg';
		this.panel.insertBefore(img, this.content);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODADashboard.prototype = new KPanel();
	CODADashboard.prototype.constructor = CODADashboard;

	CODADashboard.prototype.disable = function() {
		KPanel.prototype.disable.call(this);
	};

	CODADashboard.prototype.addPanels = function() {
	};

	CODADashboard.prototype.resize = function() {
		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990 * 50;
		var fs = Math.round(fp);

		this.setStyle({
			width: (KDOM.getBrowserWidth() - 20) + 'px',
			height: (KDOM.getBrowserHeight() - fs - 68) + 'px',
			marginTop: fs + 'px'
		});
	};

	CODADashboard.prototype.visible = function() {
		if (this.display) {
			return;
    		}
		var already = this.display;
		KPanel.prototype.visible.call(this);

		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990 * 50;
		var fs = Math.round(fp);

		this.setStyle({
			width: (KDOM.getBrowserWidth() - 20) + 'px',
			height: (KDOM.getBrowserHeight() - fs - 68) + 'px',
			marginTop: fs + 'px'
		});
		if (already) {
			return;
		}

		var _this = this;
		KSystem.addTimer(function() {
			KEffects.addEffect(_this, [
			{
				f : KEffects.easeOutExpo,
				type : 'move',
				duration : 500,
				gravity : {
					x : 'right'
				},
				lock : {
					y : true
				},
				go : {
					x : 0
				},
				from : {
					x : window.top.CODA_NAVIGATION_FROM * -410
				}
			},
			{
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				applyToElement : true,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				}
			}]);
		}, 250);
	};

	CODADashboard.prototype.draw = function() {
		this.addPanels();
		KPanel.prototype.draw.call(this);
	};

	CODADashboard.OPTIONS = {
		modifications : false
	};

	KSystem.included('CODADashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAFeedbackWizard(parent) {
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODAFeedbackWizard' ]);
		this.setTitle(gettext('$CODA_FEEDBACK_WIZARD_TITLE'));
		this.hash = '/feedback';
	}
}

KSystem.include([
	'CODAWizard',
	'KRadioButton',
	'KDate',
	'KPassword'
], function() {
	CODAFeedbackWizard.prototype = new CODAWizard();
	CODAFeedbackWizard.prototype.constructor = CODAFeedbackWizard;

	CODAFeedbackWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				var refers_to_main = this.values[0].refers_to_main;
				var panel = this.childWidget('/feedback/1');
				var refers_to = panel.childWidget('/category/refers_to');

				if (refers_to_main.indexOf('/app/') != -1) {
					var apps = undefined;
					if(typeof(Storage) !== "undefined") {
						apps = JSON.parse(localStorage['apps_owned']);
					}

					panel.description = gettext('$CODA_FEEDBACK_WIZ_CATEGORY2_APP_PANEL_DESC');

					refers_to.clear();
					refers_to.setLabel(gettext('$CODA_FEEDBACK_WIZ_REFERSTO_APP') + ' *');
					var currentapp = !!this.getShell().app_data ? this.getShell().app_data.id : undefined;
					for (var a in apps) {
						apps[a].repository = 'redpanda';
						refers_to.addOption(apps[a], '<img style="width: 29px; height: 29px; vertical-align: middle;" src="' +  apps[a].icon + '"></img>&nbsp;&nbsp;' + apps[a].name, apps[a].id == currentapp);
					}
				}
				else if (refers_to_main.indexOf('/backoffice/') != -1) {
					panel.description = gettext('$CODA_FEEDBACK_WIZ_CATEGORY2_BACKOFFICE_PANEL_DESC');

					var currentmod = this.getShell().module;
					refers_to.clear();
					refers_to.setLabel(gettext('$CODA_FEEDBACK_WIZ_REFERSTO_BACKOFFICE') + ' *');
					refers_to.addOption({ repository : 'coda', type : 'module-applications' }, '<i class="fa fa-puzzle-piece" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Applications Module'), currentmod == 'applications');
					refers_to.addOption({ repository : 'coda', type : 'module-accounts' }, '<i class="fa fa-group" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Accounts Module'), currentmod == 'accounts');
					refers_to.addOption({ repository : 'coda', type : 'module-campaigns' }, '<i class="fa fa-bullhorn" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Campaigns Module'), currentmod == 'campaigns');
					refers_to.addOption({ repository : 'coda', type : 'module-cms' }, '<i class="fa fa-compass" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA CMS Module'), currentmod == 'cms');
					refers_to.addOption({ repository : 'coda', type : 'module-forms' }, '<i class="fa fa-list-alt" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Forms Module'), currentmod == 'forms');
					refers_to.addOption({ repository : 'coda', type : 'module-payments' }, '<i class="fa fa-credit-card" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Payments Module'), currentmod == 'payments');
					refers_to.addOption({ repository : 'coda', type : 'module-products' }, '<i class="fa fa-shopping-cart" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Products Module'), currentmod == 'products');
					refers_to.addOption({ repository : 'coda', type : 'module-social-integration' }, '<i class="rp rp-share" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Social Integration'), currentmod == 'social-integration');
					refers_to.addOption({ repository : 'coda', type : 'module-statistics' }, '<i class="fa fa-bar-chart-o" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Statistics'), currentmod == 'statistics');
					refers_to.addOption({ repository : 'coda', type : 'module-store' }, '<i class="fa fa-building-o" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Store Module'), currentmod == 'store');
					refers_to.addOption({ repository : 'coda', type : 'module-styling' }, '<i class="rp rp-brush" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Styling Module'), currentmod == 'styling');
					refers_to.addOption({ repository : 'coda', type : 'module-users' }, '<i class="fa fa-user" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Users Module'), currentmod == 'users');
					refers_to.addOption({ repository : 'coda', type : 'module-vouchers' }, '<i class="fa fa-ticket" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA Vouchers Module'), currentmod == 'vouchers');
					refers_to.addOption({ repository : 'coda', type : 'module-myinfo' }, '<i class="fa fa-user" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('CODA MyInfo Module'), currentmod == 'my');
					refers_to.addOption({ repository : 'redpanda', type : 'module-quiz' }, '<i class="fa fa-question" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('REDPANDA Quiz Module'), currentmod == 'quiz');					
					refers_to.addOption({ repository : 'coda', type : 'general' }, '<i class="fa fa-asterisk" style="font-size: 2em;"></i>&nbsp;&nbsp;' + gettext('$CODA_FEEDBACK_GENERAL'), currentmod == 'general');
				}
				else {
					this.stepOverNext();
				}
				break;
			}
		}
	};

	CODAFeedbackWizard.prototype.onPrevious = function() {
		switch(this.current) {
			case 2: {
				var refers_to_main = this.values[0].refers_to_main;
				if (refers_to_main.indexOf('/general/') != -1 || refers_to_main.indexOf('/website/') != -1) {
					this.stepOverPrevious();
				}
				break;
			}
		}
	};

	CODAFeedbackWizard.prototype.mergeValues = function() {
		var params = {};
		for (var v  = 0; v != this.values.length; v++) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}

		params.status = 'new';
		switch(params.kind) {
			case 'bug' : {
				params.priority = 'major';
				break;
			}
			case 'enhancement' : {
				params.priority = 'minor';
				break;
			}
			case 'proposal' : {
				params.priority = 'trivial';
				break;
			}
		}

		params.content = '<i>' + params.name + ' <' + params.email + '></i> wrote:<br><br>' + params.content;
		if (!!params.screenshot) {
			params.content += '<br><hr></hr><br><br><img src="' + params.screenshot.replace(/ /gi, '%20') + '"></img><br><br>';
		}

		params.content += '<br><hr></hr><br><br><i>Application details:</i><br><br><ul>';
		switch(params.refers_to_main) {
			case '/app/general' : {
				params.content += '<li> <b>app_id: </b>' + params.refers_to.id;
				params.content += '</li><br><li> <b>app_name: </b>' + params.refers_to.name;
				params.content += '</li><br><li> <b>app_status: </b>' + params.refers_to.state;
				if (!!params.refers_to.channels && !!params.refers_to.channels.elements && !!params.refers_to.channels.elements.length) {
					if (!!params.refers_to.channels.elements[0].app_url) {
						params.content += '</li><br><li> <b>app_url: </b>' + params.refers_to.channels.elements[0].app_url;
					}
					params.content += '</li><br><li> <b>app_preview_url: </b>https://prototype.redpandasocial.com/?referrer_app=' + params.refers_to.id + '&fb_app_id=' + params.refers_to.channels.elements[0].app_id + '&page_id=' + params.refers_to.channels.elements[0].page_id;
				}
				break;
			}
			case '/backoffice/general' : {
				params.content += '<li> <b>app_id: </b>my.redpandasocial.com';
				params.content += '</li><br><li> <b>app_name: </b>My Redpanda Backoffice';
				params.content += '</li><br><li> <b>app_status: </b>active';
				break;
			}
			case '/website/' : {
				params.content += '<li> <b>app_id: </b>www.redpandasocial.com';
				params.content += '</li><br><li> <b>app_name: </b>Redpanda Website';
				params.content += '</li><br><li> <b>app_status: </b>active';
				params.refers_to = { repository : 'redpanda', type : 'website' };
				break;
			}
			case '/general/' : {
				params.content += '<li> <b>app_id: </b>-- unavailable --';
				params.refers_to = { repository : 'redpanda', type : 'general' };
				break;
			}
		}
		params.content += '</li></ul>';

		params.content += '<br><hr></hr><br><br><i>OS & Browser:</i><br><br><ul><li> <b>browser_name: </b>' + KBrowserDetect.browsername;
		params.content += '</li><br><li> <b>browser_version: </b>' + KBrowserDetect.version;
		params.content += '</li><br><li> <b>browser_major_version: </b>' + navigator.appVersion;
		params.content += '</li><br><li> <b>browser_engine: </b>' + navigator.product;
		params.content += '</li><br><li> <b>browser_agent: </b>' + navigator.userAgent;
		params.content += '</li><br><li> <b>os_language: </b>' + navigator.language;
		params.content += '</li><br><li> <b>os_platform: </b>' + navigator.platform;
		params.content += '</li><br><li> <b>os_screen: </b>' + window.screen.width + 'x' + window.screen.height;
		params.content += '</li><br><li> <b>os_window: </b>' + KDOM.getBrowserWidth() + 'x' + KDOM.getBrowserHeight() + '</li>';
		params.content += '</ul>';

		delete params.screenshot;
		delete params.name;
		delete params.email;

		params.title = params.title.toLowerCase();

		params.content = params.content.replace(/<img src=\"/gi, '![screenshot](').replace(/\"><\/img>/gi, ')\n');
		params.content = params.content.replace(/<a href=\"/gi, '[link](').replace(/\">([^<]*)<\/a>/gi, ')\n');
		params.content = params.content.replace(/<hr><\/hr>/gi, '--------------------').replace(/<hr\/>/gi, '\n--------------------\n').replace(/<hr>/gi, '\n--------------------\n');
		params.content = params.content.replace(/<h1>/gi, '\n# ').replace(/<br\/><\/h1>/gi, ' #\n').replace(/<br><\/h1>/gi, ' #\n').replace(/<\/h1>/gi, ' #\n');
		params.content = params.content.replace(/<br>/gi, '\n').replace(/<br\/>/gi, '\n').replace(/<\/p>/gi, '\n').replace(/<p>/gi, '');
		params.content = params.content.replace(/<b>/gi, '**').replace(/<\/b>/gi, '** ').replace(/<i>/gi, '*').replace(/<\/i>/gi, '* ').replace(/<u>/gi, '').replace(/<\/u>/gi, '');
		params.content = params.content.replace(/<ul>/gi, '').replace(/<\/ul>/gi, '\n').replace(/<ol>/gi, '1. ').replace(/<\/ol>/gi, '\n').replace(/<li>/gi, '* ').replace(/<\/li>/gi, '');

		return params;
	};

	CODAFeedbackWizard.prototype.onPut = CODAFeedbackWizard.prototype.onPost = CODAFeedbackWizard.prototype.onCreated = function(data, request) {
		var _shell = this.shell;
		this.getParent('KFloatable').closeMe();
		KMessageDialog.alert({
			message : gettext('$CODA_FEEDBACK_SUCCESS_MSG'),
			shell : _shell,
			html : true,
			modal : true,
			width : 400,
			height : 300
		});		
	};

	CODAFeedbackWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				var uri = 'oauth:/bitbucket/repositories/redpandasocial/' + params.refers_to.repository + '/issues'
				params.component = '' + params.refers_to.type;
				delete params.refers_to;
				delete params.refers_to_main;

				this.kinky.post(this, uri, params);
			}
		}
		catch (e) {

		}
	};

	CODAFeedbackWizard.addWizardPanel = function(parent)  {
		var bugPanel = new KPanel(parent);
		{
			bugPanel.hash = '/general';
			bugPanel.title = gettext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_TITLE');
			bugPanel.description = gettext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_DESC');
			bugPanel.icon = 'css:fa fa-tags';
			
			var refers_to_main = new KRadioButton(bugPanel, gettext('$CODA_FEEDBACK_WIZ_REFERSTO') + '*', 'refers_to_main');
			{
				refers_to_main.hash = '/category/main';

				refers_to_main.addOption('/app/general', gettext('$CODA_FEEDBACK_APP_GENERAL'), false);
				refers_to_main.addOption('/backoffice/general', gettext('$CODA_FEEDBACK_BACKOFFICE_GENERAL'), false);
				refers_to_main.addOption('/website/', 'Website', false);
				refers_to_main.addOption('/general/', gettext('$CODA_FEEDBACK_GENERAL'), false);

				refers_to_main.setHelp(gettext('$CODA_FEEDBACK_WIZ_REFERSTO_HELP'), CODAForm.getHelpOptions());
				refers_to_main.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			bugPanel.appendChild(refers_to_main);

			var kind = new KRadioButton(bugPanel, gettext('$CODA_FEEDBACK_WIZ_KIND') + '*', 'kind');
			{
				kind.addOption('bug', gettext('$CODA_FEEDBACK_BUG'), false);
				kind.addOption('enhancement', gettext('$CODA_FEEDBACK_SUGGESTION'), false);
				kind.addOption('proposal', gettext('$CODA_FEEDBACK_PROPOSAL'), false);

				kind.setHelp(gettext('$CODA_FEEDBACK_WIZ_KIND_HELP'), CODAForm.getHelpOptions());
				kind.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			bugPanel.appendChild(kind);
		}

		var categoryPanel = new KPanel(parent);
		{
			categoryPanel.hash = '/general4';
			categoryPanel.title = gettext('$CODA_FEEDBACK_WIZ_CATEGORY2_PANEL_TITLE');
			categoryPanel.description = gettext('$CODA_FEEDBACK_WIZ_CATEGORY2_PANEL_DESC');
			categoryPanel.icon = 'css:fa fa-tags';
			
			var refers_to = new KCombo(categoryPanel, gettext('$CODA_FEEDBACK_WIZ_REFERSTO') + '*', 'refers_to');
			{
				refers_to.hash = '/category/refers_to';
				refers_to.setHelp(gettext('$CODA_FEEDBACK_WIZ_REFERSTO_HELP'), CODAForm.getHelpOptions());
				refers_to.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				refers_to.setStyle({
					height : '2.5em',
					verticalAlign : 'middle'
				}, 'comboText');
				refers_to.setStyle({
					marginBottom : '8em !important'
				});
			}
			categoryPanel.appendChild(refers_to);
		}

		var infoPanel = new KPanel(parent);
		{
			infoPanel.hash = '/general2';
			infoPanel.title = gettext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_TITLE');
			infoPanel.description = gettext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_DESC');
			infoPanel.icon = 'css:fa fa-pencil';

			var title = new KInput(infoPanel, gettext('$CODA_FEEDBACK_WIZ_TITLE') + '*', 'title');
			{
				title.setHelp(gettext('$CODA_FEEDBACK_WIZ_TITLE_HELP'), CODAForm.getHelpOptions());
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			infoPanel.appendChild(title);

			var content = new KRichText(infoPanel, gettext('$CODA_FEEDBACK_WIZ_DESC') + '*', 'content');
			{
				content.setHelp(gettext('$CODA_FEEDBACK_WIZ_DESC_HELP'), CODAForm.getHelpOptions());
				content.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				content.setConfig({
					language : 'pt',
					coreStyles_bold : {
						element : 'b'
					},
					coreStyles_italic : {
						element : 'i'
					},
					fontSize_sizes : 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
					fontSize_style : {
						element : 'font',
						attributes : {
							'size' : '#(size)'
						}
					},
					format_tags : 'p;h1',
					height : this.height + 'px',
					toolbar : [
						[
							'Bold',
							'Italic'
						],
						[
	 						'BulletedList',
							'Outdent', 
							'Indent'
						],
						[
							'Undo',
							'Redo'
						],
						[
							'Link',
							'Unlink',
							'HorizontalRule', 
							'SpecialChar',
							'-',
							'RemoveFormat',
							'Format'
						]
					]
				})
			}
			infoPanel.appendChild(content);

			var screenshot = new KImageUpload(infoPanel, gettext('$CODA_FEEDBACK_WIZ_SCREENSHOT') + '*', 'screenshot', CODAGenericShell.getUploadAuthorization());
			{
				screenshot.setHelp(gettext('$CODA_FEEDBACK_WIZ_SCREENSHOT_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(screenshot, 'image/*');
				screenshot.appendChild(mediaList);	
			}
			infoPanel.appendChild(screenshot);

		}
		
		var reporterPanel = new KPanel(parent);
		{
			reporterPanel.hash = '/general3';
			reporterPanel.title = gettext('$CODA_FEEDBACK_WIZ_USER_PANEL_TITLE');
			reporterPanel.description = gettext('$CODA_FEEDBACK_WIZ_USER_PANEL_DESC');
			reporterPanel.icon = 'css:fa fa-user';

			var name = new KInput(reporterPanel, gettext('$CODA_FEEDBACK_WIZ_NAME') + '*', 'name');
			{
				if (!!Kinky.getLoggedUser()) {
					name.setValue(Kinky.getLoggedUser().name);
				}
				name.setHelp(gettext('$CODA_FEEDBACK_WIZ_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			reporterPanel.appendChild(name);

			var email = new KInput(reporterPanel, gettext('$CODA_FEEDBACK_WIZ_EMAIL') + '*', 'email');
			{
				if (!!Kinky.getLoggedUser()) {
					email.setValue(Kinky.getLoggedUser().email);
				}
				email.setHelp(gettext('$CODA_FEEDBACK_WIZ_EMAIL_HELP'), CODAForm.getHelpOptions());
				email.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				email.addValidator(/(^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9_\.\-]+$)|(^$)/, gettext('$ERROR_MUST_BE_EMAIL'));
			}
			reporterPanel.appendChild(email);

		}

		return [ bugPanel, categoryPanel, infoPanel, reporterPanel ];

	};

	KSystem.included('CODAFeedbackWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAFooter(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAFooter.prototype = new KPanel();
	CODAFooter.prototype.constructor = CODAFooter;

	KSystem.included('CODAFooter');
}, Kinky.CODA_INCLUDER_URL);
function CODAFormIconDashboard(parent, options, elements) {
	if (parent != null) {
		CODAIconDashboard.call(this, parent, options, elements);
		this.addCSSClass('CODAFormIconDashboard');
		this.removeCSSClass('CODADashboard');
		this.reducedDescription = true;
		this.columns = 7;
		this.filterValue = [];
		this.filterFields = [ 'title' ];
		this.actions = [];
		this.pageSize = 30;

		this.SELECTED = undefined;
		this.FORM_OPENED = undefined;

		this.noFadeTransition = true;
		this.helpURL = CODAGenericShell.generateHelpURL(this);

		var forms = undefined;
		if((forms = KCache.restore('/__tweaks')) != undefined) {
			if (!!forms[this.className]) {
				this.fieldConfig = forms[this.className];
			}
		}

	}
}

KSystem.include([
	'CODAIcon',
	'CODAIconDashboard'
], function() {
	CODAFormIconDashboard.prototype = new CODAIconDashboard();
	CODAFormIconDashboard.prototype.constructor = CODAFormIconDashboard;

	CODAFormIconDashboard.prototype.setTitle = function(text) {
		KWidget.prototype.setTitle.call(this, text, true);
		this.panel.insertBefore(this.title, this.content);
	};

	CODAFormIconDashboard.prototype.onClose = function() {
		for (var a in this.actions) {
			this.getShell().removeToolbarButton(this.actions[a]);
		}
		KBreadcrumb.dispatchEvent(null, {
			action : '/close/form' + this.hash
		});
	};	

	CODAFormIconDashboard.prototype.onLoad = function(data, request) {
		if (this.page != 0) {
			this.elements = !!data.elements ? this.elements.concat(data.elements) : this.elements;
			this.addPanels();
			CODAFormIconDashboard.wall(this, this.columns);			
		}
		else {
			this.elements = !!data.elements ? data.elements : [];
			this.draw();
		}
	};

	CODAFormIconDashboard.prototype.destroy = function() {
		if (!!CODAFormIconDashboard.CURRENT && CODAFormIconDashboard.CURRENT.id == this.id) {
			CODAFormIconDashboard.CURRENT = undefined;
		}
		KSystem.removeTimer(this.scrolltimer);
		CODAIconDashboard.prototype.destroy.call(this);
	};
	
	CODAFormIconDashboard.prototype.resize = function() {
		CODADashboard.prototype.resize.call(this);
		CODAFormIconDashboard.wall(this, this.columns);
	};

	CODAFormIconDashboard.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAFormIconDashboard.prototype.visible = function() {
		if (this.display) {
			return;
		}
		
		CODAFormIconDashboard.CURRENT = this;
		CODAFormIconDashboard.wall(this, this.columns);

		var already = this.display;
		KPanel.prototype.visible.call(this);
		this.setStyle({
			width: (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN - 25) + 'px',
			height: (KDOM.getBrowserHeight() - 85) + 'px'
		});
		this.getShell().onFormOpen(this.getParent('KFloatable'));
		if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			var self = this;
			KSystem.addTimer(function() {
				self.scrolltimer = KSystem.addTimer('CODAFormIconDashboard.scrollListener(\'' + self.id + '\')', 500, true);
			}, 1000);
		}		

	};

	CODAFormIconDashboard.prototype.draw = function() {
		var refresh = this.refreshing;
		this.refreshing = false;
		CODAIconDashboard.prototype.draw.call(this);
		this.content.removeChild(this.margin);
		delete this.margin;

		if (!!refresh) {
			CODAFormIconDashboard.wall(this, this.columns);
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 0
				},
				go : {
					alpha : 1
				},
				applyToElement : true
			});
		}
		else {
			this.makeHelp();
			this.makeActions();
			if (!!this.fieldConfig) {
				CODAGenericShell.applyButtonConfiguration(this.fieldConfig, this.className);
			}
		}
	};

	CODAIconDashboard.prototype.refresh = function() {
		if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			this.FORM_OPENED = undefined;
			this.SELECTED = undefined;
			this.page = 0;
			delete this.elements;
			this.elements = undefined;
			this.nomore = false;
			this.refreshing = true;
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 750,
				from : {
					alpha : 1
				},
				go : {
					alpha : 0
				},
				onComplete : function(w, t) {
					for ( var elementKey in w.childWidgets()) {
						if (w.childWidget(elementKey) instanceof CODAIcon) {
							w.removeChild(w.childWidget(elementKey));
						}
					}
					w.load();
					if (!w.scrolltimer && !!w.options.links && !!w.options.links.feed && !!w.options.links.feed.href) {
						KSystem.addTimer(function() {
							w.scrolltimer = KSystem.addTimer('CODAFormIconDashboard.scrollListener(\'' + w.id + '\')', 500, true);
						}, 1000);
					}
				},
				applyToElement : true
			});
		}
	};

	CODAFormIconDashboard.prototype.closeMe = function(new_form, config) {
		var form = this.FORM_OPENED;
		if (!!form) {
			form.setStyle({
				margin : 0
			});
			KEffects.addEffect(form, {
				f : KEffects.easeOutExpo,
				type : 'resize',
				duration : 500,
				lock : {
					width : true
				},
				go : {
					height : 0
				},
				from : {
					height : form.getRootHeight()
				},
				onComplete : function(w, t) {
					var dash = form.parent;
					dash.removeChild(form);
					dash.FORM_OPENED = undefined;
					if (!!new_form) {
						dash.getShell().makeListForm(new_form, config);
					}
					else {
						if (!!dash.SELECTED) {
							dash.SELECTED.setStyle({
								outlineWidth : '0px'
							});
							dash.SELECTED.removeCSSClass('CODAFormIconSelected');
							dash.SELECTED = undefined;
						}						
					}
				}
			});
		}
	};

	CODAFormIconDashboard.iconClicked = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (!!widget.parent.SELECTED) {
			widget.parent.SELECTED.setStyle({
				outlineWidth : '0px'
			});
			widget.parent.SELECTED.removeCSSClass('CODAFormIconSelected');
			if (widget.parent.SELECTED.id == widget.id) {
				return;				
			}
		}
		widget.setStyle({
			outlineWidth : (Math.ceil(widget.pad) + 1) + 'px'
		});
		widget.parent.SELECTED = widget;
		widget.parent.SELECTED.addCSSClass('CODAFormIconSelected');
	};

	CODAFormIconDashboard.wall = function(p, columns) {
		columns = columns || 4;
		var nc = columns || 4;
		var gw = (!!p.getParent().data.params.width ? p.getParent().data.params.width : p.getParent().getRootWidth()) - 40;
		var fp = KDOM.getBrowserWidth() / 1710 * 0.75;
		var pd = Math.round(fp * 20);
		var fs = Math.round(fp * 100);
		var iw = Math.round(fp * 250);

		nc = 0;
		for (; gw >=  iw * (nc + 1); ) {
			nc++;
		}
		p.columns = nc;
		iw -= Math.ceil(10 / nc);

		p.setStyle({
			width : (gw) + 'px'
		}, p.content);

		var tab = 1;
		for (var c in p.childWidgets()) {
			var w = p.childWidget(c);
			if (!!w.pad) {
				continue;
			}
			w.pad = pd;

			if (w instanceof CODAIcon) {
				w.h3.innerHTML = '<div>' + w.h3.innerHTML + '</div>';
				KDOM.addEventListener(w.h3, 'click', CODAFormIconDashboard.iconClicked);
				KDOM.addEventListener(w.icon, 'click', CODAFormIconDashboard.iconClicked);

				w.setStyle({
					display : 'inline-block',
					padding: pd + 'px ' + pd + 'px ' + Math.round(pd / 3) + 'px',
					width : (iw - 4 * pd) + 'px',
					margin: Math.round(pd) + 'px',
					fontSize :  fs + '%'
				});
			}
		}
	};

	CODAFormIconDashboard.prototype.exportList = function() {
	};

	CODAFormIconDashboard.prototype.addItem = function() {
	};

	CODAFormIconDashboard.prototype.addActions = function() {
	};

	CODAFormIconDashboard.prototype.makeActions = function() {
		var shell = this.getShell();
		var self = this;

		if (!!this.options.filter_input) {
			this.addFilter( function(parent) {
				var filterInput = new KInput(parent, '', 'pages-main-search');
				{
					filterInput.input.placeholder = gettext('$CODA_LIST_FILTER');
					filterInput.addEventListener('keyup', function(event) {
						event = KDOM.getEvent(event);

						if (event.keyCode == 13) {
							self.filterResults(filterInput.getValue(), filterInput.index);
							parent.onSelection(filterInput.getValue());
						}
						else if (event.keyCode == 27) {
							parent.onClose();
						}
					});
				}
				return filterInput;
			});
		}

		if (!!this.options.export_button) {
			var exportBt = new KButton(shell, '<i class="fa fa-share-square-o"></i> ' + gettext('$CODA_LIST_EXPORT_BUTTON_' + this.className.toUpperCase()), 'export_button', function(event) {
				KDOM.stopEvent(event);
				self.exportList();
			});
			this.addAction(exportBt);
		}
		if (!!this.options.add_button) {
			var addBt = new KButton(shell, gettext('$CODA_LIST_ADD_BUTTON_' + this.className.toUpperCase()), 'add_button', function(event) {
				KDOM.stopEvent(event);
				self.addItem();
			});
			this.addAction(addBt);
		}

		this.addActions();
	};

	CODAFormIconDashboard.prototype.execFilter = function(value, label, index) {
		var filterpanel = new KPanel(this);
		{
			filterpanel.addCSSClass('CODAToolbarFilters ' + filterpanel.className + 'ToolbarFilters');
			filterpanel.content.innerHTML = '<span>' + (!!label ? label : value) + '</span>';
			var removebt = window.document.createElement('button');
			KDOM.addEventListener(removebt, 'click', function(event) {
				filterpanel.parent.clearFilter(value);
				filterpanel.onClose();
			});
			filterpanel.index = index;
			filterpanel.content.appendChild(removebt);
			filterpanel.onClose = function () {
				KEffects.addEffect(this,  {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 300,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					},
					onComplete : function(w, t) {
						filterpanel.parent.removeChild(filterpanel);
					}
				});
			};
		}
		filterpanel.visible = function() {
			if (!!this.display) {
				return;
			}
			this.panel.style.opacity = '0';
			KPanel.prototype.visible.call(this);
			KEffects.addEffect(this,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				}
			});
		};

		this.appendChild(filterpanel, this.title);
		filterpanel.go();
		this.filterResults(value, index);
	};

	CODAFormIconDashboard.prototype.clearFilter = function(value) {
		KSystem.removeTimer(this.scrolltimer);
		this.scrolltimer = undefined;
		if (!!value) {
			for (var v in this.filterValue) {
				if (!this.filterValue[v]) {
					continue;
				}
				this.filterValue[v] = this.filterValue[v].replace('|' + value, '').replace(value + '|', '').replace(value, '').trim();
				if (this.filterValue[v].length == 0 || this.filterValue[v] == '|') {
					this.filterValue[v] = undefined;
				}
			}
		}
		else {
			this.filterValue = [];
		}
		this.page = 0;
		this.refresh();
	};

	CODAFormIconDashboard.prototype.filterResults = function(value, index) {
		if (this.filterFields !== null && this.filterFields.length > index) {
			KSystem.removeTimer(this.scrolltimer);
			this.scrolltimer = undefined;
			this.closeMe();
			if (!!this.filterValue[index] && this.filterValue[index] !== '') {
				this.filterValue[index] += '|' + value;
			}
			else {
				this.filterValue[index] = value;
			}
			this.page = 0;
			this.refresh();
		}
	};

	CODAFormIconDashboard.prototype.addFilter = function(input_creator) {
		input_creator._filtered = this;
		if (this.filteractivator == undefined) {
			if (!this.title) {
				this.setTitle('');
			}
			this.filteractivator = new KButton(this, '', 'add-to-list', function(event) {
				KDOM.stopEvent(event);
				event = KDOM.getEvent(event);
				var widget = KDOM.getEventWidget(event);
				if (!!widget.filterpanel) {
					widget.filterpanel.onClose();
					return;
				}

				widget.filterpanel = new KPanel(widget.parent);
				{
					widget.filterpanel.addCSSClass('CODAToolbarFilters ' + widget.filterpanel.className + 'ToolbarFilters');
					widget.filterpanel.onSelection = function (value, label) {
						widget.filterpanel.removeAllChildren();
						widget.filterpanel.content.innerHTML = '<span>' + (!!label ? label : value) + '</span>';
						var removebt = window.document.createElement('button');
						KDOM.addEventListener(removebt, 'click', function(event) {
							widget.parent.clearFilter(value);
							KDOM.getEventWidget(event).onClose(true);
						});
						widget.filterpanel.content.appendChild(removebt);
						widget.filterpanel = undefined;
					};					
					widget.filterpanel.onClose = function (notFilterPanel) {
						KEffects.addEffect(this,  {
							f : KEffects.easeOutExpo,
							type : 'fade',
							duration : 300,
							go : {
								alpha : 0
							},
							from : {
								alpha : 1
							},
							onComplete : function(w, t) {
								widget.parent.removeChild(w);
								if (!notFilterPanel) {
									delete widget.filterpanel;
									widget.filterpanel = undefined;
								}
							}
						});
					};
				}
				widget.filterpanel.visible = function() {
					if (!!this.display) {
						return;
					}
					this.panel.style.opacity = '0';
					KPanel.prototype.visible.call(this);
					KEffects.addEffect(this,  {
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 500,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					});
				};

				for (var f in widget.parent.filters) {
					var callback = widget.parent.filters[f];
					var element = callback(widget.filterpanel);
					element.index = f;
					widget.filterpanel.appendChild(element);
				}

				widget.parent.appendChild(widget.filterpanel, widget.parent.title);
				widget.filterpanel.go();

				var first = widget.filterpanel.childAt(0);
				if (first instanceof KAbstractInput) {
					KSystem.addTimer(function() {
						first.focus();
					}, 200);
				}
			});
			this.filteractivator.addCSSClass('CODAFilterButton');
			this.filteractivator.setText('<i class="fa fa-filter"></i>', true);
			var params = {
				text : gettext('$CODA_LIST_FILTER_TIP'),
				delay : 300,
				gravity : {
					x : 'right',
					y : 'top'
				},
				offsetX : 35,
				offsetY : 0,
				cssClass : 'CODAInputBaloon CODAIconBaloon'
			};
			KBaloon.make(this.filteractivator, params);
			this.appendChild(this.filteractivator, this.title);
		}
		if (this.filters == undefined) {
			this.filters = [];
		}
		this.filters.push(input_creator);
	};

	CODAFormIconDashboard.prototype.addAction = function(button) {
		button.hash = '/' + this.className + '/' + button.inputID;
		button.data = {
			tween : {
				show : [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 750,
					lock : {
						x : true
					},
					go : {
						y : 0
					},
					from : {
						y : -40
					}
				}, {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}],
				hide : [{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}]
			}
		}
		button.panel.style.opacity = '0';
		this.actions.push(button);
		this.getShell().addToolbarButton(button, 0, 'right', true);
	};

	CODAFormIconDashboard.scrollListener = function(id) {
		var self = Kinky.getWidget(id);
		if (!!self.nomore) {
			KSystem.removeTimer(self.scrolltimer);
			self.scrolltimer = undefined;
			return;
		}
		var scroll = self.panel.scrollTop;
		var offset = (self.content.offsetHeight + 70) - self.panel.offsetHeight;

		if(scroll > offset - 40) {
			self.gotoPage(self.page + 1);
		}
	}

	CODAFormIconDashboard.CURRENT = undefined;
	
	KSystem.included('CODAFormIconDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAForm(parent, href, layout, parentHref) {
	if (parent != null) {
		KLayeredPanel.call(this, parent);
		this.layout = KLayeredPanel.TABS;
		this.target = {
			href : href,
			layout : layout,
			parent : parentHref
		};
		this.childForms = {};
		this.schemas = {};
		this.processing = 1;

		this.canAddWidget = CODAForm.hasScope('add_widget');
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.moreOptionsButton = false;

		this.hasChildrenPanel = true;
		this.config = {
			href : (!!href ? href : null)
		};

		this.messages = window.document.createElement('div');
		this.messages.innerHTML = '&nbsp;';
		this.setStyle({
			opacity : 0
		}, this.messages);
		this.panel.appendChild(this.messages);

		this.nillable = false;
		this.defs = {};
		this.headers = {};
		this.cur_width = 0;

		this.actions = [];
		this.single = false;
		this.helpURL = CODAGenericShell.generateHelpURL(this);

		var forms = undefined;
		if((forms = KCache.restore('/__tweaks')) != undefined) {
			if (!!forms[this.className]) {
				this.fieldConfig = forms[this.className];
			}
		}
	}
}

KSystem.include([
	'KButton',
	'KInput',
	'KCheckBox',
	'KRadioButton',
	'KTextArea',
	'KCombo',
	'KSelect',
	'KPanel',
	'KButton',
	'KHidden',
	'KStatic',
	'KImageUpload',
	'CODAMediaList',
	'KJSONText',
	'KFileDropPanel',
	'KRichText',
	'KPanel',
	'KLayeredPanel'
], function() {
	CODAForm.prototype = new KLayeredPanel();
	CODAForm.prototype.constructor = CODAForm;

	CODAForm.prototype.refresh = function() {
		if (!!this.getParent('CODAFormIconDashboard')) {
			var list = this.getParent('CODAFormIconDashboard');
			var temp = list.getIconFor(this.target.data);
			list.SELECTED.options = temp.options;
			temp.destroy();
			delete temp;
			list.SELECTED.refresh();
		}
		else {
			var dialog = this.getParent('KFloatable');
			var dispatched = dialog._url;
			dialog.closeMe();
			KSystem.addTimer(function() {
				KBreadcrumb.dispatchURL({
					hash : dispatched
				});
			}, 750);
		}
	};

	CODAForm.prototype.load = function() {
		if (!!this.config && !!this.config.href && !this.target.data) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, this.headers);
			this.kinky.get(this, (this.config.href.indexOf(':') == -1 ? 'rest:' : '') + this.config.href, {}, headers);
		}
		else if (!!this.target.data) {
			this.onLoad(this.target.data);
		}
		else {
			this.onLoad();
		}
	};

	CODAForm.prototype.refreshData = function() {
		this.getParent('KFloatable');
	};

	CODAForm.prototype.setData = function(data) {
		if (!!data) {
			this.target._id = data.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAForm.prototype.onLoad = function(data, request) {
		this.setData(data);
		this.loadForm();
	};

	CODAForm.prototype.onLoadSchema = function(data, request) {
		data = { schema : data };

		if (request.service == this.config.schema) {
			this.rootSchema = data.schema;
		}
		this.schemas[request.service] = data.schema;
		if (!!request.action) {
			KCache.commit(request.service, data);
		}
		this.loadForm();
	};

	CODAForm.prototype.loadForm = function() {
		this.draw();
	};

	CODAForm.prototype.onNotFound = CODAForm.prototype.onNoContent = function(data, request) {
		this.activate();
	};

	CODAForm.prototype.onUnauthorized = function(data) {

	};

	CODAForm.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		for (var c in this.childWidget(this.selected).childWidgets()) {
			var child = this.childWidget(this.selected).childWidget(c);
			if (child instanceof KAbstractInput && !(child instanceof KStatic)) {
				KSystem.addTimer(function() {
					child.focus();
				}, 150);
				break;
			}
		}
	};

	CODAForm.prototype.visibleTab = function(child) {
		this.focus();
		if (!!child && !!child.onVisibleTab) {
			child.onVisibleTab();
		}
	};

	CODAForm.prototype.visible = function(tween) {
		if (this.display) {
			return;
		}
		KLayeredPanel.prototype.visible.call(this);

		if (this.minimized) {
			this.setStyle({
				height : (this.getParent('KFloatable').height - 80) + 'px'
			}, KWidget.CONTENT_DIV);
		}
		else {
			this.setStyle({
				height : (this.getParent('KFloatable').height - 125) + 'px'
			}, KWidget.CONTENT_DIV);
		}
		if (!!this.header && !!this.messages) {
			this.panel.insertBefore(this.header, this.messages);
		}

		var width = 0;
		for ( var c in this.childWidgets()) {
			if (!(this.childWidget(c) instanceof KButton)) {
				var p = this.childWidget(c);

				if (this.minimized) {
					p.setStyle({
						height : (this.getParent('KFloatable').height - 136) + 'px'
					});
				}
				else {
					p.setStyle({
						height : (this.getParent('KFloatable').height - 199) + 'px'
					});

					p._visible = p.visible;
					p.visible = function() {
						this._visible();
						var cur_width = this.getWidth();
						var floatable = this.getParent('KFloatable');

						if (cur_width != 0 && this.parent.panel.className.indexOf('CODAMiddleForm') == -1 && this.parent.panel.className.indexOf('CODAToolsDialog') == -1) {
							floatable.setStyle({
								width : (cur_width + (!!p.parent.single ? 52 : 230))  + 'px'
							});
							this.getShell().onFormOpen(this.getParent('KFloatable'));
						}
					};
				}
			}
		}

	};

	CODAForm.prototype.listform_visible = function(tween) {
		if (this.display) {
			return;
		}

		this.setStyle({
			width : (this.getParent('CODAFormIconDashboard').getRootWidth() - 45) + 'px',
			height : 0,
			margin : 0
		});

		var form = this;
		var cur_height = 0;
		for ( var c in this.childWidgets()) {
			if (!(this.childWidget(c) instanceof KButton)) {
				var p = this.childWidget(c);
				p.setStyle({
					overflow : 'visible'
				});
				if (cur_height == 0) {
					cur_height = p.getHeight();
				}
				p._visible = p.visible;
				p.visible = function() {
					this._visible();
					var size = {
						height : this.getHeight()
					};

					this.parent.setStyle({
						height : (size.height + 100) + 'px'
					}, KWidget.CONTENT_DIV);

					KEffects.addEffect(form, {
						f : KEffects.easeOutExpo,
						type : 'resize',
						duration : 500,
						lock : {
							width : true
						},
						from : {
							height : form.getRootHeight()
						},
						go : {
							height : size.height + 150
						}
					});
				}
				p.addResizeListener(function(w, size) {
					if (w.parent.selected != w.hash ) {
						return;
					}
					w.parent.setStyle({
						height : (size.height + 100) + 'px'
					}, KWidget.CONTENT_DIV);

					KEffects.addEffect(form, {
						f : KEffects.easeOutExpo,
						type : 'resize',
						duration : 500,
						lock : {
							width : true
						},
						from : {
							height : form.getRootHeight()
						},
						go : {
							height : size.height + 150
						}
					});

				});
			}
		}

		KLayeredPanel.prototype.visible.call(this, tween);
		this.setStyle({
			height : (this.getParent('KFloatable').height - 350) + 'px'
		}, KWidget.CONTENT_DIV);

		this.panel.insertBefore(this.header, this.messages);
	};

	CODAForm.prototype.minimized_draw = function() {
		this.addCSSClass('CODAFormMinimized');
		this.addCSSClass('CODAFormContentMinimized', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.minimized = true;

		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.hash = '/buttons/confirm';
		go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true)
		go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(go);

		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', CODAForm.closeMe);
		nogo.hash = '/buttons/cancel';
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAFormButtonNoGo CODAFormButton');
		this.appendChild(nogo);

		if (this.moreOptionsButton && !!this.target && !!this.target.href) {
			var advanced = new KButton(this, gettext('$MORE_OPTIONS'), 'more', function(event) {
				var form = KDOM.getEventWidget(event).parent;
				KBreadcrumb.dispatchURL({
					hash : '/edit-full' + form.target.href
				});
				form.parent.closeMe();

			});
			advanced.setText(gettext('$MORE_OPTIONS'), true);
			advanced.hash = '/more-properties';
			advanced.addCSSClass('CODAFormMinimizedButtonAdvanced CODAFormButton');
			this.appendChild(advanced);
		}

		this.addMinimizedPanel();
		KLayeredPanel.prototype.draw.call(this);
		this.makeHelp();
	};

	CODAForm.prototype.draw = function() {
		this.addCSSClass('CODAForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.hash = '/buttons/confirm';
		go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
		go.addCSSClass('CODAFormButtonGo CODAFormButton');;
		this.appendChild(go);

		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', CODAForm.closeMe);
		nogo.hash = '/buttons/cancel';
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAFormButtonNoGo CODAFormButton');
		this.appendChild(nogo);

		this.addPanels();
		KLayeredPanel.prototype.draw.call(this);
		this.makeHelp();
		if (!!this.msg) {
			this.showMessage(this.msg);
			this.msg = null;
		}
		if (!!this.single) {
			this.addCSSClass('CODAFormSingleContent', this.content);
			this.header.style.display = 'none';
		}
	};

	CODAForm.prototype.hideConfirm = function() {
		this.childWidget('/buttons/confirm').setStyle({
			display : 'none'
		});
		if (this.className.indexOf('Wizard') == -1) {
			this.childWidget('/buttons/cancel').setStyle({
				right : this.minimized ? '21px' : '20px'
			});
		}
	};

	CODAForm.prototype.hideCancel = function() {
		this.childWidget('/buttons/cancel').setStyle({
			display : 'none'
		});
	};

	CODAForm.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAForm.prototype.showMessage = function(message, persistent) {
		if (!!this.timer) {
			KSystem.removeTimer(this.timer);
			this.timer = null;
		}
		if (persistent) {
			this.disable(true);
		}
		else {
			this.enable();
			this.messages.innerHTML = message;
			this.removeCSSClass('CODAFormErrorMessages', this.messages);
			this.addCSSClass('CODAFormSuccessMessages', this.messages);

			this.setStyle({
				display : 'block',
				position: 'fixed',
				bottom : '50px',
				right: '360px'
			}, this.messages);

			KEffects.addEffect(this.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				applyToElement : true,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onComplete : function(w, t) {
					if (!!w && !persistent) {
						w.timer = KSystem.addTimer('CODAForm.hideMessage(\'' + w.id + '\')', 3000);
					}
				}
			});
		}

	};

	CODAForm.hideMessage = function(id) {
		var form = Kinky.getWidget(id);
		if (!form) {
			return;
		}
		form.timer = null;
		if (!!form) {
			form.enable();
			KEffects.addEffect(form.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (!!w) {
						w.messages.style.display = 'none';
						w.messages.innerHTML = '&nbsp;';
					}
				}
			});
		}
	};

	CODAForm.prototype.addPanel = function(element, title, isDefault) {
		var extra = (this.single ? 135 : 335);
		KLayeredPanel.prototype.addPanel.call(this, element, title, isDefault);
		if (this.state != 'listform') {
			element.setStyle({
				overflowY : 'auto'
			});
		}

		if (this.panel.className.indexOf('CODAMiddleForm') != -1) {
			var usablew = KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.MIDDLE_DIALOG_MARGIN;

			if (CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH < usablew) {
				element.setStyle({
					width : (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN - (usablew - CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH) - extra + (element instanceof CODAList ? 27 : 27)) + 'px'
				});
			}
			else {
				element.setStyle({
					width : (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN -	extra) + 'px'
				});
			}
		}
		else if (this.panel.className.indexOf('CODASideForm') != -1) {
		}

		if (!!this.fieldConfig) {
			this.applyFieldConfiguration(element);
		}
	};

	CODAForm.prototype.applyFieldConfiguration = function(panel) {
		CODAGenericShell.applyFieldConfiguration(panel, this.fieldConfig, this.className);
	};

	CODAForm.prototype.onDestroy = function() {
		this.cleanCache();
	};

	CODAForm.prototype.onClose = function() {
		if (!this.minimized) {
			for (var a in this.actions) {
				this.getShell().removeToolbarButton(this.actions[a]);
			}
		}
		KBreadcrumb.dispatchEvent(null, {
			action : '/close/form' + this.config.href
		});
	};

	CODAForm.prototype.addSeparator = function(text, panel) {
		var c = (panel.activated() ? panel.content : panel.container);
		var h4 = window.document.createElement('h4');
		h4.innerHTML = text;
		h4.className = ' CODAFormSeparator ';
		c.appendChild(h4);
	};

	CODAForm.prototype.addFilter = function(input) {
		input._filtered = this;
	};

	CODAForm.prototype.addAction = function(button) {
		button.hash = '/' + this.className + '/' + button.inputID;
		button.data = {
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		}
		button.panel.style.opacity = '0';
		this.actions.push(button);
		this.getShell().addToolbarButton(button, 0, 'right', true);
	};

	CODAForm.prototype.listform_onClose = function(tween) {
	};

	CODAForm.prototype.addPanels = function() {
	};

	CODAForm.prototype.addMinimizedPanel = function() {
	};

	CODAForm.addWizardPanel = function(parent)  {
	};

	CODAForm.prototype.onGet = function(data, request) {
	};

	CODAForm.prototype.onError = function(data, request) {
		this.enable();
		var msg = undefined;
		eval('msg = ' + gettext('$CONFIRM_OPERATION_ERROR').replace('[NUMBER]', data.error_code));
		this.showMessage(msg);
		this.removeCSSClass('CODAFormSuccessMessages', this.messages);
		this.addCSSClass('CODAFormErrorMessages', this.messages);
	};

	CODAForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAForm.prototype.onPost = CODAForm.prototype.onCreated = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAForm.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAForm.prototype.instantiateDefaults = function(params) {
		this.setDefaults(params);
	};

	CODAForm.prototype.setDefaults = function(params) {
	};

	CODAForm.prototype.enable = function() {
		if (this.loader_t !== undefined) {
			KSystem.removeTimer(this.loader_t);
			this.loader_t = undefined;
			return;
		}
		KLayeredPanel.prototype.enable.call(this);
	};

	CODAForm.prototype.disable = function(communicating) {
		if (!!communicating) {
			if (this.loader_t === undefined) {
				var self = this;
				this.loader_t = KSystem.addTimer(function() {
					self.loader_t = undefined;
					KLayeredPanel.prototype.disable.call(self);
				}, 250);
			}
		}
		else {
			if (this.loader_t !== undefined) {
				KSystem.removeTimer(this.loader_t);
				this.loader_t = undefined;
			}
			KLayeredPanel.prototype.disable.call(this);
		}
	};

	CODAForm.prototype.instantiateParams = function(params) {
		var doSubmit = true;

		for ( var c in this.childWidgets()) {
			var panel = this.childWidget(c);
			if (panel instanceof KPanel) {
				if (!!panel.getRequest) {
					var value = panel.getRequest();
					if (!!value) {
						CODAForm.createObject(params, panel.fieldID.split('.'));
						eval('params.' + panel.fieldID + ' = value;');
					}
				}
				else {
					for ( var i in panel.childWidgets()) {
						var input = panel.childWidget(i);

						if ((input instanceof KAbstractInput || input instanceof KHidden) && !(input instanceof KButton)) {
							var value = input.getValue();
							var validation = input.validate();
							var objpath = input.getInputID();
							CODAForm.createObject(params, objpath.split('.'));
							var oldvalue = null;
							eval('oldvalue = params.' + objpath + ' ;');

							if (validation != true) {
								if (doSubmit) {
									input.focus();
								}
								input.errorArea.innerHTML = validation;
								doSubmit = false;
							}
							else if (!!value && value != '') {
								eval('params.' + objpath + ' = value');
							}
							else if(!!oldvalue && oldvalue != '') {
								eval('params.' + objpath + ' = null');
							}

							if (input.getMetadata) {
								var meta = input.getMetadata();
								if (!!meta) {
									eval('params.' + objpath + '_metadata = meta');
								}
							}
						}
						else if (!!input.parent.save) {
							var value = input.parent.save();

							for ( var c in value) {
								var oldvalue = null;
								eval('oldvalue = params.' + c + ' ;');

								if ((!!value[c] && value[c] != '')) {
									CODAForm.createObject(params, c.split('.'));
									eval('params.' + c + ' = value[c]');
								}
								else if((!!oldvalue && oldvalue != '')) {
									eval('params.' + c + ' = null');
								}
							}
						}
					}
				}
			}
		}
		return doSubmit;
	};
	
	CODAForm.prototype.save = function(toSend) {
		this.disable(true);

		if (!!this.target.http && !!this.target.http.headers && !!this.target.http.headers['Content-Language']) {
			this.target.http.headers['Accept-Language'] = this.target.http.headers['Content-Language'];
		}

		var params = (!!toSend ? toSend : (!!this.target.data ? this.target.data : {}));
		var doSubmit = this.instantiateParams(params);
		delete params.http;
		this.instantiateDefaults(params);

		try {
			if (!!params && doSubmit) {
				var headers = { 'E-Tag' : '' + (new Date()).getTime() };
				headers = KSystem.merge(headers, this.config.headers || {});
				headers = KSystem.merge(headers, this.headers);
				headers['X-Fields-Nillable'] = '' + this.nillable;

				if (!!this.target.href) {
					this.kinky.put(this, 'rest:' + this.target.href, params, headers);
				}
				else {
					this.kinky.post(this, 'rest:' + this.target.collection, params, headers);
				}
			}
			else if(!doSubmit){
				this.enable();
				CODAForm.hideMessage(this.id);
			}
		}
		catch (e) {

		}
	};

	CODAForm.closeMe = function(event) {
		KDOM.stopEvent(event);
		var form = KDOM.getEventWidget(event).parent;
		form.parent.closeMe();
	};

	CODAForm.createObject = function(params, objpath) {
		if (typeof objpath == 'string') {
			objpath = objpath.split('.');
		}
		if (objpath.length == 0) {
			return;
		}
		var exists = false;
		var arrindex = 0;
		if ((arrindex = objpath[0].indexOf('[')) != -1) {
			eval('exists = !!params.' + objpath[0].substr(0, arrindex) + ';');
			if (!exists) {
				eval('params.' + objpath[0].substr(0, arrindex) + ' = new Array();');
			}
		}

		eval('exists = !!params.' + objpath[0] + ';');
		if (!exists && objpath.length > 1) {
			eval('params.' + objpath[0] + ' = {};');
		}
		var p = null;
		eval('p = params.' + objpath[0] + ';');
		CODAForm.createObject(p, objpath.slice(1));
	};

	CODAForm.sendRest = function(event) {
		var widget = KDOM.getEventWidget(event, 'CODAForm');
		widget.showMessage( '<i class="fa-spinner fa-spin"></i> ' + gettext('$CODA_WORKING'), true);
		widget.save();
	};

	CODAForm.instantiate = function(params, field, node) {
	};

	CODAForm.hasScope = function(scope) {
		var scopes = KCache.restore('/admin-scopes');
		if (!!scopes) {
			for ( var s in scopes.elements) {
				if (scopes.elements[s].id == scope) {
					return true;
				}
			}
		}
		return false;
	};

	CODAForm.getHelpOptions = function() {
		return KSystem.clone(CODAForm.HELP_OPTIONS);
	};

	CODAForm.HELP_OPTIONS = {
		gravity : {
			x : 'right',
			y : 'middle'
		},
		max : {
			width : 450
		},
		offsetX : 20,
		offsetY : 0,
		cssClass : 'CODAInputBaloon'
	};

	KWidget.STATE_FUNCTIONS .push('onClose');

	KAbstractInput.onEnterPressed = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;

		if (keyCode != 13) {
			return;
		}
		var widget = KDOM.getEventWidget(event);
		if (!widget) {
			return;
		}
		if (widget.actOnEnter()) {
			KDOM.stopEvent(event);

			var form = widget.getParent('CODAForm');
			if (!!form) {
				CODAForm.sendRest(event);
			}
			else {
				CODAWizard.next(event);
			}
		}
	};

	KSystem.included('CODAForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAGenericShell(parent) {
	if (parent != null) {
		Kinky.LOADER_IMAGE = '';
		KShell.call(this, parent);
		this.addWindowResizeListener(CODAGenericShell.resize);
		this.addCSSClass('CODAGenericShell');

		this.loader.removeChild(this.loader.childNodes[0]);
		this.messages = window.document.createElement('div');
		{
			this.messages.innerHTML = gettext('$CODA_WORKING');
			this.addCSSClass('CODAMessages', this.messages);
		}
		this.loader.appendChild(this.messages);

		this.header = new CODAToolbar(this);
		this.appendChild(this.header);

		this.footer = new CODAFooter(this);
		this.appendChild(this.footer, this.panel);

		var logout = new KButton(this, gettext('$CODA_SHELL_LOGOUT_BT'), 'logout', function(event) {
			KDOM.stopEvent(event);
			CODAGenericShell.logout();
		});
		this.addToolbarButton(logout, 'EndSession', 'right', '');

		this.notifybutton = new KButton(this, gettext('$CODA_SHELL_NOTIFICATIONS_BT'), 'notifications', function(event) {
			KDOM.stopEvent(event);
			KDOM.getEventWidget(event).getShell().showNotifications();
		});
		this.addToolbarButton(this.notifybutton, 5, 'right', '');

		var title = '';
		if (this.className.indexOf('CODAMyShell') != -1) {
			title = gettext('$CODA_SHELL_APPS_BT');
		}
		else {
			title = (!!Kinky.getLoggedUser().mug ? '<img style="width: 30px; height: 30px; "src="' + Kinky.getLoggedUser().mug + '"></img> ' : '<i class="fa fa-user"></i> ') + Kinky.getLoggedUser().name.substr(0, Kinky.getLoggedUser().name.indexOf(' '));
		} 
		var user = new KButton(this, title, 'user', function(event) {
			KDOM.stopEvent(event);
			var shell = KDOM.getEventWidget(event).getShell();
			var location = undefined;

			if (shell.className.indexOf('CODAMyShell') != -1) {
				location = CODA_VERSION + '/apps/';
				window.top.CODA_NAVIGATION_FROM = -1;
			}
			else {
				location = CODA_VERSION + '/my/';
				window.top.CODA_NAVIGATION_FROM = 1;
			}

			if (!!shell.dashboard) {
				if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined) {
					CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
					CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
				}
				if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
					CODAGenericShell.CURRENT_SIDE_DIALOG.closeMe();
					CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
				}
				if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
					CODAGenericShell.CURRENT_FORM_SWITCHING = true;

					var last = CODAGenericShell.CURRENT_BIG_SIDE_DIALOG;
					CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = undefined;
					last.closeMe();
				}

				KEffects.addEffect(shell.dashboard, [
				{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 350,
					gravity : {
						x : 'right'
					},
					lock : {
						y : true
					},
					go : {
						x : (window.top.CODA_NAVIGATION_FROM) * 410
					},
					from : {
						x : 0
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 350,
					applyToElement : true,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					},
					onComplete : function() {				
						window.document.location = location;
					}
				}]);
			}
			else {		
				window.document.location = location;
			}
		});
		this.addToolbarButton(user, 1, 'right', '');

		var contactbt = new KButton(this, gettext('$CODA_CONTACT_US'), 'contact', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/show-feedback-wizard'
			})
		});
		this.addBottomButton(contactbt, 5);

		this.back = new KButton(this, '', 'back', function(event) {
			KDOM.stopEvent(event);
			CODAGenericShell.fadeOut(KDOM.getEventWidget(event).parent);
		});
		this.back.setText(gettext('$CODA_SHELL_BACK_BT'), true);
		this.back.addCSSClass('CODAGenericShellBackButton');
		this.back.setStyle({
			height: (KDOM.getBrowserHeight() - 55) + 'px'
		}, this.back.input);
		KShell.prototype.appendChild.call(this, this.back);

		CODAGenericShell.LEFT_FRAME_MARGIN = this.LEFT_FRAME_MARGIN = 350;
		KFloatable.close = CODAGenericShell.closeFloatable;
		this.addLocationListener(CODAGenericShell.actions);

		this.notifications = undefined;
		this.hinted = undefined;

		this.module = this.className.replace('CODA', '').replace('Shell', '').toLowerCase();

		ga('create', window.top.google_analytics_code, window.top.google_analytics_domain);
		ga('send', 'pageview', window.document.location.pathname);
	}
}

KSystem.include([
	'KCalendar',
	'CODAToolbar', 
	'CODAFooter',
	'CODAWizard',
	'CODAList',
	'CODAMediaList',
	'CODAFeedbackWizard',
	'CODACloudBrowser',
	'CODAImageCrop',
	'KButton',
	'KShell',
	'KPanel', 
	'KImage',
	'KText',
	'HTMLEntities'
], function() {
	CODAGenericShell.prototype = new KShell();
	CODAGenericShell.prototype.constructor = CODAGenericShell;

	CODAGenericShell.prototype.focus = function(dispatcher) {
		if (dispatcher instanceof KFloatable) {
			CODA_FOCUSED_WINDOW = dispatcher;
		}
	};

	CODAGenericShell.prototype.blur = function(dispatcher) {
		if (dispatcher instanceof KFloatable) {
			if (!!CODA_FOCUSED_WINDOW && CODA_FOCUSED_WINDOW.id == dispatcher.id) {
				CODA_FOCUSED_WINDOW = undefined;
			}
		}
	};

	CODAGenericShell.prototype.visible = function() {
		var already = this.display;
		KShell.prototype.visible.call(this);
		if (already) {
			return;
		} 

		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990;
		var fs = Math.round(fp * 100);

		KCSS.setStyle({
			height: Math.round(fp * 64) + 'px',
			lineHeight: Math.round(fp * 44) + 'px',
			fontSize: Math.round(fp * 24) + 'px'
		}, [ this.title ]);

		KCSS.setStyle({
			fontSize: (fs * 1.5) + '%'
		}, [ this.back.panel ]);

		KEffects.addEffect(this, {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 600,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			}, 
			onComplete : function(widget, tween) {
				KDOM.fireEvent(widget.header.activator.input, 'click');
				if (!!widget.hinted) {
					widget.showHint();
				}
			}
		});
	};	

	CODAGenericShell.prototype.load = function() {
		if (Kinky.bunnyMan.loggedUser && !Kinky.bunnyMan.loggedUser.error) {
			this.default_onNoContent = this.onNoContent;
			this.onNoContent = this.generic_onNoContent;
			this.kinky.get(this, 'oauth:/applications/' + this.data.app_id, {}, { 'E-Tag' : '' + (new Date()).getTime(), 'Authorization' : 'OAuth2.0 ' + this.data.module_token }, 'onLoadApplicationData');
		}
		else {
			window.parent.document.location.href = '/1.0/login/';
		}
	};

	CODAGenericShell.prototype.onLoadApplicationData = function(data, request) {
		delete data.http;
		delete data.links;
		this.app_data = data;
		var _self = this;

		var tweak_locale = window.document.createElement('script');
		tweak_locale.charset = 'utf-8';
		tweak_locale.src =  '/tweak/' + this.module + '/locale/' + this.app_data.type + '.' + KLocale.LANG.toLowerCase() + '.js';
		window.document.getElementsByTagName('head')[0].appendChild(tweak_locale);

		var tweak_forms = window.document.createElement('script');
		tweak_forms.charset = 'utf-8';
		tweak_forms.src =  '/tweak/' + this.module + '/forms/' + this.app_data.type + '.js';
		window.document.getElementsByTagName('head')[0].appendChild(tweak_forms);

		var tweak_css = window.document.createElement('link');
		tweak_css.rel = 'stylesheet';
		tweak_css.type = 'text/css';
		tweak_css.href =  '/tweak/' + this.module + '/css/' + this.app_data.type + '.css';
		window.document.getElementsByTagName('head')[0].appendChild(tweak_css);

		this.onLoad();
	};

	CODAGenericShell.prototype.onUnauthorized = function(data, request) {
		window.parent.document.location = '/1.0/login/';
	};

	CODAGenericShell.prototype.disable = function() {
		if (!this.w_overlay) {
			this.w_overlay = window.document.createElement('div');
			this.w_overlay.className = this.className + "Overlay KWidgetDisableOverlay";
		}
		var width = Math.max(this.panel.offsetWidth, this.content.offsetWidth);
		KCSS.setStyle({
			position : 'absolute',
			top : '0',
			left : '0',
			width : (isNaN(width) ? KDOM.getBrowserWidth() : width) + 'px',
			height : (Math.max(this.panel.offsetHeight, this.content.offsetHeight) - 41) + 'px'
		}, [
		this.w_overlay
		]);
		this.panel.appendChild(this.w_overlay);
		KCSS.setStyle({
			opacity : '1'
		}, [
		this.w_overlay
		]);
	};


	CODAGenericShell.prototype.onLoad = function(data, request) {
		KCache.commit('/admin-scopes', Kinky.getLoggedUser().scopes);
		this.kinky.get(this, 'rest:/notifications?embed=elements(*(owner,sender))', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadNotifications');
	};

	CODAGenericShell.prototype.onNotFound = function(data, request) {
		if (/applications/.test(request.service)) {
			this.app_data = {};
			this.onLoad();
		}
		else if (/users|notifications/.test(request.service)) {
			this.onNoContent = this.default_onNoContent;
			this.loadShell();
		}
		else {
			this.draw();
		}
	};

	CODAGenericShell.prototype.onLoadNotifications = function(data, request) {
		this.notifications = data;
		this.onNoContent = this.default_onNoContent;
		this.loadShell();
	};

	CODAGenericShell.prototype.loadShell = function() {
		this.draw();
	};
	
	CODAGenericShell.prototype.generic_onNoContent = CODAGenericShell.prototype.onNoContent = function(data, request) {
		if (/applications/.test(request.service)) {
			this.app_data = {};
			this.onLoad();
		}
		else if (/users|notifications/.test(request.service)) {
			this.onNoContent = this.default_onNoContent;
			this.loadShell();
		}
		else {
			this.draw();
		}
	};

	CODAGenericShell.prototype.appendChild = function(element, childDiv) {
		if (element instanceof KButton) {
			KShell.prototype.appendChild.call(this, element, this.header);
			return;
		}

		if (element instanceof CODADashboard) {
			this.dashboard = element;
		}
		KShell.prototype.appendChild.call(this, element, childDiv);
	};

	CODAGenericShell.prototype.setTitle = function(text) {
		CODAGenericShell.pushHistory(text);
		var title = '';
		var h = CODAGenericShell.getHistory();
		for (var t in h) {
			title +=  ' / <a' + (t != h.length - 1 ? ' onclick="CODAGenericShell.popHistoryUntil(event)"' : '') + ' href="' + (t != h.length - 1 ? h[t].href : 'javascript:void(0)') + '" '  + (t == h.length - 1 ? (t == 1 ? ' class="app_name"' :' class="current"') : (t == 1 ? ' class="app_name_dim"' : '')) + '>' + h[t].title + '</a>';
		}
		KShell.prototype.setTitle.call(this, title, true);
		var help = window.document.createElement('a');
		{
			help.className = ' CODAShellHelpElement ';
			help.target = '_blank';
			help.href = this.helpURL || CODA_VERSION + '/docs/' + KLocale.LANG + '/' + this.module;
		}
		this.title.insertBefore(help, this.title.childNodes[0]);
		var params = {
			text : gettext('$CODA_FORM_HELP_TIP'),
			delay : 300,
			gravity : {
				x : 'right',
				y : 'top'
			},
			target : help,
			offsetParent : help,
			offsetX : 30,
			offsetY : 0,
			cssClass : 'CODAInputBaloon CODAIconBaloon'
		};
		KBaloon.make(this, params);
	};

	CODAGenericShell.prototype.removeToolbarButton = function(button) {
		this.header.removeToolbarButton(button);
	};
	
	CODAGenericShell.prototype.addToolbarButton = function(button, color, position, level) {
		this.header.addToolbarButton(button, color, position, level);
	};
		
	CODAGenericShell.prototype.addToolbarSeparator = function(position) {
		this.header.addToolbarSeparator(position);
	};

	CODAGenericShell.prototype.addBottomButton = function(button, color) {
		var gw = KDOM.getBrowserWidth() - 20;

		var fp = gw / 1610;
		var fs = Math.round(fp * 100);

		button.parent = this.footer;
		{
			button.setText(button.label, true);
			button.addCSSClass('CODAToolbarButton');
			button.addCSSClass('CODAFooterButton' + (color !== undefined && color !== null ? color : '1'));
			button.setStyle({
				marginRight: '0',
				fontSize :  fs + '%'
			});
		}
		this.footer.appendChild(button);
	};

	CODAGenericShell.closeFloatable = function(event) {
		var widget = KDOM.getEventWidget(event);
		if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined && widget.id == CODAGenericShell.CURRENT_SIDE_DIALOG.id) {
			CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
		}
		widget.closeMe();		
	};

	CODAGenericShell.prototype.makeListForm = function(form, config) {
		var dashboard = CODAFormIconDashboard.CURRENT;

		if (!!dashboard && !!dashboard.SELECTED) {
			var icon = dashboard.SELECTED;
			if (!!dashboard.FORM_OPENED) {
				dashboard.closeMe(form, config);
				return;
			}

			var icon_index = dashboard.indexOf(icon);
			var line = Math.floor(icon_index / dashboard.columns) + 1;
			var index = line * dashboard.columns - 1;

			if (index > dashboard.nChildren - 1) {
				index = dashboard.nChildren - 1;
			}
			form.parent = dashboard;
			form.state = 'listform';
			form.dispatchState('listform');
			dashboard.insertAfter(form, dashboard.childAt(index));

			var h2 = form.panel.getElementsByTagName('h2')[0];
			h2.style.display = 'none';

			dashboard.FORM_OPENED = form;

		}
	};	

	CODAGenericShell.prototype.makeWizard = function(title, forms, hash) {
		this.showOverlay();

		var configForm = null;
		if (title instanceof CODAWizard) {
			configForm = title;
		}
		else {
			configForm = new CODAWizard(this, forms);
			{
				configForm.hash = hash;
				configForm.setTitle(title);
			}
		}
		var dialog = null;

		if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined) {
			CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
			CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
		}
		if (CODAGenericShell.CURRENT_WIZARD_DIALOG !== undefined) {
			CODAGenericShell.CURRENT_WIZARD_DIALOG.closeMe();
			CODAGenericShell.CURRENT_WIZARD_DIALOG = undefined;
		}

		CODAGenericShell.CURRENT_WIZARD_DIALOG = dialog = KFloatable.make(configForm, {
			buttons : 'close',
			expandable : true,
			transition_wait_target : true,
			modal : false,
			width : 900
		});
		{
			dialog.closeMe = function() {
				CODAGenericShell.closeAllOthers(this);

				CODAGenericShell.CURRENT_WIZARD_DIALOG = undefined;
				configForm.getShell().hideOverlay();
				configForm.onClose();
				KFloatable.prototype.closeMe.call(this);
			};
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAFloatablePropDialog');
			dialog.addCSSClass('CODAVersionsFormDialog');
		}
		dialog._url = KBreadcrumb.getHash();
		KFloatable.show(dialog);
	};

	CODAGenericShell.prototype.makeWindow = function(form) {
		this.showOverlay();

		var dialog = KFloatable.make(form, {
			buttons : 'close',
			modal : true,
			width : 900,
			scale : {
				height : 0.85
			}
		});
		{
			dialog.closeMe = function() {
				form.onClose();
				KFloatable.prototype.closeMe.call(this);
			};
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAFloatablePropDialog');
			dialog.addCSSClass('CODAVersionsFormDialog');
		}
		dialog._url = KBreadcrumb.getHash();
		KFloatable.show(dialog);
	};

	CODAGenericShell.prototype.makeToolbarWindow = function(form, _options) {
		var pos = (!!_options && !!_options.x ? _options.x : Kinky.BROWSER_CLICK.x);
		var width = (!!_options && !!_options.width ? _options.width : 400);
		var left = pos - Math.round(width / 2);
		var top = (!!_options && !!_options.y ? _options.y + 26 : 47)

		var dialog = KFloatable.make(form, {
			buttons : 'close',
			modal : false,
			width : width,
			left : left,
			top : top,
			tween : {
				show : 	{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 850,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 1000,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}		
		});
		{
			dialog.closeMe = function() {
				form.onClose();
				KFloatable.prototype.closeMe.call(this);
			};
			dialog.addCSSClass('CODAToolbarDialog');
		}
		dialog._url = KBreadcrumb.getHash();
		{
			var pointerImg = window.document.createElement('i');
			pointerImg.className = ' CODAToobarDialogPointer fa fa-caret-up ';
			pointerImg.style.left = (!!_options && !!_options.pointer && !!_options.pointer.left ? _options.pointer.left + 'px' : (Math.round(width / 2) - 15) + 'px');
			dialog.panel.insertBefore(pointerImg, dialog.content.content);
		}

		KFloatable.show(dialog);
		return dialog;
	};

	CODAGenericShell.prototype.makeMiddleDialog = function(form, _options) {
		var options = KSystem.merge(KSystem.clone(CODAGenericShell.FORM_OPTIONS), _options || {});
		var dialog = null;
		var extramargin = CODAGenericShell.MIDDLE_DIALOG_MARGIN;
		var width = Math.min(CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH, KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.MIDDLE_DIALOG_MARGIN);

		if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined) {
			CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
			CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
		}
		if (CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG !== undefined) {
			CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.closeMe();
			CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = undefined;
		}
		if (options.modal) {
			if (CODAGenericShell.CURRENT_WIZARD_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_WIZARD_DIALOG.disable();
			}
			if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_BIG_SIDE_DIALOG.disable();
			}
			if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_SIDE_DIALOG.disable();
			}
		}
		form.addCSSClass('CODAMiddleForm');

		CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = dialog = KFloatable.make(form, {
				buttons : 'close',
				className : options.className,
				right : -width,
				top : 41,
				height : KDOM.getBrowserHeight() - 61,
				width: width,
				tween : {
					show : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : 0
						},
						from : {
							x : -width
						}
					},
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 850,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					}
					],
					hide : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : -width
						},
						from : {
							x : 0
						}
					}, 
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 1000,
						go : {
							alpha : 0
						},
						from : {
							alpha : 1
						}
					}
					]
				}
		});
		{
			dialog.closeMe = (!!options.callbacks && !!options.callbacks.close ? options.callbacks.close : function() {
				if (!!CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG && CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.id == this.id) {
					CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = undefined;
				}
				CODAGenericShell.closeAllOthers(this);

				var shell = form.getShell();
				form.onClose();
				KFloatable.prototype.closeMe.call(this);
				if (options.modal) {
					if (CODAGenericShell.CURRENT_WIZARD_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_WIZARD_DIALOG.enable();
					}
					if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_BIG_SIDE_DIALOG.enable();
					}
					if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_SIDE_DIALOG.enable();
					}
				}
			});
			dialog.addCSSClass('CODAFloatablePropDialog');
			dialog.addCSSClass('CODAWidgetDetailsDialog');
			dialog.modal = options.modal;
		}
		dialog._url = KBreadcrumb.getHash();
		KFloatable.show(dialog);
		return dialog;
	};

	CODAGenericShell.prototype.makeToolsDialog = function(form, _options) {
		var options = KSystem.merge(KSystem.clone(CODAGenericShell.FORM_OPTIONS), _options || {});
		var dialog = null;
		var extramargin = CODAGenericShell.BROWSER_DIALOG_MARGIN;
		var width = Math.min(CODAGenericShell.BROWSER_DIALOG_MAXWIDTH, KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.BROWSER_DIALOG_MARGIN);

		if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined) {
			CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
			CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
		}
		if (options.modal) {
			if (CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.disable();
			}
			if (CODAGenericShell.CURRENT_WIZARD_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_WIZARD_DIALOG.disable();
			}
			if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_BIG_SIDE_DIALOG.disable();
			}
			if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_SIDE_DIALOG.disable();
			}
		}
		form.addCSSClass('CODAToolsDialog');

		CODAGenericShell.CURRENT_BROWSER_DIALOG = dialog = KFloatable.make(form, {
				buttons : 'close',
				className : options.className,
				right : -width,
				top : 41,
				height : KDOM.getBrowserHeight() - 61,
				width: width,
				tween : {
					show : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : 0
						},
						from : {
							x : -width
						}
					},
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 850,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					}
					],
					hide : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : -width
						},
						from : {
							x : 0
						}
					}, 
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 1000,
						go : {
							alpha : 0
						},
						from : {
							alpha : 1
						}
					}
					]
				}
		});
		{
			dialog.closeMe = (!!options.callbacks && !!options.callbacks.close ? options.callbacks.close : function() {
				if (!!CODAGenericShell.CURRENT_BROWSER_DIALOG && CODAGenericShell.CURRENT_BROWSER_DIALOG.id == this.id) {
					CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
				}
				CODAGenericShell.closeAllOthers(this);

				var shell = form.getShell();
				form.onClose();
				KFloatable.prototype.closeMe.call(this);
				if (options.modal) {
					if (CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.enable();
					}
					if (CODAGenericShell.CURRENT_WIZARD_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_WIZARD_DIALOG.enable();
					}
					if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_BIG_SIDE_DIALOG.enable();
					}
					if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
						CODAGenericShell.CURRENT_SIDE_DIALOG.enable();
					}
				}
			});
			dialog.addCSSClass('CODAFloatablePropDialog');
			dialog.addCSSClass('CODAWidgetDetailsDialog');
			dialog.modal = options.modal;
		}
		dialog._url = KBreadcrumb.getHash();
		KFloatable.show(dialog);
		return dialog;
	};

	CODAGenericShell.prototype.makeDialog = function(form, _options) {
		var options = KSystem.merge(KSystem.clone(CODAGenericShell.FORM_OPTIONS), _options || {});
		var dialog = null;

		if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined && !!CODAGenericShell.CURRENT_BROWSER_DIALOG.isTool) {
			CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
			CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
		}

		if (options.minimized) {
			if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_SIDE_DIALOG.closeMe();
				CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
			}
			form.dispatchState('minimized');

			CODAGenericShell.CURRENT_SIDE_DIALOG = dialog = KFloatable.make(form, {
				width : 405,
				right : -410,
				top : 41,
				height : CODAGenericShell.FRAME_HEIGHT + 58,
				modal : false,
				buttons : 'close',
				tween : {
					show : [ 
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 500,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : 0
						},
						from : {
							x : -410
						}
					},
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 600,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					}
					],
					hide : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 500,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : -410
						},
						from : {
							x : 0
						}
					}, 
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 500,
						go : {
							alpha : 0
						},
						from : {
							alpha : 1
						}
					}
					]
				}
			});
			{
				dialog.closeMe = (!!options.callbacks && !!options.callbacks.close ? options.callbacks.close : function() {
					if (!!CODAGenericShell.CURRENT_SIDE_DIALOG && CODAGenericShell.CURRENT_SIDE_DIALOG.id == this.id) {
						CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
					}
					CODAGenericShell.closeAllOthers(this);

					var shell = form.getShell();
					form.onClose();
					KFloatable.prototype.closeMe.call(this);
				});
				dialog.addCSSClass('CODAWidgetsDialog');
				dialog.addCSSClass('CODAMinimizedDialog');
			}
		}
		else {
			if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_SIDE_DIALOG.closeMe();
				CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
			}
			if (CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.closeMe();
				CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = undefined;
			}
			if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_FORM_SWITCHING = true;

				var last = CODAGenericShell.CURRENT_BIG_SIDE_DIALOG;
				CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = undefined;
				last.closeMe();
			}

			CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = dialog = KFloatable.make(form, {
				right : -(KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN),
				top : 41,
				height : KDOM.getBrowserHeight() - 61,
				width: (form instanceof CODAForm ? CODAGenericShell.LEFT_FRAME_MARGIN : KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN),
				buttons : 'close',
				transition_wait_target : form instanceof CODAList,
				tween : {
					show : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : 0
						},
						from : {
							x : -(KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN)
						}
					},
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 850,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					}
					],
					hide : [
					{
						f : KEffects.easeOutExpo,
						type : 'move',
						duration : 750,
						gravity : {
							x : 'right'
						},
						lock : {
							y : true
						},
						go : {
							x : -(KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN)
						},
						from : {
							x : 0
						}
					}, 
					{
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 1000,
						go : {
							alpha : 0
						},
						from : {
							alpha : 1
						}
					}
					]
				}
				
			});
			{
				dialog.closeMe = (!!options.callbacks && !!options.callbacks.close ? options.callbacks.close : function() {
					CODAGenericShell.closeAllOthers(this);

					var shell = form.getShell();
					form.onClose();
					KFloatable.prototype.closeMe.call(this);
					if (!CODAGenericShell.CURRENT_FORM_SWITCHING || (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG != undefined && CODAGenericShell.CURRENT_BIG_SIDE_DIALOG.id == this.id)) {
						shell.onDialogClose();
						CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = undefined;			
					}
					CODAGenericShell.CURRENT_FORM_SWITCHING = false
				});
				dialog.addCSSClass('CODAFloatablePropDialog');
				dialog.addCSSClass('CODAWidgetDetailsDialog');
			}

			if (!CODAGenericShell.CURRENT_FORM_SWITCHING) {
				this.onDialogOpen();
			}
		}
		dialog._url = KBreadcrumb.getHash();
		KFloatable.show(dialog);
		return dialog;
	};

	CODAGenericShell.prototype.onFormOpen = function(dialog) {
		if (!!this.dashboard && this.dashboard.panel.className.indexOf('CODAMenuDashboard')) {
			var width = KDOM.normalizePixelValue(dialog.panel.style.width);
			width = isNaN(width) ? dialog.width : width;
			this.dashboard.content.style.width = (KDOM.getBrowserWidth() - width - 50) + 'px';
		}							
	};

	CODAGenericShell.prototype.onDialogOpen = function() {
		this.disable();
	};

	CODAGenericShell.prototype.onDialogClose = function() {
		this.enable();
	};

	CODAGenericShell.prototype.onResize = function(size) {
		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990;
		var fs = Math.round(fp * 100);

		KCSS.setStyle({
			fontSize: Math.round(fp * 24) + 'px'
		}, [ this.title ]);

		KCSS.setStyle({
			fontSize: (fs * 1.5) + '%'
		}, [ this.back.panel ]);
		this.back.setStyle({
			height: (KDOM.getBrowserHeight() - 55) + 'px'
		}, this.back.input);	

		CODAGenericShell.FRAME_HEIGHT = KDOM.getBrowserHeight() - 119;
		KCSS.setStyle({
			height :  KDOM.getBrowserHeight() + 'px'
		}, [
		this.panel
		]);
		KCSS.setStyle({
			height :  KDOM.getBrowserHeight() - 105+ 'px'
		}, [
		this.content
		]);

		if (!!this.footer) {
			gw = KDOM.getBrowserWidth() - 20;
			fp = gw / 1610;
			fs = Math.round(fp * 100);
			var children = this.footer.childWidgets();

			for (var b in children){
				var button = children[b];
				if (button instanceof KButton) {
					button.setStyle({
						fontSize :  fs + '%'
					});
				}
			}
		}

		if (!!this.dashboard && !!this.dashboard.resize) {
			this.dashboard.resize();
		}
	};

	CODAGenericShell.prototype.draw = function() {

		KShell.prototype.draw.call(this);

		CODAGenericShell.FRAME_HEIGHT = KDOM.getBrowserHeight() - 119;
		KCSS.setStyle({
			height :  KDOM.getBrowserHeight() + 'px'
		}, [
		this.panel
		]);
		KCSS.setStyle({
			height :  KDOM.getBrowserHeight() - 105+ 'px'
		}, [
		this.content
		]);

		this.hint = window.document.createElement('div');
		{
			this.hint.appendChild(window.document.createTextNode(' '));
			this.hint.className = ' CODAHintOverlay ';
			this.setStyle({
				width : KDOM.getBrowserWidth() + 'px',
				height : KDOM.getBrowserHeight() + 'px',
				position : 'fixed',
				zIndex : 9999,
				top : '0',
				left : '0',
				display : 'none'
			}, this.hint);
		}
		this.panel.appendChild(this.hint);

		if (!!this.notifications && !!this.notifications.elements && this.notifications.elements.length != 0) {
			KBubble.make(this.notifybutton, {
				text : this.notifications.elements.length,
				id : 'notifications-global-bubble',
				cssClass : 'CODAToolbardBubble',
				offsetX : '2.5em',
				offsetY : '-2em',
				gravity : {
					x : 'left',
					y : 'bottom'
				},
				callback : CODAGenericShell.showNotifications
			});							
		}
	};

	CODAGenericShell.prototype.showNotifications = function(e) {
		if (!!this.CURRENT_NOTIFICATIONS_DIALOG) {
			this.CURRENT_NOTIFICATIONS_DIALOG.closeMe();
		}
		var panel = new CODANotifications(this, this.notifications);
		this.CURRENT_NOTIFICATIONS_DIALOG = this.makeToolbarWindow(panel, {
			width: 500,
			pointer : {
				left : 325
			},
			x : KDOM.getBrowserWidth() - 300
		});
	};

	CODAGenericShell.prototype.showFeedbackWizard = function() {
		this.makeWizard(new CODAFeedbackWizard(this));
	};

	CODAGenericShell.prototype.showCloudBrowser = function(target, filter) {
		var configForm = new CODACloudBrowser(this, target, filter);
		{
			configForm.hash = '/cloud-browser';
			configForm.setTitle(gettext('$CODA_CLOUD_BROWSER'));
		}
		var dialog = this.makeToolsDialog(configForm, { minimized : false, modal : true, className : 'CODAMiddleDialog' });
		dialog.isTool = true;
	};

	CODAGenericShell.prototype.showImageCrop = function(target, options) {
		var configForm = new CODAImageCrop(this, target, options);
		{
			configForm.hash = '/image-crop';
			configForm.setTitle(gettext('$CODA_IMAGE_CROP'), true);
		}
		var dialog = this.makeToolsDialog(configForm, { minimized : false, modal : true, className : 'CODAMiddleDialog CODAImageCropDialog' });
		dialog.isTool = true;
	};

	CODAGenericShell.prototype.showHint = function() {
		if(typeof(Storage) !== "undefined") {
			if (!localStorage[this.module]) {
				localStorage[this.module] = '0';
			}
			localStorage[this.module] = parseInt(localStorage[this.module]) + 1;
			var hint = localStorage[this.module];

			if (!!this.hinted['hint' + hint]) {
				var self = this;
				var channel = null;

				if (window.XMLHttpRequest) { 
					channel = new window.XMLHttpRequest();
				}
				else if (window.ActiveXObject) { 
					try {
						channel = new ActiveXObject("Msxml2.XMLHTTP");
					}
					catch (e) {
						try {
							channel = new ActiveXObject("Microsoft.XMLHTTP");
						}
						catch (e1) {
						}
					}
				}

				channel.onreadystatechange = function() {
					channel.onreadystatechange = function() {
						if (channel.readyState == 4) {
							if (channel.status == 0) {
								return;
							}

							if (channel.status != 200) {
								return;
							}

							if (channel.responseText != '' && channel.getResponseHeader("Content-Type").indexOf('html') != -1) {
								self.setStyle({
									display : 'block'
								}, self.hint);
								self.setStyle({
									opacity : '1'
								}, self.hint);

								self.hint.innerHTML = channel.responseText;
							}
							else {
								return;
							}

						}			
					};

				};

				channel.open('GET', '//' + window.document.location.host + '/hints/' + KLocale.LANG + '/' + this.hinted['hint' + hint], true);
				channel.setRequestHeader("Pragma", "no-cache");
				channel.setRequestHeader("Cache-Control", "no-store");

				try {
					channel.send(null);
				}
				catch (e) {
				}
			}
		}

	};

	CODAGenericShell.prototype.hideHint = function() {
		this.setStyle({
			display : 'none'
		}, this.hint);
		this.setStyle({
			opacity : '0'
		}, this.hint);
		this.hint.innerHTML = '';
	};

	CODAGenericShell.prototype.popupBlocked = function() {
		KMessageDialog.alert({
			message : gettext('$POPUP_BLOCKER'),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300,
			html : true
		});		
	};

	CODAGenericShell.logout = function(no_redirection) {
		if(typeof(Storage) !== "undefined") {
			if (localStorage.access_token !== undefined) {
				delete localStorage.access_token;
			}
			delete localStorage.dashboard;
		}
		if (!no_redirection) {
			window.top.document.location = CODA_VERSION + "/login/";
		}
	};

	CODAGenericShell.getDialogLayers = function() {
		return (!!CODAGenericShell.CURRENT_SIDE_DIALOG ? 1 : 0) + (!!CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG ? 1 : 0) + (!!CODAGenericShell.CURRENT_BIG_SIDE_DIALOG ? 1 : 0)
	};

	CODAGenericShell.popHistoryUntil = function(e) {
		var a = KDOM.getEventTarget(e);
		if(typeof(Storage) !== "undefined") {
			var h = null;
			if (localStorage.dashboard !== undefined) {
				h = JSON.parse(localStorage.dashboard);
			}
			else {
				if (window.document.location.href.indexOf('/my/') != -1) {
					return CODA_VERSION + '/apps/';
				}
				CODAGenericShell.logout();
				return;
			}
			if (h.length == 0) {
				if (window.document.location.href.indexOf('/my/') != -1) {
					return CODA_VERSION + '/apps/';
				}
				CODAGenericShell.logout();
				return;
			}

			while (h[h.length - 1].href != a.href) {
				h.pop();
			}
			localStorage.dashboard = JSON.stringify(h);
		}		
	};

	CODAGenericShell.pushHistory = function(title) {
		if(typeof(Storage) !== "undefined") {
			var h = null;
			if (localStorage.dashboard === undefined) {
				h = [];
			}
			else {
				h = JSON.parse(localStorage.dashboard);
			}
			if (h.length != 0 && h[h.length - 1].href.split('#')[0] == window.document.location.href.split('#')[0]) {
				return;
			}
			h.push({href : window.document.location.href, title : title });
			localStorage.dashboard = JSON.stringify(h);
		}		
	};

	CODAGenericShell.popHistory = function() {
		if(typeof(Storage) !== "undefined") {
			var h = null;
			if (localStorage.dashboard !== undefined) {
				h = JSON.parse(localStorage.dashboard);
			}
			else {
				if (window.document.location.href.indexOf('/my/') != -1) {
					return CODA_VERSION + '/apps/';
				}
				CODAGenericShell.logout(true);
				return;
			}
			if (h.length == 0) {
				if (window.document.location.href.indexOf('/my/') != -1) {
					return CODA_VERSION + '/apps/';
				}
				CODAGenericShell.logout(true);
				return;
			}
			var ret = h.pop();
			localStorage.dashboard = JSON.stringify(h);
			return ret.href;
		}		
	};

	CODAGenericShell.getHistory = function() {
		if(typeof(Storage) !== "undefined") {
			var h = null;
			if (localStorage.dashboard !== undefined) {
				h = JSON.parse(localStorage.dashboard);
			}
			else {
				CODAGenericShell.logout();
				return;
			}
			return h;
		}		
	};

	CODAGenericShell.getAccessToken = function() {
		var access_token = null;
		if(typeof(Storage) !== "undefined") {
			if (localStorage.access_token !== undefined) {
				access_token = localStorage.access_token;
			}
		}
		return access_token;
	};

	CODAGenericShell.getUploadAuthorization = function() {
		var access_token = null;
		if(typeof(Storage) !== "undefined") {
			if (localStorage.access_token !== undefined) {
				access_token = localStorage.access_token;
			}
			else {
				return {};
			}
		}
		else {
			return {};
		}

		var shell = Kinky.getDefaultShell();
		var id = shell.data.app_id;
		var ret = {
			access_token : access_token,
			endpoint : shell.data.providers.oauth.services + '/filemanager/upload',
		};
		if (!!id) {
			ret.aditional_args = 'mediaServer=media_url_2&folderPath=' + encodeURIComponent('/' + id.charAt(0) + '/' + id + '/' + shell.module);
		}
		else {
			ret.aditional_args = 'mediaServer=media_url_1&folderPath=' + encodeURIComponent(Kinky.getLoggedUser()._id + '/applications');
		}
		return ret;
	};

	CODAGenericShell.resize = function(shell, size) {
		shell.onResize(size);
	};

	CODAGenericShell.fadeOut = function(shell) {
		if (!!shell.dashboard) {
			if (CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG.closeMe();
				CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = undefined;
			}
			if (CODAGenericShell.CURRENT_BROWSER_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_BROWSER_DIALOG.closeMe();
				CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
			}
			if (CODAGenericShell.CURRENT_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_SIDE_DIALOG.closeMe();
				CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
			}
			if (CODAGenericShell.CURRENT_BIG_SIDE_DIALOG !== undefined) {
				CODAGenericShell.CURRENT_FORM_SWITCHING = true;

				var last = CODAGenericShell.CURRENT_BIG_SIDE_DIALOG;
				CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = undefined;
				last.closeMe();
			}

			window.top.CODA_NAVIGATION_FROM = -1;

			KEffects.addEffect(shell.dashboard, [
				{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 350,
					gravity : {
						x : 'right'
					},
					lock : {
						y : true
					},
					go : {
						x : -410
					},
					from : {
						x : 0
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 350,
					applyToElement : true,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					},
					onComplete : function() {
						CODAGenericShell.popHistory();
						var uri = CODAGenericShell.popHistory();
						if (uri === undefined) {
							window.top.document.location =  '/' + KLocale.LANG + '/add-application/';
						}
						else {
							window.document.location = uri;
						}
					}
				}]);
		}
		else {
			CODAGenericShell.popHistory();
			window.document.location = CODAGenericShell.popHistory();
		}
	};

	CODAGenericShell.makeHelp = function(widget) {
		if (!widget.title) {
			widget.setTitle('');
		}
		var help = window.document.createElement('a');
		{
			help.className = ' CODATitleHelpElement ';
			help.target = '_blank';
			help.href = widget.helpURL || CODAGenericShell.generateHelpURL(widget);
		}
		widget.title.appendChild(help);
		var params = {
			text : gettext('$CODA_FORM_HELP_TIP'),
			delay : 300,
			gravity : {
				x : 'right',
				y : 'top'
			},
			target : help,
			offsetParent : help,
			offsetX : 30,
			offsetY : 0,
			cssClass : 'CODAInputBaloon CODAIconBaloon'
		};
		KBaloon.make(widget, params);
	};

	CODAGenericShell.generateHelpURL = function(widget) {
		var hash = KBreadcrumb.getHash();
		return CODA_VERSION + '/docs/' + KLocale.LANG + '/' + widget.getShell().module + '/#' + (!!hash ? hash.replace(/([A-Z]{2}[0-9]{6}[A-Z]{2}[0-9]{6}[A-Z]{2})/g, '').replace(/([0-9]+)/g, '').replace(/\/\//g, '/') : '');
	};

	CODAGenericShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'show-notifications': {
				Kinky.getDefaultShell().showNotifications();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'show-feedback-wizard': {
				Kinky.getDefaultShell().showFeedbackWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
		if (hash != '') {
			ga('send', 'pageview', window.document.location.pathname + hash.substr(1));
		}
	};

	CODAGenericShell.showNotifications = function(e) {
		KDOM.getEventWidget(e).getShell().showNotifications(e);
	};

	CODAGenericShell.closeAllOthers = function(form) {
		var active = Kinky.getActiveModalWidget();
		while (!!active && !!form && active.id != form.id) {
			Kinky.unsetModal(active);
			active = Kinky.getActiveModalWidget();
		}		
	};

	CODAGenericShell.pageUp = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;

		if (!!CODA_FOCUSED_WINDOW) {
			if (CODA_FOCUSED_WINDOW.target instanceof CODAWizard) {
				KDOM.stopEvent(event);
				CODA_FOCUSED_WINDOW.target.dispatchPrevious();
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAForm) {
				KDOM.stopEvent(event);
				CODA_FOCUSED_WINDOW.target.dispatchPrevious();
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAList) {
				KDOM.stopEvent(event);
				KList.dispatchPrevious(null, CODA_FOCUSED_WINDOW.target.list);
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAFormIconDashboard) {
				if (!!CODA_FOCUSED_WINDOW.target.FORM_OPENED) {
					KDOM.stopEvent(event);
					CODA_FOCUSED_WINDOW.target.FORM_OPENED.dispatchPrevious();
				}
			}
		}
	};

	CODAGenericShell.pageDown = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;

		if (!!CODA_FOCUSED_WINDOW) {
			if (CODA_FOCUSED_WINDOW.target instanceof CODAWizard) {
				KDOM.stopEvent(event);
				CODA_FOCUSED_WINDOW.target.dispatchNext();
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAForm) {
				KDOM.stopEvent(event);
				CODA_FOCUSED_WINDOW.target.dispatchNext();
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAList) {
				KDOM.stopEvent(event);
				KList.dispatchNext(null, CODA_FOCUSED_WINDOW.target.list);
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAFormIconDashboard) {
				if (!!CODA_FOCUSED_WINDOW.target.FORM_OPENED) {
					KDOM.stopEvent(event);
					CODA_FOCUSED_WINDOW.target.FORM_OPENED.dispatchNext();
				}
			}
		}
	};

	CODAGenericShell.search = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		KDOM.stopEvent(event);

		if (!!CODA_FOCUSED_WINDOW) {
			if (CODA_FOCUSED_WINDOW.target instanceof CODAList) {
				if (!!CODA_FOCUSED_WINDOW.target.filteractivator) {
					KDOM.fireEvent(CODA_FOCUSED_WINDOW.target.filteractivator.input, 'click');
				}
				
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAFormIconDashboard) {
				KDOM.fireEvent(CODA_FOCUSED_WINDOW.target.filteractivator.input, 'click');
			}
		}
	};

	CODAGenericShell.exit = function(event) {
		if (!!CODA_FOCUSED_WINDOW) {
			if (CODA_FOCUSED_WINDOW.target instanceof CODAFormIconDashboard) {
				if (!!CODA_FOCUSED_WINDOW.target.FORM_OPENED) {
					KDOM.stopEvent(event);
					CODA_FOCUSED_WINDOW.target.closeMe();
					return;
				}
			}
			else if (CODA_FOCUSED_WINDOW.target instanceof CODAForm && CODA_FOCUSED_WINDOW.target.className == 'CODAStylingToolbox') {
				return;
			}
			CODA_FOCUSED_WINDOW.closeMe();
		}
	};

	CODAGenericShell.applyFieldConfiguration = function(panel, fieldConfig, className) {
		if (!fieldConfig.fields) {
			return;
		}
		var children = panel.childWidgets();
		for (var c in children) {
			var child = children[c];
			if (child instanceof KAbstractInput || child instanceof KHidden) {
				if (!!fieldConfig.fields[child.getInputID()]) {
					var conf = fieldConfig.fields[child.getInputID()];
					if (!!conf.predefined) {
						child.setValue(conf.predefined);
					}
					if (!!conf.readonly) {
						var stat = new KStatic(panel, child.labelText, child.getInputID());
						stat.setValue(child.getValue());
						panel.appendChild(stat);
						child.panel.parentNode.replaceChild(stat.panel, child.panel);
						panel.removeChild(child);
						child = stat;
					}
					if (!!conf.hidden) {
						child.panel.style.display = 'none';
					}
					if (!!conf.options && !!child.addOption) {
						for (var o in conf.options) {
							child.addOption(conf.options[o].value, gettext(conf.options[o].label), conf.options[o].selected || conf.options[o].value == child.getValue());
						}
					}
				}
			}
		}

		CODAGenericShell.applyButtonConfiguration(fieldConfig, className);
	};

	CODAGenericShell.applyButtonConfiguration = function(buttonConfig, className) {
		if (!buttonConfig.buttons) {
			return;
		}
		for (var c in buttonConfig.buttons) {
			if (buttonConfig.buttons[c] === false) {
				Kinky.getDefaultShell().removeToolbarButton('/' + className + '/' + c);
			}
		}
	};

	CODAGenericShell.applyTableConfiguration = function(list, tableConfig) {
		if (!tableConfig.columns) {
			return;
		}
		for (var f in list.table) {
			if (tableConfig.columns[f] === false) {
				delete list.table[f];
			}
		}
	};

	CODAGenericShell.addTweak = function(data) {
		KCache.commit('/__tweaks', data);
	};

	window.document.onkeypress = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;

		switch(keyCode) {
			case 27 : { // ESC
				CODAGenericShell.exit(event);
				break;
			}
			case 33 : { // PAGE_UP
				if (!event.shiftKey) {
					CODAGenericShell.pageUp(event);
				}
				break;
			}
			case 34 : { // PAGE_DOWN
				if (!event.shiftKey) {
					CODAGenericShell.pageDown(event);
				}
				break;
			}
			case 47 : { // SEARCH
				if (!!event.shiftKey && !!event.ctrlKey) {
					CODAGenericShell.search(event);
				}
				break;
			}
		}
	};

	CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH = 775;
	CODAGenericShell.MIDDLE_DIALOG_MARGIN = 90;
	CODAGenericShell.BROWSER_DIALOG_MARGIN = 90;
	CODAGenericShell.BROWSER_DIALOG_MAXWIDTH = 725;
	CODAGenericShell.FRAME_HEIGHT = 0;
	CODAGenericShell.CURRENT_BROWSER_DIALOG = undefined;
	CODAGenericShell.CURRENT_WIZARD_DIALOG = undefined;
	CODAGenericShell.CURRENT_SIDE_DIALOG = undefined;
	CODAGenericShell.CURRENT_MIDDLE_SIDE_DIALOG = undefined;
	CODAGenericShell.CURRENT_BIG_SIDE_DIALOG = undefined;
	CODAGenericShell.FORM_OPTIONS = {
		minimized : true,
		modal : false,
		callbacks : {}
	};
	CODAGenericShell.N_HINTS = 40;

	// ANALYTICS LOADING
	{
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	}

	KConfirmPanel.prototype.__draw = KConfirmPanel.prototype.draw;
	KConfirmPanel.prototype.draw = function() {
		this.__draw();
		var image = window.document.createElement('img');
		image.src = '/imgs/question.png';
		this.panel.appendChild(image);
	};

	KMessagePanel.prototype.__draw = KMessagePanel.prototype.draw;
	KMessagePanel.prototype.draw = function() {
		this.__draw();
		var image = window.document.createElement('img');
		image.src = '/imgs/question.png';
		this.panel.appendChild(image);
	};

	KSystem.included('CODAGenericShell');
}, Kinky.CODA_INCLUDER_URL);

function geterrorfields(data) {
	var ret = '';
	if (!!data.detail) {
		if (!!data.detail.required_fields) {
			for (var f in data.detail.required_fields) {
				if (ret.length != 0) {
					ret += ', ';
				}
				ret += gettext('$ERROR_NUMBER_' + data.error_code + '_FIELD_' + data.detail.required_fields[f].toUpperCase());
			}
		}
	}
	return ret;
}

KLocale.load = function(prefix, notMinified) {

	if (!KLocale.DONE_GLOBAL) {
		var locale_global = window.document.createElement('script');
		locale_global.charset = 'utf-8';
		locale_global.src = CODA_VERSION + '/coda.locale.' + KLocale.LANG + '.min.js';
		window.document.getElementsByTagName('head')[0].appendChild(locale_global);
	}

	var locale_plugin = window.document.createElement('script');
	locale_plugin.charset = 'utf-8';
	locale_plugin.src = prefix + '.locale.' + KLocale.LANG + (!notMinified ? '.min' : '') + '.js';
	window.document.getElementsByTagName('head')[0].appendChild(locale_plugin);

	if (!KLocale.DONE_GLOBAL) {
		settext('$CODA_APPS_TRANSLATION', 'apps', 'pt');
		settext('$CODA_APPS_TRANSLATION', 'apps', 'br');
		settext('$CODA_APPS_TRANSLATION', 'apps', 'us');
		settext('$CODA_APPS_TRANSLATION', 'apps', 'eu');
		settext('$CODA_APPS_TRANSLATION', 'apps', 'es');

		switch (KLocale.LANG) {
			case 'pt' : {
				KLocale.LOCALE = 'pt_PT';
				KLocale.DATE_FORMAT = 'Y-M-d H:i';
				break;
			}
			case 'br' : {
				KLocale.LOCALE = 'pt_BR';
				KLocale.DATE_FORMAT = 'd-M-Y H:i';
				break;
			}
			case 'us' : {
				KLocale.LOCALE = 'en_US';
				KLocale.DATE_FORMAT = 'Y-d-M H:i';
				break;
			}
			case 'es' : {
				KLocale.LOCALE = 'es_ES';
				KLocale.DATE_FORMAT = 'Y-M-d H:i';
				break;
			}
			case 'eu' : {
				KLocale.LOCALE = 'en_US';
				KLocale.DATE_FORMAT = 'Y-M-d H:i';
				break;
			}
			default : {
				KLocale.LOCALE = 'pt_PT';
				KLocale.DATE_FORMAT = 'Y-M-d H:i';
				break;
			}
		}
	}
	KLocale.DONE_GLOBAL = true;

};

CODA_VERSION = '/1.0';
CODA_COUNTRIES = [
{"name" : { locales : { "pt_PT" : "Mundo", "pt_BR" : "Mundo", "en_US" : "World-wide", "en_GB" : "Europe" } }, "code" : "ALL", "locale" : "all_ALL"},
{"name" : { locales : { "pt_PT" : "Europa", "pt_BR" : "Europa", "en_US" : "Europe", "en_GB" : "Europe" } }, "code" : "EU", "locale" : "en_EU"},
{"name" : { locales : { "pt_PT" : "Afghanistan", "pt_BR" : "Afghanistan", "en_US" : "Afghanistan", "en_GB" : "Afghanistan" } }, "code" : "AF", "locale" : "af_AF"},
{"name" : { locales : { "pt_PT" : "Åland Islands", "pt_BR" : "Åland Islands", "en_US" : "Åland Islands", "en_GB" : "Åland Islands" } }, "code" : "AX", "locale" : "ax_AX"},
{"name" : { locales : { "pt_PT" : "Albania", "pt_BR" : "Albania", "en_US" : "Albania", "en_GB" : "Albania" } }, "code" : "AL", "locale" : "al_AL"},
{"name" : { locales : { "pt_PT" : "Algeria", "pt_BR" : "Algeria", "en_US" : "Algeria", "en_GB" : "Algeria" } }, "code" : "DZ", "locale" : "dz_DZ"},
{"name" : { locales : { "pt_PT" : "American Samoa", "pt_BR" : "American Samoa", "en_US" : "American Samoa", "en_GB" : "American Samoa" } }, "code" : "AS", "locale" : "as_AS"},
{"name" : { locales : { "pt_PT" : "Andorra", "pt_BR" : "Andorra", "en_US" : "Andorra", "en_GB" : "Andorra" } }, "code" : "AD", "locale" : "es_AD"},
{"name" : { locales : { "pt_PT" : "Angola", "pt_BR" : "Angola", "en_US" : "Angola", "en_GB" : "Angola" } }, "code" : "AO", "locale" : "ao_AO"},
{"name" : { locales : { "pt_PT" : "Anguilla", "pt_BR" : "Anguilla", "en_US" : "Anguilla", "en_GB" : "Anguilla" } }, "code" : "AI", "locale" : "ai_AI"},
{"name" : { locales : { "pt_PT" : "Antarctica", "pt_BR" : "Antarctica", "en_US" : "Antarctica", "en_GB" : "Antarctica" } }, "code" : "AQ", "locale" : "aq_AQ"},
{"name" : { locales : { "pt_PT" : "Antigua and Barbuda", "pt_BR" : "Antigua and Barbuda", "en_US" : "Antigua and Barbuda", "en_GB" : "Antigua and Barbuda" } }, "code" : "AG", "locale" : "ag_AG"},
{"name" : { locales : { "pt_PT" : "Argentina", "pt_BR" : "Argentina", "en_US" : "Argentina", "en_GB" : "Argentina" } }, "code" : "AR", "locale" : "es_AR"},
{"name" : { locales : { "pt_PT" : "Armenia", "pt_BR" : "Armenia", "en_US" : "Armenia", "en_GB" : "Armenia" } }, "code" : "AM", "locale" : "am_AM"},
{"name" : { locales : { "pt_PT" : "Aruba", "pt_BR" : "Aruba", "en_US" : "Aruba", "en_GB" : "Aruba" } }, "code" : "AW", "locale" : "aw_AW"},
{"name" : { locales : { "pt_PT" : "Australia", "pt_BR" : "Australia", "en_US" : "Australia", "en_GB" : "Australia" } }, "code" : "AU", "locale" : "en_AU"},
{"name" : { locales : { "pt_PT" : "Austria", "pt_BR" : "Austria", "en_US" : "Austria", "en_GB" : "Austria" } }, "code" : "AT", "locale" : "de_AT"},
{"name" : { locales : { "pt_PT" : "Azerbaijan", "pt_BR" : "Azerbaijan", "en_US" : "Azerbaijan", "en_GB" : "Azerbaijan" } }, "code" : "AZ", "locale" : "az_AZ"},
{"name" : { locales : { "pt_PT" : "Bahamas", "pt_BR" : "Bahamas", "en_US" : "Bahamas", "en_GB" : "Bahamas" } }, "code" : "BS", "locale" : "bs_BS"},
{"name" : { locales : { "pt_PT" : "Bahrain", "pt_BR" : "Bahrain", "en_US" : "Bahrain", "en_GB" : "Bahrain" } }, "code" : "BH", "locale" : "bh_BH"},
{"name" : { locales : { "pt_PT" : "Bangladesh", "pt_BR" : "Bangladesh", "en_US" : "Bangladesh", "en_GB" : "Bangladesh" } }, "code" : "BD", "locale" : "bd_BD"},
{"name" : { locales : { "pt_PT" : "Barbados", "pt_BR" : "Barbados", "en_US" : "Barbados", "en_GB" : "Barbados" } }, "code" : "BB", "locale" : "bb_BB"},
{"name" : { locales : { "pt_PT" : "Belarus", "pt_BR" : "Belarus", "en_US" : "Belarus", "en_GB" : "Belarus" } }, "code" : "BY", "locale" : "by_BY"},
{"name" : { locales : { "pt_PT" : "Belgium", "pt_BR" : "Belgium", "en_US" : "Belgium", "en_GB" : "Belgium" } }, "code" : "BE", "locale" : "be_BE"},
{"name" : { locales : { "pt_PT" : "Belize", "pt_BR" : "Belize", "en_US" : "Belize", "en_GB" : "Belize" } }, "code" : "BZ", "locale" : "bz_BZ"},
{"name" : { locales : { "pt_PT" : "Benin", "pt_BR" : "Benin", "en_US" : "Benin", "en_GB" : "Benin" } }, "code" : "BJ", "locale" : "bj_BJ"},
{"name" : { locales : { "pt_PT" : "Bermuda", "pt_BR" : "Bermuda", "en_US" : "Bermuda", "en_GB" : "Bermuda" } }, "code" : "BM", "locale" : "bm_BM"},
{"name" : { locales : { "pt_PT" : "Bhutan", "pt_BR" : "Bhutan", "en_US" : "Bhutan", "en_GB" : "Bhutan" } }, "code" : "BT", "locale" : "bt_BT"},
{"name" : { locales : { "pt_PT" : "Bolivia", "pt_BR" : "Bolivia", "en_US" : "Bolivia", "en_GB" : "Bolivia" } }, "code" : "BO", "locale" : "es_BO"},
{"name" : { locales : { "pt_PT" : "Bosnia and Herzegovina", "pt_BR" : "Bosnia and Herzegovina", "en_US" : "Bosnia and Herzegovina", "en_GB" : "Bosnia and Herzegovina" } }, "code" : "BA", "locale" : "ba_BA"},
{"name" : { locales : { "pt_PT" : "Botswana", "pt_BR" : "Botswana", "en_US" : "Botswana", "en_GB" : "Botswana" } }, "code" : "BW", "locale" : "bw_BW"},
{"name" : { locales : { "pt_PT" : "Bouvet Island", "pt_BR" : "Bouvet Island", "en_US" : "Bouvet Island", "en_GB" : "Bouvet Island" } }, "code" : "BV", "locale" : "bv_BV"},
{"name" : { locales : { "pt_PT" : "Brazil", "pt_BR" : "Brazil", "en_US" : "Brazil", "en_GB" : "Brazil" } }, "code" : "BR", "locale" : "pt_BR"},
{"name" : { locales : { "pt_PT" : "British Indian Ocean Territory", "pt_BR" : "British Indian Ocean Territory", "en_US" : "British Indian Ocean Territory", "en_GB" : "British Indian Ocean Territory" } }, "code" : "IO", "locale" : "en_IO"},
{"name" : { locales : { "pt_PT" : "Brunei Darussalam", "pt_BR" : "Brunei Darussalam", "en_US" : "Brunei Darussalam", "en_GB" : "Brunei Darussalam" } }, "code" : "BN", "locale" : "bn_BN"},
{"name" : { locales : { "pt_PT" : "Bulgaria", "pt_BR" : "Bulgaria", "en_US" : "Bulgaria", "en_GB" : "Bulgaria" } }, "code" : "BG", "locale" : "bg_BG"},
{"name" : { locales : { "pt_PT" : "Burkina Faso", "pt_BR" : "Burkina Faso", "en_US" : "Burkina Faso", "en_GB" : "Burkina Faso" } }, "code" : "BF", "locale" : "bf_BF"},
{"name" : { locales : { "pt_PT" : "Burundi", "pt_BR" : "Burundi", "en_US" : "Burundi", "en_GB" : "Burundi" } }, "code" : "BI", "locale" : "bi_BI"},
{"name" : { locales : { "pt_PT" : "Cambodia", "pt_BR" : "Cambodia", "en_US" : "Cambodia", "en_GB" : "Cambodia" } }, "code" : "KH", "locale" : "kh_KH"},
{"name" : { locales : { "pt_PT" : "Cameroon", "pt_BR" : "Cameroon", "en_US" : "Cameroon", "en_GB" : "Cameroon" } }, "code" : "CM", "locale" : "cm_CM"},
{"name" : { locales : { "pt_PT" : "Canada", "pt_BR" : "Canada", "en_US" : "Canada", "en_GB" : "Canada" } }, "code" : "CA", "locale" : "en_CA"},
{"name" : { locales : { "pt_PT" : "Cape Verde", "pt_BR" : "Cape Verde", "en_US" : "Cape Verde", "en_GB" : "Cape Verde" } }, "code" : "CV", "locale" : "cv_CV"},
{"name" : { locales : { "pt_PT" : "Cayman Islands", "pt_BR" : "Cayman Islands", "en_US" : "Cayman Islands", "en_GB" : "Cayman Islands" } }, "code" : "KY", "locale" : "ky_KY"},
{"name" : { locales : { "pt_PT" : "Central African Republic", "pt_BR" : "Central African Republic", "en_US" : "Central African Republic", "en_GB" : "Central African Republic" } }, "code" : "CF", "locale" : "cf_CF"},
{"name" : { locales : { "pt_PT" : "Chad", "pt_BR" : "Chad", "en_US" : "Chad", "en_GB" : "Chad" } }, "code" : "TD", "locale" : "td_TD"},
{"name" : { locales : { "pt_PT" : "Chile", "pt_BR" : "Chile", "en_US" : "Chile", "en_GB" : "Chile" } }, "code" : "CL", "locale" : "cl_CL"},
{"name" : { locales : { "pt_PT" : "China", "pt_BR" : "China", "en_US" : "China", "en_GB" : "China" } }, "code" : "CN", "locale" : "cn_CN"},
{"name" : { locales : { "pt_PT" : "Christmas Island", "pt_BR" : "Christmas Island", "en_US" : "Christmas Island", "en_GB" : "Christmas Island" } }, "code" : "CX", "locale" : "cx_CX"},
{"name" : { locales : { "pt_PT" : "Cocos (Keeling) Islands", "pt_BR" : "Cocos (Keeling) Islands", "en_US" : "Cocos (Keeling) Islands", "en_GB" : "Cocos (Keeling) Islands" } }, "code" : "CC", "locale" : "cc_CC"},
{"name" : { locales : { "pt_PT" : "Colombia", "pt_BR" : "Colombia", "en_US" : "Colombia", "en_GB" : "Colombia" } }, "code" : "CO", "locale" : "co_CO"},
{"name" : { locales : { "pt_PT" : "Comoros", "pt_BR" : "Comoros", "en_US" : "Comoros", "en_GB" : "Comoros" } }, "code" : "KM", "locale" : "km_KM"},
{"name" : { locales : { "pt_PT" : "Congo", "pt_BR" : "Congo", "en_US" : "Congo", "en_GB" : "Congo" } }, "code" : "CG", "locale" : "cg_CG"},
{"name" : { locales : { "pt_PT" : "Congo, The Democratic Republic of the", "pt_BR" : "Congo, The Democratic Republic of the", "en_US" : "Congo, The Democratic Republic of the", "en_GB" : "Congo, The Democratic Republic of the" } }, "code" : "CD", "locale" : "cd_CD"},
{"name" : { locales : { "pt_PT" : "Cook Islands", "pt_BR" : "Cook Islands", "en_US" : "Cook Islands", "en_GB" : "Cook Islands" } }, "code" : "CK", "locale" : "ck_CK"},
{"name" : { locales : { "pt_PT" : "Costa Rica", "pt_BR" : "Costa Rica", "en_US" : "Costa Rica", "en_GB" : "Costa Rica" } }, "code" : "CR", "locale" : "cr_CR"},
{"name" : { locales : { "pt_PT" : "Cote D\"Ivoire", "pt_BR" : "Cote D\"Ivoire", "en_US" : "Cote D\"Ivoire", "en_GB" : "Cote D\"Ivoire" } }, "code" : "CI", "locale" : "ci_CI"},
{"name" : { locales : { "pt_PT" : "Croatia", "pt_BR" : "Croatia", "en_US" : "Croatia", "en_GB" : "Croatia" } }, "code" : "HR", "locale" : "hr_HR"},
{"name" : { locales : { "pt_PT" : "Cuba", "pt_BR" : "Cuba", "en_US" : "Cuba", "en_GB" : "Cuba" } }, "code" : "CU", "locale" : "cu_CU"},
{"name" : { locales : { "pt_PT" : "Cyprus", "pt_BR" : "Cyprus", "en_US" : "Cyprus", "en_GB" : "Cyprus" } }, "code" : "CY", "locale" : "cy_CY"},
{"name" : { locales : { "pt_PT" : "Czech Republic", "pt_BR" : "Czech Republic", "en_US" : "Czech Republic", "en_GB" : "Czech Republic" } }, "code" : "CZ", "locale" : "cz_CZ"},
{"name" : { locales : { "pt_PT" : "Denmark", "pt_BR" : "Denmark", "en_US" : "Denmark", "en_GB" : "Denmark" } }, "code" : "DK", "locale" : "dk_DK"},
{"name" : { locales : { "pt_PT" : "Djibouti", "pt_BR" : "Djibouti", "en_US" : "Djibouti", "en_GB" : "Djibouti" } }, "code" : "DJ", "locale" : "dj_DJ"},
{"name" : { locales : { "pt_PT" : "Dominica", "pt_BR" : "Dominica", "en_US" : "Dominica", "en_GB" : "Dominica" } }, "code" : "DM", "locale" : "dm_DM"},
{"name" : { locales : { "pt_PT" : "Dominican Republic", "pt_BR" : "Dominican Republic", "en_US" : "Dominican Republic", "en_GB" : "Dominican Republic" } }, "code" : "DO", "locale" : "do_DO"},
{"name" : { locales : { "pt_PT" : "Ecuador", "pt_BR" : "Ecuador", "en_US" : "Ecuador", "en_GB" : "Ecuador" } }, "code" : "EC", "locale" : "ec_EC"},
{"name" : { locales : { "pt_PT" : "Egypt", "pt_BR" : "Egypt", "en_US" : "Egypt", "en_GB" : "Egypt" } }, "code" : "EG", "locale" : "eg_EG"},
{"name" : { locales : { "pt_PT" : "El Salvador", "pt_BR" : "El Salvador", "en_US" : "El Salvador", "en_GB" : "El Salvador" } }, "code" : "SV", "locale" : "sv_SV"},
{"name" : { locales : { "pt_PT" : "Equatorial Guinea", "pt_BR" : "Equatorial Guinea", "en_US" : "Equatorial Guinea", "en_GB" : "Equatorial Guinea" } }, "code" : "GQ", "locale" : "gq_GQ"},
{"name" : { locales : { "pt_PT" : "Eritrea", "pt_BR" : "Eritrea", "en_US" : "Eritrea", "en_GB" : "Eritrea" } }, "code" : "ER", "locale" : "er_ER"},
{"name" : { locales : { "pt_PT" : "Estonia", "pt_BR" : "Estonia", "en_US" : "Estonia", "en_GB" : "Estonia" } }, "code" : "EE", "locale" : "ee_EE"},
{"name" : { locales : { "pt_PT" : "Ethiopia", "pt_BR" : "Ethiopia", "en_US" : "Ethiopia", "en_GB" : "Ethiopia" } }, "code" : "ET", "locale" : "et_ET"},
{"name" : { locales : { "pt_PT" : "Falkland Islands (Malvinas)", "pt_BR" : "Falkland Islands (Malvinas)", "en_US" : "Falkland Islands (Malvinas)", "en_GB" : "Falkland Islands (Malvinas)" } }, "code" : "FK", "locale" : "fk_FK"},
{"name" : { locales : { "pt_PT" : "Faroe Islands", "pt_BR" : "Faroe Islands", "en_US" : "Faroe Islands", "en_GB" : "Faroe Islands" } }, "code" : "FO", "locale" : "fo_FO"},
{"name" : { locales : { "pt_PT" : "Fiji", "pt_BR" : "Fiji", "en_US" : "Fiji", "en_GB" : "Fiji" } }, "code" : "FJ", "locale" : "fj_FJ"},
{"name" : { locales : { "pt_PT" : "Finland", "pt_BR" : "Finland", "en_US" : "Finland", "en_GB" : "Finland" } }, "code" : "FI", "locale" : "fi_FI"},
{"name" : { locales : { "pt_PT" : "France", "pt_BR" : "France", "en_US" : "France", "en_GB" : "France" } }, "code" : "FR", "locale" : "fr_FR"},
{"name" : { locales : { "pt_PT" : "French Guiana", "pt_BR" : "French Guiana", "en_US" : "French Guiana", "en_GB" : "French Guiana" } }, "code" : "GF", "locale" : "gf_GF"},
{"name" : { locales : { "pt_PT" : "French Polynesia", "pt_BR" : "French Polynesia", "en_US" : "French Polynesia", "en_GB" : "French Polynesia" } }, "code" : "PF", "locale" : "pf_PF"},
{"name" : { locales : { "pt_PT" : "French Southern Territories", "pt_BR" : "French Southern Territories", "en_US" : "French Southern Territories", "en_GB" : "French Southern Territories" } }, "code" : "TF", "locale" : "tf_TF"},
{"name" : { locales : { "pt_PT" : "Gabon", "pt_BR" : "Gabon", "en_US" : "Gabon", "en_GB" : "Gabon" } }, "code" : "GA", "locale" : "ga_GA"},
{"name" : { locales : { "pt_PT" : "Gambia", "pt_BR" : "Gambia", "en_US" : "Gambia", "en_GB" : "Gambia" } }, "code" : "GM", "locale" : "gm_GM"},
{"name" : { locales : { "pt_PT" : "Georgia", "pt_BR" : "Georgia", "en_US" : "Georgia", "en_GB" : "Georgia" } }, "code" : "GE", "locale" : "ge_GE"},
{"name" : { locales : { "pt_PT" : "Germany", "pt_BR" : "Germany", "en_US" : "Germany", "en_GB" : "Germany" } }, "code" : "DE", "locale" : "de_DE"},
{"name" : { locales : { "pt_PT" : "Ghana", "pt_BR" : "Ghana", "en_US" : "Ghana", "en_GB" : "Ghana" } }, "code" : "GH", "locale" : "gh_GH"},
{"name" : { locales : { "pt_PT" : "Gibraltar", "pt_BR" : "Gibraltar", "en_US" : "Gibraltar", "en_GB" : "Gibraltar" } }, "code" : "GI", "locale" : "gi_GI"},
{"name" : { locales : { "pt_PT" : "Greece", "pt_BR" : "Greece", "en_US" : "Greece", "en_GB" : "Greece" } }, "code" : "GR", "locale" : "gr_GR"},
{"name" : { locales : { "pt_PT" : "Greenland", "pt_BR" : "Greenland", "en_US" : "Greenland", "en_GB" : "Greenland" } }, "code" : "GL", "locale" : "gl_GL"},
{"name" : { locales : { "pt_PT" : "Grenada", "pt_BR" : "Grenada", "en_US" : "Grenada", "en_GB" : "Grenada" } }, "code" : "GD", "locale" : "gd_GD"},
{"name" : { locales : { "pt_PT" : "Guadeloupe", "pt_BR" : "Guadeloupe", "en_US" : "Guadeloupe", "en_GB" : "Guadeloupe" } }, "code" : "GP", "locale" : "gp_GP"},
{"name" : { locales : { "pt_PT" : "Guam", "pt_BR" : "Guam", "en_US" : "Guam", "en_GB" : "Guam" } }, "code" : "GU", "locale" : "gu_GU"},
{"name" : { locales : { "pt_PT" : "Guatemala", "pt_BR" : "Guatemala", "en_US" : "Guatemala", "en_GB" : "Guatemala" } }, "code" : "GT", "locale" : "gt_GT"},
{"name" : { locales : { "pt_PT" : "Guernsey", "pt_BR" : "Guernsey", "en_US" : "Guernsey", "en_GB" : "Guernsey" } }, "code" : "GG", "locale" : "gg_GG"},
{"name" : { locales : { "pt_PT" : "Guinea", "pt_BR" : "Guinea", "en_US" : "Guinea", "en_GB" : "Guinea" } }, "code" : "GN", "locale" : "gn_GN"},
{"name" : { locales : { "pt_PT" : "Guinea-Bissau", "pt_BR" : "Guinea-Bissau", "en_US" : "Guinea-Bissau", "en_GB" : "Guinea-Bissau" } }, "code" : "GW", "locale" : "gw_GW"},
{"name" : { locales : { "pt_PT" : "Guyana", "pt_BR" : "Guyana", "en_US" : "Guyana", "en_GB" : "Guyana" } }, "code" : "GY", "locale" : "gy_GY"},
{"name" : { locales : { "pt_PT" : "Haiti", "pt_BR" : "Haiti", "en_US" : "Haiti", "en_GB" : "Haiti" } }, "code" : "HT", "locale" : "ht_HT"},
{"name" : { locales : { "pt_PT" : "Heard Island and Mcdonald Islands", "pt_BR" : "Heard Island and Mcdonald Islands", "en_US" : "Heard Island and Mcdonald Islands", "en_GB" : "Heard Island and Mcdonald Islands" } }, "code" : "HM", "locale" : "hm_HM"},
{"name" : { locales : { "pt_PT" : "Holy See (Vatican City State)", "pt_BR" : "Holy See (Vatican City State)", "en_US" : "Holy See (Vatican City State)", "en_GB" : "Holy See (Vatican City State)" } }, "code" : "VA", "locale" : "va_VA"},
{"name" : { locales : { "pt_PT" : "Honduras", "pt_BR" : "Honduras", "en_US" : "Honduras", "en_GB" : "Honduras" } }, "code" : "HN", "locale" : "hn_HN"},
{"name" : { locales : { "pt_PT" : "Hong Kong", "pt_BR" : "Hong Kong", "en_US" : "Hong Kong", "en_GB" : "Hong Kong" } }, "code" : "HK", "locale" : "hk_HK"},
{"name" : { locales : { "pt_PT" : "Hungary", "pt_BR" : "Hungary", "en_US" : "Hungary", "en_GB" : "Hungary" } }, "code" : "HU", "locale" : "hu_HU"},
{"name" : { locales : { "pt_PT" : "Iceland", "pt_BR" : "Iceland", "en_US" : "Iceland", "en_GB" : "Iceland" } }, "code" : "IS", "locale" : "is_IS"},
{"name" : { locales : { "pt_PT" : "India", "pt_BR" : "India", "en_US" : "India", "en_GB" : "India" } }, "code" : "IN", "locale" : "in_IN"},
{"name" : { locales : { "pt_PT" : "Indonesia", "pt_BR" : "Indonesia", "en_US" : "Indonesia", "en_GB" : "Indonesia" } }, "code" : "ID", "locale" : "id_ID"},
{"name" : { locales : { "pt_PT" : "Iran, Islamic Republic Of", "pt_BR" : "Iran, Islamic Republic Of", "en_US" : "Iran, Islamic Republic Of", "en_GB" : "Iran, Islamic Republic Of" } }, "code" : "IR", "locale" : "ir_IR"},
{"name" : { locales : { "pt_PT" : "Iraq", "pt_BR" : "Iraq", "en_US" : "Iraq", "en_GB" : "Iraq" } }, "code" : "IQ", "locale" : "iq_IQ"},
{"name" : { locales : { "pt_PT" : "Ireland", "pt_BR" : "Ireland", "en_US" : "Ireland", "en_GB" : "Ireland" } }, "code" : "IE", "locale" : "ie_IE"},
{"name" : { locales : { "pt_PT" : "Isle of Man", "pt_BR" : "Isle of Man", "en_US" : "Isle of Man", "en_GB" : "Isle of Man" } }, "code" : "IM", "locale" : "im_IM"},
{"name" : { locales : { "pt_PT" : "Israel", "pt_BR" : "Israel", "en_US" : "Israel", "en_GB" : "Israel" } }, "code" : "IL", "locale" : "il_IL"},
{"name" : { locales : { "pt_PT" : "Italy", "pt_BR" : "Italy", "en_US" : "Italy", "en_GB" : "Italy" } }, "code" : "IT", "locale" : "it_IT"},
{"name" : { locales : { "pt_PT" : "Jamaica", "pt_BR" : "Jamaica", "en_US" : "Jamaica", "en_GB" : "Jamaica" } }, "code" : "JM", "locale" : "jm_JM"},
{"name" : { locales : { "pt_PT" : "Japan", "pt_BR" : "Japan", "en_US" : "Japan", "en_GB" : "Japan" } }, "code" : "JP", "locale" : "jp_JP"},
{"name" : { locales : { "pt_PT" : "Jersey", "pt_BR" : "Jersey", "en_US" : "Jersey", "en_GB" : "Jersey" } }, "code" : "JE", "locale" : "je_JE"},
{"name" : { locales : { "pt_PT" : "Jordan", "pt_BR" : "Jordan", "en_US" : "Jordan", "en_GB" : "Jordan" } }, "code" : "JO", "locale" : "jo_JO"},
{"name" : { locales : { "pt_PT" : "Kazakhstan", "pt_BR" : "Kazakhstan", "en_US" : "Kazakhstan", "en_GB" : "Kazakhstan" } }, "code" : "KZ", "locale" : "kz_KZ"},
{"name" : { locales : { "pt_PT" : "Kenya", "pt_BR" : "Kenya", "en_US" : "Kenya", "en_GB" : "Kenya" } }, "code" : "KE", "locale" : "ke_KE"},
{"name" : { locales : { "pt_PT" : "Kiribati", "pt_BR" : "Kiribati", "en_US" : "Kiribati", "en_GB" : "Kiribati" } }, "code" : "KI", "locale" : "ki_KI"},
{"name" : { locales : { "pt_PT" : "Korea, Democratic People's Republic of", "pt_BR" : "Korea, Democratic People's Republic of", "en_US" : "Korea, Democratic People's Republic of", "en_GB" : "Korea, Democratic People's Republic of" } }, "code" : "KP", "locale" : "kp_KP"},
{"name" : { locales : { "pt_PT" : "Korea, Republic of", "pt_BR" : "Korea, Republic of", "en_US" : "Korea, Republic of", "en_GB" : "Korea, Republic of" } }, "code" : "KR", "locale" : "kr_KR"},
{"name" : { locales : { "pt_PT" : "Kuwait", "pt_BR" : "Kuwait", "en_US" : "Kuwait", "en_GB" : "Kuwait" } }, "code" : "KW", "locale" : "kw_KW"},
{"name" : { locales : { "pt_PT" : "Kyrgyzstan", "pt_BR" : "Kyrgyzstan", "en_US" : "Kyrgyzstan", "en_GB" : "Kyrgyzstan" } }, "code" : "KG", "locale" : "kg_KG"},
{"name" : { locales : { "pt_PT" : "Lao People\"S Democratic Republic", "pt_BR" : "Lao People\"S Democratic Republic", "en_US" : "Lao People\"S Democratic Republic", "en_GB" : "Lao People\"S Democratic Republic" } }, "code" : "LA", "locale" : "la_LA"},
{"name" : { locales : { "pt_PT" : "Latvia", "pt_BR" : "Latvia", "en_US" : "Latvia", "en_GB" : "Latvia" } }, "code" : "LV", "locale" : "lv_LV"},
{"name" : { locales : { "pt_PT" : "Lebanon", "pt_BR" : "Lebanon", "en_US" : "Lebanon", "en_GB" : "Lebanon" } }, "code" : "LB", "locale" : "lb_LB"},
{"name" : { locales : { "pt_PT" : "Lesotho", "pt_BR" : "Lesotho", "en_US" : "Lesotho", "en_GB" : "Lesotho" } }, "code" : "LS", "locale" : "ls_LS"},
{"name" : { locales : { "pt_PT" : "Liberia", "pt_BR" : "Liberia", "en_US" : "Liberia", "en_GB" : "Liberia" } }, "code" : "LR", "locale" : "lr_LR"},
{"name" : { locales : { "pt_PT" : "Libyan Arab Jamahiriya", "pt_BR" : "Libyan Arab Jamahiriya", "en_US" : "Libyan Arab Jamahiriya", "en_GB" : "Libyan Arab Jamahiriya" } }, "code" : "LY", "locale" : "ly_LY"},
{"name" : { locales : { "pt_PT" : "Liechtenstein", "pt_BR" : "Liechtenstein", "en_US" : "Liechtenstein", "en_GB" : "Liechtenstein" } }, "code" : "LI", "locale" : "li_LI"},
{"name" : { locales : { "pt_PT" : "Lithuania", "pt_BR" : "Lithuania", "en_US" : "Lithuania", "en_GB" : "Lithuania" } }, "code" : "LT", "locale" : "lt_LT"},
{"name" : { locales : { "pt_PT" : "Luxembourg", "pt_BR" : "Luxembourg", "en_US" : "Luxembourg", "en_GB" : "Luxembourg" } }, "code" : "LU", "locale" : "lu_LU"},
{"name" : { locales : { "pt_PT" : "Macao", "pt_BR" : "Macao", "en_US" : "Macao", "en_GB" : "Macao" } }, "code" : "MO", "locale" : "mo_MO"},
{"name" : { locales : { "pt_PT" : "Macedonia, The Former Yugoslav Republic of", "pt_BR" : "Macedonia, The Former Yugoslav Republic of", "en_US" : "Macedonia, The Former Yugoslav Republic of", "en_GB" : "Macedonia, The Former Yugoslav Republic of" } }, "code" : "MK", "locale" : "mk_MK"},
{"name" : { locales : { "pt_PT" : "Madagascar", "pt_BR" : "Madagascar", "en_US" : "Madagascar", "en_GB" : "Madagascar" } }, "code" : "MG", "locale" : "mg_MG"},
{"name" : { locales : { "pt_PT" : "Malawi", "pt_BR" : "Malawi", "en_US" : "Malawi", "en_GB" : "Malawi" } }, "code" : "MW", "locale" : "mw_MW"},
{"name" : { locales : { "pt_PT" : "Malaysia", "pt_BR" : "Malaysia", "en_US" : "Malaysia", "en_GB" : "Malaysia" } }, "code" : "MY", "locale" : "my_MY"},
{"name" : { locales : { "pt_PT" : "Maldives", "pt_BR" : "Maldives", "en_US" : "Maldives", "en_GB" : "Maldives" } }, "code" : "MV", "locale" : "mv_MV"},
{"name" : { locales : { "pt_PT" : "Mali", "pt_BR" : "Mali", "en_US" : "Mali", "en_GB" : "Mali" } }, "code" : "ML", "locale" : "ml_ML"},
{"name" : { locales : { "pt_PT" : "Malta", "pt_BR" : "Malta", "en_US" : "Malta", "en_GB" : "Malta" } }, "code" : "MT", "locale" : "mt_MT"},
{"name" : { locales : { "pt_PT" : "Marshall Islands", "pt_BR" : "Marshall Islands", "en_US" : "Marshall Islands", "en_GB" : "Marshall Islands" } }, "code" : "MH", "locale" : "mh_MH"},
{"name" : { locales : { "pt_PT" : "Martinique", "pt_BR" : "Martinique", "en_US" : "Martinique", "en_GB" : "Martinique" } }, "code" : "MQ", "locale" : "mq_MQ"},
{"name" : { locales : { "pt_PT" : "Mauritania", "pt_BR" : "Mauritania", "en_US" : "Mauritania", "en_GB" : "Mauritania" } }, "code" : "MR", "locale" : "mr_MR"},
{"name" : { locales : { "pt_PT" : "Mauritius", "pt_BR" : "Mauritius", "en_US" : "Mauritius", "en_GB" : "Mauritius" } }, "code" : "MU", "locale" : "mu_MU"},
{"name" : { locales : { "pt_PT" : "Mayotte", "pt_BR" : "Mayotte", "en_US" : "Mayotte", "en_GB" : "Mayotte" } }, "code" : "YT", "locale" : "yt_YT"},
{"name" : { locales : { "pt_PT" : "Mexico", "pt_BR" : "Mexico", "en_US" : "Mexico", "en_GB" : "Mexico" } }, "code" : "MX", "locale" : "es_MX"},
{"name" : { locales : { "pt_PT" : "Micronesia, Federated States of", "pt_BR" : "Micronesia, Federated States of", "en_US" : "Micronesia, Federated States of", "en_GB" : "Micronesia, Federated States of" } }, "code" : "FM", "locale" : "fm_FM"},
{"name" : { locales : { "pt_PT" : "Moldova, Republic of", "pt_BR" : "Moldova, Republic of", "en_US" : "Moldova, Republic of", "en_GB" : "Moldova, Republic of" } }, "code" : "MD", "locale" : "md_MD"},
{"name" : { locales : { "pt_PT" : "Monaco", "pt_BR" : "Monaco", "en_US" : "Monaco", "en_GB" : "Monaco" } }, "code" : "MC", "locale" : "mc_MC"},
{"name" : { locales : { "pt_PT" : "Mongolia", "pt_BR" : "Mongolia", "en_US" : "Mongolia", "en_GB" : "Mongolia" } }, "code" : "MN", "locale" : "mn_MN"},
{"name" : { locales : { "pt_PT" : "Montserrat", "pt_BR" : "Montserrat", "en_US" : "Montserrat", "en_GB" : "Montserrat" } }, "code" : "MS", "locale" : "ms_MS"},
{"name" : { locales : { "pt_PT" : "Morocco", "pt_BR" : "Morocco", "en_US" : "Morocco", "en_GB" : "Morocco" } }, "code" : "MA", "locale" : "ma_MA"},
{"name" : { locales : { "pt_PT" : "Mozambique", "pt_BR" : "Mozambique", "en_US" : "Mozambique", "en_GB" : "Mozambique" } }, "code" : "MZ", "locale" : "mz_MZ"},
{"name" : { locales : { "pt_PT" : "Myanmar", "pt_BR" : "Myanmar", "en_US" : "Myanmar", "en_GB" : "Myanmar" } }, "code" : "MM", "locale" : "mm_MM"},
{"name" : { locales : { "pt_PT" : "Namibia", "pt_BR" : "Namibia", "en_US" : "Namibia", "en_GB" : "Namibia" } }, "code" : "NA", "locale" : "na_NA"},
{"name" : { locales : { "pt_PT" : "Nauru", "pt_BR" : "Nauru", "en_US" : "Nauru", "en_GB" : "Nauru" } }, "code" : "NR", "locale" : "nr_NR"},
{"name" : { locales : { "pt_PT" : "Nepal", "pt_BR" : "Nepal", "en_US" : "Nepal", "en_GB" : "Nepal" } }, "code" : "NP", "locale" : "np_NP"},
{"name" : { locales : { "pt_PT" : "Netherlands", "pt_BR" : "Netherlands", "en_US" : "Netherlands", "en_GB" : "Netherlands" } }, "code" : "NL", "locale" : "nl_NL"},
{"name" : { locales : { "pt_PT" : "Netherlands Antilles", "pt_BR" : "Netherlands Antilles", "en_US" : "Netherlands Antilles", "en_GB" : "Netherlands Antilles" } }, "code" : "AN", "locale" : "an_AN"},
{"name" : { locales : { "pt_PT" : "New Caledonia", "pt_BR" : "New Caledonia", "en_US" : "New Caledonia", "en_GB" : "New Caledonia" } }, "code" : "NC", "locale" : "nc_NC"},
{"name" : { locales : { "pt_PT" : "New Zealand", "pt_BR" : "New Zealand", "en_US" : "New Zealand", "en_GB" : "New Zealand" } }, "code" : "NZ", "locale" : "nz_NZ"},
{"name" : { locales : { "pt_PT" : "Nicaragua", "pt_BR" : "Nicaragua", "en_US" : "Nicaragua", "en_GB" : "Nicaragua" } }, "code" : "NI", "locale" : "ni_NI"},
{"name" : { locales : { "pt_PT" : "Niger", "pt_BR" : "Niger", "en_US" : "Niger", "en_GB" : "Niger" } }, "code" : "NE", "locale" : "ne_NE"},
{"name" : { locales : { "pt_PT" : "Nigeria", "pt_BR" : "Nigeria", "en_US" : "Nigeria", "en_GB" : "Nigeria" } }, "code" : "NG", "locale" : "ng_NG"},
{"name" : { locales : { "pt_PT" : "Niue", "pt_BR" : "Niue", "en_US" : "Niue", "en_GB" : "Niue" } }, "code" : "NU", "locale" : "nu_NU"},
{"name" : { locales : { "pt_PT" : "Norfolk Island", "pt_BR" : "Norfolk Island", "en_US" : "Norfolk Island", "en_GB" : "Norfolk Island" } }, "code" : "NF", "locale" : "nf_NF"},
{"name" : { locales : { "pt_PT" : "Northern Mariana Islands", "pt_BR" : "Northern Mariana Islands", "en_US" : "Northern Mariana Islands", "en_GB" : "Northern Mariana Islands" } }, "code" : "MP", "locale" : "mp_MP"},
{"name" : { locales : { "pt_PT" : "Norway", "pt_BR" : "Norway", "en_US" : "Norway", "en_GB" : "Norway" } }, "code" : "NO", "locale" : "no_NO"},
{"name" : { locales : { "pt_PT" : "Oman", "pt_BR" : "Oman", "en_US" : "Oman", "en_GB" : "Oman" } }, "code" : "OM", "locale" : "om_OM"},
{"name" : { locales : { "pt_PT" : "Pakistan", "pt_BR" : "Pakistan", "en_US" : "Pakistan", "en_GB" : "Pakistan" } }, "code" : "PK", "locale" : "pk_PK"},
{"name" : { locales : { "pt_PT" : "Palau", "pt_BR" : "Palau", "en_US" : "Palau", "en_GB" : "Palau" } }, "code" : "PW", "locale" : "pw_PW"},
{"name" : { locales : { "pt_PT" : "Palestinian Territory, Occupied", "pt_BR" : "Palestinian Territory, Occupied", "en_US" : "Palestinian Territory, Occupied", "en_GB" : "Palestinian Territory, Occupied" } }, "code" : "PS", "locale" : "ps_PS"},
{"name" : { locales : { "pt_PT" : "Panama", "pt_BR" : "Panama", "en_US" : "Panama", "en_GB" : "Panama" } }, "code" : "PA", "locale" : "pa_PA"},
{"name" : { locales : { "pt_PT" : "Papua New Guinea", "pt_BR" : "Papua New Guinea", "en_US" : "Papua New Guinea", "en_GB" : "Papua New Guinea" } }, "code" : "PG", "locale" : "pg_PG"},
{"name" : { locales : { "pt_PT" : "Paraguay", "pt_BR" : "Paraguay", "en_US" : "Paraguay", "en_GB" : "Paraguay" } }, "code" : "PY", "locale" : "py_PY"},
{"name" : { locales : { "pt_PT" : "Peru", "pt_BR" : "Peru", "en_US" : "Peru", "en_GB" : "Peru" } }, "code" : "PE", "locale" : "pe_PE"},
{"name" : { locales : { "pt_PT" : "Philippines", "pt_BR" : "Philippines", "en_US" : "Philippines", "en_GB" : "Philippines" } }, "code" : "PH", "locale" : "ph_PH"},
{"name" : { locales : { "pt_PT" : "Pitcairn", "pt_BR" : "Pitcairn", "en_US" : "Pitcairn", "en_GB" : "Pitcairn" } }, "code" : "PN", "locale" : "pn_PN"},
{"name" : { locales : { "pt_PT" : "Poland", "pt_BR" : "Poland", "en_US" : "Poland", "en_GB" : "Poland" } }, "code" : "PL", "locale" : "pl_PL"},
{"name" : { locales : { "pt_PT" : "Portugal", "pt_BR" : "Portugal", "en_US" : "Portugal", "en_GB" : "Portugal" } }, "code" : "PT", "locale" : "pt_PT"},
{"name" : { locales : { "pt_PT" : "Puerto Rico", "pt_BR" : "Puerto Rico", "en_US" : "Puerto Rico", "en_GB" : "Puerto Rico" } }, "code" : "PR", "locale" : "pr_PR"},
{"name" : { locales : { "pt_PT" : "Qatar", "pt_BR" : "Qatar", "en_US" : "Qatar", "en_GB" : "Qatar" } }, "code" : "QA", "locale" : "qa_QA"},
{"name" : { locales : { "pt_PT" : "Reunion", "pt_BR" : "Reunion", "en_US" : "Reunion", "en_GB" : "Reunion" } }, "code" : "RE", "locale" : "re_RE"},
{"name" : { locales : { "pt_PT" : "Romania", "pt_BR" : "Romania", "en_US" : "Romania", "en_GB" : "Romania" } }, "code" : "RO", "locale" : "ro_RO"},
{"name" : { locales : { "pt_PT" : "Russian Federation", "pt_BR" : "Russian Federation", "en_US" : "Russian Federation", "en_GB" : "Russian Federation" } }, "code" : "RU", "locale" : "ru_RU"},
{"name" : { locales : { "pt_PT" : "RWANDA", "pt_BR" : "RWANDA", "en_US" : "RWANDA", "en_GB" : "RWANDA" } }, "code" : "RW", "locale" : "rw_RW"},
{"name" : { locales : { "pt_PT" : "Saint Helena", "pt_BR" : "Saint Helena", "en_US" : "Saint Helena", "en_GB" : "Saint Helena" } }, "code" : "SH", "locale" : "sh_SH"},
{"name" : { locales : { "pt_PT" : "Saint Kitts and Nevis", "pt_BR" : "Saint Kitts and Nevis", "en_US" : "Saint Kitts and Nevis", "en_GB" : "Saint Kitts and Nevis" } }, "code" : "KN", "locale" : "kn_KN"},
{"name" : { locales : { "pt_PT" : "Saint Lucia", "pt_BR" : "Saint Lucia", "en_US" : "Saint Lucia", "en_GB" : "Saint Lucia" } }, "code" : "LC", "locale" : "lc_LC"},
{"name" : { locales : { "pt_PT" : "Saint Pierre and Miquelon", "pt_BR" : "Saint Pierre and Miquelon", "en_US" : "Saint Pierre and Miquelon", "en_GB" : "Saint Pierre and Miquelon" } }, "code" : "PM", "locale" : "pm_PM"},
{"name" : { locales : { "pt_PT" : "Saint Vincent and the Grenadines", "pt_BR" : "Saint Vincent and the Grenadines", "en_US" : "Saint Vincent and the Grenadines", "en_GB" : "Saint Vincent and the Grenadines" } }, "code" : "VC", "locale" : "vc_VC"},
{"name" : { locales : { "pt_PT" : "Samoa", "pt_BR" : "Samoa", "en_US" : "Samoa", "en_GB" : "Samoa" } }, "code" : "WS", "locale" : "ws_WS"},
{"name" : { locales : { "pt_PT" : "San Marino", "pt_BR" : "San Marino", "en_US" : "San Marino", "en_GB" : "San Marino" } }, "code" : "SM", "locale" : "sm_SM"},
{"name" : { locales : { "pt_PT" : "Sao Tome and Principe", "pt_BR" : "Sao Tome and Principe", "en_US" : "Sao Tome and Principe", "en_GB" : "Sao Tome and Principe" } }, "code" : "ST", "locale" : "st_ST"},
{"name" : { locales : { "pt_PT" : "Saudi Arabia", "pt_BR" : "Saudi Arabia", "en_US" : "Saudi Arabia", "en_GB" : "Saudi Arabia" } }, "code" : "SA", "locale" : "sa_SA"},
{"name" : { locales : { "pt_PT" : "Senegal", "pt_BR" : "Senegal", "en_US" : "Senegal", "en_GB" : "Senegal" } }, "code" : "SN", "locale" : "sn_SN"},
{"name" : { locales : { "pt_PT" : "Serbia and Montenegro", "pt_BR" : "Serbia and Montenegro", "en_US" : "Serbia and Montenegro", "en_GB" : "Serbia and Montenegro" } }, "code" : "CS", "locale" : "cs_CS"},
{"name" : { locales : { "pt_PT" : "Seychelles", "pt_BR" : "Seychelles", "en_US" : "Seychelles", "en_GB" : "Seychelles" } }, "code" : "SC", "locale" : "sc_SC"},
{"name" : { locales : { "pt_PT" : "Sierra Leone", "pt_BR" : "Sierra Leone", "en_US" : "Sierra Leone", "en_GB" : "Sierra Leone" } }, "code" : "SL", "locale" : "sl_SL"},
{"name" : { locales : { "pt_PT" : "Singapore", "pt_BR" : "Singapore", "en_US" : "Singapore", "en_GB" : "Singapore" } }, "code" : "SG", "locale" : "sg_SG"},
{"name" : { locales : { "pt_PT" : "Slovakia", "pt_BR" : "Slovakia", "en_US" : "Slovakia", "en_GB" : "Slovakia" } }, "code" : "SK", "locale" : "sk_SK"},
{"name" : { locales : { "pt_PT" : "Slovenia", "pt_BR" : "Slovenia", "en_US" : "Slovenia", "en_GB" : "Slovenia" } }, "code" : "SI", "locale" : "si_SI"},
{"name" : { locales : { "pt_PT" : "Solomon Islands", "pt_BR" : "Solomon Islands", "en_US" : "Solomon Islands", "en_GB" : "Solomon Islands" } }, "code" : "SB", "locale" : "sb_SB"},
{"name" : { locales : { "pt_PT" : "Somalia", "pt_BR" : "Somalia", "en_US" : "Somalia", "en_GB" : "Somalia" } }, "code" : "SO", "locale" : "so_SO"},
{"name" : { locales : { "pt_PT" : "South Africa", "pt_BR" : "South Africa", "en_US" : "South Africa", "en_GB" : "South Africa" } }, "code" : "ZA", "locale" : "za_ZA"},
{"name" : { locales : { "pt_PT" : "South Georgia and the South Sandwich Islands", "pt_BR" : "South Georgia and the South Sandwich Islands", "en_US" : "South Georgia and the South Sandwich Islands", "en_GB" : "South Georgia and the South Sandwich Islands" } }, "code" : "GS", "locale" : "gs_GS"},
{"name" : { locales : { "pt_PT" : "Spain", "pt_BR" : "Spain", "en_US" : "Spain", "en_GB" : "Spain" } }, "code" : "ES", "locale" : "es_ES"},
{"name" : { locales : { "pt_PT" : "Sri Lanka", "pt_BR" : "Sri Lanka", "en_US" : "Sri Lanka", "en_GB" : "Sri Lanka" } }, "code" : "LK", "locale" : "lk_LK"},
{"name" : { locales : { "pt_PT" : "Sudan", "pt_BR" : "Sudan", "en_US" : "Sudan", "en_GB" : "Sudan" } }, "code" : "SD", "locale" : "sd_SD"},
{"name" : { locales : { "pt_PT" : "Suriname", "pt_BR" : "Suriname", "en_US" : "Suriname", "en_GB" : "Suriname" } }, "code" : "SR", "locale" : "sr_SR"},
{"name" : { locales : { "pt_PT" : "Svalbard and Jan Mayen", "pt_BR" : "Svalbard and Jan Mayen", "en_US" : "Svalbard and Jan Mayen", "en_GB" : "Svalbard and Jan Mayen" } }, "code" : "SJ", "locale" : "sj_SJ"},
{"name" : { locales : { "pt_PT" : "Swaziland", "pt_BR" : "Swaziland", "en_US" : "Swaziland", "en_GB" : "Swaziland" } }, "code" : "SZ", "locale" : "sz_SZ"},
{"name" : { locales : { "pt_PT" : "Sweden", "pt_BR" : "Sweden", "en_US" : "Sweden", "en_GB" : "Sweden" } }, "code" : "SE", "locale" : "se_SE"},
{"name" : { locales : { "pt_PT" : "Switzerland", "pt_BR" : "Switzerland", "en_US" : "Switzerland", "en_GB" : "Switzerland" } }, "code" : "CH", "locale" : "ch_CH"},
{"name" : { locales : { "pt_PT" : "Syrian Arab Republic", "pt_BR" : "Syrian Arab Republic", "en_US" : "Syrian Arab Republic", "en_GB" : "Syrian Arab Republic" } }, "code" : "SY", "locale" : "sy_SY"},
{"name" : { locales : { "pt_PT" : "Taiwan, Province of China", "pt_BR" : "Taiwan, Province of China", "en_US" : "Taiwan, Province of China", "en_GB" : "Taiwan, Province of China" } }, "code" : "TW", "locale" : "tw_TW"},
{"name" : { locales : { "pt_PT" : "Tajikistan", "pt_BR" : "Tajikistan", "en_US" : "Tajikistan", "en_GB" : "Tajikistan" } }, "code" : "TJ", "locale" : "tj_TJ"},
{"name" : { locales : { "pt_PT" : "Tanzania, United Republic of", "pt_BR" : "Tanzania, United Republic of", "en_US" : "Tanzania, United Republic of", "en_GB" : "Tanzania, United Republic of" } }, "code" : "TZ", "locale" : "tz_TZ"},
{"name" : { locales : { "pt_PT" : "Thailand", "pt_BR" : "Thailand", "en_US" : "Thailand", "en_GB" : "Thailand" } }, "code" : "TH", "locale" : "th_TH"},
{"name" : { locales : { "pt_PT" : "Timor-Leste", "pt_BR" : "Timor-Leste", "en_US" : "Timor-Leste", "en_GB" : "Timor-Leste" } }, "code" : "TL", "locale" : "tl_TL"},
{"name" : { locales : { "pt_PT" : "Togo", "pt_BR" : "Togo", "en_US" : "Togo", "en_GB" : "Togo" } }, "code" : "TG", "locale" : "tg_TG"},
{"name" : { locales : { "pt_PT" : "Tokelau", "pt_BR" : "Tokelau", "en_US" : "Tokelau", "en_GB" : "Tokelau" } }, "code" : "TK", "locale" : "tk_TK"},
{"name" : { locales : { "pt_PT" : "Tonga", "pt_BR" : "Tonga", "en_US" : "Tonga", "en_GB" : "Tonga" } }, "code" : "TO", "locale" : "to_TO"},
{"name" : { locales : { "pt_PT" : "Trinidad and Tobago", "pt_BR" : "Trinidad and Tobago", "en_US" : "Trinidad and Tobago", "en_GB" : "Trinidad and Tobago" } }, "code" : "TT", "locale" : "tt_TT"},
{"name" : { locales : { "pt_PT" : "Tunisia", "pt_BR" : "Tunisia", "en_US" : "Tunisia", "en_GB" : "Tunisia" } }, "code" : "TN", "locale" : "tn_TN"},
{"name" : { locales : { "pt_PT" : "Turkey", "pt_BR" : "Turkey", "en_US" : "Turkey", "en_GB" : "Turkey" } }, "code" : "TR", "locale" : "tr_TR"},
{"name" : { locales : { "pt_PT" : "Turkmenistan", "pt_BR" : "Turkmenistan", "en_US" : "Turkmenistan", "en_GB" : "Turkmenistan" } }, "code" : "TM", "locale" : "tm_TM"},
{"name" : { locales : { "pt_PT" : "Turks and Caicos Islands", "pt_BR" : "Turks and Caicos Islands", "en_US" : "Turks and Caicos Islands", "en_GB" : "Turks and Caicos Islands" } }, "code" : "TC", "locale" : "tc_TC"},
{"name" : { locales : { "pt_PT" : "Tuvalu", "pt_BR" : "Tuvalu", "en_US" : "Tuvalu", "en_GB" : "Tuvalu" } }, "code" : "TV", "locale" : "tv_TV"},
{"name" : { locales : { "pt_PT" : "Uganda", "pt_BR" : "Uganda", "en_US" : "Uganda", "en_GB" : "Uganda" } }, "code" : "UG", "locale" : "ug_UG"},
{"name" : { locales : { "pt_PT" : "Ukraine", "pt_BR" : "Ukraine", "en_US" : "Ukraine", "en_GB" : "Ukraine" } }, "code" : "UA", "locale" : "ua_UA"},
{"name" : { locales : { "pt_PT" : "United Arab Emirates", "pt_BR" : "United Arab Emirates", "en_US" : "United Arab Emirates", "en_GB" : "United Arab Emirates" } }, "code" : "AE", "locale" : "ae_AE"},
{"name" : { locales : { "pt_PT" : "United Kingdom", "pt_BR" : "United Kingdom", "en_US" : "United Kingdom", "en_GB" : "United Kingdom" } }, "code" : "GB", "locale" : "en_GB"},
{"name" : { locales : { "pt_PT" : "United States", "pt_BR" : "United States", "en_US" : "United States", "en_GB" : "United States" } }, "code" : "US", "locale" : "en_US"},
{"name" : { locales : { "pt_PT" : "United States Minor Outlying Islands", "pt_BR" : "United States Minor Outlying Islands", "en_US" : "United States Minor Outlying Islands", "en_GB" : "United States Minor Outlying Islands" } }, "code" : "UM", "locale" : "en_UM"},
{"name" : { locales : { "pt_PT" : "Uruguay", "pt_BR" : "Uruguay", "en_US" : "Uruguay", "en_GB" : "Uruguay" } }, "code" : "UY", "locale" : "uy_UY"},
{"name" : { locales : { "pt_PT" : "Uzbekistan", "pt_BR" : "Uzbekistan", "en_US" : "Uzbekistan", "en_GB" : "Uzbekistan" } }, "code" : "UZ", "locale" : "uz_UZ"},
{"name" : { locales : { "pt_PT" : "Vanuatu", "pt_BR" : "Vanuatu", "en_US" : "Vanuatu", "en_GB" : "Vanuatu" } }, "code" : "VU", "locale" : "vu_VU"},
{"name" : { locales : { "pt_PT" : "Venezuela", "pt_BR" : "Venezuela", "en_US" : "Venezuela", "en_GB" : "Venezuela" } }, "code" : "VE", "locale" : "ve_VE"},
{"name" : { locales : { "pt_PT" : "Viet Nam", "pt_BR" : "Viet Nam", "en_US" : "Viet Nam", "en_GB" : "Viet Nam" } }, "code" : "VN", "locale" : "vn_VN"},
{"name" : { locales : { "pt_PT" : "Virgin Islands, British", "pt_BR" : "Virgin Islands, British", "en_US" : "Virgin Islands, British", "en_GB" : "Virgin Islands, British" } }, "code" : "VG", "locale" : "vg_VG"},
{"name" : { locales : { "pt_PT" : "Virgin Islands, U.S.", "pt_BR" : "Virgin Islands, U.S.", "en_US" : "Virgin Islands, U.S.", "en_GB" : "Virgin Islands, U.S." } }, "code" : "VI", "locale" : "vi_VI"},
{"name" : { locales : { "pt_PT" : "Wallis and Futuna", "pt_BR" : "Wallis and Futuna", "en_US" : "Wallis and Futuna", "en_GB" : "Wallis and Futuna" } }, "code" : "WF", "locale" : "wf_WF"},
{"name" : { locales : { "pt_PT" : "Western Sahara", "pt_BR" : "Western Sahara", "en_US" : "Western Sahara", "en_GB" : "Western Sahara" } }, "code" : "EH", "locale" : "eh_EH"},
{"name" : { locales : { "pt_PT" : "Yemen", "pt_BR" : "Yemen", "en_US" : "Yemen", "en_GB" : "Yemen" } }, "code" : "YE", "locale" : "ye_YE"},
{"name" : { locales : { "pt_PT" : "Zambia", "pt_BR" : "Zambia", "en_US" : "Zambia", "en_GB" : "Zambia" } }, "code" : "ZM", "locale" : "zm_ZM"},
{"name" : { locales : { "pt_PT" : "Zimbabwe", "pt_BR" : "Zimbabwe", "en_US" : "Zimbabwe", "en_GB" : "Zimbabwe" } }, "code" : "ZW", "locale" : "zw_ZW"}
]
function CODAIconDashboard(parent, options, elements) {
	if (parent != null) {
		CODADashboard.call(this, parent, options);
		this.elements = elements;//(!!elements ? elements : []);
		this.addCSSClass('CODAIconDashboard');
		this.reducedDescription = false;
		this.columns = 4;
		this.page = 0;
		this.refreshing = false;
		this.pageSize = 0;
	}
}

KSystem.include([
	'CODAIcon',
	'CODADashboard'
], function() {
	CODAIconDashboard.prototype = new CODADashboard();
	CODAIconDashboard.prototype.constructor = CODAIconDashboard;

	CODAIconDashboard.prototype.prepareArgs = function() {
		var args = null;
		if (this.pageSize != 0) {
			args = {
				fields : 'elements,size',
				pageSize : this.pageSize,
				pageStartIndex : this.page * this.pageSize
			};
		}
		if (!!this.fieldslist) {
			args = {
				fields : this.fieldslist
			};
		}
		if (!!this.orderBy) {
			args.orderBy = this.orderBy;
		}
		if (this.filterFields !== null && this.filterFields.length !== 0 && !!this.filterValue.length !== 0) {
			for (var f in this.filterFields) {
				if (!this.filterValue[f]) {
					continue;
				}
				args[this.filterFields[f]] = 'm/' + this.filterValue[f] + '/i'
			}
		}
		return args;
	};

	CODAIconDashboard.prototype.load = function() {
		if (!!this.elements) {
			this.draw();
		}
		else if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			var args = this.prepareArgs();
			this.kinky.get(this, (this.options.links.feed.href.indexOf(':') == -1 ? 'rest:' : '') + KSystem.urlify(this.options.links.feed.href, args), { }, headers, 'onLoad');			
		}
		else {
			this.elements = [];
			this.draw();
		}
	};

	CODAIconDashboard.prototype.gotoPage = function(page) {
		this.page = page;
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		var args = this.prepareArgs();
		this.kinky.get(this, (this.options.links.feed.href.indexOf(':') == -1 ? 'rest:' : '') + KSystem.urlify(this.options.links.feed.href, args), { }, headers, 'onLoad');			
	};

	CODAIconDashboard.prototype.onNoContent = function(data, request) {
		if (this.page == 0) {
			this.elements = [];
			this.draw();
		}
		else {
			this.nomore = true;
		}
	};

	CODAIconDashboard.prototype.onLoad = function(data, request) {
		if (this.page != 0) {
			this.elements = !!data.elements ? this.elements.concat(data.elements) : this.elements;
			this.addPanels();
			CODAIconDashboard.wall(this, this.columns);
		}
		else {
			this.elements = !!data.elements ? data.elements : [];
			this.draw();
		}
	};

	CODAIconDashboard.prototype.refresh = function() {
		if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			this.removeAllChildren();
			this.page = 0;
			delete this.elements;
			this.elements = undefined;
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 750,
				from : {
					alpha : 1
				},
				go : {
					alpha : 0
				},
				onComplete : function(w, t) {
					w.removeAllChildren();
					w.load();
				},
				applyToElement : true
			});
		}
	};

	CODAIconDashboard.prototype.getIconFor = function(elementData) {
	};

	CODAIconDashboard.prototype.addPanels = function() {
		for (var d = this.page * this.pageSize; d != this.elements.length; d++) {
			var button = this.getIconFor(this.elements[d]);
			if (button instanceof CODAIcon) {
				if (this.reducedDescription) {
					button.addCSSClass('CODAIconReducedDescription');
				}
				this.appendChild(button);
			}
			else if (button instanceof Array) {
				for (var b in button) {
					if (this.reducedDescription) {
						button[b].addCSSClass('CODAIconReducedDescription');
					}
					this.appendChild(button[b]);
				}				
			}
		}
		if (!!this.refreshing) {
			this.refreshing = false;
			CODAIconDashboard.wall(this, this.columns);
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 0
				},
				go : {
					alpha : 1
				},
				applyToElement : true
			});
		}
	};

	CODAIconDashboard.prototype.draw = function() {
		CODADashboard.prototype.draw.call(this);
		this.margin = window.document.createElement('p');
		this.content.appendChild(this.margin);
	};

	CODAIconDashboard.prototype.visible = function() {
		CODAIconDashboard.wall(this, this.columns);
		CODADashboard.prototype.visible.call(this);
	};

	CODAIconDashboard.prototype.resize = function() {
		CODADashboard.prototype.resize.call(this);
		CODAIconDashboard.wall(this, this.columns);
	};

	CODAIconDashboard.wall = function(p, columns) {
		columns = columns || 4;
		var nc = columns || 4;
		var iw = 400;
		var tc = [];
		var lc = [];
		var gw = KDOM.getBrowserWidth() - 60;
		var pd = Math.round((KDOM.getBrowserWidth() / 1710) * 20);
		var fp = KDOM.getBrowserWidth() / 1710 * 0.7;
		var fs = Math.round(fp * 100);

		iw = Math.floor(gw / nc);
		for (var c = 0; c != nc; c++) {
			tc[c] = 0;
			lc[c] = iw * c;
		}

		var dpd = 0;
		if (p.nChildren < nc) {
			if (p.nChildren > 2) {
				nc = (Math.ceil(p.nChildren / 2) == nc ? Math.floor(p.nChildren / 2) : Math.ceil(p.nChildren / 2));
				dpd = Math.floor((gw - nc * iw) / 2);
			}
			else {
				dpd = Math.floor((gw - p.nChildren * iw) / 2);
			}
		}
		else if (p.nChildren < nc * 2) {
			nc = (Math.ceil(p.nChildren / 2) == nc ? Math.floor(p.nChildren / 2) : Math.ceil(p.nChildren / 2));
			dpd = Math.floor((gw - nc * iw) / 2);
		}
		else if (p.nChildren < nc * 3) {
			nc = (Math.ceil(p.nChildren / 3) == nc ? Math.floor(p.nChildren / 3) : Math.ceil(p.nChildren / 3));
			dpd = Math.floor((gw - nc * iw) / 2);
		}
		else {
			dpd = Math.floor((gw - nc * iw) / 2);		
		}
		p.setStyle({
			marginLeft : dpd + 'px'
		}, p.content);

		var tab = 1;
		var col = 0;
		for (var c in p.childWidgets()) {
			var w = p.childWidget(c);
			w.pad = pd;

			w.setStyle({
				position: 'absolute',
				padding: pd + 'px',
				maxWidth : (iw - 3 * pd)+ 'px',
				minWidth : (iw - 3 * pd) + 'px',
				width : (iw - 3 * pd) + 'px',
				top : tc[col] + 'px',
				left : (lc[col] + pd) + 'px',
				fontSize :  fs + '%'
			});

			tc[col] += w.panel.offsetHeight + pd;
			col++;
			if (col > nc - 1) {
				col = 0;
			}
		}

		var _wallit_h = 0;
		for (var i = 0; i != nc; i++) {
			if (_wallit_h < tc[i]) {
				_wallit_h = tc[i];
			}
		}

		KCSS.setStyle({
			width: '1px',
			height : '50px',
			position : 'absolute',
			top : (_wallit_h + pd) + 'px'
		}, [ p.margin ]);
	};

	KSystem.included('CODAIconDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAIcon(parent, options) {
	if (parent != null) {
		KWidget.call(this, parent);
		this.options = KSystem.merge(KSystem.clone(CODAIcon.OPTIONS), options || {});
		this.addCSSClass('CODAIcon');
		this.headers = {};

		this.menuActivator = window.document.createElement('button');
		this.menuActivator.className = 'CODAIconMenuActivator';
		this.menuActivator.innerHTML = '<i class="fa fa-ellipsis-v"></i>';
		KDOM.addEventListener(this.menuActivator, 'click', CODAIcon.openMenu);
		this.panel.appendChild(this.menuActivator);

		this.menu = window.document.createElement('nav');
		this.menu.className = 'CODAIconMenu';
		this.panel.appendChild(this.menu);
	}
}

KSystem.include([
	'KWidget'
], function() {
	CODAIcon.prototype = new KWidget();
	CODAIcon.prototype.constructor = CODAIcon;

	CODAIcon.prototype.refresh = function() {
		if (!!this.icon) {
			if (this.options.icon.indexOf('css:') == 0) {
				this.icon.className = this.options.icon.replace('css:', '') + ' fa-2x';
			}
			else if (this.options.icon.indexOf('text:') == 0) {
			}
			else {
				this.icon.src= this.options.icon;
				this.icon.style.cssFloat = 'left';
				this.icon.style.display = 'inline';
			}
		}


		this.h3.innerHTML = (!!this.options.title ? this.options.title : '[TITLE]');

		if (!!this.options.description) {
			this.descriptionTextNode.innerHTML = this.options.description;
		}

	};


	CODAIcon.prototype.visible = function() {
		if (this.display) {
			return;
    		}
    		if (!this.options) {
    			return;
    		}

		if (!!this.menu && this.menu.childNodes.length == 0) {
			this.panel.removeChild(this.menuActivator);
			this.panel.removeChild(this.menu);			
		}

		if (this.display || this.options.icon.indexOf('css:') == 0 || this.options.icon.indexOf('text:') == 0) {
			KWidget.prototype.visible.call(this);
		}
		else {
			var self = this;
			this.loadTimer = KSystem.addTimer(function() {
				if (!self.icon) {
					KSystem.removeTimer(self.loadTimer);
					delete self.loadTimer;			
					return;
				}

				var s = KCSS.getComputedStyle(self.icon);
				var width = Math.round(KDOM.normalizePixelValue(s.width));
				var height = Math.round(KDOM.normalizePixelValue(s.height));
				if (!width || !height || width == 0 || height == 0) {
					return;
				}

				KSystem.removeTimer(self.loadTimer);
				delete self.loadTimer;
				self.icon.style.height = height + 'px';
				KWidget.prototype.visible.call(self);
				self.parent.visible();
			}, 500, true);
		}
	};

	CODAIcon.prototype.destroy = function() {
		Kinky.unsetModal(this);
		KWidget.prototype.destroy.call(this);
	};

	CODAIcon.prototype.blur = function() {
		if (this.menuToggled) {
			this.menuToggled = false;

			this.removeCSSClass('Selected', this.menuActivator);
			KEffects.addEffect(this.menu,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				applyToElement : true,
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, tween) {
					if (!!w.menu) {
						w.menu.style.display = 'none';
					}
				}
			});			
		}
	};
	CODAIcon.prototype.load = function() {
		if (!!this.options.notifications) {
		}
		else {
			this.draw();
		}
	};

	CODAIcon.prototype.draw = function() {
		this.icon = undefined;
		{
			if (this.options.icon.indexOf('css:') == 0) {
				this.icon = window.document.createElement('i');
				this.icon.className = this.options.icon.replace('css:', '') + ' fa-2x';
			}
			else if (this.options.icon.indexOf('text:') == 0) {
				this.icon = window.document.createElement('i');
				this.icon.innerHTML = this.options.icon.replace('text:', '');
				this.icon.className = 'text-icon fa-2x';
			}
			else {
				this.icon = window.document.createElement('img');
				this.icon.src= this.options.icon;
				this.icon.style.cssFloat = 'left';
				this.icon.style.display = 'inline';
			}
		}
		this.panel.appendChild(this.icon);

		this.h3 = window.document.createElement('h3');
		{
			this.h3.innerHTML = (!!this.options.title ? this.options.title : '[TITLE]');
		}
		this.panel.appendChild(this.h3);

		if (!!this.options.actions) {

			for (var a in this.options.actions) {
				var data = this.options.actions[a];
				if (!data.icon || (!data.link && !data.events)) {
					continue;
				}
				if (!!data.link && a == 0) {
					this.icon['data-link'] = this.h3['data-link'] = data.link;
					KDOM.addEventListener(this.icon, 'click', CODAIcon.executeAction);
					KDOM.addEventListener(this.h3, 'click', CODAIcon.executeAction);
					continue;
				}

				var action = window.document.createElement('button');
				{
					action.setAttribute('type', 'button');
					var aicon = undefined;
					{
						if (data.icon.indexOf('css:') == 0) {
							aicon = window.document.createElement('i');
							aicon.className= data.icon.replace('css:', '');
						}
						else {
							aicon = window.document.createElement('img');
							aicon.src= data.icon;
						}
					}
					action.appendChild(aicon);
					action.appendChild(window.document.createTextNode(' '));
					action.appendChild(window.document.createTextNode(data.desc));

					if (!!data.link) {
						aicon['data-link'] = action['data-link'] = data.link;
						KDOM.addEventListener(action, 'click', CODAIcon.executeAction);
						if (a == 0) {
							this.icon['data-link'] = this.h3['data-link'] = data.link;
							KDOM.addEventListener(this.icon, 'click', CODAIcon.executeAction);
							KDOM.addEventListener(this.h3, 'click', CODAIcon.executeAction);
						}
					}
					if (!!data.events) {
						for (var e in data.events) {
							KDOM.addEventListener(action, data.events[e].name, data.events[e].callback);
						}
					}
				}
				this.menu.appendChild(action);
			}
		}

		if (!!this.options.description) {
			var description = window.document.createElement('div');
			{
				description.className = ' CODAIconDescription ';

				if (this.options.activate !== false) {
					var activate = window.document.createElement('button');
					{
						activate.setAttribute('type', 'button');
						var vicon = window.document.createElement('i');
						vicon.className = 'fa ' + this.options.activate;
						activate.appendChild(vicon);

						activate.style.cursor = 'help';
						var params = {
							text : gettext('$CODA_ICON_STATE_' + this.options.activate.toUpperCase()),
							delay : 300,
							gravity : {
								x : 'right',
								y : 'bottom'
							},
							max : {
								width : 200
							},
							offsetX : 35,
							offsetY : 0,
							offsetParent : activate,
							target : activate,
							cssClass : 'CODAInputBaloon CODAIconBaloon'
						};
						KBaloon.make(this, params);
					}
					description.appendChild(activate);

					this.addCSSClass("CODAIconActivated");
				}

				this.descriptionTextNode = window.document.createElement('p');
				this.descriptionTextNode.innerHTML = this.options.description;
				description.appendChild(this.descriptionTextNode);
			}
			this.panel.appendChild(description);
		}

		if (!!this.menu && this.menu.childNodes.length != 0) {
			KDOM.addEventListener(this.panel, 'contextmenu', function(e) {
				KDOM.stopEvent(e);
				return false;
			});

			KDOM.addEventListener(this.panel, 'mousedown', function(e) {
				var mouseEvent = KDOM.getEvent(e);
				KDOM.stopEvent(mouseEvent);

				if (mouseEvent.button != 2) {
					return;
				}

				var w = KDOM.getEventWidget(mouseEvent);
				if (!!w.menuActivator) {
					KDOM.fireEvent(w.menuActivator, 'click');
				}
			});
		}
		this.activate();
	};

	CODAIcon.executeAction = function(e) {
		var widget = KDOM.getEventWidget(e);
		if (!!widget.menuToggled) {
			Kinky.unsetModal(widget);
		}
		if (!!widget.parent.SELECTED && widget.parent.SELECTED.id == widget.id) {
			var dashboard = CODAFormIconDashboard.CURRENT;
			if (!!dashboard.FORM_OPENED) {
				dashboard.closeMe();
				dashboard.FORM_OPENED = undefined;
			}
			return;				
		}

		var ele = KDOM.getEventTarget(e);
		while(!ele['data-link']) {
			ele = ele.parentNode;
		}
		var shell = widget.getParent('KShell');
		var uri = ele['data-link'];

		if (!!shell.dashboard && !shell.dashboard.noFadeTransition) {
			window.top.CODA_NAVIGATION_FROM = 1;
			
			KEffects.addEffect(shell.dashboard, [
				{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 350,
					gravity : {
						x : 'right'
					},
					lock : {
						y : true
					},
					go : {
						x : 410
					},
					from : {
						x : 0
					}
				},
				{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 350,
					applyToElement : true,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					},
					onComplete : function() {
						if (uri.indexOf('#') == 0) {
							KBreadcrumb.dispatchURL({
								url : uri.substr(1)
							});
						}
						else {
							window.document.location = (uri.toLowerCase().indexOf('http') == 0 ? '' : CODA_VERSION) + uri;
						}
					}
				}]);
		}
		else {		
			if (uri.indexOf('#') == 0) {
				KBreadcrumb.dispatchURL({
					url : uri.substr(1)
				});
			}
			else {
				window.document.location = (uri.toLowerCase().indexOf('http') == 0 ? '' : CODA_VERSION) + uri;
			}
		}
	};

	CODAIcon.activateIcon = function(e) {
		var ele = KDOM.getEventTarget(e);
		while(!ele['data-link']) {
			ele = ele.parentNode;
		}
		var widget = KDOM.getEventWidget(e, 'CODAIcon');
		if (!!widget.menuToggled) {
			Kinky.unsetModal(widget);
		}
		if (uri.indexOf('#') == 0) {
			KBreadcrumb.dispatchURL({
				url : uri.substr(1)
			});
		}
		else {
			window.document.location = uri;
		}
	};

	CODAIcon.openMenu = function(e) {
		var widget = KDOM.getEventWidget(e, 'CODAIcon');

		if (!!widget.menuToggled) {
			Kinky.unsetModal(widget);
		}
		else {
			CODAIcon.closeAllOthers(widget);

			widget.addCSSClass('Selected', widget.menuActivator);
			widget.menu.style.display = 'block';

			var top = 30;
			var delta = Math.round(widget.menu.offsetHeight / 2);

			KCSS.setStyle({
				opacity : '0',
				top : (top - delta) + 'px',
				display: 'block'
			}, [
			widget.menu
			]);
			widget.menuToggled = true;
			Kinky.setModal(widget);

			KEffects.addEffect(widget.menu,  [ 
			{
				f : KEffects.easeOutExpo,
				type : 'move',
				duration : 200,
				applyToElement : true,
				lock : {
					x : true
				},
				go : {
					y : top
				},
				from : {
					y : top - delta
				}
			},
			{
				f : KEffects.easeOutExpo,
				type : 'fade',
				applyToElement : true,
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				}
			}
			]);
		}
	};	

	CODAIcon.closeAllOthers = function(form) {
		var active = Kinky.getActiveModalWidget();
		while (!!active && (active instanceof CODAIcon) &&  !!form && active.id != form.id) {
			Kinky.unsetModal(active);
			active = Kinky.getActiveModalWidget();
		}		
	};

	CODAIcon.OPTIONS = {
		icon : 'css:fa fa-ellipsis-h',
		activate : undefined,
		title : undefined,
		description : undefined,
		notifications : undefined,
		actions : undefined
	};

	KSystem.included('CODAIcon');
}, Kinky.CODA_INCLUDER_URL);
function CODAImageCrop(parent, target, _options) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.headers = {
			'Authorization' : 'OAuth2.0 ' + decodeURIComponent(this.getShell().data.module_token)
		}
		this.config = this.config || {};
		this._target = target;
		this.options = KSystem.merge(KSystem.clone(CODAImageCrop.OPTIONS), _options || {});
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAImageCrop.prototype = new KPanel();
	CODAImageCrop.prototype.constructor = CODAImageCrop;

	CODAImageCrop.prototype.refresh = function() {
		var target = this._target;
		var filter = this.filter;
		var shell = this.getShell();

		this.getParent('KFloatable').closeMe();
		KSystem.addTimer(function() {
			shell.showCloudBrowser(target, filter);
		}, 750);
	};

	CODAImageCrop.prototype.visible = function(tween) {
		if (this.display) {
			return;
		}
		KPanel.prototype.visible.call(this);

		this.setStyle({
			height : (this.getParent('KFloatable').height - 63) + 'px'
		}, KWidget.CONTENT_DIV);
	};

	CODAImageCrop.prototype.onClose = function() {
		this._target.setMetadata(this.metadata);
	};

	CODAImageCrop.prototype.load = function(element) {
		if (!this._target._filedata) {
			var uri = this._target.getValue();
			this.kinky.get(this, 'oauth:/filemanager/dimensions?uri=' + encodeURIComponent(uri), {}, { 'E-Tag' : '' + (new Date()).getTime()}, 'onImageSize');
			return;
		}	
		this.draw();
	};

	CODAImageCrop.prototype.onImageSize = function(data, request) {
 		this._target._filedata = data;
 		this.draw();
	};

	CODAImageCrop.prototype.draw = function() {
		var max_h = (KDOM.getBrowserHeight() - 300);

		if (!this.options.height) {
			this.options.height = Math.round(this.options.width * this.options.aspect_ratio);
		}

		if (this._target._filedata.width > 600) {
			this.options.scale_w = 600 / this._target._filedata.width;
		}
		if (this._target._filedata.height > max_h) {
			this.options.scale_h = max_h / this._target._filedata.height;
		}
		this.options.scale = Math.min(this.options.scale_h, this.options.scale_w);
		var _w = Math.round(this._target._filedata.width * this.options.scale);
		var _h = Math.round(this._target._filedata.height * this.options.scale);

		this.metadata = {
			y : 0,
			x : 0,
			width : (this.options.width > this.options.height ? _w : Math.round((_h * this.options.width) / this.options.height)),
			height : (this.options.height > this.options.width ? _h : Math.round((_w * this.options.height) / this.options.width))
		};
		this.options.aspect_ratio_w = this.metadata.width / this.metadata.height;
		this.options.aspect_ratio_h = this.metadata.height / this.metadata.width;
		this.metadata.aspect_ratio_x = 0;
		this.metadata.aspect_ratio_y = 0;

		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);

		this.image = window.document.createElement('img');
		{
			this.image.src = this._target.getValue();
			this.setStyle({
				maxWidth: '600px',
				maxHeight: max_h + 'px',
				width : 'auto',
				height : 'auto',
				position : 'absolute',
				top : '50px',
				left : '20px',
				opacity : '0.25'
			}, this.image);
		}
		this.content.appendChild(this.image);

		this.view = window.document.createElement('img');
		{
			this.view.src = this._target.getValue();
			this.setStyle({
				maxWidth: '600px',
				maxHeight: (KDOM.getBrowserHeight() - 300) + 'px',
				width : 'auto',
				height : 'auto',
				position : 'absolute',
				top : '50px',
				left : '20px',
				cursor : 'move',
				clip : 'rect(0px, ' + this.metadata.width + 'px, ' + this.metadata.height + 'px, 0px)'
			}, this.view);
			KDOM.addEventListener(this.view, 'mousedown', CODAImageCrop.startDrag);
		}
		this.content.appendChild(this.view);

		if (this.options.allow_resize) {
			this.corners = [];

			this.corners[0] = window.document.createElement('s');
			{
				this.corners[0].id = 'nw';
				this.setStyle({
					width : CODAImageCrop.CORNER_SIZE + 'px',
					height : CODAImageCrop.CORNER_SIZE + 'px',
					top : (CODAImageCrop.OFFSET.top - CODAImageCrop.CORNER_SIZE) + 'px',
					left : (CODAImageCrop.OFFSET.left - CODAImageCrop.CORNER_SIZE) + 'px',
					cursor : 'nw-resize'
				}, this.corners[0]);
				KDOM.addEventListener(this.corners[0], 'mousedown', CODAImageCrop.startResize);
			}
			this.content.appendChild(this.corners[0]);

			this.corners[1] = window.document.createElement('s');
			{
				this.corners[1].id = 'ne';
				this.setStyle({
					width : CODAImageCrop.CORNER_SIZE + 'px',
					height : CODAImageCrop.CORNER_SIZE + 'px',
					top : (CODAImageCrop.OFFSET.top - CODAImageCrop.CORNER_SIZE) + 'px',
					left : (this.metadata.width + CODAImageCrop.OFFSET.left) + 'px',
					cursor : 'ne-resize'
				}, this.corners[1]);
				KDOM.addEventListener(this.corners[1], 'mousedown', CODAImageCrop.startResize);
			}
			this.content.appendChild(this.corners[1]);

			this.corners[2] = window.document.createElement('s');
			{
				this.corners[2].id = 'se';
				this.setStyle({
					width : CODAImageCrop.CORNER_SIZE + 'px',
					height : CODAImageCrop.CORNER_SIZE + 'px',
					top : (this.metadata.height + CODAImageCrop.OFFSET.top) + 'px',
					left : (this.metadata.width + CODAImageCrop.OFFSET.left) + 'px',
					cursor : 'se-resize'
				}, this.corners[2]);
				KDOM.addEventListener(this.corners[2], 'mousedown', CODAImageCrop.startResize);
			}
			this.content.appendChild(this.corners[2]);

			this.corners[3] = window.document.createElement('s');
			{
				this.corners[3].id = 'sw';
				this.setStyle({
					width : CODAImageCrop.CORNER_SIZE + 'px',
					height : CODAImageCrop.CORNER_SIZE + 'px',
					top : (this.metadata.height + CODAImageCrop.OFFSET.top) + 'px',
					left : (CODAImageCrop.OFFSET.left - CODAImageCrop.CORNER_SIZE) + 'px',
					cursor : 'sw-resize'
				}, this.corners[3]);
				KDOM.addEventListener(this.corners[3], 'mousedown', CODAImageCrop.startResize);
			}
			this.content.appendChild(this.corners[3]);
		}


		var go = new KButton(this, gettext('$CONFIRM'), 'go', function(e) {
			KDOM.stopEvent(e);
			KDOM.getEventWidget(e).parent.confirm();
		});
		{
			go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
			go.addCSSClass('CODAFormButtonGo CODAFormButton');
		}
		this.appendChild(go);

		KPanel.prototype.draw.call(this);
	};

	CODAImageCrop.prototype.confirm = function() {
		this.metadata.scale = this.options.scale;
		this.metadata.x = Math.round(this.metadata.x / this.options.scale);
		this.metadata.y = Math.round(this.metadata.y / this.options.scale);
		this.metadata.width = Math.round(this.metadata.width / this.options.scale);
		this.metadata.height = Math.round(this.metadata.height / this.options.scale);

		if (!this.options.only_metadata && !!this.options.target_uri && !!this.options.media_server && !!this.options.out_mime) {
			this._target.removeEventListener('change', CODAImageCrop.targetChange);

			var params = { 
				mediaServer : this.options.media_server,
				uri_image : this.options.target_uri, 
				external_uri_image : this._target.getValue(), 
				out_format : this.options.out_mime,
				crop_w : this.metadata.width,
				crop_h : this.metadata.height,
				crop_x : this.metadata.x,
				crop_y : this.metadata.y
			};
			this.kinky.post(this, 'oauth:/filemanager/cropimage', params, null, 'onImageCrop');
		}
		else {
			this._target.setMetadata(this.metadata);
			this.getParent('KFloatable').closeMe();
		}
	};

	CODAImageCrop.prototype.onImageCrop = function(data, request) {
		if (!!this.options.force_resize) {
			var params = { 
				mediaServer : this.options.media_server,
				uri_image : this.options.target_uri, 
				external_uri_image : data.href, 
				out_format : this.options.out_mime,
				resize_w : this.options.width,
				resize_h : this.options.height
			};
			if (!!this.options.force_resize_to) {
				if (this.options.force_resize_to == 'both') {
					params.resize_w = this.options.width;
					params.resize_h = this.options.height;
				}
				else if (this.options.force_resize_to == 'width') {
					params.resize_w = this.options.width;
					params.resize_h = Math.ceil((this.options.width * data.height) / data.width);
				}
				else {
					params.resize_h = this.options.height;
					params.resize_w = Math.ceil((this.options.height * data.width) / data.height);
				}
			}
			
			this.kinky.post(this, 'oauth:/filemanager/resizeimage', params, null, 'onImageResize');
		}
		else {
			this.onImageResize(data);
		}
	};

	CODAImageCrop.prototype.onImageResize = function(data, request) {
		this._target.setValue(data.href);
		this._target.addEventListener('change', CODAImageCrop.targetChange);
		this.getParent('KFloatable').closeMe();
	};

	CODAImageCrop.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAImageCrop.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAImageCrop.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_USERS_NOT_FOUND'));
	};

	CODAImageCrop.prototype.onPost = CODAImageCrop.prototype.onCreated = function(data, request){
		this.getShell().dashboard.refresh();
		this.parent.closeMe();
	};

	CODAImageCrop.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAImageCrop.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAImageCrop.addCrop = function(target, options) {
		target._crop_options = options;
		target.addEventListener('change', CODAImageCrop.targetChange);
	};

	CODAImageCrop.targetChange = function(event) {
		var target = KDOM.getEventWidget(event);
		KSystem.addTimer(function() {
			target.getShell().showImageCrop(target, target._crop_options);
		}, 850);
	};

	CODAImageCrop.startDrag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var e = CODAImageCrop.getElementCoords(w.image);
		w.width = Math.round(KDOM.normalizePixelValue(KCSS.getComputedStyle(w.image).width));
		w.height = Math.floor(KDOM.normalizePixelValue(KCSS.getComputedStyle(w.image).height));
		w.offsety = mouseEvent.clientY - e.y - w.metadata.y;
		w.offsetx = mouseEvent.clientX - e.x - w.metadata.x;

		KDOM.addEventListener(w.content, 'mouseup', CODAImageCrop.stopDrag);
		KDOM.addEventListener(w.content, 'mousemove', CODAImageCrop.drag);		
	};
	
	CODAImageCrop.drag = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var e = CODAImageCrop.getElementCoords(w.image);
		var y = mouseEvent.clientY - e.y;
		var x = mouseEvent.clientX - e.x;

		w.metadata.y = Math.min(w.height - w.metadata.height, Math.max(0, y - w.offsety));
		w.metadata.x = Math.min(w.width - w.metadata.width, Math.max(0, x - w.offsetx));

		w.view.style.clip = 'rect(' + w.metadata.y + 'px, ' + (w.metadata.x + w.metadata.width) + 'px, ' + (w.metadata.y + w.metadata.height) + 'px, ' + 	w.metadata.x + 'px)';
		w.corners[0].style.top = (w.corners[1].style.top = (w.metadata.y + CODAImageCrop.OFFSET.top - CODAImageCrop.CORNER_SIZE) + 'px');
		w.corners[0].style.left = (w.corners[3].style.left = (w.metadata.x + CODAImageCrop.OFFSET.left - CODAImageCrop.CORNER_SIZE) + 'px');
		w.corners[2].style.top = (w.corners[3].style.top = (w.metadata.y + w.metadata.height + CODAImageCrop.OFFSET.top) + 'px');
		w.corners[2].style.left = (w.corners[1].style.left = (w.metadata.x + w.metadata.width + CODAImageCrop.OFFSET.left) + 'px');
	};
	
	CODAImageCrop.stopDrag = function(event) {
		var w = KDOM.getEventWidget(event);
		w.offsety = 0;
		w.offsetx = 0;
		KDOM.removeEventListener(w.content, 'mouseup', CODAImageCrop.stopDrag);
		KDOM.removeEventListener(w.content, 'mousemove', CODAImageCrop.drag);
	};

	CODAImageCrop.startResize = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var corner = KDOM.getEventTarget(event);
		var w = KDOM.getEventWidget(event);

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var cornerpos = CODAImageCrop.getElementCoords(corner);
		w.offsety = mouseEvent.clientY - cornerpos.y;
		w.offsetx = mouseEvent.clientX - cornerpos.x;

		w.width = Math.round(KDOM.normalizePixelValue(KCSS.getComputedStyle(w.image).width));
		w.height = Math.floor(KDOM.normalizePixelValue(KCSS.getComputedStyle(w.image).height));
		switch (corner.id) {
			case 'nw' : {
				CODAImageCrop.CURRENT_CORNER = 0;
				break;
			}
			case 'ne' : {
				CODAImageCrop.CURRENT_CORNER = 1;
				break;
			}
			case 'se' : {
				CODAImageCrop.CURRENT_CORNER = 2;
				break;
			}
			case 'sw' : {
				CODAImageCrop.CURRENT_CORNER = 3;
				break;
			}
		}

		KDOM.removeEventListener(w.view, 'mousedown', CODAImageCrop.startDrag);
		KDOM.addEventListener(w.content, 'mouseup', CODAImageCrop.stopResize);
		KDOM.addEventListener(w.content, 'mousemove', CODAImageCrop.resize);	
	};
	
	CODAImageCrop.resize = function(event) {
		var mouseEvent = KDOM.getEvent(event);
		var w = KDOM.getEventWidget(event);
		var corner = w.corners[CODAImageCrop.CURRENT_CORNER];

		if (mouseEvent.preventDefault) {
			mouseEvent.preventDefault();
		}
		else {
			mouseEvent.returnValue = false;
		}
		
		var e = CODAImageCrop.getElementCoords(w.image);
		var y = mouseEvent.clientY - e.y;
		var x = mouseEvent.clientX - e.x;

		var yoffset = 0;
		var xoffset = 0;

		switch (CODAImageCrop.CURRENT_CORNER) {
			case 0 : {
				y += (CODAImageCrop.CORNER_SIZE - w.offsety);
				x += (CODAImageCrop.CORNER_SIZE - w.offsetx);

				y = Math.min(w.height, w.metadata.y + w.metadata.height - 1, Math.max(0, y));
				x = Math.min(w.width, w.metadata.x + w.metadata.width - 1, Math.max(0, x));
				yoffset = y - w.metadata.y;
				xoffset = x - w.metadata.x;

				if (w.options.keep_aspect_ratio) {
					if (w.options.aspect_ratio_h > w.options.aspect_ratio_w) {
						w.metadata.aspect_ratio_x += w.options.aspect_ratio_w * yoffset;
						xoffset = Math.round(w.metadata.aspect_ratio_x);
						if (xoffset != 0) {
							w.metadata.aspect_ratio_x = 0;
						}1
					}
					else {
						w.metadata.aspect_ratio_y += w.options.aspect_ratio_h * xoffset;
						yoffset = Math.round(w.metadata.aspect_ratio_y);
						if (yoffset != 0) {
							w.metadata.aspect_ratio_y = 0;
						}1
					}
					x = w.metadata.x + xoffset;
					y = w.metadata.y + yoffset;
				}

				if (!w.options.lock_w) {
					w.metadata.x = x;
					w.metadata.width -= xoffset;
				}
				if (!w.options.lock_h) {
					w.metadata.y = y;
					w.metadata.height -= yoffset;
				}
				break;
			}
			case 1 : {
				y += (CODAImageCrop.CORNER_SIZE - w.offsety);
				x -= w.offsetx;

				y = Math.min(w.height, w.metadata.y + w.metadata.height - 1, Math.max(0, y));
				x = Math.min(w.width, Math.max(0, x, w.metadata.x + 1));
				yoffset = y - w.metadata.y;
				xoffset = x - (w.metadata.x + w.metadata.width);

				if (w.options.keep_aspect_ratio) {
					if (w.options.aspect_ratio_h > w.options.aspect_ratio_w) {
						w.metadata.aspect_ratio_x += w.options.aspect_ratio_w * -yoffset;
						xoffset = Math.round(w.metadata.aspect_ratio_x);
						if (xoffset != 0) {
							w.metadata.aspect_ratio_x = 0;
						}1
					}
					else {
						w.metadata.aspect_ratio_y += w.options.aspect_ratio_h * -xoffset;
						yoffset = Math.round(w.metadata.aspect_ratio_y);
						if (yoffset != 0) {
							w.metadata.aspect_ratio_y = 0;
						}1
					}

					y = w.metadata.y + yoffset;
				}


				if (!w.options.lock_w) {
					w.metadata.width += xoffset;
				}
				if (!w.options.lock_h) {
					w.metadata.height -= yoffset;
					w.metadata.y = y;
				}
				break;
			}
			case 2 : {
				y -= w.offsety;
				x -= w.offsetx;

				y = Math.min(w.height, Math.max(0, y, w.metadata.y + 1));
				x = Math.min(w.width, Math.max(0, x, w.metadata.x + 1));
				yoffset = y - (w.metadata.y + w.metadata.height);
				xoffset = x - (w.metadata.x + w.metadata.width);

				if (w.options.keep_aspect_ratio) {
					if (w.options.aspect_ratio_h > w.options.aspect_ratio_w) {
						w.metadata.aspect_ratio_x += w.options.aspect_ratio_w * yoffset;
						xoffset = Math.round(w.metadata.aspect_ratio_x);
						if (xoffset != 0) {
							w.metadata.aspect_ratio_x = 0;
						}1
					}
					else {
						w.metadata.aspect_ratio_y += w.options.aspect_ratio_h * xoffset;
						yoffset = Math.round(w.metadata.aspect_ratio_y);
						if (yoffset != 0) {
							w.metadata.aspect_ratio_y = 0;
						}1
					}
				}

				if (!w.options.lock_w) {
					w.metadata.width += xoffset;
				}
				if (!w.options.lock_h) {
					w.metadata.height += yoffset;
				}
				break;
			}
			case 3 : {
				y -= w.offsety;
				x += (CODAImageCrop.CORNER_SIZE - w.offsetx);

				y = Math.min(w.height, Math.max(0, y, w.metadata.y + 1));
				x = Math.min(w.width, w.metadata.x + w.metadata.width - 1, Math.max(0, x));
				yoffset = y - (w.metadata.y + w.metadata.height);
				xoffset = x - w.metadata.x;

				if (w.options.keep_aspect_ratio) {
					if (w.options.aspect_ratio_h > w.options.aspect_ratio_w) {
						w.metadata.aspect_ratio_x += w.options.aspect_ratio_w * -yoffset;
						xoffset = Math.round(w.metadata.aspect_ratio_x);
						if (xoffset != 0) {
							w.metadata.aspect_ratio_x = 0;
						}1
					}
					else {
						w.metadata.aspect_ratio_y += w.options.aspect_ratio_h * -xoffset;
						yoffset = Math.round(w.metadata.aspect_ratio_y);
						if (yoffset != 0) {
							w.metadata.aspect_ratio_y = 0;
						}1
					}
					x = w.metadata.x + xoffset;
				}

				if (!w.options.lock_w) {
					w.metadata.x = x;
					w.metadata.width -= xoffset;
				}
				if (!w.options.lock_h) {
					w.metadata.height += yoffset;
				}
				break;
			}
		}

		w.view.style.clip = 'rect(' + w.metadata.y + 'px, ' + (w.metadata.x + w.metadata.width) + 'px, ' + (w.metadata.y + w.metadata.height) + 'px, ' + w.metadata.x + 'px)';
		w.corners[0].style.top = (w.corners[1].style.top = (w.metadata.y + CODAImageCrop.OFFSET.top - CODAImageCrop.CORNER_SIZE) + 'px');
		w.corners[0].style.left = (w.corners[3].style.left = (w.metadata.x + CODAImageCrop.OFFSET.left - CODAImageCrop.CORNER_SIZE) + 'px');
		w.corners[2].style.top = (w.corners[3].style.top = (w.metadata.y + w.metadata.height + CODAImageCrop.OFFSET.top) + 'px');
		w.corners[2].style.left = (w.corners[1].style.left = (w.metadata.x + w.metadata.width + CODAImageCrop.OFFSET.left) + 'px');
	};
	
	CODAImageCrop.stopResize = function(event) {
		var w = KDOM.getEventWidget(event);
		w.offsety = 0;
		w.offsetx = 0;
		CODAImageCrop.CURRENT_CORNER = undefined;
		KDOM.removeEventListener(w.content, 'mouseup', CODAImageCrop.stopResize);
		KDOM.removeEventListener(w.content, 'mousemove', CODAImageCrop.resize);
		KDOM.addEventListener(w.view, 'mousedown', CODAImageCrop.startDrag);
	};

	CODAImageCrop.getElementCoords = function(element) {
		var e = element;
		var ex = 0;
		var ey = 0;
		do {
			ex += e.offsetLeft;
			ey += e.offsetTop;
		} while (e = e.offsetParent);

		return { x : ex, y : ey };
	};

	CODAImageCrop.CURRENT_CORNER = undefined;
	CODAImageCrop.OFFSET = { top : 50, left : 20};
	CODAImageCrop.OPTIONS = { allow_resize : true, keep_aspect_ratio : false, force_resize : false, only_metadata : true, aspect_ratio : 1, width : 100, scale : 1, scale_h : 1, scale_w : 1, lock_h : false, lock_w : false };
	CODAImageCrop.CORNER_SIZE = 8;
	KSystem.included('CODAImageCrop');
}, Kinky.CODA_INCLUDER_URL);
function CODAListItem(parent, isTitle) {
	if (parent != null) {

		KDropPanel.call(this, parent);
		this.selected = false;

		this.titleContainer = window.document.createElement('div');
		this.titleContainer.className = 'CODAListItemTitle';
		this.panel.appendChild(this.titleContainer);
		this.addCSSClass('CODAListItem');

		if (!isTitle) {
			this.menuActivator = window.document.createElement('button');
			this.menuActivator.className = 'CODAListItemMenuActivator';
			this.menuActivator.innerHTML = '<i class="fa fa-ellipsis-v"></i>';
			KDOM.addEventListener(this.menuActivator, 'click', CODAListItem.openMenu);
			this.panel.appendChild(this.menuActivator);

			this.menu = window.document.createElement('nav');
			this.menu.className = 'CODAListItemMenu';
			this.panel.appendChild(this.menu);
		}

		this.isTitle = isTitle;
		this.recordID = 'href';
		this.nElements = 0;
	}
}

KSystem.include([
	'KPanel',
	'KDropPanel',
	'KButton',
	'KUploadButton',
	'KCheckBox'
], function() {
	CODAListItem.prototype = new KDropPanel();
	CODAListItem.prototype.constructor = CODAListItem;

	CODAListItem.prototype.setTitle = function() {
		return;
	};

	CODAListItem.prototype.droppedIt = function(widgetDropped) {
		this.onDrop(widgetDropped);
	};


	CODAListItem.prototype.visible = function() {
		if (this.display) {
			return;
    		}
		KDropPanel.prototype.visible.call(this);
		if (!this.isTitle && !!this.menu && this.menu.childNodes.length == 0) {
			this.panel.removeChild(this.menuActivator);
			this.panel.removeChild(this.menu);	
		}
	};

	CODAListItem.prototype.appendChild = function(child, target) {
		if (!this.isTitle && (child instanceof KButton || child instanceof KUploadButton)) {
			if (!!child.hasClickOutListener) {
				KDOM.removeEventListener(child.input, 'click', child.callback);
				KDOM.addEventListener(child.input, 'click', function(e) {
					var w = KDOM.getEventWidget(e, 'CODAListItem');
					if (!w.selected) {
						CODAListItem.selectElement(w);
					}
					if (!!w.menuToggled) {
						CODAListItem.openMenu(e);
					}
					child.callback(e);
				});
				child.hasClickOutListener = true;
			}
			KDropPanel.prototype.appendChild.call(this, child, 'menu');
		}
		else {
			this.nElements++;
			KDropPanel.prototype.appendChild.call(this, child, target);			
		}
	};

	CODAListItem.prototype.onDrop = function(w) {
		if (!!this.orderBy) {
			if (w instanceof KWidget) {
				this.setStyle({
					backgroundColor : 'transparent'
				});
				w.move(this.data[this.orderBy]);
			}
			else {
				var args = {};
				args[this.recordID] = w;
				this.kinky.get(this, 'rest:' + KSystem.urlify(this.parent.data.links.feed.href, args), {}, null, 'onWidgetToMove');
			}
		}
		else {
			if (w instanceof KWidget) {
				this.setStyle({
					backgroundColor : 'transparent'
				});
				w.move(this.parent.indexOf(this));
			}			
		}
	};

	CODAListItem.prototype.onRemove = function(data, request) {
		KEffects.addEffect(this, {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 800,
			go : {
				alpha : 0
			},
			from : {
				alpha : 1
			},
			onComplete : function(w, t) {
				w.parent.refreshPages();
				//KPagedList.prototype.removeChild.call(w.parent, w);
			}
		});
	};

	CODAListItem.prototype.onNoContent = function(data, request) {
		this.setStyle({
			backgroundColor : 'transparent'
		});
	};

	CODAListItem.prototype.onWidgetToMove = function(data, request) {
		if (!data.elements || data.elements.length == 0) {
			return;
		}
		var w = null;
		eval('w = new ' + this.className + '(this.parent)');
		w.data = data.elements[0];
		w.recordID = this.recordID;
		w.orderBy = this.orderBy;
		w.toDelete = true;
		w.move(this.data[this.orderBy]);
	};

	CODAListItem.prototype.prepareDrag = function() {
		var node = this.panel.cloneNode(true);
		node.removeChild(node.childNodes[0]);
		for (; node.childNodes[0].childNodes.length > 1;) {
			node.childNodes[0].removeChild(node.childNodes[0].childNodes[1]);
		}
		node.style.display = 'block';
		return node;
	};

	CODAListItem.prototype.prepareDrop = function(dpanel) {
		var node = this.panel.cloneNode(true);
		node.removeChild(node.childNodes[0]);
		for (; node.childNodes[0].childNodes.length > 1;) {
			node.childNodes[0].removeChild(node.childNodes[0].childNodes[1]);
		}

		KDOM.addEventListener(node.childNodes[0], 'click', function(event) {
			var widget = Kinky.getWidget(KDOM.getEventTarget(event).parentNode.parentNode.name);
			KBreadcrumb.dispatchURL({
				hash : '/edit' + widget.data.href
			})
		});

		return node;
	};

	CODAListItem.prototype.addMenuButtons = function() {
	};

	CODAListItem.prototype.draw = function() {
		if (!this.isTitle) {
			for (var f in this.parent.table) {
				var h2 = window.document.createElement('h2');
				var value = null;
				try {
					eval('value = this.data.' + f + ';')
				}
				catch (e) {
				}
				if (value != undefined) {
					h2.innerHTML = value;
				}
				this.titleContainer.appendChild(h2);
			}
		}
		else {
			this.titleContainer.style.cursor = 'normal';
			var list = this.getParent('CODAList');
			for (var f in this.parent.table) {
				var h2 = window.document.createElement('h2');
				h2.innerHTML = this.parent.table[f].label;
				if (!!this.parent.table[f].orderBy) {
					if (!!list.orderBy && list.orderBy.indexOf(f) != -1) {
						KCSS.addCSSClass('CODAListOrder' + (list.orderBy[0] == '-' ? 'DESC' : 'ASC'), h2);
						list.lastOrderIndex = this.titleContainer.childNodes.length;
					} 
					KCSS.addCSSClass('CODAListOrder', h2);
					h2.style.cursor = 'pointer';
					h2.setAttribute('onclick', 'CODAList.reorder(event, \'' + f + '\')');
				}
				this.titleContainer.appendChild(h2);
			}
		}

		if (!!this.data.cssClass) {
			this.addCSSClass(this.data.cssClass);
		}

		if (!this.isTitle) {
			if (!!this.orderBy) {
				KDOM.addEventListener(this.panel, 'mouseover', function(e) {
					if (KDraggable.dragging) {
						KDOM.getEventWidget(e).setStyle({
							backgroundColor : 'rgba(0, 146, 191, 0.25)'
						});
					}
				});

				KDOM.addEventListener(this.panel, 'mouseout', function(e) {
					if (KDraggable.dragging) {
						KDOM.getEventWidget(e).setStyle({
							backgroundColor : 'transparent'
						});
					}
				});
			}

			KDOM.addEventListener(this.panel, 'contextmenu', function(e) {
				KDOM.stopEvent(e);
				return false;
			});

			KDOM.addEventListener(this.panel, 'mousedown', function(e) {
				var mouseEvent = KDOM.getEvent(e);
				KDOM.stopEvent(mouseEvent);

				if (mouseEvent.button != 2) {
					return;
				}

				var w = KDOM.getEventWidget(mouseEvent);
				if (!!w.menuActivator) {
					KDOM.fireEvent(w.menuActivator, 'click');
				}
			});

			if (this.getParent('CODAList').options.edit_button) {
				var detailsBt = new KButton(this, ' ', 'details', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAListItem');
					Kinky.unsetModal(widget);
					widget.showDetails();
				});
				{
					detailsBt.hasClickOutListener = true;
					detailsBt.setText('<i class="fa fa-pencil"></i> ' + gettext('$KLISTITEM_EDIT_ACTION'), true);
				}
				this.appendChild(detailsBt);
			}

			if (this.getParent('CODAList').options.delete_button) {
				var deleteBt = new KButton(this, ' ', 'remove', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAListItem');
					Kinky.unsetModal(widget);
					widget.remove();
				});
				{
					deleteBt.hasClickOutListener = true;
					deleteBt.setText('<i class="fa fa-trash-o"></i> ' + gettext('$KLISTITEM_DELETE_ACTION'), true);
				}
				this.appendChild(deleteBt);
			}

			this.addMenuButtons();

			KDOM.addEventListener(this.titleContainer, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				var e = KDOM.getEvent(event);
				widget.onClick((e.ctrlKey == true || e.ctrlKey == 1 ? 17 : 0) + (e.altKey == true || e.altKey == 1 ? 18 : 0) + (e.shiftKey == true || e.shiftKey == 1 ? 16 : 0));
			});

			this.addActionListener(CODAListItem.unmark, [ '/close/form' + this.data.href ]);
		}

		KDropPanel.prototype.draw.call(this);
	};

	CODAListItem.prototype.onMove = function(data, request) {
		CODAForm.hideMessage(this.getParent('CODAForm').id);
		this.parent.refreshPages();
		if (!!this.toDelete) {
			this.destroy();
		}
	};

	CODAListItem.prototype.move = function(order) {
	};

	CODAListItem.prototype.remove = function() {
	};

	CODAListItem.prototype.showDetails = function() {
		if (!this.selected) {
			CODAListItem.selectElement(this);
		}
		KBreadcrumb.dispatchURL({
			hash : '/edit' + this.data.href
		});
	};

	CODAListItem.prototype.destroy = function() {
		Kinky.unsetModal(this);
		KDropPanel.prototype.destroy.call(this);
	};

	CODAListItem.prototype.blur = function() {
		if (this.menuToggled) {
			this.menuToggled = false;

			this.removeCSSClass('Selected', this.menuActivator);
			KEffects.addEffect(this.menu,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				applyToElement : true,
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, tween) {
					if (!!w && !!w.menu) {
						w.menu.style.display = 'none';
					}
				}
			});			
		}
	};

	CODAListItem.unmark = function(widget, hash) {
		if (widget.selected) {
			widget.onClick();
		}
	};

	CODAListItem.prototype.onClick = function(keyPressed) {
		if (!!this.menuToggled) {
			Kinky.unsetModal(this);
		}
		CODAListItem.selectElement(this, keyPressed);
		if (keyPressed == 0 && !!this.selected) {
			if (!!this.menu && this.menu.childNodes.length != 0 && this.menu.childNodes[0].childNodes[0].childNodes[0].id != 'remove') {
				KDOM.fireEvent(this.menu.childNodes[0].childNodes[0].childNodes[0], 'click');
			}			
		}
		this.parent.parent.onClickItem(this.data);
	};

	CODAListItem.openMenu = function(e) {
		var widget = KDOM.getEventWidget(e, 'CODAListItem');

		if (!!widget.menuToggled) {
			Kinky.unsetModal(widget);
		}
		else {
			CODAListItem.closeAllOthers(widget);

			widget.addCSSClass('Selected', widget.menuActivator);
			widget.menu.style.display = 'block';

			var top = 27;
			var delta = Math.round(widget.menu.offsetHeight / 2);

			KCSS.setStyle({
				opacity : '0',
				top : (top - delta) + 'px',
				display: 'block'
			}, [
			widget.menu
			]);
			widget.menuToggled = true;
			Kinky.setModal(widget);

			KEffects.addEffect(widget.menu,  [ 
			{
				f : KEffects.easeOutExpo,
				type : 'move',
				duration : 200,
				applyToElement : true,
				lock : {
					x : true
				},
				go : {
					y : top
				},
				from : {
					y : top - delta
				}
			},
			{
				f : KEffects.easeOutExpo,
				type : 'fade',
				applyToElement : true,
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				}
			}
			]);
		}
	};

	CODAListItem.selectElement = function(widget, keyPressed) {
		var parentHref = widget.parent;

		if (parentHref.parent.onechoice) {
			CODAListItem.SELECTED_FOR_SHIFT = undefined;
			if (widget.selected) {
				widget.removeCSSClass('CODAListItemMarked');
			}
			else {
				var siblings = parentHref.childWidgets();

				for (var s in siblings) {
					siblings[s].removeCSSClass('CODAListItemMarked');
					siblings[s].selected = false;
				}

				widget.addCSSClass('CODAListItemMarked');
			}
			widget.selected = !widget.selected;
		}
		else if (!parentHref.parent.onechoice && keyPressed == 16) {
			CODAGenericShell.closeAllOthers(widget.getParent('KFloatable'));

			var start = parentHref.indexOf(CODAListItem.SELECTED_FOR_SHIFT);
			var end = parentHref.indexOf(widget);
			var increment = start < end ? 1 : -1;

			for (var i = start; i != end + increment; i = i + increment) {
				var sibling = parentHref.childAt(i);
				sibling.addCSSClass('CODAListItemMarked');
				sibling.selected = true;
			}

			CODAListItem.SELECTED_FOR_SHIFT = widget;
		}
		else if (!parentHref.parent.onechoice && keyPressed != 17) {
			CODAListItem.SELECTED_FOR_SHIFT = widget;
			if (widget.selected) {
				widget.removeCSSClass('CODAListItemMarked');
			}
			else {
				var siblings = parentHref.childWidgets();

				for (var s in siblings) {
					siblings[s].removeCSSClass('CODAListItemMarked');
					siblings[s].selected = false;
				}

				widget.addCSSClass('CODAListItemMarked');
			}
			widget.selected = !widget.selected;
		}
		else {
			CODAGenericShell.closeAllOthers(widget.getParent('KFloatable'));

			CODAListItem.SELECTED_FOR_SHIFT = widget;
			if (widget.selected) {
				widget.removeCSSClass('CODAListItemMarked');
			}
			else {
				widget.addCSSClass('CODAListItemMarked');
			}
			widget.selected = !widget.selected;
		}
	};

	CODAListItem.closeAllOthers = function(form) {
		var active = Kinky.getActiveModalWidget();
		while (!!active && (active instanceof CODAListItem) &&  !!form && active.id != form.id) {
			Kinky.unsetModal(active);
			active = Kinky.getActiveModalWidget();
		}		
	};

	CODAListItem.SELECTED_FOR_SHIFT = undefined;
	KSystem.included('CODAListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAList(parent, data, schema, _options) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.options = KSystem.merge(KSystem.clone(CODAList.OPTIONS), _options || {});
		this.data = data;
		this.schema = schema;
		this.elements = null;
		this.addCSSClass('CODAList');
		this.pageSize = 0;
		this.totalSize = 0;
		this.onechoice = this.options.one_choice || true;
		this.orderBy = null;
		this.filterFields = null;
		this.filterValue = [];
		this.headers = {};
		this.staticData = false;
		this.fieldslist = 'elements,size';

		this.actions = [];
		this._postponedactions = [];
		this._postponedfilters = [];

		this.table = {
			"title" : {
				label : 'Título'
			}
		};

		this.list = new KPagedList(this);
		{
			this.list.pageSize = this.pageSize;
			this.list.no_children = true;
			this.list.nElements = 0;

			this.tableHeader = window.document.createElement('div');
			this.tableHeader.className = 'KPagedListContent';

			this.list.panel.insertBefore(this.tableHeader, this.list.content);
		}

		this.LIST_MARGIN = 0;
		this.helpURL = CODAGenericShell.generateHelpURL(this);

		var forms = undefined;
		if((forms = KCache.restore('/__tweaks')) != undefined) {
			if (!!forms[this.className]) {
				this.fieldConfig = forms[this.className];
			}
		}
	}
}

KSystem.include([
	'KPanel',
	'CODAForm',
	'CODAListItem',
	'KFile',
	'KPagedList',
	'KDropPanel',
	'KDraggable'
], function() {
	CODAList.prototype = new KPanel();
	CODAList.prototype.constructor = CODAList;

	CODAList.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAList.prototype.visible = function() {
		if (this.parent instanceof CODAForm) {
			KPanel.prototype.visible.call(this);
			if (this.parent.selected == this.hash) {
				this.makeActions(true);
			}
		}
		else {
			this.setStyle({
				height : (this.getParent('KFloatable').height - 29) + 'px'
			}, KWidget.CONTENT_DIV);
			this.setStyle({
				marginLeft : '20px',
				marginRight : '20px'
			});
			KPanel.prototype.visible.call(this);
			this.getShell().onFormOpen(this.getParent('KFloatable'));
		}
	};

	CODAList.prototype.invisible = function() {
		this.actionsAdded = false;
		if (this.parent instanceof CODAForm) {
			for (var a in this.actions) {
				this.getShell().removeToolbarButton(this.actions[a]);
			}
		}
		KPanel.prototype.invisible.call(this);
	};

	CODAList.prototype.onClose = function() {
		this.actionsAdded = false;
		for (var a in this.actions) {
			this.getShell().removeToolbarButton(this.actions[a]);
		}
	};

	CODAList.prototype.onUnauthorized = function() {
	};

	CODAList.prototype.getRequest = function() {
		return null;
	};

	CODAList.prototype.setData = function(data) {
		this.data = this.list.data = data;
	};

	CODAList.prototype.draw = function() {
		if (!!this.fieldConfig) {
			CODAGenericShell.applyTableConfiguration(this, this.fieldConfig);
		}
		var height = KDOM.getBrowserHeight() - 290;
		this.pageSize = (this.pageSize != 0 ? Math.floor(height /  35) : this.pageSize);
		this.staticData = this.staticData || (!!this.elements && (!this.data.links || !this.data.links.feed || !this.data.links.feed.href));

		{
			this.list.pageSize = this.pageSize;
			this.list.data = this.data;
			this.list.table = this.table;

			var div = window.document.createElement('div');
			div.className = 'KListPage KPagedListPage';
			this.tableHeader.appendChild(div);

			this.tableHeaderTotal = window.document.createElement('div');
			if (this.pageSize != 0) {
				this.tableHeaderTotal.className = 'KListPageTotal KPagedListPageTotal';
				this.tableHeader.appendChild(this.tableHeaderTotal);
			}

			var listItem = new CODAListItem(this, true);
			listItem.hash = '/title';
			listItem.data = this.table;
			listItem.addCSSClass('CODAListItemHeader');
			listItem.go();
			div.appendChild(listItem.panel.cloneNode(true));
			this.lastOrder = div.childNodes[0].childNodes[1].childNodes[this.lastOrderIndex];
			this.removeChild(listItem);
			delete listItem;

			this.list.appendChild = function(listItem, childDiv) {
				if (this.feed_retrieved) {
					var _href = '' + (listItem.data.href || listItem.data._id || listItem.data.id || listItem.index || this.nChildren);
					if (_href.charAt(0) != '/') {
						_href = '/' + _href;
					}
					listItem.hash = '/item' + _href;
				}
				listItem.orderBy = this.parent.orderBy;
				var ret = KPagedList.prototype.appendChild.call(this, listItem, childDiv);
				if (listItem instanceof CODAListItem && this.parent.options.make_draggable) {
					KDraggable.make(listItem);
				}
				if (listItem instanceof CODAListItem) {
					this.nElements++;
				}
				return ret;
			};
			this.list.refreshPages = function(all) {
				KCache.find(new RegExp(this.ref + '/children(.*)'), KCache.clear);
				KCache.find(new RegExp(this.ref + '/feed(.*)'), KCache.clear);
				KCache.find(new RegExp(this.ref + '/data(.*)'), KCache.clear);
				KCache.find(new RegExp(this.ref + '/config(.*)'), KCache.clear);
				for ( var w in this.children) {
					if (this.children[w] instanceof CODAListItem && !this.children[w].isTitle) {
						this.removeChild(this.children[w]);
					}
				}
				delete this.data.feed;
				this.content.innerHTML = '';
				this.lastpage = -1;
				if (all) {
					this.page = 0;
					this.header.innerHTML = '';
				}
				delete this.pages;
				this.pages = {};
				if (!this.parent.staticData) {
					this.onLoad();
				}
				else {
					if (!!this.parent.elements && this.parent.elements.length != 0) {
						this.header.style.display = 'block';
						this.content.style.display = 'block';
						this.parent.noItems.style.display = 'none';
						this.parent.addItems(this);
						this.parent.repositionItems();
						this.pageVisible(0);
					}
					else {
						this.parent.repositionItems();
						this.header.style.display = 'none';
						this.content.style.display = 'none';
						this.parent.noItems.style.display = 'block';
						if (!!this.parent.dropit) {
							this.parent.dropit.panel.style.display = 'none';
						}
					}
				}
			};
		}

		if (!this.staticData) {
			this.list.go = function() {
				this.gone = true;
				this.page = 0;
				this.onLoad();
			};
			this.list.prepareArgs = function() {
				var args = null;
				if (this.pageSize != 0) {
					args = {
						fields : 'elements,size',
						pageSize : this.pageSize,
						pageStartIndex : this.page * this.pageSize
					};
				}
				else {
					if (!!this.parent.fieldslist) {
						args = {
							fields : this.parent.fieldslist
						};
					}
				}
				if (!!this.parent.orderBy) {
					args.orderBy = this.parent.orderBy;
				}
				if (this.parent.filterFields !== null && this.parent.filterFields.length !== 0 && !!this.parent.filterValue.length !== 0) {
					for (var f in this.parent.filterFields) {
						if (!this.parent.filterValue[f]) {
							continue;
						}
						args[this.parent.filterFields[f]] = 'm/' + this.parent.filterValue[f] + '/i'
					}
				}
				return args;
			};
			this.list.onLoad = function() {
				if (!this.data.links) {
					this.activate();
					return;
				}
				var headers = { 'E-Tag' : '' + (new Date()).getTime() };
				headers = KSystem.merge(headers, this.parent.headers);
				var args = this.prepareArgs();
				this.kinky.get(this, (this.data.links.feed.href.indexOf(':') == -1 ? 'rest:' : '') + KSystem.urlify(this.data.links.feed.href, args), { }, headers, 'onFeed');
			};
			this.list.onFeed = function(data, request) {
				this.totalSize = data.size;
				this.feed_retrieved = true;
				this.addPage(this.page);
				if (!this.data.feed) {
					this.data.feed = data;
				}

				var n_pages = Math.ceil(this.totalSize / this.pageSize);
				this.parent.tableHeaderTotal.innerHTML = '<span>' + gettext('$CODA_LIST_HEADER_TOTAL_LABEL') + ':</span> ' + this.totalSize;

				this.data.feed.size = this.totalSize;
				KCache.commit(this.ref + '/feed/' + this.page, this.data.feed);
				if (!!data.elements) {
					this.parent.elements = data.elements;
				}
				else {
					this.parent.elements = data;
					delete this.parent.elements.http;
					delete this.parent.elements.size;
				}
				this.parent.addItems(this);

				var floatable = this.parent.parent;
				if (floatable instanceof KFloatable && floatable.data.params.transition_wait_target) {
					floatable.data.params.transition_wait_target = false;
					floatable.transition_show();
				}

				this.draw();
				this.parent.repositionItems();
			};
			this.list.onNoElements = this.onNoElements;
			this.list.onNoContent = function(data, request) {
				if (!this.parent.filterValue || this.parent.filterValue.length == 0) {
					this.onNoElements(data, request);
				}
				else {
					CODAList.prototype.onNoElements.call(this, data, request);
				}
			};
			this.list.draw = function() {
				for ( var child in this.childWidgets()) {
					var w = this.childWidget(child);
					if (!w.gone) {
						if (w.silent) {
							this.loadedChildren++;
							this.onChildLoaded(w);
						}
						w.go();
					}
				}

				this.activate();
				this.header.style.display = 'block';
				this.content.style.display = 'block';
				this.parent.noItems.style.display = 'none';
				this.panel.style.opacity = '1';
				this.pageVisible(this.page);
				if (!!this.parent.dropit) {
					this.parent.dropit.panel.style.display = 'block';
				}
			}
		}

		this.makeHelp();
		this.appendChild(this.list);

		this.makeBulkActions();
		this.makeActions();
		this.list.nChildren = 0;
		if (!!this.fieldConfig) {
			CODAGenericShell.applyButtonConfiguration(this.fieldConfig, this.className);
		}
		KPanel.prototype.draw.call(this);

		this.noItems = window.document.createElement('div');
		this.noItems.className = ' CODAListNoItems ';
		this.noItems.appendChild(window.document.createTextNode(gettext('$CODA_LIST_NO_ITEMS')));
		this.noItems.style.display = 'none';
		this.content.appendChild(this.noItems);

		if (this.staticData) {
			if (!!this.elements && this.elements.length != 0) {
				this.addItems(this.list);
				this.repositionItems();
				this.list.pageVisible(0);
			}
			else {
				this.repositionItems();
				this.list.header.style.display = 'none';
				this.list.content.style.display = 'none';
				this.noItems.style.display = 'block';
				if (!!this.dropit) {
					this.dropit.panel.style.display = 'none';
				}
			}
		}
	};

	CODAList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAListItem(list);
			listItem.data = this.elements[index];
			list.appendChild(listItem);
		}
	};

	CODAList.prototype.repositionItems = function() {
		if (this.parent.panel.className.indexOf('CODAMiddleForm') != -1) {
			if (this.parent.single) {
				this.LIST_MARGIN = CODAGenericShell.MIDDLE_DIALOG_MARGIN + 80;
			}
			else {
				this.LIST_MARGIN = CODAGenericShell.MIDDLE_DIALOG_MARGIN + 265;
			}
			var usablew = KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.MIDDLE_DIALOG_MARGIN;
			if (CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH < usablew) {
				this.LIST_MARGIN += usablew - CODAGenericShell.MIDDLE_DIALOG_MAXWIDTH ;
			}
		}
		else if (this.parent.panel.className.indexOf('CODAToolsDialog') != -1) {
			if (this.parent.single) {
				this.LIST_MARGIN = CODAGenericShell.BROWSER_DIALOG_MARGIN + 80;
			}
			else {
				this.LIST_MARGIN = CODAGenericShell.BROWSER_DIALOG_MARGIN + 265;
			}
			var usablew = KDOM.getBrowserWidth() - CODAGenericShell.LEFT_FRAME_MARGIN - CODAGenericShell.BROWSER_DIALOG_MARGIN;
			if (CODAGenericShell.BROWSER_DIALOG_MAXWIDTH < usablew) {
				this.LIST_MARGIN += usablew - CODAGenericShell.BROWSER_DIALOG_MAXWIDTH ;
			}
		}
		else if (!this.getParent('CODAFormIconDashboard') && this.parent.panel.className.indexOf('CODASideForm') != -1) {
			this.LIST_MARGIN = KDOM.getBrowserWidth() - 970;			
		}
		else if (this.parent.panel.className.indexOf('KFloatable') != -1) {
			this.LIST_MARGIN = 60;
		}

		var n = 0;
		var width = (KDOM.getBrowserWidth() - this.getShell().LEFT_FRAME_MARGIN - this.LIST_MARGIN - this.options.right_margin);
		var dividing = width;
		var fixed = [];
		var k = 0;
		for (var f in this.table) {
			if (!!this.table[f].fixed) {
				dividing -= this.table[f].fixed;
			}
			else {
				n++;
			}
			fixed[k] = this.table[f];
			k++;
		}

		this.noItems.style.width = width + 'px';
		this.list.header.style.width = width + 'px';

		var __margin = 90;
		var nbt = 0;
		for ( var c in this.list.childWidgets()) {
			var li = this.list.childWidget(c);
			if (li instanceof CODAListItem) {
				nbt = li.nElements;
				li.setStyle({
					width :  width + 'px'
				});

				var k = 0;
				for (var i = 0; i != li.titleContainer.childNodes.length; i++) {
					if (li.titleContainer.childNodes[i].tagName.toLowerCase() == 'h2') {
						if (!!fixed[k].fixed) {
							li.titleContainer.childNodes[i].style.width = (fixed[k].fixed) + 'px';
						}
						else {
							li.titleContainer.childNodes[i].style.width = Math.floor((dividing - (26 * nbt + __margin)) / n) + 'px';
						}
						li.titleContainer.childNodes[i].style.textAlign = fixed[k].align || 'left';
						k++;
					}
				}
			}
		}

		var k = 0;
		this.tableHeader.childNodes[0].childNodes[0].style.width = width + 'px';
		for (var i = 0; i != this.tableHeader.childNodes[0].childNodes[0].childNodes[1].childNodes.length; i++) {
			if (this.tableHeader.childNodes[0].childNodes[0].childNodes[1].childNodes[i].tagName.toLowerCase() == 'h2') {
				if (!!fixed[k].fixed) {
					this.tableHeader.childNodes[0].childNodes[0].childNodes[1].childNodes[i].style.width = (fixed[k].fixed) + 'px';
				}
				else {
					this.tableHeader.childNodes[0].childNodes[0].childNodes[1].childNodes[i].style.width = Math.floor((dividing - (26 * nbt + __margin)) / n) + 'px';
				}
				this.tableHeader.childNodes[0].childNodes[0].childNodes[1].childNodes[i].style.textAlign = fixed[k].align || 'left';
				k++;
			}
		}
	};

	CODAList.prototype.onClickItem = function(item) {
	};

	CODAList.prototype.exportList = function() {
	};

	CODAList.prototype.addItem = function() {
		if (!!this.data.links && !!this.data.links.feed && !!this.data.links.feed.href) {
			KBreadcrumb.dispatchURL({
				hash : '/add' + this.data.links.feed.href
			});
		}
	};

	CODAList.prototype.addActions = function() {
	};

	CODAList.prototype.addBulkActions = function() {
	};

	CODAList.prototype.getSelection = function() {
		var childs = this.list.childWidgets();
		var ret = [];
		for ( var index in childs) {
			if (childs[index] instanceof CODAListItem && !childs[index].isTitle && childs[index].selected) {
				ret.push(childs[index].data);
			}
		}
		return ret;
	};

	CODAList.prototype.makeBulkActions = function() {
		this.addBulkActions();
	};

	CODAList.prototype.refresh = function() {
		this.list.refreshPages();
	};

	CODAList.prototype.makeActions = function(force) {
		if (!!this.actionsAdded) {
			return;
		}
		if ((this.parent instanceof CODAForm) && !force) {
			return;
		}

		var shell = this.getShell();
		var self = this;

		if (!!this.options.export_button) {
			var exportBt = new KButton(shell, '<i class="fa fa-share-square-o"></i> ' + gettext('$CODA_LIST_EXPORT_BUTTON_' + this.className.toUpperCase()), 'export_button', function(event) {
				KDOM.stopEvent(event);
				self.exportList();
			});
			this.addAction(exportBt);
		}
		
		if (!!this.options.add_button) {
			var addBt = new KButton(shell, gettext('$CODA_LIST_ADD_BUTTON_' + this.className.toUpperCase()), 'add_button', function(event) {
				KDOM.stopEvent(event);
				self.addItem();
			});
			this.addAction(addBt);
		}

		if (!this.filtersAdded) {
			if (!!this.filterFields && !!this.options.filter_input) {
				this.addFilter( function(parent) {
					var filterInput = new KInput(parent, '', 'pages-main-search');
					{
						filterInput.input.placeholder = gettext('$CODA_LIST_FILTER');
						filterInput.addEventListener('keyup', function(event) {
							event = KDOM.getEvent(event);

							if (event.keyCode == 13) {
								self.list.parent.filterResults(self.list, filterInput.getValue(), filterInput.index);
								parent.onSelection(filterInput.getValue());
							}
							else if (event.keyCode == 27) {
								KDOM.stopEvent(event);
								parent.onClose();
							}
						});
					}
					return filterInput;
				});
			}

			this.addActions();
			this.filtersAdded = true;
		}
		this.actionsAdded = true;
	};

	CODAList.prototype.orderResults = function(orderBy) {
		this.orderBy = orderBy
		this.list.panel.style.opacity = '0';
		var self = this;
		KSystem.addTimer(function() {
			self.list.refreshPages(true);
		}, 200);
	};

	CODAList.prototype.execFilter = function(value, label, index) {
		var filterpanel = new KPanel(this);
		{
			filterpanel.addCSSClass('CODAToolbarFilters ' + filterpanel.className + 'ToolbarFilters');
			filterpanel.content.innerHTML = '<span>' + (!!label ? label : value) + '</span>';
			var removebt = window.document.createElement('button');
			KDOM.addEventListener(removebt, 'click', function(event) {
				filterpanel.parent.clearFilter(value);
				filterpanel.onClose();
			});
			filterpanel.index = index;
			filterpanel.content.appendChild(removebt);
			filterpanel.onClose = function () {
				KEffects.addEffect(this,  {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 300,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					},
					onComplete : function(w, t) {
						filterpanel.parent.removeChild(filterpanel);
					}
				});
			};
		}
		filterpanel.visible = function() {
			if (!!this.display) {
				return;
			}
			this.panel.style.opacity = '0';
			KPanel.prototype.visible.call(this);
			KEffects.addEffect(this,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				}
			});
		};

		this.appendChild(filterpanel, this.title);
		filterpanel.go();
		this.filterResults(this.list, value, index);
	};

	CODAList.prototype.clearFilter = function(value) {
		if (!!value) {
			for (var v in this.filterValue) {
				if (!this.filterValue[v]) {
					continue;
				}
				this.filterValue[v] = this.filterValue[v].replace('|' + value, '').replace(value + '|', '').replace(value, '').trim();
				if (this.filterValue[v].length == 0 || this.filterValue[v] == '|') {
					this.filterValue[v] = undefined;
				}
			}
		}
		else {
			this.filterValue = [];
		}
		this.list.panel.style.opacity = '0';
		var self = this;
		KSystem.addTimer(function() {
			self.list.refreshPages(true);
		}, 200);
	};

	CODAList.prototype.filterResults = function(list, value, index) {
		if (this.filterFields !== null && this.filterFields.length > index) {
			if (!!this.filterValue[index] && this.filterValue[index] !== '') {
				this.filterValue[index] += '|' + value;
			}
			else {
				this.filterValue[index] = value;
			}
			this.list.panel.style.opacity = '0';
			var self = this;
			KSystem.addTimer(function() {
				self.list.refreshPages(true);
			}, 200);
		}
	};

	CODAList.prototype.addFilter = function(input_creator) {
		if (this.filteractivator == undefined) {
			if (!this.title) {
				this.setTitle('');
			}
			this.filteractivator = new KButton(this, '', 'add-to-list', function(event) {
				KDOM.stopEvent(event);
				event = KDOM.getEvent(event);
				var widget = KDOM.getEventWidget(event);
				if (!!widget.filterpanel) {
					widget.filterpanel.onClose();
					return;
				}

				widget.filterpanel = new KPanel(widget.parent);
				{
					widget.filterpanel.addCSSClass('CODAToolbarFilters ' + widget.filterpanel.className + 'ToolbarFilters');
					widget.filterpanel.onSelection = function (value, label) {
						widget.filterpanel.removeAllChildren();
						widget.filterpanel.content.innerHTML = '<span>' + (!!label ? label : value) + '</span>';
						var removebt = window.document.createElement('button');
						KDOM.addEventListener(removebt, 'click', function(event) {
							widget.parent.clearFilter(value);
							KDOM.getEventWidget(event).onClose(true);
						});
						widget.filterpanel.content.appendChild(removebt);
						widget.filterpanel = undefined;
					};					
					widget.filterpanel.onClose = function (notFilterPanel) {
						KEffects.addEffect(this,  {
							f : KEffects.easeOutExpo,
							type : 'fade',
							duration : 300,
							go : {
								alpha : 0
							},
							from : {
								alpha : 1
							},
							onComplete : function(w, t) {
								widget.parent.removeChild(w);
								if (!notFilterPanel) {
									delete widget.filterpanel;
									widget.filterpanel = undefined;
								}
							}
						});
					};
				}
				widget.filterpanel.visible = function() {
					if (!!this.display) {
						return;
					}
					this.panel.style.opacity = '0';
					KPanel.prototype.visible.call(this);
					KEffects.addEffect(this,  {
						f : KEffects.easeOutExpo,
						type : 'fade',
						duration : 500,
						go : {
							alpha : 1
						},
						from : {
							alpha : 0
						}
					});
				};

				for (var f in widget.parent.filters) {
					var callback = widget.parent.filters[f];
					var element = callback(widget.filterpanel);
					element.index = f;
					widget.filterpanel.appendChild(element);
				}

				widget.parent.appendChild(widget.filterpanel, widget.parent.title);
				widget.filterpanel.go();

				var first = widget.filterpanel.childAt(0);
				if (first instanceof KAbstractInput) {
					KSystem.addTimer(function() {
						first.focus();
					}, 200);
				}
			});
			this.filteractivator.addCSSClass('CODAFilterButton');
			this.filteractivator.setText('<i class="fa fa-filter"></i>', true);
			var params = {
				text : gettext('$CODA_LIST_FILTER_TIP'),
				delay : 300,
				gravity : {
					x : 'right',
					y : 'top'
				},
				offsetX : 35,
				offsetY : 0,
				cssClass : 'CODAInputBaloon CODAIconBaloon'
			};
			KBaloon.make(this.filteractivator, params);
			this.appendChild(this.filteractivator, this.title);
		}
		if (this.filters == undefined) {
			this.filters = [];
		}
		this.filters.push(input_creator);
	};

	CODAList.prototype.addAction = function(button) {
		button.hash = '/' + this.className + '/' + button.inputID;
		button.data = {
			tween : {
				show : [{
					f : KEffects.easeOutExpo,
					type : 'move',
					duration : 750,
					lock : {
						x : true
					},
					go : {
						y : 0
					},
					from : {
						y : -40
					}
				}, {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				}],
				hide : [{
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}]
			}
		}
		button.panel.style.opacity = '0';
		this.actions.push(button);
		this.getShell().addToolbarButton(button, 0, 'right', true);
	};

	CODAList.prototype.onNoElements = function(data, request) {
		var floatable = this.parent.parent;
		if (floatable instanceof KFloatable && floatable.data.params.transition_wait_target) {
			floatable.data.params.transition_wait_target = false;
			floatable.transition_show();
		}
		
		this.draw();
		this.header.style.display = 'none';
		this.content.style.display = 'none';
		this.parent.noItems.style.display = 'block';
		if (!!this.parent.dropit) {
			this.parent.dropit.panel.style.display = 'none';
		}
		this.parent.repositionItems();
		this.parent.tableHeaderTotal.innerHTML = '';
	};

	CODAList.reorder = function(event, field) {
		var h2 = KDOM.getEventTarget(event);
		var item = KDOM.getEventWidget(event);
		var widget = item.getParent('CODAList');
		var direction = (!!widget.orderBy && widget.orderBy.indexOf(field) != -1 && widget.orderBy[0] == '+' ? '-' : '+');

		if(!!widget.lastOrder) {
			KCSS.removeCSSClass('CODAListOrderASC', widget.lastOrder);
			KCSS.removeCSSClass('CODAListOrderDESC', widget.lastOrder);
		}
		KCSS.removeCSSClass('CODAListOrderASC', h2);
		KCSS.removeCSSClass('CODAListOrderDESC', h2);
		KCSS.addCSSClass('CODAListOrder' + (direction == '-' ? 'DESC' : 'ASC'), h2);
		widget.lastOrder = h2;

		widget.orderResults(direction + field);
	};

	CODAList.clipboardNode = null;
	CODAList.clipboardWidget = null;
	CODAList.OPTIONS = {
		one_choice : true,
		drop_panel : false,
		make_draggable : true,
		left_margin : 0,
		right_margin : 0,
		add_button : false,
		delete_button : true,
		edit_button : true,
		filter_input : true
	}

	KSystem.included('CODAList');
}, Kinky.CODA_INCLUDER_URL);

function CODALocaleForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);

		this.locales = window.document.createElement('ul');
		this.locales.className = ' CODALocaleFormFlags ';
		this.panel.insertBefore(this.locales, this.content);

		this.acceptedLocales = [ KLocale.LOCALE ];
		this.currentLocale = KLocale.LOCALE;
	}
}

KSystem.include([
	'CODAForm'
], function() {
	CODALocaleForm.prototype = new CODAForm();
	CODALocaleForm.prototype.constructor = CODALocaleForm;

	CODALocaleForm.prototype.addLocale = function(locale) {
		if (locale == KLocale.LOCALE) {
			return;
		}
		this.acceptedLocales.push(locale);
	};

	CODALocaleForm.prototype.visible = function() {
		var display = this.display;
		CODAForm.prototype.visible.call(this);
		if (!display) {			
			for ( var c in this.childWidgets()) {
				if (!(this.childWidget(c) instanceof KButton) && !(this.childWidget(c) instanceof CODAList)) {
					var p = this.childWidget(c);
					for ( var pc in p.childWidgets()) {
						if (p.childWidget(pc) instanceof KAbstractInput) {
							var field = p.childWidget(pc);
							if (!!field.isLocalized) {
								field.addCSSClass('CODALocaleFormInputFlagParent')
								field.flag = window.document.createElement('img');
								field.flag.className = ' CODALocaleFormInputFlag ';
								field.flag.src = CODA_VERSION + '/images/flags/' + this.currentLocale.split('_')[1].toLowerCase() + '.png';
								field.inputContainer.appendChild(field.flag);
							}
						}
					}
				}
			}
		}
	};

	CODALocaleForm.prototype.listform_visible = function() {
		var display = this.display;
		CODAForm.prototype.listform_visible.call(this);
		if (!display) {			
			for ( var c in this.childWidgets()) {
				if (!(this.childWidget(c) instanceof KButton) && !(this.childWidget(c) instanceof CODAList)) {
					var p = this.childWidget(c);
					for ( var pc in p.childWidgets()) {
						if (p.childWidget(pc) instanceof KAbstractInput) {
							var field = p.childWidget(pc);
							if (!!field.isLocalized) {
								field.addCSSClass('CODALocaleFormInputFlagParent')
								field.flag = window.document.createElement('img');
								field.flag.className = ' CODALocaleFormInputFlag ';
								field.flag.src = CODA_VERSION + '/images/flags/' + this.currentLocale.split('_')[1].toLowerCase() + '.png';
								field.inputContainer.appendChild(field.flag);
							}
						}
					}
				}
			}
		}
	};

	CODALocaleForm.prototype.showLocale = function(locale) {
		for ( var c in this.childWidgets()) {
			if (!(this.childWidget(c) instanceof KButton)) {
				var p = this.childWidget(c);
				if (p instanceof CODAList) {
					if (p.fieldID && p.isLocalized) {
						try {
							CODAForm.createObject(this.data, field.fieldID.split('.'));
							eval('this.data.' + p.fieldID + ' = p.getRequest()');
						}
						catch (e) {}
						p.fieldID = p.fieldID.replace(this.currentLocale, locale);
						try {
							eval('value = this.data.' + p.fieldID);
						}
						catch (e) {
							value = [];
						}
						p.elements = value;
						p.refresh();
					}
				}
				else {
					for ( var pc in p.childWidgets()) {
						if (p.childWidget(pc) instanceof KAbstractInput || p.childWidget(pc) instanceof KHidden) {
							var field = p.childWidget(pc);
							if (!!field.isLocalized) {
								var value = null;
								var current = null;

								if (field.getValue() !== undefined && field.getValue() !== '') {
									try {
										CODAForm.createObject(this.data, field.inputID.split('.'));
										eval('this.data.' + field.inputID + ' = field.getValue()');
									}
									catch (e) {}
								}

								try {
									eval('current = this.data.' + field.inputID);
								}
								catch (e) {
									current = undefined;
								}
								field.inputID = field.inputID.replace(this.currentLocale, locale);
								try {
									eval('value = this.data.' + field.inputID);
								}
								catch (e) {
									value = undefined;
								}

								if (field instanceof KAbstractInput) {
									field.setValue(value || '');
									field.flag.src = CODA_VERSION + '/images/flags/' + locale.split('_')[1].toLowerCase() + '.png';

									field.addCSSClass('CODAFormLocaleSignalTranslation');
									KSystem.addTimer('CODALocaleForm.removeSignalClass(\'' + field.id + '\')', 1000);
								}
								else if (field instanceof KHidden) {
									field.setValue(value || current);
								}

							}
						}
					}
				}
			}
		}
		this.currentLocale = locale;
	};

	CODALocaleForm.prototype.drawFlags = function() {
		for (var f in this.acceptedLocales) {
			var li = window.document.createElement('li');
			{
				var flag = window.document.createElement('img');
				flag.src = CODA_VERSION + '/images/flags/' + this.acceptedLocales[f].split('_')[1].toLowerCase() + '.png';
				flag.alt = this.acceptedLocales[f];
				flag.title = this.acceptedLocales[f].split('_')[1].toUpperCase();
				if (this.acceptedLocales[f] == KLocale.LOCALE) {
					flag.className = ' CODALocaleFormFlagsSelected ';
				}
				KDOM.addEventListener(flag, 'click', CODALocaleForm.changeLocale);
				li.appendChild(flag);
			}
			this.locales.appendChild(li);
		}
	};

	CODALocaleForm.prototype.minimized_draw = function() {
		this.drawFlags();
		CODAForm.prototype.minimized_draw.call(this);
	};

	CODALocaleForm.prototype.draw = function() {
		this.drawFlags();
		CODAForm.prototype.draw.call(this);
	};

	CODALocaleForm.removeSignalClass = function(id) {
		Kinky.getWidget(id).removeCSSClass('CODAFormLocaleSignalTranslation');
	};

	CODALocaleForm.changeLocale = function(event) {
		var img = KDOM.getEventTarget(event);
		var locale = KDOM.getEventTarget(event).alt;
		for (var n = 0; n != img.parentNode.parentNode.childNodes.length; n++) {
			KCSS.removeCSSClass('CODALocaleFormFlagsSelected', img.parentNode.parentNode.childNodes[n].childNodes[0]);
		}
		KCSS.addCSSClass('CODALocaleFormFlagsSelected', img);
		KDOM.getEventWidget(event).showLocale(img.alt);
	};

	KSystem.included('CODALocaleForm');
}, Kinky.CODA_INCLUDER_URL);
function CODALoginShell(parent) {
	if (!!parent) {
		KShell.call(this, parent);
	}
}

KSystem.include([
	'KShell',
	'KPanel',
	'KForm',
	'KInput',
	'KPassword',
	'KText',
	'KLink',
	'KButton',
	'KImage',
	'KList',
	'KConnectionREST'
], function() {
	
	settext('$CODA_LOGIN_USERNAME_LABEL', 'E-mail', 'pt');
	settext('$CODA_LOGIN_PASSWORD_LABEL', 'Palavra-passe', 'pt');
	settext('$CONFIRM', 'OK', 'pt');
	settext('$CODA_LOGIN_USERNAME_LABEL', 'E-mail', 'br');
	settext('$CODA_LOGIN_PASSWORD_LABEL', 'Palavra-passe', 'br');
	settext('$CONFIRM', 'OK', 'br');
	settext('$CODA_LOGIN_USERNAME_LABEL', 'E-mail', 'en');
	settext('$CODA_LOGIN_PASSWORD_LABEL', 'Password', 'en');
	settext('$CONFIRM', 'OK', 'en');


	CODALoginShell.prototype = new KShell();
	CODALoginShell.prototype.construtor = CODALoginShell;
	
	CODALoginShell.prototype.load = function() {
		this.draw();
	};
	
	CODALoginShell.prototype.draw = function() {
		
		var mainPanel = new KPanel(this);
		mainPanel.addCSSClass('MainPanel');
		this.appendChild(mainPanel);
		
		{
			
			var myForm = new KForm(mainPanel);
			myForm.addCSSClass('AuthmyForm');
			myForm.hash = '/auth-myForm';
			myForm.staticmyForm = true;
			myForm.method = 'post';
			myForm.action = '';
			mainPanel.appendChild(myForm);
			
			{
				
				this.username = new KInput(myForm, gettext('$CODA_LOGIN_USERNAME_LABEL'), 'username');
				this.username.addCSSClass('Username');
				this.username.addValidator(/.+/, '');
				myForm.addInput(this.username);
				this.username.addEventListener('keypress', function(event) {
					CODALoginShell.keyToSubmit(event);
				});
				
				this.password = new KPassword(myForm, gettext('$CODA_LOGIN_PASSWORD_LABEL'), 'password');
				this.password.addCSSClass('Password');
				this.password.addValidator(/.+/, '');
				myForm.addInput(this.password);
				this.password.addEventListener('keypress', function(event) {
					CODALoginShell.keyToSubmit(event);
				});
				
				this.messageContainer = new KText(myForm);
				this.messageContainer.addCSSClass('LoginMessage');
				myForm.appendChild(this.messageContainer);
				
				this.messageContainer.setStyle({
					display : 'none'
				});
				
				var submitButton = new KButton(myForm, 'login', 'login', KForm.goSubmit);
				submitButton.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
				myForm.appendChild(submitButton);
				
				var passwordRecovery = new KLink(myForm);
				passwordRecovery.addCSSClass('PasswordRecovery');
				passwordRecovery.setLinkURL('');
				passwordRecovery.setLinkText('problemas com o login?');
				myForm.appendChild(passwordRecovery);
				
				myForm.onValidate = function(text, params, status) {
					if (status) {
						myForm.form.action = this.getShell().data.providers.rest.services + '/oauth/' + params.username + '/login?redirect_uri=' + encodeURIComponent('/1.0/login/loggedin.html');
						this.getInput('password').input.value = this.getInput('password').getValue();
						myForm.form.submit();

						return false;
					}
					else {
						this.showAllErrors(params);
					}
				};
				
				myForm.onSuccess = function(response_data) {
				};
				
			}

			var rest_url = window.document.location.protocol + this.getShellConfig().providers.rest.services;
			var doc_root = window.document.location.protocol + '//' + window.document.location.host + '/';
			/*var fb = new KButton(mainPanel, 'facebook', 'facebook', function() {
				var left = Math.round((KDOM.getBrowserWidth() - 1020) / 2);
				var top = Math.round((KDOM.getBrowserHeight() - 575) / 2);
				var pup = window.open(rest_url + '/accounts/facebook/login?display=popup&redirect_uri=' + encodeURIComponent(doc_root + '1.0/login/loggedin.html'), "", "width=820,height=450,status=1,location=1,resizable=no,left=" + left + ",top=" + top);
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			});
			fb.setText('<i class="fa fa-facebook-square"></i> FACEBOOK', true);
			fb.addCSSClass('ConnectBT FBBT');
			mainPanel.appendChild(fb);

			var gp = new KButton(mainPanel, 'google', 'google', function() {
				var left = Math.round((KDOM.getBrowserWidth() - 1020) / 2);
				var top = Math.round((KDOM.getBrowserHeight() - 575) / 2);
				var pup = window.open(rest_url + '/accounts/google/login?display=popup&redirect_uri=' + encodeURIComponent(doc_root + '1.0/login/loggedin.html'), "", "width=820,height=450,status=1,location=1,resizable=no,left=" + left + ",top=" + top);
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			});
			gp.setText('<i class="fa fa-google-plus-square"></i> GOOGLE+', true);
			gp.addCSSClass('ConnectBT GPBT');
			mainPanel.appendChild(gp);

			var gp = new KButton(mainPanel, 'linkedin', 'linkedin', function() {
				var left = Math.round((KDOM.getBrowserWidth() - 1020) / 2);
				var top = Math.round((KDOM.getBrowserHeight() - 575) / 2);
				var pup = window.open(rest_url + '/accounts/linkedin/login?display=popup&redirect_uri=' + encodeURIComponent(doc_root + '1.0/login/loggedin.html'), "", "width=820,height=450,status=1,location=1,resizable=no,left=" + left + ",top=" + top);
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			});
			gp.setText('<i class="fa fa-linkedin-square"></i> LINKEDIN', true);
			gp.addCSSClass('ConnectBT GPBT');*/
		}
		
		KShell.prototype.draw.call(this);
	};
	
	CODALoginShell.keyToSubmit = function(event) {
		event = KDOM.getEvent(event);
		var keyCode = event.keyCode || event.which;
		if (keyCode == 13) {
			var form = KDOM.getWidget(event, 'KForm');
			var params = form.validate();

			if ((params = form.onValidate(form.action, params, form.validation)) != false) {
				var method = form.method.toLowerCase();
				form.kinky[(method == 'delete' ? 'remove' : method)](form, form.action, params, null, 'onSuccess');
			}
		}
	};
	
	CODALoginShell.prototype.onError = function(data) {
		debug("onError");
		
	};
	
	CODALoginShell.prototype.onBadRequest = function(data) {
		debug("onBadRequest");
	};
	
	CODALoginShell.prototype.onUnauthorized = function(data) {
		debug("onUnauthorized");
		
	};
	
	CODALoginShell.prototype.onMethodNotAllowed = function(data) {
		debug("onMethodNotAllowed");
	};
	
	KSystem.included('CODALoginShell');
});
function CODAMediaList(parent, filter) {
	if (parent != null) {
		KAddOn.call(this, parent);
		this.filter = filter;
		this.parent.addCSSClass('KMediaListButtonParent');
	}
}

KSystem.include([
	'KAddOn'
], function() {
	CODAMediaList.prototype = new KAddOn();
	CODAMediaList.prototype.constructor = CODAMediaList;
	
	CODAMediaList.prototype.setDialogProps = function(props) {
		this.dialogProps = props;
	};
	
	CODAMediaList.prototype.draw = function() {
		var kmlID = this.id;
		var filter = this.filter;
		var mediaButton = window.document.createElement('button');
		mediaButton.className = 'CODAMediaListButton KMediaListButton';
		mediaButton.innerHTML = gettext('$KMEDIALIST_OPEN');
		KDOM.addEventListener(mediaButton, 'click', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event);
			widget.getShell().showCloudBrowser(widget, filter);
		});
		
		if (this.parent.activated()) {
			this.parent.content.appendChild(mediaButton);
		}
		else {
			this.parent.container.appendChild(mediaButton);
		}
	};
	
	KSystem.registerAddOn('CODAMediaList');
	KSystem.included('CODAMediaList');
}, Kinky.ZEPP_INCLUDER_URL);
function CODAMenuDashboard(parent, options, elements) {
	if (parent != null) {
		CODADashboard.call(this, parent, options);
		this.elements = elements;//(!!elements ? elements : []);
		this.addCSSClass('CODAMenuDashboard');
		this.addCSSClass('CODAMenuDashboardContent', this.content);
		this.addCSSClass('CODAMenuDashboardContent', this.container);
		this.state = 1;
		//CODAGenericShell.LEFT_FRAME_MARGIN = parent.LEFT_FRAME_MARGIN = 250;
		this.page = 0;
		this.pageSize = 30;

		this.parent.onDialogOpen = CODAMenuDashboard.onDialogOpen;
		this.parent.onDialogClose = CODAMenuDashboard.onDialogClose;

		this.noFadeTransition = true;
		this.refreshing = false;
	}
}

KSystem.include([
	'CODAIcon',
	'CODADashboard'
], function() {
	CODAMenuDashboard.prototype = new CODADashboard();
	CODAMenuDashboard.prototype.constructor = CODAMenuDashboard;

	CODAMenuDashboard.prototype.visible = function() {
		if (this.display) {
			return;
    		}    	
   		CODADashboard.prototype.visible.call(this);

		var gw = KDOM.getBrowserWidth();
		var fp = gw / 1760 * 100;
		var fs = Math.round(fp);

		for (var c in this.childWidgets()) {
			var w = this.childWidget(c);

			if (w instanceof CODAIcon) {
				KDOM.addEventListener(w.h3, 'click', CODAMenuDashboard.iconClicked);
				KDOM.addEventListener(w.icon, 'click', CODAMenuDashboard.iconClicked);
			}

			w.setStyle({
				fontSize :  (fs > 100 ? 100 : fs) + '%'
			});
		}
	};

	CODAMenuDashboard.prototype.resize = function() {
		CODADashboard.prototype.resize.call(this);
		var gw = KDOM.getBrowserWidth();
		var fp = gw / 1760 * 100;
		var fs = Math.round(fp);

		for (var c in this.childWidgets()) {
			var w = this.childWidget(c);
			w.setStyle({
				fontSize :  (fs > 100 ? 100 : fs) + '%'
			});
		}
	};

	CODAMenuDashboard.onDialogOpen = function() {
		this.dashboard.reduce();
	};

	CODAMenuDashboard.onDialogClose = function() {
		this.dashboard.enlarge();
	};

	CODAMenuDashboard.prototype.reduce = function() {
		if (this.state == 0) {
			return;
		}
		this.removeCSSClass('CODAMenuDashboardContent', this.content);
		this.addCSSClass('CODAMenuDashboardContentReduced', this.content);
		this.state = 0;
	};	

	CODAMenuDashboard.prototype.enlarge = function() {
		if (this.state == 1) {
			return;
		}
		if (!!CODAMenuDashboard.last_item) {
			CODAMenuDashboard.last_item.removeCSSClass('CODAMenuIconSelected');
		}
		this.content.style.width = '50%';
		this.removeCSSClass('CODAMenuDashboardContentReduced', this.content);
		this.addCSSClass('CODAMenuDashboardContent', this.content);
		this.state = 1;
	};	

	CODAMenuDashboard.prototype.prepareArgs = function() {
		var args = null;
		if (this.pageSize != 0) {
			args = {
				fields : 'elements,size',
				pageSize : this.pageSize,
				pageStartIndex : this.page * this.pageSize
			};
		}
		if (!!this.fieldslist) {
			args = {
				fields : this.fieldslist
			};
		}
		if (!!this.orderBy) {
			args.orderBy = this.orderBy;
		}
		if (this.filterFields !== null && !!this.filterValue) {
			for (var f in this.filterFields) {
				args[this.filterFields[f]] = 'm/' + this.filterValue + '/i';
			}
		}
		return args;
	};

	CODAMenuDashboard.prototype.load = function() {
		if (!!this.elements) {
			this.draw();
		}
		else if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			var args = this.prepareArgs();
			this.kinky.get(this, (this.options.links.feed.href.indexOf(':') == -1 ? 'rest:' : '') + KSystem.urlify(this.options.links.feed.href, args), { }, headers, 'onLoad');			
		}
		else {
			this.elements = [];
			this.draw();
		}
	};

	CODAMenuDashboard.prototype.onLoad = function(data, request) {
		this.elements = !!data.elements ? data.elements : [];
		this.draw();
	};

	CODAMenuDashboard.prototype.refresh = function() {
		if (!!this.options.links && !!this.options.links.feed && !!this.options.links.feed.href) {
			this.page = 0;
			delete this.elements;
			this.elements = undefined;
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 750,
				from : {
					alpha : 1
				},
				go : {
					alpha : 0
				},
				onComplete : function(w, t) {
					w.removeAllChildren();
					w.load();
				},
				applyToElement : true
			});
		}
	};

	CODAMenuDashboard.prototype.getIconFor = function(elementData) {
	};

	CODAMenuDashboard.prototype.addPanels = function() {

		for (var d in this.elements) {
			var button = this.getIconFor(this.elements[d]);
			if (button instanceof CODAIcon) {
				this.appendChild(button);
			}
			else if (button instanceof Array) {
				for (var b in button) {
					this.appendChild(button[b]);
				}				
			}
		}
		if (!!this.refreshing) {
			this.refreshing = false;
			KEffects.addEffect(this.content,  {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1000,
				from : {
					alpha : 0
				},
				go : {
					alpha : 1
				},
				applyToElement : true
			});
		}
	};

	CODAMenuDashboard.iconClicked = function(event) {
		if (!!CODAMenuDashboard.last_item) {
			try {
				CODAMenuDashboard.last_item.removeCSSClass('CODAIconSelected');
			}
			catch (e) {}
		}
		CODAMenuDashboard.last_item = KDOM.getEventWidget(event);
		CODAMenuDashboard.last_item.addCSSClass('CODAIconSelected');
	};

	CODAMenuDashboard.last_item = undefined;	

	KSystem.included('CODAMenuDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODANotifications(parent, notifications) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.headers = {
			'Authorization' : 'OAuth2.0 ' + decodeURIComponent(this.getShell().data.module_token)
		}
		this.notifications = notifications;
		this.marked = [];
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODANotifications.prototype = new KPanel();
	CODANotifications.prototype.constructor = CODANotifications;

	CODANotifications.prototype.visible = function(tween) {
		if (this.display) {
			return;
		}
		KPanel.prototype.visible.call(this);
	};

	CODANotifications.prototype.onClose = function() {
		if (!this.notifications) {
			return;
		}
		for (var i in this.marked) {
			for (var n = 0; n != this.notifications.elements.length; n++) {
				if (this.notifications.elements[n]._id == this.marked[i]) {
					delete this.notifications.links[this.notifications.elements[n].id];
					delete this.notifications.links[this.notifications.elements[n]._id];
					this.notifications.elements.splice(n, 1);
					n--;
				}
			}
		}
		if (this.notifications.elements.length == 0) {
			delete this.getShell().notifications;
			this.getShell().notifications = undefined;
		}
	};

	CODANotifications.prototype.load = function(element) {
		this.draw();
	};

	CODANotifications.prototype.draw = function() {
		this.setTitle(gettext('$CODA_NOTIFICATIONS_TITLE'));

		if (!this.notifications) {
			var title = window.document.createElement('div');
			{
				title.innerHTML = gettext('$CODA_NOTIFICATIONS_EMPTY');
			}
			this.content.appendChild(title);
			KPanel.prototype.draw.call(this);
			return;
		}

		this.list = window.document.createElement('ul');
		for (var n in this.notifications.elements) {
			var note = window.document.createElement('li');
			note.id = this.notifications.elements[n]._id;
			this.notifications.links[this.notifications.elements[n]._id] = this.notifications.elements[n];

			var button = window.document.createElement('i');
			{
				KDOM.addEventListener(button, 'click', function(e) {
					KDOM.stopEvent(e);
					var widget = KDOM.getEventWidget(e);
					var i = KDOM.getEventTarget(e);
					var li = i.parentNode;
					var ul = i.parentNode.parentNode;
					var content = ul.parentNode;
					widget.markRead(li.id);
					ul.removeChild(li);
					if (ul.childNodes.length == 0) {
						content.removeChild(ul);
						var title = window.document.createElement('div');
						{
							title.innerHTML = gettext('$CODA_NOTIFICATIONS_EMPTY');
						}
						content.appendChild(title);
						widget.removeChild(widget.go);
					}
				});
				button.className = 'fa fa-times';
			}
			note.appendChild(button);

			var title = window.document.createElement('a');
			{
				title.innerHTML = CODANotifications.resolve(this.notifications.elements[n], 'title');
				KDOM.addEventListener(title, 'click', function(e) {
					KDOM.stopEvent(e);
					var h3 = KDOM.getEventTarget(e);
					if (h3.nextSibling.style.display == 'none') {
						h3.nextSibling.style.display = 'block';
						if (!!h3.nextSibling.nextSibling) {
							h3.nextSibling.nextSibling.style.display = 'block';
						}
						KCSS.addCSSClass('Read', h3);
						KCSS.addCSSClass('Opened', h3.parentNode);
						KDOM.getEventWidget(e).markRead(h3.parentNode.id);
					}
					else {
						h3.nextSibling.style.display = 'none';						
						if (!!h3.nextSibling.nextSibling) {
							h3.nextSibling.nextSibling.style.display = 'none';
						}
						KCSS.removeCSSClass('Opened', h3.parentNode);
					}
				});
			}
			note.appendChild(title);

			var message = window.document.createElement('p');
			{
				message.style.display = 'none';
				message.innerHTML = CODANotifications.resolve(this.notifications.elements[n], 'message');
			}
			note.appendChild(message);

			if (!!this.notifications.elements[n].action) {
				var link = window.document.createElement('button');
				{
					link.style.display = 'none';
					link.innerHTML = '<i class="fa fa-chevron-right"></i>';
					link.setAttribute('data-action', this.notifications.elements[n].action);

					KDOM.addEventListener(link, 'click', function(event) {
						KDOM.stopEvent(event);
						var action = KDOM.getEventTarget(event).getAttribute('data-action');
						if (action[0] == '#') {
							KBreadcrumb.dispatchURL({
								url : action.substr(1)
							});
						}
						else {
							window.document.location = action;
						}						
					});
				}
				note.appendChild(link);
			}
			this.list.appendChild(note);
		}
		this.content.appendChild(this.list);

		this.go = new KButton(this, '', 'go', function(e) {
			KDOM.getEventWidget(e, 'CODANotifications').markAllRead();
		});
		this.go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CODA_NOTIFICATIONS_MARK_ALL_READ'), true);
		this.go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(this.go, this.panel);

		KPanel.prototype.draw.call(this);
	};

	CODANotifications.prototype.markRead = function(id) {
		for (var i in this.marked) {
			if (this.marked[i] == id) {
				return;
			}
		}
		this.marked.push(id);
		KBreadcrumb.dispatchEvent(null, {
			action : '/bubble/unmark/' + this.notifications.links[id].target_href
		});
		KBreadcrumb.dispatchEvent(null, {
			action : '/bubble/unmark/notifications-global-bubble'
		});
		this.kinky.remove(this, 'rest:' + id, {});
	};

	CODANotifications.prototype.markAllRead = function() {
		this.kinky.remove(this, 'rest:/users/me/notifications' + id, {});
	};

	CODANotifications.prototype.onBadRequest = function(data, request) {
	};

	CODANotifications.prototype.onPreConditionFailed = function(data, request) {
	};

	CODANotifications.prototype.onNotFound = function(data, request) {
	};

	CODANotifications.prototype.onPost = CODANotifications.prototype.onCreated = function(data, request){
	};

	CODANotifications.prototype.onPut = function(data, request) {
	};

	CODANotifications.prototype.onRemove = function(data, request) {
		KBreadcrumb.dispatchEvent(null, {
			action : request.service + '/removed'
		});
	};

	CODANotifications.register = function(shell, href) {
		CODANotifications.RESOLVERS[href] = shell;
	};

	CODANotifications.resolve = function(notification, field) {
		var raw = undefined;
		eval('raw = notification.' + field + '');
		
		if (raw != gettext(raw)){
			return gettext(raw);
		}

		try {
			var evaled = undefined;
			eval('evaled = ' + raw);
			return evaled;
		}
		catch (e) {
		}
		return raw;
	};

	CODANotifications.RESOLVERS = {};

	KSystem.included('CODANotifications');
}, Kinky.CODA_INCLUDER_URL);
function CODAPermissions(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = (!!data ? data : {});
		this.fieldID = 'permissions';
	}
}

KSystem.include([
	'KPanel',
	'KCheckBox'
], function() {
	CODAPermissions.prototype = new KPanel();
	CODAPermissions.prototype.constructor = CODAPermissions;
	
	CODAPermissions.prototype.load = function() {
		var scopes = KCache.restore('/scopes');
		
		if (!scopes) {
			var headers = null;
			this.kinky.get(this, 'rest:' + KSystem.urlify('/scopes', {
				fields : 'elements'
			}), {}, headers);
		}
		else {
			this.onLoad(scopes);
		}
	};
	
	CODAPermissions.prototype.hasScope = function(scope) {
		for ( var w in this.data) {
			if (scope == this.data[w]) {
				return true;
			}
		}
		return false;
	};
	
	CODAPermissions.prototype.onLoad = function(data, request) {
		if (!!request) {
			KCache.commit('/scopes', data);
		}
		
		this.check = new KCheckBox(this, gettext('$KRESOURCE_PERMISSIONS_LABEL'), 'permissions');
		for ( var p in data.elements) {
			this.check.addOption(data.elements[p].id, data.elements[p].name, !!this.data && this.hasScope(data.elements[p].id));
		}
		this.appendChild(this.check);
		this.draw();
	};
	
	CODAPermissions.prototype.getRequest = function() {
		var params = this.check.getValue();
		return params;
	};
	
	KSystem.included('CODAPermissions');
}, Kinky.CODA_INCLUDER_URL);

function CODASubsriptionWizard(parent, products) {
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODASubsriptionWizard']);
		this.setTitle(gettext('$CODA_APPS_SUBSCRIPTION_TITLE'));
		this.hash = '/apps/subscribe';
		this.products = products;
	}
}

KSystem.include([
	'CODAWizard'
], function() {
	CODASubsriptionWizard.prototype = new CODAWizard();
	CODASubsriptionWizard.prototype.constructor = CODASubsriptionWizard;

	CODASubsriptionWizard.prototype.onPrevious = function() {	
		switch(this.current) {
			case 2: {
				delete this.choosen;
				this.choosen = undefined;
				this.childWidget('/apps/subscribe/2').childWidget('/products').clear();
				this.providerCode.clear();
				break;
			}
			case 3: {
				this.providerCode.clear();
				if (this.choosen.available_subscriptions.length == 1 && this.choosen.available_subscriptions[0].total_value == 0) {
					this.stepOverPrevious();
				}
				break;
			}
			case 5 : {
				if (this.values[3].paymentProvider_code == 'paypal' || this.values[3].paymentProvider_code == 'free') {
					this.stepOverPrevious();
				}
				break;
			}
		}
	};

	CODASubsriptionWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				this.values[0].fb_page_id = this.values[0].facebook.page_id;
				if (!this.valid_subscriptions) {
					this.cancelNext = true;
					this.kinky.post(this, 'rest:/applications/subscriptions/' + this.values[0].fb_page_id, { type : "facebook" }, null, 'onGetSubscriptions');
				}
				break;
			}
			case 1: {
				if (!this.choosen) {
					this.cancelNext = true;

					var params = { product_id : this.values[1].product_id };
					params.fb_page_id = this.values[0].fb_page_id;
					for (var i in this.products.elements) {
						if (this.products.elements[i]._id == params.product_id) {
							params.subscription_id = this.products.elements[i].base_reference;
							break;
						}
					}

					this.kinky.post(this, 'rest:/applications/subscribe/plan', params);
				}
				else {
					if (this.choosen.available_subscriptions.length == 1 && this.choosen.available_subscriptions[0].total_value == 0) {
						this.stepOverNext();
						var panel = this.childWidget('/apps/subscribe/3');
						panel.title = gettext(this.choosen.available_subscriptions[0].reference);
						var text = '<br><b>' + gettext('total') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', KSystem.truncateDouble(this.choosen.available_subscriptions[0].total_value, 2)).replace('$CURRENCY', this.choosen.available_subscriptions[0].currency) + '</span><br>';
						if (!!this.values[0].promotional_code && this.values[0].promotional_code != '') {
							text += '<br>' + gettext('$CODA_APPS_SUBSCRIPTION_PROMOCODE_FREE') + '</span><br>';
						}
						else {
							text += '<br>' + gettext('$CODA_APPS_SUBSCRIPTION_FREEMIUM') + '</span><br>';							
						}
						panel.description = text;
						panel.icon = 'css:fa fa-shopping-cart';

						var products = this.childWidget('/apps/subscribe/2').childWidget('/products');
						products.addOption(this.choosen.available_subscriptions[0], 'free', true);
						this.values[2].product_id = this.choosen.available_subscriptions[0];

						this.providerCode.addOption('free', 'Redpanda', true);
						this.providerCode.setStyle({
							display : 'none'
						});

					}
					else {
						var panel = this.childWidget('/apps/subscribe/2');
						var products = panel.childWidget('/products');
						for (var p in this.choosen.available_subscriptions) {
							products.addOption(this.choosen.available_subscriptions[p], '<b style="font-size: 16px;">' + KSystem.truncateDouble(this.choosen.available_subscriptions[p].total_value, 2) + '&nbsp;' + this.choosen.available_subscriptions[p].currency + '</b><br><i class="fa fa-chevron-right"></i> <i>' + gettext(this.choosen.available_subscriptions[p].reference) + '</i><br>&nbsp;', false);
						}
					}
				}
				break;
			}
			case 2: {
				var choosed = this.childWidget('/apps/subscribe/2').childWidget('/products').getValue();

				var panel = this.childWidget('/apps/subscribe/3');
				panel.title = gettext(choosed.reference);
				var text = '<br><b>' + gettext('net_value') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', parseFloat(choosed.net_value).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				if (!!choosed.discount_amount) {
					text += '<b>' + gettext('discount_amount') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', parseFloat(choosed.discount_amount).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				}
				if (!!choosed.discount_percentage) {
					text += '<b>' + gettext('discount_amount') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', (parseFloat(choosed.discount_percentage) * parseFloat(choosed.net_value)).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				}
				text += '<b>' + gettext('vat') + '</b>: <span>' + (parseFloat(choosed.vat) * 100) + '%</span></br>';
				text += '<b>' + gettext('total') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', KSystem.truncateDouble(choosed.total_value, 2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				panel.description = text;
				panel.icon = 'css:fa fa-shopping-cart';

				if (parseFloat(KSystem.truncateDouble(choosed.total_value, 2)) == 0) {
					this.providerCode.addOption('free', 'Redpanda', true);
					this.providerCode.setStyle({
						display : 'none'
					});

					panel.description += '<br><span>';
				}
				else {
					this.providerCode.setStyle({
						display : 'block'
					});
					this.providerCode.clear();
					for (var p in this.choosen.providers) {
						if (this.choosen.providers[p].code == 'free' ) {
							continue;
						}
						this.providerCode.addOption(this.choosen.providers[p].code, this.choosen.providers[p].name, false);
					}
				}
				break;
			}
			case 3: {
				if (this.values[3].paymentProvider_code == 'paypal' || this.values[3].paymentProvider_code == 'free') {
					this.stepOverNext();
				}
				break;
			}
			case 4: {
				if (this.values[3].paymentProvider_code == 'free') {
					this.stepOverNext();
				}
				break;
			}
		}
	};

	CODASubsriptionWizard.prototype.mergeValues = function() {
		var params = {};

		for (var v in this.values) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}

		if (!!params.product_id && typeof(params.product_id) == 'object' ) {
			params.subscription_id = params.product_id.reference;
			params.product_id = '/products/' + params.product_id.product_id;				
		}
		else {
			for (var i in this.products.elements) {
				if (this.products.elements[i]._id == params.product_id) {
					params.subscription_id = this.products.elements[i].base_reference;
					break;
				}
			}				
		}
		params.terms_accepted = (!!params.terms_accepted && params.terms_accepted.length == 1 ? params.terms_accepted[0] : 'no');

		return params;
	};

	CODASubsriptionWizard.prototype.onNoContent = function(data, request) {
		this.valid_subscriptions = {};
		this.fireNext();
	};

	CODASubsriptionWizard.prototype.onGetSubscriptions = function(data, request) {
		this.valid_subscriptions = data;
		this.fireNext();
	};

	CODASubsriptionWizard.prototype.onAccepted = function(data, request) {
		this.choosen = data;
		this.fireNext();
	};

	CODASubsriptionWizard.prototype.onPost = CODASubsriptionWizard.prototype.onCreated = function(data, request) {
		var _shell = this.shell;
		this.getParent('KFloatable').closeMe();
		KMessageDialog.alert({
			message : gettext('$CODA_SUBSCRIPTION_SUCCESS_MSG'),
			shell : _shell,
			html : true,
			modal : true,
			width : 400,
			height : 300
		});				
	};

	CODASubsriptionWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				window.CODA_APPS_WAITING_PAYMENT = this;
				var url = CODA_VERSION + '/modules/payments/payment_ongoing.html?rest_url=' + encodeURIComponent(this.getShell().data.providers.rest.services);
				for (var p in params) {
					url += '&' + p + '=' + encodeURIComponent(params[p]);
				}
				var pup = window.open(url, '*', 'width=1000,height=800,resizable=yes,scrollbars=yes');
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		catch (e) {

		}
	};

	CODASubsriptionWizard.addWizardPanel = function(parent)  {
		var pagePanel = new CODAApplicationFacebookResolver(parent);
		{
			pagePanel.hash = '/general0';

		}

		var typePanel = new KPanel(parent);
		{
			typePanel.hash = '/general';
			typePanel.title = gettext('$CODA_APPS_SUBSCRIPTION_TYPE');
			typePanel.description = gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP2').replace('$LANG', KLocale.LANG);
			typePanel.icon = 'css:fa fa-shopping-cart';

			var product_id = new KRadioButton(typePanel, gettext('$CODA_APPS_SUBSCRIPTION_TYPE') + '*', 'product_id');
			{
				for (var i in parent.products.elements) {
					product_id.addOption(parent.products.elements[i]._id, gettext(parent.products.elements[i].base_reference), false);
				}

				product_id.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP'), CODAForm.getHelpOptions());
				product_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			typePanel.appendChild(product_id);

			var promotionalCode = new KInput(typePanel, gettext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE') + '*', 'promotional_code');
			{
				promotionalCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE_HELP'), CODAForm.getHelpOptions());
			}
			typePanel.appendChild(promotionalCode);
		}

		var productsPanel = new KPanel(parent);
		{
			productsPanel.hash = '/general1';
			productsPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_TITLE');
			productsPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP').replace('$LANG', KLocale.LANG);
			productsPanel.icon = 'css:fa fa-shopping-cart';

			var product_id = new KRadioButton(productsPanel, gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT') + '*', 'product_id');
			{
				product_id.hash = '/products';
				product_id.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP'), CODAForm.getHelpOptions());
				product_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			productsPanel.appendChild(product_id);
		}

		var providerPanel = new KPanel(parent);
		{
			providerPanel.hash = '/general2';

			parent.providerCode = new KRadioButton(providerPanel, gettext('$CODA_APPS_SUBSCRIPTION_PROVIDER') + '*', 'paymentProvider_code');
			{
				parent.providerCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP'), CODAForm.getHelpOptions());
				parent.providerCode.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			providerPanel.appendChild(parent.providerCode);

			var termsAccepted = new KCheckBox(providerPanel, '', 'terms_accepted');
			{
				termsAccepted.addOption("yes", '', false);
				termsAccepted.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				termsAccepted.addCSSClass('CODAApplicationSubsriptionTermsCheck');
			}
			providerPanel.appendChild(termsAccepted);				

			var termsText = new KText(providerPanel); 
			{
				termsText.setStyle({
					marginLeft: '-15px',
					top: '4px'
				});
				termsText.setText(gettext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT'));
				termsText.addCSSClass('CODAApplicationSubsriptionTermsText');
			}
			providerPanel.appendChild(termsText);

		}

		var paymentPanel = new KPanel(parent);
		{
			paymentPanel.hash = '/general3';
			paymentPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_TITLE');
			paymentPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_HELP').replace('$LANG', KLocale.LANG);
			paymentPanel.icon = 'css:fa fa-credit-card';

			var cardNumber = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER') + '*', 'card_number');
			{
				cardNumber.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER_HELP'), CODAForm.getHelpOptions());
				cardNumber.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardNumber);

			var cardCvv = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDCVV') + '*', 'card_cvv');
			{
				cardCvv.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDCVV_HELP'), CODAForm.getHelpOptions());
				cardCvv.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardCvv);

			var cardHolder = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME') + '*', 'card_holder_name');
			{
				cardHolder.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME_HELP'), CODAForm.getHelpOptions());
				cardHolder.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardHolder);

			var cardExpDate = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE') + '*', 'card_expiration_date', 'Y/M', 'Y/M');
			{
				cardExpDate.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE_HELP'), CODAForm.getHelpOptions());
				cardExpDate.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardExpDate);

		}

		var addressPanel = new KPanel(parent);
		{
			addressPanel.hash = '/general4';
			addressPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_ADDRESS_TITLE');
			addressPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_ADDRESS_HELP').replace('$LANG', KLocale.LANG);
			addressPanel.icon = 'css:fa fa-envelope';

			var billingAddress = new KTextArea(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS') + '*', 'billing_address');
			{
				billingAddress.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS_HELP'), CODAForm.getHelpOptions());
				billingAddress.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingAddress);

			var billingPostalCode = new KInput(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE') + '*', 'billing_postal_code');
			{
				billingPostalCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE_HELP'), CODAForm.getHelpOptions());
				billingPostalCode.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingPostalCode);

			var billingCountry = new KAutoCompletion(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY') + '*', 'billing_country');
			{
				for (var c in CODA_COUNTRIES) {
					billingCountry.addOption(getlocale(CODA_COUNTRIES[c].name), getlocale(CODA_COUNTRIES[c].name), false);
				}

				billingCountry.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY_HELP'), CODAForm.getHelpOptions());
				billingCountry.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingCountry);

		}

		return [ pagePanel, typePanel, productsPanel, providerPanel, paymentPanel, addressPanel ];

	};

	KSystem.included('CODASubsriptionWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAToolbarDialog(parent, config, dispatcher) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = this.config = config;
		this.dispatcher = dispatcher;
	}
}

KSystem.include([
	'KButton',
	'KPanel'
], function() {
	CODAToolbarDialog.prototype = new KPanel();
	CODAToolbarDialog.prototype.constructor = CODAToolbarDialog;
	
	CODAToolbarDialog.prototype.unload = function() {
		if (this.display) {
			if (!!this.dispatcher) {
				this.dispatcher.enable();
				this.dispatcher.removeCSSClass(this.className + 'ButtonSelected');
			}
		}
		KWidget.prototype.unload.call(this);
	};
	
	CODAToolbarDialog.prototype.draw = function() {
		this.addCSSClass('CODAToolbarDialog');
		
		var pointerImg = window.document.createElement('img');
		pointerImg.className = ' CODAToobarDialogPointer ';
		pointerImg.src = Kinky.getShellConfig(this.shell).docroot + 'images/widget_dialog_attached_bg.png';
		this.container.appendChild(pointerImg);
		
		KPanel.prototype.draw.call(this);
	};
	
	KSystem.included('CODAToolbarDialog');
}, Kinky.CODA_INCLUDER_URL);
function CODAToolbar(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.menuActivated = false;

		this.menu = new KPanel(this);
		{
			this.addCSSClass('CODAToolbarMenu ' + this.className + 'ToolbarMenu');
		}
		this.appendChild(this.menu);

		this.activator = new KButton(this, '', 'activator', CODAToolbar.activateMenu);
		{
			this.activator.setText(gettext('$CODA_HEADER_MENU_ACTVATOR'), true);
			this.activator.addCSSClass('CODAToolbarMenuActivator ' + this.className + 'ToolbarMenuActivator');
		}
		this.appendChild(this.activator);

		this.addEventListener('dblclick', function(event) {
			alert(KLocale.speeky.failures);
		});

		var homehref  = window.document.createElement('a');
		{
			homehref.href = '/' + KLocale.LANG + '/www/';
			homehref.target = '_top';
			var img = window.document.createElement('img');
			{
				img.src = '/imgs/logo.png';
			}
			homehref.appendChild(img);
		}
		this.panel.insertBefore(homehref, this.content);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAToolbar.prototype = new KPanel();
	CODAToolbar.prototype.constructor = CODAToolbar;

	CODAToolbar.prototype.removeToolbarButton = function(button) {
		if (typeof(button) == 'string') {
			button = this.menu.childWidget(button);
		}
		if (!button) {
			return;
		}
		this.menu.removeChild(button);
	};

	CODAToolbar.prototype.addToolbarButton = function(button, color, position, level) {
		{
			button.parent = this.menu;
			button.level = (!!level ? CODAGenericShell.getDialogLayers() : (level === '' ? '' : 0))
			button.setText(button.label, true);
			button.addCSSClass('CODAToolbarButton');
			button.addCSSClass('CODAToolbarButtonLevel' + button.level);
			button.addCSSClass('CODAToolbarButton' + (color !== undefined && color !== null ? color : '1'));
			button.setStyle({
				marginRight: '0'
			});
		}
		this.menu.appendChild(button);
	};

	CODAToolbar.prototype.addToolbarSeparator= function() {
		return;
	};

	CODAToolbar.activateMenu = function(e) {
		var self = KDOM.getEventWidget(e).getParent('CODAToolbar');
		var xf = (self.menuActivated ? -KDOM.getBrowserWidth() : 0);
		var xi = (!self.menuActivated ? -KDOM.getBrowserWidth() : 0);
		var af = (self.menuActivated ? 0 : 1);
		var ai = (!self.menuActivated ? 0: 1);
		self.menuActivated = !self.menuActivated;
		self.activator.input.style.color = (self.menuActivated ? 'rgba(255, 255, 255, 0.75)' : 'rgb(255, 255, 255)');


		KEffects.addEffect(self.menu, [
			{
				f : KEffects.easeOutExpo,
				type : 'move',
				duration : 500,
				gravity : {
					x : 'right'
				},
				lock : {
					y : true
				},
				go : {
					x : xf
				},
				from : {
					x : xi
				}
			},
			{
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 1500,
				go : {
					alpha : af
				},
				from : {
					alpha : ai
				}
			}
		]);
	};

	KSystem.included('CODAToolbar');
}, Kinky.CODA_INCLUDER_URL);
function CODAWizard(parent, forms) {
	if (parent != null) {
		KLayeredPanel.call(this, parent);
		this.fclass = forms;
		this.forms = [];
		this.values = [];
		this.current = 0;
		this.headers = {};

		this.layout = KLayeredPanel.TABS;
		this.messages = window.document.createElement('div');
		this.messages.innerHTML = '&nbsp;';
		this.setStyle({
			opacity : 0
		}, this.messages);
		this.panel.appendChild(this.messages);

		this.info = window.document.createElement('div');
		this.info.innerHTML = '';
		this.info.className = ' CODAWizardInfoPanel ' + this.className + 'InfoPanel ';
		
		this.INFO_HEIGHT = 60;
		this.max_height = 0;
		this.dynamic_height = false;
		this.helpURL = CODAGenericShell.generateHelpURL(this);

		var forms = undefined;
		if((forms = KCache.restore('/__tweaks')) != undefined) {
			if (!!forms[this.className]) {
				this.fieldConfig = forms[this.className];
			}
		}		
	}
}

KSystem.include([
	'KPanel',
	'KLayeredPanel'
], function() {
	CODAWizard.prototype = new KLayeredPanel();
	CODAWizard.prototype.constructor = CODAWizard;

	CODAWizard.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAWizard.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		for (var c in this.forms[this.current].childWidgets()) {
			var child = this.forms[this.current].childWidget(c);
			if (child instanceof KAbstractInput && !(child instanceof KStatic)) {
				KSystem.addTimer(function() {
					child.focus();
				}, 150);
				break;
			}
		}
	};

	CODAWizard.prototype.load = function() {
		this.draw();
	};

	CODAWizard.prototype.onNotFound = CODAWizard.prototype.onNoContent = function(data, request) {
		this.activate();
	};

	CODAWizard.prototype.onUnauthorized = function(data) {
	};

	CODAWizard.prototype.invisible = function() {
		KLayeredPanel.prototype.invisible.call(this);
	};

	CODAWizard.prototype.visible = function() {
		if (this.display) {
			return;
		}
		var floatable = this.getParent('KFloatable');
		this.dynamic_height = floatable.height == -1;

		KLayeredPanel.prototype.visible.call(this);
		this.panel.insertBefore(this.header, this.messages);

		var height = 0;
		for ( var c in this.childWidgets()) {
			if (!(this.childWidget(c) instanceof KButton)) {
				var p = this.childWidget(c);
				if (floatable.height != -1) {
					p.setStyle({
						height : (floatable.height - 150 - this.INFO_HEIGHT) + 'px'
					});
				}
				else {
					height = Math.max(p.getHeight(), height);
				}
				for (var pc in p.childWidgets()) {
					var w = p.childWidget(pc);
					if (w instanceof KImageUpload) {
						w.addEventListener('imageload', function(event) {
							KDOM.getEventWidget(event, 'CODAWizard').resize();
						});
					}
					else if (w instanceof KFile) {
						w.addEventListener('change', function(event) {
							KDOM.getEventWidget(event, 'CODAWizard').resize();
						});
					}
				}
			}
		}

		if (floatable.height == -1 && height != 0) {
			this.setStyle({
				height : (height + this.INFO_HEIGHT) + 'px'
			}, KWidget.CONTENT_DIV);
		}
		else {
			this.setStyle({
				height : (floatable.height - 150) + 'px'
			}, KWidget.CONTENT_DIV);
		}

	};

	CODAWizard.prototype.visibleTab = function(child) {
		this.resize(child);
		this.focus();
	};	
	
	CODAWizard.prototype.invisibleTab = function(child) {
	};	

	CODAWizard.prototype.resize = function(child) {
		if (this.dynamic_height) {
			if (!child) {
				child = this.forms[this.current];
			}
			var floatable = this.getParent('KFloatable');
			this.max_height = Math.max(child.getHeight(), child.getRootHeight());

			if (this.max_height != 0) {
				floatable.data.params.transition_wait_target = (child.hash != this.hash + '/0');
				floatable.resize(this.max_height + this.INFO_HEIGHT + 150);

				child.setStyle({
					height : this.max_height + 'px'
				});
				this.setStyle({
					height : (this.max_height + this.INFO_HEIGHT) + 'px'
				}, KWidget.CONTENT_DIV);
			}
		}
	};	
	
	CODAWizard.prototype.setInfo = function(node) {
		this.info.innerHTML = '';
		if (!!this.infotoggle) {
			delete this.infotoggle;
		}
		this.removeCSSClass('CODAWizardInfoPanelMinimized', this.info);

		if (typeof node == 'string') {
			this.info.innerHTML = node;
		}
		else {
			this.info.appendChild(node);
		}
		if (this.forms.length > this.current) {
			this.forms[this.current].content.insertBefore(this.info, this.forms[this.current].content.childNodes[0]);
		}

		this.infotoggle = window.document.createElement('button');
		this.infotoggle.innerHTML = '<i class="fa fa-chevron-up"></i>';
		KDOM.addEventListener(this.infotoggle, 'click', CODAWizard.toggleInfo);
		this.info.appendChild(this.infotoggle);
	};

	CODAWizard.prototype.stepOverPrevious = function() {
		this.cancelPrevious = true;
		this.current--;
		this.firePrevious();
	};

	CODAWizard.prototype.stepOverNext = function() {
		this.cancelNext = true;
		this.current++;
		this.steppedOverNext = true;
		this.fireNext();
	};

	CODAWizard.prototype.fireNext = function() {
		KDOM.fireEvent(this.nextbt.input, 'click');		
	};

	CODAWizard.prototype.firePrevious= function() {
		KDOM.fireEvent(this.prevbt.input, 'click');		
	};

	CODAWizard.prototype.draw = function() {
		this.addCSSClass('CODAForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addCSSClass('CODAWizard');
		this.addCSSClass('CODAWizardContent', this.content);
		this.addCSSClass('CODAWizardMessages', this.messages);

		this.go = new KButton(this, gettext('$CONFIRM'), 'go', CODAWizard.next);
		this.go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
		this.go.addCSSClass('CODAWizardButtonGo');
		this.appendChild(this.go);

		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', CODAWizard.closeMe);
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAWizardButtonNoGo');
		this.appendChild(nogo);

		for (var f in this.fclass) {
			var panel = null;
			eval('panel = ' + this.fclass[f] + '.addWizardPanel(this);');
			if (panel instanceof Array) {
				for (var p in panel) {
					this.addPanel(panel[p]);
				}
			}
			else {
				this.addPanel(panel);
			}
		}

		this.prevbt = new KButton(this, gettext('$PREVIOUS'), 'prev', CODAWizard.previous);
		this.prevbt.setText('<i class="fa fa-chevron-circle-left"></i> ' + gettext('$PREVIOUS'), true);
		this.prevbt.addCSSClass('CODAWizardButtonPrev');
		this.appendChild(this.prevbt);

		this.nextbt = new KButton(this, gettext('$NEXT'), 'next', CODAWizard.next);
		this.nextbt.setText(gettext('$NEXT') + ' <i class="fa fa-chevron-circle-right"></i>', true);
		this.nextbt.addCSSClass('CODAWizardButtonNext');
		this.appendChild(this.nextbt);

		if (this.forms.length == 1) {
			this.nextbt.setStyle({
				display: 'none'
			});
			this.go.setStyle({
				display: 'block'
			});
		}
		this.prevbt.setStyle({
			display: 'none'
		});

		KLayeredPanel.prototype.draw.call(this);
		if (!!this.msg) {
			this.showMessage(this.msg);
			this.msg = null;
		}

		this.steps = window.document.createElement('span');
		this.steps.className = ' CODAWizardSteps ' + this.className + 'Steps ';
		this.steps.innerHTML = (this.current + 1) + '/' + this.forms.length;
		this.title.appendChild(this.steps);

		if (!!this.forms[this.current].title && !!this.forms[this.current].description) {
			var desc = this.forms[this.current].title || '';
			desc = '<div class="info"><b class="title">' + desc + '</b><br><div class="body">' + this.forms[this.current].description + '</div>';
			if (!!this.forms[this.current].icon && this.forms[this.current].icon.indexOf('css:') == 0) {
				desc = '<i class="' + this.forms[this.current].icon.replace('css:', '') + '  fa-2x pull-left"></i>' + desc;
			}
			else if (!!this.forms[this.current].icon) {
				desc = '<img src="' + this.forms[this.current].icon + '"></img>' + desc;
			}
			else {
				desc = '<i class="no-icon">&nbsp;</i>' + desc;
			}
			desc += '</div>';
			this.setInfo(desc);
		}
		else if (this.info.innerHTML != '') {
			this.forms[this.current].content.insertBefore(this.info, this.forms[this.current].content.childNodes[0]);			
		}
		this.makeHelp();
	};

	CODAWizard.prototype.removePanel = function(numberHash) {
		if (!numberHash) {
			return;
		}
		var child = undefined;
		if (typeof numberHash == 'string') {
			child = this.childWidget(numberHash);
		}
		else if (typeof numberHash == 'number') {
			child = this.childWidget(this.hash + '/' + numberHash);
		}

		if (!!child) {
			for (var i in this.forms) {
				if (this.forms[i].id == child.id) {
					this.forms.splice(i, 1);
					break;
				}
			}
			this.removeChild(child);
		}
	};

	CODAWizard.prototype.addPanel = function(panel) {
		if (!panel) {
			return;
		}
		if (typeof panel == 'string') {
			eval('panel = ' + panel + '.addWizardPanel(this);');
			if (panel instanceof Array) {
				for (var p in panel) {
					this.addPanel(panel[p]);
				}
			}
			else {
				this.addPanel(panel);
			}
			return;
		}
		panel.hash = this.hash + '/' + this.forms.length;
		KLayeredPanel.prototype.addPanel.call(this, panel, '' + this.forms.length, this.forms.length == 0);
		this.forms.push(panel);
		if(this.activated()) {
			var max_height = Math.min(KDOM.getBrowserHeight() - this.INFO_HEIGHT - 150, Math.max(panel.getHeight(), panel.getRootHeight()));

			if (max_height != 0) {
				panel.setStyle({
					height : max_height + 'px'
				});
			}
		}
		if (!!this.fieldConfig) {
			this.applyFieldConfiguration(panel);
		}
	};

	CODAWizard.prototype.applyFieldConfiguration = function(panel) {
		CODAGenericShell.applyFieldConfiguration(panel, this.fieldConfig, this.className);
	};

	CODAWizard.prototype.showMessage = function(message, persistent) {
		if (!!this.timer) {
			KSystem.removeTimer(this.timer);
			this.timer = null;
		}
		if (persistent) {
			this.disable();
		}
		else {
			this.enable();
			this.messages.innerHTML = message;
			this.removeCSSClass('CODAFormErrorMessages', this.messages);
			this.addCSSClass('CODAFormSuccessMessages', this.messages);
			
			this.setStyle({
				display : 'block',
				position: 'absolute',
				bottom : '30px',
				right: '210px'
			}, this.messages);

			KEffects.addEffect(this.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				applyToElement : true,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onComplete : function(w, t) {
					if (!!w && !persistent) {
						w.timer = KSystem.addTimer('CODAWizard.hideMessage(\'' + w.id + '\')', 3000);
					}
				}
			});
		}

	};

	CODAWizard.hideMessage = function(id) {
		var form = Kinky.getWidget(id);
		if (!form) {
			return;
		}
		form.timer = null;
		if (!!form) {
			form.enable();
			KEffects.addEffect(form.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (!!w) {
						w.messages.style.display = 'none';
						w.messages.innerHTML = '&nbsp;';
					}
				}
			});
		}
	};

	CODAWizard.prototype.close = function() {
		this.onClose();
		this.parent.closeMe();
	};

	CODAWizard.prototype.onClose = function() {
	};

	CODAWizard.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAWizard.prototype.onGet = function(data, request) {
	};

	CODAWizard.prototype.onError = function(data, request) {
		this.enable();
		var msg = undefined;
		eval('msg = ' + gettext('$CONFIRM_OPERATION_ERROR').replace('[NUMBER]', data.error_code));
		this.showMessage(msg);
		this.removeCSSClass('CODAFormSuccessMessages', this.messages);
		this.addCSSClass('CODAFormErrorMessages', this.messages);
	};

	CODAWizard.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAWizard.prototype.onPost = CODAWizard.prototype.onCreated = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAWizard.prototype.mergeValues = function() {
		var params = {};
		for (var v in this.values) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}
		return params;
	};

	CODAWizard.prototype.save = function() {
		this.disable();
		this.showMessage( '<i class="fa-spinner fa-spin"></i> ' + gettext('$CODA_WORKING'), true);

		var params = this.mergeValues();
		this.send(params);
	};

	CODAWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				var headers = KSystem.merge({}, this.config.headers || {});
				headers = KSystem.merge(headers, this.headers);
				headers['X-Fields-Nillable'] = '' + this.nillable;

				if (!!this.target.href) {
					this.kinky.put(this, 'rest:' + this.target.href, params, headers);
				}
				else {
					this.kinky.post(this, 'rest:' + this.target.collection, params, headers);
				}
			}
		}
		catch (e) {

		}
	};

	CODAWizard.prototype.onPrevious = function() {	
	};

	CODAWizard.prototype.onNext = function() {	
	};

	CODAWizard.prototype.dispatchPrevious = function() {		
		if (this.current == 0) {
			//alert("something wrong!!!!!!!!! this.current == " + this.current);
			return;
		}
		
		this.onPrevious();
		if (this.cancelPrevious) {
			this.cancelPrevious = false;
			return;
		}
		this.current--;
		this.steps.innerHTML = (this.current + 1) + '/' + this.forms.length;

		if (this.current == 0) {
			this.prevbt.setStyle({
				display: 'none'
			});
		}
		if (this.current != this.forms.length - 1) {
			this.nextbt.setStyle({
				display: 'block'
			});
			this.go.setStyle({
				display: 'none'
			});
		}

		if (!!this.forms[this.current].title && !!this.forms[this.current].description) {
			var title = this.forms[this.current].title;
			var body = this.forms[this.current].description;

			var desc = title || '';
			desc = '<div class="info"><b class="title">' + desc + '</b><br><div class="body">' + body + '</div>';
			if (!!this.forms[this.current].icon && this.forms[this.current].icon.indexOf('css:') == 0) {
				desc = '<i class="' + this.forms[this.current].icon.replace('css:', '') + '  fa-2x pull-left"></i>' + desc;
			}
			else if (!!this.forms[this.current].icon) {
				desc = '<img src="' + this.forms[this.current].icon + '"></img>' + desc;
			}
			else {
				desc = '<i class="no-icon">&nbsp;</i>' + desc;
			}
			desc += '</div>';
			this.setInfo(desc);
		}
		
		KBreadcrumb.dispatchEvent(this.id, {
			action : this.forms[this.current].hash
		});
	};

	CODAWizard.prototype.dispatchNext = function() {		
		if (this.current == this.forms.length) {
			//alert("something wrong!!!!!!!!! wizard.current == " + this.current);
			return;
		}

		var panel = this.forms[this.current];
		if (panel instanceof KPanel) {
			if (this.steppedOverNext) {
				this.values[this.current] = {};
				this.steppedOverNext = false;
			}
			else {
				if (!!panel.params) {
					if (panel.validate()) {	
						this.values[this.current] = panel.params;
					}
					else {
						return;					
					}
				}
				else {
					var params = {};
					var doSubmit = true;
					for ( var i in panel.childWidgets()) {
						var input = panel.childWidget(i);

						if ((input instanceof KAbstractInput || input instanceof KHidden) && !(input instanceof KButton)) {
							var value = input.getValue();
							var validation = input.validate();

							if (validation != true) {
								if (doSubmit) {
									input.focus();
								}
								input.errorArea.innerHTML = validation;
								doSubmit = false;
							}
							else if ((!!value && value != '') || (!this.nillable)) {
								var objpath = input.getInputID();
								CODAWizard.createObject(params, objpath.split('.'));
								eval('params.' + input.getInputID() + ' = value');
							}
						}
						else if (!!input.parent.save) {
							var value = input.parent.save();

							for ( var c in value) {
								if ((!!value[c] && value[c] != '') || (!this.nillable)) {
									CODAWizard.createObject(params, c.split('.'));
									eval('params.' + c + ' = value[c]');
								}
							}
						}
					}
					if (!doSubmit) {
						return;
					}
					this.values[this.current] = params;
				}
			}
		}

		this.onNext();
		if (this.cancelNext) {
			this.cancelNext = false;
			return;
		}
		this.current++;

		if (this.current == this.forms.length) {
			this.current--;
			this.save();
			return;
		}

		this.steps.innerHTML = (this.current + 1) + '/' + this.forms.length;

		if (!!this.forms[this.current].title && !!this.forms[this.current].description) {
			var desc = this.forms[this.current].title || '';
			desc = '<div class="info"><b class="title">' + desc + '</b><br><div class="body">' + this.forms[this.current].description + '</div>';
			if (!!this.forms[this.current].icon && this.forms[this.current].icon.indexOf('css:') == 0) {
				desc = '<i class="' + this.forms[this.current].icon.replace('css:', '') + ' fa-2x pull-left"></i>' + desc;
			}
			else if (!!this.forms[this.current].icon) {
				desc = '<img src="' + this.forms[this.current].icon + '"></img>' + desc;
			}
			else {
				desc = '<i class="no-icon">&nbsp;</i>' + desc;
			}
			desc += '</div>';
			this.setInfo(desc);
		}

		if (this.current == this.forms.length - 1) {
			this.nextbt.setStyle({
				display: 'none'
			});
			this.go.setStyle({
				display: 'block'
			});
		}
		this.prevbt.setStyle({
			display: 'block'
		});
		ga('send', 'pageview', window.document.location.pathname + this.forms[this.current].hash.substr(1));
		KBreadcrumb.dispatchEvent(this.id, {
			action : this.forms[this.current].hash
		});
	};

	CODAWizard.previous = function(event) {
		var wizard = KDOM.getEventWidget(event).getParent('CODAWizard');
		wizard.dispatchPrevious();
	};

	CODAWizard.next = function(event) {
		var wizard = KDOM.getEventWidget(event).getParent('CODAWizard');
		wizard.dispatchNext();
	};

	CODAWizard.closeMe = function(event) {
		var form = KDOM.getEventWidget(event).parent;
		KBreadcrumb.dispatchURL({
			hash : ''
		});
		form.onClose();
		form.parent.closeMe();
	};

	CODAWizard.createObject = function(params, objpath) {
		if (typeof objpath == 'string') {
			objpath = objpath.split('.');
		}
		if (objpath.length == 0) {
			return;
		}
		var exists = false;
		var arrindex = 0;
		if ((arrindex = objpath[0].indexOf('[')) != -1) {
			eval('exists = !!params.' + objpath[0].substr(0, arrindex) + ';');
			if (!exists) {
				eval('params.' + objpath[0].substr(0, arrindex) + ' = new Array();');
			}
		}

		eval('exists = !!params.' + objpath[0] + ';');
		if (!exists) {
			eval('params.' + objpath[0] + ' = {};');
		}
		var p = null;
		eval('p = params.' + objpath[0] + ';');
		CODAWizard.createObject(p, objpath.slice(1));
	};

	CODAWizard.toggleInfo = function(event) {
		var w = KDOM.getEventWidget(event, 'CODAWizard');
		if (/CODAWizardInfoPanelMinimized/.test(w.info.className)) {
			w.removeCSSClass('CODAWizardInfoPanelMinimized', w.info);
			w.infotoggle.innerHTML = '<i class="fa fa-chevron-up"></i>';
		}
		else {
			w.addCSSClass('CODAWizardInfoPanelMinimized', w.info);
			w.infotoggle.innerHTML = '<i class="fa fa-chevron-down"></i>';
		}
	};

	CODAWizard.instantiate = function(params, field, node) {
	};
	
	KSystem.included('CODAWizard');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

