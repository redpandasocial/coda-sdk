KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAPaymentListItem(parent) {
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAPaymentListItem.prototype = new CODAListItem();
	CODAPaymentListItem.prototype.constructor = CODAPaymentListItem;

	CODAPaymentListItem.prototype.remove = function() {
		var widget = this;
		if(!!widget.data.status && widget.data.status == 'scheduled'){
			KConfirmDialog.confirm({
				question : gettext('$DELETE_PAYMENT_CONFIRM'),
				callback : function() {
					widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onPaymentDelete');
				},
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}else{
			KMessageDialog.alert({
				message : gettext('$DELETE_PAYMENT_CANNOT_BE_DONE'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}

	};

	CODAPaymentListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);

		if(!!this.data && !!this.data.status){
			var execute_paymentBt = new KButton(this, '', 'executepayment', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAPaymentListItem');
				KBreadcrumb.dispatchURL({
					hash :  widget.data.status == 'scheduled' ? '/execute-payment' + widget.data.href : 'already-payed'
				});
			});
			{
				execute_paymentBt.setText(this.data.status == 'scheduled' ? '<i class="fa fa-gear"></i> ' + gettext('$EXECUTE_PAYMENT_LABEL') : '<i class="fa fa-flag"></i> ' + gettext('$PAYMENT_EXECUTED_LABEL'), true);
			}

			this.appendChild(execute_paymentBt);
		}

	};

	CODAPaymentListItem.prototype.onPaymentDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	KSystem.included('CODAPaymentListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAPaymentsDashboard(parent) {
	if (parent != null) {
		var icons = [];

		icons.push({
			icon : 'css:fa fa-money',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_PAYMENTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_PAYMENTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-payments'
			}]
		});

		icons.push({
			icon : 'css:fa fa-barcode',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/get-promotional-codes'
			}]
		});

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAPaymentsDashboard.prototype = new CODAMenuDashboard();
	CODAPaymentsDashboard.prototype.construtor = CODAPaymentsDashboard;

	CODAPaymentsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAPaymentsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAPaymentsGetPromotionalCodeWizard(parent, themeID) {
	if (parent != null) {
		CODAWizard.call(this, parent, [ 'CODAPaymentsGetPromotionalCodeWizard' ]);
		this.setTitle(gettext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW'));
		this.hash = '/promotional/codes/get';
		this.codes = [];
	}
}

KSystem.include([
	'CODAWizard'
], function() {
	CODAPaymentsGetPromotionalCodeWizard.prototype = new CODAWizard();
	CODAPaymentsGetPromotionalCodeWizard.prototype.constructor = CODAPaymentsGetPromotionalCodeWizard;

	CODAPaymentsGetPromotionalCodeWizard.prototype.onPost = CODAPaymentsGetPromotionalCodeWizard.prototype.onCreated = function(data, request) {
		this.count--;
		this.codes.push(data.promotional_code);
		if (this.count != 0) {
			this.kinky.post(this, 'rest:/payments/codes/promotional', { discount_percentage : this.values[0].discount_percentage });
		}
		else {
			var codes = this.codes;
			var shell = this.shell;
			this.close();
			KMessageDialog.alert({
				html : true,
				message : '<div style="font-size: 12px;height: 160px;overflow-y: auto;line-height: 1.2em;">' + codes.join('<br>') + '</div>',
				shell : shell,
				modal : true,
				width : 400,
				height : 300
			});			
		}
	};

	CODAPaymentsGetPromotionalCodeWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				this.count = params.ncodes;
				this.kinky.post(this, 'rest:/payments/codes/promotional', { discount_percentage : params.discount_percentage });
			}
		}
		catch (e) {

		}
	};

	CODAPaymentsGetPromotionalCodeWizard.addWizardPanel = function(parent) {
		var codePanel = new KPanel(parent);
		{
			codePanel.hash = '/general';
			codePanel.title = gettext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_TITLE');
			codePanel.description = gettext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_DESC');
			codePanel.icon = 'css:fa fa-barcode';

			var discount_percentage = new KInput(codePanel, gettext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE') + ' *', 'discount_percentage');
			{
				discount_percentage.setLength(8);
				discount_percentage.setHelp(gettext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE_HELP'), CODAForm.getHelpOptions());
				discount_percentage.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				discount_percentage.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
			}
			codePanel.appendChild(discount_percentage);

			var ncodes = new KInput(codePanel, gettext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT'), 'ncodes');
			{
				ncodes.setHelp(gettext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT_HELP'), CODAForm.getHelpOptions());
				ncodes.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				ncodes.addValidator(/^([1-9])$/, gettext('$ERROR_PAYMENTS_PROMOTIONALCODE_DISCOUNT_COUNT_MAX'));
			}
			codePanel.appendChild(ncodes);

		}
		return [ codePanel ];
	};

	KSystem.included('CODAPaymentsGetPromotionalCodeWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAPaymentsList (parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, '', {
			add_button : false,
			export_button : true,
			one_choice : true,
			delete_button : false,
			edit_button : false
		});
		this.pageSize = 5;
		this.table = {
			"reference" : {
				label : gettext('$PAYMENTS_LIST_REFERENCE'),
				orderBy : true
			},
			"status_name" : {
				label : gettext('$PAYMENTS_LIST_STATUS')
			},
			"value" : {
				label : gettext('$PAYMENTS_LIST_VALUE')
			},
			"date" : {
				label : gettext('$PAYMENTS_LIST_DATE'),
				orderBy : true
			},
			"payment_provider_name" : {
				label : gettext('$PAYMENTS_LIST_PAYMENTPROVIDER')
			}
		};
		this.LIST_MARGIN = 50;
		this.filterFields = [ 'reference' ];
		this.orderBy = '-reference';
	}
}

KSystem.include([
	'CODAList',
	'KLink',
], function() {
	CODAPaymentsList.prototype = new CODAList();
	CODAPaymentsList.prototype.constructor = CODAPaymentsList;

	CODAPaymentsList.prototype.draw = function() {

		CODAList.prototype.draw.call(this);
	};

	CODAPaymentsList.prototype.addItems = function(list) {
		for(var index in this.elements) {
			var listItem = new CODAListItem(list);

			listItem.data = this.elements[index];

			var paymentproviders = KCache.restore('/paymentproviders');
			if (!!paymentproviders){
				for (var index1 in paymentproviders){
					if(!isNaN(index1) && (!!this.elements[index]["paymentmethod"] && paymentproviders [index1].code == this.elements[index]["paymentmethod"]["paymentProvider_code"])){
						listItem.data.payment_provider_name = paymentproviders [index1].name;
						break;
					}else{
						continue;
					}
				}
			}

			if(!!this.elements[index]["paymentparcels"]){
				var total_value = 0;
				for (var index2 in this.elements[index]["paymentparcels"]["elements"]){
					total_value = total_value + (this.elements[index]["paymentparcels"]["elements"][index2]["total_value"]);
				}
				listItem.data.value = total_value;
			}

			if(!!this.elements[index].status){
				switch (this.elements[index].status){
					case 'payed' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_PAYED');
						break;
					}
					case 'scheduled' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_SCHEDULED');
						break;
					}
					case 'voided' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_VOIDED');
						break;
					}
					case 'unauthorized' : {
						this.elements[index].status_name = gettext('$PAYMENT_STATUS_UNAUTHORIZED');
						break;
					}
				}
			}


			listItem.addMenuButtons = function() {
				if (this.data.status == 'payed') {
					var getInvoice = new KButton(this, '', 'getinvoice', function(event) {
						KDOM.stopEvent(event);
					});
					{
						getInvoice.setText('<i class="fa fa-files-o"></i> ' + gettext('$CODA_MY_PAYMENTS_LIST_GET_INVOICE'), true);
					}
					this.appendChild(getInvoice);
				}
			};

			list.appendChild(listItem);
		}
	};

	CODAPaymentsList.prototype.exportList = function() {
		KBreadcrumb.dispatchEvent(null, {
			hash : '/generate-payments-report'
		});
	};

    KSystem.included('CODAPaymentsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAPaymentsShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAPaymentsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAPaymentsGetPromotionalCodeWizard',
	'CODAPaymentsDashboard',
	'CODAPaymentsList',
	'CODAPaymentListItem',
	'CODASchedulePaymentsWizard',
	'CODASchedulePaymentsForm'
], function(){
	CODAPaymentsShell.prototype = new CODAGenericShell();
	CODAPaymentsShell.prototype.constructor = CODAPaymentsShell;

	CODAPaymentsShell.prototype.loadShell = function() {
		this.kinky.get(this, 'rest:/retrieveproviders', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadProviders');
	};

	CODAPaymentsShell.prototype.onLoadProviders = function(data, request) {
		if(!!data && !!data.length > 0){
			KCache.commit('/paymentproviders', data);
		}
		this.draw();
	};

	CODAPaymentsShell.prototype.draw = function() {
		this.setTitle(gettext('$PAYMENTS_SHELL_TITLE'));

		this.dashboard = new CODAPaymentsDashboard(this);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAPaymentsShell.sendRequest = function() {
	};

	CODAPaymentsShell.prototype.listPayments = function(href, minimized) {
		CODAPaymentsShell.payments_list = new CODAPaymentsList(this, {
			links : {
				feed : { href : href + '&fields=elements,size'},
			}
		});
		CODAPaymentsShell.payments_list.setTitle(gettext('$LIST_PAYMENTS_PROPERTIES'));
		this.makeDialog(CODAPaymentsShell.payments_list, {minimized : minimized, modal : true});

	};

	CODAPaymentsShell.prototype.getPromotionalCodes = function() {
		this.makeWizard(new CODAPaymentsGetPromotionalCodeWizard(this));
	};

	CODAPaymentsShell.prototype.executePayment = function(href, minimized) {
		setTimeout(function(){CODAPaymentsShell.payments_list.list.refreshPages();},30000);
		var pup = window.open(this.getShellConfig().providers.rest.data + '/paypal/approve?payment_id=' + href + '&access_token=' + encodeURIComponent(this.getShellConfig().headers.Authorization.replace('OAuth2.0 ', '')), 'Pagamento', 'width=1000,height=800');
		KSystem.popup.check(pup, Kinky.getDefaultShell())
	};

	CODAPaymentsShell.prototype.schedulePayments = function() {
		this.makeWizard(new CODASchedulePaymentsWizard(this));
	};

	CODAPaymentsShell.prototype.generatePaymentsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/payments', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onPaymentsReportGenerated');
	};

	CODAPaymentsShell.prototype.onPaymentsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_PAYMENTS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAPaymentsShell.prototype.onPreConditionFailed = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$PAYMENTS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAPaymentsShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]){
			case 'list-payments' : {
				Kinky.getDefaultShell().listPayments('/payments?embed=elements(*(paymentmethod,paymentparcels))', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'execute-payment' : {
				Kinky.getDefaultShell().executePayment(parts[3], false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'schedule-payments' : {
				Kinky.getDefaultShell().schedulePayments();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
			}
			case 'get-promotional-codes' : {
				Kinky.getDefaultShell().getPromotionalCodes();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit' : {
				break;
			}
			case 'edit-full' : {
				break;
			}
			case 'generate-payments-report': {
				Kinky.getDefaultShell().generatePaymentsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}

	};

	CODAPaymentsShell.prototype.onNoContent = function(data, request) {
		if(request.service == "/generatereport/payments"){
			KMessageDialog.alert({
				message : gettext('$PAYMENTS_MODULE_ERROR_REPORT_NO_CONTENT'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}
		else{
			this.draw();
		}
	};

	KSystem.included('CODAPaymentsShell');
}, Kinky.CODA_INCLUDER_URL);
function CODASchedulePaymentsForm(parent, href) {
	if (parent != null) {
		CODAForm.call(this, parent, href, null, null);
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = '/shcedulepayments';
	}
}

KSystem.include([
 	'CODAForm',
 	'KRadioButton',
 	'KDate',
 	'KTextArea',
 	'KCalendar'
 ], function() {
	CODASchedulePaymentsForm.prototype = new CODAForm();
	CODASchedulePaymentsForm.prototype.constructor = CODASchedulePaymentsForm;

	CODASchedulePaymentsForm.prototype.load = function() {

		this.kinky.get(this, 'rest:/retrieveproviders', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadProviders');

	};

	CODASchedulePaymentsForm.prototype.onLoadProviders = function(data, request) {

		if(!!data && !!data.elements && !!data.elements.length > 0){
			this.payment_providers = data.elements;
		}

		CODAForm.prototype.load.call(this);
	};

	CODASchedulePaymentsForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODASchedulePaymentsForm.addWizardPanel = function(parent)  {

		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/schedulepayments_1';
			var paymentproviders = KCache.restore('/paymentproviders');
			if (!!paymentproviders){
				var payment_providers_combo = new KCombo(firstPanel, gettext('$SCHEDULE_PAYMENT_PROVIDERS') + ' *', 'paymentProvider_code');
				{
					for (var index in paymentproviders){
						if(!isNaN(index)){
							payment_providers_combo.addOption(paymentproviders [index].code, paymentproviders [index].name, (!!this.data && !!this.data.paymentProvider_code && this.data.paymentProvider_code == paymentproviders[index].code) ? true : false);
						}
					}
					payment_providers_combo.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
				firstPanel.appendChild(payment_providers_combo);
			}

			//os valores passados para o pagamento serão obtidos do objecto que lhe dá origem. estes apenas estão aqui como exemplo
			var currency = new KStatic(firstPanel, gettext('$SCHEDULE_PAYMENT_CURRENCY'), 'currency');
			currency.setValue("EUR");
			firstPanel.appendChild(currency);

			var values = new KStatic(firstPanel, gettext('$SCHEDULE_PAYMENT_VALUE'), 'values');
			values.setValue(1.01);
			firstPanel.appendChild(values);

			var nr_of_payments = new KStatic(firstPanel, gettext('$SCHEDULE_PAYMENT_NR_OF_PAYMENTS'), 'nr_of_payments');
			nr_of_payments.setValue(10);
			firstPanel.appendChild(nr_of_payments);

			var nr_days_interval = new KStatic(firstPanel, gettext('$SCHEDULE_PAYMENT_NR_DAYS_INTERVAL'), 'nr_days_interval');
			nr_days_interval.setValue(2);
			firstPanel.appendChild(nr_days_interval);

			var first_payment_date = new KStatic(firstPanel, gettext('$SCHEDULE_PAYMENT_FIRST_PAYMENT_DATE'), 'first_payment_date');
			var today_date = new Date();
			var payment_date = new Date();
			payment_date.setDate(today_date.getDate() + 3);

			var dd = payment_date.getDate();
			var mm = payment_date.getMonth() + 1;
			var y = payment_date.getFullYear();

			var h = payment_date.getHours();
			var m = payment_date.getMinutes() + 1;
			var s = payment_date.getSeconds();

			var payment_date_formatted = y + '-'+ mm + '-'+ dd + ' ' + h + ':' + m + ':' + s;

			first_payment_date.setValue(payment_date_formatted);
			firstPanel.appendChild(first_payment_date);

		}

		var secondPanel = new KPanel(parent);

		return [firstPanel, secondPanel];

	};

	CODASchedulePaymentsForm.prototype.onNoContent = function(data, request) {
		CODASchedulePaymentsForm.prototype.load.call(this);
	};

	KSystem.included('CODASchedulePaymentsForm');
}, Kinky.CODA_INCLUDER_URL);
function CODASchedulePaymentsWizard(parent){
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODASchedulePaymentsForm']);
		this.setTitle(gettext('$PAYMENTS_SCHEDULE_NEW'));
		this.hash = '/payments/schedule';
		var desc = gettext('$PAYMENTS_SCHEDULE_WIZARD_TITLE1');
		desc = '<b>' + desc + '</b><br><span>' + gettext('$PAYMENTS_SCHEDULE_WIZARD_INFO1') + '</span>';
		desc = '<i class="fa fa-money fa-2x pull-left"></i>' + desc;
		this.setInfo(desc);
	}
}

KSystem.include([
 	'CODAWizard'
], function() {
	CODASchedulePaymentsWizard.prototype = new CODAWizard();
	CODASchedulePaymentsWizard.prototype.constructor = CODASchedulePaymentsWizard;

	CODASchedulePaymentsWizard.prototype.onPrevious = function() {
		switch(this.current) {
			case 1: {
				var desc = gettext('$PAYMENTS_SCHEDULE_WIZARD_TITLE1');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PAYMENTS_SCHEDULE_WIZARD_INFO1') + '</span>';
				desc = '<i class="fa fa-money fa-2x pull-left"></i>' + desc;
				this.setInfo(desc);
				break;
			}
		}
	};

	CODASchedulePaymentsWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				if(this.values[0].paymentProvider_code && this.values[0].paymentProvider_code == 'paypal'){

					var payments_list_panel = new KPanel(this.forms[1]);
					payments_list_panel.hash = '/payments-list-panel';

					var tableContainer = window.document.createElement('div');
					tableContainer.className = 'CODASchedulePaymentListPreview';
					tableContainer.appendChild(window.document.createElement('h2'));

					var table = tableContainer.appendChild(window.document.createElement('table'));
					var table_head = table.appendChild(window.document.createElement('thead'));
					var table_head_cols = table_head.appendChild(window.document.createElement('tr'));

					var table_head_col_payment_nr = window.document.createElement('th');
					table_head_col_payment_nr.className = 'CODASchedulePaymentListPreviewHeaderName';
					cellText = document.createTextNode(gettext('$PAYMENTS_SCHEDULE_TABLE_PAYMENT_NR'));
					table_head_col_payment_nr.appendChild(cellText);
					table_head_cols.appendChild(table_head_col_payment_nr);

					var table_head_col_date = window.document.createElement('th');
					table_head_col_date.className = 'CODASchedulePaymentListPreviewHeaderName';
					cellText = document.createTextNode(gettext('$PAYMENTS_SCHEDULE_TABLE_DATE'));
					table_head_col_date.appendChild(cellText);
					table_head_cols.appendChild(table_head_col_date);

					var table_head_col_value = window.document.createElement('th');
					table_head_col_value.className = 'CODASchedulePaymentListPreviewHeaderName';
					cellText = document.createTextNode(gettext('$PAYMENTS_SCHEDULE_TABLE_VALUE') + ' (' + this.values[0].currency + ')');
					table_head_col_value.appendChild(cellText);
					table_head_cols.appendChild(table_head_col_value);

					var table_body = table.appendChild(window.document.createElement('tbody'));

					var start_date_parsed = Date.parse(this.values[0].first_payment_date);
					var start_date = new Date(start_date_parsed);
					var cellText;
					for (var i = 0; i < this.values[0].nr_of_payments; i++){
						var table_row = table_body.appendChild(window.document.createElement('tr'));
						var table_row_value_payment = window.document.createElement('td');
						cellText = document.createTextNode(innerHTMML = i + 1);
						table_row_value_payment.appendChild(cellText);

						table_row.appendChild(table_row_value_payment);
						var table_row_value_date = window.document.createElement('td');

						var payment_date = new Date();
						payment_date.setDate(start_date.getDate() + (i * this.values[0].nr_days_interval));

						var dd = payment_date.getDate();
						var mm = payment_date.getMonth() + 1;
						var y = payment_date.getFullYear();

						var h = payment_date.getHours();
						var m = payment_date.getMinutes();
						var s = payment_date.getSeconds();

						var payment_date_formatted = y + '-'+ mm + '-'+ dd + ' ' + h + ':' + m + ':' + s;

						cellText = document.createTextNode(payment_date_formatted);
						table_row_value_date.appendChild(cellText);

						table_row.appendChild(table_row_value_date);
						var table_row_value_value = window.document.createElement('td');
						cellText = document.createTextNode(this.values[0].values);
						table_row_value_value.appendChild(cellText);

						table_row.appendChild(table_row_value_value);
					}

					payments_list_panel.content.appendChild(tableContainer);

					this.forms[1].appendChild(payments_list_panel);

					var desc = gettext('$PAYMENTS_SCHEDULE_WIZARD_TITLE2');
					desc = '<b>' + desc + '</b><br><span>' + gettext('$PAYMENTS_SCHEDULE_WIZARD_INFO2') + '</span>';
					desc = '<i class="fa fa-thumbs-up fa-2x pull-left"></i>' + desc;
					this.setInfo(desc);
				}
				break;
			}
		}
	};

	CODASchedulePaymentsWizard.prototype.mergeValues = function() {

		var params = {};
		params[0] = {};
		for (var v  = 0 ; v != this.values.length; v++) {
			if(v == 0 || v==3){
				for (var f in this.values[v]) {
					params[0][f] = this.values[v][f];
				}
			}
		}
		/*TODO
		 * retirar as linhas abaixo
		 */
		//o owner será o user autenticado e o order o que motiva o pagamento. Os valores abaixo são apenas para teste
		params[0]["order_id"] = "/campaigns/5";
		params[0]["owner_id"] = "/accounts";
		params[0]["values"] = JSON.parse('[{"net_value": ' + parseFloat(this.values[0]["values"]) + '}]');
		return params;
	};

	CODASchedulePaymentsWizard.prototype.send = function(params) {
		try {
			if (!!params) {
				this.kinky.post(this, 'rest:' + '/schedulepayments', params[0]);
			}
		}
		catch (e) {

		}
	};

	CODASchedulePaymentsWizard.prototype.onPost = CODASchedulePaymentsWizard.prototype.onCreated = function(data, request) {
		CODAPaymentsShell.payments_list.list.refreshPages();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		this.parent.closeMe();
	};

	CODASchedulePaymentsWizard.prototype.onNoContent = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SCHEDULE_PAYMENTS_FAIL'));
	};

	CODASchedulePaymentsWizard.prototype.onNotFound = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SCHEDULE_PAYMENTS_FAIL'));
	};

	KSystem.included('CODASchedulePaymentsWizard');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

