///////////////////////////////////////////////////////////////////
//FORMS

KSystem.included('CODAPaymentsLocale_Help_pt');



///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$PAYMENTS_SHELL_TITLE', 'PAGAMENTO', 'pt');
settext('$LIST_PAYMENTS_PROPERTIES', 'Pagamentos', 'pt');
settext('$PAYMENTS_MODULE_ERROR_0', '', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PAYMENTS', 'Pagamentos', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PAYMENTS_DESC', 'Acede \u00e0 lista de pagamentos e consulta o seu detalhe.', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES', 'Códigos Promocionais', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES_DESC', 'Gere códigos promocionais para enviar a parceiros e clientes específicos.', 'pt');
///////////////////////////////////////////////////////////////////
//FORMS
settext('$PAYMENTS_LIST_DATE', 'Data', 'pt');
settext('$PAYMENTS_LIST_REFERENCE', 'Refer\u00eancia', 'pt');
settext('$PAYMENTS_LIST_STATUS', 'Estado', 'pt');
settext('$PAYMENTS_LIST_VALUE', 'Valor', 'pt');
settext('$PAYMENTS_LIST_PAYMENTPROVIDER', 'Meio de Pagamento', 'pt');

settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW', 'Geração de Código Promocional', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_TITLE', 'Valor e Quantidade', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_DESC', 'Introduz o desconto que pretendes, representado por um número entre 0 e 1.<br/>Introduz o número de códigos que pretendes gerar (máximo de 9)', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE', 'Desconto associado ao Código Promocional', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE_HELP', '', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT', 'Número de Códigos Promocionais a gerar', 'pt');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT_HELP', '', 'pt');
settext('$ERROR_PAYMENTS_PROMOTIONALCODE_DISCOUNT_COUNT_MAX', 'valor do campo deverá ser entre 1 e 9', 'pt');

///////////////////////////////////////////////////////////////////
//LISTS
settext('$DELETE_PAYMENT_CONFIRM', 'Tens a certeza que queres remover o pagamento?', 'pt');
settext('$DELETE_PAYMENT_CANNOT_BE_DONE', 'O pagamento n\u00e3o pode ser removido pois j\u00e1 foi pago', 'pt');
settext('$PAYMENT_STATUS_PAYED', 'Pago', 'pt');
settext('$PAYMENT_STATUS_SCHEDULED', 'Agendado', 'pt');
settext('$PAYMENT_STATUS_VOIDED', 'Devolvido', 'pt');
settext('$PAYMENT_STATUS_UNAUTHORIZED', 'N\u00e3o Autorizado', 'pt');

settext('$CODA_LIST_EXPORT_BUTTON_CODAPAYMENTSLIST', 'EXPORTAR PAGAMENTOS', 'pt');
settext('$CODA_LIST_PAYMENTS_REPORT', 'Clica no link abaixo para descarregares:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$PAYMENTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'pt');

///////////////////////////////////////////////////////////////////
//WIZARDS
settext('$PAYMENTS_SCHEDULE_NEW', 'Agendar Pagamentos', 'pt');
settext('$SCHEDULE_PAYMENT_PROVIDERS', 'Meio de Pagamento', 'pt');
settext('$SCHEDULE_PAYMENT_CURRENCY', 'Moeda', 'pt');
settext('$SCHEDULE_PAYMENT_VALUE', 'Valor', 'pt');
settext('$SCHEDULE_PAYMENT_NR_OF_PAYMENTS', 'Número de Pagamentos', 'pt');
settext('$SCHEDULE_PAYMENT_NR_DAYS_INTERVAL', 'Periodicidade (dias)', 'pt');
settext('$SCHEDULE_PAYMENT_FIRST_PAYMENT_DATE', 'Data do Primeiro Pagamento', 'pt');
settext('$PAYMENTS_SCHEDULE_TABLE_PAYMENT_NR', 'Nr Pagamento', 'pt');
settext('$PAYMENTS_SCHEDULE_TABLE_DATE', 'Data', 'pt');
settext('$PAYMENTS_SCHEDULE_TABLE_VALUE', 'Valor', 'pt');
settext('$PAYMENTS_SCHEDULE_WIZARD_TITLE1', 'Agendamento de Pagamentos', 'pt');
settext('$PAYMENTS_SCHEDULE_WIZARD_INFO1', 'Dados sobre o meio de pagamento e valores a pagar<br>Escolhe um dos meios de pagamento disponíveis<br>Se houver dados a preencher para o meio de pagamento seleccionado, serão pedidos no próximo passo', 'pt');
settext('$PAYMENTS_SCHEDULE_WIZARD_TITLE2', 'Resumo dos Pagamentos', 'pt');
settext('$PAYMENTS_SCHEDULE_WIZARD_INFO2', 'A lista acima contém os pagamentos a fazer e a data até à qual cada um pode ser feito<br>A data dos pagamentos serve apenas para referência<br>A data exacta de cada um será calculada após confirmação', 'pt');

KSystem.included('CODAPaymentsLocale_pt');
