///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$PAYMENTS_SHELL_TITLE', 'PAGAMENTO', 'br');
settext('$LIST_PAYMENTS_PROPERTIES', 'Pagamentos', 'br');
settext('$PAYMENTS_MODULE_ERROR_0', '', 'br');
settext('$DASHBOARD_BUTTON_LIST_PAYMENTS', 'Pagamentos', 'br');
settext('$DASHBOARD_BUTTON_LIST_PAYMENTS_DESC', 'Acessar \u00e0 lista de pagamentos e consultar o detalhe.', 'br');
settext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES', 'Códigos Promocionais', 'br');
settext('$DASHBOARD_BUTTON_LIST_PROMOTIONALCODES_DESC', 'Gerar códigos promocionais para enviar aos parceiros e clientes específicos.', 'br');
///////////////////////////////////////////////////////////////////
//FORMS
settext('$PAYMENTS_LIST_DATE', 'Data', 'br');
settext('$PAYMENTS_LIST_REFERENCE', 'Refer\u00eancia', 'br');
settext('$PAYMENTS_LIST_STATUS', 'Estado', 'br');
settext('$PAYMENTS_LIST_VALUE', 'Valor', 'br');
settext('$PAYMENTS_LIST_PAYMENTPROVIDER', 'Forma de Pagamento', 'br');

settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW', 'Geração de Código Promocional', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_TITLE', 'Valor e Quantidade', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_NEW_PANEL_DESC', 'Coloque o desconto que desejar, representado por um número entre 0 e 1.<br/>Coloque o número de códigos que deseja gerar (máximo de 9)', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE', 'Desconto associado ao Código Promocional', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_DISCOUNT_PERCENTAGE_HELP', '', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT', 'Número de Códigos Promocionais a gerar', 'br');
settext('$CODA_PAYMENTS_PROMOTIONALCODE_COUNT_HELP', '', 'br');
settext('$ERROR_PAYMENTS_PROMOTIONALCODE_DISCOUNT_COUNT_MAX', 'valor do campo deverá ser entre 1 e 9', 'br');

///////////////////////////////////////////////////////////////////
//LISTS
settext('$DELETE_PAYMENT_CONFIRM', 'Tem certeza que deseja remover o pagamento?', 'br');
settext('$DELETE_PAYMENT_CANNOT_BE_DONE', 'O pagamento n\u00e3o pode ser removido pois j\u00e1 foi pago', 'br');
settext('$PAYMENT_STATUS_PAYED', 'Pago', 'br');
settext('$PAYMENT_STATUS_SCHEDULED', 'Agendado', 'br');
settext('$PAYMENT_STATUS_VOIDED', 'Devolvido', 'br');
settext('$PAYMENT_STATUS_UNAUTHORIZED', 'N\u00e3o Autorizado', 'br');

settext('$CODA_LIST_EXPORT_BUTTON_CODAPAYMENTSLIST', 'EXPORTAR PAGAMENTOS', 'br');
settext('$CODA_LIST_PAYMENTS_REPORT', 'Clique no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$PAYMENTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'br');

///////////////////////////////////////////////////////////////////
//WIZARDS
settext('$PAYMENTS_SCHEDULE_NEW', 'Agendar Pagamentos', 'br');
settext('$SCHEDULE_PAYMENT_PROVIDERS', 'Forma de Pagamento', 'br');
settext('$SCHEDULE_PAYMENT_CURRENCY', 'Moeda', 'br');
settext('$SCHEDULE_PAYMENT_VALUE', 'Valor', 'br');
settext('$SCHEDULE_PAYMENT_NR_OF_PAYMENTS', 'Número de Pagamentos', 'br');
settext('$SCHEDULE_PAYMENT_NR_DAYS_INTERVAL', 'Periodicidade (dias)', 'br');
settext('$SCHEDULE_PAYMENT_FIRST_PAYMENT_DATE', 'Data do Primeiro Pagamento', 'br');
settext('$PAYMENTS_SCHEDULE_TABLE_PAYMENT_NR', 'Nr Pagamento', 'br');
settext('$PAYMENTS_SCHEDULE_TABLE_DATE', 'Data', 'br');
settext('$PAYMENTS_SCHEDULE_TABLE_VALUE', 'Valor', 'br');
settext('$PAYMENTS_SCHEDULE_WIZARD_TITLE1', 'Agendamento de Pagamentos', 'br');
settext('$PAYMENTS_SCHEDULE_WIZARD_INFO1', 'Dados sobre o meio de pagamento e valores a pagar<br>Selecione um dos meios de pagamento disponíveis<br>Se for necessario preencher algum dado para a forma de pagamento selecionado, esses dados serão pedidos no próximo passo', 'br');
settext('$PAYMENTS_SCHEDULE_WIZARD_TITLE2', 'Resumo dos Pagamentos', 'br');
settext('$PAYMENTS_SCHEDULE_WIZARD_INFO2', 'A lista acima contém os pagamentos pendentes e a data limite que cada um pode ser feito<br>A data dos pagamentos serve apenas para referência<br>A data exata de cada um será calculada após confirmação', 'br');

KSystem.included('CODAPaymentsLocale_pt');
///////////////////////////////////////////////////////////////////
//FORMS

KSystem.included('CODAPaymentsLocale_Help_pt');



