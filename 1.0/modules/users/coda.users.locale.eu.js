//MODULO DE Users
settext('$CODA_USERS_SHELL_ADD_USER', '<i class="fa fa-user"></i> New', 'eu');
settext('$USERS_LIST', 'List', 'eu');
settext('$USERS_LIST_ACTION_DESCRIPTION', 'Manage users', 'eu');
settext('$USERS_ADD', 'New user', 'eu');
settext('$USERS_ADD_ACTION_DESCRIPTION', 'Add a new user to your backoffice', 'eu');
settext('$USERS_SHELL_TITLE', 'Users', 'eu');

settext('$EDIT_USER_PROPERTIES', 'User', 'eu');

settext('$NEW_USER_PROPERTIES', 'Add user', 'eu');
settext('$USERS_ITEM_FORM_ID_LABEL', 'Username', 'eu');
settext('$USERS_ITEM_FORM_ID', 'Username', 'eu');
settext('$USERS_ITEM_FORM_ID_HELP', '', 'eu');
settext('$USERS_ITEM_FORM_NAME', 'Name', 'eu');
settext('$USERS_ITEM_FORM_NAME_HELP', '', 'eu');
settext('$USERS_ITEM_FORM_EMAIL', 'Email', 'eu');
settext('$USERS_ITEM_FORM_EMAIL_HELP', '', 'eu');
settext('$USERS_ITEM_FORM_PASS', 'Password', 'eu');
settext('$USERS_ITEM_FORM_PASS_HELP', '', 'eu');
settext('$USERS_ITEM_FORM_PASS_CONFIRM', 'Re-type password', 'eu');
settext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP', '', 'eu');
settext('$USERS_ITEM_FORM_STATE', 'State', 'eu');
settext('$USERS_STATE_ACTIVE_CODE', 'active', 'eu');
settext('$USERS_STATE_ACTIVE_TITLE', 'Active', 'eu');
settext('$USERS_STATE_INACTIVE_CODE', 'inactive', 'eu');
settext('$USERS_STATE_INACTIVE_TITLE', 'Inactive', 'eu');
settext('$USERS_ITEM_FORM_STATE_HELP', '', 'eu');
settext('$USERS_LIST_TITLE', 'Users', 'eu');
settext('$RESOURCE_PERMISSIONS', 'Permissions', 'eu');
settext('$USERS_LIST_RESETPASS_BUTTON', '<i class="fa fa-key"></i>', 'eu');
settext('$USERS_LIST_USER_RESET_USER_PASSWORD', 'Reset Password', 'eu');
settext('$USERS_LIST_CHANGESTATE_TOACTIVE_BUTTON', '<i class="fa fa-circle-o"></i>', 'eu');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE', 'Enable user', 'eu');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK', 'Block user', 'eu');
settext('$USERS_LIST_CHANGESTATE_TOBLOCK_BUTTON', '<i class="fa fa-ban"></i>', 'eu');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1', 'The user is active. Do you want to block him?', 'eu');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2', 'The user is blocked. Do you want to activate him?', 'eu');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3', 'Reset Password?', 'eu');
