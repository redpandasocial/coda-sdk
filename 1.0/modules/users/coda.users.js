KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAUserItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = false;
		this.target.collection = '/users';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KPassword'
], function() {
	CODAUserItemForm.prototype = new CODAForm();
	CODAUserItemForm.prototype.constructor = CODAUserItemForm;

	CODAUserItemForm.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAUserItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

		if (this.canEditAdvanced) {
			this.scopes_p = new CODAUsersManageScopesItemForm(this, this.target);
			this.addPanel(this.scopes_p, gettext('$RESOURCE_PERMISSIONS'), this.nPanels == 0);
		}
	};

	CODAUserItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_USER_PROPERTIES') + ' \u00ab' + this.target.data.id + '\u00bb');
		}

		var userItem = new KPanel(this);
		{
			userItem.hash = '/general';

			if(this.target.href == null){
				var id = new KInput(userItem, gettext('$USERS_ITEM_FORM_ID') + ' *', 'id');
				{
					if(!!this.data && !!this.data && !!this.data.id){
						id.setValue(this.data.id);
					}
					id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
					id.setHelp(gettext('$USERS_ITEM_FORM_ID_HELP'), CODAForm.getHelpOptions());
				}
				userItem.appendChild(id);
			}else{
				var id = new KStatic(userItem, gettext('$USERS_ITEM_FORM_ID_LABEL') + ' *', 'id');
				{
					if (!!this.data && !!this.data && !!this.data.id) {
						id.setValue(this.data.id);
					}
					id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
				userItem.appendChild(id);
			}

			var name = new KInput(userItem, gettext('$USERS_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$USERS_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			userItem.appendChild(name);

			var email = new KInput(userItem, gettext('$USERS_ITEM_FORM_EMAIL') + ' *', 'email');
			{
				if(!!this.data && !!this.data && !!this.data.email){
					email.setValue(this.data.email);
				}
				email.setHelp(gettext('$USERS_ITEM_FORM_EMAIL_HELP'), CODAForm.getHelpOptions());
				email.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				email.addValidator(/(^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9_\.\-]+$)|(^$)/, gettext('$ERROR_MUST_BE_EMAIL'));
			}
			userItem.appendChild(email);

			var password = new KPassword(userItem, gettext('$USERS_ITEM_FORM_PASS') + ' *', 'password');
			{
				password.hash = '/password';
				if (!!this.data && !!this.data && !!this.data.password) {
					password.setValue(this.data.password);
				}
				if (!this.target.href) { 
					password.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
				password.setHelp(gettext('$USERS_ITEM_FORM_PASS_HELP'), CODAForm.getHelpOptions());
			}
			userItem.appendChild(password);

			var confpassword = new KPassword(userItem, gettext('$USERS_ITEM_FORM_PASS_CONFIRM') + ' *', 'confpassword');
			{
				if (!!this.data && !!this.data && !!this.data.confpassword) {
					confpassword.setValue(this.data.confpassword);
				}
				confpassword.validate = function() {
					var pass = this.parent.childWidget('/password');
					var canbenull = !!this.getParent('CODAUserItemForm').target.href;

					if (!canbenull && (!this.getValue(true) || this.getValue(true) == '')) {
						this.addCSSClass('KAbstractInputError');
						return gettext('$ERROR_MUST_NOT_BE_NULL');
					}
					else if (pass.getValue(true) != this.getValue(true)) {
						pass.addCSSClass('KAbstractInputError');
						this.addCSSClass('KAbstractInputError');
						return gettext('$ERROR_USER_PASSWORD_MUST_MATCH');
					}
					else {
						pass.errorArea.innerHTML = '';
						pass.removeCSSClass('KAbstractInputError');

						this.errorArea.innerHTML = '';
						this.removeCSSClass('KAbstractInputError');
					}
					return true;
				};
				confpassword.setHelp(gettext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP'), CODAForm.getHelpOptions());
			}
			userItem.appendChild(confpassword);

			var statusSelector = new KCombo(userItem, gettext('$USERS_ITEM_FORM_STATE') + ' *', 'status');
			{
				statusSelector.addOption(gettext('$USERS_STATE_ACTIVE_CODE'), gettext('$USERS_STATE_ACTIVE_TITLE'), (!!this.data && !!this.data && !!this.data.status && this.data.status == gettext('$USERS_STATE_ACTIVE_CODE')) ? true : false);
				statusSelector.addOption(gettext('$USERS_STATE_INACTIVE_CODE'), gettext('$USERS_STATE_INACTIVE_TITLE'), (!!this.data && !!this.data && !!this.data.status && this.data.status == gettext('$USERS_STATE_INACTIVE_CODE')) ? true : false);
				statusSelector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				statusSelector.setHelp(gettext('$USERS_ITEM_FORM_STATE_HELP'), CODAForm.getHelpOptions());
			}
			userItem.appendChild(statusSelector);

		}

		this.addPanel(userItem, gettext('$FORM_GENERAL_DATA_PANEL'), true);
	};

	CODAUserItemForm.prototype.setDefaults = function(params){
		delete params.data;
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;
		delete params.scopes;

		if (!params.password || params.password == '') {
			delete params.password;
			delete params.confpassword;
		}
	};

	CODAUserItemForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAUserItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAUserItemForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_USERS_NOT_FOUND'));
	};

	CODAUserItemForm.prototype.onPost = CODAUserItemForm.prototype.onCreated = function(data, request){
		this.parent.closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-users'
		});
	};

	CODAUserItemForm.prototype.onPut = function(data, request) {
		if (this.scopes_p !== undefined) {
			var scopes = { scopes : this.scopes_p.childWidget('/scopes').getValue() };
			this.kinky.put(this, 'rest:' + this.target.href + '/scopes', scopes, null, 'onPutScopes');
		}
		else {
			this.enable();
			this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		}
	};

	CODAUserItemForm.prototype.onPutScopes = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAUserItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAUserItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAUsersDashboard(parent) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, [{
			icon : 'css:fa fa-list',
			activate : false,
			title : gettext('$USERS_LIST'),
			description : '<span>' + gettext('$USERS_LIST_ACTION_DESCRIPTION') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-users'
			}]
		}, 
		{
			icon : 'css:fa fa-user',
			activate : false,
			title : gettext('$USERS_ADD'),
			description : '<span>' + gettext('$USERS_ADD_ACTION_DESCRIPTION') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Adicionar',
				link : '#/add'
			}]
		}]);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODAUsersDashboard.prototype = new CODAMenuDashboard();
	CODAUsersDashboard.prototype.constructor = CODAUsersDashboard;

	CODAUsersDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAUsersDashboard');
}, Kinky.CODA_INCLUDER_URL);

function CODAUsersListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox',
	'CODAUserItemForm'
], function() {
	CODAUsersListItem.prototype = new CODAListItem();
	CODAUsersListItem.prototype.constructor = CODAUsersListItem;

	CODAUsersListItem.prototype.remove = function() {
		var widget = this;

		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_USER_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:/users/' + encodeURIComponent(widget.data.id), {}, null);
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAUsersListItem.prototype.onPut = function() {
	};

	CODAUsersListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);

		var resetUserPassBt = new KButton(this, ' ', 'resetuserpass', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAUsersListItem');
			KBreadcrumb.dispatchURL({
				hash : '/reset-pass' + widget.data.href
			})
		});
		{
			resetUserPassBt.setText(gettext('$USERS_LIST_RESETPASS_BUTTON') + ' ' + gettext('$USERS_LIST_USER_RESET_USER_PASSWORD'), true);
			resetUserPassBt.addCSSClass('CODAUsersListItemResetPassButton');
		}
		this.appendChild(resetUserPassBt);

		var changeUserStateBt = new KButton(this, ' ', 'changeuserstate', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAUsersListItem');
			KBreadcrumb.dispatchURL({
				hash : '/change-state' + widget.data.href + (!!widget.data.status ? '/' + widget.data.status : '')
			})
		});
		{
			if (!this.data.status || this.data.status == gettext('$USERS_STATE_INACTIVE_CODE')) {
				changeUserStateBt.setText(gettext('$USERS_LIST_CHANGESTATE_TOACTIVE_BUTTON') + ' ' + (!this.data.status || this.data.status == gettext('$USERS_STATE_INACTIVE_CODE') ? gettext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE') : gettext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK')), true);
			}
			else {
				changeUserStateBt.setText(gettext('$USERS_LIST_CHANGESTATE_TOBLOCK_BUTTON') + ' ' +  (!this.data.status || this.data.status == gettext('$USERS_STATE_INACTIVE_CODE') ? gettext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE') : gettext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK')), true);
			}
			changeUserStateBt.addCSSClass('CODAUsersListItemChangeUserStateButton');
		}
		this.appendChild(changeUserStateBt);

	};

	KSystem.included('CODAUsersListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAUsersList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema);
		this.onechoice = true;
		this.pageSize = 5;
		this.table = {
			"id" : {
				label : 'Nick'
			},
			"name" : {
				label : "Nome"
			}
		};
		this.filterFields = [ 'id' ];
		this.LIST_MARGIN = 285;
	}
}

KSystem.include([
	'CODAList',
	'CODAUsersListItem',
	'KLink'
], function() {
	CODAUsersList.prototype = new CODAList();
	CODAUsersList.prototype.constructor = CODAUsersList;

	CODAUsersList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODAUsersList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAUsersListItem(list);
			listItem.index = index;
			listItem.data = this.elements[index];
			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
			list.appendChild(listItem);
		}
	};

	CODAUsersList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add'
		});
	};

	KSystem.included('CODAUsersList');
}, Kinky.CODA_INCLUDER_URL);

function CODAUsersManageScopesItemForm(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.noscopes = false;
		this.scopes = [];
		this.userScopes = [];
		this.data = data || {};
		this.hash = '/user-scopes';
		this.fieldID = 'scopes';
	}
}

KSystem.include([
	'KPanel',
	'KRadioButton',
	'KDate'
], function() {
	CODAUsersManageScopesItemForm.prototype = new KPanel();
	CODAUsersManageScopesItemForm.prototype.constructor = CODAUsersManageScopesItemForm;

	CODAUsersManageScopesItemForm.prototype.load = function() {
		this.kinky.get(this, 'rest:/scopes', {}, null);

	};

	CODAUsersManageScopesItemForm.prototype.onLoad = function(data, request) {
		this.scopes = data.elements;
		this.kinky.get(this, 'rest:' + this.data.href + '/scopes', {}, null, 'onLoadUserScopes');
	};

	CODAUsersManageScopesItemForm.prototype.onLoadUserScopes = function(data, request) {
		this.userScopes = data.elements;
		this.draw();
	};

	CODAUsersManageScopesItemForm.prototype.onNoContent = function(data, request) {
		this.draw();
	};

	CODAUsersManageScopesItemForm.prototype.draw = function() {
		if(this.scopes.length > 0){

			var existingScopes = new KCheckBox(this, gettext('$KRESOURCE_PERMISSIONS_LABEL'), 'scopes');
			existingScopes.hash = '/scopes';

			for(var index in this.scopes){

				if(this.userScopes.length == 0){
					existingScopes.addOption(this.scopes[index].id, this.scopes[index].name, false);
				}
				else{
					var userScopeExists = false;
					for(var index1 in this.userScopes){
						if (this.userScopes[index1].id == this.scopes[index].id){
							existingScopes.addOption(this.scopes[index].id, this.scopes[index].name, true);
							userScopeExists = true;
							break;
						}
					}
					if(userScopeExists == false){
						existingScopes.addOption(this.scopes[index].id, this.scopes[index].name, false);
					}
				}
			}

			this.appendChild(existingScopes);
		}
		KPanel.prototype.draw.call(this);
	};

	CODAUsersManageScopesItemForm.prototype.getRequest = function() {
		return null;
	};

	KSystem.included('CODAUsersManageScopesItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAUsersShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addCSSClass('CODAShell');
		this.addCSSClass('CODAApplicationsShell');
		this.addLocationListener(CODAUsersShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODAUsersList'
], function() {
	CODAUsersShell.prototype = new CODAGenericShell();
	CODAUsersShell.prototype.constructor = CODAUsersShell;

	CODAUsersShell.prototype.draw = function() {
		var newUser = new KButton(this, gettext('$CODA_USERS_SHELL_ADD_USER'), 'add-user', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/add'
			});
		});
		{
			newUser.addCSSClass('CODAToolbarButton');
			newUser.addCSSClass('CODABlueButton');
		}
		this.addBottomButton(newUser);

		this.dashboard = new CODAUsersDashboard(this);
		this.appendChild(this.dashboard);

		this.setTitle(gettext('$USERS_SHELL_TITLE'));

		CODAGenericShell.prototype.draw.call(this);

		this.addCSSClass('CODAApplicationsShellContent', this.content);
	};

	CODAUsersShell.sendRequest = function() {
	};

	CODAUsersShell.prototype.listUsers = function(href, minimized) {
		CODAUsersShell.users_list = new CODAUsersList(this, {
			links : {
				feed : { href :  '/users' }
			}
		});
		CODAUsersShell.users_list.setTitle(gettext('$USERS_LIST_TITLE'));
		this.makeDialog(CODAUsersShell.users_list, { minimized : false, modal : true });
	};

	CODAUsersShell.prototype.editUser = function(href, minimized) {
		var configForm = new CODAUserItemForm(this, href, null, null);
		{
			configForm.hash = '/users/advanced';
			configForm.setTitle(gettext('$EDIT_USER_PROPERTIES'));
		}
		var dialog = this.makeDialog(configForm, { minimized : minimized });
	};

	CODAUsersShell.prototype.addUser = function() {
		var configForm = new CODAUserItemForm(this, null, null, null);
		{
			configForm.hash = '/users/advanced';
			configForm.setTitle(gettext('$NEW_USER_PROPERTIES'));
		}
		var dialog = this.makeDialog(configForm, {minimized : false, modal : true });
	};

	CODAUsersShell.prototype.resetUserPass = function(href) {
		//validar se user tem email e se n tiver gerar msg de erro
		var self = this;
		KConfirmDialog.confirm({
			question : confirmQuestion,
			callback : function() {
				self.kinky.post(self, KSystem.urlify('rest:' + href + '/resetuserpassword'), {}, null, 'passwordResetResult');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAUsersShell.prototype.changeUserState = function(href, status) {
		var statusToUpdate = null;

		if(!!status){
			if (status == gettext('$USERS_STATE_ACTIVE_CODE')){
				confirmQuestion = gettext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1');
				statusToUpdate = gettext('$USERS_STATE_INACTIVE_CODE');
			}
			else if (status == gettext('$USERS_STATE_INACTIVE_CODE')){
				confirmQuestion = gettext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2');
				statusToUpdate = gettext('$USERS_STATE_ACTIVE_CODE');
			}
		}
		else{
			confirmQuestion = gettext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3');
			statusToUpdate = gettext('$USERS_STATE_ACTIVE_CODE');
		}

		var self = this;
		KConfirmDialog.confirm({
			question : confirmQuestion,
			callback : function() {
				self.kinky.put(self, KSystem.urlify('rest:' + href), {status : statusToUpdate}, { 'X-Fields-Nillable' : 'false' }, 'changeUserStateResult');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAUsersShell.prototype.changeUserStateResult = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$USERS_STATE_CHANGED_SUCCESS'),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
		this.dashboard.usersList.list.refreshPages();
	};

	CODAUsersShell.prototype.passwordResetResult = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$USERS_PASS_CHANGED_SUCCESS'),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});	
		this.dashboard.usersList.list.refreshPages();
	};

	CODAUsersShell.prototype.onError = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$USERS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAUsersShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]) {
			case 'list-users': {
				Kinky.getDefaultShell().listUsers();
				break;
			}
			case 'edit': {
				Kinky.getDefaultShell().editUser('/' + parts[2] + '/' + parts[3], true);
				break;
			}
			case 'edit-full': {
				Kinky.getDefaultShell().editUser('/' + parts[2] + '/' + parts[3], false);
				break;
			}
			case 'add': {
				Kinky.getDefaultShell().addUser();
				break;
			}
			case 'change-state': {
				Kinky.getDefaultShell().changeUserState('/' + parts[2] + '/' + parts[3], parts[4]);
				break;
			}
			case 'reset-pass': {
				Kinky.getDefaultShell().changeUserState('/' + parts[2] + '/' + parts[3]);
				break;
			}
		}
		KBreadcrumb.dispatchURL({
			hash : ''
		});

	};

	KSystem.included('CODAUsersShell');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

