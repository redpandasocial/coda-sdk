///////////////////////////////////////////////////////////////////
// FORMS
settext('$USERS_ITEM_FORM_NAME_HELP', 'Nome que irá representar o utilizador nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'pt');
settext('$USERS_ITEM_FORM_EMAIL_HELP', 'Endereço de e-mail no qual o utilizador deseja receber as comunicações da plataforma. Caso o alteres presente, o processo de validação de endereço de e-mail será desencadeado.', 'pt');
settext('$USERS_ITEM_FORM_ID_HELP', 'Nome a ser utilizado como identificador textual para o utilizador. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema.', 'pt');
settext('$USERS_ITEM_FORM_STATE_HELP', 'Estado em que se encontra a conta de utilizador. Caso seja diferente de <i>Activa</i> significa que foi executada uma acção de administração que poderá impedir este utilizador de aceder ao sistema.', 'pt');
settext('$USERS_ITEM_FORM_PASS_HELP', 'Palavra chave a ser utilizada nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema. ', 'pt');
settext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP', 'Deverás escrever novamente a palavra-chave que introduziu no campo acima.', 'pt');

settext('$CODA_USERS_MUG_HELP', 'Imagem que te irá representar nesta plataforma, sempre que uma imagem representativa do utilizador for necessária.', 'pt');
settext('$CODA_USERS_TITLE_HELP', 'Nome que te irá representar nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'pt');
settext('$CODA_USERS_EMAIL_HELP', 'Endereço de e-mail no qual desejas receber as comunicações da plataforma. Caso o alteres, o processo de validação de endereço de e-mail será desencadeado.', 'pt');
settext('$CODA_USERS_ADDRESS_HELP', 'Morada de faturação que desejas que figure nos recibos e faturas emitidos pela plataforma.', 'pt');
settext('$CODA_USERS_ZIPCODE_HELP', 'Código postal associado à <i>Morada de Faturação</i>.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAUsersLocale_Help_pt');

settext('$USERS_SHELL_TITLE', 'Utilizadores', 'pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$NEW_USER_PROPERTIES', 'NOVO UTILIZADOR', 'pt');
settext('$EDIT_USER_PROPERTIES', 'Utilizador ', 'pt');

settext('$USERS_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$USERS_ITEM_FORM_EMAIL', 'Email', 'pt');
settext('$USERS_ITEM_FORM_ID', 'Username', 'pt');
settext('$USERS_ITEM_FORM_STATE', 'Estado', 'pt');
settext('$USERS_ITEM_FORM_ID_LABEL', 'Username', 'pt');
settext('$USERS_ITEM_FORM_PASS', 'Password', 'pt');
settext('$USERS_ITEM_FORM_PASS_CONFIRM', 'Confirmar Password', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$USERS_LIST_PANEL', 'UTILIZADORES', 'pt');
settext('$USERS_LIST_USER_RESET_USER_PASSWORD', 'Reset Password', 'pt');
settext('$USERS_LIST_USER_MANAGE_USER_SCOPES', 'Gerir Scopes', 'pt');
settext('$USERS_LIST_USER_MANAGE_USER_SCOPES_SUCCESS', 'Altera\u00e7\u00e3o de scopes do utilizador feita com sucesso', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE', 'Alterar Estado do Utilizador', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK', 'Bloquear Utilizador', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE', 'Activar Utilizador', 'pt');
settext('$USERS_LIST_RESETPASS_BUTTON', '<i class="fa fa-key"></i>', 'pt');
settext('$USERS_LIST_CHANGESTATE_TOBLOCK_BUTTON', '<i class="fa fa-ban"></i>', 'pt');
settext('$USERS_LIST_CHANGESTATE_TOACTIVE_BUTTON', '<i class="fa fa-ok"></i>', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1', 'O utilizador est\u00e1 com o estado ACTIVO, desejas bloque\u00e1-lo?', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2', 'O utilizador est\u00e1 com o estado BLOQUEADO, desejas activ\u00e1-lo?', 'pt');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3', 'O utilizador n\u00e3o tem estado associado. Desejas activ\u00e1-lo?', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
settext('$CODA_USERS_SHELL_LIST', 'Utilizadores', 'pt');
settext('$CODA_USERS_SHELL_ADD_USER', '<i class="fa fa-user"></i> NOVO', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CODES
settext('$USERS_STATE_ACTIVE_TITLE', 'Activo', 'pt');
settext('$USERS_STATE_ACTIVE_CODE', 'active', 'pt');
settext('$USERS_STATE_INACTIVE_TITLE', 'Bloqueado', 'pt');
settext('$USERS_STATE_INACTIVE_CODE', 'inactive', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$USERS_LIST', 'Listagem', 'pt');
settext('$USERS_LIST_ACTION_DESCRIPTION', 'Consulta a listagem de utilizadores e altera definições, password e estados.', 'pt');
settext('$USERS_ADD', 'Novo utilizador', 'pt');
settext('$USERS_ADD_ACTION_DESCRIPTION', 'Adiciona um novo acesso à tua aplicação.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$USERS_PASS_CHANGED_SUCCESS', 'Reset \u00e0 password feito com sucesso', 'pt');
settext('$USERS_STATE_CHANGED_SUCCESS', 'Estado do utilizador alterado com sucesso', 'pt');
settext('$CODA_EXCEPTION_DUPLICATE_ENTRY', 'O username e / ou email inseridos j\u00e1 existem para outro utilizador', 'pt');
settext('$CODA_EXCEPTION_USERS_NOT_FOUND', 'O Utilizador n\u00e3o foi encontrado', 'pt');
settext('$CODA_DELETE_USER_CONFIRM', 'Tens a certeza que queres remover o utilizador?', 'pt');
settext('$CODA_USER_DELETED_SUCCESS', 'Utilizador removido com sucesso.', 'pt');
settext('$CODA_USER_DELETED_ERROR', 'Ocorreu um erro na remo\u00e7\u00e3o do utilizador', 'pt');
settext('The given user was not found', '<span style="color: rgb(248, 82, 1);">O utilizador n\u00e3o foi encontrado<span>', 'pt');

settext('$ERROR_USER_PASSWORD_MUST_MATCH', 'As passwords têm que coincidir.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$USERS_MODULE_ERROR_0', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAUsersLocale_pt');

