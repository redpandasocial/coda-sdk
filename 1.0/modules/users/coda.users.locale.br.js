settext('$USERS_SHELL_TITLE', 'Usuários', 'br');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$NEW_USER_PROPERTIES', 'NOVO USUÁRIO', 'br');
settext('$EDIT_USER_PROPERTIES', 'Usuário ', 'br');

settext('$USERS_ITEM_FORM_NAME', 'Nome', 'br');
settext('$USERS_ITEM_FORM_EMAIL', 'Email', 'br');
settext('$USERS_ITEM_FORM_ID', 'Username', 'br');
settext('$USERS_ITEM_FORM_STATE', 'Estado', 'br');
settext('$USERS_ITEM_FORM_ID_LABEL', 'Username', 'br');
settext('$USERS_ITEM_FORM_PASS', 'Senha', 'br');
settext('$USERS_ITEM_FORM_PASS_CONFIRM', 'Confirmar Senha', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$USERS_LIST_PANEL', 'usuários', 'br');
settext('$USERS_LIST_USER_RESET_USER_PASSWORD', 'Nova Senha', 'br');
settext('$USERS_LIST_USER_MANAGE_USER_SCOPES', 'Gerir Scopes', 'br');
settext('$USERS_LIST_USER_MANAGE_USER_SCOPES_SUCCESS', 'Altera\u00e7\u00e3o de scopes do usuário feita com sucesso', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE', 'Alterar Estado do Usuário', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK', 'Bloquear Usuário', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE', 'Ativar Usuário', 'br');
settext('$USERS_LIST_RESETPASS_BUTTON', '<i class="fa fa-key"></i>', 'br');
settext('$USERS_LIST_CHANGESTATE_TOBLOCK_BUTTON', '<i class="fa fa-ban"></i>', 'br');
settext('$USERS_LIST_CHANGESTATE_TOACTIVE_BUTTON', '<i class="fa fa-ok"></i>', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1', 'O usuário est\u00e1 com o estado ATIVO, deseja bloque\u00e1-lo?', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2', 'O usuário est\u00e1 com o estado BLOQUEADO, deseja ativ\u00e1-lo?', 'br');
settext('$USERS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3', 'O usuário n\u00e3o tem estado associado. Deseja ativ\u00e1-lo?', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
settext('$CODA_USERS_SHELL_LIST', 'usuários', 'br');
settext('$CODA_USERS_SHELL_ADD_USER', '<i class="fa fa-user"></i> NOVO', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CODES
settext('$USERS_STATE_ACTIVE_TITLE', 'Activo', 'br');
settext('$USERS_STATE_ACTIVE_CODE', 'active', 'br');
settext('$USERS_STATE_INACTIVE_TITLE', 'Bloqueado', 'br');
settext('$USERS_STATE_INACTIVE_CODE', 'inactive', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$USERS_LIST', 'Listagem', 'br');
settext('$USERS_LIST_ACTION_DESCRIPTION', 'Consulta a listagem de usuários e altera definições, password e estados.', 'br');
settext('$USERS_ADD', 'Novo usuário', 'br');
settext('$USERS_ADD_ACTION_DESCRIPTION', 'Adiciona um novo acesso à tua aplicação.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$USERS_PASS_CHANGED_SUCCESS', 'Reset \u00e0 password feito com sucesso', 'br');
settext('$USERS_STATE_CHANGED_SUCCESS', 'Estado do usuário alterado com sucesso', 'br');
settext('$CODA_EXCEPTION_DUPLICATE_ENTRY', 'O username e / ou email inseridos j\u00e1 existem para outro usuário', 'br');
settext('$CODA_EXCEPTION_USERS_NOT_FOUND', 'O usuário n\u00e3o foi encontrado', 'br');
settext('$CODA_DELETE_USER_CONFIRM', 'Tem certeza que deseja remover o usuário?', 'br');
settext('$CODA_USER_DELETED_SUCCESS', 'usuário removido com sucesso.', 'br');
settext('$CODA_USER_DELETED_ERROR', 'Ocorreu um erro na remo\u00e7\u00e3o do usuário', 'br');
settext('The given user was not found', '<span style="color: rgb(248, 82, 1);">O usuário n\u00e3o foi encontrado<span>', 'br');

settext('$ERROR_USER_PASSWORD_MUST_MATCH', 'As senhas têm que coincidir.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$USERS_MODULE_ERROR_0', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAUsersLocale_pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$USERS_ITEM_FORM_NAME_HELP', 'Nome que representará o usuário nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'br');
settext('$USERS_ITEM_FORM_EMAIL_HELP', 'Endereço de e-mail no qual o usuário deseja receber as comunicações da plataforma. Caso altere, o processo de validação de endereço de e-mail será desencadeado.', 'br');
settext('$USERS_ITEM_FORM_ID_HELP', 'Nome a ser utilizado como identificador textual para o usuário. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para acessar ao sistema.', 'br');
settext('$USERS_ITEM_FORM_STATE_HELP', 'Estado em que se encontra a conta de usuário. Caso seja diferente de <i>Ativa</i> significa que foi executada uma acção de administração que poderá impedir este usuário de acessar ao sistema.', 'br');
settext('$USERS_ITEM_FORM_PASS_HELP', 'Palavra chave a ser utilizada nas credenciais (nome, palavra-chave) necessárias para acessar ao sistema. ', 'br');
settext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP', 'Deverás escrever novamente a palavra-chave que introduziu no campo acima.', 'br');

settext('$CODA_USERS_MUG_HELP', 'Imagem que te irá representar nesta plataforma, sempre que uma imagem representativa do usuário for necessária.', 'br');
settext('$CODA_USERS_TITLE_HELP', 'Nome que te irá representar nesta plataforma, sempre que um nome por extenso (e não o username) for necessário.', 'br');
settext('$CODA_USERS_EMAIL_HELP', 'Endereço de e-mail no qual desejas receber as comunicações da plataforma. Caso o alteres, o processo de validação de endereço de e-mail será desencadeado.', 'br');
settext('$CODA_USERS_ADDRESS_HELP', 'Morada de faturação que desejas que figure nos recibos e faturas emitidos pela plataforma.', 'br');
settext('$CODA_USERS_ZIPCODE_HELP', 'Código postal associado à <i>Morada de Faturação</i>.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAUsersLocale_Help_pt');

