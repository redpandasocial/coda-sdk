///////////////////////////////////////////////////////////////////
//SHELL
settext('$CODA_LIST_USERS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$CODA_LIST_APPLICATIONS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$CODA_LIST_SUBSCRIPTIONS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$CODA_LIST_SUBSCRIPTIONS_FULL_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$REPORTS_SHELL_TITLE', 'Listagens', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_USERS_REPORTS', 'Dados de Utilizadores', 'br');
settext('$DASHBOARD_BUTTON_USERS_REPORTS_DESC', 'Obtém a lista dos utilizadores para consultares os dados de quem está registado no RedPanda', 'br');
settext('$DASHBOARD_BUTTON_USERS_REPORTS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS', 'Dados de Aplicações', 'br');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_DESC', 'Obtém a lista das aplicações dos utilizadores e o detalhe de cada', 'br');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS', 'Dados de Subscrições', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_DESC', 'Obtém a lista das subscrições que foram feitas e consulta os seus dados', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS', 'Dados de Subscrições (dados de Facebook atualizados)', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_DESC', 'Obtém a lista das subscrições que foram feitas e consulta os seus dados<br> <b>NOTA:</b> Este report faz pedidos ao Facebook para mostrar informação atualizada sobre as páginas de cada subscrição e por isso pode ser demorado. O report anterior contém os dados de Facebook que foram obtidos no momento da subscrição', 'br');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS', 'Gráficos de Utilizadores, Aplicações e Subscrições', 'br');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS_DESC', 'Acede a informação estatística sobre Utilizadores, Aplicações e Subscrições', 'br');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS_LIST', 'LISTA', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$USERS_STATISTICS_AGE_INTERVAL1', '13-17', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL2', '18-24', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL3', '25-34', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL4', '35-44', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL5', '45-54', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL6', '54-64', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL7', '65+', 'br');
settext('$USERS_STATISTICS_AGE_INTERVAL8', 'não definido', 'br');

settext('$USERS_STATISTICS_AGE_CHART_LABEL_AGE', 'Idade', 'br');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_MALE', 'Masculino', 'br');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_FEMALE', 'Feminino', 'br');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE', 'não disponível', 'br');

settext('$USERS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS', 'Idade', 'br');
settext('$USERS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS', 'Não há participantes', 'br');
settext('$USERS_STATISTICS_GENDER_CHART_LABEL_GENDER', 'Género', 'br');
settext('$USERS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS', 'Idade / Género', 'br');

settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_LOCATION', 'Localização', 'br');
settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS', 'Participantes', 'br');
settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_SPARTICIPANTS', ' participantes', 'br');
settext('$USERS_STATISTICS_LOCATION_CHART_TITLE', 'Top 5 de localizações dos participantes', 'br');

settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK', 'Rede Social', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_YES', 'Sim', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_NO', 'Não', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FACEBOOK', 'Facebook', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FLICKR', 'Flickr', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_GOOGLE', 'Google', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_INSTAGRAM', 'Instagram', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_LINKEDIN', 'LinkedIn', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TUMBLR', 'Tumblr', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TWITTER', 'Twitter', 'br');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_YAHOO', 'Yahoo', 'br');

settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_CONTENT_OFFER', 'Angariador de Leads', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZANSWERPAGE_CONTEST', 'Quiz', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PHOTOVIDEO_CONTEST', 'Concurso de Fotografias', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FRIENDS_DISCCOUNT', 'Desconta com Amigos', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZ_CONTEST', 'Concurso de perguntas', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VIDEO_CONTEST', 'Concurso de Vídeos', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_SINGLE', 'Loja de Vendas Flash', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_PROMOTIONS', 'Loja de Promoções', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_STORE', 'Loja Social', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_INSTAGRAM_FEED', 'Feed de Instagram', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FLICKR_FEED', 'Feed de Flickr', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_TWITTER_FEED', 'Feed de Twitter', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VOUCHER_OFFER', 'Voucher', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_DIRECT_OFFER', 'Ganha Direto', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_EMAIL_SIGNUP', 'Registo Email', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FAN_VOTE', 'Sondagem', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PROFILE_QUIZ', 'Teste de Personalidade', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_STOREFRONT_PROMOTIONS', 'Catálogo', 'br');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'br');

settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_APP_STATE', 'Estado de Aplicação', 'br');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CREATED', 'Criada', 'br');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ACTIVE', 'Activa', 'br');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_NO_USER_CONTENT', 'Sem Conteúdos', 'br');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CANCELED', 'Cancelada', 'br');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ARCHIVED', 'Arquivada', 'br');

settext('$SUBSCRIPTIONS_STATISTICS_PROD_NAME_CHART_LABEL_PROD_NAME', 'Tipo de Subscrição', 'br');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAReportsLocale_br');
///////////////////////////////////////////////////////////////////
//FORMS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
///////////////////////////////////////////////////////////////////

KSystem.included('CODAReportsLocale_Help_br');
