///////////////////////////////////////////////////////////////////
//FORMS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
///////////////////////////////////////////////////////////////////

KSystem.included('CODAReportsLocale_Help_pt');
///////////////////////////////////////////////////////////////////
//SHELL
settext('$CODA_LIST_USERS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$CODA_LIST_APPLICATIONS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$CODA_LIST_SUBSCRIPTIONS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$CODA_LIST_SUBSCRIPTIONS_FULL_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$REPORTS_SHELL_TITLE', 'Listagens', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_USERS_REPORTS', 'Dados de Utilizadores', 'pt');
settext('$DASHBOARD_BUTTON_USERS_REPORTS_DESC', 'Obtém a lista dos utilizadores para consultares os dados de quem está registado no RedPanda', 'pt');
settext('$DASHBOARD_BUTTON_USERS_REPORTS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS', 'Dados de Aplicações', 'pt');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_DESC', 'Obtém a lista das aplicações dos utilizadores e o detalhe de cada', 'pt');
settext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS', 'Dados de Subscrições', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_DESC', 'Obtém a lista das subscrições que foram feitas e consulta os seus dados', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS', 'Dados de Subscrições (dados de Facebook atualizados)', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_DESC', 'Obtém a lista das subscrições que foram feitas e consulta os seus dados<br> <b>NOTA:</b> Este report faz pedidos ao Facebook para mostrar informação atualizada sobre as páginas de cada subscrição e por isso pode ser demorado. O report anterior contém os dados de Facebook que foram obtidos no momento da subscrição', 'pt');
settext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS', 'Gráficos de Utilizadores, Aplicações e Subscrições', 'pt');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS_DESC', 'Acede a informação estatística sobre Utilizadores, Aplicações e Subscrições', 'pt');
settext('$DASHBOARD_BUTTON_CHARTS_REPORTS_LIST', 'LISTA', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$USERS_STATISTICS_AGE_INTERVAL1', '13-17', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL2', '18-24', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL3', '25-34', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL4', '35-44', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL5', '45-54', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL6', '54-64', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL7', '65+', 'pt');
settext('$USERS_STATISTICS_AGE_INTERVAL8', 'não definido', 'pt');

settext('$USERS_STATISTICS_AGE_CHART_LABEL_AGE', 'Idade', 'pt');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_MALE', 'Masculino', 'pt');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_FEMALE', 'Feminino', 'pt');
settext('$USERS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE', 'não disponível', 'pt');

settext('$USERS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS', 'Idade', 'pt');
settext('$USERS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS', 'Não há participantes', 'pt');
settext('$USERS_STATISTICS_GENDER_CHART_LABEL_GENDER', 'Género', 'pt');
settext('$USERS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS', 'Idade / Género', 'pt');

settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_LOCATION', 'Localização', 'pt');
settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS', 'Participantes', 'pt');
settext('$USERS_STATISTICS_LOCATION_CHART_LABEL_SPARTICIPANTS', ' participantes', 'pt');
settext('$USERS_STATISTICS_LOCATION_CHART_TITLE', 'Top 5 de localizações dos participantes', 'pt');

settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_ABSOLUTE', 'Rede Social (valor absoluto)', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_PERCENTAGE', 'Rede Social (percentagem)', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_YES', 'Sim', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_NO', 'Não', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FACEBOOK', 'Facebook', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FLICKR', 'Flickr', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_GOOGLE', 'Google', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_INSTAGRAM', 'Instagram', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_LINKEDIN', 'LinkedIn', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TUMBLR', 'Tumblr', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TWITTER', 'Twitter', 'pt');
settext('$USERS_STATISTICS_NETWORK_CHART_LABEL_YAHOO', 'Yahoo', 'pt');

settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_CONTENT_OFFER', 'Angariador de Leads', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZANSWERPAGE_CONTEST', 'Quiz', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PHOTOVIDEO_CONTEST', 'Concurso de Fotografias', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FRIENDS_DISCCOUNT', 'Desconta com Amigos', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZ_CONTEST', 'Concurso de perguntas', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VIDEO_CONTEST', 'Concurso de Vídeos', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_SINGLE', 'Loja de Vendas Flash', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_PROMOTIONS', 'Loja de Promoções', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_STORE', 'Loja Social', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_INSTAGRAM_FEED', 'Feed de Instagram', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FLICKR_FEED', 'Feed de Flickr', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_TWITTER_FEED', 'Feed de Twitter', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VOUCHER_OFFER', 'Voucher', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_DIRECT_OFFER', 'Ganha Direto', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_EMAIL_SIGNUP', 'Registo Email', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FAN_VOTE', 'Sondagem', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PROFILE_QUIZ', 'Teste de Personalidade', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_STOREFRONT_PROMOTIONS', 'Catálogo', 'pt');
settext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE', 'Tipo de Aplicação', 'pt');

settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_APP_STATE', 'Estado de Aplicação', 'pt');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CREATED', 'Criada', 'pt');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ACTIVE', 'Activa', 'pt');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_NO_USER_CONTENT', 'Sem Conteúdos', 'pt');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CANCELED', 'Cancelada', 'pt');
settext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ARCHIVED', 'Arquivada', 'pt');

settext('$SUBSCRIPTIONS_STATISTICS_PROD_NAME_CHART_LABEL_PROD_NAME', 'Tipo de Subscrição', 'pt');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAReportsLocale_pt');
