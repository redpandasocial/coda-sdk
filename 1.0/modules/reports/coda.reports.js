KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAChartsReport(parent, data) {
	if (parent != null) {
		CODAGraphForm.call(this, parent);
		this.data = data;
		this.filterFields = null;
		this.filterValue = [];
		this.headers = {};
		this.helpURL = CODAGenericShell.generateHelpURL(this);
	}
}

KSystem.include([
	'CODAGraphForm'
], function() {
	CODAChartsReport.prototype = new CODAGraphForm();
	CODAChartsReport.prototype.constructor = CODAChartsReport;

	CODAChartsReport.prototype.visible = function() {
		var display = this.display;
		CODAGraphForm.prototype.visible.call(this);

		if (!!display) {
			return;
		}

		var self = this;
		google.load('visualization', '1.0', {'packages':['corechart'], callback : function() { 
				self.loadCharts(); 
				self.drawCharts(); 
			} 
		});
	};

	CODAChartsReport.prototype.loadCharts = function() {		
		
		this.graphs = {};

		CODAReportsShell.cur_width = !!CODAReportsShell.cur_width ? CODAReportsShell.cur_width : (CODAReportsShell.cur_width = this.getParent('KFloatable').getWidth() - 120);

		this.processUsersData();
		this.processApplicationsData();
		this.processSubscriptionsData();
			
	};

	CODAChartsReport.prototype.drawCharts = function() {

		for (var gid in this.graphs) {
			var chart_div = window.document.createElement('div');
			chart_div.style.display = 'inline-block';
			chart_div.id = this.id + '_' + gid;
			this.content.appendChild(chart_div);

			var chart = null;

			switch (this.graphs[gid].type){
				case 'LineChart' : 
				chart = new google.visualization.LineChart(chart_div);
				break;
				case 'PieChart' :
				chart = new google.visualization.PieChart(chart_div);
				break;
				case 'ColumnChart' :
				chart = new google.visualization.ColumnChart(chart_div);
				break;
			}

			if (chart != null){
				chart.draw(this.graphs[gid].data, this.graphs[gid].options);
			}

		}

	};

	CODAChartsReport.prototype.processUsersData = function() {
		
		var age_interval1 = 0;
		var age_interval1_label = gettext('$USERS_STATISTICS_AGE_INTERVAL1');
		var age_interval1_male = 0;
		var age_interval1_female = 0;
		var age_interval1_unknown = 0;
		var age_interval2 = 0;
		var age_interval2_label = gettext('$USERS_STATISTICS_AGE_INTERVAL2');
		var age_interval2_male = 0;
		var age_interval2_female = 0;
		var age_interval2_unknown = 0;
		var age_interval3 = 0;
		var age_interval3_label = gettext('$USERS_STATISTICS_AGE_INTERVAL3');
		var age_interval3_male = 0;
		var age_interval3_female = 0;
		var age_interval3_unknown = 0;
		var age_interval4 = 0;
		var age_interval4_label = gettext('$USERS_STATISTICS_AGE_INTERVAL4');
		var age_interval4_male = 0;
		var age_interval4_female = 0;
		var age_interval4_unknown = 0;
		var age_interval5 = 0;
		var age_interval5_label = gettext('$USERS_STATISTICS_AGE_INTERVAL5');
		var age_interval5_male = 0;
		var age_interval5_female = 0;
		var age_interval5_unknown = 0;
		var age_interval6 = 0;
		var age_interval6_label = gettext('$USERS_STATISTICS_AGE_INTERVAL6');
		var age_interval6_male = 0;
		var age_interval6_female = 0;
		var age_interval6_unknown = 0;
		var age_interval7 = 0;
		var age_interval7_label = gettext('$USERS_STATISTICS_AGE_INTERVAL7');
		var age_interval7_male = 0;
		var age_interval7_female = 0;
		var age_interval7_unknown = 0;
		var age_interval8 = 0;
		var age_interval8_label = gettext('$USERS_STATISTICS_AGE_INTERVAL8');
		var age_interval8_male = 0;
		var age_interval8_female = 0;
		var gender_male = 0;
		var gender_female = 0;
		var gender_notavailable = 0;

		var facebook_account_yes = 0;
		var facebook_account_no = 0;
		var flickr_account_yes = 0;
		var flickr_account_no = 0;
		var google_account_yes = 0;
		var google_account_no = 0;
		var instagram_account_yes = 0;
		var instagram_account_no = 0;
		var linkedin_account_yes = 0;
		var linkedin_account_no = 0;
		var tumblr_account_yes = 0;
		var tumblr_account_no = 0;
		var twitter_account_yes = 0;
		var twitter_account_no = 0;
		var yahoo_account_yes = 0;
		var yahoo_account_no = 0;

		var all_locations = new Array();

		if(!!this.data.users){

			var users = this.data.users;

			for (var i = 0; i < users.length; i++){

				var gender = null;
				if(!!users[i].facebook_account_gender){
					if (users[i].facebook_account_gender == "male"){
						gender_male++;
					}else if (users[i].facebook_account_gender == "female"){
						gender_female++;
					}
					gender = users[i].facebook_account_gender;
				}else{
					gender_notavailable++;
					gender = "gender_notavailable";
				}

				if(!!users[i].facebook_account_birthday && users[i].facebook_account_birthday != ""){

					var participant_birthday = new Date(users[i].facebook_account_birthday);
					var ageDifMs = Date.now() - participant_birthday.getTime();
					var ageDate = new Date(ageDifMs);
					var age = Math.abs(ageDate.getUTCFullYear() - 1970);

					if(age <= 17){
						age_interval1++;
						if (gender == "male"){
							age_interval1_male++;
						}else if(gender == "female"){
							age_interval1_female++;
						}else{
							age_interval1_unknown++;
						}	
					}else if(age >= 18 && age <= 24){
						age_interval2++;
						if (gender == "male"){
							age_interval2_male++;
						}else if(gender == "female"){
							age_interval2_female++;
						}else{
							age_interval2_unknown++;
						}							
					}else if(age >= 25 && age <= 34){
						age_interval3++;
						if (gender == "male"){
							age_interval3_male++;
						}else if(gender == "female"){
							age_interval3_female++;
						}else{
							age_interval3_unknown++;
						}
					}else if(age >= 35 && age <= 44){
						age_interval4++;
						if (gender == "male"){
							age_interval4_male++;
						}else if(gender == "female"){
							age_interval4_female++;
						}else{
							age_interval4_unknown++;
						}
					}else if(age >= 45 && age <= 54){
						age_interval5++;
						if (gender == "male"){
							age_interval5_male++;
						}else if(gender == "female"){
							age_interval5_female++;
						}else{
							age_interval5_unknown++;
						}
					}else if(age >= 24 && age <= 64){
						age_interval6++;
						if (gender == "male"){
							age_interval6_male++;
						}else if(gender == "female"){
							age_interval6_female++;
						}else{
							age_interval6_unknown++;
						}
					}else if(age >= 65){
						age_interval7++;
						if (gender == "male"){
							age_interval7_male++;
						}else if(gender == "female"){
							age_interval7_female++;
						}else{
							age_interval7_unknown++;
						}
					}

				}else{

					age_interval8++;

				}

				if(!!users[i].facebook_account_location_id && !!users[i].facebook_account_location_name && users[i].facebook_account_location_id != "" && users[i].facebook_account_location_name != ""){

					if(users[i].facebook_account_location_name.indexOf(",") != -1){
						users[i].facebook_account_location_name = users[i].facebook_account_location_name.replace(/ /g,'').split(",");
						users[i].facebook_account_location_name = users[i].facebook_account_location_name[users[i].facebook_account_location_name.length - 2].toString();
					}

					if(all_locations.length > 0){
						var found = false;
						for (var j = 0; j < all_locations.length; j++){

							if(users[i].facebook_account_location_name.toLowerCase() == all_locations[j][0]){
								found = true;
								all_locations[j][1] = all_locations[j][1] + 1; 
								break;
							}			

						}

						if (found == false){
							all_locations[j] = new Array();
							all_locations[j][0] = users[i].facebook_account_location_name.toLowerCase();
							all_locations[j][1] = 1;	
						}

					}else{
						all_locations[0] = new Array();
						all_locations[0][0] = users[i].facebook_account_location_name.toLowerCase();
						all_locations[0][1] = 1;
					}

				}

				if(!!users[i].facebook_account){
					if(users[i].facebook_account == "yes"){
						facebook_account_yes++;
					}else if(users[i].facebook_account == "no"){
						facebook_account_no++;
					}
				}
				if(!!users[i].flickr_account){
					if(users[i].flickr_account == "yes"){
						flickr_account_yes++;
					}else if(users[i].flickr_account == "no"){
						flickr_account_no++;
					}
				}
				if(!!users[i].google_account){
					if(users[i].google_account == "yes"){
						google_account_yes++;
					}else if(users[i].google_account == "no"){
						google_account_no++;
					}
				}
				if(!!users[i].instagram_account){
					if(users[i].instagram_account == "yes"){
						instagram_account_yes++;
					}else if(users[i].instagram_account == "no"){
						instagram_account_no++;
					}
				}
				if(!!users[i].linkedin_account){
					if(users[i].linkedin_account == "yes"){
						linkedin_account_yes++;
					}else if(users[i].linkedin_account == "no"){
						linkedin_account_no++;
					}
				}
				if(!!users[i].tumblr_account){
					if(users[i].tumblr_account == "yes"){
						tumblr_account_yes++;
					}else if(users[i].tumblr_account == "no"){
						tumblr_account_no++;
					}
				}
				if(!!users[i].twitter_account){
					if(users[i].twitter_account == "yes"){
						twitter_account_yes++;
					}else if(users[i].twitter_account == "no"){
						twitter_account_no++;
					}
				}
				if(!!users[i].yahoo_account){
					if(users[i].yahoo_account == "yes"){
						yahoo_account_yes++;
					}else if(users[i].yahoo_account == "no"){
						yahoo_account_no++;
					}
				}
				
			}
		}
		
		{

			var chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_AGE');
			chart_data[0][1] = title =  this.statistics_type == "participants" ? gettext('$USERS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_LOCATION_CHART_LABEL_CUSTOMERS') : '';
			chart_data[1] = new Array();
			chart_data[1][0] = age_interval2_label;
			chart_data[1][1] = age_interval1_male + age_interval1_female;
			chart_data[2] = new Array();
			chart_data[2][0] = age_interval2_label;
			chart_data[2][1] = age_interval2_male + age_interval2_female;
			chart_data[3] = new Array();
			chart_data[3][0] = age_interval3_label;
			chart_data[3][1] = age_interval3_male + age_interval3_female;
			chart_data[4] = new Array();
			chart_data[4][0] = age_interval4_label;
			chart_data[4][1] = age_interval4_male + age_interval4_female;
			chart_data[5] = new Array();
			chart_data[5][0] = age_interval5_label;
			chart_data[5][1] = age_interval5_male + age_interval5_female;
			chart_data[6] = new Array();
			chart_data[6][0] = age_interval6_label;
			chart_data[6][1] = age_interval6_male + age_interval6_female;
			chart_data[7] = new Array();
			chart_data[7][0] = age_interval7_label;
			chart_data[7][1] = age_interval7_male + age_interval7_female;
			if(age_interval8_male > 0 || age_interval8_female > 0){
				chart_data[8] = new Array();
				chart_data[8][0] = age_interval8_label;
				chart_data[8][1] = age_interval8_male + age_interval8_female;
			}			

			var data = new google.visualization.arrayToDataTable(chart_data);

			var title = gettext('$USERS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS');

			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};

			var type = 'ColumnChart';

			this.graphs['graph1'] = { data : data, options : options, type : type};

		}

		{

			chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$USERS_STATISTICS_GENDER_CHART_LABEL_GENDER');
			chart_data[0][1] = gettext('$USERS_STATISTICS_GENDER_CHART_LABEL_GENDER');
			chart_data[1] = new Array();
			chart_data[1][0] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_MALE');
			chart_data[1][1] = gender_male;					
			chart_data[2] = new Array();
			chart_data[2][0] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_FEMALE');
			chart_data[2][1] = gender_female;
			chart_data[3] = new Array();
			chart_data[3][0] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE');
			chart_data[3][1] = gender_notavailable;

			var data = new google.visualization.arrayToDataTable(chart_data);

			var title = gettext('$USERS_STATISTICS_GENDER_CHART_LABEL_GENDER');
			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};

			var type = 'PieChart';

			this.graphs['graph2'] = { data : data, options : options, type : type};

		}

		{

			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_AGE');
			chart_data[0][1] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_MALE');
			chart_data[0][2] = gettext('$USERS_STATISTICS_AGE_CHART_LABEL_FEMALE');
			chart_data[1] = new Array();
			chart_data[1][0] = age_interval1_label;
			chart_data[1][1] = age_interval1_male;
			chart_data[1][2] = age_interval1_female;
			chart_data[2] = new Array();
			chart_data[2][0] = age_interval2_label;
			chart_data[2][1] = age_interval2_male;
			chart_data[2][2] = age_interval2_female;
			chart_data[3] = new Array();
			chart_data[3][0] = age_interval3_label;
			chart_data[3][1] = age_interval3_male;
			chart_data[3][2] = age_interval3_female;
			chart_data[4] = new Array();
			chart_data[4][0] = age_interval4_label;
			chart_data[4][1] = age_interval4_male;
			chart_data[4][2] = age_interval4_female;
			chart_data[5] = new Array();
			chart_data[5][0] = age_interval5_label;
			chart_data[5][1] = age_interval5_male;
			chart_data[5][2] = age_interval5_female;
			chart_data[6] = new Array();
			chart_data[6][0] = age_interval6_label;
			chart_data[6][1] = age_interval6_male;
			chart_data[6][2] = age_interval6_female;
			chart_data[7] = new Array();
			chart_data[7][0] = age_interval7_label;
			chart_data[7][1] = age_interval7_male;
			chart_data[7][2] = age_interval7_female;
			if(age_interval8_male > 0 || age_interval8_female > 0){
				chart_data[8] = new Array();
				chart_data[8][0] = age_interval8_label;
				chart_data[8][1] = age_interval8_male;
				chart_data[8][2] = age_interval8_female;	
			}								

			var data = new google.visualization.arrayToDataTable(chart_data);

			var title = gettext('$USERS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS');

			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};

			var type = 'ColumnChart';

			this.graphs['graph3'] = { data : data, options : options, type : type};

		}

		{

			if (all_locations.length > 0){
				all_locations.sort(function(a,b){return parseInt(a[1],10) - parseInt(b[1],10);}).reverse();

				if (all_locations.length > 5){
					all_locations = all_locations.slice(0,4);
				}

				all_locations.unshift([gettext('$USERS_STATISTICS_LOCATION_CHART_LABEL_LOCATION'), gettext('$USERS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS')]);

				var data = new google.visualization.arrayToDataTable(all_locations);
				var title = gettext('$USERS_STATISTICS_LOCATION_CHART_TITLE');

				var options = {
					'title': title,
					'width':300,
					'height':300,
					'legend': { position: 'bottom'}
				};

				var type = 'PieChart';

				this.graphs['graph4'] = { data : data, options : options, type : type};

			}

		}

		{

			chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK');
			chart_data[0][1] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_YES');
			chart_data[0][2] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_NO');
			chart_data[1] = new Array();
			chart_data[1][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FACEBOOK');
			chart_data[1][1] = facebook_account_yes;					
			chart_data[1][2] = facebook_account_no;
			chart_data[2] = new Array();
			chart_data[2][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FLICKR');
			chart_data[2][1] = flickr_account_yes;					
			chart_data[2][2] = flickr_account_no;
			chart_data[3] = new Array();
			chart_data[3][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_GOOGLE');
			chart_data[3][1] = google_account_yes;					
			chart_data[3][2] = google_account_no;
			chart_data[4] = new Array();
			chart_data[4][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_INSTAGRAM');
			chart_data[4][1] = instagram_account_yes;					
			chart_data[4][2] = instagram_account_no;
			chart_data[5] = new Array();
			chart_data[5][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_LINKEDIN');
			chart_data[5][1] = linkedin_account_yes;					
			chart_data[5][2] = linkedin_account_no;
			chart_data[6] = new Array();
			chart_data[6][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TUMBLR');
			chart_data[6][1] = tumblr_account_yes;					
			chart_data[6][2] = tumblr_account_no;
			chart_data[7] = new Array();
			chart_data[7][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TWITTER');
			chart_data[7][1] = twitter_account_yes;					
			chart_data[7][2] = twitter_account_no;
			chart_data[8] = new Array();
			chart_data[8][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_YAHOO');
			chart_data[8][1] = yahoo_account_yes;					
			chart_data[8][2] = yahoo_account_no;

			var data = new google.visualization.arrayToDataTable(chart_data);

			var title = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_ABSOLUTE');

			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};

			var type = 'ColumnChart';

			this.graphs['graph5'] = { data : data, options : options, type : type};
			
			chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK');
			chart_data[0][1] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK');
			chart_data[1] = new Array();
			chart_data[1][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FACEBOOK');
			chart_data[1][1] = facebook_account_yes;					
			chart_data[2] = new Array();
			chart_data[2][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_FLICKR');
			chart_data[2][1] = flickr_account_yes;					
			chart_data[3] = new Array();
			chart_data[3][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_GOOGLE');
			chart_data[3][1] = google_account_yes;					
			chart_data[4] = new Array();
			chart_data[4][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_INSTAGRAM');
			chart_data[4][1] = instagram_account_yes;					
			chart_data[5] = new Array();
			chart_data[5][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_LINKEDIN');
			chart_data[5][1] = linkedin_account_yes;					
			chart_data[6] = new Array();
			chart_data[6][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TUMBLR');
			chart_data[6][1] = tumblr_account_yes;					
			chart_data[7] = new Array();
			chart_data[7][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_TWITTER');
			chart_data[7][1] = twitter_account_yes;					
			chart_data[8] = new Array();
			chart_data[8][0] = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_YAHOO');
			chart_data[8][1] = yahoo_account_yes;					

			var data = new google.visualization.arrayToDataTable(chart_data);

			var title = gettext('$USERS_STATISTICS_NETWORK_CHART_LABEL_NETWORK_PERCENTAGE');

			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};

			var type = 'PieChart';

			this.graphs['graph6'] = { data : data, options : options, type : type};
			
		}
		
	};
	
	CODAChartsReport.prototype.processSubscriptionsData = function() {
		
		if(!!this.data.subscriptions){

			var subscriptions = this.data.subscriptions;
			
			var product_type_index = new Array();
			var product_type_info = new Array();
			var index = 0;
			
			for (var i = 0; i < subscriptions.length; i++){
				
				if (product_type_index.indexOf(subscriptions[i].product_name) == -1){
					product_type_index[index] = subscriptions[i].product_name;
					product_type_info[index] = new Array();
					product_type_info[index]["product_name"] = subscriptions[i].product_name;
					product_type_info[index]["quantity"] = 1;
					
					index++;
				}
				
			}
			
			for (var i = 0; i < subscriptions.length; i++){
				if(!!subscriptions[i].product_name){
					if (product_type_index.indexOf(subscriptions[i].product_name) != -1){
						product_type_info[product_type_index.indexOf(subscriptions[i].product_name)]["quantity"] = product_type_info[product_type_index.indexOf(subscriptions[i].product_name)]["quantity"] + 1;  
					}
				}
			}
			
			{
				chart_data = new Array();
				chart_data[0] = new Array();
				chart_data[0][0] = gettext('$SUBSCRIPTIONS_STATISTICS_PROD_NAME_CHART_LABEL_PROD_NAME');
				chart_data[0][1] = gettext('$SUBSCRIPTIONS_STATISTICS_PROD_NAME_CHART_LABEL_PROD_NAME');
				
				for (var index in product_type_info){
					
					chart_data[Number(index)+1] = new Array();
					chart_data[Number(index)+1][0] = product_type_info[index]["product_name"];
					chart_data[Number(index)+1][1] = Number(product_type_info[index]["quantity"]);
					
				}

				var data = new google.visualization.arrayToDataTable(chart_data);

				var title = gettext('$SUBSCRIPTIONS_STATISTICS_PROD_NAME_CHART_LABEL_PROD_NAME');
				
				var options = {
					'title': title,
					'width':300,
					'height':300,
					'legend': { position: 'bottom'}
				};

				var type = 'PieChart';

				this.graphs['graph9'] = { data : data, options : options, type : type};
			
			}
			
		}

	};

	CODAChartsReport.prototype.processApplicationsData = function() {
	
		var applications = this.data.applications;
		
		var redpanda_content_offer = 0;
		var redpanda_quizanswerpage_contest = 0;
		var redpanda_photovideo_contest = 0;
		var redpanda_friends_disccount = 0;
		var redpanda_quiz_contest = 0;
		var redpanda_video_contest = 0;
		var redpanda_commerce_single = 0;
		var redpanda_commerce_promotions = 0;
		var redpanda_commerce_store = 0;
		var redpanda_instagram_feed = 0;
		var redpanda_flickr_feed = 0;
		var redpanda_twitter_feed = 0;
		var redpanda_voucher_offer = 0;
		var redpanda_direct_offer = 0;
		var redpanda_email_signup = 0;
		var redpanda_fan_vote = 0;
		var redpanda_profile_quiz = 0;
		var redpanda_storefront_promotions = 0;
		
		var applications_active = 0;
		var applications_inactive = 0;
		var app_active_redpanda_content_offer = 0;
		var app_active_redpanda_quizanswerpage_contest = 0;
		var app_active_redpanda_photovideo_contest = 0;
		var app_active_redpanda_friends_disccount = 0;
		var app_active_redpanda_quiz_contest = 0;
		var app_active_redpanda_video_contest = 0;
		var app_active_redpanda_commerce_single = 0;
		var app_active_redpanda_commerce_promotions = 0;
		var app_active_redpanda_commerce_store = 0;
		var app_active_redpanda_instagram_feed = 0;
		var app_active_redpanda_flickr_feed = 0;
		var app_active_redpanda_twitter_feed = 0;
		var app_active_redpanda_voucher_offer = 0;
		var app_active_redpanda_direct_offer = 0;
		var app_active_redpanda_email_signup = 0;
		var app_active_redpanda_fan_vote = 0;
		var app_active_redpanda_profile_quiz = 0;
		var app_active_redpanda_storefront_promotions = 0;
		var app_inactive_redpanda_content_offer = 0;
		var app_inactive_redpanda_quizanswerpage_contest = 0;
		var app_inactive_redpanda_photovideo_contest = 0;
		var app_inactive_redpanda_friends_disccount = 0;
		var app_inactive_redpanda_quiz_contest = 0;
		var app_inactive_redpanda_video_contest = 0;
		var app_inactive_redpanda_commerce_single = 0;
		var app_inactive_redpanda_commerce_promotions = 0;
		var app_inactive_redpanda_commerce_store = 0;
		var app_inactive_redpanda_instagram_feed = 0;
		var app_inactive_redpanda_flickr_feed = 0;
		var app_inactive_redpanda_twitter_feed = 0;
		var app_inactive_redpanda_voucher_offer = 0;
		var app_inactive_redpanda_direct_offer = 0;
		var app_inactive_redpanda_email_signup = 0;
		var app_inactive_redpanda_fan_vote = 0;
		var app_inactive_redpanda_profile_quiz = 0;
		var app_inactive_redpanda_storefront_promotions = 0;
		
		var app_state_created = 0;
		var app_state_active = 0;
		var app_state_no_user_content = 0;
		var app_state_canceled = 0;
		var app_state_archived = 0;
		
		for (var i = 0; i < applications.length; i++){
			
			switch (applications[i].type) {
			
				case "redpanda-content-offer" : {
					redpanda_content_offer++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_content_offer++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_content_offer++;
						}
					}
					break;
				}
				case "redpanda-quizanswerpage-contest" : {
					redpanda_quizanswerpage_contest++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_quizanswerpage_contest++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_quizanswerpage_contest++;
						}
					}
					break;
				}
				case "redpanda-photovideo-contest" : {
					redpanda_photovideo_contest++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_photovideo_contest++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_photovideo_contest++;
						}
					}
					break;
				}
				case "redpanda-friends-disccount" : {
					redpanda_friends_disccount++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_friends_disccount++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_friends_disccount++;
						}
					}
					break;
				}
				case "redpanda-quiz-contest" : {
					redpanda_quiz_contest++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_quiz_contest++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_quiz_contest++;
						}
					}
					break;
				}
				case "redpanda-video-contest" : {
					redpanda_video_contest++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_video_contest++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_video_contest++;
						}
					}
					break;
				}
				case "redpanda-commerce-single" : {
					redpanda_commerce_single++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_commerce_single++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_commerce_single++;
						}
					}
					break;
				}
				case "redpanda-commerce-promotions" : {
					redpanda_commerce_promotions++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_commerce_promotions++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_commerce_promotions++;
						}
					}
					break;
				}
				case "redpanda-commerce-store" : {
					redpanda_commerce_store++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_commerce_store++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_commerce_store++;
						}
					}
					break;
				}
				case "redpanda-instagram-feed" : {
					redpanda_instagram_feed++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_instagram_feed++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_instagram_feed++;
						}
					}
					break;
				}
				case "redpanda-flickr-feed" : {
					redpanda_flickr_feed++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_flickr_feed++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_flickr_feed++;
						}
					}
					break;
				}
				case "redpanda-twitter-feed" : {
					redpanda_twitter_feed++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_twitter_feed++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_twitter_feed++;
						}
					}
					break;
				}
				case "redpanda-voucher-offer" : {
					redpanda_voucher_offer++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_voucher_offer++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_voucher_offer++;
						}
					}
					break;
				}
				case "redpanda-direct-offer" : {
					redpanda_direct_offer++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_direct_offer++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_direct_offer++;
						}
					}
					break;
				}
				case "redpanda-email-signup" : {
					redpanda_email_signup++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_email_signup++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_email_signup++;
						}
					}
					break;
				}
				case "redpanda-fan-vote" : {
					redpanda_fan_vote++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_fan_vote++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_fan_vote++;
						}
					}
					break;
				}
				case "redpanda-profile-quiz" : {
					redpanda_profile_quiz++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_profile_quiz++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_profile_quiz++;
						}
					}
					break;
				}
				case "redpanda-storefront-promotions" : {
					redpanda_storefront_promotions++;
					if (!!applications[i].active){
						if (applications[i].active == "yes"){
							applications_active++;
							app_active_redpanda_storefront_promotions++;
						}else if (applications[i].active == "no"){
							applications_inactive++;
							app_inactive_redpanda_storefront_promotions++;
						}
					}
					break;
				}
			
			}
			
			switch (applications[i].state) {
			
				case "created" :{
					app_state_created++;
					break;
				}
				case "active" :{
					app_state_active++;
					break;
				}
				case "no_user_content" :{
					app_state_no_user_content++;
					break;
				}
				case "canceled" :{
					app_state_canceled++;
					break;
				}
				case "archived" :{
					app_state_archived++;
					break;
				}
			
			}
			
		}
	
		{
			
			chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE');
			chart_data[0][1] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE');
			chart_data[1] = new Array();
			chart_data[1][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_CONTENT_OFFER');
			chart_data[1][1] = redpanda_content_offer;
			chart_data[2] = new Array();
			chart_data[2][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZANSWERPAGE_CONTEST');
			chart_data[2][1] = redpanda_quizanswerpage_contest;
			chart_data[3] = new Array();
			chart_data[3][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PHOTOVIDEO_CONTEST');
			chart_data[3][1] = redpanda_photovideo_contest;
			chart_data[4] = new Array();
			chart_data[4][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FRIENDS_DISCCOUNT');
			chart_data[4][1] = redpanda_friends_disccount;
			chart_data[5] = new Array();
			chart_data[5][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_QUIZ_CONTEST');
			chart_data[5][1] = redpanda_quiz_contest;
			chart_data[6] = new Array();
			chart_data[6][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VIDEO_CONTEST');
			chart_data[6][1] = redpanda_video_contest;
			chart_data[7] = new Array();
			chart_data[7][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_SINGLE');
			chart_data[7][1] = redpanda_commerce_single;
			chart_data[8] = new Array();
			chart_data[8][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_PROMOTIONS');
			chart_data[8][1] = redpanda_commerce_promotions;
			chart_data[9] = new Array();
			chart_data[9][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_COMMERCE_STORE');
			chart_data[9][1] = redpanda_commerce_store;
			chart_data[10] = new Array();
			chart_data[10][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_INSTAGRAM_FEED');
			chart_data[10][1] = redpanda_instagram_feed;
			chart_data[11] = new Array();
			chart_data[11][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FLICKR_FEED');
			chart_data[11][1] = redpanda_flickr_feed;
			chart_data[12] = new Array();
			chart_data[12][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_TWITTER_FEED');
			chart_data[12][1] = redpanda_twitter_feed;
			chart_data[13] = new Array();
			chart_data[13][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_VOUCHER_OFFER');
			chart_data[13][1] = redpanda_voucher_offer;
			chart_data[14] = new Array();
			chart_data[14][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_DIRECT_OFFER');
			chart_data[14][1] = redpanda_direct_offer;
			chart_data[15] = new Array();
			chart_data[15][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_EMAIL_SIGNUP');
			chart_data[15][1] = redpanda_email_signup;
			chart_data[16] = new Array();
			chart_data[16][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_FAN_VOTE');
			chart_data[16][1] = redpanda_fan_vote;
			chart_data[17] = new Array();
			chart_data[17][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_PROFILE_QUIZ');
			chart_data[17][1] = redpanda_profile_quiz;
			chart_data[18] = new Array();
			chart_data[18][0] = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_REDPANDA_STOREFRONT_PROMOTIONS');
			chart_data[18][1] = redpanda_storefront_promotions;										
	
			var data = new google.visualization.arrayToDataTable(chart_data);
	
			var title = gettext('$APPS_STATISTICS_APP_TYPE_CHART_LABEL_APP_TYPE');
		
			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};
	
			var type = 'PieChart';
	
			this.graphs['graph7'] = { data : data, options : options, type : type};
			
			chart_data = new Array();
			chart_data[0] = new Array();
			chart_data[0][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_APP_STATE');
			chart_data[0][1] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_APP_STATE');
			chart_data[1] = new Array();
			chart_data[1][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CREATED');
			chart_data[1][1] = app_state_created;
			chart_data[2] = new Array();
			chart_data[2][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ACTIVE');
			chart_data[2][1] = app_state_active;
			chart_data[3] = new Array();
			chart_data[3][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_NO_USER_CONTENT');
			chart_data[3][1] = app_state_no_user_content;
			chart_data[4] = new Array();
			chart_data[4][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_CANCELED');
			chart_data[4][1] = app_state_canceled;
			chart_data[5] = new Array();
			chart_data[5][0] = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_ARCHIVED');
			chart_data[5][1] = app_state_archived;
	
			var data = new google.visualization.arrayToDataTable(chart_data);
	
			var title = gettext('$APPS_STATISTICS_APP_STATE_CHART_LABEL_APP_STATE');
	
			var options = {
				'title': title,
				'width':300,
				'height':300,
				'legend': { position: 'bottom'}
			};
	
			var type = 'PieChart';
	
			this.graphs['graph8'] = { data : data, options : options, type : type};
			
		}
		
	};
	
	KSystem.included('CODAChartsReport');
}, Kinky.CODA_INCLUDER_URL);
function CODAGraphForm(parent, href, layout, parentHref) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.target = {
			href : href,
			layout : layout,
			parent : parentHref
		};
		this.childForms = {};
		this.schemas = {};
		this.processing = 1;

		this.moreOptionsButton = false;

		this.hasChildrenPanel = true;
		this.config = {
			href : (!!href ? href : null)
		};

		this.messages = window.document.createElement('div');
		this.messages.innerHTML = '&nbsp;';
		this.setStyle({
			opacity : 0
		}, this.messages);
		this.panel.appendChild(this.messages);

		this.headers = {};
		this.cur_width = 0;

		this.actions = [];
		this.helpURL = CODAGenericShell.generateHelpURL(this);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAGraphForm.prototype = new KPanel();
	CODAGraphForm.prototype.constructor = CODAGraphForm;

	CODAGraphForm.prototype.refresh = function() {
		if (!!this.getParent('CODAFormIconDashboard')) {
			var list = this.getParent('CODAFormIconDashboard');
			var temp = list.getIconFor(this.target.data);
			list.SELECTED.options = temp.options;
			temp.destroy();
			delete temp;
			list.SELECTED.refresh();
		}
		else {
			var dialog = this.getParent('KFloatable');
			var dispatched = dialog._url;
			dialog.closeMe();
			KSystem.addTimer(function() {
				KBreadcrumb.dispatchURL({
					hash : dispatched
				});
			}, 750);
		}
	};

	CODAGraphForm.prototype.load = function() {
		if (!!this.config && !!this.config.href && !this.target.data) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, this.headers);
			this.kinky.get(this, (this.config.href.indexOf(':') == -1 ? 'rest:' : '') + this.config.href, {}, headers);
		}
		else if (!!this.target.data) {
			this.onLoad(this.target.data);
		}
		else {
			this.onLoad();
		}
	};

	CODAGraphForm.prototype.refreshData = function() {
		this.getParent('KFloatable');
	};

	CODAGraphForm.prototype.setData = function(data) {
		if (!!data) {
			this.target._id = data.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAGraphForm.prototype.onLoad = function(data, request) {
		this.setData(data);
		this.loadForm();
	};

	CODAGraphForm.prototype.onLoadSchema = function(data, request) {
		data = { schema : data };

		if (request.service == this.config.schema) {
			this.rootSchema = data.schema;
		}
		this.schemas[request.service] = data.schema;
		if (!!request.action) {
			KCache.commit(request.service, data);
		}
		this.loadForm();
	};

	CODAGraphForm.prototype.loadForm = function() {
		this.draw();
	};

	CODAGraphForm.prototype.onNotFound = CODAGraphForm.prototype.onNoContent = function(data, request) {
		this.activate();
	};

	CODAGraphForm.prototype.onUnauthorized = function(data) {

	};

	CODAGraphForm.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		for (var c in this.childWidget(this.selected).childWidgets()) {
			var child = this.childWidget(this.selected).childWidget(c);
			if (child instanceof KAbstractInput && !(child instanceof KStatic)) {
				KSystem.addTimer(function() {
					child.focus();
				}, 150);
				break;
			}
		}
	};

	CODAGraphForm.prototype.visible = function(tween) {
		if (this.display) {
			return;
		}
		KPanel.prototype.visible.call(this);

		this.setStyle({
			height : (this.getParent('KFloatable').height - 70) + 'px'
		}, KWidget.CONTENT_DIV);

		if (!!this.header && !!this.messages) {
			this.panel.insertBefore(this.header, this.messages);
		}

		var floatable = this.getParent('KFloatable');
		this.getShell().onFormOpen(floatable);
	};

	CODAGraphForm.prototype.draw = function() {
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAGraphForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		KPanel.prototype.draw.call(this);
		this.makeHelp();
		if (!!this.msg) {
			this.showMessage(this.msg);
			this.msg = null;
		}
	};

	CODAGraphForm.prototype.hideConfirm = function() {
		this.childWidget('/buttons/confirm').setStyle({
			display : 'none'
		});
		if (this.className.indexOf('Wizard') == -1) {
			this.childWidget('/buttons/cancel').setStyle({
				right : this.minimized ? '21px' : '20px'
			});
		}
	};

	CODAGraphForm.prototype.hideCancel = function() {
		this.childWidget('/buttons/cancel').setStyle({
			display : 'none'
		});
	};

	CODAGraphForm.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAGraphForm.prototype.showMessage = function(message, persistent) {
		if (!!this.timer) {
			KSystem.removeTimer(this.timer);
			this.timer = null;
		}
		if (persistent) {
			this.disable(true);
		}
		else {
			this.enable();
			this.messages.innerHTML = message;
			this.removeCSSClass('CODAFormErrorMessages', this.messages);
			this.addCSSClass('CODAFormSuccessMessages', this.messages);

			this.setStyle({
				display : 'block',
				position: 'fixed',
				bottom : '50px',
				right: '360px'
			}, this.messages);

			KEffects.addEffect(this.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				applyToElement : true,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onComplete : function(w, t) {
					if (!!w && !persistent) {
						w.timer = KSystem.addTimer('CODAGraphForm.hideMessage(\'' + w.id + '\')', 3000);
					}
				}
			});
		}

	};

	CODAGraphForm.hideMessage = function(id) {
		var form = Kinky.getWidget(id);
		if (!form) {
			return;
		}
		form.timer = null;
		if (!!form) {
			form.enable();
			KEffects.addEffect(form.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (!!w) {
						w.messages.style.display = 'none';
						w.messages.innerHTML = '&nbsp;';
					}
				}
			});
		}
	};

	CODAGraphForm.prototype.onDestroy = function() {
		this.cleanCache();
	};

	CODAGraphForm.prototype.onClose = function() {
		if (!this.minimized) {
			for (var a in this.actions) {
				this.getShell().removeToolbarButton(this.actions[a]);
			}
		}
		KBreadcrumb.dispatchEvent(null, {
			action : '/close/form' + this.config.href
		});
	};

	CODAGraphForm.prototype.addSeparator = function(text, panel) {
		var c = (panel.activated() ? panel.content : panel.container);
		var h4 = window.document.createElement('h4');
		h4.innerHTML = text;
		h4.className = ' CODAGraphFormSeparator ';
		c.appendChild(h4);
	};

	CODAGraphForm.prototype.addAction = function(button) {
		button.hash = '/' + this.className + '/' + button.inputID;
		button.data = {
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		}
		button.panel.style.opacity = '0';
		this.actions.push(button);
		this.getShell().addToolbarButton(button, 0, 'right', true);
	};

	CODAGraphForm.prototype.onGet = function(data, request) {
	};

	CODAGraphForm.prototype.onError = function(data, request) {
		this.enable();
		var msg = undefined;
		eval('msg = ' + gettext('$CONFIRM_OPERATION_ERROR').replace('[NUMBER]', data.error_code));
		this.showMessage(msg);
		this.removeCSSClass('CODAFormSuccessMessages', this.messages);
		this.addCSSClass('CODAFormErrorMessages', this.messages);
	};

	CODAGraphForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAGraphForm.prototype.onPost = CODAGraphForm.prototype.onCreated = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAGraphForm.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAGraphForm.prototype.enable = function() {
		if (this.loader_t !== undefined) {
			KSystem.removeTimer(this.loader_t);
			this.loader_t = undefined;
			return;
		}
		KPanel.prototype.enable.call(this);
	};

	CODAGraphForm.prototype.disable = function(communicating) {
		if (!!communicating) {
			if (this.loader_t === undefined) {
				var self = this;
				this.loader_t = KSystem.addTimer(function() {
					self.loader_t = undefined;
					KPanel.prototype.disable.call(self);
				}, 250);
			}
		}
		else {
			if (this.loader_t !== undefined) {
				KSystem.removeTimer(this.loader_t);
				this.loader_t = undefined;
			}
			KPanel.prototype.disable.call(this);
		}
	};

	CODAGraphForm.closeMe = function(event) {
		KDOM.stopEvent(event);
		var form = KDOM.getEventWidget(event).parent;
		form.parent.closeMe();
	};

	CODAGraphForm.getHelpOptions = function() {
		return KSystem.clone(CODAGraphForm.HELP_OPTIONS);
	};

	KWidget.STATE_FUNCTIONS .push('onClose');

	KSystem.included('CODAGraphForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAReportsDashboard(parent) {
	if (parent != null) {
		var icons = [];

		icons.push({
			icon : 'css:fa fa-bar-chart-o',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_CHARTS_REPORTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_CHARTS_REPORTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : gettext('$DASHBOARD_BUTTON_CHARTS_REPORTS_LIST'),
				link : '#/generate-charts-report' + '/' + this.getShell().data.app_id
			}]
		});

		icons.push({
			icon : 'css:fa fa-users',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_USERS_REPORTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_USERS_REPORTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : gettext('$DASHBOARD_BUTTON_USERS_REPORTS_LIST'),
				link : '#/generate-users-report' + '/' + this.getShell().data.app_id
			}]
		});
		
		icons.push({
			icon : 'css:fa fa-puzzle-piece',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : gettext('$DASHBOARD_BUTTON_APPLICATIONS_REPORTS_LIST'),
				link : '#/generate-applications-report' + '/' + this.getShell().data.app_id
			}]
		});
		
		icons.push({
			icon : 'css:fa fa-credit-card',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_REPORTS_LIST'),
				link : '#/generate-subscriptions-report' + '/' + this.getShell().data.app_id
			}]
		});
		
		icons.push({
			icon : 'css:fa fa-credit-card',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : gettext('$DASHBOARD_BUTTON_SUBSCRIPTIONS_FULL_REPORTS_LIST'),
				link : '#/generate-subscriptions-full-report' + '/' + this.getShell().data.app_id
			}]
		});
		
		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAReportsDashboard.prototype = new CODAMenuDashboard();
	CODAReportsDashboard.prototype.construtor = CODAReportsDashboard;

	CODAReportsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAReportsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAReportsShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAReportsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAReportsDashboard',
	'CODAChartsReport'
], function(){
	CODAReportsShell.prototype = new CODAGenericShell();
	CODAReportsShell.prototype.constructor = CODAReportsShell;

	CODAReportsShell.prototype.loadShell = function() {
		this.draw();	
	};

	CODAReportsShell.prototype.draw = function() {
		this.setTitle(gettext('$REPORTS_SHELL_TITLE'));

		this.dashboard = new CODAReportsDashboard(this);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAReportsShell.sendRequest = function() {
	};
	
	CODAReportsShell.prototype.generateChartsReport = function() {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		params = {};
		this.kinky.post(this, 'rest:/chartsreports/retrieve', params, headers, 'onGenerateChartsReport');
	};

	CODAReportsShell.prototype.onGenerateChartsReport = function(data, request) {
		var chartsreports = new CODAChartsReport(this, data);
		chartsreports.hash = '/chartsreports/hash/';
		
		this.makeDialog(chartsreports, { minimized : false, modal : true });
	};

	CODAReportsShell.prototype.generateUsersReport = function() {
		this.kinky.post(this, 'rest:/generatereport/users', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onUsersReportGenerated');
	};
	
	CODAReportsShell.prototype.onUsersReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_USERS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};
	
	CODAReportsShell.prototype.generateSubscriptionsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/subscriptions', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onSubscriptionsReportGenerated');
	};
	
	CODAReportsShell.prototype.generateSubscriptionsFullReport = function() {
		this.kinky.post(this, 'rest:/generatereport/subscriptions?type=full', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onSubscriptionsReportGenerated');
	};
	
	CODAReportsShell.prototype.onSubscriptionsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_SUBSCRIPTIONS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};
	
	CODAReportsShell.prototype.generateApplicationsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/applications', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onApplicationsReportGenerated');
	};
	
	CODAReportsShell.prototype.onApplicationsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_APPLICATIONS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};
	
	CODAReportsShell.prototype.onNoContent = function(data, request) {
		CODAGenericShell.prototype.onNoContent.call(this, data, request);
	};

	CODAReportsShell.prototype.onNotFound = function(data, request) {
		this.draw();
	};

	CODAReportsShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]){
			case 'generate-charts-report': {
				Kinky.getDefaultShell().generateChartsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-users-report': {
				Kinky.getDefaultShell().generateUsersReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-applications-report': {
				Kinky.getDefaultShell().generateApplicationsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-subscriptions-report': {
				Kinky.getDefaultShell().generateSubscriptionsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-subscriptions-full-report': {
				Kinky.getDefaultShell().generateSubscriptionsFullReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}

	};

	KSystem.included('CODAReportsShell');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

