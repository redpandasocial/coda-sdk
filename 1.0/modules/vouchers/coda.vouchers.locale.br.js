///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$VOUCHERCONFIGURATIONS_SHELL_TITLE', 'VOUCHER CONFIGURATIONS', 'br');
settext('$LIST_VOUCHERS_PROPERTIES', 'Vouchers', 'br');
settext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION', 'Nova Configura\u00e7\u00e3o de Voucher', 'br');
settext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION_DESC', 'Adiciona uma nova configura\u00e7\u00e3o para os voucher a gerar.', 'br');
settext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION', 'Configura\u00e7\u00f5es de Vouchers', 'br');
settext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION_DESC', 'Acesse \u00e0s configura\u00e7\u00f5es de vouchers para inserir uma nova ou consultar e alterar os dados de uma das existentes.', 'br');
settext('$DASHBOARD_BUTTON_LIST_VOUCHER', 'Vouchers', 'br');
settext('$DASHBOARD_BUTTON_LIST_VOUCHER_DESC', 'Acede \u00e0 lista de vouchers gerados e consulta os seus dados. Dá baixa dos vouchers que j\u00e1 foram utilizados.', 'br');

///////////////////////////////////////////////////////////////////
//FORMS
settext('$ADD_VOUCHERCONFIGURATION_PROPERTIES', 'Nova Configuração de Voucher', 'br');
settext('$EDIT_VOUCHERCONFIGURATION_PROPERTIES', 'Editar Configuração de Voucher ', 'br');

settext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME', 'Título', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT', 'Texto', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL', 'Imagem', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK', 'Estoque', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE', 'Valor para atribuição', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS', 'Validade (em dias)', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE', 'Data de Validade (desde)', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE', 'Data de Validade (até)', 'br');

settext('$VOUCHERCONFIGURATION_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$VOUCHERCONFIGURATION_AVAILABILITY_PANEL', '<i class="fa fa-calendar"></i> Datas de Utilização', 'br');

settext('$CODA_EXCEPTION_NOT_FOUND', 'Configuração de Voucher não foi encontrada', 'br');

settext('$ERROR_MUST_NOT_BE_NULL', 'inserir tradução em vouchers', 'br');

settext('$DELETE_VOUCHERCONFIGURATION_CONFIRM', 'Tem certeza que deseja remover a configura\u00e7\u00e3o de voucher', 'br');

///////////////////////////////////////////////////////////////////
//WIZARD
settext('$VOUCHERCONFIGURATION_WIZ_PANEL_TITLE', 'Configuração de dados para Vouchers', 'br');
settext('$VOUCHERCONFIGURATION_WIZ_PANEL_DESC', 'Define os dados que vão constar nos vouchers que forem gerados com base na configuração.<br>Configura também quantos vouchers podem ser gerados e qual o valor mínimo que se tem que obter para o poder receber.<br><br>Depois da inserção acesse à configuração pela listagem, consulte e altere os dados e gere os dados relativos a datas de validade.', 'br');

///////////////////////////////////////////////////////////////////
//LIST
settext('$VOUCHERS_LIST_ID', 'C\u00f3digo', 'br');
settext('$VOUCHERS_LIST_START_DATE', 'V\u00e1lido de', 'br');
settext('$VOUCHERS_LIST_END_DATE', 'V\u00e1lido at\u00e9', 'br');
settext('$VOUCHERS_LIST_USE_DATE', 'Utilizado em', 'br');
settext('$VOUCHERS_LIST_STATUS', 'Estado', 'br');

settext('$VOUCHER_OWNER_PARTICIPATION', 'Dados do detentor do voucher', 'br');
settext('$VOUCHER_OWNER_ACCOUNT', 'Dados do detentor do voucher', 'br');
settext('$VOUCHER_OWNER_NO_INFO', 'Dados do detentor do voucher', 'br');
settext('$VOUCHER_USED_SUCCESS', 'Voucher for registado como usado', 'br');

settext('$VOUCHER_DOWNLOAD_OPTION', 'Download', 'br');
settext('$VOUCHER_USE_OPTION', 'Marcar como usado', 'br');
settext('$VOUCHER_USED_INFO', 'O voucher j\u00e1 foi utilizado', 'br');
settext('$VOUCHER_EXPIRED_INFO', 'O voucher expirou e n\u00e3o deve ser aceite', 'br');

settext('$CODA_LIST_ADD_BUTTON_CODAVOUCHERCONFIGURATIONSDASHBOARD', '<i class="fa fa-gear"></i> Nova Configuração de Voucher', 'br');

KSystem.included('CODAVouchersLocale_pt');
///////////////////////////////////////////////////////////////////
// FORMS
settext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME_HELP', 'Título do Voucher<br> Aparece no topo de cada voucher gerado.', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT_HELP', 'Texto de descrição do voucher<br> Quando está definido aparece no voucher.', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL_HELP', 'Imagem a colocar no voucher<br> Se um voucher estiver associado a um prêmio físico poderá colocar-se a imagem correspondente.', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK_HELP', 'Número de vouchers que podem ser gerados a partir desta configuração.', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE_HELP', 'Valor do Voucher<br>Deverá ser utilizado conforme os requisitos do módulo em que seja utilizado (ex: para a atribuição de um voucher como prêmio em função do número de pontos obtidos numa participação, este valor corresponderá ao número mínimo de pontos necessários para o voucher ser gerado).', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher é válido, sendo que:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE_HELP', 'Data de início de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE_HELP', 'Data de fim de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAVouchersLocale_Help_pt');

