///////////////////////////////////////////////////////////////////
// FORMS
settext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME_HELP', 'Título do Voucher<br> Aparece no topo de cada voucher gerado.', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT_HELP', 'Texto de descrição do voucher<br> Quando está definido aparece no voucher.', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL_HELP', 'Imagem a colocar no voucher<br> Se um voucher estiver associado a um prémio físico poderá colocar-se a imagem correspondente.', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK_HELP', 'Número de vouchers que podem ser gerados a partir desta configuração.', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE_HELP', 'Valor do Voucher<br>Deverá ser utilizado conforme os requisitos do módulo em que seja utilizado (ex: para a atribuição de um voucher como prémio em função do número de pontos obtidos numa participação, este valor corresponderá ao número mínimo de pontos necessários para o voucher ser gerado).', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher é válido, sendo que:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE_HELP', 'Data de início de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE_HELP', 'Data de fim de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAVouchersLocale_Help_pt');

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$VOUCHERCONFIGURATIONS_SHELL_TITLE', 'VOUCHER CONFIGURATIONS', 'pt');
settext('$LIST_VOUCHERS_PROPERTIES', 'Vouchers', 'pt');
settext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION', 'Nova Configura\u00e7\u00e3o de Voucher', 'pt');
settext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION_DESC', 'Adiciona uma nova configura\u00e7\u00e3o para os voucher a gerar.', 'pt');
settext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION', 'Configura\u00e7\u00f5es de Vouchers', 'pt');
settext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION_DESC', 'Acede \u00e0s configura\u00e7\u00f5es de vouchers para inserires uma nova ou consultar e alterar os dados de uma das existentes.', 'pt');
settext('$DASHBOARD_BUTTON_LIST_VOUCHER', 'Vouchers', 'pt');
settext('$DASHBOARD_BUTTON_LIST_VOUCHER_DESC', 'Acede \u00e0 lista de vouchers gerados e consulta os seus dados. Dá baixa dos vouchers que j\u00e1 foram utilizados.', 'pt');

///////////////////////////////////////////////////////////////////
//FORMS
settext('$ADD_VOUCHERCONFIGURATION_PROPERTIES', 'Nova Configuração de Voucher', 'pt');
settext('$EDIT_VOUCHERCONFIGURATION_PROPERTIES', 'Editar Configuração de Voucher ', 'pt');

settext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME', 'Título', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT', 'Texto', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL', 'Imagem', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK', 'Stock', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE', 'Valor para atribuição', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS', 'Validade (em dias)', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE', 'Data de Validade (desde)', 'pt');
settext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE', 'Data de Validade (até)', 'pt');

settext('$VOUCHERCONFIGURATION_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$VOUCHERCONFIGURATION_AVAILABILITY_PANEL', '<i class="fa fa-calendar"></i> Datas de Utilização', 'pt');

settext('$CODA_EXCEPTION_NOT_FOUND', 'Configuração de Voucher não foi encontrada', 'pt');

settext('$ERROR_MUST_NOT_BE_NULL', 'inserir tradução em vouchers', 'pt');

settext('$DELETE_VOUCHERCONFIGURATION_CONFIRM', 'Tens a certeza que queres remover a configura\u00e7\u00e3o de voucher', 'pt');

///////////////////////////////////////////////////////////////////
//WIZARD
settext('$VOUCHERCONFIGURATION_WIZ_PANEL_TITLE', 'Configuração de dados para Vouchers', 'pt');
settext('$VOUCHERCONFIGURATION_WIZ_PANEL_DESC', 'Define os dados que vão constar nos vouchers que forem gerados com base na configuração.<br>Configura também quantos vouchers podem ser gerados e qual o valor mínimo que se tem que obter para o poder receber.<br><br>Depois da inserção acede à configuração pela listagem, consulta e altera os dados e gere os dados relativos a datas de validade.', 'pt');

///////////////////////////////////////////////////////////////////
//LIST
settext('$VOUCHERS_LIST_ID', 'C\u00f3digo', 'pt');
settext('$VOUCHERS_LIST_START_DATE', 'V\u00e1lido de', 'pt');
settext('$VOUCHERS_LIST_END_DATE', 'V\u00e1lido at\u00e9', 'pt');
settext('$VOUCHERS_LIST_USE_DATE', 'Utilizado em', 'pt');
settext('$VOUCHERS_LIST_STATUS', 'Estado', 'pt');

settext('$VOUCHER_OWNER_PARTICIPATION', 'Dados do detentor do voucher', 'pt');
settext('$VOUCHER_OWNER_ACCOUNT', 'Dados do detentor do voucher', 'pt');
settext('$VOUCHER_OWNER_NO_INFO', 'Dados do detentor do voucher', 'pt');
settext('$VOUCHER_USED_SUCCESS', 'Voucher for registado como usado', 'pt');

settext('$VOUCHER_DOWNLOAD_OPTION', 'Download', 'pt');
settext('$VOUCHER_USE_OPTION', 'Marcar como usado', 'pt');
settext('$VOUCHER_USED_INFO', 'O voucher j\u00e1 foi utilizado', 'pt');
settext('$VOUCHER_EXPIRED_INFO', 'O voucher expirou e n\u00e3o deve ser aceite', 'pt');

settext('$CODA_LIST_ADD_BUTTON_CODAVOUCHERCONFIGURATIONSDASHBOARD', '<i class="fa fa-gear"></i> Nova Configuração de Voucher', 'pt');

KSystem.included('CODAVouchersLocale_pt');
