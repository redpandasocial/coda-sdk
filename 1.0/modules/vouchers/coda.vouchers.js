KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAVoucherConfigurationItemForm(parent, href){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		this.target.collection = '/voucherconfigurations';
		this.nillable = true;
	}
}

KSystem.include([
	'CODAForm',
	'KCalendar',
	'KTextArea',
	'KImageUpload'
], function() {
	CODAVoucherConfigurationItemForm.prototype = new CODAForm();
	CODAVoucherConfigurationItemForm.prototype.constructor = CODAVoucherConfigurationItemForm;

	CODAVoucherConfigurationItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAVoucherConfigurationItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAVoucherConfigurationItemForm.prototype.addMinimizedPanel = function() {
		if(!!this.data && !!this.data.code){
			this.setTitle(gettext('$EDIT_VOUCHERCONFIGURATION_PROPERTIES') + ' \u00ab' + this.data.code + '\u00bb');
		}else{
			this.setTitle(gettext('$ADD_VOUCHERCONFIGURATION_PROPERTIES'));
		}

		var voucherconfiguration_general_item_panel = new KPanel(this);
		{

			voucherconfiguration_general_item_panel.hash = '/general';

			var name = new KInput(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME') + ' *', 'name');
			{
				if(!!this.data && !!this.data.name){
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			voucherconfiguration_general_item_panel.appendChild(name);

			var text = new KTextArea(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT'), 'text');
			{
				if (!!this.data && !!this.data.text){
					text.setValue(this.data.text);
				}
				text.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT_HELP'), CODAForm.getHelpOptions());
			}
			voucherconfiguration_general_item_panel.appendChild(text);

			var image_url = new KImageUpload(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.url) {
					image_url.setValue(this.data.url);
				}
				image_url.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(image_url, 'image/*');
				image_url.appendChild(mediaList);
			}
			voucherconfiguration_general_item_panel.appendChild(image_url);

			var stock = new KInput(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK'), 'stock');
			{
				if (!!this.data && !!this.data.stock) {
					stock.setValue(this.data.stock);
				}
				stock.setLength(8);
				stock.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK_HELP'), CODAForm.getHelpOptions());
			}
			voucherconfiguration_general_item_panel.appendChild(stock);

			var value = new KInput(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE'), 'value');
			{
				if (!!this.data && !!this.data.value) {
					value.setValue(this.data.value);
				}
				value.setLength(8);
				value.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE_HELP'), CODAForm.getHelpOptions());
			}
			voucherconfiguration_general_item_panel.appendChild(value);

		}

		this.addPanel(voucherconfiguration_general_item_panel, gettext('$VOUCHERCONFIGURATION_GENERALINFO_PANEL'), true);

		if(!!this.target._id && this.target._id != null){

			var voucherconfiguration_availability_item_panel = new KPanel(this);
			{

				voucherconfiguration_availability_item_panel.hash = '/availability';

				var expiration_days = new KInput(voucherconfiguration_availability_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS'), 'expiration_days');
				{
					if (!!this.data && !!this.data.expiration_days) {
						expiration_days.setValue(this.data.expiration_days);
					}
					expiration_days.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_EXPIRATION_DAYS_HELP'), CODAForm.getHelpOptions());
				}
				voucherconfiguration_availability_item_panel.appendChild(expiration_days);

				var start_date = new KDate(voucherconfiguration_availability_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE'), 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					start_date.hash = '/start_date';
					if(!!this.data && !!this.data.start_date){
						start_date.setValue(new Date(this.data.start_date));
					}
					start_date.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
					start_date.addEventListener('change', CODAVoucherConfigurationItemForm.onDateChanged);
				}
				voucherconfiguration_availability_item_panel.appendChild(start_date);

				var end_date = new KDate(voucherconfiguration_availability_item_panel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE'), 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					if(!!this.data && !!this.data.end_date){
						end_date.setValue(new Date(this.data.end_date));
					}
					end_date.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
				}
				voucherconfiguration_availability_item_panel.appendChild(end_date);

			}

			this.addPanel(voucherconfiguration_availability_item_panel, gettext('$VOUCHERCONFIGURATION_AVAILABILITY_PANEL'), false);
		}

	};

	CODAVoucherConfigurationItemForm.onDateChanged = function(){
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
	};

	CODAVoucherConfigurationItemForm.prototype.setDefaults = function(params) {
		delete params.requestTypes;
		delete params.responseTypes;

//		delete params.start_date;
//		delete params.end_date;

		if(!params._id){
			delete params.expiration_days;
		}

	};

	CODAVoucherConfigurationItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.title = gettext("$VOUCHERCONFIGURATION_WIZ_PANEL_TITLE");
			firstPanel.description = gettext("$VOUCHERCONFIGURATION_WIZ_PANEL_DESC");
			firstPanel.icon = 'css:fa fa-cog';

			var name = new KInput(firstPanel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME') + ' *', 'name');
			{
				name.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(name);

			var text = new KTextArea(firstPanel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT'), 'text');
			{
				text.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_TEXT_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(text);

			var image_url = new KImageUpload(firstPanel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.url) {
					image_url.setValue(this.data.url);
				}
				image_url.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_IMAGE_URL_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(image_url, 'image/*');
				image_url.appendChild(mediaList);
			}
			firstPanel.appendChild(image_url);

			var stock = new KInput(firstPanel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK'), 'stock');
			{
				stock.setLength(8);
				stock.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_STOCK_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(stock);

			var value = new KInput(firstPanel, gettext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE'), 'value');
			{
				value.setLength(8);
				value.setHelp(gettext('$VOUCHERCONFIGURATION_ITEM_FORM_VALUE_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(value);
		}
		return [firstPanel];
	};

	CODAVoucherConfigurationItemForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAVoucherConfigurationItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAVoucherConfigurationItemForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_NOT_FOUND'));
	};

	CODAVoucherConfigurationItemForm.prototype.onPost = CODAVoucherConfigurationItemForm.prototype.onCreated = function(data, request){
		this.getParent('KFloatable').closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-voucherconfigurations'
		});
		return;
	};

	CODAVoucherConfigurationItemForm.prototype.onPut = function(data, request){
		this.refresh();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAVoucherConfigurationItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAVoucherConfigurationItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAVoucherConfigurationsDashboard(parent, options, apps) {
	if (parent != null) {
		CODAFormIconDashboard.call(this, parent, options, apps);
	}
}

KSystem.include([
    'KConfirmDialog',
	'CODAIcon',
	'CODAFormIconDashboard'
], function() {
	CODAVoucherConfigurationsDashboard.prototype = new CODAFormIconDashboard();
	CODAVoucherConfigurationsDashboard.prototype.constructor = CODAVoucherConfigurationsDashboard;

	CODAVoucherConfigurationsDashboard.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-voucherconfiguration-wizard'
		});
	};

	CODAVoucherConfigurationsDashboard.prototype.getIconFor = function(data) {

		var dummy = window.document.createElement('p');
		if(!!data.description){
			dummy.innerHTML = data.description;
		}else{
			dummy.innerHTML = '';
		}

		var aditional = '';

		var actions = [];
		actions.push({
			icon : 'css:fa fa-pencil',
			link : '#/edit' + data.href,
			desc : 'Gerir'
		});
		actions.push({
			icon : 'css:fa fa-trash-o',
			desc : 'Remover',
			events : [
			{
				name : 'click',
				callback : CODAVoucherConfigurationsDashboard.remove
			}

			]
		});

		return new CODAIcon(this, {
			icon : !!data.url ? data.url : 'css:fa fa-gift',
			activate : false,
			title : data.name,
			href : data.href,
			description : '<span>' + dummy.textContent + '</span>' + aditional,
			actions : actions
		});
	};

	CODAVoucherConfigurationsDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_VOUCHERCONFIGURATION_CONFIRM'),
			callback : function() {
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onVoucherConfigurationDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400
		});
	};

	CODAVoucherConfigurationsDashboard.prototype.onVoucherConfigurationDelete = function(data, request) {
		this.getParent('KFloatable').closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-voucherconfigurations' + request.service
		});
		return;
	};

	KSystem.included('CODAVoucherConfigurationsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAVoucherListItem(parent) {
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAVoucherListItem.prototype = new CODAListItem();
	CODAVoucherListItem.prototype.constructor = CODAVoucherListItem;

//	CODAVoucherListItem.prototype.remove = function() {
//		var widget = this;
//		KConfirmDialog.confirm({
//			question : gettext('$DELETE_FORM_CONFIRM'),
//			callback : function() {
//				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onFormDelete');
//			},
//			shell : this.shell,
//			modal : true,
//			width : 400,
//			height : 300
//		});
//	};
//
//	CODAVoucherListItem.prototype.onFormDelete = function(data, message) {
//		this.parent.removeChild(this);
//	};

	CODAVoucherListItem.prototype.draw = function() {
//		CODAListItem.prototype.draw.call(this);

		if (!this.isTitle) {
			for (var f in this.parent.table) {
				var h2 = window.document.createElement('h2');
				var value = null;
				eval('value = this.data.' + f + ';')
				if (value != undefined) {
					h2.innerHTML = value;
				}
				this.titleContainer.appendChild(h2);
			}
		}
		else {
			for (var f in this.parent.table) {
				var h2 = window.document.createElement('h2');
				h2.innerHTML = this.parent.table[f].label;
				this.titleContainer.appendChild(h2);
			}
		}

		if (!!this.data.cssClass) {
			this.addCSSClass(this.data.cssClass);
		}

		if (!this.isTitle) {
			if (!!this.orderBy) {
				KDOM.addEventListener(this.panel, 'mouseover', function(e) {
					if (KDraggable.dragging) {
						KDOM.getEventWidget(e).setStyle({
							backgroundColor : 'rgba(0, 146, 191, 0.25)'
						});
					}
				})

				KDOM.addEventListener(this.panel, 'mouseout', function(e) {
					if (KDraggable.dragging) {
						KDOM.getEventWidget(e).setStyle({
							backgroundColor : 'transparent'
						});
					}
				})
			}

//			var owner_infoBt = new KButton(this, '', 'ownerinfo', function(event) {
//				KDOM.stopEvent(event);
//				var widget = KDOM.getEventWidget(event, 'CODAVoucherListItem');
//				if(widget.data.owner_id.search("/participations/") != 1){
//					KBreadcrumb.dispatchURL({
//						hash :  '/view-participation' + widget.data.owner_id
//					});
//				}else if (widget.data.owner_id.search("/accounts/") != 1){
//					KBreadcrumb.dispatchURL({
//						hash :  '/view-account' + widget.data.owner_id
//					});
//				}else{
//					KBreadcrumb.dispatchURL({
//						hash :  'owner-no-info'
//					});
//				}
//			});
//			{
//				owner_infoBt.setText('<i class="fa fa-user"></i>', true);
//				var item_text;
//				if(this.data.owner_id.search("/participations/") != 1){
//					item_text = gettext('$VOUCHER_OWNER_PARTICIPATION');
//				}else if(this.data.owner_id.search("/accounts/") != 1){
//					item_text = gettext('$VOUCHER_OWNER_ACCOUNT');
//				}else{
//					item_text = gettext('$VOUCHER_OWNER_NO_INFO');
//				}
//				var params = {
//						text : item_text + '<div></div>' ,
//						delay : 750,
//						offsetX : 0,
//						offsetY : 20,
//						cssClass : 'CODAListItemButtonBaloon',
//						gravity : {
//							y : 'bottom',
//							x : 'center'
//						}
//					};
//				KBaloon.make(owner_infoBt, params);
//			}
//
//			this.appendChild(owner_infoBt);

			var download_voucherBt = new KButton(this, '', 'downloadvoucher', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAVoucherListItem');
				KBreadcrumb.dispatchURL({
					hash :  '/download-voucher/' + widget.data.id
				});
			});
			{
				download_voucherBt.setText('<i class="fa fa-arrow-circle-o-down"></i> ' + gettext('$VOUCHER_DOWNLOAD_OPTION'), true);
			}

			this.appendChild(download_voucherBt);

			var use_voucherBt = null;
			if(!!this.data.status && this.data.status == 'generated'){
				use_voucherBt = new KButton(this, '', 'usevoucher', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAVoucherListItem');
					KBreadcrumb.dispatchURL({
						hash :  '/use-voucher/' + widget.data.id
					});
					//widget.kinky.post(widget, 'rest:' + '/vouchers/' + widget.data.id + '/usevoucher', null);
				});
				{
					use_voucherBt.setText('<i class="fa fa-flag-o"></i> ' + gettext('$VOUCHER_USE_OPTION'), true);
				}
			}else if (!!this.data.status && this.data.status == 'used'){
				use_voucherBt = new KButton(this, '', 'usevoucher', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAVoucherListItem');
					KBreadcrumb.dispatchURL({
						hash :  '/used-voucher/' + widget.data.id
					});
				});
				{
					use_voucherBt.setText('<i class="fa fa-flag"></i> ' + gettext('$VOUCHER_USED_INFO'), true);
				}
			}else if (!!this.data.status && this.data.status == 'expired'){
				use_voucherBt = new KButton(this, '', 'usevoucher', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAVoucherListItem');
					KBreadcrumb.dispatchURL({
						hash :  '/expired-voucher/' + widget.data.id
					});
				});
				{
					use_voucherBt.setText('<i class="fa fa-warning-sign"></i> ' + gettext('$VOUCHER_EXPIRED_INFO'), true);
				}
			}

			this.appendChild(use_voucherBt);

			KDOM.addEventListener(this.titleContainer, 'click', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.onClick();
			});
		}

		KDropPanel.prototype.draw.call(this);
	};

	CODAVoucherListItem.prototype.onPost = CODAVoucherListItem.prototype.onCreated = function(data, request) {
		/*
		 * TODO ao fazer refresh dá erro na função KWidget.prototype.getWidth. ao fazer o dispatch url dá o mesmo erro - validar porquê
		 */
		CODAVouchersShell.vouchers_list.list.refreshPages();
	};

	/*
	 * TODO não está a usar este callback quando dá precondition failed - validar porquê
	 */
	CODAVoucherListItem.prototype.onPreconditionFailed = function(data, request) {
		CODAVouchersShell.vouchers_list.list.refreshPages();
	};

	KSystem.included('CODAVoucherListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAVouchersDashboard(parent) {
	if (parent != null) {
		var icons = [];

//		icons.push({
//			icon : 'css:fa fa-archive',
//			activate : false,
//			title : gettext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION'),
//			description : '<span>' + gettext('$DASHBOARD_BUTTON_ADD_VOUCHERCONFIGURATION_DESC') + '</span>',
//			actions : [{
//				icon : 'css:fa fa-chevron-right',
//				desc : 'Adicionar',
//				link : '#/add-voucherconfiguration'
//			}]
//		});

		icons.push({
			icon : 'css:fa fa-ticket',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_VOUCHER'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_VOUCHER_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-vouchers'
			}]
		});

		icons.push({
			icon : 'css:fa fa-cog',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_VOUCHERCONFIGURATION_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-voucherconfigurations'
			}]
		});

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAVouchersDashboard.prototype = new CODAMenuDashboard();
	CODAVouchersDashboard.prototype.construtor = CODAVouchersDashboard;

	CODAVouchersDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAVouchersDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAVouchersList (parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, '', { one_choice : false, add_button : false});
		this.pageSize = 5;
		this.table = {
			"id" : {
				label : gettext('$VOUCHERS_LIST_ID')
			},
			"start_date" : {
				label : gettext('$VOUCHERS_LIST_START_DATE')
			},
			"end_date" : {
				label : gettext('$VOUCHERS_LIST_END_DATE')
			},
			"use_date" : {
				label : gettext('$VOUCHERS_LIST_USE_DATE')
			},
			"status" : {
				label : gettext('$VOUCHERS_LIST_STATUS')
			}
		};
		this.LIST_MARGIN = 50;
		this.filterFields = [ 'id' ];
		this.orderBy = 'start_date';
	}
}

KSystem.include([
	'CODAVoucherListItem'
], function() {
	CODAVouchersList.prototype = new CODAList();
	CODAVouchersList.prototype.constructor = CODAVouchersList;

	CODAVouchersList.prototype.addItems = function(list) {

    	for(var index in this.elements) {
    		var listItem = new CODAVoucherListItem(list);
    		listItem.data = this.elements[index];

    		if(this.elements[index].start_date.search("WEST") != -1){
				this.elements[index].start_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].start_date.substring(0,this.elements[index].start_date.length - 5))) + ':00';
			}else{
				this.elements[index].start_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].start_date)) + ':00';
			}
    		if(!!this.elements[index].end_date){
				if(this.elements[index].end_date.search("WEST") != -1){
					this.elements[index].end_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].end_date.substring(0,this.elements[index].end_date.length - 5))) + ':00';
				}else{
					this.elements[index].end_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].end_date)) + ':00';
				}
			}
    		if(!!this.elements[index].use_date){
				if(this.elements[index].use_date.search("WEST") != -1){
					this.elements[index].use_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].use_date.substring(0,this.elements[index].use_date.length - 5))) + ':00';
				}else{
					this.elements[index].use_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].use_date)) + ':00';
				}
			}

    		if (this.elements[index].status == 'expired'){
    			this.elements[index].use_date = '';
    		}

    		list.appendChild(listItem);
    	}

    };

//    CODAVouchersList.prototype.onNoElements = function(data, request) {
//		this.getParent('KFloatable').closeMe();
//		KBreadcrumb.dispatchURL({
//			hash : '/add-form'
//		});
//	};

    KSystem.included('CODAVouchersList');
}, Kinky.CODA_INCLUDER_URL);
function CODAVouchersShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAVouchersShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAVouchersDashboard',
	'CODAVoucherConfigurationItemForm',
	'CODAVoucherConfigurationsDashboard',
	'CODAVouchersList',
	'CODAVoucherListItem'
], function(){
	CODAVouchersShell.prototype = new CODAGenericShell();
	CODAVouchersShell.prototype.constructor = CODAVouchersShell;

	CODAVouchersShell.prototype.loadShell = function() {
		this.draw();
	};

	CODAVouchersShell.prototype.draw = function() {
		this.setTitle(gettext('$VOUCHERCONFIGURATIONS_SHELL_TITLE'));

		this.dashboard = new CODAVouchersDashboard(this);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAVouchersShell.sendRequest = function() {
	};

	CODAVouchersShell.prototype.listVoucherConfigurations = function(href, minimized) {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:/voucherconfigurations', {}, headers, 'onListVoucherConfigurations');
	};

	CODAVouchersShell.prototype.onListVoucherConfigurations = function(data, request) {
		var voucherconfigurationsInfo = new CODAVoucherConfigurationsDashboard(this, { filter_input : true, add_button : true}, (!!data.elements ? data.elements : []));
		voucherconfigurationsInfo.hash = '/voucherconfigurations/hash/';
		this.makeDialog(voucherconfigurationsInfo, { minimized : false, modal : true });
	};

	CODAVouchersShell.prototype.listVouchers = function(href, minimized) {
		CODAVouchersShell.vouchers_list = new CODAVouchersList(this, {
			links : {
				feed : { href : href + '?fields=elements,size'},
			}
		});
		CODAVouchersShell.vouchers_list.setTitle(gettext('$LIST_VOUCHERS_PROPERTIES'));
		this.makeDialog(CODAVouchersShell.vouchers_list, {minimized : minimized, modal : true});

	};

	CODAVouchersShell.prototype.addVoucherConfiguration = function() {
		var configForm = new CODAVoucherConfigurationItemForm(this, null);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAVouchersShell.prototype.addVoucherConfigurationWizard = function() {

		var screens = [ 'CODAVoucherConfigurationItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_VOUCHERCONFIGURATION_PROPERTIES'));
		wiz.hash = '/voucherconfiguration';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			this.getParent('KFloatable').closeMe();
			KBreadcrumb.dispatchURL({
				hash : '/list-voucherconfigurations'
			});
			return;
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + '/voucherconfigurations', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODAVouchersShell.prototype.editVoucherConfiguration = function(href, minimized) {
		var configForm = new CODAVoucherConfigurationItemForm(this, href);
		{
			configForm.hash = '/voucherconfigurations/hash';
			configForm.setTitle(gettext('$EDIT_VOUCHERCONFIGURATION_PROPERTIES'));
		}
		this.makeListForm(configForm, { minimized : false, modal : true});
	};

	CODAVouchersShell.prototype.viewParticipation = function(href, minimized) {
		var configForm = new CODACampaignParticipationItemForm(this, href, null, null);
		{
			configForm.hash = '/campaignparticipation/advanced';
			configForm.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : minimized });
	};

	CODAVouchersShell.prototype.viewAccount = function(href, minimized) {
		alert("m\u00f3dulo de utilizadores finais ainda n\u00e3o implementado");
	};

	CODAVouchersShell.prototype.downloadVoucher = function(href, minimized) {
		//setTimeout(function(){CODAPaymentsShell.payments_list.list.refreshPages();},30000);
		var pup = window.open(this.getShellConfig().providers.rest.data + '/vouchers/' + href + '/pdf?access_token=' + encodeURIComponent(this.getShellConfig().headers.Authorization.replace('OAuth2.0 ', '')), 'Vocuher', 'width=1000,height=800');
		KSystem.popup.check(pup, Kinky.getDefaultShell());
	};

//	CODAVouchersShell.prototype.useVoucher = function(href, minimized) {
//		this.kinky.post(this, 'rest:' + '/vouchers/' + href + '/usevoucher', {}, Kinky.SHELL_CONFIG[this.shell].headers);
//	};

	CODAVouchersShell.prototype.onNoContent = function(data, request) {
		if(request.service.indexOf('/voucherconfigurations') == request.service.length - 22){
			//CODAVouchersShell.prototype.addVoucherConfiguration.call(this);
			CODAVouchersShell.prototype.addVoucherConfigurationWizard.call(this);
		}else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODAVouchersShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]){
			case 'list-voucherconfigurations' : {
				Kinky.getDefaultShell().listVoucherConfigurations('/voucherconfigurations', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'list-vouchers' : {
				Kinky.getDefaultShell().listVouchers('/vouchers', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-voucherconfiguration' : {
				Kinky.getDefaultShell().addVoucherConfiguration();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-voucherconfiguration-wizard' : {
				Kinky.getDefaultShell().addVoucherConfigurationWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit' : {
				if(parts[2] == 'voucherconfigurations'){
					Kinky.getDefaultShell().editVoucherConfiguration('/' + parts[2] + '/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'edit-full' : {
				if(parts[2] == 'voucherconfigurations'){
					Kinky.getDefaultShell().editVoucherConfiguration('/' + parts[2] + '/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'view-participation' : {
				Kinky.getDefaultShell().viewParticipation('/' + parts[2] + '/' + parts[3], true);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'view-account' : {
				Kinky.getDefaultShell().viewAccount('/' + parts[2] + '/' + parts[3], true);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'download-voucher' : {
				Kinky.getDefaultShell().downloadVoucher(parts[2], true);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
//			case 'use-voucher' : {
//				Kinky.getDefaultShell().useVoucher(parts[2], true);
//				KBreadcrumb.dispatchURL({
//					hash : ''
//				});
//				break;
//			}
		}

	};

	KSystem.included('CODAVouchersShell');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

