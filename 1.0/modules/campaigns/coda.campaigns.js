KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODACampaignItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = '/campaigns';
		this.single = true;
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText',
	'KStatic'
], function() {
	CODACampaignItemForm.prototype = new CODAForm();
	CODACampaignItemForm.prototype.constructor = CODACampaignItemForm;

	CODACampaignItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACampaignItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

//		if (this.canEditAdvanced) {
//			this.scopes_p = new CODAUsersManageScopesItemForm(this, this.target);
//			this.addPanel(this.scopes_p, gettext('$RESOURCE_PERMISSIONS'), this.nPanels == 0);
//		}
	};

	CODACampaignItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_CAMPAIGN_PROPERTIES') + ' \u00ab' + this.target.data.name + '\u00bb');
		}

		var campaignItem = new KPanel(this);
		{
			campaignItem.hash = '/general';

			var application = new KStatic(campaignItem, gettext('$CAMPAIGN_ITEM_FORM_APPLICATION'), 'application');
			{
				if (!!this.data && !!this.data.application) {
					application.setValue(this.data.application);
				}
				else {
					application.setValue(this.getShell().data.app_id);
				}
				application.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			campaignItem.appendChild(application);

			var client = new KStatic(campaignItem, gettext('$CAMPAIGN_ITEM_FORM_CLIENT'), 'client');
			{
				client.setValue(this.getShell().app_data.name);
			}
			campaignItem.appendChild(client);

			var name = new KInput(campaignItem, gettext('$CAMPAIGN_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$CAMPAIGN_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			campaignItem.appendChild(name);

			var description = new KTextArea(campaignItem, gettext('$CAMPAIGN_ITEM_FORM_DESCRIPTION'), 'description');
			{
				if (!!this.data && !!this.data.description){
					description.setValue(this.data.description);
				}
			}
			description.setHelp(gettext('$CAMPAIGN_ITEM_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());

			campaignItem.appendChild(description);

		}

		this.addPanel(campaignItem, gettext('$FORM_GENERALINFO_PANEL'), true);
	};

	CODACampaignItemForm.prototype.setDefaults = function(params){
		if(!!params.requestTypes){
			delete params.requestTypes;
		}

		if(!!params.responseTypes){
			delete params.responseTypes;
		}

		params.type = !!this.getShell().app_data.type ? this.getShell().app_data.type : 'app';
		params.status = 'active';
	};

	CODACampaignItemForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACampaignItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACampaignItemForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_CAMPAIGN_NOT_FOUND'));
	};

	CODACampaignItemForm.prototype.onPost = CODACampaignItemForm.prototype.onCreated = function(data, request){
		window.document.location.reload(true);
	};

//	CODACampaignItemForm.prototype.onPost = CODACampaignItemForm.prototype.onCreated = function(data, request){
//		this.getShell().dashboard.campaignList.list.refreshPages();
//		KBreadcrumb.dispatchURL({
//			hash : '/edit' + data.http.headers.Location
//		});
//	};

	CODACampaignItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODACampaignItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignPhaseItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = parent.active_campaign_href + '/phases';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText',
	'KCalendar'
], function() {
	CODACampaignPhaseItemForm.prototype = new CODAForm();
	CODACampaignPhaseItemForm.prototype.constructor = CODACampaignPhaseItemForm;

	CODACampaignPhaseItemForm.prototype.load = function() {

		if(!!this.getShell().app_data && !!this.getShell().app_data && !!this.getShell().app_data.type){
			if(this.getShell().app_data.type == 'redpanda-content-offer'){
				this.kinky.get(this, 'rest:/forms', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadForms');
			}else{
				CODAForm.prototype.load.call(this);
			}
		}
		else{
			CODAForm.prototype.load.call(this);
		}

	};

	CODACampaignPhaseItemForm.prototype.onLoadForms = function(data, request) {

		if(!!data && !!data.elements && !!data.elements.length > 0){
			this.forms_for_phases = data.elements;
		}

		CODAForm.prototype.load.call(this);
	};


	CODACampaignPhaseItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACampaignPhaseItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

//		if (this.canEditAdvanced) {
//			this.scopes_p = new CODAUsersManageScopesItemForm(this, this.target);
//			this.addPanel(this.scopes_p, gettext('$RESOURCE_PERMISSIONS'), this.nPanels == 0);
//		}
	};

	CODACampaignPhaseItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$ADD_CAMPAIGNPHASE_PROPERTIES') + ' \u00ab' + this.target.data.name + '\u00bb');
		}else{
			this.setTitle('Inserir ' + gettext('$ADD_CAMPAIGNPHASE_PROPERTIES'));
		}

		var campaign_phase_item = new KPanel(this);
		{
			campaign_phase_item.hash = '/general';

			var name = new KInput(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			campaign_phase_item.appendChild(name);

			var description = new KTextArea(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION'), 'description');
			{
				if (!!this.data && !!this.data.description){
					description.setValue(this.data.description);
				}
			}
			description.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());

			campaign_phase_item.appendChild(description);

			var start_date = new KDate(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE') + ' *', 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if(!!this.data && !!this.data.start_date){
					start_date.setValue(new Date(this.data.start_date));
				}
				else {
					start_date.setValue(new Date());
				}
				start_date.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
				start_date.hash = '/start-date';
			}
			campaign_phase_item.appendChild(start_date);

			var end_date = new KDate(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE') + ' *', 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if(!!this.data && !!this.data.end_date){
					end_date.setValue(new Date(this.data.end_date));
				}
				else {
					end_date.setValue(new Date((new Date()).getTime() + 3600000 * 24 * 31));
				}
				end_date.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
				end_date.addValidator('>', gettext('$ERROR_DATE_MUST_BE_GREATER_THAN_START_DATE'), { comparison : start_date });
				end_date.hash = '/end-date';
			}
			campaign_phase_item.appendChild(end_date);

			var participations_number = new KInput(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER'), 'allowed_participations');
			{
				if(!!this.data && !!this.data.allowed_participations){
					participations_number.setValue(this.data.allowed_participations);
				}
				participations_number.setLength(8);
				participations_number.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				participations_number.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER_HELP'), CODAForm.getHelpOptions());
			}

			campaign_phase_item.appendChild(participations_number);

			var prizes_method_selector = new KCombo(campaign_phase_item, gettext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD') + ' *', 'prizes_method');
			{
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_NAME'), (!!this.data && !!this.data.prizes_method && this.data.prizes_method == gettext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_CODE')) ? true : false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_ALL_NO_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_ALL_NO_REPETITIONS_NAME'), (!!this.data && !!this.data.prizes_method && this.data.prizes_method == gettext('$PRIZES_METHOD_ALL_NO_REPETITIONS_CODE')) ? true : false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_NAME'), (!!this.data && !!this.data.prizes_method && this.data.prizes_method == gettext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_CODE')) ? true : false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_NAME'), (!!this.data && !!this.data.prizes_method && this.data.prizes_method == gettext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_CODE')) ? true : false);
				prizes_method_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				prizes_method_selector.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP'), CODAForm.getHelpOptions());
			}
			campaign_phase_item.appendChild(prizes_method_selector);

			if(!!this.getShell().app_data && !!this.getShell().app_data && !!this.getShell().app_data.type){
				if(this.getShell().app_data.type == 'redpanda-content-offer'){
					if (!!this.forms_for_phases){
						var form_href = new KCombo(campaign_phase_item, gettext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM'), 'form_href');
						{
							form_href.addOption(null, '-', true);
							for (var index in this.forms_for_phases){
								form_href.addOption(this.forms_for_phases[index].href, this.forms_for_phases[index].name, (!!this.data && !!this.data.form_href && this.data.form_href == this.forms_for_phases[index].href) ? true : false);
							}
						}
						form_href.setHelp(gettext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM_HELP'), CODAForm.getHelpOptions());
						campaign_phase_item.appendChild(form_href);
					}
				}
			}
		}

		this.addPanel(campaign_phase_item, gettext('$FORM_GENERALINFO_PANEL'), true);

	};
	
	CODACampaignPhaseItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.title = gettext("$CAMPAIGN_PHASE_WIZ_PANEL_TITLE_" + parent.className.toUpperCase());
			firstPanel.description = gettext("$CAMPAIGN_PHASE_WIZ_PANEL_DESC_" + parent.className.toUpperCase());
			firstPanel.icon = 'css:fa fa-calendar';

			var name = undefined;
			if (parent.className == 'CODAWizard') {
				name = new KInput(firstPanel, gettext('$CAMPAIGN_PHASE_ITEM_FORM_NAME') + ' *', 'name');
				name.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
			}
			else {
				name = new KHidden(firstPanel, 'name');
				name.setValue(gettext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_DEFAULT_VALUE'));
			}
			{
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(name);

			var start_date = new KDate(firstPanel, gettext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE') + ' *', 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			start_date.setValue(new Date());
			start_date.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			start_date.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
			start_date.hash = '/start-date';
			firstPanel.appendChild(start_date);
			
			var end_date = new KDate(firstPanel, gettext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE') + ' *', 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			end_date.setValue(new Date((new Date()).getTime() + 3600000 * 24 * 31));
			end_date.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			end_date.addValidator('>', gettext('$ERROR_DATE_MUST_BE_GREATER_THAN_START_DATE'), { comparison : start_date });
			end_date.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
			end_date.hash = '/end-date';
			firstPanel.appendChild(end_date);

			var participations_number = new KInput(firstPanel, gettext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER'), 'allowed_participations');
			{
				participations_number.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER_HELP'), CODAForm.getHelpOptions());
				participations_number.setLength(8);
				participations_number.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
			}
			firstPanel.appendChild(participations_number);

			var prizes_method_selector = new KCombo(firstPanel, gettext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD') + ' *', 'prizes_method');
			{
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_NAME'), false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_ALL_NO_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_ALL_NO_REPETITIONS_NAME'), false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_NAME'), false);
				prizes_method_selector.addOption(gettext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_CODE'), gettext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_NAME'), false);
				prizes_method_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				prizes_method_selector.setHelp(gettext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(prizes_method_selector);
		}

		return [firstPanel];
	};
	
	CODACampaignPhaseItemForm.prototype.setDefaults = function(params) {
		//params.status = 'inactive';
		params.status = 'active';

		if(!!params.requestTypes){
			delete params.requestTypes;
		}

		if(!!params.responseTypes){
			delete params.responseTypes;
		}
	};


	CODACampaignPhaseItemForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACampaignPhaseItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODACampaignPhaseItemForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_CAMPAIGN_NOT_FOUND'));
	};

	CODACampaignPhaseItemForm.prototype.onPost = CODACampaignPhaseItemForm.prototype.onCreated = function(data, request){
		var update_campaign_here = true;
		if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
			CODACampaignsShell.phases_list.list.refreshPages();
			if(!CODACampaignsShell.phases_list.elements){
				update_campaign_here = false;
				window.document.location.reload(true);
			}else{
				this.enable();
				this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
			}
		}else if(!!CODACampaignItemForm && !!CODACampaignItemForm.phasesList && !!CODACampaignItemForm.phasesList.list){
			CODACampaignItemForm.phasesList.list.refreshPages();
			if(!CODACampaignItemForm.phasesList.elements){
				update_campaign_here = false;
				window.document.location.reload(true);
			}else{
				this.enable();
				this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
			}
		}
		if(update_campaign_here == true){
			//this.parent.closeMe();
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.get(this, 'rest:' + this.getShell().active_campaign_href + '/phases?orderBy=+start_date', {}, headers, 'onNewCampaignPhase');
		}
	};

	CODACampaignPhaseItemForm.prototype.onNewCampaignPhase = function(data, request){
		if(!!data.elements){
			this.getShell().all_campaign_phases = data.elements;
			var actual_phase = CODACampaignsShell.prototype.findActualPhase(data.elements);
			if(actual_phase){
				this.getShell().active_phase_href = actual_phase;
			}
		}
	};

	CODACampaignPhaseItemForm.prototype.onPut = function(data, request){
		if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
			CODACampaignsShell.phases_list.list.refreshPages();
		}else if(!!CODACampaignItemForm && !!CODACampaignItemForm.phasesList && !!CODACampaignItemForm.phasesList.list){
			CODACampaignItemForm.phasesList.list.refreshPages();
		}
//		this.parent.closeMe();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODACampaignPhaseItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODACampaignPhaseItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignPhasePrizeItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = this.getShell().active_phase_href + '/prizes';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText',
	'KCalendar'
], function() {
	CODACampaignPhasePrizeItemForm.prototype = new CODAForm();
	CODACampaignPhasePrizeItemForm.prototype.constructor = CODACampaignPhasePrizeItemForm;

	CODACampaignPhasePrizeItemForm.prototype.visible = function() {
		CODAForm.prototype.visible.call(this);
		if (!!this.prize_type) {
			this.prize_selector.setValue(this.prize_type);
		}
	};

	CODACampaignPhasePrizeItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACampaignPhasePrizeItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

	};

	CODACampaignPhasePrizeItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_CAMPAIGNPHASEPRIZE_PROPERTIES') + ' \u00ab' + this.target.data.name + '\u00bb');
		}else{
			this.setTitle('Inserir ' + gettext('$EDIT_CAMPAIGNPHASEPRIZE_PROPERTIES'));
		}

		var prize_general_data = new KPanel(this);
		{

			prize_general_data.hash = '/generaldata';
			if(!!this.getShell().all_campaign_phases){
				var campaign_phases = this.getShell().all_campaign_phases;
				
				//não se pode alterar a fase de um prémio porque o formato do href do prémio é /campaigns/<campaign_id>/phases/<phase_id>/prizes/<prize_id>
				var prize_phase = null;
				if (!!this.target.href) {
					
					prize_phase = new KStatic(prize_general_data, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE'), 'phase_name');
					{
						if (!!this.data && !!this.data.phase_id){
							for (var index in campaign_phases){
								if(campaign_phases[index].id == this.data.phase_id){
									prize_phase.setValue(campaign_phases[index].name);
									break;
								}
							}
						}
					}

				}else{

					if(this.getShell().all_campaign_phases.length == 1){

						prize_phase = new KStatic(prize_general_data, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE'), 'phase_name');
						var prize_phase_id = new KHidden(prize_general_data, 'phase_id');
						{
							prize_phase.setValue(campaign_phases[0].name);
							prize_phase_id.setValue(campaign_phases[0].id);
						}

					}else if(this.getShell().all_campaign_phases.length > 1){

						prize_phase = new KCombo(prize_general_data, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE') + ' *', 'phase_id');
						{
							for (var index in campaign_phases){
								prize_phase.addOption(campaign_phases[index].id, campaign_phases[index].name, false);
							}
							prize_phase.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
						}
					}
				}
				prize_phase.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE_HELP'), CODAForm.getHelpOptions());
				prize_general_data.appendChild(prize_phase);
			}

			var name = new KInput(prize_general_data, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.isLocalized = true;
				name.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			prize_general_data.appendChild(name);

			var description = new KTextArea(prize_general_data, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION'), 'description');
			{
				if (!!this.data && !!this.data.description){
					description.setValue(this.data.description);
				}
			}
			description.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());

			prize_general_data.appendChild(description);

		}

		this.addPanel(prize_general_data, gettext('$FORM_GENERALINFO_PANEL'), true);

		var prize_images_panel = new KPanel(this);
		{
			prize_images_panel.hash = '/images';
			prize_images_panel.addCSSClass('CODAFormImageGallery');

			//var prize_url = new CODACampaignKLinkFile(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			var prize_url = new KImageUpload(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.url) {
					prize_url.setValue(this.data.url);
				}
				var mediaList = new CODAMediaList(prize_url, 'image/*');
				prize_url.appendChild(mediaList);
				prize_url.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL_HELP'), CODAForm.getHelpOptions());
			}
			prize_images_panel.appendChild(prize_url);

			var prize_thumbnail_1 = new KImageUpload(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1'), 'thumbnail_1', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_1) {
					prize_thumbnail_1.setValue(this.data.thumbnail_1);
				}
				var mediaList = new CODAMediaList(prize_thumbnail_1, 'image/*');
				prize_thumbnail_1.appendChild(mediaList);
				prize_thumbnail_1.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());
			}
			
			prize_images_panel.appendChild(prize_thumbnail_1);

			var prize_thumbnail_2 = new KImageUpload(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2'), 'thumbnail_2', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_2) {
					prize_thumbnail_2.setValue(this.data.thumbnail_2);
				}
				var mediaList = new CODAMediaList(prize_thumbnail_2, 'image/*');
				prize_thumbnail_2.appendChild(mediaList);
				prize_thumbnail_2.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());
			}
			prize_images_panel.appendChild(prize_thumbnail_2);

			var prize_thumbnail_3 = new KImageUpload(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3'), 'thumbnail_3', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_3) {
					prize_thumbnail_3.setValue(this.data.thumbnail_3);
				}
				var mediaList = new CODAMediaList(prize_thumbnail_3, 'image/*');
				prize_thumbnail_3.appendChild(mediaList);
				prize_thumbnail_3.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());
			}
			prize_images_panel.appendChild(prize_thumbnail_3);

			var prize_thumbnail_4 = new KImageUpload(prize_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4'), 'thumbnail_4', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_4) {
					prize_thumbnail_4.setValue(this.data.thumbnail_4);
				}
				var mediaList = new CODAMediaList(prize_thumbnail_4, 'image/*');
				prize_thumbnail_4.appendChild(mediaList);
				prize_thumbnail_4.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());
			}
			prize_images_panel.appendChild(prize_thumbnail_4);

		}
		this.addPanel(prize_images_panel, gettext('$FORM_PRIZES_IMAGES_PANEL'), false);

		var prize_assignment_panel = new KPanel(this);
		{
			prize_assignment_panel.hash = '/assignment';
			var prize_value = new KInput(prize_assignment_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE'), 'value');
			{
				if(!!this.data && !!this.data.value){
					prize_value.setValue(this.data.value);
				}
				prize_value.setLength(8);
				prize_value.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE_HELP'), CODAForm.getHelpOptions());
				prize_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			prize_assignment_panel.appendChild(prize_value);

			var prize_stock = new KInput(prize_assignment_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK'), 'stock');
			{
				if(!!this.data && (!!this.data.stock || this.data.stock == 0)){
					if (this.data.stock == 0){
						prize_stock.setValue("0");
					}else{
						prize_stock.setValue(this.data.stock);	
					}					
				}
				prize_stock.setLength(8);
				prize_stock.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK_HELP'), CODAForm.getHelpOptions());
				prize_stock.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
			}
			prize_assignment_panel.appendChild(prize_stock);

			var prize_type_selector = new KCombo(prize_assignment_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE'), 'prize_type');
			{
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_PHYSICAL_CODE'), gettext('$PRIZE_TYPE_PHYSICAL_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_PHYSICAL_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_IMAGE_CODE'), gettext('$PRIZE_TYPE_IMAGE_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_IMAGE_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE'), gettext('$PRIZE_TYPE_TEXT_PLAIN_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_TEXT_HTML_CODE'), gettext('$PRIZE_TYPE_TEXT_HTML_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_HTML_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VIDEO_CODE'), gettext('$PRIZE_TYPE_VIDEO_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_VIDEO_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VIMEO_CODE'), gettext('$PRIZE_TYPE_VIMEO_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_VIMEO_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'), gettext('$PRIZE_TYPE_VOUCHER_PDF_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'));
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_YOUTUBE_CODE'), gettext('$PRIZE_TYPE_YOUTUBE_NAME'), !!this.data.digital_mime_type && this.data.digital_mime_type == gettext('$PRIZE_TYPE_YOUTUBE_CODE'));
				prize_type_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				prize_type_selector.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE_HELP'), CODAForm.getHelpOptions());

				prize_type_selector.addEventListener('change', CODACampaignPhasePrizeItemForm.onPrizeTypeSelected);
			}
			prize_assignment_panel.appendChild(prize_type_selector);


			prize_type_selector.visible = function() {
				if (this.display) {
					return;
				}
				KCombo.prototype.visible.call(this);
				KDOM.fireEvent(this.input, 'change');
			};

			this.prize_selector = prize_type_selector;
			this.prize_type = null;
			if(!!this.data && !!this.data.digital_mime_type){
				if (this.data.digital_mime_type == 'image/jpeg' || this.data.digital_mime_type == 'image/gif; charset=binary' || this.data.digital_mime_type == 'image/png; charset=binary' || this.data.digital_mime_type == 'image/jpeg; charset=binary' || this.data.digital_mime_type == 'image/svg; charset=binary' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_IMAGE_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_IMAGE_CODE');
				}else if(this.data.digital_mime_type == 'text/plain; charset=utf-8' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE');
				}else if(this.data.digital_mime_type == 'text/html; charset=utf-8' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_HTML_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_TEXT_HTML_CODE');
				}else if(this.data.digital_mime_type == 'video/webm; charset=binary' || this.data.digital_mime_type == 'video/ogg; charset=binary' || this.data.digital_mime_type == 'video/mp4; charset=binary' || this.data.digital_mime_type == 'video/flv; charset=binary' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_VIDEO_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_VIDEO_CODE');
				}else if(this.data.digital_mime_type == 'embed/vimeo; charset=binary' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_VIMEO_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_VIMEO_CODE');
				}else if(this.data.digital_mime_type == 'application/pdf; charset=binary' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE');
				}else if(this.data.digital_mime_type == 'embed/youtube; charset=binary' || this.data.digital_mime_type == gettext('$PRIZE_TYPE_YOUTUBE_CODE')){
					this.prize_type = gettext('$PRIZE_TYPE_YOUTUBE_CODE');
				}
				else {
					this.prize_type = gettext('$PRIZE_TYPE_PHYSICAL_CODE');
				}
			}
			else {
				this.prize_type = gettext('$PRIZE_TYPE_PHYSICAL_CODE');
			}
		}
		this.addPanel(prize_assignment_panel, gettext('$FORM_PRIZES_ASSIGNMENT_PANEL'), false);
	};

	CODACampaignPhasePrizeItemForm.onPrizeTypeSelected = function(event){
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);

		if (!!widget.parent.childWidget('/digital-url')) {
			widget.parent.removeChild(widget.parent.childWidget('/digital-url'));
		}
		if (!!widget.parent.childWidget('/expiration-days')) {
			widget.parent.removeChild(widget.parent.childWidget('/expiration-days'));
		}
		if (!!widget.parent.childWidget('/start-date')) {
			widget.parent.removeChild(widget.parent.childWidget('/start-date'));
		}
		if (!!widget.parent.childWidget('/end-date')) {
			widget.parent.removeChild(widget.parent.childWidget('/end-date'));
		}

		if(widget.getValue() != '' || (!!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type)){
			if(widget.getValue() == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
				//widget.setValue('');
				//widget.parent.parent.kinky.get(widget.parent.parent, 'rest:/voucherconfigurations', {}, Kinky.SHELL_CONFIG[widget.shell].headers, 'onRetrieveVouchers');
				//return;
				var expiration_days = new KInput(widget.parent, gettext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS'), 'expiration_days');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.expiration_days) {
						expiration_days.setValue(widget.parent.parent.data.expiration_days);
					};
					expiration_days.setLength(8);
					expiration_days.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
					expiration_days.hash = '/expiration-days';
					expiration_days.setHelp(gettext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS_HELP'), CODAForm.getHelpOptions());
					widget.parent.appendChild(expiration_days);
				}
				var start_date = new KDate(widget.parent, gettext('$PRIZE_TYPE_VOUCHER_START_DATE'), 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.start_date) {
						start_date.setValue(widget.parent.parent.data.start_date);
					};
					start_date.setHelp(gettext('$PRIZE_TYPE_VOUCHER_START_DATE_HELP'), CODAForm.getHelpOptions());
					start_date.hash = '/start-date';
					widget.parent.appendChild(start_date);
				}
				var end_date = new KDate(widget.parent, gettext('$PRIZE_TYPE_VOUCHER_END_DATE'), 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.end_date) {
						end_date.setValue(widget.parent.parent.data.end_date);
					};
					end_date.setHelp(gettext('$PRIZE_TYPE_VOUCHER_END_DATE_HELP'), CODAForm.getHelpOptions());
					end_date.hash = '/end-date';
					widget.parent.appendChild(end_date);
				}
			}

			var prize_digital_url = null;
			if(widget.getValue() == gettext('$PRIZE_TYPE_IMAGE_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_IMAGE_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_IMAGE_CODE'));
				}
				prize_digital_url = new KImageUpload(widget.parent, gettext('$PRIZE_TYPE_IMAGE_NAME'), 'digital_url', CODAGenericShell.getUploadAuthorization());
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_IMAGE_CODE');
					}
				}

			}
			else if(widget.getValue() == gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE'));
				}
				prize_digital_url = new KTextArea(widget.parent, gettext('$PRIZE_TYPE_TEXT_PLAIN_NAME'), 'digital_url');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE');
					}
				}

			}else if(widget.getValue() == gettext('$PRIZE_TYPE_TEXT_HTML_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_TEXT_HTML_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_TEXT_HTML_CODE'));
				}
				prize_digital_url = new KRichText(widget.parent, gettext('$PRIZE_TYPE_TEXT_HTML_NAME'), 'digital_url');
//				prize_digital_url = new KTextArea(widget.parent, gettext('$PRIZE_TYPE_TEXT_HTML_NAME'), 'digital_url');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_TEXT_HTML_CODE');
					}
				}
			}else if(widget.getValue() == gettext('$PRIZE_TYPE_VIDEO_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_VIDEO_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_VIDEO_CODE'));
				}
				prize_digital_url = new KInput(widget.parent, gettext('$PRIZE_TYPE_VIDEO_NAME'), 'digital_url');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_VIDEO_CODE');
					}
					var mediaList = new CODAMediaList(prize_digital_url);
					prize_digital_url.appendChild(mediaList);
				}
			}else if(widget.getValue() == gettext('$PRIZE_TYPE_YOUTUBE_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_YOUTUBE_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_YOUTUBE_CODE'));
				}
				prize_digital_url = new KInput(widget.parent, 'Embed do ' + gettext('$PRIZE_TYPE_YOUTUBE_NAME'), 'digital_url');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_YOUTUBE_CODE');
					}
				}
			}else if(widget.getValue() == gettext('$PRIZE_TYPE_VIMEO_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_VIMEO_CODE'))){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_VIMEO_CODE'));
				}
				prize_digital_url = new KInput(widget.parent, 'Embed do ' + gettext('$PRIZE_TYPE_VIMEO_NAME'), 'digital_url');
				{
					if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_url) {
						prize_digital_url.setValue(widget.parent.parent.data.digital_url);
					};
					if(!!widget.parent.parent.data){
						widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_VIMEO_CODE');
					}
				}
			}else if(widget.getValue() == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE') || (widget.getValue() == '' && !!widget.parent.parent.data && !!widget.parent.parent.data.digital_mime_type && widget.parent.parent.data.digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'))){
//				if(widget.getValue() == ''){
//					widget.setValue(gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'));
//				}
//				if(!!widget.parent.parent.vouchers_configurations && widget.parent.parent.vouchers_configurations.elements.length > 0){
//
//					prize_digital_url = new KCombo(widget.parent, gettext('$PRIZE_TYPE_VOUCHER_PDF_NAME'), 'generated_url');
//					{
//						var voucher_configuration_code;
//						for( var index in widget.parent.parent.vouchers_configurations.elements){
//							voucher_configuration_code = '/vouchers?voucherConfiguration_id=' + widget.parent.parent.vouchers_configurations.elements[index].id;
//							prize_digital_url.addOption(voucher_configuration_code, widget.parent.parent.vouchers_configurations.elements[index].name, !!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.generated_url && widget.parent.parent.data.generated_url == voucher_configuration_code ? true : false);
//						}
						if(!!widget.parent.parent.data){
							widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE');
							widget.parent.parent.data.generated_url = "/vouchers";
						}
//					}
//				}else{
//					prize_digital_url = new KStatic(widget.parent, 'Embed do ' + gettext('$PRIZE_TYPE_VOUCHER_PDF_NAME'), 'generated_url_message');
//					prize_digital_url.setValue(gettext('$PRIZE_TYPE_VOUCHER_PDF_MESSAGE'));
//				}
			}else if(widget.getValue() == gettext('$PRIZE_TYPE_PHYSICAL_CODE')){
				if(widget.getValue() == ''){
					widget.setValue(gettext('$PRIZE_TYPE_PHYSICAL_CODE'));
				}
				if(!!widget.parent.parent.data){
					widget.parent.parent.data.digital_mime_type = gettext('$PRIZE_TYPE_PHYSICAL_CODE');
				}
			}

			if(prize_digital_url != null){
				prize_digital_url.hash = '/digital-url';
			}

//			prize_digital_url.setStyle({
//				position: 'absolute',
//				left: style.left,
//				top: '158px',
//				minWidth : style.minWidth,
//				maxWidth : style.maxWidth
//			});
			widget.parent.appendChild(prize_digital_url);

		}
	};

	CODACampaignPhasePrizeItemForm.onPrizeTypeSelectedWizard = function(event){
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);

		if(widget.getValue() != ''){
			if(widget.getValue() == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
				var generated_url = new KHidden(widget.parent, 'generated_url');
				generated_url.setValue("/vouchers");
				widget.parent.appendChild(generated_url);
			}
		}
		
	};
	
//	CODACampaignPhasePrizeItemForm.prototype.onRetrieveVouchers = function(data, request) {
//		this.vouchers_configurations = data;
//		this.prize_selector.setValue(gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'));
//	};

	CODACampaignPhasePrizeItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.title = gettext("$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_TITLE");
			firstPanel.description = gettext("$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_DESC");
			firstPanel.icon = 'css:fa fa-gift';

			if(!!parent.getShell().all_campaign_phases){
				var campaign_phases = parent.getShell().all_campaign_phases;

//				var prize_phases_selector = new KCombo(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE') + ' *', 'phase_id');
//				{
//					for (var index in campaign_phases){
//						prize_phases_selector.addOption(campaign_phases[index].id, campaign_phases[index].name, false);
//					}
//					prize_phases_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
//					prize_phases_selector.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE_HELP'), CODAForm.getHelpOptions());
//				}
				
				prize_phase = new KStatic(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE'), 'phase_name');
				var prize_phase_id = new KHidden(firstPanel, 'phase_id');
				{
					prize_phase.setValue(campaign_phases[0].name);
					prize_phase_id.setValue(campaign_phases[0].id);
				}				

				firstPanel.appendChild(prize_phase);
			}

			var name = new KInput(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME') + ' *', 'name');
			{
				name.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(name);

			var prize_value = new KInput(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE'), 'value');
			{
				prize_value.setLength(8);
				prize_value.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE_HELP'), CODAForm.getHelpOptions());
				prize_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			firstPanel.appendChild(prize_value);

			var prize_stock = new KInput(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK'), 'stock');
			{
				prize_stock.setLength(8);
				prize_stock.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK_HELP'), CODAForm.getHelpOptions());
				prize_stock.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				prize_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			firstPanel.appendChild(prize_stock);

			var prize_url = new KImageUpload(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				var mediaList = new CODAMediaList(prize_url, 'image/*');
				prize_url.appendChild(mediaList);
				prize_url.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(prize_url);

			var prize_type_selector = new KCombo(firstPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE') + '*', 'digital_mime_type');
			{
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_PHYSICAL_CODE'), gettext('$PRIZE_TYPE_PHYSICAL_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_IMAGE_CODE'), gettext('$PRIZE_TYPE_IMAGE_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_TEXT_PLAIN_CODE'), gettext('$PRIZE_TYPE_TEXT_PLAIN_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_TEXT_HTML_CODE'), gettext('$PRIZE_TYPE_TEXT_HTML_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VIDEO_CODE'), gettext('$PRIZE_TYPE_VIDEO_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VIMEO_CODE'), gettext('$PRIZE_TYPE_VIMEO_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE'), gettext('$PRIZE_TYPE_VOUCHER_PDF_NAME'), false);
				prize_type_selector.addOption(gettext('$PRIZE_TYPE_YOUTUBE_CODE'), gettext('$PRIZE_TYPE_YOUTUBE_NAME'), false);
				prize_type_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				prize_type_selector.setHelp(gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE_HELP'), CODAForm.getHelpOptions());

				prize_type_selector.addEventListener('change', CODACampaignPhasePrizeItemForm.onPrizeTypeSelectedWizard);
				prize_type_selector.visible = function() {
					KCombo.prototype.visible.call(this);
					KDOM.fireEvent(this.combo, 'propertychange');
				};
			}
			firstPanel.appendChild(prize_type_selector);

		}

		return [firstPanel];
	};

	CODACampaignPhasePrizeItemForm.prototype.setDefaults = function(params) {
		if(!!params.phase_id && this.target.collection.search('/prizes') == -1){
			this.target.collection = this.target.collection + '/' + params.phase_id + '/prizes';
		}

		if (!!params.digital_mime_type && params.digital_mime_type != gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
			delete params.generated_url;
		}
		
//		if(!!params.digital_url && !!params.digital_mime_type && params.digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
//			delete params.digital_url;
//		}

		if(!!params.prize_type){
			delete params.prize_type;
		}

		if(!params.value || params.value == ''){
			delete params.value;
		}

		delete params.requestTypes;
		delete params.responseTypes;
	};

	CODACampaignPhasePrizeItemForm.prototype.onPost = CODACampaignPhasePrizeItemForm.prototype.onCreated = function(data, request){
		if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
			CODACampaignsShell.phases_list.list.refreshPages();
		}else if(!!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list.list){
			CODACampaignPhasePrizesPanel.prizes_list.list.refreshPages();
		}else{
			this.getParent('KFloatable').closeMe();
			KBreadcrumb.dispatchURL({
				hash : '/campaign-prizes'
			});
			return;
		}
		//this.getParent('KFloatable').closeMe();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODACampaignPhasePrizeItemForm.prototype.onPut = function(data, request){
		if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
			CODACampaignsShell.phases_list.list.refreshPages();
		}else if(!!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list.list){
			CODACampaignPhasePrizesPanel.prizes_list.list.refreshPages();
		}
		this.enable();
		this.refresh();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODACampaignPhasePrizeItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODACampaignPhasePrizeItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignPhasePrizesListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
		this.config.headers = {};
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox'
], function() {
	CODACampaignPhasePrizesListItem.prototype = new CODAListItem();
	CODACampaignPhasePrizesListItem.prototype.constructor = CODACampaignPhasePrizesListItem;

	CODACampaignPhasePrizesListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PRIZE_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onPrizeDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODACampaignPhasePrizesListItem.prototype.onPrizeDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	KSystem.included('CODACampaignPhasePrizesListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignPhasePrizesList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, { one_choice : true, add_button : true});
		this.pageSize = 5;
		this.table = {
			"name" : {
				label : gettext('$CAMPAIGNS_LIST_NAME')
			},
			"value" : {
				label : gettext('$CAMPAIGNS_LIST_VALUE')
			},
			"stock" : {
				label : gettext('$CAMPAIGNS_LIST_STOCK')
			}
		};
		this.filterFields = [ 'name' ];
		this.LIST_MARGIN = 285;
		this.orderBy = 'name';

		var actualPhaseSplit = this.getShell().active_phase_href.split('/');
		var actualPhaseID = actualPhaseSplit[actualPhaseSplit.length - 1];

		this.phaseContext = new CODAContextList(this, {
			links : {
				feed : {
					href : this.getShell().active_campaign_href + '/phases'
				}
			},
			uiDetails : {
				initialElementSelected : actualPhaseID,
				timeContext : {
					Prev : gettext('$PARTICIPATIONS_CLIST_PREVIOUS_PHASE'),
					Actual : gettext('$PARTICIPATIONS_CLIST_ACTUAL_PHASE'),
					Next : gettext('$PARTICIPATIONS_CLIST_NEXT_PHASE')
				},
				dataToShow : {
					name : gettext('$PARTICIPATIONS_CLIST_NAME'),
					end_date : gettext('$PARTICIPATIONS_CLIST_END_DATE'),
					start_date : gettext('$PARTICIPATIONS_CLIST_START_DATE')
				}
			},
			callback : function(value) {
				CODACampaignsShell.prizes_list.data.links.feed.href = '/phases/' + value + '/prizes?access=dbfields=elements,size';
				CODACampaignsShell.prizes_list.list.refreshPages();
			}
		});

	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODACampaignPhasePrizesList.prototype = new CODAList();
	CODACampaignPhasePrizesList.prototype.constructor = CODACampaignPhasePrizesList;

	CODACampaignPhasePrizesList.prototype.draw = function() {

		this.appendChild(this.phaseContext);

		CODAList.prototype.draw.call(this);
	};

	CODACampaignPhasePrizesList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODACampaignPhasePrizesListItem(list);
			listItem.index = index;
			listItem.data = this.elements[index];
			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
			list.appendChild(listItem);
		}
	};

	CODACampaignPhasePrizesList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-prize'
		});
	};

	KSystem.included('CODACampaignPhasePrizesList');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignPhasePrizesPanel(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = false;
		this.target.collection = '';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText',
	'KStatic'
], function() {
	CODACampaignPhasePrizesPanel.prototype = new CODAForm();
	CODACampaignPhasePrizesPanel.prototype.constructor = CODACampaignPhasePrizesPanel;

	CODACampaignPhasePrizesPanel.prototype.setData = function(data) {
//		this.config.schema = undefined;
		this.config.requestType = "KPanel";
	};

	CODACampaignPhasePrizesPanel.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODACampaignPhasePrizesPanel.prototype.addMinimizedPanel = function() {

		var prizes_list_panel = new KPanel(this);
		{
			prizes_list_panel.hash = '/prizeslist';
			CODACampaignPhasePrizesPanel.prizes_list = new CODACampaignPhasePrizesList(this, {
				links : {
					feed : { href : this.getShell().active_phase_href + '/prizes'}
				}
			});

			prizes_list_panel.appendChild(CODACampaignPhasePrizesPanel.prizes_list);
		}

		this.addPanel(prizes_list_panel, gettext('$FORM_PRIZES_PANEL'), true);

//		var prizes_stock_panel = new KPanel(this);
//		{
//			prizes_stock_panel.hash = '/prizesstock';
//			var prizes_stock_list = new CODACampaignPhasePrizeStockList(this, {
//				links : {
//					feed : { href : this.getShell().active_phase_href + '/prizes/10'}
//				}
//			});
//
//			prizes_stock_panel.appendChild(prizes_stock_list);
//
//		}
//
//		this.addPanel(prizes_stock_panel, gettext('$FORM_PRIZES_STOCK_PANEL'), false);

	};

	KSystem.included('CODACampaignPhasePrizesPanel');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignPhasePrizeStockList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema);
		this.onechoice = true;
		this.pageSize = 5;
		this.table = {
			"owner_id" : {
				label : gettext('$CAMPAIGNS_LIST_PARTICIPANT')
			},
			"date" : {
				label : gettext('$CAMPAIGNS_LIST_AWARD_DATE')
			},
			"content" : {
				label : gettext('$CAMPAIGNS_LIST_CONTENT')
			}
		};
		this.filterFields = [ 'owner_id' ];
		this.LIST_MARGIN = 285;
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODACampaignPhasePrizeStockList.prototype = new CODAList();
	CODACampaignPhasePrizeStockList.prototype.constructor = CODACampaignPhasePrizeStockList;

	CODACampaignPhasePrizeStockList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
//			var listItem = new CODACampaignPhasePrizesListItem(list);
//			listItem.index = index;
//			listItem.data = this.elements[index];
//			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
//			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
//			list.appendChild(listItem);
		}
	};

	KSystem.included('CODACampaignPhasePrizeStockList');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignPhasesListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
		this.config.headers = {};
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox'
], function() {
	CODACampaignPhasesListItem.prototype = new CODAListItem();
	CODACampaignPhasesListItem.prototype.constructor = CODACampaignPhasesListItem;

	CODACampaignPhasesListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PHASE_CONFIRM'),
			callback : function() {
				//widget.kinky.remove(widget, 'rest:' + encodeURIComponent(widget.data.href), null, null, 'onUserDelete');
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onPhaseDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODACampaignPhasesListItem.prototype.onPhaseDelete = function(data, message) {
		if(!!this.parent.content.children[0].childElementCount && this.parent.content.children[0].childElementCount == 1){
			this.parent.removeChild(this);
			window.document.location.reload(true);
		}else{
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.get(this, 'rest:' + this.getShell().active_campaign_href + '/phases?orderBy=+start_date', {}, headers, 'onDeleteCampaignPhase');
		}
	};

	CODACampaignPhasesListItem.prototype.onDeleteCampaignPhase = function(data, request){
		if(!!data.elements){
			this.getShell().all_campaign_phases = data.elements;
			var actual_phase = CODACampaignsShell.prototype.findActualPhase(data.elements);
			if(actual_phase){
				this.getShell().active_phase_href = actual_phase;
			}
		}
		this.parent.removeChild(this);
	};

	KSystem.included('CODACampaignPhasesListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignPhasesList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, { one_choice : true, add_button : true});
		this.pageSize = 5;
		this.table = {
			"name" : {
				label : gettext('$CAMPAIGNS_LIST_NAME')
			},
			"start_date" : {
				label : gettext('$CAMPAIGNS_LIST_START_DATE')
			},
			"end_date" : {
				label : gettext('$CAMPAIGNS_LIST_END_DATE')
			},
			"allowed_participations" : {
				label : gettext('$CAMPAIGNS_LIST_ALLOWED_PARTICIPATIONS')
			}
		};
		this.filterFields = [ 'name' ];
		this.LIST_MARGIN = 50;//(parent.className == 'CODACampaignItemForm' ? 485 : 285);
		this.orderBy = 'start_date';
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODACampaignPhasesList.prototype = new CODAList();
	CODACampaignPhasesList.prototype.constructor = CODACampaignPhasesList;

	CODACampaignPhasesList.prototype.addItems = function(list) {

		var dates = [];

		for ( var index in this.elements) {
			if(!!this.elements[index].start_date){
				if(this.elements[index].start_date.search("WEST") != -1){
					this.elements[index].start_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].start_date.substring(0,this.elements[index].start_date.length - 5)));
				}else{
					this.elements[index].start_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].start_date));
				}

			}
			if(!!this.elements[index].end_date){
				if(this.elements[index].end_date.search("WEST") != -1){
					this.elements[index].end_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].end_date.substring(0,this.elements[index].end_date.length - 5)));
				}else{
					this.elements[index].end_date = KSystem.formatDate('Y-M-d H:i', new Date(this.elements[index].end_date));
				}
			}

			dates.push({
				start_date : this.elements[index].start_date + ':00',
				end_date : this.elements[index].end_date + ':00'
			});

			this.elements[index].name = '<i class="ColoredCollection_' + ((index % 6) + 1) + '">&nbsp;&nbsp;</i> ' + this.elements[index].name;

			var listItem = new CODACampaignPhasesListItem(list);
			listItem.index = index;
			listItem.data = this.elements[index];
			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
			list.appendChild(listItem);
		}

		/*if (!!this.childWidget('/phases/calendar')) {
			this.removeChild(this.childWidget('/phases/calendar'));
		}
		this.contextCalendar = new CODAContextCalendar(this, {
			dates : dates
		});
		this.contextCalendar.hash = '/phases/calendar';
		this.appendChild(this.contextCalendar);*/
	};

	CODACampaignPhasesList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-phase-wizard'
		});
	};

	CODACampaignPhasesList.prototype.onNoElements = function(data, request) {
		this.getParent('KFloatable').closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/add-phase-wizard'
		});
	};

	KSystem.included('CODACampaignPhasesList');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignsDashboard(parent, phases) {
	if (parent != null) {
		var icons = [];

		icons.push({
			icon : 'css:fa fa-calendar',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_PHASES'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_PHASES_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/campaign-phases'
			}]
		});

		if(!!parent.active_phase_href && parent.active_phase_href != null) {
			icons.push({
				icon : 'css:fa fa-gift',
				activate : false,
				title : gettext('$DASHBOARD_BUTTON_PRIZES'),
				description : '<span>' + gettext('$DASHBOARD_BUTTON_PRIZES_DESC') + '</span>' ,
				actions : [{
					icon : 'css:fa fa-chevron-right',
					desc : 'Listar',
					link : '#/campaign-prizes'
				}]
			});

		}

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODACampaignsDashboard.prototype = new CODAMenuDashboard();
	CODACampaignsDashboard.prototype.constructor = CODACampaignsDashboard;

	CODACampaignsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODACampaignsDashboard');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignsShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODACampaignsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODACampaignsDashboard',

	'CODACampaignItemForm',
	'CODACampaignPhasesList',
	'CODACampaignPhasesListItem',
	'CODACampaignPhaseItemForm',
	'CODACampaignPhasePrizesPanel',
	'CODACampaignPhasePrizesList',
	'CODACampaignPhasePrizesListItem',
	'CODACampaignPhasePrizeItemForm',
	'CODACampaignPhasePrizeStockList',

	'CODAPrizesDashboard'
], function() {
	CODACampaignsShell.prototype = new CODAGenericShell();
	CODACampaignsShell.prototype.constructor = CODACampaignsShell;

	CODACampaignsShell.prototype.loadShell = function() {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:/campaigns', {}, headers, 'onLoadCampaign');
	};

	CODACampaignsShell.prototype.onLoadCampaign = function(data, request) {
		if(!!data && !!data.elements && data.elements.length == 1){
			this.active_campaign_href = data.elements[0].href;
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			//BO:PRIZE_NO_PHASE_SELECTION comentada a linha abaixo para associar aos prémios a primeira fase inserida enquanto BO não tem opção de selecção de fase ao aceder à lista de prémios
			//this.kinky.get(this, 'rest:' + data.elements[0].href + '/phases?orderBy=+start_date', {}, headers, 'onLoadCampaignPhases');
			this.kinky.get(this, 'rest:' + data.elements[0].href + '/phases?orderBy=id', {}, headers, 'onLoadCampaignPhases');

		}
		else{
			this.draw();
		}
	};

	CODACampaignsShell.prototype.onLoadCampaignPhases = function(data, request) {
		if(!!data && !!data.elements && data.elements.length > 0){
			this.all_campaign_phases = data.elements;
			var actual_phase = data.elements[0].href;
			//BO:PRIZE_NO_PHASE_SELECTION a função para encontrar a fase atual pressupõe que o BO tenha a opção de selecção de fase ao aceder à lista de prémios
			//var actual_phase = CODACampaignsShell.prototype.findActualPhase(data.elements);
			if(actual_phase){
				this.active_phase_href = actual_phase;
			}
		}else{
			this.all_campaign_phases = new Array();
		}

		this.draw();
	};

	CODACampaignsShell.prototype.findActualPhase = function(elements) {
		if(!!elements && elements.length > 0){
			var actual_date = new Date();
			var actual_phase_href = null;

			if(actual_date < new Date(elements[0].start_date)){
				actual_phase_href = elements[0].href;
			}
			else if(actual_date > new Date(elements[elements.length - 1].end_date)){
				actual_phase_href = elements[elements.length - 1].href;
			}
			else{
				for ( var index in elements){
					if(actual_date >= new Date(elements[index].start_date) && actual_date <= new Date(elements[index].end_date)){
						actual_phase_href = elements[index].href;
						break;
					}else{
						continue;
					}
				}
			}
			return actual_phase_href;
		}
		return false;
	};

	CODACampaignsShell.prototype.draw = function() {
		this.setTitle(gettext('$CAMPAIGNS_SHELL_TITLE'));

		this.dashboard = new CODACampaignsDashboard(this, this.all_campaign_phases);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);

		if(!this.active_campaign_href){
			this.dashboard.disable();
			this.addCampaign();
		}
	};

	CODACampaignsShell.sendRequest = function() {
	};

	CODACampaignsShell.prototype.addCampaign = function() {
		var configForm = new CODACampaignItemForm(this, null, null, null);
		{
			configForm.hash = '/campaign/add';
			configForm.setTitle(gettext('$NEW_CAMPAIGN_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : true });
	};

	CODACampaignsShell.prototype.editCampaign = function(href, minimized) {
		var configForm = new CODACampaignItemForm(this, href, null, null);
		{
			configForm.hash = '/campaign/edit';
			configForm.setTitle(gettext('$EDIT_CAMPAIGN_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : false, modal : true });
	};

	CODACampaignsShell.prototype.addCampaignPhase = function() {
		var configForm = new CODACampaignPhaseItemForm(this, null, null, null);
		{
			configForm.hash = '/campaign/phase/add';
			configForm.setTitle(gettext('$ADD_CAMPAIGNPHASE_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : true });
	};

	CODACampaignsShell.prototype.addPhaseWizard = function() {
		
		var screens = [ 'CODACampaignPhaseItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_CAMPAIGNPHASE_PROPERTIES'));
		wiz.hash = '/campaign/phase/add';

		wiz.mergeValues = function() {
			this.values[0].status = 'active';
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			//params.parent_root = CODA.ACTIVE_SHELL;
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			var update_campaign_here = true;
			if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
				CODACampaignsShell.phases_list.list.refreshPages();
				if(!CODACampaignsShell.phases_list.elements){
					update_campaign_here = false;
					window.document.location.reload(true);
				}else{
					this.enable();
					this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
				}
				if(update_campaign_here == true){
					this.parent.closeMe();
					var headers = { 'E-Tag' : '' + (new Date()).getTime() };
					headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
					this.kinky.get(this, 'rest:' + this.getShell().active_campaign_href + '/phases?orderBy=+start_date', {}, headers, 'onNewCampaignPhase');
				}
			}else{
				window.document.location.reload(true);
			}

		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + this.getShell().active_campaign_href + '/phases', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		wiz.onNewCampaignPhase = function(data, request){
			if(!!data.elements){
				this.getShell().all_campaign_phases = data.elements;
				var actual_phase = CODACampaignsShell.prototype.findActualPhase(data.elements);
				if(actual_phase){
					this.getShell().active_phase_href = actual_phase;
				}
			}
		};

		this.makeWizard(wiz);

	};

	CODACampaignsShell.prototype.listCampaignPhases = function(href, minimized) {
		CODACampaignsShell.phases_list = new CODACampaignPhasesList(this, {
			links : {
				feed : { href :  href + '?fields=elements,size' }
			}
		});
		CODACampaignsShell.phases_list.hash = '/campaign/phase/list';
		CODACampaignsShell.phases_list.setTitle(gettext('$LIST_CAMPAIGNPHASE_PROPERTIES'));
		this.makeDialog(CODACampaignsShell.phases_list, { minimized : false, modal : true });
	};

	CODACampaignsShell.prototype.editCampaignPhase = function(href, minimized) {
		var configForm = new CODACampaignPhaseItemForm(this, href, null, null);
		{
			configForm.hash = '/campaign/phase/edit';
			configForm.setTitle(gettext('$EDIT_CAMPAIGNPHASE_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : minimized });
	};

	CODACampaignsShell.prototype.listCampaignPhasePrizes = function(href, minimized) {
		var prizesInfo = new CODAPrizesDashboard(this, { 
			links : {
				feed : { href :  this.getShell().active_phase_href + '/prizes?fields=elements,size' }
			},
			filter_input : true, 
			add_button : true
		});
		prizesInfo.hash = '/campaign/prize/list';
		this.makeDialog(prizesInfo, { minimized : false, modal : true });
	};

	CODACampaignsShell.prototype.editCampaignPhasePrize = function(href, minimized) {
		var configForm = new CODACampaignPhasePrizeItemForm(this, href, null, null);
		{
			configForm.hash = '/campaign/prize/edit';
			configForm.setTitle(gettext('$EDIT_CAMPAIGNPHASEPRIZE_PROPERTIES'));
		}
		this.makeListForm(configForm, { minimized : false, modal : true });
	};

	CODACampaignsShell.prototype.addCampaignPhasePrize = function() {
		var configForm = new CODACampaignPhasePrizeItemForm(this, null, null, null);
		{
			configForm.hash = '/campaign/prize/add';
			configForm.setTitle(gettext('$ADD_CAMPAIGNPRIZEPHASE_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : true });
	};

	CODACampaignsShell.prototype.addPrizeWizard = function() {

		var screens = [ 'CODACampaignPhasePrizeItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_CAMPAIGNPHASEPRIZE_PROPERTIES'));
		wiz.hash = '/campaign/prize/add';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			//params.parent_root = CODA.ACTIVE_SHELL;
			//BO:PRIZE_NO_PHASE_SELECTION colocada a condição abaixo porque no BO não dá para seleccionar a fase para a que se querem ver prémios
			if(!!this.getShell().all_campaign_phases){
				params.phase_id = this.getShell().all_campaign_phases[0].id;
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			if(!!CODACampaignsShell.phases_list && !!CODACampaignsShell.phases_list.list){
				CODACampaignsShell.phases_list.list.refreshPages();
			}else if(!!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list && !!CODACampaignPhasePrizesPanel.prizes_list.list){
				CODACampaignPhasePrizesPanel.prizes_list.list.refreshPages();
			}else{
				this.getParent('KFloatable').closeMe();
				KBreadcrumb.dispatchURL({
					hash : '/campaign-prizes'
				});
				return;
			}
			//this.getParent('KFloatable').closeMe();
			this.enable();
			this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		};

		wiz.send = function(params) {

			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + this.getShell().active_campaign_href + '/phases/' + params['phase_id'] + '/prizes', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODACampaignsShell.prototype.onError = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CAMPAIGNS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODACampaignsShell.prototype.onNoContent = function(data, request) {
		if(request.service.indexOf('/prizes') == request.service.length - 7){
//			CODACampaignsShell.prototype.addCampaignPhasePrize.call(this);
			CODACampaignsShell.prototype.addPrizeWizard.call(this);
		}else if(request.service.indexOf('/phases') == request.service.length - 7){
//			CODACampaignsShell.prototype.addCampaignPhase.call(this);
			CODACampaignsShell.prototype.addPhaseWizard.call(this);
		}else if(request.service.indexOf('/quizconfigurations') != -1){
			var configForm = new CODAQuizConfigurationsForm(this, null);
			{
				configForm.hash = '/quizconfigurations/advanced';
				configForm.setTitle(gettext('$QUIZ_CONFIGURATIONS_PROPERTIES'));
			}
			this.makeDialog(configForm, { minimized : false, modal : true });
		}else if(request.service.indexOf('/questions?phase_id') != -1){
			KCache.clear('/questionslist');
			CODACampaignsShell.prototype.addQuestionWizard.call(this);
		}else if(request.service.indexOf('/answers') != -1){
			KCache.clear('/questionanswers');
			CODACampaignsShell.prototype.addAnswerWizard.call(this);
		}else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODACampaignsShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'edit-campaign': {
				if(!!this.widget.active_campaign_href){
					Kinky.getDefaultShell().editCampaign(this.widget.active_campaign_href, false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
					break;
				}else{
					Kinky.getDefaultShell().addCampaign();
					KBreadcrumb.dispatchURL({
						hash : ''
					});
					break;
				}
			}
			case 'campaign-phases': {
				if(!!this.widget.active_campaign_href){
					Kinky.getDefaultShell().listCampaignPhases(this.widget.active_campaign_href + '/phases', false);
				}
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'campaign-prizes': {
				if(this.widget.active_phase_href != null){
					Kinky.getDefaultShell().listCampaignPhasePrizes(null, false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'add-phase': {
				Kinky.getDefaultShell().addCampaignPhase();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-phase-wizard' : {
				Kinky.getDefaultShell().addPhaseWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-prize': {
				Kinky.getDefaultShell().addCampaignPhasePrize();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-prize-wizard' : {
				Kinky.getDefaultShell().addPrizeWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit': {
				if(parts[parts.length - 2] == 'phases'){
					Kinky.getDefaultShell().editCampaignPhase('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5], true);
				}
				else if(parts[parts.length - 2] == 'prizes'){
					Kinky.getDefaultShell().editCampaignPhasePrize('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5] + '/' + parts[6] + '/' + parts[7], true);
				}

				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-full': {
				if(parts[parts.length - 2] == 'phases'){
					Kinky.getDefaultShell().editCampaignPhase('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5], true);
				}else if(parts[parts.length - 2] == 'prizes'){
					Kinky.getDefaultShell().editCampaignPhasePrize('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5] + '/' + parts[6] + '/' + parts[7], true);
				}

				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}

//			case 'forms-configurations' : {
//				if(!!this.widget.active_campaign_href){
//					var actualPhaseSplit = this.widget.active_phase_href.split('/');
//					var actualPhaseID = actualPhaseSplit[actualPhaseSplit.length - 1];
//					if (!!actualPhaseID){
//						Kinky.getDefaultShell().listForms('/forms', false);
//						KBreadcrumb.dispatchURL({
//							hash : ''
//						});
//						break;
//					}
//				}
//			}
		}
	};

	KSystem.included('CODACampaignsShell');
}, Kinky.CODA_INCLUDER_URL);
function CODAContextCalendar(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.calendars = {};
		this.datesToSelect = data.dates || null;
		KCalendar.localization['weekDayName'] = new Array('Domingo', 'Segunda', 'Ter&ccedil;a', 'Quarta', 'Quinta', 'Sexta', 'S&aacute;bado');
		KCalendar.localization['monthName'] = new Array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
		this.classNameCount = 0;
	}
}

KSystem.include([
	'KButton',
	'KPanel'
], function() {
	CODAContextCalendar.prototype = new KPanel();
	CODAContextCalendar.prototype.constructor = CODAContextCalendar;

	CODAContextCalendar.prototype.draw = function() {
		KPanel.prototype.draw.call(this);

		var currentDate = new Date();
		var actualMonth = currentDate.getMonth();
		var actualYear = currentDate.getFullYear();

		this.monthShown = [actualMonth + '/' + actualYear, (actualMonth > 10 ? 0 + '/' + (actualYear + 1) : (actualMonth + 1)  + '/' + actualYear), (actualMonth > 9 ? 0 + '/' + (actualYear + 1) : (actualMonth + 2)  + '/' + actualYear)];

		for (var index in this.monthShown){
			this.calendars[this.monthShown[index]] = {
				days : new Array()
			};
			var newCalendar = this.drawCalendar(this.monthShown[index]);
			this.content.appendChild(newCalendar);
		}

		if (!!this.datesToSelect) {
			for (var index in this.datesToSelect){
				this.markDates(this.datesToSelect[index]);
			}
		}
	};

	CODAContextCalendar.prototype.markDates = function(dateObj) {
		var start_date = new Date(dateObj.start_date);
		var end_date = new Date(dateObj.end_date);

		if (!dateObj.className || dateObj.className == '' ){
			this.classNameCount++;
		}

		var arrayToMark = new Array();
		var matchClass = {};
		if (start_date < end_date){
			for (var fill = start_date; fill <= end_date; fill.setDate(fill.getDate() + 1)){
				var mark = fill.getDate() + '/' + fill.getMonth() + '/' + fill.getFullYear();
				arrayToMark.push(mark);
				matchClass[mark] = dateObj.className || ('ColoredCollection_' + this.classNameCount);
			}
		}else{
			arrayToMark.push(start_date.getDate() + '/' + start_date.getMonth() + '/' + start_date.getFullYear());
		}

		for (var cal_idx in this.calendars){
			for ( var i = 0; i != 6; i++) {
				for ( var k = 0; k != 7; k++) {
					var idx = undefined;
					if((idx = arrayToMark.indexOf(this.calendars[cal_idx].days[i][k].name)) != -1){
						var match = arrayToMark[idx];
						if (match == this.calendars[cal_idx].days[i][k].name){
							this.calendars[cal_idx].days[i][k].className = 'Selected' + ' ' + (!!matchClass[match] ? matchClass[match] : '' );
						}
					}
				}
			}
		}
		return;
	};

	CODAContextCalendar.prototype.drawCalendar = function(month_year) {

		var calendarContainer = window.document.createElement('div');
		calendarContainer.appendChild(window.document.createElement('h2'));

		var table = calendarContainer.appendChild(window.document.createElement('table'));
		var tHead = table.appendChild(window.document.createElement('thead'));
		var tHeadDays = tHead.appendChild(window.document.createElement('tr'));
		var tBody = table.appendChild(window.document.createElement('tbody'));

		for ( var i = 0; i != 6; i++) {
			var tRow = tBody.appendChild(window.document.createElement('tr'));
			this.calendars[month_year].days[i] = new Array();
			for ( var k = 0; k != 7; k++) {
				if (i == 0) {
					var th = window.document.createElement('th');
					th.className = ' CODAContextCalendarDayName ';
					var abbr = window.document.createElement('abbr');
					abbr.title = KCalendar.localization['weekDayName'][k];
					abbr.appendChild(window.document.createTextNode(KCalendar.localization['weekDayName'][k].charAt(0)));
					th.appendChild(abbr);
					tHeadDays.appendChild(th);
				}
				this.calendars[month_year].days[i][k] = tRow.appendChild(window.document.createElement('td')).appendChild(window.document.createElement('button'));
				this.calendars[month_year].days[i][k].setAttribute('type', 'button');
				tRow.style.textAlign = 'right';
				this.calendars[month_year].days[i][k].appendChild(window.document.createTextNode(' '));
				this.calendars[month_year].days[i][k].className = ' CODAContextCalendarDay ';
			}
		}

		this.showMonth(calendarContainer, month_year);

		return calendarContainer;
	};

	CODAContextCalendar.prototype.showMonth = function(calendarContainer, month_year) {

		var dateSplited = month_year.split('/');
		var monthToShow = dateSplited[0];
		var monthYear = dateSplited[1];

		if (monthToShow > 11) {
			monthToShow = 0;
			monthYear++;
		}
		if (monthToShow < 0) {
			monthToShow = 11;
			monthYear--;
		}
		var monthDate = new Date();
		var nDayInMonth = this.getDaysInMonth(monthToShow, monthYear);
		monthDate.setDate(1);
		monthDate.setMonth(monthToShow);
		monthDate.setFullYear(monthYear);
		this.currentDate = monthDate;


		var titleText = calendarContainer.getElementsByTagName("h2");
		titleText[0].appendChild(window.document.createTextNode(' ' + KCalendar.localization['monthName'][monthToShow] + ' ' + monthYear + ' '));

		for ( var i = 0, dayOfMonth = 0; i != 6; i++) {
			for ( var k = 0; k != 7; k++) {
				if ((dayOfMonth == 0) && (k == monthDate.getDay())) {
					dayOfMonth = 1;
				}
				if (this.calendars[month_year].days[i][k].childNodes.length != 0) {
					this.calendars[month_year].days[i][k].removeChild(this.calendars[month_year].days[i][k].childNodes[0]);
				}
				this.calendars[month_year].days[i][k].style.visibility = ((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? 'hidden' : 'visible');
				if ((dayOfMonth == 0) || (dayOfMonth > nDayInMonth)){
					this.calendars[month_year].days[i][k].name = '0';
				}else{
					this.calendars[month_year].days[i][k].name = '' + dayOfMonth + '/' + monthToShow + '/' + monthYear;
				}
				this.calendars[month_year].days[i][k].appendChild(window.document.createTextNode((dayOfMonth == 0) || (dayOfMonth > nDayInMonth) ? '0' : '' + dayOfMonth++));
			}
		}
	};

	CODAContextCalendar.prototype.getDaysInMonth = function(month, year) {
		var m = [
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		];
		if (month != 1) {
			return m[month];
		}
		if (year % 4 != 0) {
			return m[1];
		}
		if ((year % 100 == 0) && (year % 400 != 0)) {
			return m[1];
		}
		return m[1] + 1;
	};

	KSystem.included('CODAContextCalendar');
}, Kinky.CODA_INCLUDER_URL);
function CODAContextList(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.config = data.links.feed;
		this.uiDetails = data.uiDetails;
		this.callback = data.callback;
		this.selected = null;
	}
}

KSystem.include([
	'KButton',
	'KPanel'
], function() {
	CODAContextList.prototype = new KPanel();
	CODAContextList.prototype.constructor = CODAContextList;

	CODAContextList.prototype.draw = function() {
		this.setTitle(gettext('Phases'));
		KPanel.prototype.draw.call(this);

		var navUpBt = window.document.createElement('button');
		navUpBt.className = 'fa fa-angle-up';
		this.content.appendChild(navUpBt);
		KDOM.addEventListener(navUpBt, 'click', CODAContextList._navigate);

		var navDownBt = window.document.createElement('button');
		navDownBt.className = 'fa fa-angle-down';
		this.content.appendChild(navDownBt);
		KDOM.addEventListener(navDownBt, 'click', CODAContextList._navigate);

		this.list = window.document.createElement('ul');
		this.list.className = 'ListElements';
		for (var index in this.data.elements){
			var element = window.document.createElement('li');
			var elementText = window.document.createElement('span');
			elementText.innerHTML = this.data.elements[index].name;
			element.appendChild(elementText);
			element.value = this.data.elements[index].id;
			KDOM.addEventListener(element, 'click', CODAContextList._selectElement);
			this.list.appendChild(element);

			if (this.uiDetails.initialElementSelected == this.data.elements[index].id){
				this.selected = this.data.elements[index];
			}
		}

		var ulchildren = this.list.childNodes;

        for (var i = 0; i < ulchildren.length; i++){
            if (ulchildren[i].value == this.selected.id){
            	KCSS.addCSSClass('Selected', ulchildren[i]);
            	break;
            }
        }

		this.content.appendChild(this.list);

		this.info = window.document.createElement('div');
		this.content.appendChild(this.info);
		this.refreshInfo();
		var marked = false;

		if (!!this.uiDetails.timeContext){
			for (var i = 0; i < ulchildren.length; i++){
	            if (ulchildren[i].value == this.selected.id){
	            	KCSS.addCSSClass('Actual', ulchildren[i]);
	            	marked = true;
	            }else if (!marked){
	            	KCSS.addCSSClass('Prev', ulchildren[i]);
	            }else {
	            	KCSS.addCSSClass('Next', ulchildren[i]);
	            }
	        }


			var timeContextInfo = window.document.createElement('ul');
			timeContextInfo.className = 'TimeContextInfo';
			for (var tidx in this.uiDetails.timeContext){
				var timeContextLI = window.document.createElement('li');
				timeContextLI.className = 'fa fa-circle ' + tidx;
				timeContextLI.innerHTML = this.uiDetails.timeContext[tidx];
				timeContextInfo.appendChild(timeContextLI);

			}
			this.content.appendChild(timeContextInfo);


		}
	};

	CODAContextList.prototype.refreshInfo = function() {
		this.info.innerHTML = '';
		for(var index in this.uiDetails.dataToShow){
			var infoTitle = window.document.createElement('h3');
			infoTitle.innerHTML = this.uiDetails.dataToShow[index];
			this.info.appendChild(infoTitle);
			var infoText = window.document.createElement('p');

			if (/date/.test(index)){
				if(this.selected[index].search("WEST") != -1){
					infoText.innerHTML = KSystem.formatDate('Y-M-d H:i:s', new Date(this.selected[index].substring(0,this.selected[index].length - 5)));
				}else{
					infoText.innerHTML = KSystem.formatDate('Y-M-d H:i:s', new Date(this.selected[index]));
				}
			}else{
				infoText.innerHTML = this.selected[index];
			}

			this.info.appendChild(infoText);
		}
	};

	CODAContextList.prototype.selectElement = function(el) {
		if (!(/Selected/.test(el.className))){
			var ulchildren = this.list.childNodes;
			for (var i = 0; i < ulchildren.length; i++){
				if (ulchildren[i].value == this.selected.id){
					KCSS.removeCSSClass('Selected', ulchildren[i]);
					KCSS.addCSSClass('Selected', el);
					for (var index in this.data.elements){
						if (this.data.elements[index].id == el.value){
							this.selected = this.data.elements[index];
							this.filterMainList(el.value);
							this.refreshInfo();
							break;
						}
					}
					break;
				}
			}
		}
	};

	CODAContextList._selectElement = function(e) {
		var el = KDOM.getEventCurrentTarget(e);
		KDOM.getEventWidget(e).selectElement(el);
	};

	CODAContextList._navigate = function(e) {
		var el = KDOM.getEventTarget(e);
		var self = KDOM.getEventWidget(e);
		var ulchildren = self.list.childNodes;
		var ulchildrenLength = ulchildren.length;

		for (var i = 0; i < ulchildrenLength; i++){
			if (/Selected/.test(ulchildren[i].className)){
				if (/down/.test(el.className)){
					if (i == ulchildrenLength - 1){
						return;
					}
					self.selectElement(ulchildren[i+1]);
					return;
				}else{
					if (i < 1){
						return;
					}
					self.selectElement(ulchildren[i-1]);
					return;
				}
			}
		}
	};

	CODAContextList.prototype.filterMainList = function(value) {
		this.callback(value);
	};

	KSystem.included('CODAContextList');
}, Kinky.CODA_INCLUDER_URL);
function CODAPrizesDashboard(parent, options, apps) {
	if (parent != null) {
		CODAFormIconDashboard.call(this, parent, options, apps);
		this.setTitle(gettext('$LIST_CAMPAIGNPHASEPRIZE_PROPERTIES'));
		this.filterFields = [ 'name' ];
	}
}

KSystem.include([
    'KConfirmDialog',
	'CODAIcon',
	'CODAFormIconDashboard'
], function() {
	CODAPrizesDashboard.prototype = new CODAFormIconDashboard();
	CODAPrizesDashboard.prototype.constructor = CODAPrizesDashboard;

	CODAPrizesDashboard.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-prize-wizard'
		});
	};

	CODAPrizesDashboard.prototype.getIconFor = function(data) {

		var dummy = window.document.createElement('p');
		if(!!data.description){
			dummy.innerHTML = data.description;
		}else{
			dummy.innerHTML = '';
		}

		var aditional = '';
		if(data.stock){
			aditional += '<span><b>' + gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK') + ':</b> ' + data.stock + '</span>';
		}
		if(data.value){
			aditional += '<span><b>' + gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE') + ':</b> ' + data.value + '</span>';
		}

		var actions = [];
 		actions.push({
			icon : 'css:fa fa-chevron-down',
			link : '#/edit' + data.href,
			desc : 'Gerir'
		});
		actions.push({
			icon : 'css:fa fa-trash-o',
			desc : 'Remover',
			events : [
				{
					name : 'click',
					callback : CODAPrizesDashboard.remove
				}
			]
		});

		return new CODAIcon(this, {
			icon : !!data.url ? data.url : 'css:fa fa-gift',
			activate : false,
			title : data.name,
			href : data.href,
			description : '<span>' + dummy.textContent + '</span>' + aditional,
			actions : actions
		});
	};

	CODAPrizesDashboard.prototype.addActions = function() {

	};

	CODAPrizesDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PRIZE_CONFIRM'),
			callback : function() {
				widget.parent.closeMe();
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onPrizeDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400
		});
	};

	CODAPrizesDashboard.prototype.onPrizeDelete = function(data, message) {
		this.refresh();
		return;
	};

	KSystem.included('CODAPrizesDashboard');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

