///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$CAMPAIGNS_SHELL_TITLE', 'CALENDARIZAÇÃO e PRÊMIOS', 'br');

settext('$CAMPAIGNS_PHASE', 'Fase $PHASE_NAME', 'br');

settext('$CAMPAIGNS_DASHBOARD_TITLE', '<i class="fa fa-globe"></i> M\u00f3dulo de Campanhas', 'br');
settext('$DASHBOARD_BUTTON_CONFIGURE_CAMPAIGN', 'Informação da Campanha', 'br');
settext('$DASHBOARD_BUTTON_PHASES', 'Calendarização', 'br');
settext('$DASHBOARD_BUTTON_PRIZES', 'Pr\u00e9mios', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS', 'Participa\u00e7\u00f5es', 'br');
settext('$DASHBOARD_BUTTON_WINNERS', 'Vencedores', 'br');
settext('$DASHBOARD_BUTTON_FORMS', 'Formul\u00e1rios', 'br');

settext('$DASHBOARD_BUTTON_CONFIGURE_CAMPAIGN_DESC', 'Gere as defini\u00e7\u00f5es da campanha e acessa \u00e0s funcionalidades mais relevantes', 'br');
settext('$DASHBOARD_BUTTON_PHASES_DESC', 'Define a calendariza\u00e7\u00e3o da campanha', 'br');
settext('$DASHBOARD_BUTTON_PRIZES_DESC', 'Faz a gest\u00e3o dos pr\u00e9mios a atribuir aos participantes', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_DESC', 'Vê quem j\u00e1 participou na sua campanha e consulta os detalhes de cada participa\u00e7\u00e3o', 'br');

settext('$DASHBOARD_BUTTON_FORMS_DESC', 'Configure aqui os formulários', 'br');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$FORM_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$FORM_REGULATION_PANEL', '<i class="fa fa-gavel"></i> Regulamento', 'br');
settext('$FORM_PHASES_PANEL', '<i class="fa fa-adjust"></i> Fases', 'br');
settext('$FORM_PARTICIPATIONS_PANEL', '<i class="fa fa-adjust"></i> Participa\u00e7\u00f5es', 'br');
settext('$FORM_PRIZES_PANEL', '<i class="fa fa-adjust"></i> Pr\u00e9mios', 'br');
settext('$FORM_PRIZES_IMAGES_PANEL', '<i class="fa fa-picture-o"></i> Imagens', 'br');
settext('$FORM_PRIZES_ASSIGNMENT_PANEL', '<i class="fa fa-truck"></i> Estoque & Entrega', 'br');
settext('$FORM_PRIZES_STOCK_PANEL', '<i class="fa fa-adjust"></i> Gest\u00e3o de Stock', 'br');

settext('$LIST_CAMPAIGNPHASE_PROPERTIES', 'Calendarização de Fases', 'br');
settext('$LIST_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Prêmios', 'br');
settext('$LIST_CAMPAIGNPARTICIPATIONS_PROPERTIES', 'Participações', 'br');

settext('$NEW_CAMPAIGN_PROPERTIES', 'Nova Campanha', 'br');
settext('$EDIT_CAMPAIGN_PROPERTIES', 'Editar Campanha ', 'br');
settext('$ADD_CAMPAIGNPHASE_PROPERTIES', 'Nova Fase ', 'br');
settext('$EDIT_CAMPAIGNPHASE_PROPERTIES', 'Editar Fase ', 'br');
settext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES', 'Participa\u00e7\u00e3o ', 'br');
settext('$EDIT_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Pr\u00e9mio ', 'br');
settext('$ADD_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Novo Pr\u00e9mio', 'br');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_DEFAULT_VALUE', '1ª Fase de Participação', 'br');

settext('$CAMPAIGN_ITEM_FORM_APPLICATION', 'Aplica\u00e7\u00e3o', 'br');
settext('$CAMPAIGN_ITEM_FORM_CLIENT', 'Cliente', 'br');
settext('$CAMPAIGN_ITEM_FORM_NAME', 'T\u00edtulo', 'br');
settext('$CAMPAIGN_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');

settext('$CAMPAIGN_ITEM_FORM_REGULATION', 'Regulamento', 'br');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME', 'Nome', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE', 'Data de In\u00edcio', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE', 'Data de Fim', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER', 'N\u00famero M\u00e1ximo de Participa\u00e7\u00f5es', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD', 'Atribui\u00e7\u00e3o de Pr\u00e9mio', 'br');
settext('$SDATE_AFTER_EDATE', 'Data de In\u00edcio posterior a Data de Fim', 'br');
settext('$SDATE_PREVIOUS_NOW', 'Data de In\u00edcio anterior a data atual', 'br');
settext('$EDATE_BEFORE_SDATE', 'Data de Fim anterior a data de In\u00edcio', 'br');
settext('$EDATE_PREVIOUS_NOW', 'Data de Fim anterior a data atual', 'br');

settext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM', 'Formul\u00e1rio', 'br');

settext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_CODE', 'all_with_repetitions', 'br');
settext('$PRIZES_METHOD_ALL_NO_REPETITIONS_CODE', 'all_no_repetitions', 'br');
settext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_CODE', 'higher_with_repetitions', 'br');
settext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_CODE', 'higher_no_repetitions', 'br');

settext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_NAME', 'Todos com repeti\u00e7\u00f5es', 'br');
settext('$PRIZES_METHOD_ALL_NO_REPETITIONS_NAME', 'Todos sem repeti\u00e7\u00f5es', 'br');
settext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_NAME', 'Maior com repeti\u00e7\u00f5es', 'br');
settext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_NAME', 'Maior sem repeti\u00e7\u00f5es', 'br');

settext('$PARTICIPATION_ITEM_FORM_OWNER', 'Participante', 'br');
settext('$PARTICIPATION_ITEM_FORM_START_DATE', 'Data de In\u00edcio', 'br');
settext('$PARTICIPATION_ITEM_FORM_END_DATE', 'Data de Fim', 'br');
settext('$PARTICIPATION_ITEM_FORM_VALUE', 'Valor', 'br');
settext('$PARTICIPATION_ITEM_FORM_RANKING', 'Ranking', 'br');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME', 'Nome', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE', 'Fase', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL', 'Apresenta\u00e7\u00e3o', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1', 'Imagem 1', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2', 'Imagem 2', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3', 'Imagem 3', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4', 'Imagem 4', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE', 'Pontuação miníma para atribuição', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK', 'Estoque', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE', 'Tipo de Pr\u00e9mio', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_CONTENT', 'Conte\u00fado do Pr\u00e9mio', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_MIME_TYPE', 'Mime Type - Será de atribuição automática', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VOUCHER_TYPE', 'Miniatura 4', 'br');

settext('$PRIZE_TYPE_PHYSICAL_CODE', 'no/mime', 'br');
settext('$PRIZE_TYPE_IMAGE_CODE', 'image/*; charset=binary', 'br');
settext('$PRIZE_TYPE_TEXT_PLAIN_CODE', 'text/plain; charset=utf-8', 'br');
settext('$PRIZE_TYPE_TEXT_HTML_CODE', 'text/html; charset=utf-8', 'br');
settext('$PRIZE_TYPE_VIDEO_CODE', 'video/*; charset=binary', 'br');
settext('$PRIZE_TYPE_VIMEO_CODE', 'embed/vimeo; charset=binary', 'br');
settext('$PRIZE_TYPE_VOUCHER_PDF_CODE', 'application/pdf; charset=binary', 'br');
settext('$PRIZE_TYPE_YOUTUBE_CODE', 'embed/youtube; charset=binary', 'br');

settext('$PRIZE_TYPE_PHYSICAL_NAME', 'Físico (entregue presencialmente)', 'br');
settext('$PRIZE_TYPE_IMAGE_NAME', 'Imagem', 'br');
settext('$PRIZE_TYPE_TEXT_PLAIN_NAME', 'Texto', 'br');
settext('$PRIZE_TYPE_TEXT_HTML_NAME', 'Texto (html)', 'br');
settext('$PRIZE_TYPE_VIDEO_NAME', 'V\u00eddeo', 'br');
settext('$PRIZE_TYPE_VIMEO_NAME', 'Vimeo', 'br');
settext('$PRIZE_TYPE_VOUCHER_PDF_NAME', 'Voucher', 'br');
settext('$PRIZE_TYPE_YOUTUBE_NAME', 'Youtube', 'br');

settext('$PRIZE_TYPE_VOUCHER_PDF_MESSAGE', 'N\u00e3o h\u00e1 vouchers dispon\u00edveis', 'br');
settext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS', 'Validade (em dias)', 'br');
settext('$PRIZE_TYPE_VOUCHER_START_DATE', 'Data de Validade (desde)', 'br');
settext('$PRIZE_TYPE_VOUCHER_END_DATE', 'Data de Validade (até)', 'br');

settext('$CAMPAIGN_PHASE_WIZ_PANEL_TITLE_CODAAPPLICATIONWIZARD', 'Calendarização de Fase Temporal', 'br');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_DESC_CODAAPPLICATIONWIZARD', 'Define o nome para identificar aa fase que pretende configurar (e.g. "1ª fase de participações" ou "fase de apuramento de resultados").<br>Indica as datas que os usuários poderão participar.<br>Indica o número máximo de participações por utilizador.<br>Indica o tipo de atribuição de prêmio.<br><br><i>Pode adicionar mais fases ou alterar estes valores na área de gestão do aplicativo.</i><br>', 'br');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_TITLE_CODAWIZARD', 'Calendarização de Fase Temporal', 'br');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_DESC_CODAWIZARD', 'Indique o nome identificativo da fase que pretende configurar (e.g. "1ª fase de participações" ou "fase de apuramento de resultados").<br>Indica as datas entre as quais os utilizadores poderão participar.<br>Indica, se quiser o número máximo de participações por utilizador.<br>Indica o tipo de atribuição de prêmio.<br><br>', 'br');
settext('$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_TITLE', 'Prémio', 'br');
settext('$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_DESC', 'Indique o nome e imagem para o prêmio.<br>Indique a partir de que pontuação o prêmio é atribuído (se aplicável).<br>Indica o estoque disponível para este prêmio.<br><br>Você poderá adicionar mais prêmios ou alterar os valores na área de gestão do aplicativo.<br>', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_ICON', '<i class="fa fa-user"></i>', 'br');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_TOOLTIP', 'Participante', 'br');

settext('$CAMPAIGNS_LIST_NAME', 'Nome', 'br');
settext('$CAMPAIGNS_LIST_VALUE', 'Valor', 'br');
settext('$CAMPAIGNS_LIST_STOCK', 'Estoque', 'br');

settext('$CAMPAIGNS_LIST_PARTICIPANT', 'Participante', 'br');
settext('$CAMPAIGNS_LIST_START_DATE', 'Data de In\u00edcio', 'br');
settext('$CAMPAIGNS_LIST_END_DATE', 'Data de Fim', 'br');
settext('$CAMPAIGNS_LIST_VALUE', 'Valor', 'br');
settext('$CAMPAIGNS_LIST_STATUS', 'Estado', 'br');

settext('$CAMPAIGNS_LIST_AWARD_DATE', 'Data de Atribui\u00e7\u00e3o', 'br');
settext('$CAMPAIGNS_LIST_CONTENT', 'Conte\u00fado', 'br');

settext('$CAMPAIGNS_LIST_ALLOWED_PARTICIPATIONS', 'Nr Máx Participa\u00e7\u00f5es', 'br');

settext('$PARTICIPATIONS_CLIST_PREVIOUS_PHASE', 'Fase anterior', 'br');
settext('$PARTICIPATIONS_CLIST_ACTUAL_PHASE', 'Fase atual', 'br');
settext('$PARTICIPATIONS_CLIST_NEXT_PHASE', 'Fase por iniciar', 'br');
settext('$PARTICIPATIONS_CLIST_NAME', 'Nome', 'br');
settext('$PARTICIPATIONS_CLIST_END_DATE', 'Data de Início', 'br');
settext('$PARTICIPATIONS_CLIST_START_DATE', 'Data de Fim', 'br');

settext('$CODA_LIST_ADD_BUTTON_CODACAMPAIGNPHASESLIST', '<i class="fa fa-calendar"></i> CALENDARIZAR FASE', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAPRIZESDASHBOARD', '<i class="fa fa-gift"></i> NOVO PRÊMIO', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$DELETE_PHASE_CONFIRM', 'Tem certeza que deseja remover a fase?', 'br');
settext('$DELETE_PARTICIPATION_CONFIRM', 'Tem certeza que deseja remover a participa\u00e7\u00e3o?', 'br');
settext('$DELETE_PRIZE_CONFIRM', 'Tem a certeza que deseja remover o pr\u00e9mio?', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$USERS_MODULE_ERROR_0', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACampaignsLocale_pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$CAMPAIGN_ITEM_FORM_NAME_HELP', 'Defina o título do seu aplicativo. Será utilizado para:<br>- identificar o aplicativo <strong>nesta plataforma</strong>;<br>- identificar o aplicativo <strong> nas redes sociais</strong>.', 'br');
settext('$CAMPAIGN_ITEM_FORM_DESCRIPTION_HELP', 'Pequena descrição do seu aplicativo.', 'br');
settext('$CAMPAIGN_ITEM_FORM_REGULATION_HELP', 'Regulamento da campanha', 'br');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_HELP', 'Indica o nome da fase nesta plataforma e nos aplicativos que apresentarem um calendário.', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION_HELP', 'Texto explicativo sobre o objetivo da fase, utilizado nesta plataforma e nos aplicativos que considerarem este dado.', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE_HELP', 'Data em que a fase ficará ativa, permitindo o acesso às ações referentes à mecânica do aplicativo.<br>O formato aceito para a data é AAAA/MM/DD hh:mm onde:<br>- <b>AAAA</b>: ano com os 4 dígitos (e.g. 2013)<br>- <b>MM</b>: número do mês com máximo de 2 dígitos (e.g. 12, 01)<br>- <b>DD</b>: número do dia do mês com máximo de 2 dígitos (e.g. 21, 4)<br>- <b>hh</b>: hora do dia com máximo de 2 dígitos, aceitando valores entre 0 e 23 (e.g. 22, 01, 9)<br>- <b>mm</b>: minutos da hora com máximo de 2 dígitos e aceitando valores entre 0 e 59 (e.g. 45, 0, 14)', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE_HELP', 'Data em que a fase ficará inativa, negando acesso às ações referentes à mecânica do aplicativo.<br>O formato aceito para a data é AAAA/MM/DD hh:mm onde:<br>- <b>AAAA</b>: ano com os 4 dígitos (e.g. 2013)<br>- <b>MM</b>: número do mês com máximo de 2 dígitos (e.g. 12, 01)<br>- <b>DD</b>: número do dia do mês com máximo de 2 dígitos (e.g. 21, 4)<br>- <b>hh</b>: hora do dia com máximo de 2 dígitos, aceitando valores entre 0 e 23 (e.g. 22, 01, 9)<br>- <b>mm</b>: minutos da hora com máximo de 2 dígitos e aceitando valores entre 0 e 59 (e.g. 45, 0, 14)', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER_HELP', 'Número máximo de participações que cada usuário poderá fazer entre as datas de início e fim da fase.', 'br');
//settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP', 'Forma de atribuição de prêmios para as participações na fase, caso a mecânica e aplicativo permitam a sua atribuição automática.</br> As opções disponíveis são:<br>- <strong>Todos com repetições</strong>: o usuário pode ganhar todos os prêmios, desde que numa participação obtenha o valor mínimo de pontos configurados para cada. Pode ganhar mais do que um exemplar de cada prêmio.<br>- <strong>Todos sem repetições:</strong>: o usuário pode ganhar todos os prêmios, desde que na participação obtenha o valor mínimo de pontos configurados para cada. Apenas pode ganhar um exemplar de cada prêmio.<br>- <strong>Maior com repetições:</strong> o usuário apenas pode ganhar o prêmio que tenha o maior valor mínimo de atribuição abaixo da pontuação obtida pelo usuário. Pode ganhar mais do que um exemplar.<br>- <strong>Maior sem repetições:</strong> o usuário apenas pode ganhar o prêmio que tenha o maior valor mínimo de atribuição abaixo da pontuação obtida pelo usuário. O usuário pode ganhar apenas um exemplar do prêmio.', 'br');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP', 'Forma de atribuição de prêmios para as participações na fase, caso a mecânica e aplicativo permitam a sua atribuição automática.</br> As opções disponíveis são:<br>- <strong>Todos com repetições</strong>: o usuário pode ganhar todos os prêmios. Pode ganhar mais do que um exemplar de cada.<br>- <strong>Todos sem repetições</strong>: o usuário pode ganhar todos os prêmios. Apenas pode ganhar um exemplar de cada.<br>- <strong>Maior com repetições</strong>: o usuário apenas pode ganhar o prêmio que tenha o maior valor mínimo de atribuição abaixo da sua pontuação. Pode ganhar mais do que um exemplar.<br>- <strong>Maior sem repetições:</strong> o usuário apenas pode ganhar o prêmio que tenha o maior valor mínimo de atribuição abaixo da sua pontuação. Apenas pode ganhar um exemplar.', 'br');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE_HELP', 'Fase a que o prêmio pertence. Apenas poderá ser atribuído ou associado às participações da fase selecionada.', 'br');

settext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM_HELP', 'Formul\u00e1rio que ser\u00e1 apresentado para preenchimento na aplica\u00e7\u00e3o quando a fase estiver ativa', 'br');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME_HELP', 'Identificador textual do prêmio a ser utilizado nesta plataforma e no aplicativo. Irá aparecer como nome do prêmio.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION_HELP', 'Descrição textual do prêmio a ser utilizado nesta plataforma e no aplicativo. Irá aparecer como descrição do prêmio.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE_HELP', 'Tipologia do prêmio a ser entregue. Caso o prêmio permita um formato digital, será apresentado ao usuário na página de resultado de participação. As tipologias disponíveis são:<br>- <strong>Físico:</strong> prêmio que não suporta um formato digital e será enviado ou entregue ao vencedor.<br>- <strong>Imagem:</strong> o prêmio é um ficheiro de imagem ou fotografia, em formato JPEG, PNG ou GIF.<br>- <strong>Texto:</strong> o prêmio é um bloco de texto simples.<br>- <strong>Texto (html):</strong> o prêmio é um bloco de texto HTML que poderá conter algumas formatações mais ricas.<br>- <strong>Video:</strong> o prêmio é um ficheiro de video que se encontra na sua cloud e que será apresentado no player do aplicativo.<br>- <strong>Vimeo:</strong> o prêmio é um video que se encontra armazenado no Vimeo e será apresentado no player do Vimeo.<br>- <strong>Voucher:</strong> o prêmio é um voucher gerado pela plataforma e será apresentado ao usuário um link para descarregar o PDF referente ao prêmio. Poderá gerir os vouchers redimidos na área de gestão de vouchers.<br>- <strong>Youtube:</strong> o prêmio é um video que se encontra armazenado no Youtube e será apresentado no player do Youtube.', 'br');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE_HELP', 'Valor mínimo para atribuição do prêmio. Significa que apenas são elegíveis para obtenção do prêmio as participações que tenham uma pontuação igual ou acima deste valor.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK_HELP', 'Número de unidades do prêmio que estão disponíveis para atribuição.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL_HELP', 'Imagem do prêmio que serve para apresentação nesta plataforma e no aplicativo, sempre que uma representação gráfica seja requerida.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP', 'Imagem do prêmio que será considerada como outras imagens de apresentação no aplicativo', 'br');

settext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher será válido, sendo que se:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$PRIZE_TYPE_VOUCHER_START_DATE_HELP', 'Data inicial de validade dos vouchers<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$PRIZE_TYPE_VOUCHER_END_DATE_HELP', 'Data final de validade dos vouchers<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');

settext('$USERS_ITEM_FORM_EMAIL_HELP', 'Endereço de email que será associado ao usuário e será utilizado para contato.', 'br');
settext('$USERS_ITEM_FORM_ID_HELP', 'Nome que será utilizado como identificador textual para o usuário. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para acessar o sistema.', 'br');
settext('$USERS_ITEM_FORM_STATE_HELP', 'Estado', 'br');
settext('$USERS_ITEM_FORM_PASS_HELP', 'Palavra chave que será utilizada nas credenciais (nome, palavra-chave) necessárias para acessar o sistema. ', 'br');
settext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP', 'Confirmar confirmação da palavra-chave introduzida no campo acima.', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACampaignsLocale_Help_pt');

