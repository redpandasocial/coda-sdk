///////////////////////////////////////////////////////////////////
// FORMS
settext('$CAMPAIGN_ITEM_FORM_NAME_HELP', 'Define o título da tua aplicação. Será utilizado para:<br>- identificar a aplicação <strong>nesta plataforma</strong>;<br>- identificar a aplicação em <strong>partilhas nas diversas redes sociais</strong>.', 'pt');
settext('$CAMPAIGN_ITEM_FORM_DESCRIPTION_HELP', 'Descreve sucintamente a tua aplicação.', 'pt');
settext('$CAMPAIGN_ITEM_FORM_REGULATION_HELP', 'Regulamento da campanha', 'pt');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_HELP', 'Nome pelo qual a fase é identificada nesta plataforma e nas aplicações que apresentarem um calendário.', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION_HELP', 'Texto explicativo do propósito da fase, utilizado nesta plataforma e nas aplicações que considerarem este dado.', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE_HELP', 'Data a partir da qual a fase ficará activa, permitindo o acesso às ações referentes à mecânica da aplicação.<br>O formato aceite para a data é AAAA/MM/DD hh:mm onde:<br>- <b>AAAA</b>: ano com os 4 dígitos (e.g. 2013)<br>- <b>MM</b>: número do mês com máximo de 2 dígitos (e.g. 12, 01)<br>- <b>DD</b>: número do dia do mês com máximo de 2 dígitos (e.g. 21, 4)<br>- <b>hh</b>: hora do dia com máximo de 2 dígitos, aceitando valores entre 0 e 23 (e.g. 22, 01, 9)<br>- <b>mm</b>: minutos da hora com máximo de 2 dígitos e aceitando valores entre 0 e 59 (e.g. 45, 0, 14)', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE_HELP', 'Data a partir da qual a fase ficará inactiva, negando o acesso às ações referentes à mecânica da aplicação.<br>O formato aceite para a data é AAAA/MM/DD hh:mm onde:<br>- <b>AAAA</b>: ano com os 4 dígitos (e.g. 2013)<br>- <b>MM</b>: número do mês com máximo de 2 dígitos (e.g. 12, 01)<br>- <b>DD</b>: número do dia do mês com máximo de 2 dígitos (e.g. 21, 4)<br>- <b>hh</b>: hora do dia com máximo de 2 dígitos, aceitando valores entre 0 e 23 (e.g. 22, 01, 9)<br>- <b>mm</b>: minutos da hora com máximo de 2 dígitos e aceitando valores entre 0 e 59 (e.g. 45, 0, 14)', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER_HELP', 'Número máximo de participações que cada utilizador poderá fazer entre as datas de início e fim da fase.', 'pt');
//settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP', 'Forma de atribuição de prémios para as participações na fase, caso a mecânica e aplicação permitam a sua atribuição automática.</br> As opções disponíveis são:<br>- <strong>Todos com repetições</strong>: o utilizador pode ganhar todos os prémios, desde que numa participação obtenha o valor mínimo de pontos configurados para cada. Pode ganhar mais do que um exemplar de cada prémio.<br>- <strong>Todos sem repetições:</strong>: o utilizador pode ganhar todos os prémios, desde que na participação obtenha o valor mínimo de pontos configurados para cada. Apenas pode ganhar um exemplar de cada prémio.<br>- <strong>Maior com repetições:</strong> o utilizador apenas pode ganhar o prémio que tenha o maior valor mínimo de atribuição abaixo da pontuação obtida pelo utilizador. Pode ganhar mais do que um exemplar.<br>- <strong>Maior sem repetições:</strong> o utilizador apenas pode ganhar o prémio que tenha o maior valor mínimo de atribuição abaixo da pontuação obtida pelo utilizador. O utilizador pode ganhar apenas um exemplar do prémio.', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD_HELP', 'Forma de atribuição de prémios para as participações na fase, caso a mecânica e aplicação permitam a sua atribuição automática.</br> As opções disponíveis são:<br>- <strong>Todos com repetições</strong>: o utilizador pode ganhar todos os prémios. Pode ganhar mais do que um exemplar de cada.<br>- <strong>Todos sem repetições</strong>: o utilizador pode ganhar todos os prémios. Apenas pode ganhar um exemplar de cada.<br>- <strong>Maior com repetições</strong>: o utilizador apenas pode ganhar o prémio que tenha o maior valor mínimo de atribuição abaixo da sua pontuação. Pode ganhar mais do que um exemplar.<br>- <strong>Maior sem repetições:</strong> o utilizador apenas pode ganhar o prémio que tenha o maior valor mínimo de atribuição abaixo da sua pontuação. Apenas pode ganhar um exemplar.', 'pt');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE_HELP', 'Fase a que o prémio pertence. Apenas poderá ser atribuído ou associado às participações da fase seleccionada.', 'pt');

settext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM_HELP', 'Formul\u00e1rio que ser\u00e1 apresentado para preenchimento na aplica\u00e7\u00e3o quando a fase estiver ativa', 'pt');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME_HELP', 'Identificador textual do prémio a ser utilizado nesta plataforma e na aplicação. Irá aparecer como nome do prémio.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION_HELP', 'Descrição textual do prémio a ser utilizado nesta plataforma e na aplicação. Irá aparecer como descrição do prémio.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE_HELP', 'Tipologia do prémio a ser entregue. Caso o prémio permita um formato digital, será apresentado ao utilizador na página de resultado de participação. As tipologias disponíveis são:<br>- <strong>Físico:</strong> prémio que não suporta um formato digital e será enviado ou entregue ao vencedor.<br>- <strong>Imagem:</strong> o prémio é um ficheiro de imagem ou fotografia, em formato JPEG, PNG ou GIF.<br>- <strong>Texto:</strong> o prémio é um bloco de texto simples.<br>- <strong>Texto (html):</strong> o prémio é um blcoo de texto HTML que poderá conter algumas formatações mais ricas.<br>- <strong>Video:</strong> o prémio é um ficheiro de video que se encontra na sua cloud e que será apresentado no player da aplicação.<br>- <strong>Vimeo:</strong> o prémio é um video que se encontra armazenado no Vimeo e será apresentado no player do Vimeo.<br>- <strong>Voucher:</strong> o prémio é um voucher gerado pela plataforma e será apresentado ao utilizador um link para descarregar o PDF referente ao prémio. Poderá gerir os vouchers redimidos na área de gestão de vouchers.<br>- <strong>Youtube:</strong> o prémio é um video que se encontra armazenado no Youtube e será apresentado no player do Youtube.', 'pt');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE_HELP', 'Valor mínimo para atribuição do prémio. Significa que apenas são elegíveis para obtenção do prémio as participações que tenham uma pontuação igual ou acima deste valor.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK_HELP', 'Número de unidades do prémio que estão disponíveis para atribuição.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL_HELP', 'Imagem do prémio que serve para apresentação nesta plataforma e na aplicação, sempre que uma representação gráfica seja requerida.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_HELP', 'Imagem do prémio que será considerada como outras imagens de apresentação na aplicação', 'pt');

settext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher será válido, sendo que se:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$PRIZE_TYPE_VOUCHER_START_DATE_HELP', 'Data de início de validade dos vouchers<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$PRIZE_TYPE_VOUCHER_END_DATE_HELP', 'Data de fim de validade dos vouchers<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');

settext('$USERS_ITEM_FORM_EMAIL_HELP', 'Endereço de email a ser associado ao utilizador e utilizado como forma de contacto.', 'pt');
settext('$USERS_ITEM_FORM_ID_HELP', 'Nome a ser utilizado como identificador textual para o utilizador. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema.', 'pt');
settext('$USERS_ITEM_FORM_STATE_HELP', 'Estado', 'pt');
settext('$USERS_ITEM_FORM_PASS_HELP', 'Palavra chave a ser utilizada nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema. ', 'pt');
settext('$USERS_ITEM_FORM_PASS_CONFIRM_HELP', 'Confirmar confirmação da palavra-chave introduzida no campo acima.', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACampaignsLocale_Help_pt');

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$CAMPAIGNS_SHELL_TITLE', 'CALENDARIZAÇÃO e PRÉMIOS', 'pt');

settext('$CAMPAIGNS_PHASE', 'Fase $PHASE_NAME', 'pt');

settext('$CAMPAIGNS_DASHBOARD_TITLE', '<i class="fa fa-globe"></i> M\u00f3dulo de Campanhas', 'pt');
settext('$DASHBOARD_BUTTON_CONFIGURE_CAMPAIGN', 'Informação da Campanha', 'pt');
settext('$DASHBOARD_BUTTON_PHASES', 'Calendarização', 'pt');
settext('$DASHBOARD_BUTTON_PRIZES', 'Pr\u00e9mios', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS', 'Participa\u00e7\u00f5es', 'pt');
settext('$DASHBOARD_BUTTON_WINNERS', 'Vencedores', 'pt');
settext('$DASHBOARD_BUTTON_FORMS', 'Formul\u00e1rios', 'pt');

settext('$DASHBOARD_BUTTON_CONFIGURE_CAMPAIGN_DESC', 'Gere as defini\u00e7\u00f5es da campanha e acede \u00e0s funcionalidades mais relevantes', 'pt');
settext('$DASHBOARD_BUTTON_PHASES_DESC', 'Define a calendariza\u00e7\u00e3o da campanha', 'pt');
settext('$DASHBOARD_BUTTON_PRIZES_DESC', 'Faz a gest\u00e3o dos pr\u00e9mios a atribuir aos participantes', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_DESC', 'Vê quem j\u00e1 participou na tua campanha e consulta os detalhes de cada participa\u00e7\u00e3o', 'pt');

settext('$DASHBOARD_BUTTON_FORMS_DESC', 'Configura aqui os formulários', 'pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$FORM_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$FORM_REGULATION_PANEL', '<i class="fa fa-gavel"></i> Regulamento', 'pt');
settext('$FORM_PHASES_PANEL', '<i class="fa fa-adjust"></i> Fases', 'pt');
settext('$FORM_PARTICIPATIONS_PANEL', '<i class="fa fa-adjust"></i> Participa\u00e7\u00f5es', 'pt');
settext('$FORM_PRIZES_PANEL', '<i class="fa fa-adjust"></i> Pr\u00e9mios', 'pt');
settext('$FORM_PRIZES_IMAGES_PANEL', '<i class="fa fa-picture-o"></i> Imagens', 'pt');
settext('$FORM_PRIZES_ASSIGNMENT_PANEL', '<i class="fa fa-truck"></i> Stock & Entrega', 'pt');
settext('$FORM_PRIZES_STOCK_PANEL', '<i class="fa fa-adjust"></i> Gest\u00e3o de Stock', 'pt');

settext('$LIST_CAMPAIGNPHASE_PROPERTIES', 'Calendarização de Fases', 'pt');
settext('$LIST_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Prémios', 'pt');
settext('$LIST_CAMPAIGNPARTICIPATIONS_PROPERTIES', 'Participações', 'pt');

settext('$NEW_CAMPAIGN_PROPERTIES', 'Nova Campanha', 'pt');
settext('$EDIT_CAMPAIGN_PROPERTIES', 'Editar Campanha ', 'pt');
settext('$ADD_CAMPAIGNPHASE_PROPERTIES', 'Nova Fase ', 'pt');
settext('$EDIT_CAMPAIGNPHASE_PROPERTIES', 'Editar Fase ', 'pt');
settext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES', 'Participa\u00e7\u00e3o ', 'pt');
settext('$EDIT_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Pr\u00e9mio ', 'pt');
settext('$ADD_CAMPAIGNPHASEPRIZE_PROPERTIES', 'Novo Pr\u00e9mio', 'pt');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME_DEFAULT_VALUE', '1ª Fase de Participação', 'pt');

settext('$CAMPAIGN_ITEM_FORM_APPLICATION', 'Aplica\u00e7\u00e3o', 'pt');
settext('$CAMPAIGN_ITEM_FORM_CLIENT', 'Cliente', 'pt');
settext('$CAMPAIGN_ITEM_FORM_NAME', 'T\u00edtulo', 'pt');
settext('$CAMPAIGN_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');

settext('$CAMPAIGN_ITEM_FORM_REGULATION', 'Regulamento', 'pt');

settext('$CAMPAIGN_PHASE_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_START_DATE', 'Data de In\u00edcio', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_END_DATE', 'Data de Fim', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PARTICIPATIONS_NUMBER', 'N\u00famero M\u00e1ximo de Participa\u00e7\u00f5es', 'pt');
settext('$CAMPAIGN_PHASE_ITEM_FORM_PRIZES_METHOD', 'Atribui\u00e7\u00e3o de Pr\u00e9mio', 'pt');
settext('$SDATE_AFTER_EDATE', 'Data de In\u00edcio posterior a Data de Fim', 'pt');
settext('$SDATE_PREVIOUS_NOW', 'Data de In\u00edcio anterior a data atual', 'pt');
settext('$EDATE_BEFORE_SDATE', 'Data de Fim anterior a data de In\u00edcio', 'pt');
settext('$EDATE_PREVIOUS_NOW', 'Data de Fim anterior a data atual', 'pt');

settext('$CAMPAIGN_PHASE_CONTENT_OFFER_FORM', 'Formul\u00e1rio', 'pt');

settext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_CODE', 'all_with_repetitions', 'pt');
settext('$PRIZES_METHOD_ALL_NO_REPETITIONS_CODE', 'all_no_repetitions', 'pt');
settext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_CODE', 'higher_with_repetitions', 'pt');
settext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_CODE', 'higher_no_repetitions', 'pt');

settext('$PRIZES_METHOD_ALL_WITH_REPETITIONS_NAME', 'Todos com repeti\u00e7\u00f5es', 'pt');
settext('$PRIZES_METHOD_ALL_NO_REPETITIONS_NAME', 'Todos sem repeti\u00e7\u00f5es', 'pt');
settext('$PRIZES_METHOD_HIGHER_WITH_REPETITIONS_NAME', 'Maior com repeti\u00e7\u00f5es', 'pt');
settext('$PRIZES_METHOD_HIGHER_NO_REPETITIONS_NAME', 'Maior sem repeti\u00e7\u00f5es', 'pt');

settext('$PARTICIPATION_ITEM_FORM_OWNER', 'Participante', 'pt');
settext('$PARTICIPATION_ITEM_FORM_START_DATE', 'Data de In\u00edcio', 'pt');
settext('$PARTICIPATION_ITEM_FORM_END_DATE', 'Data de Fim', 'pt');
settext('$PARTICIPATION_ITEM_FORM_VALUE', 'Valor', 'pt');
settext('$PARTICIPATION_ITEM_FORM_RANKING', 'Ranking', 'pt');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PHASE', 'Fase', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_URL', 'Apresenta\u00e7\u00e3o', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1', 'Imagem 1', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2', 'Imagem 2', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3', 'Imagem 3', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4', 'Imagem 4', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VALUE', 'Pontuação mínima para atribuição', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_STOCK', 'Stock', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_TYPE', 'Tipo de Pr\u00e9mio', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_PRIZE_CONTENT', 'Conte\u00fado do Pr\u00e9mio', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_MIME_TYPE', 'Mime Type - Será de atribuição automática', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_VOUCHER_TYPE', 'Miniatura 4', 'pt');

settext('$PRIZE_TYPE_PHYSICAL_CODE', 'no/mime', 'pt');
settext('$PRIZE_TYPE_IMAGE_CODE', 'image/*; charset=binary', 'pt');
settext('$PRIZE_TYPE_TEXT_PLAIN_CODE', 'text/plain; charset=utf-8', 'pt');
settext('$PRIZE_TYPE_TEXT_HTML_CODE', 'text/html; charset=utf-8', 'pt');
settext('$PRIZE_TYPE_VIDEO_CODE', 'video/*; charset=binary', 'pt');
settext('$PRIZE_TYPE_VIMEO_CODE', 'embed/vimeo; charset=binary', 'pt');
settext('$PRIZE_TYPE_VOUCHER_PDF_CODE', 'application/pdf; charset=binary', 'pt');
settext('$PRIZE_TYPE_YOUTUBE_CODE', 'embed/youtube; charset=binary', 'pt');

settext('$PRIZE_TYPE_PHYSICAL_NAME', 'Físico (entregue presencialmente)', 'pt');
settext('$PRIZE_TYPE_IMAGE_NAME', 'Imagem', 'pt');
settext('$PRIZE_TYPE_TEXT_PLAIN_NAME', 'Texto', 'pt');
settext('$PRIZE_TYPE_TEXT_HTML_NAME', 'Texto (html)', 'pt');
settext('$PRIZE_TYPE_VIDEO_NAME', 'V\u00eddeo', 'pt');
settext('$PRIZE_TYPE_VIMEO_NAME', 'Vimeo', 'pt');
settext('$PRIZE_TYPE_VOUCHER_PDF_NAME', 'Voucher', 'pt');
settext('$PRIZE_TYPE_YOUTUBE_NAME', 'Youtube', 'pt');

settext('$PRIZE_TYPE_VOUCHER_PDF_MESSAGE', 'N\u00e3o h\u00e1 vouchers dispon\u00edveis', 'pt');
settext('$PRIZE_TYPE_VOUCHER_EXPIRATION_DAYS', 'Validade (em dias)', 'pt');
settext('$PRIZE_TYPE_VOUCHER_START_DATE', 'Data de Validade (desde)', 'pt');
settext('$PRIZE_TYPE_VOUCHER_END_DATE', 'Data de Validade (até)', 'pt');

settext('$CAMPAIGN_PHASE_WIZ_PANEL_TITLE_CODAAPPLICATIONWIZARD', 'Calendarização de Fase Temporal', 'pt');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_DESC_CODAAPPLICATIONWIZARD', 'Define o nome para identificar a fase que pretendes configurar (e.g. "1ª fase de participações" ou "fase de apuramento de resultados").<br>Indica as datas entre as quais os utilizadores poderão participar.<br>Indica o número máximo de participações por utilizador.<br>Indica o tipo de atribuição de prémio.<br><br><i>Podes adicionar mais fases ou alterar estes valores na área de gestão da aplicação.</i><br>', 'pt');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_TITLE_CODAWIZARD', 'Calendarização de Fase Temporal', 'pt');
settext('$CAMPAIGN_PHASE_WIZ_PANEL_DESC_CODAWIZARD', 'Indica o nome identificativo da fase que pretendes configurar (e.g. "1ª fase de participações" ou "fase de apuramento de resultados").<br>Indica as datas entre as quais os utilizadores poderão participar.<br>Indica, se quiseres o número máximo de participações por utilizador.<br>Indica o tipo de atribuição de prémio.<br><br>', 'pt');
settext('$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_TITLE', 'Prémio', 'pt');
settext('$CAMPAIGN_PHASEPRIZE_WIZ_PANEL_DESC', 'Indica o nome e imagem para o prémio.<br>Indica a partir de que pontuação o prémio é atribuído (se aplicável).<br>Indica o stock disponível para este prémio.<br><br>Poderás adicionar mais prémios ou alterar estes valores na área de gestão da aplicação.<br>', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_ICON', '<i class="fa fa-user"></i>', 'pt');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_TOOLTIP', 'Participante', 'pt');

settext('$CAMPAIGNS_LIST_NAME', 'Nome', 'pt');
settext('$CAMPAIGNS_LIST_VALUE', 'Valor', 'pt');
settext('$CAMPAIGNS_LIST_STOCK', 'Stock', 'pt');

settext('$CAMPAIGNS_LIST_PARTICIPANT', 'Participante', 'pt');
settext('$CAMPAIGNS_LIST_START_DATE', 'Data de In\u00edcio', 'pt');
settext('$CAMPAIGNS_LIST_END_DATE', 'Data de Fim', 'pt');
settext('$CAMPAIGNS_LIST_VALUE', 'Valor', 'pt');
settext('$CAMPAIGNS_LIST_STATUS', 'Estado', 'pt');

settext('$CAMPAIGNS_LIST_AWARD_DATE', 'Data de Atribui\u00e7\u00e3o', 'pt');
settext('$CAMPAIGNS_LIST_CONTENT', 'Conte\u00fado', 'pt');

settext('$CAMPAIGNS_LIST_ALLOWED_PARTICIPATIONS', 'Nr Máx Participa\u00e7\u00f5es', 'pt');

settext('$PARTICIPATIONS_CLIST_PREVIOUS_PHASE', 'Fase anterior', 'pt');
settext('$PARTICIPATIONS_CLIST_ACTUAL_PHASE', 'Fase actual', 'pt');
settext('$PARTICIPATIONS_CLIST_NEXT_PHASE', 'Fase por iniciar', 'pt');
settext('$PARTICIPATIONS_CLIST_NAME', 'Nome', 'pt');
settext('$PARTICIPATIONS_CLIST_END_DATE', 'Data de Início', 'pt');
settext('$PARTICIPATIONS_CLIST_START_DATE', 'Data de Fim', 'pt');

settext('$CODA_LIST_ADD_BUTTON_CODACAMPAIGNPHASESLIST', '<i class="fa fa-calendar"></i> CALENDARIZAR FASE', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAPRIZESDASHBOARD', '<i class="fa fa-gift"></i> NOVO PRÉMIO', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$DELETE_PHASE_CONFIRM', 'Tens a certeza que queres remover a fase?', 'pt');
settext('$DELETE_PARTICIPATION_CONFIRM', 'Tens a certeza que queres remover a participa\u00e7\u00e3o?', 'pt');
settext('$DELETE_PRIZE_CONFIRM', 'Tem a certeza que queres remover o pr\u00e9mio?', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$USERS_MODULE_ERROR_0', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACampaignsLocale_pt');

