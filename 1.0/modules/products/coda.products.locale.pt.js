///////////////////////////////////////////////////////////////////
//FORMS
settext('$PRODUCT_ITEM_FORM_NAME_HELP', 'Este é o nome que identifica o produto na plataforma e na aplicação.', 'pt');
settext('$PRODUCT_ITEM_FORM_BASE_REFERENCE_HELP', 'Referência base do produto.<br> A referência base fará parte da referência de todos os modelos que sejam inseridos para o produto.<br> Se um modelo do produto tiver uma referência, esta é concatenada (como sufixo) com a referência base.', 'pt');
settext('$PRODUCT_ITEM_FORM_HASHTAGS_HELP', 'Define hashtags para os teus produtos.<br> Hashtags são termos associados a uma informação, antecedidas pelo símbolo #. Ao defini-las vai ser mais fácil para os utilizadores encontrarem os produtos.', 'pt');
settext('$PRODUCT_ITEM_FORM_TYPE_HELP', 'Tipo de produto.<br> A escolha do tipo de produto tem influência nos dados que podem ser definidos para os modelos.<br> Por exemplo, o modelo de um produto do tipo Vestuário pode ter um tamanho e uma cor.', 'pt');
settext('$PRODUCT_ITEM_FORM_IS_HIGHLIGHT_HELP', 'Os produtos que tenham esta opção seleccionada têm maior destaque na loja relativamente aos outros', 'pt');
settext('$PRODUCT_ITEM_FORM_SPECIFICATIONS_HELP', 'Informação sobre o produto', 'pt');
settext('$PRODUCT_ITEM_FORM_CHARACTERISTICS_HELP', 'Características do modelo de produto.<br> Se tiver sido utilizado o campo de especificações do produto, este apenas deverá ter caracterísitcas específicas do modelo', 'pt');

settext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher é válido, sendo que:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$PRODUCT_TYPE_VOUCHER_START_DATE_HELP', 'Data de início de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');
settext('$PRODUCT_TYPE_VOUCHER_END_DATE_HELP', 'Data de fim de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'pt');

settext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP', 'Referência específica do modelo.<br> Se estiver definida uma referência base para o produto, a do modelo será colocada como sufixo.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP', 'Deve ser usado um modelo de tamanhos base para os modelos de produtos para simplificar a apresentação para os clientes (s, m, l, xl,...)', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP', 'A definição de cores dos modelos deve ser coerente para simplificar a apresentação para os clientes (ex: usar sempre ou amarelo ou amarela para que apenas uma das opções seja considerada)', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_STOCK_HELP', 'Número de unidades a disponibilizar para o modelo de produto.<br> Se só houver um modelo, o stock será corresponderá como o stock do produto.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_STOCK_AVAILABLE_HELP', 'Número de unidades do modelo de produto atualmente disponíveis', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_CURRENCY_HELP', 'Unidade de moeda em que os valores do produto deverão ser definidos', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_NET_VALUE_HELP', 'Valor líquido do produto sem qualquer imposto / desconto / encargo.<br> Se não se quiseres dividir em parcelas os vários valores de um produto, poderá ser indicado neste campo o valor final', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_VAT_HELP', 'Percentagem de imposto a aplicar sobre o valor do produto, já com descontos e comissões incluídos.<br> Deverá ser um valor entre 0 e 1.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_FEE_PERCENTAGE_HELP', 'Encargos associados à compra do produto e que irão ser acrescidos ao valor (ex: custos de envio).<br> Será uma percentagem sobre o valor líquido.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_FEE_AMOUNT_HELP', 'Encargos associados à compra do produto e que irão ser acrescidos ao valor (ex: custos de envio).<br> Será um valor absoluto a acrescentar ao valor líquido.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_PERCENTAGE_HELP', 'Descontos associados à compra do produto e que irão ser retirados ao valor (ex: promoção).<br> Será uma percentagem do valor líquido.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_AMOUNT_HELP', 'Descontos associados à compra do produto e que irão ser retirados ao valor (ex: promoção).<br> Será um valor absoluto a acrescentar ao valor líquido.', 'pt');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas na aplicação.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas na aplicação.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas na aplicação.', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas na aplicação.', 'pt');

settext('$PRODUCTBRAND_ITEM_FORM_NAME_HELP', 'Identificador textual da marca para a identificar nesta plataforma e nas aplicações.', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION_HELP', 'Dados de apresentação da marca que poderá ser apresentada nas aplicações que disponibilizem esta informação', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_URL_HELP', 'Endereço do website da marca. Poderá ser utilizado nas aplicações.', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL_HELP', 'Imagem de apresentação da marca para que os utilizadores possam fazer a sua identificação visual.', 'pt');

KSystem.included('CODAProductsLocale_Help_pt');



///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_ADD_PRODUCT', 'Novo produto', 'pt');
settext('$DASHBOARD_BUTTON_ADD_PRODUCT_DESC', 'Adiciona um novo produto a disponibilizar na aplicação', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTS', 'Produtos', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTS_DESC', 'Gere os produtos e modelos de produto que estão disponíveis na tua aplicação ou consulta o seu detalhe.', 'pt');
settext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND', 'Nova marca', 'pt');
settext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND_DESC', 'Gere as marcas a que queres associar os produtos', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS', 'Marcas', 'pt');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS_DESC', 'Faz a gestão das marcas dos teus produtos. Para cada um podes escolher a marca correspondente', 'pt');
settext('$PRODUCTS_SHELL_TITLE', 'PRODUTOS', 'pt');
settext('$EDIT_PRODUCT_PROPERTIES', 'Editar', 'pt');
settext('$ADD_PRODUCTMODEL_PROPERTIES', 'Novo Modelo de Produto', 'pt');
settext('$EDIT_PRODUCTMODEL_PROPERTIES', 'Editar Modelo de Produto ', 'pt');
settext('$ADD_PRODUCTBRAND_PROPERTIES', 'Nova Marca', 'pt');
settext('$EDIT_PRODUCTBRAND_PROPERTIES', 'Editar Marca ', 'pt');
settext('$DELETE_PRODUCT_CONFIRM', 'Tens a certeza que queres remover o produto?', 'pt');
settext('$DELETE_PRODUCT_BRAND_CONFIRM', 'Tens a certeza que queres remover a marca de produtos? (todos os produtos que a têm associada ficarão sem marca)', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$PRODUCT_ITEM_FORM_BRAND', 'Marca', 'pt');
settext('$PRODUCT_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$PRODUCT_ITEM_FORM_CODE', 'C\u00f3digo', 'pt');
settext('$PRODUCT_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$PRODUCT_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$PRODUCTMODEL_STOCK_PANEL', '<i class="fa fa-truck"></i> Stock & Custo', 'pt');
settext('$PRODUCTMODEL_IMAGES_PANEL', '<i class="fa fa-picture-o"></i> Imagens', 'pt');
settext('$CHECKBOX_TRUE_OPTION', 'SIM', 'pt');

settext('PRODUCT_ITEM_FORM_BASE_REFERENCE', 'Refer\u00eancia base', 'pt');
settext('PRODUCT_ITEM_FORM_HASHTAGS', 'Hashtags', 'pt');
settext('$PRODUCT_ITEM_FORM_TYPE', 'Tipo de Produto', 'pt');
settext('$TYPE_CLOTHING_CODE', 'clothing', 'pt');
settext('$TYPE_CLOTHING_NAME', 'Vestur\u00e1rio', 'pt');
settext('$TYPE_TECHNOLOGY_CODE', 'technology', 'pt');
settext('$TYPE_TECHNOLOGY_NAME', 'Tecnologia', 'pt');
settext('$TYPE_VOUCHER_CODE', 'voucher', 'pt');
settext('$TYPE_VOUCHER_NAME', 'Voucher', 'pt');
settext('$TYPE_OTHER_CODE', 'other', 'pt');
settext('$TYPE_OTHER_NAME', 'Outro', 'pt');

settext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS', 'Validade (em dias)', 'pt');
settext('$PRODUCT_TYPE_VOUCHER_START_DATE', 'Data de Validade (desde)', 'pt');
settext('$PRODUCT_TYPE_VOUCHER_END_DATE', 'Data de Validade (até)', 'pt');

settext('$PRODUCT_ITEM_FORM_URL', 'Url', 'pt');
settext('$PRODUCT_ITEM_FORM_IMAGE_URL', 'Fotografia de Apresenta\u00e7\u00e3o', 'pt');
settext('$PRODUCT_ITEM_FORM_AVAILABLE_SINCE', 'Dispon\u00edvel de', 'pt');
settext('$PRODUCT_ITEM_FORM_AVAILABLE_UNTIL', 'Dispon\u00edvel at\u00e9', 'pt');
settext('$PRODUCT_ITEM_FORM_HIGHLIGHT', '\u00c9 destaque?', 'pt');

settext('$PRODUCT_ITEM_FORM_SPECIFICATIONS', 'Especifica\u00e7\u00f5es', 'pt');
settext('$PRODUCT_PRESENTATION_PANEL', '<i class="fa fa-eye"></i> Apresenta\u00e7\u00e3o', 'pt');
settext('$PRODUCTMODELS_LIST_PANEL', '<i class="fa fa-umbrella"></i> Modelos do Produto', 'pt');

settext('$PRODUCTMODEL_ITEM_REFERENCE', 'Refer\u00eancia', 'pt');
settext('$PRODUCTMODEL_ITEM_SIZE', 'Tamanho', 'pt');
settext('$PRODUCTMODEL_ITEM_COLOUR', 'Cor', 'pt');
settext('$PRODUCTMODEL_ITEM_LENGTH', 'Comprimento', 'pt');
settext('$PRODUCTMODEL_ITEM_HEIGHT', 'Altura', 'pt');
settext('$PRODUCTMODEL_ITEM_WEIGHT', 'Peso', 'pt');
settext('$PRODUCT_ITEM_FORM_CHARACTERISTICS', 'Caracter\u00edsticas', 'pt');
settext('$PRODUCTMODEL_ITEM_STOCK', 'Stock', 'pt');
settext('$PRODUCTMODEL_ITEM_STOCK_AVAILABLE', 'Stock (disponível)', 'pt');
settext('$PRODUCTMODEL_ITEM_CURRENCY', 'Moeda', 'pt');
settext('$CURRENCY_BRL_NAME', 'Real do Brasil', 'pt');
settext('$CURRENCY_EUR_NAME', 'Euro', 'pt');
settext('$CURRENCY_BTC_NAME', 'Bitcoin', 'pt');
settext('$PRODUCTMODEL_ITEM_NET_VALUE', 'Valor l\u00edquido', 'pt');
settext('$PRODUCTMODEL_ITEM_VAT', 'Taxa', 'pt');
settext('$PRODUCTMODEL_ITEM_FEE_PERCENTAGE', 'Encargos (percentagem)', 'pt');
settext('$PRODUCTMODEL_ITEM_FEE_AMOUNT', 'Encargos (valor absoluto)', 'pt');
settext('$PRODUCTMODEL_ITEM_DISCOUNT_PERCENTAGE', 'Desconto (percentagem)', 'pt');
settext('$PRODUCTMODEL_ITEM_DISCOUNT_AMOUNT', 'Desconto (valor absoluto)', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1', 'Imagem 1', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2', 'Imagem 2', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3', 'Imagem 3', 'pt');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4', 'Imagem 4', 'pt');
settext('$PRODUCTMODEL_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');

settext('$PRODUCT_ADD_NEW', 'Novo Produto', 'pt');

settext('$PRODUCTBRAND_ITEM_FORM_CODE', 'C\u00f3digo', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_URL', 'Endereço do website', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL', 'Logótipo', 'pt');
settext('$PRODUCTBRAND_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//LISTS
settext('$DELETE_PRODUCTMODEL_CONFIRM', 'Tens a certeza que queres remover o modelo do produto?', 'pt');
settext('$PRODUCTMODEL_MANAGE_STOCK', 'Stock e Custo', 'pt');
settext('$PRODUCTMODEL_MANAGE_MULTIMEDIA', 'Imagens', 'pt');

settext('$PRODUCTMODEL_LIST_REFERENCE', 'Refer\u00eancia', 'pt');
settext('$PRODUCTMODEL_LIST_STOCK_AVAILABLE', 'Stock (disponível)', 'pt');
settext('$PRODUCTMODEL_LIST_TOTAL_VALUE', 'Valor total', 'pt');

settext('$CODA_LIST_EXPORT_BUTTON_CODAPRODUCTSLISTDASHBOARD', 'EXPORTAR PRODUTOS', 'pt');
settext('$CODA_LIST_PRODUCTS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$PRODUCTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//WIZARDS
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE1', 'Dados base', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO1', 'Dados iniciais.<br>Dependendo do tipo de produto seleccionado est\u00e3o dispon\u00edveis campos espec\u00edficos no pr\u00f3ximo ecr\u00e3.', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE2', 'Dados espec\u00edficos', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO2', 'Caracteriza os dados específicos para o tipo de produto.', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE3', 'Stock e Custo', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO3', 'Define o custo de aquisição e respectivo stock se o quiseres vender online<br> Podes optar por editar esta informação posteriormente pela listagem de produtos.', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE4', 'Imagem do produto', 'pt');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO4', 'Se já tiveres uma imagem adiciona-a aqui.<br> Se quiseres poderás adicionar mais imagens para o produto através da listagem de produtos.', 'pt');

settext('$CODA_PRODUCTS_LIST_TITLE', 'Produtos', 'pt');
settext('$CODA_BRANDS_LIST_TITLE', 'Marcas', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTSLISTDASHBOARD', '<i class="fa fa-book"></i> Novo produto', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTBRANDSLISTDASHBOARD', '<i class="fa fa-bullseye"></i> Nova marca', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTMODELSLIST', '<i class="fa fa-umbrella"></i> Novo modelo', 'pt');

settext('$PRODUCTBRAND_ITEM_FORM_PANEL_TITLE', 'Dados da Marca', 'pt');
settext('$PRODUCTBRAND_ITEM_FORM_PANEL_DESC', 'Introduz os dados da marca de produtos.<br>Define o nome, o endereço do website e a imagem de apresentação.<br><br>Podes alterar os dados de cada marca pela sua seleção na listagem.', 'pt');
settext('$PRODUCT_ITEM_FORM_PANEL_TITLE', 'Produto', 'pt');
settext('$PRODUCT_ITEM_FORM_PANEL_DESC', 'Introduz o nome do produto que queres disponibilizar na sua aplicação.<br>Indica que termos de pesquisa (hashtags) deverão ficar associados ao teu produto. Cada hashtag deverá começar por «#».<br><br>Poderás alterar estes dados e introduzir mais produtos na área de gestão de produtos.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_TITLE', 'Modelo de Produto', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_DESC', 'Introduz o modelo do produto, preenchendo os dados que melhor o caracterizam. O único obrigatório é a referência.<br><br><strong>NOTA:</strong> um modelo é um sub-conjunto de características de um determinado produto. Por exemplo, o produto "Botas de Cabedal" pode ter vários modelos, diferenciados pela cor e tamanho.<br><br>Poderás alterar estes dados e introduzir mais modelos através do produto.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_TITLE', 'Stock & Preço do Modelo', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_DESC', 'Introduz o stock, preço, comissões e descontos associados ao modelo.<br><br>Poderás alterar estes dados através da edição do modelo de produto.', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_TITLE', 'Fotografia do Modelo', 'pt');
settext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_DESC', 'Introduz a fotografia associada ao Modelo configurado.<br><br>Poderás alterá-la e associar mais fotografias através da edição do modelo de produto.', 'pt');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAProductsLocale_pt');
