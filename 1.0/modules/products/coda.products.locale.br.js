///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_ADD_PRODUCT', 'Novo produto', 'br');
settext('$DASHBOARD_BUTTON_ADD_PRODUCT_DESC', 'Adicione um novo produto', 'br');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTS', 'Produtos', 'br');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTS_DESC', 'Crie os produtos e modelos de produto que estão disponíveis na seu aplicativo ou consulte o detalhe.', 'br');
settext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND', 'Nova marca', 'br');
settext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND_DESC', 'Crie as marcas que deseja associar os produtos', 'br');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS', 'Marcas', 'br');
settext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS_DESC', 'Faça a gestão das marcas dos seus produtos. Para cada produto você escolhe a marca correspondente', 'br');
settext('$PRODUCTS_SHELL_TITLE', 'PRODUTOS', 'br');
settext('$EDIT_PRODUCT_PROPERTIES', 'Editar', 'br');
settext('$ADD_PRODUCTMODEL_PROPERTIES', 'Novo Modelo de Produto', 'br');
settext('$EDIT_PRODUCTMODEL_PROPERTIES', 'Editar Modelo de Produto ', 'br');
settext('$ADD_PRODUCTBRAND_PROPERTIES', 'Nova Marca', 'br');
settext('$EDIT_PRODUCTBRAND_PROPERTIES', 'Editar Marca ', 'br');
settext('$DELETE_PRODUCT_CONFIRM', 'Tem certeza que deseja remover o produto?', 'br');
settext('$DELETE_PRODUCT_BRAND_CONFIRM', 'Tem certeza que deseja remover essa a marca? (todos os produtos associados, ficarão sem marca)', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$PRODUCT_ITEM_FORM_BRAND', 'Marca', 'br');
settext('$PRODUCT_ITEM_FORM_NAME', 'Nome', 'br');
settext('$PRODUCT_ITEM_FORM_CODE', 'C\u00f3digo', 'br');
settext('$PRODUCT_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$PRODUCT_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$PRODUCTMODEL_STOCK_PANEL', '<i class="fa fa-truck"></i> Stock & Custo', 'br');
settext('$PRODUCTMODEL_IMAGES_PANEL', '<i class="fa fa-picture-o"></i> Imagens', 'br');
settext('$CHECKBOX_TRUE_OPTION', 'SIM', 'br');

settext('PRODUCT_ITEM_FORM_BASE_REFERENCE', 'Refer\u00eancia base', 'br');
settext('PRODUCT_ITEM_FORM_HASHTAGS', 'Hashtags', 'br');
settext('$PRODUCT_ITEM_FORM_TYPE', 'Tipo de Produto', 'br');
settext('$TYPE_CLOTHING_CODE', 'clothing', 'br');
settext('$TYPE_CLOTHING_NAME', 'Vestur\u00e1rio', 'br');
settext('$TYPE_TECHNOLOGY_CODE', 'technology', 'br');
settext('$TYPE_TECHNOLOGY_NAME', 'Tecnologia', 'br');
settext('$TYPE_VOUCHER_CODE', 'voucher', 'br');
settext('$TYPE_VOUCHER_NAME', 'Voucher', 'br');
settext('$TYPE_OTHER_CODE', 'other', 'br');
settext('$TYPE_OTHER_NAME', 'Outro', 'br');

settext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS', 'Validade (em dias)', 'br');
settext('$PRODUCT_TYPE_VOUCHER_START_DATE', 'Data de Validade (desde)', 'br');
settext('$PRODUCT_TYPE_VOUCHER_END_DATE', 'Data de Validade (até)', 'br');

settext('$PRODUCT_ITEM_FORM_URL', 'Url', 'br');
settext('$PRODUCT_ITEM_FORM_IMAGE_URL', 'Fotografia de Apresenta\u00e7\u00e3o', 'br');
settext('$PRODUCT_ITEM_FORM_AVAILABLE_SINCE', 'Dispon\u00edvel de', 'br');
settext('$PRODUCT_ITEM_FORM_AVAILABLE_UNTIL', 'Dispon\u00edvel at\u00e9', 'br');
settext('$PRODUCT_ITEM_FORM_HIGHLIGHT', '\u00c9 destaque?', 'br');

settext('$PRODUCT_ITEM_FORM_SPECIFICATIONS', 'Especifica\u00e7\u00f5es', 'br');
settext('$PRODUCT_PRESENTATION_PANEL', '<i class="fa fa-eye"></i> Apresenta\u00e7\u00e3o', 'br');
settext('$PRODUCTMODELS_LIST_PANEL', '<i class="fa fa-umbrella"></i> Modelos do Produto', 'br');

settext('$PRODUCTMODEL_ITEM_REFERENCE', 'Refer\u00eancia', 'br');
settext('$PRODUCTMODEL_ITEM_SIZE', 'Tamanho', 'br');
settext('$PRODUCTMODEL_ITEM_COLOUR', 'Cor', 'br');
settext('$PRODUCTMODEL_ITEM_LENGTH', 'Comprimento', 'br');
settext('$PRODUCTMODEL_ITEM_HEIGHT', 'Altura', 'br');
settext('$PRODUCTMODEL_ITEM_WEIGHT', 'Peso', 'br');
settext('$PRODUCT_ITEM_FORM_CHARACTERISTICS', 'Caracter\u00edsticas', 'br');
settext('$PRODUCTMODEL_ITEM_STOCK', 'Stock', 'br');
settext('$PRODUCTMODEL_ITEM_STOCK_AVAILABLE', 'Estoque(disponível)', 'br');
settext('$PRODUCTMODEL_ITEM_CURRENCY', 'Moeda', 'br');
settext('$CURRENCY_BRL_NAME', 'Real Brasileiro', 'br');
settext('$CURRENCY_EUR_NAME', 'Euro', 'br');
settext('$CURRENCY_BTC_NAME', 'Bitcoin', 'br');
settext('$PRODUCTMODEL_ITEM_NET_VALUE', 'Valor l\u00edquido', 'br');
settext('$PRODUCTMODEL_ITEM_VAT', 'Taxa', 'br');
settext('$PRODUCTMODEL_ITEM_FEE_PERCENTAGE', 'Encargos (percentagem)', 'br');
settext('$PRODUCTMODEL_ITEM_FEE_AMOUNT', 'Encargos (valor absoluto)', 'br');
settext('$PRODUCTMODEL_ITEM_DISCOUNT_PERCENTAGE', 'Desconto (percentagem)', 'br');
settext('$PRODUCTMODEL_ITEM_DISCOUNT_AMOUNT', 'Desconto (valor absoluto)', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1', 'Imagem 1', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2', 'Imagem 2', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3', 'Imagem 3', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4', 'Imagem 4', 'br');
settext('$PRODUCTMODEL_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');

settext('$PRODUCT_ADD_NEW', 'Novo Produto', 'br');

settext('$PRODUCTBRAND_ITEM_FORM_CODE', 'C\u00f3digo', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_NAME', 'Nome', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_URL', 'Endereço do website', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL', 'Logótipo', 'br');
settext('$PRODUCTBRAND_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//LISTS
settext('$DELETE_PRODUCTMODEL_CONFIRM', 'Tem certeza que deseja remover o modelo do produto?', 'br');
settext('$PRODUCTMODEL_MANAGE_STOCK', 'Estoque e Custo', 'br');
settext('$PRODUCTMODEL_MANAGE_MULTIMEDIA', 'Imagens', 'br');

settext('$PRODUCTMODEL_LIST_REFERENCE', 'Refer\u00eancia', 'br');
settext('$PRODUCTMODEL_LIST_STOCK_AVAILABLE', 'Estoque (disponível)', 'br');
settext('$PRODUCTMODEL_LIST_TOTAL_VALUE', 'Valor total', 'br');

settext('$CODA_LIST_EXPORT_BUTTON_CODAPRODUCTSLISTDASHBOARD', 'EXPORTAR PRODUTOS', 'br');
settext('$CODA_LIST_PRODUCTS_REPORT', 'Clique no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$PRODUCTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//WIZARDS
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE1', 'Dados base', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO1', 'Dados iniciais.<br>Dependendo do tipo de produto selecionado est\u00e3o dispon\u00edveis campos espec\u00edficos no pr\u00f3ximo ecr\u00e3.', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE2', 'Dados espec\u00edficos', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO2', 'Caracteriza os dados específicos para o tipo de produto.', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE3', 'Estoque e Custo', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO3', 'Defina o custo de aquisição e respectivo estoque se deseja vender online<br> Você podes optar por editar esta informação posteriormente pela listagem de produtos.', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE4', 'Imagem do produto', 'br');
settext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO4', 'Se já tiver uma imagem adicione aqui.<br> Se desejar adicione mais imagens para o produto através da listagem de produtos.', 'br');

settext('$CODA_PRODUCTS_LIST_TITLE', 'Produtos', 'br');
settext('$CODA_BRANDS_LIST_TITLE', 'Marcas', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTSLISTDASHBOARD', '<i class="fa fa-book"></i> Novo produto', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTBRANDSLISTDASHBOARD', '<i class="fa fa-bullseye"></i> Nova marca', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAPRODUCTMODELSLIST', '<i class="fa fa-umbrella"></i> Novo modelo', 'br');

settext('$PRODUCTBRAND_ITEM_FORM_PANEL_TITLE', 'Dados da Marca', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_PANEL_DESC', 'Preencha os dados da marca dos produtos.<br>Defina o nome, o endereço do website e a imagem de apresentação.<br><br>Se desajar você pode alterar os dados de cada marca pela sua seleção na listagem.', 'br');
settext('$PRODUCT_ITEM_FORM_PANEL_TITLE', 'Produto', 'br');
settext('$PRODUCT_ITEM_FORM_PANEL_DESC', 'Preencha o nome do produto que deseja disponibilizar na seu aplicativo.<br>Indique que termos de pesquisa (hashtags) deverão ficar associados ao seu produto. Cada hashtag deve começar por «#».<br><br>Se desejar altere esses dados e adicione mais produtos na área de gestão de produtos.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_TITLE', 'Modelo de Produto', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_DESC', 'Preencha o modelo do produto, com os dados que o caracterizam melhor. O único obrigatório é a referência.<br><br><strong>NOTA:</strong> um modelo é um sub-conjunto de características de um determinado produto. Por exemplo, o produto "Botas de Cabedal" pode ter vários modelos, diferenciados pela cor e tamanho.<br><br>Se desejar altere esses dados e adicione mais modelos através do produto.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_TITLE', 'Estoque & Preço do Modelo', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_DESC', 'Preencha o estoque, preço, comissões e descontos associados ao modelo.<br><br>Se desejar altere esses dados através da edição do modelo de produto.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_TITLE', 'Fotografia do Modelo', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_DESC', 'Adicione a fotografia associada ao Modelo configurado.<br><br>Se desejar, associe mais fotografias através da edição do modelo de produto.', 'br');
///////////////////////////////////////////////////////////////////

KSystem.included('CODAProductsLocale_pt');
///////////////////////////////////////////////////////////////////
//FORMS
settext('$PRODUCT_ITEM_FORM_NAME_HELP', 'Nome que identifica o produto na plataforma e no aplicativo.', 'br');
settext('$PRODUCT_ITEM_FORM_BASE_REFERENCE_HELP', 'Referência base do produto.<br> A referência base fará parte da referência de todos os modelos que sejam inseridos para o produto.<br> Se um modelo do produto tiver uma referência, esta é concatenada (como sufixo) com a referência base.', 'br');
settext('$PRODUCT_ITEM_FORM_HASHTAGS_HELP', 'Define hashtags para os teus produtos.<br> Hashtags são termos associados a uma informação, antecedidas pelo símbolo #. Ao defini-las vai ser mais fácil para os usuários encontrarem os produtos.', 'br');
settext('$PRODUCT_ITEM_FORM_TYPE_HELP', 'Tipo de produto.<br> A escolha do tipo de produto tem influência nos dados que podem ser definidos para os modelos.<br> Por exemplo, o modelo de um produto do tipo Vestuário pode ter um tamanho e uma cor.', 'br');
settext('$PRODUCT_ITEM_FORM_IS_HIGHLIGHT_HELP', 'Os produtos que tenham esta opção selecionada têm relativamento maior destaque na loja aos outros', 'br');
settext('$PRODUCT_ITEM_FORM_SPECIFICATIONS_HELP', 'Informação sobre o produto', 'br');
settext('$PRODUCT_ITEM_FORM_CHARACTERISTICS_HELP', 'Características do modelo de produto.<br> Se tiver sido usado o campo de especificações do produto, este apenas deverá ter caracterísitcas específicas do modelo', 'br');

settext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS_HELP', 'Número de dias em que o voucher é válido, sendo que:<br>- <b>Datas de validade não estão definidas:</b> voucher terá como data de início de validade a data do dia em que é gerado e será válido durante x dias até expirar<br>- <b>Apenas data de validade inicial está definida:</b>voucher terá como data de início de validade a que estiver definida e será válido durante x dias até expirar<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$PRODUCT_TYPE_VOUCHER_START_DATE_HELP', 'Data de início de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');
settext('$PRODUCT_TYPE_VOUCHER_END_DATE_HELP', 'Data de fim de validade dos vouchers gerados com base nesta configuração<br><b>NOTA:</b>&nbsp;Se não estiverem definidas datas de validade e número de dias para expiração o voucher não terá validade', 'br');

settext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP', 'Referência específica do modelo.<br> Se estiver definida uma referência base para o produto, a do modelo será colocada como sufixo.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP', 'Deve ser usado um modelo de tamanhos base para os modelos de produtos para simplificar a apresentação para os clientes (s, m, l, xl,...)', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP', 'A definição de cores dos modelos deve ser coerente para simplificar a apresentação para os clientes (ex: usar sempre ou amarelo ou amarela para que apenas uma das opções seja considerada)', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP', 'Em todos os produtos deve ser utilizada a mesma unidade de medida para simplificar a apresentação para os clientes', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_estoque_HELP', 'Número de unidades a disponibilizar para o modelo de produto.<br> Se só houver um modelo, o estoque será corresponderá como o estoque do produto.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_estoque_AVAILABLE_HELP', 'Número de unidades do modelo de produto atualmente disponíveis', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_CURRENCY_HELP', 'Unidade de moeda em que os valores do produto deverão ser definidos', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_NET_VALUE_HELP', 'Valor líquido do produto sem nenhum imposto / desconto / encargo.<br> Se não desejar dividir em parcelas os vários valores de um produto, poderá ser indicado neste campo o valor final', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_VAT_HELP', 'Percentagem de imposto a aplicar sobre o valor do produto, já com descontos e comissões incluídos.<br> Deverá ser um valor entre 0 e 1.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_FEE_PERCENTAGE_HELP', 'Encargos associados à compra do produto e que irão ser acrescidos ao valor (ex: custos de envio).<br> Será uma percentagem sobre o valor líquido.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_FEE_AMOUNT_HELP', 'Encargos associados à compra do produto e que irão ser acrescidos ao valor (ex: custos de envio).<br> Será um valor absoluto a acrescentar ao valor líquido.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_PERCENTAGE_HELP', 'Descontos associados à compra do produto e que irão ser retirados ao valor (ex: promoção).<br> Será uma percentagem do valor líquido.', 'br');
settext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_AMOUNT_HELP', 'Descontos associados à compra do produto e que irão ser retirados ao valor (ex: promoção).<br> Será um valor absoluto a acrescentar ao valor líquido.', 'br');

settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas no aplicativo.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas no aplicativo.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas no aplicativo.', 'br');
settext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4_HELP', 'Uma imagem para o modelo de produto. Cada modelo pode ter várias imagens que serão mostradas no aplicativo.', 'br');

settext('$PRODUCTBRAND_ITEM_FORM_NAME_HELP', 'Texto para identificar a marca nesta plataforma e nos aplicativos.', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION_HELP', 'Dados de apresentação da marca que poderá ser apresentada nos aplicativos que disponibilizem esta informação', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_URL_HELP', 'Endereço do website da marca. Poderá ser utilizado nos aplicativos.', 'br');
settext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL_HELP', 'Imagem de apresentação da marca para que os usuários possam fazer a sua identificação visual.', 'br');

KSystem.included('CODAProductsLocale_Help_pt');



