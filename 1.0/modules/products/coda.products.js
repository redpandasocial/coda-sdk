KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAProductBrandItemForm(parent, href){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		this.target.collection = '/productbrands';
		this.nillable = true;
		this.single = true;
	}
}

KSystem.include([
	'CODAForm',
	'KInput',
	'KTextArea'
], function() {
	CODAProductBrandItemForm.prototype = new CODAForm();
	CODAProductBrandItemForm.prototype.constructor = CODAProductBrandItemForm;

	CODAProductBrandItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAProductBrandItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAProductBrandItemForm.prototype.addMinimizedPanel = function() {
		if(!!this.data && !!this.data.code){
			this.setTitle(gettext('$EDIT_PRODUCTBRAND_PROPERTIES') + ' \u00ab' + this.data.code + '\u00bb');
		}else{
			this.setTitle(gettext('$ADD_PRODUCTBRAND_PROPERTIES'));
		}

		var productbrand_item_panel = new KPanel(this);
		{

			productbrand_item_panel.hash = '/general';

			var code = new KHidden(productbrand_item_panel, 'code');
			{
				if(!!this.data && !!this.data.code){
					code.setValue(this.data.code);
				}
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			productbrand_item_panel.appendChild(code);

			var name = new KInput(productbrand_item_panel, gettext('$PRODUCTBRAND_ITEM_FORM_NAME') + ' *', 'name');
			{
				if(!!this.data && !!this.data.name){
					name.setValue(this.data.name);
				}
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				name.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
			}
			productbrand_item_panel.appendChild(name);

			var description = new KTextArea(productbrand_item_panel, gettext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION'), 'description');
			{
				if (!!this.data && !!this.data.description){
					description.setValue(this.data.description);
				}
				description.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());
			}
			productbrand_item_panel.appendChild(description);

			var url = new KInput(productbrand_item_panel, gettext('$PRODUCTBRAND_ITEM_FORM_URL'), 'url');
			{
				if (!!this.data && !!this.data.url) {
					url.setValue(this.data.url);
				}
				url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
				url.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_URL_HELP'), CODAForm.getHelpOptions());
			}
			productbrand_item_panel.appendChild(url);

			var product_brand_thumbnail = new KImageUpload(productbrand_item_panel, gettext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL'), 'thumbnail', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_1) {
					product_brand_thumbnail.setValue(this.data.thumbnail_1);
				}
				var mediaList = new CODAMediaList(product_brand_thumbnail, 'image/*');
				product_brand_thumbnail.appendChild(mediaList);	
				product_brand_thumbnail.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());			
			}
			productbrand_item_panel.appendChild(product_brand_thumbnail);

		}

		this.addPanel(productbrand_item_panel, gettext('$PRODUCTBRAND_GENERALINFO_PANEL'), true);

	};

	CODAProductBrandItemForm.prototype.setDefaults = function(params) {
		delete params.requestTypes;
		delete params.responseTypes;
	};

	CODAProductBrandItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$PRODUCTBRAND_ITEM_FORM_PANEL_TITLE');
			firstPanel.description = gettext('$PRODUCTBRAND_ITEM_FORM_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-bullseye';

			var name = new KInput(firstPanel, gettext('$PRODUCTBRAND_ITEM_FORM_NAME') + ' *', 'name');
			{
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				name.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(name);

			var url = new KInput(firstPanel, gettext('$PRODUCTBRAND_ITEM_FORM_URL'), 'url');
			{
				url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
				url.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_URL_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(url);

			var product_brand_thumbnail = new KImageUpload(firstPanel, gettext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL'), 'thumbnail', CODAGenericShell.getUploadAuthorization());
			{
				var mediaList = new CODAMediaList(product_brand_thumbnail, 'image/*');
				product_brand_thumbnail.appendChild(mediaList);	
				product_brand_thumbnail.setHelp(gettext('$PRODUCTBRAND_ITEM_FORM_THUMBNAIL_HELP'), CODAForm.getHelpOptions());		
			}
			firstPanel.appendChild(product_brand_thumbnail);
		}
		return [firstPanel];
	};

	CODAProductBrandItemForm.prototype.onPost = CODAProductBrandItemForm.prototype.onCreated = function(data, request){
//		this.enable();
//		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		this.parent.closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-brands'
		});
		return;
	};

	CODAProductBrandItemForm.prototype.onPut = function(data, request){
		this.refresh();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAProductBrandItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAProductBrandItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAProductBrandsListDashboard(parent, options, apps) {
	if (parent != null) {
		CODAFormIconDashboard.call(this, parent, options, apps);
		this.setTitle(gettext('$CODA_BRANDS_LIST_TITLE'));
		this.filterFields = [ 'name' ];
	}
}

KSystem.include([
    'KConfirmDialog',
	'CODAIcon',
	'CODAFormIconDashboard'
], function() {
	CODAProductBrandsListDashboard.prototype = new CODAFormIconDashboard();
	CODAProductBrandsListDashboard.prototype.constructor = CODAProductBrandsListDashboard;

	CODAProductBrandsListDashboard.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-brand-wizard'
		});
	};

	CODAProductBrandsListDashboard.prototype.getIconFor = function(data) {

		var dummy = window.document.createElement('p');
		if(!!data.description){
			dummy.innerHTML = data.description;
		}else{
			dummy.innerHTML = '';
		}

		var aditional = '';

		var actions = [];
		actions.push({
			icon : 'css:fa fa-pencil',
			link : '#/edit' + data.href,
			desc : 'Gerir'
		});
		actions.push({
			icon : 'css:fa fa-trash-o',
			desc : 'Remover',
			events : [
			{
				name : 'click',
				callback : CODAProductBrandsListDashboard.remove
			}

			]
		});

		return new CODAIcon(this, {
			icon : !!data.thumbnail ? data.thumbnail : 'css:fa fa-gift',
			activate : false,
			title : data.name,
			href : data.href,
			description : '<span>' + dummy.textContent + '</span>' + aditional,
			actions : actions
		});
	};

	CODAProductBrandsListDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PRODUCT_BRAND_CONFIRM'),
			callback : function() {
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onProductBrandDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400
		});
	};

	CODAProductBrandsListDashboard.prototype.onProductBrandDelete = function(data, request) {
		this.getParent('KFloatable').closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-brands' + request.service
		});
		return;
	};

	KSystem.included('CODAProductBrandsListDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductItemForm(parent, href, layout, parentHref, type) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = '/products';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText',
	'KCalendar'
], function() {
	CODAProductItemForm.prototype = new CODAForm();
	CODAProductItemForm.prototype.constructor = CODAProductItemForm;

	CODAProductItemForm.prototype.load = function() {
		if(!!this.getShell().app_data && !!this.getShell().app_data && !!this.getShell().app_data.type){
			this.kinky.get(this, 'rest:/productbrands', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadProductBrands');
		}else{
			CODAForm.prototype.load.call(this);
		}
	};

	CODAProductItemForm.prototype.onLoadProductBrands = function(data, request) {

		if(!!data && !!data.elements && !!data.elements.length > 0){
			this.brands_for_products = data.elements;
		}

		CODAForm.prototype.load.call(this);
	};

	CODAProductItemForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAProductItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

	};

	CODAProductItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_PRODUCT_PROPERTIES') + ' \u00ab' + this.target.data.name + '\u00bb');
		}else{
			this.setTitle('Inserir ' + gettext('$EDIT_PRODUCT_PROPERTIES'));
		}

		var product_general_data = new KPanel(this);
		{

			product_general_data.hash = '/generaldata';

			var code = new KHidden(product_general_data, 'code');
			{
				if (!!this.data && !!this.data.code) {
					code.setValue(this.data.code);
				}
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			product_general_data.appendChild(code);

			var name = new KInput(product_general_data, gettext('$PRODUCT_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$PRODUCT_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			product_general_data.appendChild(name);

			var description = new KTextArea(product_general_data, gettext('$PRODUCT_ITEM_FORM_DESCRIPTION'), 'description');
			{
				if (!!this.data && !!this.data.description){
					description.setValue(this.data.description);
				}
			}
			product_general_data.appendChild(description);

			if (!!this.brands_for_products){
				var brands = new KCombo(product_general_data, gettext('$PRODUCT_ITEM_FORM_BRAND'), 'productBrand_id');
				{
					for (var index in this.brands_for_products ){
						brands.addOption(this.brands_for_products [index].id, this.brands_for_products [index].name, (!!this.data && !!this.data.productBrand_id && this.data.productBrand_id == this.brands_for_products[index].id) ? true : false);
					}
				}
				product_general_data.appendChild(brands);
			}

			var base_reference = new KInput(product_general_data, gettext('PRODUCT_ITEM_FORM_BASE_REFERENCE'), 'base_reference');
			{
				if (!!this.data && !!this.data.base_reference) {
					base_reference.setValue(this.data.base_reference);
				}
				base_reference.setHelp(gettext('$PRODUCT_ITEM_FORM_BASE_REFERENCE_HELP'), CODAForm.getHelpOptions());
			}
			product_general_data.appendChild(base_reference);

			var hashtags = new KInput(product_general_data, gettext('PRODUCT_ITEM_FORM_HASHTAGS'), 'hashtags');
			{
				if (!!this.data && !!this.data.hashtags) {
					hashtags.setValue(this.data.hashtags);
				}
				hashtags.setHelp(gettext('$PRODUCT_ITEM_FORM_HASHTAGS_HELP'), CODAForm.getHelpOptions());
			}
			product_general_data.appendChild(hashtags);

			var type_selector = new KCombo(product_general_data, gettext('$PRODUCT_ITEM_FORM_TYPE') + ' *', 'type');
			{
				type_selector.addOption(gettext('$TYPE_CLOTHING_CODE'), gettext('$TYPE_CLOTHING_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_CLOTHING_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_TECHNOLOGY_CODE'), gettext('$TYPE_TECHNOLOGY_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_TECHNOLOGY_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_VOUCHER_CODE'), gettext('$TYPE_VOUCHER_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_VOUCHER_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_OTHER_CODE'), gettext('$TYPE_OTHER_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_OTHER_CODE')) ? true : false);
				type_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				type_selector.setHelp(gettext('$PRODUCT_ITEM_FORM_TYPE_HELP'), CODAForm.getHelpOptions());
				
				type_selector.addEventListener('change', CODAProductItemForm.onTypeSelected);
			
			}
			type_selector.setHelp(gettext('$PRODUCT_ITEM_FORM_TYPE_HELP'), CODAForm.getHelpOptions());
			product_general_data.appendChild(type_selector);

			if(!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_VOUCHER_CODE')){
				var expiration_days = new KInput(product_general_data, gettext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS'), 'expiration_days');
				{
					if (!!this.data && !!this.data.expiration_days) {
						expiration_days.setValue(this.data.expiration_days);
					};
					expiration_days.setLength(8);
					expiration_days.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
					expiration_days.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS_HELP'), CODAForm.getHelpOptions());
					expiration_days.hash = '/expiration-days';
					product_general_data.appendChild(expiration_days);
				}
				var start_date = new KDate(product_general_data, gettext('$PRODUCT_TYPE_VOUCHER_START_DATE') + ' *', 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					if (!!this.data && !!this.data.start_date) {
						start_date.setValue(this.data.start_date);
					};
					start_date.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_START_DATE_HELP'), CODAForm.getHelpOptions());
					start_date.hash = '/start-date';
					product_general_data.appendChild(start_date);
				}
				var end_date = new KDate(product_general_data, gettext('$PRODUCT_TYPE_VOUCHER_END_DATE') + ' *', 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
				{
					if (!!this.data && !!this.data.end_date) {
						end_date.setValue(this.data.end_date);
					};
					end_date.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_END_DATE_HELP'), CODAForm.getHelpOptions());
					end_date.hash = '/end-date';
					product_general_data.appendChild(end_date);
				}
			}

		}

		this.addPanel(product_general_data, gettext('$PRODUCT_GENERALINFO_PANEL'), true);

		var product_presentation_data = new KPanel (this);
		{
			product_presentation_data.hash = '/presentation_data';

			var url = new KImageUpload(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_IMAGE_URL'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.url) {
					url.setValue(this.data.url);
				}
				var mediaList = new CODAMediaList(url, 'image/*');
				url.appendChild(mediaList);				
			}
			product_presentation_data.appendChild(url);

			var available_since = new KDate(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_AVAILABLE_SINCE'), 'available_since', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if(!!this.data && !!this.data.available_since){
					available_since.setValue(new Date(this.data.available_since));
				}
			}
			product_presentation_data.appendChild(available_since);

			var available_until = new KDate(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_AVAILABLE_UNTIL'), 'available_until', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if(!!this.data && !!this.data.available_until){
					available_until.setValue(new Date(this.data.available_until));
				}
			}
			product_presentation_data.appendChild(available_until);

			var is_highlight = new KCheckBox(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_HIGHLIGHT') + ' *', 'is_highlight');
			is_highlight.addOption(true, gettext('$CHECKBOX_TRUE_OPTION'), (!!this.data && !!this.data.is_highlight) ? true : false);
			is_highlight.setHelp(gettext('$PRODUCT_ITEM_FORM_IS_HIGHLIGHT_HELP'), CODAForm.getHelpOptions());

			product_presentation_data.appendChild(is_highlight);

			var specifications = new KRichText(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_SPECIFICATIONS'), 'specifications');
//			var specifications = new KTextArea(product_presentation_data, gettext('$PRODUCT_ITEM_FORM_SPECIFICATIONS'), 'specifications');
			{
				if (!!this.data && !!this.data.specifications){
					specifications.setValue(this.data.specifications);
				}
			}
			specifications.setHelp(gettext('$PRODUCT_ITEM_FORM_SPECIFICATIONS_HELP'), CODAForm.getHelpOptions());
			product_presentation_data.appendChild(specifications);

		}

		this.addPanel(product_presentation_data, gettext('$PRODUCT_PRESENTATION_PANEL'), false);

		if(!!this.data){
			CODAProductsShell.product_models_list = new CODAProductModelsList(this, {
				links : {
					feed : { href : this.data.href + '/productmodels' + '?fields=elements,size'}
				}
			});
			CODAProductsShell.product_models_list.hash = '/fields';
			this.addPanel(CODAProductsShell.product_models_list, gettext('$PRODUCTMODELS_LIST_PANEL'), false);
		}

	};
	
	CODAProductItemForm.onTypeSelected = function(event){
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		
		if (!!widget.parent.childWidget('/expiration-days')) {
			widget.parent.removeChild(widget.parent.childWidget('/expiration-days'));
		}
		if (!!widget.parent.childWidget('/start-date')) {
			widget.parent.removeChild(widget.parent.childWidget('/start-date'));
		}
		if (!!widget.parent.childWidget('/end-date')) {
			widget.parent.removeChild(widget.parent.childWidget('/end-date'));
		}
		
		if(widget.getValue() == gettext('$TYPE_VOUCHER_CODE')){
			var expiration_days = new KInput(widget.parent, gettext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS'), 'expiration_days');
			{
				if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.expiration_days) {
					expiration_days.setValue(widget.parent.parent.data.expiration_days);
				};
				expiration_days.setLength(8);
				expiration_days.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				expiration_days.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_EXPIRATION_DAYS_HELP'), CODAForm.getHelpOptions());
				expiration_days.hash = '/expiration-days';
				widget.parent.appendChild(expiration_days);
			}
			var start_date = new KDate(widget.parent, gettext('$PRODUCT_TYPE_VOUCHER_START_DATE') + ' *', 'start_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.start_date) {
					start_date.setValue(widget.parent.parent.data.start_date);
				};
				start_date.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_START_DATE_HELP'), CODAForm.getHelpOptions());
				start_date.hash = '/start-date';
				widget.parent.appendChild(start_date);
			}
			var end_date = new KDate(widget.parent, gettext('$PRODUCT_TYPE_VOUCHER_END_DATE') + ' *', 'end_date', KLocale.DATE_FORMAT, 'Y-M-d H:i');
			{
				if (!!widget.parent.parent && !!widget.parent.parent.data && !!widget.parent.parent.data.end_date) {
					end_date.setValue(widget.parent.parent.data.end_date);
				};
				end_date.setHelp(gettext('$PRODUCT_TYPE_VOUCHER_END_DATE_HELP'), CODAForm.getHelpOptions());
				end_date.hash = '/end-date';
				widget.parent.appendChild(end_date);
			}			
		}
		
	};
	
	CODAProductItemForm.addWizardPanel = function(parent)  {

		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/product_1';
			firstPanel.title = gettext('$PRODUCT_ITEM_FORM_PANEL_TITLE');
			firstPanel.description = gettext('$PRODUCT_ITEM_FORM_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-book';

			if (!!this.brands_for_products){
				var brands = new KCombo(firstPanel, gettext('$PRODUCT_ITEM_FORM_BRAND'), 'productBrand_id');
				{
					for (var index in this.brands_for_products ){
						brands.addOption(this.brands_for_products [index].id, this.brands_for_products [index].name, (!!this.data && !!this.data.productBrand_id && this.data.productBrand_id == this.brands_for_products[index].id) ? true : false);
					}
				}
				firstPanel.appendChild(brands);
			}

			var name = new KInput(firstPanel, gettext('$PRODUCT_ITEM_FORM_NAME') + ' *', 'name');
			{
				if (!!this.data && !!this.data.name) {
					name.setValue(this.data.name);
				}
				name.setHelp(gettext('$PRODUCT_ITEM_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(name);

			var hashtags = new KInput(firstPanel, gettext('PRODUCT_ITEM_FORM_HASHTAGS'), 'hashtags');
			{
				if (!!this.data && !!this.data.hashtags) {
					hashtags.setValue(this.data.hashtags);
				}
				hashtags.setHelp(gettext('$PRODUCT_ITEM_FORM_HASHTAGS_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(hashtags);

			var type_selector = new KCombo(firstPanel, gettext('$PRODUCT_ITEM_FORM_TYPE') + ' *', 'type');
			{
				type_selector.addOption(gettext('$TYPE_CLOTHING_CODE'), gettext('$TYPE_CLOTHING_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_CLOTHING_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_TECHNOLOGY_CODE'), gettext('$TYPE_TECHNOLOGY_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_TECHNOLOGY_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_VOUCHER_CODE'), gettext('$TYPE_VOUCHER_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_VOUCHER_CODE')) ? true : false);
				type_selector.addOption(gettext('$TYPE_OTHER_CODE'), gettext('$TYPE_OTHER_NAME'), (!!this.data && !!this.data.type && this.data.type == gettext('$TYPE_OTHER_CODE')) ? true : false);
				type_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				type_selector.setHelp(gettext('$PRODUCT_ITEM_FORM_TYPE_HELP'), CODAForm.getHelpOptions());

				//type_selector.addEventListener('change', CODAProductItemForm.onTypeSelected);

			}
			type_selector.setHelp(gettext('$PRODUCT_ITEM_FORM_TYPE_HELP'), CODAForm.getHelpOptions());
			firstPanel.appendChild(type_selector);

		}

		return [firstPanel];
	};

	CODAProductItemForm.prototype.setDefaults = function(params) {

		delete params.requestTypes;
		delete params.responseTypes;

		if(!!params.is_highlight && params.is_highlight.length == 1 && params.is_highlight[0] == true){
			params.is_highlight = true;
		}else{
			params.is_highlight = false;
		}

	};

	CODAProductItemForm.prototype.onPost = CODAProductItemForm.prototype.onCreated = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAProductItemForm.prototype.onPut = function(data, request){
		this.refresh();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));		
	};

	CODAProductItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAProductItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductModelItemForm(parent, href, parent_href, type, data_to_edit){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		this.config = this.config || {};
		this.config.headers = {};
		if(parent_href != null){
			this.target.collection = parent_href;
		}
		if(type != null){
			this.product_type = type;
		}
		if(data_to_edit != null){
			this.data_to_edit = data_to_edit;
		}
		this.nillable = true;
	}
}

KSystem.include([
	'CODAForm',
	'KDate',
	'KTextArea',
	'KRichText',
], function() {
	CODAProductModelItemForm.prototype = new CODAForm();
	CODAProductModelItemForm.prototype.constructor = CODAProductModelItemForm;

	CODAProductModelItemForm.prototype.setData = function(data) {
//		this.config.schema = undefined;
		this.config.requestType = "KPanel";
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;;
		}
		else {
			var types = [
				'application/json',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAProductModelItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		var productmodel_item_panel = new KPanel(this);
		{
			productmodel_item_panel.hash = '/general';

			var reference = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_REFERENCE') + ' *', 'reference');
			{
				if (!!this.data && !!this.data.reference){
					reference.setValue(this.data.reference);
				}
				reference.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP'), CODAForm.getHelpOptions());
				reference.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			productmodel_item_panel.appendChild(reference);

//			if(!!this.product_type && this.product_type == gettext('$TYPE_CLOTHING_CODE')){
//
//				var colour = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_COLOUR'), 'colour');
//				{
//					if (!!this.data && !!this.data.colour){
//						colour.setValue(this.data.colour);
//					}
//					colour.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP'), CODAForm.getHelpOptions());
//				}
//				productmodel_item_panel.appendChild(colour);
//
//				var size = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_SIZE'), 'size');
//				{
//					if (!!this.data && !!this.data.size){
//						size.setValue(this.data.size);
//					}
//					size.setLength(8);
//					size.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP'), CODAForm.getHelpOptions());
//				}
//				productmodel_item_panel.appendChild(size);				
//
//			}else if(!!this.product_type && this.product_type == gettext('$TYPE_TECHNOLOGY_CODE')){
//
//				var length = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_LENGTH'), 'length');
//				{
//					if (!!this.data && !!this.data.length){
//						length.setValue(this.data.length);
//					}
//					length.setLength(8);
//					length.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP'), CODAForm.getHelpOptions());
//				}
//				productmodel_item_panel.appendChild(length);
//
//				var height = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_HEIGHT'), 'height');
//				{
//					if (!!this.data && !!this.data.height){
//						height.setValue(this.data.height);
//					}
//					height.setLength(8);
//					height.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP'), CODAForm.getHelpOptions());
//				}
//				productmodel_item_panel.appendChild(height);
//
//				var weight = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_WEIGHT'), 'weight');
//				{
//					if (!!this.data && !!this.data.weight){
//						weight.setValue(this.data.weight);
//					}
//					weight.setLength(8);
//					weight.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP'), CODAForm.getHelpOptions());
//				}
//				productmodel_item_panel.appendChild(weight);
//
//			}else if(!!this.product_type && (this.product_type == gettext('$TYPE_OTHER_CODE') || this.product_type == 'electronics')){

				var colour = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_COLOUR'), 'colour');
				{
					if (!!this.data && !!this.data.colour){
						colour.setValue(this.data.colour);
					}
					colour.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(colour);

				var size = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_SIZE'), 'size');
				{
					if (!!this.data && !!this.data.size){
						size.setValue(this.data.size);
					}
					size.setLength(8);
					size.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(size);

				var length = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_LENGTH'), 'length');
				{
					if (!!this.data && !!this.data.length){
						length.setValue(this.data.length);
					}
					length.setLength(8);
					length.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(length);

				var height = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_HEIGHT'), 'height');
				{
					if (!!this.data && !!this.data.height){
						height.setValue(this.data.height);
					}
					height.setLength(8);
					height.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(height);

				var weight = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_WEIGHT'), 'weight');
				{
					if (!!this.data && !!this.data.weight){
						weight.setValue(this.data.weight);
					}
					weight.setLength(8);
					weight.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(weight);

//			}

			//var characteristics = new KTextArea(productmodel_item_panel, gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS'), 'characteristics');
			var characteristics = new KRichText(productmodel_item_panel, gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS'), 'characteristics');
			{
				if (!!this.data && !!this.data.characteristics){
					characteristics.setValue(this.data.characteristics);
				}
			}
			characteristics.setHelp(gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS_HELP'), CODAForm.getHelpOptions());
			productmodel_item_panel.appendChild(characteristics);
		}
		this.addPanel(productmodel_item_panel, gettext('$PRODUCTMODEL_GENERALINFO_PANEL'), !!this.data_to_edit && this.data_to_edit == 'general_data');

		var productmodel_stock_panel = new KPanel(this);
		{
			productmodel_stock_panel.hash = '/general2';

			var stock = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_STOCK_AVAILABLE'), 'stock');
			{
				if (!!this.data && !!this.data.stock){
					stock.setValue(this.data.stock);
				}
				stock.setLength(8);
				stock.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_STOCK_AVAILABLE_HELP'), CODAForm.getHelpOptions());
				stock.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
			}
			productmodel_stock_panel.appendChild(stock);

			var currency_selector = new KCombo(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_CURRENCY'), 'currency');
			{
				currency_selector.addOption('BRL', gettext('$CURRENCY_BRL_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'BRL') ? true : false);
				currency_selector.addOption('EUR', gettext('$CURRENCY_EUR_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'EUR') ? true : false);
				currency_selector.addOption('BTC', gettext('$CURRENCY_BTC_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'BTC') ? true : false);
				//currency_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				currency_selector.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_CURRENCY_HELP'), CODAForm.getHelpOptions());
			}
			productmodel_stock_panel.appendChild(currency_selector);

			var net_value = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_NET_VALUE'), 'net_value');
			{
				if (!!this.data && !!this.data.net_value){
					net_value.setValue(this.data.net_value);
				}
				net_value.setLength(8);
				net_value.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_NET_VALUE_HELP'), CODAForm.getHelpOptions());
				net_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			productmodel_stock_panel.appendChild(net_value);

			var vat = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_VAT'), 'vat');
			{
				if (!!this.data && !!this.data.vat){
					vat.setValue(this.data.vat);
				}
				vat.setLength(8);
				vat.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_VAT_HELP'), CODAForm.getHelpOptions());
				vat.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
			}
			productmodel_stock_panel.appendChild(vat);

			var fee_percentage = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_FEE_PERCENTAGE'), 'fee_percentage');
			{
				if (!!this.data && !!this.data.fee_percentage){
					fee_percentage.setValue(this.data.fee_percentage);
				}
				fee_percentage.setLength(8);
				fee_percentage.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_FEE_PERCENTAGE_HELP'), CODAForm.getHelpOptions());
				fee_percentage.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
			}
			productmodel_stock_panel.appendChild(fee_percentage);

			var fee_amount = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_FEE_AMOUNT'), 'fee_amount');
			{
				if (!!this.data && !!this.data.fee_amount){
					fee_amount.setValue(this.data.fee_amount);
				}
				fee_amount.setLength(8);
				fee_amount.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_FEE_AMOUNT_HELP'), CODAForm.getHelpOptions());
				fee_amount.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			productmodel_stock_panel.appendChild(fee_amount);

			var discount_percentage = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_DISCOUNT_PERCENTAGE'), 'discount_percentage');
			{
				if (!!this.data && !!this.data.discount_percentage){
					discount_percentage.setValue(this.data.discount_percentage);
				}
				discount_percentage.setLength(8);
				discount_percentage.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_PERCENTAGE_HELP'), CODAForm.getHelpOptions());
				discount_percentage.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
			}
			productmodel_stock_panel.appendChild(discount_percentage);

			var discount_amount = new KInput(productmodel_stock_panel, gettext('$PRODUCTMODEL_ITEM_DISCOUNT_AMOUNT'), 'discount_amount');
			{
				if (!!this.data && !!this.data.discount_amount){
					discount_amount.setValue(this.data.discount_amount);
				}
				discount_amount.setLength(8);
				discount_amount.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_AMOUNT_HELP'), CODAForm.getHelpOptions());
				discount_amount.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			productmodel_stock_panel.appendChild(discount_amount);
		}
		this.addPanel(productmodel_stock_panel, gettext('$PRODUCTMODEL_STOCK_PANEL'), !!this.data_to_edit && this.data_to_edit == 'stock_price');

		var productmodel_images_panel = new KPanel(this);
		{
			productmodel_images_panel.hash = '/general3';
			productmodel_images_panel.addCSSClass('CODAFormImageGallery');

			var product_model_thumbnail_1 = new KImageUpload(productmodel_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1'), 'thumbnail_1', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_1) {
					product_model_thumbnail_1.setValue(this.data.thumbnail_1);
				}
				product_model_thumbnail_1.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(product_model_thumbnail_1, 'image/*');
				product_model_thumbnail_1.appendChild(mediaList);				
			}
			productmodel_images_panel.appendChild(product_model_thumbnail_1);

			var product_model_thumbnail_2 = new KImageUpload(productmodel_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2'), 'thumbnail_2', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_2) {
					product_model_thumbnail_2.setValue(this.data.thumbnail_2);
				}
				product_model_thumbnail_2.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(product_model_thumbnail_2, 'image/*');
				product_model_thumbnail_2.appendChild(mediaList);				
			}
			productmodel_images_panel.appendChild(product_model_thumbnail_2);

			var product_model_thumbnail_3 = new KImageUpload(productmodel_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3'), 'thumbnail_3', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_3) {
					product_model_thumbnail_3.setValue(this.data.thumbnail_3);
				}
				product_model_thumbnail_3.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(product_model_thumbnail_3, 'image/*');
				product_model_thumbnail_3.appendChild(mediaList);				
			}
			productmodel_images_panel.appendChild(product_model_thumbnail_3);

			var product_model_thumbnail_4 = new KImageUpload(productmodel_images_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4'), 'thumbnail_4', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_4) {
					product_model_thumbnail_4.setValue(this.data.thumbnail_4);
				}
				product_model_thumbnail_4.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(product_model_thumbnail_4, 'image/*');
				product_model_thumbnail_4.appendChild(mediaList);				
			}
			productmodel_images_panel.appendChild(product_model_thumbnail_4);

		}
		this.addPanel(productmodel_images_panel, gettext('$PRODUCTMODEL_IMAGES_PANEL'), !!this.data_to_edit && this.data_to_edit == 'multimedia_files');
	};

	CODAProductModelItemForm.prototype.addMinimizedPanel = function() {
		if(!!this.data && !!this.data.reference){
			this.setTitle(gettext('$EDIT_PRODUCTMODEL_PROPERTIES') + ' \u00ab' + this.data.reference + '\u00bb');
		}else{
			this.setTitle(gettext('$ADD_PRODUCTMODEL_PROPERTIES'));
		}

		var productmodel_item_panel = new KPanel(this);
		{

			productmodel_item_panel.hash = '/general';

			if(!this.data_to_edit || (!!this.data_to_edit && this.data_to_edit == 'general_data')){
				var reference = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_REFERENCE') + ' *', 'reference');
				{
					if (!!this.data && !!this.data.reference){
						reference.setValue(this.data.reference);
					}
					reference.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP'), CODAForm.getHelpOptions());
					reference.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
				productmodel_item_panel.appendChild(reference);

//				if(!!this.product_type && this.product_type == gettext('$TYPE_CLOTHING_CODE')){
//
//					var colour = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_COLOUR'), 'colour');
//					{
//						if (!!this.data && !!this.data.colour){
//							colour.setValue(this.data.colour);
//						}
//						colour.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP'), CODAForm.getHelpOptions());
//					}
//					productmodel_item_panel.appendChild(colour);
//
//					var size = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_SIZE'), 'size');
//					{
//						if (!!this.data && !!this.data.size){
//							size.setValue(this.data.size);
//						}
//						size.setLength(8);
//						size.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP'), CODAForm.getHelpOptions());
//					}
//					productmodel_item_panel.appendChild(size);
//
//				}else if(!!this.product_type && this.product_type == gettext('$TYPE_TECHNOLOGY_CODE')){
//
//					var length = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_LENGTH'), 'length');
//					{
//						if (!!this.data && !!this.data.length){
//							length.setValue(this.data.length);
//						}
//						length.setLength(8);
//						length.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP'), CODAForm.getHelpOptions());
//					}
//					productmodel_item_panel.appendChild(length);
//
//					var height = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_HEIGHT'), 'height');
//					{
//						if (!!this.data && !!this.data.height){
//							height.setValue(this.data.height);
//						}
//						height.setLength(8);
//						height.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP'), CODAForm.getHelpOptions());						
//					}
//					productmodel_item_panel.appendChild(height);
//
//					var weight = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_WEIGHT'), 'weight');
//					{
//						if (!!this.data && !!this.data.weight){
//							weight.setValue(this.data.weight);
//						}
//						weight.setLength(8);
//						weight.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP'), CODAForm.getHelpOptions());
//					}
//					productmodel_item_panel.appendChild(weight);
//
//				}else if(!!this.product_type && (this.product_type == gettext('$TYPE_OTHER_CODE') || this.product_type == 'electronics')){

					var colour = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_COLOUR'), 'colour');
					{
						if (!!this.data && !!this.data.colour){
							colour.setValue(this.data.colour);
						}
						colour.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP'), CODAForm.getHelpOptions());
					}
					productmodel_item_panel.appendChild(colour);

					var size = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_SIZE'), 'size');
					{
						if (!!this.data && !!this.data.size){
							size.setValue(this.data.size);
						}
						size.setLength(8);
						size.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP'), CODAForm.getHelpOptions());
					}
					productmodel_item_panel.appendChild(size);

					var length = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_LENGTH'), 'length');
					{
						if (!!this.data && !!this.data.length){
							length.setValue(this.data.length);
						}
						length.setLength(8);
						length.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP'), CODAForm.getHelpOptions());
					}
					productmodel_item_panel.appendChild(length);

					var height = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_HEIGHT'), 'height');
					{
						if (!!this.data && !!this.data.height){
							height.setValue(this.data.height);
						}
						height.setLength(8);
						height.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP'), CODAForm.getHelpOptions());
					}
					productmodel_item_panel.appendChild(height);

					var weight = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_WEIGHT'), 'weight');
					{
						if (!!this.data && !!this.data.weight){
							weight.setValue(this.data.weight);
						}
						weight.setLength(8);
						weight.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP'), CODAForm.getHelpOptions());
					}
					productmodel_item_panel.appendChild(weight);

//				}

				var characteristics = new KTextArea(productmodel_item_panel, gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS'), 'characteristics');
//				var characteristics = new KRichText(productmodel_item_panel, gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS'), 'characteristics');
				{
					if (!!this.data && !!this.data.characteristics){
						characteristics.setValue(this.data.characteristics);
					}
				}
				characteristics.setHelp(gettext('$PRODUCT_ITEM_FORM_CHARACTERISTICS_HELP'), CODAForm.getHelpOptions());
				productmodel_item_panel.appendChild(characteristics);
			}else if(!!this.data_to_edit && this.data_to_edit == 'stock_price'){

				var stock = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_STOCK_AVAILABLE'), 'stock');
				{
					if (!!this.data && !!this.data.stock){
						stock.setValue(this.data.stock);
					}
					stock.setLength(8);
					stock.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_STOCK_AVAILABLE_HELP'), CODAForm.getHelpOptions());
					stock.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				}
				productmodel_item_panel.appendChild(stock);

				var currency_selector = new KCombo(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_CURRENCY'), 'currency');
				{
					currency_selector.addOption('BRL', gettext('$CURRENCY_BRL_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'BRL') ? true : false);
					currency_selector.addOption('EUR', gettext('$CURRENCY_EUR_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'EUR') ? true : false);
					currency_selector.addOption('BTC', gettext('$CURRENCY_BTC_NAME'), (!!this.data && !!this.data.currency && this.data.currency == 'BTC') ? true : false);
					//currency_selector.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
					currency_selector.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_CURRENCY_HELP'), CODAForm.getHelpOptions());
				}
				productmodel_item_panel.appendChild(currency_selector);

				//se houve valor tem que ter currency
				var net_value = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_NET_VALUE'), 'net_value');
				{
					if (!!this.data && !!this.data.net_value){
						net_value.setValue(this.data.net_value);
					}
					net_value.setLength(8);
					net_value.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_NET_VALUE_HELP'), CODAForm.getHelpOptions());
					net_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
				}
				productmodel_item_panel.appendChild(net_value);

				var vat = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_VAT'), 'vat');
				{
					if (!!this.data && !!this.data.vat){
						vat.setValue(this.data.vat);
					}
					vat.setLength(8);
					vat.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_VAT_HELP'), CODAForm.getHelpOptions());
					vat.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
				}
				productmodel_item_panel.appendChild(vat);

				var fee_percentage = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_FEE_PERCENTAGE'), 'fee_percentage');
				{
					if (!!this.data && !!this.data.fee_percentage){
						fee_percentage.setValue(this.data.fee_percentage);
					}
					fee_percentage.setLength(8);
					fee_percentage.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_FEE_PERCENTAGE_HELP'), CODAForm.getHelpOptions());
					fee_percentage.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
				}
				productmodel_item_panel.appendChild(fee_percentage);

				var fee_amount = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_FEE_AMOUNT'), 'fee_amount');
				{
					if (!!this.data && !!this.data.fee_amount){
						fee_amount.setValue(this.data.fee_amount);
					}
					fee_amount.setLength(8);
					fee_amount.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_FEE_AMOUNT_HELP'), CODAForm.getHelpOptions());
					fee_amount.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
				}
				productmodel_item_panel.appendChild(fee_amount);

				var discount_percentage = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_DISCOUNT_PERCENTAGE'), 'discount_percentage');
				{
					if (!!this.data && !!this.data.discount_percentage){
						discount_percentage.setValue(this.data.discount_percentage);
					}
					discount_percentage.setLength(8);
					discount_percentage.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_PERCENTAGE_HELP'), CODAForm.getHelpOptions());
					discount_percentage.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
				}
				productmodel_item_panel.appendChild(discount_percentage);

				var discount_amount = new KInput(productmodel_item_panel, gettext('$PRODUCTMODEL_ITEM_DISCOUNT_AMOUNT'), 'discount_amount');
				{
					if (!!this.data && !!this.data.discount_amount){
						discount_amount.setValue(this.data.discount_amount);
					}
					discount_amount.setLength(8);
					discount_amount.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_DISCOUNT_AMOUNT_HELP'), CODAForm.getHelpOptions());
					discount_amount.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
				}
				productmodel_item_panel.appendChild(discount_amount);

			}else if(!!this.data_to_edit && this.data_to_edit == 'multimedia_files'){

				var product_model_thumbnail_1 = new KImageUpload(productmodel_item_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1'), 'thumbnail_1', CODAGenericShell.getUploadAuthorization());
				{
					if (!!this.data && !!this.data.thumbnail_1) {
						product_model_thumbnail_1.setValue(this.data.thumbnail_1);
					}
					product_model_thumbnail_1.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1_HELP'), CODAForm.getHelpOptions());
					var mediaList = new CODAMediaList(product_model_thumbnail_1, 'image/*');
					product_model_thumbnail_1.appendChild(mediaList);				
				}
				productmodel_item_panel.appendChild(product_model_thumbnail_1);

				var product_model_thumbnail_2 = new KImageUpload(productmodel_item_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2'), 'thumbnail_2', CODAGenericShell.getUploadAuthorization());
				{
					if (!!this.data && !!this.data.thumbnail_2) {
						product_model_thumbnail_2.setValue(this.data.thumbnail_2);
					}
					product_model_thumbnail_2.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_2_HELP'), CODAForm.getHelpOptions());
					var mediaList = new CODAMediaList(product_model_thumbnail_2, 'image/*');
					product_model_thumbnail_2.appendChild(mediaList);				
				}
				productmodel_item_panel.appendChild(product_model_thumbnail_2);

				var product_model_thumbnail_3 = new KImageUpload(productmodel_item_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3'), 'thumbnail_3', CODAGenericShell.getUploadAuthorization());
				{
					if (!!this.data && !!this.data.thumbnail_3) {
						product_model_thumbnail_3.setValue(this.data.thumbnail_3);
					}
					product_model_thumbnail_3.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_3_HELP'), CODAForm.getHelpOptions());
					var mediaList = new CODAMediaList(product_model_thumbnail_3, 'image/*');
					product_model_thumbnail_3.appendChild(mediaList);				
				}
				productmodel_item_panel.appendChild(product_model_thumbnail_3);

				var product_model_thumbnail_4 = new KImageUpload(productmodel_item_panel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4'), 'thumbnail_4', CODAGenericShell.getUploadAuthorization());
				{
					if (!!this.data && !!this.data.thumbnail_4) {
						product_model_thumbnail_4.setValue(this.data.thumbnail_4);
					}
					product_model_thumbnail_4.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_4_HELP'), CODAForm.getHelpOptions());
					var mediaList = new CODAMediaList(product_model_thumbnail_4, 'image/*');
					product_model_thumbnail_4.appendChild(mediaList);				
				}
				productmodel_item_panel.appendChild(product_model_thumbnail_4);

			}

		}

		this.addPanel(productmodel_item_panel, gettext('$PRODUCTMODEL_GENERALINFO_PANEL'), true);

	};

	CODAProductModelItemForm.addWizardPanel = function(parent)  {

		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_TITLE');
			firstPanel.description = gettext('$PRODUCTMODEL_ITEM_FORM_METADA_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-umbrella';

			var reference = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_REFERENCE') + ' *', 'reference');
			reference.hash = '/reference';
			{
				reference.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP'), CODAForm.getHelpOptions());
				reference.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(reference);

			var colour = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_COLOUR'), 'colour');
			colour.hash = '/colour';
			{
				if (!!this.data && !!this.data.colour){
					colour.setValue(this.data.colour);
				}
				colour.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_COLOUR_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(colour);

			var size = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_SIZE'), 'size');
			size.hash = '/size';
			{
				if (!!this.data && !!this.data.size){
					size.setValue(this.data.size);
				}
				size.setLength(8);
				size.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_SIZE_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(size);

			var length = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_LENGTH'), 'length');
			length.hash = '/length';
			{
				if (!!this.data && !!this.data.length){
					length.setValue(this.data.length);
				}
				//VALIDAR OU UNIDADE DE MEDIDA POR DEFEITO OU NO NOME DO CAMPO COLOCAR UNIDADE
				length.setLength(8);
				length.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_LENGTH_HELP'), CODAForm.getHelpOptions());

			}
			firstPanel.appendChild(length);

			var height = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_HEIGHT'), 'height');
			height.hash = '/height';
			{
				if (!!this.data && !!this.data.height){
					height.setValue(this.data.height);
				}
				//VALIDAR OU UNIDADE DE MEDIDA POR DEFEITO OU NO NOME DO CAMPO COLOCAR UNIDADE
				height.setLength(8);
				height.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_HEIGHT_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(height);

			var weight = new KInput(firstPanel, gettext('$PRODUCTMODEL_ITEM_WEIGHT'), 'weight');
			weight.hash = '/weight';
			{
				if (!!this.data && !!this.data.weight){
					weight.setValue(this.data.weight);
				}
				//VALIDAR OU UNIDADE DE MEDIDA POR DEFEITO OU NO NOME DO CAMPO COLOCAR UNIDADE
				weight.setLength(8);
				weight.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_WEIGHT_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(weight);
		}

		var secondPanel = new KPanel(parent);
		{
			secondPanel.hash = '/stock';
			secondPanel.title = gettext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_TITLE');
			secondPanel.description = gettext('$PRODUCTMODEL_ITEM_FORM_STOCKPRICE_PANEL_DESC');
			secondPanel.icon = 'css:fa fa-dollar';

			var stock = new KInput(secondPanel, gettext('$PRODUCTMODEL_ITEM_STOCK'), 'stock');
			{
				if (!!this.data && !!this.data.stock){
					stock.setValue(this.data.stock);
				}
				stock.setLength(8);
				stock.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_STOCK_HELP'), CODAForm.getHelpOptions());
				stock.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));

			}
			secondPanel.appendChild(stock);

			var currency_selector = new KCombo(secondPanel, gettext('$PRODUCTMODEL_ITEM_CURRENCY'), 'currency');
			{
				currency_selector.addOption('BRL', gettext('$CURRENCY_BRL_NAME'), (!!this.data && !!this.data.type && this.data.type == 'BRL') ? true : false);
				currency_selector.addOption('EUR', gettext('$CURRENCY_EUR_NAME'), (!!this.data && !!this.data.type && this.data.type == 'EUR') ? true : false);
				currency_selector.addOption('BTC', gettext('$CURRENCY_BTC_NAME'), (!!this.data && !!this.data.type && this.data.type == 'BTC') ? true : false);
				currency_selector.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_CURRENCY_HELP'), CODAForm.getHelpOptions());
			}
			secondPanel.appendChild(currency_selector);

			//se houve valor tem que ter currency
			var net_value = new KInput(secondPanel, gettext('$PRODUCTMODEL_ITEM_NET_VALUE'), 'net_value');
			{
				if (!!this.data && !!this.data.net_value){
					net_value.setValue(this.data.net_value);
				}
				net_value.setLength(8);
				net_value.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_NET_VALUE_HELP'), CODAForm.getHelpOptions());
				net_value.addValidator(/^((([0-9]+)?)|((([0-9]+)\.([0-9]+))?))$/, gettext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE'));
			}
			secondPanel.appendChild(net_value);

			var vat = new KInput(secondPanel, gettext('$PRODUCTMODEL_ITEM_VAT'), 'vat');
			{
				if (!!this.data && !!this.data.vat){
					vat.setValue(this.data.vat);
				}
				vat.setLength(8);
				vat.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_VAT_HELP'), CODAForm.getHelpOptions());
				vat.addValidator(/^(((0\.([0-9])+)|1)?)$/, gettext('$ERROR_MUST_BE_BETWEEN_0_1'));
			}
			secondPanel.appendChild(vat);

		}

		var thirdPanel = new KPanel(parent);
		{
			thirdPanel.hash = '/multimedia';
			thirdPanel.title = gettext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_TITLE');
			thirdPanel.description = gettext('$PRODUCTMODEL_ITEM_FORM_MEDIA_PANEL_DESC');
			thirdPanel.icon = 'css:fa fa-eye';

			var product_model_thumbnail_1 = new KImageUpload(thirdPanel, gettext('$CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1'), 'url', CODAGenericShell.getUploadAuthorization());
			{
				if (!!this.data && !!this.data.thumbnail_1) {
					product_model_thumbnail_1.setValue(this.data.thumbnail_1);
				}
				product_model_thumbnail_1.setHelp(gettext('CAMPAIGN_PHASE_PRIZE_ITEM_FORM_THUMBNAIL_1_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(product_model_thumbnail_1, 'image/*');
				product_model_thumbnail_1.appendChild(mediaList);				
			}
			thirdPanel.appendChild(product_model_thumbnail_1);
		}

		return [firstPanel, secondPanel, thirdPanel];

	};

	CODAProductModelItemForm.prototype.setDefaults = function(params) {
		delete params.requestTypes;
		delete params.responseTypes;
	};

	CODAProductModelItemForm.prototype.onPost = CODAProductModelItemForm.prototype.onCreated = function(data, request){
		CODAProductsShell.product_models_list.list.refreshPages();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAProductModelItemForm.prototype.onPut = function(data, request){
		CODAProductsShell.product_models_list.list.refreshPages();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	KSystem.included('CODAProductModelItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAProductModelListItem (parent){
	if(parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
		this.config.headers = {};
	}
}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton'
], function(){
	CODAProductModelListItem.prototype = new CODAListItem();
	CODAProductModelListItem.prototype.constructor = CODAProductModelListItem;

	CODAProductModelListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PRODUCTMODEL_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onProductModelDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};


	CODAProductModelListItem.prototype.showDetails = function() {
		if (!this.selected) {
			CODAListItem.selectElement(this);
		}
		KBreadcrumb.dispatchEvent(null, {
			hash :  '/edit' + this.data.href + '/' + this.parent.parent.parent.data.type + '/general_data'
		});
	};

	CODAProductModelListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);

		if(!!this.data){
			var managestock_listBt = new KButton(this, '', 'managestock', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAProductModelListItem');
				KBreadcrumb.dispatchURL({
					hash :  '/edit' + widget.data.href + '/' + widget.parent.parent.parent.data.type + '/stock_price'
				});
			});
			{

				managestock_listBt.setText('<i class="fa fa-truck"></i> ' + gettext('$PRODUCTMODEL_MANAGE_STOCK'), true);
			}

			this.appendChild(managestock_listBt);

			var managemultimedia_listBt = new KButton(this, '', 'managemultimedia', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event, 'CODAProductModelListItem');
				KBreadcrumb.dispatchURL({
					hash :  '/edit' + widget.data.href + '/' + widget.parent.parent.parent.data.type + '/multimedia_files'
				});
			});

			{
				managemultimedia_listBt.setText('<i class="fa fa-picture-o"></i> ' + gettext('$PRODUCTMODEL_MANAGE_MULTIMEDIA'), true);
			}

			this.appendChild(managemultimedia_listBt);

		}

	};

	CODAProductModelListItem.prototype.onProductModelDelete = function(data, message) {
		this.parent.removeChild(this);
		CODAProductsShell.product_models_list.list.refreshPages();
	};

	KSystem.included('CODAProductModelListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductModelsList(parent, data) {
	if(parent != null){
		CODAList.call(this, parent, data, '', {
			one_choice : true,
			add_button : true
		});
		this.pageSize = 5;
		this.table = {
			"reference" : {
				label : gettext('$PRODUCTMODEL_LIST_REFERENCE')
			},
			"stock" : {
				label : gettext('$PRODUCTMODEL_LIST_STOCK_AVAILABLE')
			},
			"total_value" : {
				label : gettext('$PRODUCTMODEL_LIST_TOTAL_VALUE')
			}
		};
		this.LIST_MARGIN = 310;
		this.filterFields = [ 'reference' ];
		this.orderBy = 'reference';
	}
}

KSystem.include([
	'CODAList',
	'CODAProductModelListItem'
], function(){
	CODAProductModelsList.prototype = new CODAList();
	CODAProductModelsList.prototype.constructor = CODAProductModelsList;

	CODAProductModelsList.prototype.addItems = function(list) {

		for(var index in this.elements) {
			var listItem = new CODAProductModelListItem(list);
			listItem.data = this.elements[index];

			list.appendChild(listItem);
		}

	};

	CODAProductModelsList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-productmodel' + this.parent.data.href + '/' + this.parent.data.type
		});
	};

	CODAProductModelsList.prototype.visible = function() {
		CODAList.prototype.visible.call(this);
		if (this.opened() && (!this.elements || this.elements.length == 0)) {
			KBreadcrumb.dispatchURL({
				hash : '/add-productmodel' + this.parent.data.href + '/' + this.parent.data.type
			});
		}
	};

	KSystem.included('CODAProductModelsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductsDashboard(parent) {
	if (parent != null) {
		var icons = [];

//		icons.push({
//			icon : 'css:fa fa-archive',
//			activate : false,
//			title : gettext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND'),
//			description : '<span>' + gettext('$DASHBOARD_BUTTON_ADD_PRODUCTBRAND_DESC') + '</span>',
//			actions : [{
//				icon : 'css:fa fa-chevron-right',
//				desc : 'Adicionar',
//				link : '#/add-brand'
//			}]
//		});

		icons.push({
			icon : 'css:fa fa-bullseye',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_PRODUCTBRANDS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-brands'
			}]
		});

//		icons.push({
//			icon : 'css:fa fa-archive',
//			activate : false,
//			title : gettext('$DASHBOARD_BUTTON_ADD_PRODUCT'),
//			description : '<span>' + gettext('$DASHBOARD_BUTTON_ADD_PRODUCT_DESC') + '</span>',
//			actions : [{
//				icon : 'css:fa fa-chevron-right',
//				desc : 'Adicionar',
//				link : '#/add-product-wizard'
//			}]
//		});

		icons.push({
			icon : 'css:fa fa-book',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_LIST_PRODUCTS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_PRODUCTS_DESC') + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-products'
			}]
		});

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAProductsDashboard.prototype = new CODAMenuDashboard();
	CODAProductsDashboard.prototype.construtor = CODAProductsDashboard;

	CODAProductsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAProductsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductsListDashboard(parent, options, apps) {
	if (parent != null) {
		CODAFormIconDashboard.call(this, parent, options, apps);
		this.setTitle(gettext('$CODA_PRODUCTS_LIST_TITLE'));
		this.filterFields = [ 'name' ];
	}
}

KSystem.include([
    'KConfirmDialog',
	'CODAIcon',
	'CODAFormIconDashboard'
], function() {
	CODAProductsListDashboard.prototype = new CODAFormIconDashboard();
	CODAProductsListDashboard.prototype.constructor = CODAProductsListDashboard;

	CODAProductsListDashboard.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-product-wizard'
		});
	};

	CODAProductsListDashboard.prototype.getIconFor = function(data) {

		var dummy = window.document.createElement('p');
		if(!!data.description){
			dummy.innerHTML = data.description;
		}else{
			dummy.innerHTML = '';
		}

		var aditional = '';
		if(data.stock){
			aditional += '<span><b>' + gettext('$PRODUCTMODEL_ITEM_FORM_STOCK') + ':</b> ' + data.stock + '</span>';
		}
		if(data.value){
			aditional += '<span><b>' + gettext('$PRODUCTMODEL_ITEM_FORM_VALUE') + ':</b> ' + data.value + '</span>';
		}

		var actions = [];
		actions.push({
			icon : 'css:fa fa-pencil',
			link : '#/edit' + data.href,
			desc : 'Gerir'
		});
		actions.push({
			icon : 'css:fa fa-trash-o',
			desc : 'Remover',
			events : [
			{
				name : 'click',
				callback : CODAProductsListDashboard.remove
			}

			]
		});

		return new CODAIcon(this, {
			icon : !!data.url ? data.url : 'css:fa fa-gift',
			activate : false,
			title : data.name,
			href : data.href,
			description : '<span>' + dummy.textContent + '</span>' + aditional,
			actions : actions
		});
	};

	CODAProductsListDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_PRODUCT_CONFIRM'),
			callback : function() {
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onProductDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400
		});
	};

	CODAProductsListDashboard.prototype.onProductDelete = function(data, request) {
		this.getParent('KFloatable').closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-products' + request.service
		});
		return;
	};

	CODAProductsListDashboard.prototype.exportList = function() {
		KBreadcrumb.dispatchEvent(null, {
			hash : '/generate-products-report'
		});
	};

	KSystem.included('CODAProductsListDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductsShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAProductsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAProductsDashboard',
	'CODAProductsListDashboard',
//	'CODAProductsList',
	'CODAProductItemForm',
	'CODAProductWizard',
	'CODAProductModelsList',
	'CODAProductModelItemForm',
	'CODAProductBrandsListDashboard',
	'CODAProductBrandItemForm'
], function(){
	CODAProductsShell.prototype = new CODAGenericShell();
	CODAProductsShell.prototype.constructor = CODAProductsShell;

	CODAProductsShell.prototype.loadShell = function() {
		this.draw();
	};

	CODAProductsShell.prototype.draw = function() {
		this.setTitle(gettext('$PRODUCTS_SHELL_TITLE'));

		this.dashboard = new CODAProductsDashboard(this);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAProductsShell.sendRequest = function() {
	};

	CODAProductsShell.prototype.listBrands = function(href, minimized) {
		var brandsInfo = new CODAProductBrandsListDashboard(this, { 
			links : {
				feed : { href :  '/productbrands' }
			},
			filter_input : true, 
			add_button : true
		});
		brandsInfo.hash = '/brands/hash/';
		this.makeDialog(brandsInfo, { minimized : false, modal : true });
	};

	CODAProductsShell.prototype.addProductBrand = function() {
		var configForm = new CODAProductBrandItemForm(this, null);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAProductsShell.prototype.addProductBrandWizard = function() {

		var screens = [ 'CODAProductBrandItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_PRODUCTBRAND_PROPERTIES'));
		wiz.hash = '/productbrand';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			this.parent.closeMe();
			KBreadcrumb.dispatchURL({
				hash : '/list-brands'
			});
			return;
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + '/productbrands', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODAProductsShell.prototype.editProductBrand = function(href, minimized) {
		var configForm = new CODAProductBrandItemForm(this, href);
		{
			configForm.hash = '/brands/hash';
			configForm.setTitle(gettext('$EDIT_PRODUCTBRAND_PROPERTIES'));
		}
		this.makeListForm(configForm, { minimized : false, modal : true});
	};

	CODAProductsShell.prototype.listProducts = function(href, minimized) {
		CODAProductsShell.products_list = new CODAProductsListDashboard(this, { 
			links : {
				feed : { href :  '/products?orderBy=name' }
			},
			filter_input : true, 
			add_button : true, 
			export_button : true
		});
		CODAProductsShell.products_list.hash = '/products/hash/';
		this.makeDialog(CODAProductsShell.products_list, { minimized : false, modal : true });
	};

	CODAProductsShell.prototype.addProduct = function() {
		var configForm = new CODAProductItemForm(this, null);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAProductsShell.prototype.addProductWizard = function() {
		this.makeWizard(new CODAProductWizard(this));
	};
	
	CODAProductsShell.prototype.addProductModel = function(parent_href, minimnized, type) {
		var configForm = new CODAProductModelItemForm(this, null, parent_href, type);
		this.makeDialog(configForm, {minimized : minimnized});
	};
	
	CODAProductsShell.prototype.addProductModelWizard = function(parent_href, type) {

		var screens = [ 'CODAProductModelItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_PRODUCTMODEL_PROPERTIES'));
		wiz.hash = '/productmodel';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			CODAProductsShell.product_models_list.list.refreshPages();
			this.parent.closeMe();
			return;
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + parent_href, params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODAProductsShell.prototype.editProduct = function(href, minimized) {
		var configForm = new CODAProductItemForm(this, href, null, null);
		{
			configForm.hash = '/products/advanced';
			configForm.setTitle(gettext('$EDIT_PRODUCT_PROPERTIES'));
		}
		this.makeListForm(configForm, {minimized : false, modal : true});
	};

	CODAProductsShell.prototype.editProductModel = function(href, minimized, type, data_to_edit) {
		var configForm = new CODAProductModelItemForm(this, href, null, type, data_to_edit);
		{
			configForm.hash = '/productmodels/advanced';
			configForm.setTitle(gettext('$EDIT_PRODUCTMODEL_PROPERTIES'));
		}
		this.makeMiddleDialog(configForm, {minimized : false, modal : true});
	};

	CODAProductsShell.prototype.generateProductsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/products', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onProductsReportGenerated');
	};

	CODAProductsShell.prototype.onProductsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_PRODUCTS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAProductsShell.prototype.onNoContent = function(data, request) {
		if(request.service.indexOf('/products') == request.service.length - 9){
			//CODAProductsShell.prototype.addProduct.call(this);
			CODAProductsShell.prototype.addProductWizard.call(this);
		}else if(request.service.indexOf('/productbrands') == request.service.length - 14){
//			CODAProductsShell.prototype.addProductBrand.call(this);
			CODAProductsShell.prototype.addProductBrandWizard.call(this);
		}else if(request.service == '/generatereport/products'){
			KMessageDialog.alert({
				message : gettext('$PRODUCTS_MODULE_ERROR_REPORT_NO_CONTENT'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODAProductsShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]){
			case 'list-brands' : {
				Kinky.getDefaultShell().listBrands('/productbrands', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-brand' : {
				Kinky.getDefaultShell().addProductBrand();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-brand-wizard' : {
				Kinky.getDefaultShell().addProductBrandWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'list-products' : {
				Kinky.getDefaultShell().listProducts('/products', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-product' : {
				Kinky.getDefaultShell().addProduct();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-product-wizard' : {
				Kinky.getDefaultShell().addProductWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit' : {
				if (parts[2] == 'products'){
					if(parts.length == 8){
						if(parts[7] == 'general_data'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'general_data');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}else if(parts[7] == 'stock_price'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'stock_price');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}else if(parts[7] == 'multimedia_files'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'multimedia_files');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}
					}else{
						Kinky.getDefaultShell().editProduct('/products/' + parts[3], false);
						KBreadcrumb.dispatchURL({
							hash : ''
						});
					}
				}else if(parts[2] == 'productbrands'){
					Kinky.getDefaultShell().editProductBrand('/' + parts[2] + '/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'edit-full' : {
				if (parts[2] == 'products'){
					if(parts.length == 8){
						if(parts[7] == 'general_data'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'general_data');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}else if(parts[7] == 'stock_price'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'stock_price');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}else if(parts[7] == 'multimedia_files'){
							Kinky.getDefaultShell().editProductModel('/products/' + parts[3] + '/productmodels/' + parts[5], true, parts[6], 'multimedia_files');
							KBreadcrumb.dispatchURL({
								hash : ''
							});
						}
					}else{
						Kinky.getDefaultShell().editProduct('/products/' + parts[3], false);
						KBreadcrumb.dispatchURL({
							hash : ''
						});
					}
				}else if(parts[2] == 'productbrands'){
					Kinky.getDefaultShell().editProductBrand('/' + parts[2] + '/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'edit-productmodel' : {
				if (parts[parts.length - 3] == 'products'){
					Kinky.getDefaultShell().addProductModel('/products/' + parts[3] + '/productmodels', true, parts[4]);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'add-productmodel' : {
				if (parts[parts.length - 3] == 'products'){
					Kinky.getDefaultShell().addProductModelWizard('/products/' + parts[3] + '/productmodels', parts[4]);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'generate-products-report': {
				Kinky.getDefaultShell().generateProductsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}

	};

	KSystem.included('CODAProductsShell');
}, Kinky.CODA_INCLUDER_URL);
function CODAProductWizard(parent) {
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODAProductItemForm', 'CODAProductModelItemForm']);
		this.setTitle(gettext('$PRODUCT_ADD_NEW'));
		this.hash = '/products/add';
		var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE1');
		desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO1') + '</span>';
		desc = '<i class="fa fa-archive fa-2x pull-left"></i>' + desc;
		this.setInfo(desc);
	}
}

KSystem.include([
	'CODAWizard'
], function() {
	CODAProductWizard.prototype = new CODAWizard();
	CODAProductWizard.prototype.constructor = CODAProductWizard;

	CODAProductWizard.prototype.onPrevious = function() {
		switch(this.current) {
			case 1: {
				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE1');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO1') + '</span>';
				desc = '<i class="fa fa-archive fa-2x pull-left"></i>' + desc;
				this.setInfo(desc);
				break;
			}
			case 2: {
				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE2');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO2') + '</span>';
				if (this.values[0].type == gettext('$TYPE_CLOTHING_CODE')){
					desc = '<i class="fa fa-female  fa-2x pull-left"></i>' + desc;
				}else if (this.values[0].type == gettext('$TYPE_TECHNOLOGY_CODE')){
					desc = '<i class="fa fa-laptop  fa-2x pull-left"></i>' + desc;
				}else if (this.values[0].type == gettext('$TYPE_OTHER_CODE')){
					desc = '<i class="fa fa-puzzle-piece  fa-2x pull-left"></i>' + desc;
				}
				this.setInfo(desc);
				break;
			}
			case 3: {
				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE3');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO3') + '</span>';
				desc = '<i class="fa fa-eur fa-2x pull-left"></i>' + desc;
				this.setInfo(desc);
				break;
			}
		}
	};

	CODAProductWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				if(!this.childWidget('/products/add/1').childWidget('/reference')){
					var reference = new KInput(this.forms[1], gettext('$PRODUCTMODEL_ITEM_REFERENCE') + ' *', 'reference');
					reference.hash = '/reference';
					{
						reference.setHelp(gettext('$PRODUCTMODEL_ITEM_FORM_REFERENCE_HELP'), CODAForm.getHelpOptions());
						reference.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
					}
					this.forms[1].appendChild(reference);
				}

				var params = {};
				for (var f in this.values[0]) {
					params[f] = this.values[0][f];
				}

				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE2');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO2') + '</span>';
				if (this.values[0].type == gettext('$TYPE_CLOTHING_CODE')){
					desc = '<i class="fa fa-male  fa-2x pull-left"></i>' + desc;
				}else if (this.values[0].type == gettext('$TYPE_TECHNOLOGY_CODE')){
					desc = '<i class="fa fa-laptop  fa-2x pull-left"></i>' + desc;
				}else if (this.values[0].type == gettext('$TYPE_OTHER_CODE')){
					desc = '<i class="fa fa-puzzle-piece  fa-2x pull-left"></i>' + desc;
				}
				this.setInfo(desc);
				break;
			}
			case 1: {
				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE3');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO3') + '</span>';
				desc = '<i class="fa fa-eur fa-2x pull-left"></i>' + desc;
				this.setInfo(desc);
				break;
			}
			case 2: {

				var desc = gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_TITLE4');
				desc = '<b>' + desc + '</b><br><span>' + gettext('$PRODUCT_WIZARD_PRODUCT_MODEL_INFO4') + '</span>';
				desc = '<i class="fa fa-picture-o fa-2x pull-left"></i>' + desc;
				this.setInfo(desc);
				break;
			}
		}
	};

	CODAProductWizard.prototype.mergeValues = function() {
		var params = {};
		params[0] = {};
		params[1] = {};
		for (var v  = 0 ; v != this.values.length; v++) {
			if(v == 0 || v==3){
				for (var f in this.values[v]) {
					if(this.values[v][f] != ""){
						params[0][f] = this.values[v][f];
					}
				}
			}else{
				for (var f in this.values[v]) {
					if(this.values[v][f] != ""){
						params[1][f] = this.values[v][f];
					}
				}
			}
		}
		if(!!this.values[0]["product_href"]){
			this.send(params);
		}else{
			return params;
		}
	};

	CODAProductWizard.prototype.onPost = CODAProductWizard.prototype.onCreated = function(data, request) {

		if(request.service.indexOf("/productmodels") == -1){
			for (var link in data.links){
				href = link;
				break;
			}
			this.values[0]["product_href"] = href;
			this.mergeValues();
		}else{
			this.parent.closeMe();
			KBreadcrumb.dispatchURL({
				hash : '/list-products'
			});
			return;
		}
		this.parent.closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/list-products'
		});
		return;
	};

	CODAProductWizard.prototype.onPut = function(data, request) {
	};

	CODAProductWizard.prototype.send = function(params) {
		try {
			if (!!params) {

				if(params[0]["url"] === ""){
					delete params[0]["url"];
				}

				if(!this.values[0]["product_href"]){
					this.kinky.post(this, 'rest:/products', params[0]);
				}else{
					this.kinky.post(this, 'rest:' + this.values[0]["product_href"] + '/productmodels', params[1]);
				}
			}
		}
		catch (e) {

		}
	};

	KSystem.included('CODAProductWizard');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

