settext('$CODA_TEMPLATES_SHELL_TITLE', 'Templates & Themes', 'br');

///////////////////////////////////////////////////////////////////
// LIST & FORMS

settext('$CODA_TEMPLATES_CODE', 'Código', 'br');
settext('$CODA_TEMPLATES_RESTURL', 'URL base para a API', 'br');
settext('$CODA_TEMPLATES_DEFAULT_THEME', 'Theme por defeito', 'br');
settext('$CODA_TEMPLATES_DEFAULT_VERSION', 'Versão', 'br');
settext('$CODA_TEMPLATES_LIST_TITLE', 'Templates', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODATEMPLATESLIST', '<i class="fa fa-leaf"></i> Novo template', 'br');
settext('$EDIT_TEMPLATE_PROPERTIES', 'Editar Template', 'br');
settext('$CODA_TEMPLATE_ID', 'Identificador', 'br');
settext('$CODA_TEMPLATE_DEFAULT_THEME', 'Theme por defeito', 'br');
settext('$CODA_TEMPLATE_DEFAULT_VERSION', 'Versão por defeito', 'br');
settext('$CODA_TEMPLATE_FAN_GATE', 'Página de entrada (Landing Page) / Fan Gate?', 'br');
settext('$CODA_TEMPLATE_ICON', 'Icon', 'br');
settext('$CODA_TEMPLATE_MAXPARTICIPATIONS', 'Número máximo de participações', 'br');
settext('$CODA_TEMPLATE_RESTURL', 'URL base para a API', 'br');
settext('$CODA_TEMPLATE_GENERALINFO_PANEL', 'Info Geral', 'br');
settext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_PANEL', 'Configurações de Facebook', 'br');
settext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_ID', 'Identificação da aplicação (app id)', 'br');
settext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_SECRET', 'Código secreto da aplicação (app secret)', 'br');

settext('$CODA_THEMES_CODE', 'Código', 'br');
settext('$CODA_THEMES_NAME', 'Nome', 'br');
settext('$CODA_THEMES_LIST_TITLE', 'Themes', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODATHEMESLIST', '<i class="fa fa-bolt"></i> Novo theme', 'br');
settext('$EDIT_THEME_PROPERTIES', 'Editar Theme', 'br');
settext('$CODA_THEME_HREF', 'Href', 'br');
settext('$CODA_THEME_TEMPLATE_ID', 'Identificador', 'br');
settext('$CODA_THEME_ICON', 'Icon', 'br');
settext('$CODA_THEME_ORDER', 'Número de Ordem', 'br');
settext('$CODA_THEME_NAME', 'Nome', 'br');
settext('$CODA_THEME_STATE', 'Estado', 'br');
settext('$CODA_THEME_VERSION', 'Versão', 'br');
settext('$CODA_THEME_GENERALINFO_PANEL', 'Info Técnica', 'br');
settext('$CODA_THEME_LEAD', 'Lead', 'br');
settext('$CODA_THEME_DESCRIPTION', 'Descrição', 'br');
settext('$CODA_THEME_CATEGORY', 'Categoria', 'br');
settext('$CODA_THEME_DESCRIPTION_PANEL', 'Info Geral', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_TITLE', 'Título', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_DESCRIPTION', 'Descrição', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_URL', 'Imagem', 'br');
settext('$CODA_THEME_FEATURES_PANEL', 'Funcionalidades Chave', 'br');
settext('$CODA_THEME_GALLERY_PANEL', 'Galeria', 'br');
settext('$CODA_THEME_WIZARD', 'Wizard', 'br');
settext('$CODA_THEME_CONFIGURATIONS', 'Configurações', 'br');
settext('$CODA_THEME_TECH_PANEL', 'Wizard e Configurações', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODATHEMEFORM_THEME_FEATURESARRAYLIST', '<i class="fa fa-star"></i> Nova feature', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODATHEMEFORM_THEME_GALLERYARRAYLIST', '<i class="fa fa-picture-o"></i> Nova imagem', 'br');
settext('$CODATHEMEFORM_EDIT_ARRAY_ELEMENT_PROPERTIES', 'Funcionalidade Chave', 'br');
settext('$CODATHEMEFORM_ADD_ARRAY_ELEMENT_PANEL_TITLE', 'Imagem para Theme', 'br');
settext('$CODATHEMEFORM_ADD_ARRAY_ELEMENT_PANEL_DESC', 'Associa uma nova imagem ao theme indicando o título e seleccionando o ficheiro ou inserindo a sua localização', 'br');

settext('$ADD_THEME_PROPERTIES', 'Novo Theme', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_TITLE', 'Define os dados iniciais', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_DESC', 'Caracteriza o theme que vais inserir definindo os seus dados gerais<br>Indica o href<br>O identificador deverá ser único<br>Escolhe o icon através do sistema de ficheiros ou insere o url<br>Indica o estado em que o theme está e que vai influenciar se fica ou não disponível para se sejam criadas apps baseadas no mesmo', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_TITLE1', 'Define os dados de apresentação', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_DESC1', 'Os dados de apresentação vão ser utilizados no Website para caracterizar o theme<br>Define o nome para o theme<br>Faz uma pequena apresentação da utilização das apps que forem criada com base no theme<br>Descreve com maior detalhe o theme e qual a utilização que podem ter as apps que forem criadas com base no mesmo. A descrição será mostrada na página de detalhe do theme<br>Define as categorias do theme que deverão ser inseridas como hashtags (palavras precedidas por #)', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_TITLE2', 'Define a estrutura do wizard e outras configurações', 'br');
settext('$CODA_THEME_WIZ_INFO_PANEL_DESC2', 'Insere o JSON do wizard que deverá conter um JSON com um array em que cada elemento refere um painel a disponibilizar na inserção de uma app', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_BUTTON_THEMES', 'Themes', 'br');
settext('$CODA_TEMPLATES_LIST', 'Templates', 'br');
settext('$DASHBOARD_BUTTON_TEMPLATES_DESC', 'Configura os templates para os tipos de aplicações que vais disponibilizar. Cada template acede a uma api através do url que for definido. As apps de demonstração serão disponibilizadas na app de Facebook que for configurada.', 'br');
settext('$DASHBOARD_BUTTON_THEMES_DESC', 'Configura os themes que vão estar disponíveis para cada template de app configurado. Cada template de app pode ter vários themes, sendo que deverá ser definido um por defeito. Define os dados que caracterizam um theme e indica as pricipais features disponíveis.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODATemplatesLocale_br');

///////////////////////////////////////////////////////////////////
// LIST & FORMS

settext('$CODA_TEMPLATE_ID_HELP', 'Identificador único para o template<br>Será utilizado para identificar o template nesta plataforma', 'br');
settext('$CODA_TEMPLATE_DEFAULT_THEME_HELP', 'Tema por defeito que será utilizado no template', 'br');
settext('$CODA_TEMPLATE_DEFAULT_VERSION_HELP', 'Número da versão', 'br');
settext('$CODA_TEMPLATE_FAN_GATE_HELP', 'Escolhe se as aplicações que forem criadas com base no template têm uma página de entrada (landing page).<br>No caso de <strong>aplicações de Facebook</strong>, indica se as aplicações que forem criadas com base no template tem <strong>"Fan Gate"</strong><br>Esta opção pode ser alterada por aplicação', 'br');
settext('$CODA_TEMPLATE_ICON_HELP', 'Icon pelo qual o template será identificado no website e no backoffice', 'br');
settext('$CODA_TEMPLATE_MAXPARTICIPATIONS_HELP', 'Número máximo de participações permitidas para as aplicações que forem criadas com base no template.<br>Poderá ser alterado por aplicação.', 'br');
settext('$CODA_TEMPLATE_RESTURL_HELP', 'URL base em que estará disponível a API que as aplicações criadas com base no template irão usar', 'br');
settext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_ID_HELP', 'Identificador da aplicação de facebook associada ao template.<br>Deverá ser obtida pelo facebook acedendo à aplicação e copiando e conteúdo do campo App ID disponível na tab Settings.', 'br');
settext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_SECRET_HELP', 'Código secreto da aplicação de facebook associada ao template.<br>Deverá ser obtida pelo facebook acedendo à aplicação e copiando e conteúdo do campo App Secret disponível na tab Settings.', 'br');

settext('$CODA_THEME_HREF_HELP', 'HREF para o theme e que será concatenado com o identificador do template que lhe for associado', 'br');
settext('$CODA_THEME_TEMPLATE_ID_HELP', 'Identificador do template a que este theme será associado', 'br');
settext('$CODA_THEME_ICON_HELP', 'Icon pelo qual o theme será identificado no website e no backoffice', 'br');
settext('$CODA_THEME_ORDER_HELP', 'Ordem em que o theme irá aparecer no website', 'br');
settext('$CODA_THEME_NAME_HELP', 'Nome que identifica o theme no website e no backoffice', 'br');
settext('$CODA_THEME_STATE_HELP', 'Estado do theme que reflecte o estado de desenvolvimento em que o theme se encontra.<br>Apenas estão disponíveis no website os themes com o estado Release Candidate ou Final', 'br');
settext('$CODA_THEME_VERSION_HELP', 'Número da versão', 'br');
settext('$CODA_THEME_LEAD_HELP', 'Pequena descrição do theme que deverá reflectir sucintamente o objectivo das aplicações que forem criados no mesmo.<br>Irá aparecer na lista de aplicações para cada um dos themes.', 'br');
settext('$CODA_THEME_DESCRIPTION_HELP', 'Descrição do theme que deverá reflectir o objectivo das aplicações que forem criados no mesmo.<br>Irá aparecer na página de detalhe de de cada um dos themes.', 'br');
settext('$CODA_THEME_CATEGORY_HELP', 'Categorias do theme que deverão ser inseridas como hastags (precedidas por #) e separadas com um espaço.', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_TITLE_HELP', 'Título da funcionalidade.<br>Será utilizado no website na página de detalhe de cada theme, na área de apresentação de funcionalidades.', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_DESCRIPTION_HELP', 'Descrição da funcionalidade.<br>Será utilizado no website na página de detalhe de cada theme, na área de apresentação de funcionalidades.', 'br');
settext('$CODATHEMEFORM_ARRAY_ELEMENT_URL_HELP', 'URL da imagem.<br>A imagem poderá ser seleccionada pelo sistema de ficheiros ou porderá ser inserido o url em que a mesma se encontra.', 'br');
settext('$CODA_THEME_WIZARD_HELP', 'Define um JSON array com os paineis que vão aparecer no wizard para inserção da aplicações que baseadas neste theme', 'br');
settext('$CODA_THEME_CONFIGURATIONS_HELP', 'Outras configurações para o theme', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODATemplatesLocale_Help_br');

