KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODATemplateForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.target.collection = '/templates';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText'
], function() {
	CODATemplateForm.prototype = new CODAForm();
	CODATemplateForm.prototype.constructor = CODATemplateForm;

	CODATemplateForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"/wrml-schemas/template/templates-schema\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODATemplateForm.prototype.setDefaults = function(params) {
		
	};

	CODATemplateForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODATemplateForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle('\u00ab' + this.target.data.id + '\u00bb');
		}

		var infoPanel = new KPanel(this);
		{
			infoPanel.hash = '/general';

			var id = new KInput(infoPanel, gettext('$CODA_TEMPLATE_ID'), 'id');
			{
				if (!!this.data && !!this.data.id) {
					id.setValue(this.data.id);
				}
				id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				id.setHelp(gettext('$CODA_TEMPLATE_ID_HELP'), CODAForm.getHelpOptions());
   			}
			infoPanel.appendChild(id);

			var default_theme = new KInput(infoPanel, gettext('$CODA_TEMPLATE_DEFAULT_THEME'), 'default_theme');
			{
				if(!!this.data && !!this.data.default_theme){
					default_theme.setValue(this.data.default_theme);
				}
				default_theme.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				default_theme.setHelp(gettext('$CODA_TEMPLATE_DEFAULT_THEME_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(default_theme);

			var default_version = new KInput(infoPanel, gettext('$CODA_TEMPLATE_DEFAULT_VERSION'), 'default_version');
			{
				if(!!this.data && !!this.data.default_version){
					default_version.setValue(this.data.default_version);
				}
				default_version.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				default_version.setHelp(gettext('$CODA_TEMPLATE_DEFAULT_VERSION_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(default_version);

			var fan_gate = new KRadioButton(infoPanel, gettext('$CODA_TEMPLATE_FAN_GATE'), 'fan_gate');
			{
				fan_gate.addOption('yes', 'Sim', false);
				fan_gate.addOption('no', 'Não', false);

				if(!!this.data && !!this.data.fan_gate){
					fan_gate.setValue(this.data.fan_gate);
				}
				fan_gate.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				fan_gate.setHelp(gettext('$CODA_TEMPLATE_FAN_GATE_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(fan_gate);

			var icon = new KInput(infoPanel, gettext('$CODA_TEMPLATE_ICON'), 'icon');
			{
				if(!!this.data && !!this.data.icon){
					icon.setValue(this.data.icon);
				}
				icon.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				icon.setHelp(gettext('$CODA_TEMPLATE_ICON_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(icon, 'image/*');
				icon.appendChild(mediaList);
   			}
			infoPanel.appendChild(icon);

			var maxParticipations = new KInput(infoPanel, gettext('$CODA_TEMPLATE_MAXPARTICIPATIONS'), 'max_participations');
			{
				if(!!this.data && !!this.data.max_participations){
					maxParticipations.setValue(this.data.max_participations);
				}
				maxParticipations.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				maxParticipations.setHelp(gettext('$CODA_TEMPLATE_MAXPARTICIPATIONS_HELP'), CODAForm.getHelpOptions());
				maxParticipations.setLength(8);
			}
			infoPanel.appendChild(maxParticipations);

			var restURL = new KInput(infoPanel, gettext('$CODA_TEMPLATE_RESTURL'), 'rest_url');
			{
				if(!!this.data && !!this.data.rest_url){
					restURL.setValue(this.data.rest_url);
				}
				restURL.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				restURL.addValidator(/^(sftp|ftp|http|https):\/\/[^ "]+$/, gettext('$ERROR_MUST_BE_URL'));
				restURL.setHelp(gettext('$CODA_TEMPLATE_RESTURL_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(restURL);
		}
		this.addPanel(infoPanel, gettext('$CODA_TEMPLATE_GENERALINFO_PANEL'), true);

		var facebookPanel = new KPanel(this);
		{
			facebookPanel.hash = '/general2';

			var app_id = new KInput(facebookPanel, gettext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_ID'), 'facebook.app_id');
			{
				if (!!this.data && !!this.data.facebook && !!this.data.facebook.app_id) {
					app_id.setValue(this.data.facebook.app_id);
				}
				app_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				app_id.setHelp(gettext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_ID_HELP'), CODAForm.getHelpOptions());
   			}
			facebookPanel.appendChild(app_id);

			var app_secret = new KInput(facebookPanel, gettext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_SECRET'), 'facebook.app_secret');
			{
				if(!!this.data && !!this.data.facebook && !!this.data.facebook.app_secret){
					app_secret.setValue(this.data.facebook.app_secret);
				}
				app_secret.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				app_secret.setHelp(gettext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_APP_SECRET_HELP'), CODAForm.getHelpOptions());
			}
			facebookPanel.appendChild(app_secret);

			var referrer_domain = new KHidden(facebookPanel, 'facebook.referrer_domain');
			{
				referrer_domain.setValue('facebook.com');
				referrer_domain.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			facebookPanel.appendChild(referrer_domain);

		}
		this.addPanel(facebookPanel, gettext('$CODA_TEMPLATE_CHANNEL_FACEBOOK_PANEL'), false);
	};

	CODATemplateForm.prototype.onBadRequest = function(data, request) {
		this.draw();
	};

	CODATemplateForm.prototype.onPreConditionFailed = function(data, request) {
		this.draw();
	};

	CODATemplateForm.prototype.onNotFound = function(data, request) {
		this.draw();
	};

	KSystem.included('CODATemplateForm');
}, Kinky.CODA_INCLUDER_URL);
function CODATemplatesDashboard(parent) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, [{
			icon : 'css:fa fa-leaf',
			activate : false,
			title : gettext('$CODA_TEMPLATES_LIST'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_TEMPLATES_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-templates'
			}]
		}, 
		{
			icon : 'css:fa fa-bolt',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_THEMES'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_THEMES_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-themes'
			}]
		}
		]);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODATemplatesDashboard.prototype = new CODAMenuDashboard();
	CODATemplatesDashboard.prototype.constructor = CODATemplatesDashboard;

	CODATemplatesDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODATemplatesDashboard');
}, Kinky.CODA_INCLUDER_URL);

function CODATemplatesList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, {
			delete_button : true,
			edit_button : true,
			add_button : false,
			export_button : false
		});
		this.onechoice = false;
		this.pageSize = 5;
		this.table = {
			"id" : {
				label : gettext('$CODA_TEMPLATES_CODE')
			},
			"rest_url" : {
				label : gettext('$CODA_TEMPLATES_RESTURL')
			},
			"default_theme" : {
				label : gettext('$CODA_TEMPLATES_DEFAULT_THEME')
			},
			"default_version" : {
				label : gettext('$CODA_TEMPLATES_DEFAULT_VERSION')
			}
		};
		this.LIST_MARGIN = 50;
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODATemplatesList.prototype = new CODAList();
	CODATemplatesList.prototype.constructor = CODATemplatesList;

	CODATemplatesList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODATemplatesList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAListItem(list);
			{
				listItem.index = index;
				listItem.data = this.elements[index];
				listItem.data.href = '/templates/' + listItem.data.id;
			}
			list.appendChild(listItem);
		}
	};

	KSystem.included('CODATemplatesList');
}, Kinky.CODA_INCLUDER_URL);

function CODATemplatesShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addCSSClass('CODAShell');
		this.addCSSClass('CODAApplicationsShell');
		this.addLocationListener(CODATemplatesShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODATemplatesDashboard',
	'CODATemplatesList',
	'CODATemplateForm',
	'CODAThemesList',
	'CODAThemeForm'
], function() {
	CODATemplatesShell.prototype = new CODAGenericShell();
	CODATemplatesShell.prototype.constructor = CODATemplatesShell;

	CODATemplatesShell.prototype.loadShell = function() {
		this.draw();
	};

	CODATemplatesShell.prototype.draw = function() {
		this.dashboard = new CODATemplatesDashboard(this);
		this.appendChild(this.dashboard);

		this.setTitle(gettext('$CODA_TEMPLATES_SHELL_TITLE'));

		CODAGenericShell.prototype.draw.call(this);

		this.addCSSClass('CODAApplicationsShellContent', this.content);
	};

	CODATemplatesShell.sendRequest = function() {
	};

	CODATemplatesShell.prototype.listTemplates = function(href) {
		var configForm = new CODATemplatesList(this, {
			links : {
				feed : { href :  '/templates' }
			}
		});
		{
			configForm.hash = '/list/templates';
			configForm.setTitle(gettext('$CODA_TEMPLATES_LIST_TITLE'));
		}
		this.makeDialog(configForm, { minimized : false, modal : true });
	};

	CODATemplatesShell.prototype.editTemplate = function(href) {
		var configForm = new CODATemplateForm(this, href, null, null);
		{
			configForm.hash = '/edit/template';
			configForm.setTitle(gettext('$EDIT_TEMPLATE_PROPERTIES'));
		}
		this.makeMiddleDialog(configForm, { minimized : true, modal : false });
	};

	CODATemplatesShell.prototype.addTemplate = function() {

	};

	CODATemplatesShell.prototype.listThemes = function(href) {
		var configForm = new CODAThemesList(this, {
			links : {
				feed : { href :  '/themes' }
			}
		});
		{
			configForm.hash = '/list/themes';
			configForm.setTitle(gettext('$CODA_THEMES_LIST_TITLE'));
		}
		this.makeDialog(configForm, { minimized : false, modal : true });
	};

	CODATemplatesShell.prototype.editTheme = function(href) {
		var configForm = new CODAThemeForm(this, href, null, null);
		{
			configForm.hash = '/edit/theme';
			configForm.setTitle(gettext('$EDIT_THEME_PROPERTIES'));
		}
		this.makeMiddleDialog(configForm, { minimized : false, modal : false });
	};

	CODATemplatesShell.prototype.addTheme = function() {
		var screens = [ 'CODAThemeForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_THEME_PROPERTIES'));
		wiz.hash = '/add/theme';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			this.parent.closeMe();
			window.document.location.reload(true);
			return;
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.put(this, 'rest:' + '/themes', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODATemplatesShell.prototype.onNoContent = function(data, request) {
	};

	CODATemplatesShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]) {
			case 'list-templates': {
				Kinky.getDefaultShell().listTemplates();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'list-themes': {
				Kinky.getDefaultShell().listThemes();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add': {
				if (parts[2] == 'templates') {
					Kinky.getDefaultShell().addTemplate();
				}
				else if (parts[2] == 'themes') {
 					Kinky.getDefaultShell().addTheme();
				}
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit': {
				if (parts[2] == 'templates') {
					Kinky.getDefaultShell().editTemplate('/' + parts[2] + '/' + parts[3]);
				}
				else if (parts[2] == 'themes') {
 					Kinky.getDefaultShell().editTheme('/' + parts[2] + '/' + parts[3]);
				}
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	KSystem.included('CODATemplatesShell');
}, Kinky.CODA_INCLUDER_URL);
function CODAThemeForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODALocaleForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.target.collection = '/themes';

		for (var k in this.getShell().app_data.locales) {
			this.addLocale(this.getShell().app_data.locales[k]);
		}
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText'
], function() {
	CODAThemeForm.prototype = new CODALocaleForm();
	CODAThemeForm.prototype.constructor = CODAThemeForm;

	CODAThemeForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"/wrml-schemas/themes/themes-schema\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAThemeForm.prototype.setDefaults = function(params) {
		if (!!params.wizard) {
			params.wizard = JSON.parse(params.wizard);
		}
		if (!!params.configurations) {
			params.wizard = JSON.parse(params.configurations);
		}
	};

	CODAThemeForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAThemeForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle('\u00ab' + this.target.data.locales[KLocale.LOCALE].name + '\u00bb');
		}

		var infoPanel = new KPanel(this);
		{
			infoPanel.hash = '/general';

			var href = new KInput(infoPanel, gettext('$CODA_THEME_HREF') + ' *', 'href');
			{
				if (!!this.data && !!this.data.href) {
					href.setValue(this.data.href);
				}
				href.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				href.setHelp(gettext('$CODA_THEME_HREF_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(href);

			var template_id = new KInput(infoPanel, gettext('$CODA_THEME_TEMPLATE_ID') + ' *', 'template_id');
			{
				if(!!this.data && !!this.data.template_id){
					template_id.setValue(this.data.template_id);
				}
				template_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				template_id.setHelp(gettext('$CODA_THEME_TEMPLATE_ID_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(template_id);

			var icon = new KInput(infoPanel, gettext('$CODA_THEME_ICON') + ' *', 'icon');
			{
				if(!!this.data && !!this.data.icon){
					icon.setValue(this.data.icon);
				}
				icon.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				icon.setHelp(gettext('$CODA_THEME_ICON_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(icon, 'image/*');
				icon.appendChild(mediaList);
   			}
			infoPanel.appendChild(icon);

			var order = new KInput(infoPanel, gettext('$CODA_THEME_ORDER') + ' *', 'order');
			{
				if(!!this.data && !!this.data.order){
					order.setValue(this.data.order);
				}
				order.setLength(4);
				order.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				order.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				order.setHelp(gettext('$CODA_THEME_ORDER_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(order);
			
			var state = new KRadioButton(infoPanel, gettext('$CODA_THEME_STATE') + ' *', 'state');
			{
				state.addOption('prototype', 'Prototype', false);
				state.addOption('alpha', 'Alpha', false);
				state.addOption('beta', 'Beta', false);
				state.addOption('release_candidate', 'Release Candidate', false);
				state.addOption('final', 'Final', false);

				if(!!this.data && !!this.data.state){
					state.setValue(this.data.state);
				}
				state.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				state.setHelp(gettext('$CODA_THEME_STATE_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(state);

			var version = new KInput(infoPanel, gettext('$CODA_THEME_VERSION') + ' *', 'version');
			{
				if(!!this.data && !!this.data.version){
					version.setValue(this.data.version);
				}
				version.setLength(4);
				version.addValidator(/(^(([0-9]+)\.([0-9]+))$)|(^$)/, gettext('$ERROR_MUST_BE_DOUBLE'));
				version.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				version.setHelp(gettext('$CODA_THEME_VERSION_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(version);
		}

		var descPanel = new KPanel(this);
		{
			descPanel.hash = '/general2';

			var name = new KInput(descPanel, gettext('$CODA_THEME_NAME') + ' *', 'locales.' + KLocale.LOCALE + '.name');
			{
				if(!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE] && !!this.data.locales[KLocale.LOCALE].name){
					name.setValue(this.data.locales[KLocale.LOCALE].name);
				}
				name.isLocalized = true;
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				name.setHelp(gettext('$CODA_THEME_NAME_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(name);

			var demo = new KInput(descPanel, gettext('$CODA_THEME_DEMO'), 'locales.' + KLocale.LOCALE + '.demo');
			{
				if(!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE] && !!this.data.locales[KLocale.LOCALE].demo){
					demo.setValue(this.data.locales[KLocale.LOCALE].demo);
				}
				demo.isLocalized = true;
				demo.setHelp(gettext('$CODA_THEME_DEMO_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(demo);

			var lead = new KTextArea(descPanel, gettext('$CODA_THEME_LEAD'), 'locales.' + KLocale.LOCALE + '.lead');
			{
				if(!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE] && !!this.data.locales[KLocale.LOCALE].lead){
					lead.setValue(this.data.locales[KLocale.LOCALE].lead);
				}
				lead.isLocalized = true;
				lead.setHelp(gettext('$CODA_THEME_LEAD_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(lead);

			var description = new KTextArea(descPanel, gettext('$CODA_THEME_DESCRIPTION'), 'locales.' + KLocale.LOCALE + '.description');
			{
				if(!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE] && !!this.data.locales[KLocale.LOCALE].description){
					description.setValue(this.data.locales[KLocale.LOCALE].description);
				}
				description.isLocalized = true;
				description.setHelp(gettext('$CODA_THEME_DESCRIPTION_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(description);

			var category = new KTextArea(descPanel, gettext('$CODA_THEME_CATEGORY'), 'category');
			{
				if(!!this.data && !!this.data.category){
					category.setValue(this.data.category);
				}
				category.setHelp(gettext('$CODA_THEME_CATEGORY_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(category);
		}
		this.addPanel(descPanel, gettext('$CODA_THEME_DESCRIPTION_PANEL'), true);
		this.addPanel(infoPanel, gettext('$CODA_THEME_GENERALINFO_PANEL'), false);

		var featuresPanel = new CODAArrayList(this, (!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE].features ? this.data.locales[KLocale.LOCALE].features : []), 'locales.' + KLocale.LOCALE + '.features', { title : undefined, description : undefined }, { one_choice : false, add_button : true, filter_input : false});		
		featuresPanel.hash = '/theme/features';
		featuresPanel.isLocalized = true;
		this.addPanel(featuresPanel, gettext('$CODA_THEME_FEATURES_PANEL'), false);

		var gallery = new CODAArrayList(this, (!!this.data && !!this.data.locales && !!this.data.locales[KLocale.LOCALE].gallery ? this.data.locales[KLocale.LOCALE].gallery : []), 'locales.' + KLocale.LOCALE + '.gallery', { title : undefined, url : undefined }, { one_choice : false, add_button : true, filter_input : false});		
		gallery.hash = '/theme/gallery';
		gallery.isLocalized = true;
		this.addPanel(gallery, gettext('$CODA_THEME_GALLERY_PANEL'), false);

		var techPanel = new KPanel(this);
		{
			techPanel.hash = '/general3';

			var wizard = new KTextArea(techPanel, gettext('$CODA_THEME_WIZARD'), 'wizard');
			{
				wizard.height = 200;
				if(!!this.data && !!this.data.wizard){
					wizard.setValue(JSON.stringify(this.data.wizard, null, "\t"));
				}
				wizard.setHelp(gettext('$CODA_THEME_WIZARD_HELP'), CODAForm.getHelpOptions());
			}
			techPanel.appendChild(wizard);

			var configurations = new KTextArea(techPanel, gettext('$CODA_THEME_CONFIGURATIONS'), 'configurations');
			{
				configurations.height = 300;
				if(!!this.data && !!this.data.configurations){
					configurations.setValue(JSON.stringify(this.data.configurations, null, "\t"));
				}
				configurations.setHelp(gettext('$CODA_THEME_CONFIGURATIONS_HELP'), CODAForm.getHelpOptions());
			}
			techPanel.appendChild(configurations);
		}
		this.addPanel(techPanel, gettext('$CODA_THEME_TECH_PANEL'), false);
	};

	CODAThemeForm.prototype.onBadRequest = function(data, request) {
		this.draw();
	};

	CODAThemeForm.prototype.onPreConditionFailed = function(data, request) {
		this.draw();
	};

	CODAThemeForm.prototype.onNotFound = function(data, request) {
		this.draw();
	};

	CODAThemeForm.addWizardPanel = function(parent) {

		var infoPanel = new KPanel(parent);
		{
			infoPanel.hash = '/general';
			infoPanel.title = gettext('$CODA_THEME_WIZ_INFO_PANEL_TITLE');
			infoPanel.description = gettext('$CODA_THEME_WIZ_INFO_PANEL_DESC');
			infoPanel.icon = 'css:fa fa-adjust'

			var href = new KInput(infoPanel, gettext('$CODA_THEME_HREF') + ' *', 'href');
			{
				href.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				href.setHelp(gettext('$CODA_THEME_HREF_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(href);

			var template_id = new KInput(infoPanel, gettext('$CODA_THEME_TEMPLATE_ID') + ' *', 'template_id');
			{
				template_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				template_id.setHelp(gettext('$CODA_THEME_TEMPLATE_ID_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(template_id);

			var icon = new KInput(infoPanel, gettext('$CODA_TEMPLATE_ICON') + ' *', 'icon');
			{
				icon.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				var mediaList = new CODAMediaList(icon, 'image/*');
				icon.appendChild(mediaList);
				icon.setHelp(gettext('$CODA_TEMPLATE_ICON_HELP'), CODAForm.getHelpOptions());
   			}
			infoPanel.appendChild(icon);

			var state = new KRadioButton(infoPanel, gettext('$CODA_THEME_STATE') + ' *', 'state');
			{
				state.addOption('prototype', 'Prototype', false);
				state.addOption('alpha', 'Alpha', false);
				state.addOption('beta', 'Beta', false);
				state.addOption('release_candidate', 'Release Candidate', false);
				state.addOption('final', 'Final', false);

				state.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				state.setHelp(gettext('$CODA_THEME_STATE_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(state);
		}

		var descPanel = new KPanel(parent);
		{
			descPanel.hash = '/general2';
			descPanel.title = gettext('$CODA_THEME_WIZ_DESC_PANEL_TITLE1');
			descPanel.description = gettext('$CODA_THEME_WIZ_DESC_PANEL_DESC1');
			descPanel.icon = 'css:fa fa-pencil'

			var name = new KInput(descPanel, gettext('$CODA_THEME_NAME') + ' (' + KLocale.LANG.toUpperCase() + ')' + ' *', 'locales.' + KLocale.LOCALE + '.name');
			{
				name.isLocalized = true;
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				name.setHelp(gettext('$CODA_THEME_NAME_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(name);

			var lead = new KTextArea(descPanel, gettext('$CODA_THEME_LEAD') + ' (' + KLocale.LANG.toUpperCase() + ')', 'locales.' + KLocale.LOCALE + '.lead');
			{
				lead.isLocalized = true;
				lead.setHelp(gettext('$CODA_THEME_LEAD_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(lead);

			var description = new KTextArea(descPanel, gettext('$CODA_THEME_DESCRIPTION') + ' (' + KLocale.LANG.toUpperCase() + ')', 'locales.' + KLocale.LOCALE + '.description');
			{
				description.isLocalized = true;
				description.setHelp(gettext('$CODA_THEME_DESCRIPTION_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(description);

			var category = new KTextArea(descPanel, gettext('$CODA_THEME_CATEGORY'), 'category');
			{
				category.setHelp(gettext('$CODA_THEME_CATEGORY_HELP'), CODAForm.getHelpOptions());
			}
			descPanel.appendChild(category);
		}
		
		var techPanel = new KPanel(parent);
		{
			techPanel.hash = '/general3';
			techPanel.title = gettext('$CODA_THEME_WIZ_TECH_PANEL_TITLE2');
			techPanel.description = gettext('$CODA_THEME_WIZ_TECH_PANEL_DESC2');
			techPanel.icon = 'css:fa fa-flask'

			var wizard = new KTextArea(techPanel, gettext('$CODA_THEME_WIZARD'), 'wizard');
			{
				wizard.height = 200;
				wizard.setHelp(gettext('$CODA_THEME_WIZARD_HELP'), CODAForm.getHelpOptions());
			}
			techPanel.appendChild(wizard);

			var configurations = new KTextArea(techPanel, gettext('$CODA_THEME_CONFIGURATIONS'), 'configurations');
			{
				configurations.height = 300;
				configurations.setHelp(gettext('$CODA_THEME_CONFIGURATIONS_HELP'), CODAForm.getHelpOptions());
			}
			techPanel.appendChild(configurations);
		}
		
		return [ infoPanel, descPanel, techPanel ];
	};

	KSystem.included('CODAThemeForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAThemesList(parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, null, {
			export_button : false,
			edit_button : true,
			add_button : true,
			delete_button : true
		});
		this.onechoice = false;
		this.pageSize = 5;
		eval('this.table = {' + 
			'"icon" : {' +
				'label : \'<i class="fa fa-picture-o"></i>\',' +
				'fixed : 30' +
			'},' +
			'"locales.' + KLocale.LOCALE + '.name" : {' +
				'label : gettext("$CODA_THEMES_NAME")' +
			'},' +
			'"id" : {' +
				'label : gettext(\'$CODA_THEMES_CODE\')' +
			'}' +
		'}');
		this.LIST_MARGIN = 50;
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODAThemesList.prototype = new CODAList();
	CODAThemesList.prototype.constructor = CODAThemesList;

	CODAThemesList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODAThemesList.prototype.addItems = function(list) {
		for ( var index in this.elements) {

			var listItem = new CODAListItem(list);
			{
				listItem.index = index;
				listItem.data = this.elements[index];
				listItem.data.href = '/themes/' + listItem.data.id;
				listItem.data.icon = '<img src="' + listItem.data.icon + '" ></img>';
				listItem.remove = this.removeItem;
			}
			list.appendChild(listItem);
		}
	};

	CODAThemesList.prototype.removeItem = function() {
		this.kinky.remove(this, 'rest:' + this.data._id, { href : this.data.href, template_id : this.data.template_id });
	}

	KSystem.included('CODAThemesList');
}, Kinky.CODA_INCLUDER_URL);

KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

