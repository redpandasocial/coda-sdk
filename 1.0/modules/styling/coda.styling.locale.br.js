///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$STYLING', 'Estilos e Grafismo', 'br');
settext('$CODA_STYLING_EDIT_MASTER_IMAGES', 'Imagens', 'br');
settext('$CODA_STYLING_EDIT_MASTER_STYLE', 'Cores', 'br');
settext('$CODA_STYLING_EDIT_MASTER_FONT', 'Letra', 'br');
settext('$CODA_STYLING_EDIT_MASTER_TUNNING', 'Sombras', 'br');
settext('$CODA_STYLING_EDIT_MASTER_BORDER', 'Molduras', 'br');
settext('$CODA_STYLING_GOTO_PAGE', 'Ir para página...', 'br');
settext('$CODA_STYLING_REFRESH', '<i class="fa fa-refresh"></i> Recarregar...', 'br');
settext('$CODA_STYLING_PUBLISH', '<i class="fa fa-save"></i> Guardar', 'br');
settext('$CODA_STYLING_REVERT', '<i class="fa fa-undo"></i> Reverter', 'br');

settext('$CODA_STYLING_SAVE_CONFIRM', 'Caso o aplicativo já tenha sido ativado, as alterações irão ser refletidas na versão pública. Deseja continuar?', 'br');
settext('$CODA_STYLING_RESTORE_CONFIRM', 'Esta operação vai reverter TODAS as alterações efectuadas no módulo de Estilos & Grafismo. Deseja continuar?', 'br');
settext('$CODA_STYLING_RESTORE_CONFIRM_AGAIN', 'Tem mesmo a certeza que deseja reverter TODAS as alterações, deixando a aplicação com os estilos e grafismos originais?', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBOX
settext('$CODA_STYLING_TOOLBOX_TITLE', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLS
settext('$CODA_STYLING_MASTER_STYLE_TITLE', 'Estilos', 'br');

settext('$CODA_STYLING_TOOLBOX_TITLE', 'Estilos', 'br');
settext('$CODA_STYLING_IMAGEBOX_TITLE', 'Imagens', 'br');
settext('$CODA_STYLING_COLORBOX_TITLE', 'Cores', 'br');
settext('$CODA_STYLING_SHADOWBOX_TITLE', 'Sombras', 'br');
settext('$CODA_STYLING_FONTBOX_TITLE', 'Letra', 'br');
settext('$CODA_STYLING_BORDERBOX_TITLE', 'Molduras', 'br');
settext('$CODA_STYLING_GOOGLE_FONT_LINK', 'Para visualizar os tipos de letra disponíveis, consulte a página <a href="http://www.google.com/fonts" target="_blank">Google Fonts</a>. Se desejar usar o seu tipo de letra, você poderá enviar ao repositório Google Fonts, <a href="https://docs.google.com/forms/d/1w2JOnVv_Vfcg1H_nploj1FRz4LcFsLaFmFkEj50PyW4/viewform" target="_blank">aqui</a>.', 'br');

settext('BACKGROUND COLORS', 'Fundos', 'br');
settext('TITLE COLORS', 'Títulos', 'br');
settext('TEXT COLORS', 'Texto', 'br');
settext('TITLE AND TEXT COLORS', 'Títulos & Texto', 'br');
settext('BUTTON COLORS', 'Botões', 'br');
settext('SHADOW COLORS', 'Sombras', 'br');
settext('BORDER COLORS', 'Molduras', 'br');
settext('WHEEL COLORS', 'Roda de Partilha', 'br');
settext('LIST SWITCHS', 'Listagem de Posts', 'br');
settext('POST SWITCHS', 'Post', 'br');
settext('BUTTON SWITCHS', 'Botões', 'br');
settext('SECTION SWITCHS', 'Blocos e Secções', 'br');
settext('PROGRESS SWITCHS', 'Roda de Convites', 'br');
settext('WHEEL SHADOWS', 'Roda de Partilha', 'br');
settext('BUTTON SHADOWS', 'Botões', 'br');
settext('TITLE SHADOWS', 'Títulos', 'br');
settext('SECTION SHADOWS', 'Blocos e Secções', 'br');
settext('INVITATION WHEEL COLORS', 'Roda de Convites e Prémios', 'br');

settext('$APPLY', 'APLICAR', 'br');
settext('$THEME-FONT-FAMILY-PRIMARY', 'Tipo de Letra do texto', 'br');
settext('$THEME-FONT-FAMILY-SECONDARY', 'Tipo de Letra dos títulos', 'br');
settext('$THEME-FONT-SIZE', 'Tamanho da letra', 'br');
settext('$THEME-FANGATE-BACKGROUND-IMAGE', 'Imagem de fundo da landing-page/fan-gate', 'br');
settext('$THEME-BODY-BACKGROUND-IMAGE', 'Imagem de fundo global', 'br');
settext('$THEME-HOME-BACKGROUND-IMAGE', 'Imagem de fundo de homepage', 'br');
settext('$THEME-CONTENT-BACKGROUND-IMAGE', 'Imagem de fundo de páginas de conteúdo', 'br');
settext('$THEME-END-BACKGROUND-IMAGE', 'Imagem de fundo quando aplicativo está inativo', 'br');
settext('$THEME-BODY-BACKGROUND-COLOR', 'Cor de fundo global', 'br');
settext('$THEME-HOME-BACKGROUND-COLOR', 'Cor de fundo de homepage', 'br');
settext('$THEME-OPACITY', 'Transparência das cores (onde aplicável)', 'br');
settext('$THEME-BORDER-COLOR', 'Cor principal das molduras (onde aplicável)', 'br');
settext('$THEME-TITLE-COLOR1', 'Cor de títulos principal', 'br');
settext('$THEME-TITLE-COLOR2', 'Cor de títulos secundária', 'br');
settext('$THEME-TITLE-COLOR3', 'Cor de títulos terciária', 'br');
settext('$THEME-TEXT-COLOR1', 'Cor de texto principal', 'br');
settext('$THEME-TEXT-COLOR2', 'Cor de texto secundária', 'br');
settext('$THEME-TEXT-COLOR3', 'Cor de texto terciária', 'br');
settext('$THEME-TEXT-COLOR4', 'Cor de text 4', 'br');
settext('$THEME-TEXT-CORRECT-COLOR', 'Cor de texto para respostas correctas', 'br');
settext('$THEME-TEXT-INCORRECT-COLOR', 'Cor de texto para respostas incorrectas', 'br');
settext('$THEME-BK-COLOR1', 'Cor de fundo principal', 'br');
settext('$THEME-BK-COLOR2', 'Cor de fundo secundária', 'br');
settext('$THEME-BK-COLOR3', 'Cor de fundo terciária', 'br');
settext('$THEME-BK-COLOR4', 'Cor de fundo 4', 'br');
settext('$THEME-BOX-SHADOW-SECTION-SWITCH', 'A caixa do conteúdo da página tem sombra?', 'br');
settext('on', 'LIGADO', 'br');
settext('off', 'DESLIGADO', 'br');
settext('$THEME-TEXT-SHADOW-TITLES-SWITCH', 'O texto dos títulos tem sombra?', 'br');
settext('$THEME-BORDER-SECTION-SWITCH', 'A caixa de conteúdo de página tem moldura?', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-SWITCH', 'A caixa de conteúdo de página tem cantos arredondados?', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-PARTICIPATION-SWITCH', 'A caixa do conteúdo de página tem cantos arredondados?', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-FRIENDS-SWITCH', 'A caixa de visualização de amigos tem cantos arredondados?', 'br');
settext('$THEME-BUTTON-COLOR1', 'Cor de botões principal', 'br');
settext('$THEME-BUTTON-COLOR2', 'Cor de botões secundária', 'br');
settext('$THEME-BUTTON-COLOR3', 'Cor de botões terciária', 'br');
settext('$THEME-BOX-SHADOW-BUTTON-SWITCH', 'Os botões têm sombra?', 'br');
settext('$THEME-TEXT-SHADOW-BUTTON-SWITCH', 'O texto dos botões tem sombra?', 'br');
settext('$THEME-BORDER-BUTTON-SWITCH', 'Os botões têm moldura?', 'br');
settext('$THEME-BORDER-RADIUS-BUTTON-SWITCH', 'Os botões têm cantos arredondados?', 'br');
settext('$THEME-SHARE-COLOR1', 'Cor principal do círculo de share', 'br');
settext('$THEME-SHARE-COLOR2', 'Cor de fundo do círculo de share', 'br');
settext('$THEME-BOX-SHADOW-SHARE-SWITCH', 'O círculo de share tem sombra?', 'br');
settext('$THEME-TEXT-SHADOW-SHARE-SWITCH', 'Os icones do círculo de share têm sombra?', 'br');
settext('$THEME-ELEMENTS-TEXT-SHADOW-COLOR', 'Os itens da listagem têm sombra?', 'br');
settext('$THEME-BUTTON-TEXT-COLOR', 'Cor de texto do botão', 'br');
settext('$THEME-BORDER-POST-SWITCH', 'O item da listagem tem moldura?', 'br');
settext('$THEME-BORDER-RADIUS-POST-SWITCH', 'O item da listagem tem cantos arredondados?', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAStylingLocale_pt');

///////////////////////////////////////////////////////////////////
// TOOLS
settext('$THEME-FONT-FAMILY-PRIMARY_HELP', 'Tipo de letra a aplicar a todos os conteúdos de texto do aplicativo.', 'br');
settext('$THEME-FONT-FAMILY-SECONDARY_HELP', 'Tipo de letra a aplicar a todos os títulos e sub-títulos do aplicativo.', 'br');
settext('$THEME-FONT-SIZE_HELP', 'Tamanho das fontes do aplicativo definida em percentagem.<br> A alteração do tamanho da fonte tem impacto em todos os conteúdos de texto de uma forma proporcional, como por exemplo, entre títulos e sub-títulos., ', 'br');
settext('$THEME-FANGATE-BACKGROUND-IMAGE_HELP', 'Imagem de fundo a aplicar à página de entrada<br>. Apenas será considerada se na definição do aplicativo se tiver Selecionedo a opção de haver uma página de entrada.', 'br');
settext('$THEME-BODY-BACKGROUND-COLOR_HELP', 'Cor de fundo a aplicar às áreas das páginas que não tenham uma imagem.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BODY-BACKGROUND-IMAGE_HELP', '', 'br');
settext('$THEME-HOME-BACKGROUND-COLOR_HELP', '', 'br');
settext('$THEME-HOME-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para a página inicial do aplicativo.<br> Leve em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'br');
settext('$THEME-CONTENT-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para as páginas de conteúdo do aplicativo.<br> Leve em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'br');
settext('$THEME-END-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para a página que é mostrada quando o aplicativo está inativo.<br> Leve em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'br');
settext('$THEME-OPACITY_HELP', 'Transparência a aplicar às cores, definida por um valor entre 0 e 1.', 'br');
settext('$THEME-TITLE-COLOR1_HELP', 'Cor a aplicar aos títulos principais (heading 1) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TITLE-COLOR2_HELP', 'Cor a aplicar aos títulos secundários (heading 2) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TITLE-COLOR3_HELP', 'Cor a aplicar aos títulos terciários (heading 3) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TEXT-COLOR1_HELP', 'Cor a aplicar aos textos das labels (<label>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TEXT-COLOR2_HELP', 'Cor a aplicar aos textos dos parágrafos (<p>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TEXT-COLOR3_HELP', 'Cor a aplicar aos textos dos botões (<button>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TEXT-COLOR4_HELP', '', 'br');
settext('$THEME-TEXT-CORRECT-COLOR_HELP', '', 'br');
settext('$THEME-TEXT-INCORRECT-COLOR_HELP', '', 'br');
settext('$THEME-BK-COLOR1_HELP', 'Cor de fundo a aplicar às secções (<section>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BK-COLOR2_HELP', 'Cor de fundo a aplicar aos cabeçalhos das secções (<header>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BK-COLOR3_HELP', 'Cor de fundo a aplicar às caixas de texto (ex: <textarea>) das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BK-COLOR4_HELP', '', 'br');
settext('$THEME-BORDER-COLOR_HELP', 'Cor a aplicar às molduras dos elementos das páginas.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-TEXT-SHADOW-TITLES-SWITCH_HELP', 'Selecione se os textos dos títulos têm ou não sombra (text-shadow).', 'br');
settext('$THEME-BOX-SHADOW-SECTION-SWITCH_HELP', 'Selecione se as secções (<section>) dos títulos têm ou não sombra (box-shadow).', 'br');
settext('$THEME-BORDER-SECTION-SWITCH_HELP', 'Selecione se as secções (<section>) têm ou não moldura (border).', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-SWITCH_HELP', 'Selecione se as secções (<section>) têm ou não cantos arredondados (border-radius).', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-PARTICIPATION-SWITCH_HELP', '', 'br');
settext('$THEME-BORDER-RADIUS-SECTION-FRIENDS-SWITCH_HELP', '', 'br');
settext('$THEME-BUTTON-COLOR1_HELP', 'Cor a aplicar aos botões. Os botões têm gradientes e esta corresponde à primeira cor usada.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BUTTON-COLOR2_HELP', 'Cor a aplicar aos botões. Os botões têm gradientes e esta corresponde à segunda cor usada.<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BUTTON-COLOR3_HELP', 'Cor a aplicar aos botões. Esta corresponde à cor do botão quando é premido (on mouse down).<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BOX-SHADOW-BUTTON-SWITCH_HELP', 'Selecione se os botões têm ou não sombra (box-shadow).', 'br');
settext('$THEME-TEXT-SHADOW-BUTTON-SWITCH_HELP', 'Selecione se os textos dos botões têm ou não sombra (text-shadow).', 'br');
settext('$THEME-BORDER-BUTTON-SWITCH_HELP', 'Selecione se os botões têm ou não moldura (border)', 'br');
settext('$THEME-BORDER-RADIUS-BUTTON-SWITCH_HELP', 'Selecione se os botões têm ou não cantos arredondados (border-radius).', 'br');
settext('$THEME-SHARE-COLOR1_HELP', 'Cor principal a aplicar à roda de integração com redes sociais para partilha (normalmente disponível no canto superior direito das aplicações).<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-SHARE-COLOR2_HELP', 'Cor secundária a aplicar à roda de integração com redes sociais para partilha (normalmente disponível no canto superior direito das aplicações).<br> Utilize o seletor de cores ou insira o código hexadecimal da cor que deseja usar.', 'br');
settext('$THEME-BOX-SHADOW-SHARE-SWITCH_HELP', 'Selecione se a roda de integração com redes sociais para partilha têm ou não sombra (box-shadow).', 'br');
settext('$THEME-TEXT-SHADOW-SHARE-SWITCH_HELP', 'Selecione se os icones da roda de integração com redes sociais para partilha têm ou não sombra (box-shadow).', 'br');

settext('$THEME-ELEMENTS-TEXT-SHADOW-COLOR_HELP', '', 'br');
settext('$THEME-BORDER-POST-SWITCH_HELP', '', 'br');
settext('$THEME-BORDER-RADIUS-POST-SWITCH_HELP', '', 'br');
settext('$THEME-BUTTON-TEXT-COLOR_HELP', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAStylingLocale_Help_pt');
