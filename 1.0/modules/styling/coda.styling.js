KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAStylingShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.loadRetry = 0;
		
		this.addLocationListener(CODAStylingShell.actions);
		this.setStyle({
			backgroundColor : 'transparent'
		});
		this.offset = {};
	}
}

KSystem.include([
	'KConfirmDialog',
	'KMessageDialog',
	'KButton',
	'CODAStylingToolbox',
	'CODAGenericShell'
], function() {
	CODAStylingShell.prototype = new CODAGenericShell();
	CODAStylingShell.prototype.constructor = CODAStylingShell;
		
	CODAStylingShell.prototype.loadShell = function() {
		this.application = this.app_data;
		for (var k in this.application.channels.elements) {
			if (this.application.channels.elements[k].type == this.getShell().data.channel_type) {
				this.channel = this.application.channels.elements[k];
			}
		}
		this.draw();
	};
	
	CODAStylingShell.prototype.onError = function(data, request) {
		this.shellDef = {
			title : '[NO SHELL]'
		};
		this.draw();
	};		

	CODAStylingShell.prototype.onRemove = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CONFIRM_WIDGET_DELETED_SUCCESS'),
			html : true,
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};
	
	CODAStylingShell.prototype.draw = function() {
		this.setTitle(gettext('$STYLING'));
		
		this.addToolbarSeparator('right');		

		var images = new KButton(this, '<i class="fa fa-picture-o"></i>' + gettext('$CODA_STYLING_EDIT_MASTER_IMAGES'), 'edit-images', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-images'
			});
		});
		this.addToolbarButton(images, 3, 'right');

		var styles = new KButton(this, '<i class="rp rp-brush"></i>' + gettext('$CODA_STYLING_EDIT_MASTER_STYLE'), 'edit-docroot', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-style'
			});
		});
		this.addToolbarButton(styles, 3, 'right');

		var font = new KButton(this, '<i class="fa fa-font"></i>' + gettext('$CODA_STYLING_EDIT_MASTER_FONT'), 'edit-docroot', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-font'
			});
		});
		this.addToolbarButton(font, 5, 'right');

		var shadow = new KButton(this, '<i class="fa fa-adjust"></i>' + gettext('$CODA_STYLING_EDIT_MASTER_TUNNING'), 'edit-docroot', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-tunning'
			});
		});
		this.addToolbarButton(shadow, 1, 'right');

		var border = new KButton(this, '<i class="fa fa-square-o"></i>' + gettext('$CODA_STYLING_EDIT_MASTER_BORDER'), 'edit-docroot', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-border'
			});
		});
		this.addToolbarButton(border, 1, 'right');

		var restore = new KButton(this, gettext('$CODA_STYLING_REVERT'), 'restore', function(event) {
			KDOM.stopEvent(event);			
			KBreadcrumb.dispatchURL({
				hash : '/restore-theme'
			});
		});
		this.addBottomButton(restore, 4);

		var publish = new KButton(this, gettext('$CODA_STYLING_PUBLISH'), 'publish', function(event) {
			KDOM.stopEvent(event);			
			KBreadcrumb.dispatchURL({
				hash : '/save-changes'
			});
		});
		this.addBottomButton(publish);

		var refresh = new KButton(this, gettext('$CODA_STYLING_REFRESH'), 'refresh', function(event) {
			KDOM.stopEvent(event);
			var shell = KDOM.getEventWidget(event).getShell();
			switch (shell.channel.type) {
				case 'facebook' : {
					shell.refreshFBFrame();
					break;
				}
				default : {
					window.document.getElementById('siteDiv').contentDocument.location.reload();
					break;
				}
			}

		});
		this.addBottomButton(refresh, 3);		
		
		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAShellContentBackground', this.content);				
	};

	CODAStylingShell.prototype.visible = function() {
		if (this.display) {
			return;
		}
		CODAGenericShell.prototype.visible.call(this);

		window.onmessage = CODAStylingShell.resizeIframe;

		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990 * 50;
		var fs = Math.round(fp);

		var iframe = window.document.getElementById('siteDiv');
		this.dashboard = window.document.createElement('div');
		{
			this.dashboard.appendChild(iframe);
			iframe.style.display = 'block';	

			this.dashboard.className = 'CODAStylingDashboard';
			this.dashboard.style.marginTop = (41 + fs) + 'px';
			this.dashboard.style.height = (KDOM.getBrowserHeight() - 61 - fs * 2) + 'px';
			this.dashboard.style.opacity = '1';
			
		}
		this.content.appendChild(this.dashboard);

		switch (this.channel.type) {
			case 'facebook' : {
				var input = window.document.getElementById('signed_request');
				this.renderFBFrame();

				var sr = {
					page : {
						liked : true,
						id : this.channel.page_id
					},
					algorithm : 'HMAC-SHA256'
				};
				input.value = Base64.encode('some_sig') + '.' + Base64.encode(JSON.stringify(sr)).replace('+', '-').replace('/', '_');

				this.refreshFBFrame();
				break;
			}
			default : {
				var docroot = this.channel.document_root;
				iframe.src = '/api/1.0/proxy-pass/http/' + docroot;
				break;
			}
		}

		KSystem.addTimer(function() {
			window.document.getElementById('siteDiv').style.opacity = '1';
			KBreadcrumb.dispatchURL({
				hash : '/edit-master-images'
			});
		}, 1000);
	};

	CODAStylingShell.prototype.onResize = function(size) {
		CODAGenericShell.prototype.onResize.call(this, size);

		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990 * 50;
		var fs = Math.round(fp);
		this.dashboard.style.marginTop = (41 + fs) + 'px';
		this.dashboard.style.height = (KDOM.getBrowserHeight() - 61 - fs * 2) + 'px';
		
		switch (this.channel.type) {
			case 'facebook' : {
				this.renderFBFrame();
				break;
			}
			default : {
				break;
			}
		}
	};

	CODAStylingShell.prototype.refreshFBFrame = function() {
		var docroot = this.data.providers.oauth.services;
		var form = window.document.getElementById('app_auth_form');
		form.action = docroot + (docroot.lastIndexOf('/') == docroot.length - 1 ? '' : '/') + '?tid=' + this.channel.app_id + '&referrer_token=' + encodeURIComponent(this.data.referrer_token) + '&tsc=' + (new Date()).getTime(); 
		form.submit();
	};

	CODAStylingShell.prototype.renderFBFrame = function() {
		var iframe = window.document.getElementById('siteDiv');
		iframe.style.width = '810px';
		var bwidth = KDOM.getBrowserWidth();
		var available = bwidth - 405;

		if (available < 845) {
			iframe.style.left = '35px';
		}
		else if (bwidth > 1655) {
			iframe.style.left = (Math.round(bwidth / 2) - 405) + 'px';
		}
		else {
			iframe.style.left = (available - 845) + 'px';
		}
		this.title.style.width = this.dashboard.style.width = (available - 15) + 'px';
	};

	CODAStylingShell.prototype.commit = function() {
		var self = this;
		self.isTainted = KSystem.addTimer(function() {
			if (!!CODAStylingShell.TAINTED) {
				return;
			}
			KSystem.removeTimer(self.isTainted);
			KConfirmDialog.confirm({
				question : gettext('$CODA_STYLING_SAVE_CONFIRM'),
				callback : function() {
					self.kinky.post(self, 'rest:/deferrer/commit', { }, null, 'onSave');
				},
				shell : self.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}, 200, true);

	};

	CODAStylingShell.prototype.restore = function() {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_STYLING_RESTORE_CONFIRM'),
			callback : function() {
				KSystem.addTimer(function() {
					KConfirmDialog.confirm({
						question : gettext('$CODA_STYLING_RESTORE_CONFIRM_AGAIN'),
						callback : function() {
							self.kinky.post(self, 'rest:/applications/self/restore/theme', { }, null, 'onRestore');
						},
						shell : this.shell,
						modal : true,
						width : 400,
						height : 300
					});
				}, 400);
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAStylingShell.prototype.onSave = function(data, request) {
	};

	CODAStylingShell.prototype.onRestore = function(data, request) {
		this.refreshFBFrame();;
	};

	CODAStylingShell.prototype.editStyles = function() {
		var configForm = new CODAStylingToolbox(this, 'color,color-opacity');
		{
			configForm.hash = '/styling/colors';
			configForm.setTitle(gettext('$CODA_STYLING_COLORBOX_TITLE'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};	

	CODAStylingShell.prototype.editImages = function() {
		var configForm = new CODAStylingToolbox(this, 'image');
		{
			configForm.hash = '/styling/images';
			configForm.setTitle(gettext('$CODA_STYLING_IMAGEBOX_TITLE'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};	

	CODAStylingShell.prototype.editFont = function() {
		var configForm = new CODAStylingToolbox(this, 'font,font-size');
		{
			configForm.hash = '/styling/font';
			configForm.setTitle(gettext('$CODA_STYLING_FONTBOX_TITLE'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};	

	CODAStylingShell.prototype.editTunning = function() {
		var configForm = new CODAStylingToolbox(this, 'shadow-switch');
		{
			configForm.hash = '/styling/tunning';
			configForm.setTitle(gettext('$CODA_STYLING_SHADOWBOX_TITLE'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};	

	CODAStylingShell.prototype.editBorder = function() {
		var configForm = new CODAStylingToolbox(this, 'border-switch');
		{
			configForm.hash = '/styling/border';
			configForm.setTitle(gettext('$CODA_STYLING_BORDERBOX_TITLE'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};	

	CODAStylingShell.resizeIframe = function(e) {
		if (typeof(e.data) == 'object' && !!e.data.action) {
			switch(e.data.action) {
				case 'resize' : {
					var iframe = window.document.getElementById('siteDiv');
					iframe.style.height = e.data.height + 'px';
					break;
				}
				case 'tainted' : {
					CODAStylingShell.TAINTED = true;
					break;
				}
				case 'saved' : {
					CODAStylingShell.TAINTED = false;
					break;
				}
			}
		}
	};

	CODAStylingShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]) {
			case 'edit-master-style': {
				Kinky.getDefaultShell().editStyles();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-master-images': {
				Kinky.getDefaultShell().editImages();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-master-font': {
				Kinky.getDefaultShell().editFont();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-master-tunning': {
				Kinky.getDefaultShell().editTunning();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-master-border': {
				Kinky.getDefaultShell().editBorder();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'save-changes': {
				Kinky.getDefaultShell().commit();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}			
			case 'restore-theme': {
				Kinky.getDefaultShell().restore();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};
	
	CODAStylingShell.LEFT_FRAME_MARGIN = 75;
	CODAStylingShell.TOOLBOX_WIDTH = 500;

	KSystem.included('CODAStylingShell');
}, Kinky.CODA_INCLUDER_URL);
function CODAStylingToolbox(parent, filter) {
	if (parent != null) {
		CODAForm.call(this, parent, 'rest:/applications/self', null, null);
		/*this.headers = {
			'Authorization' : 'OAuth2.0 ' + decodeURIComponent(this.getShell().data.module_token)
		}*/
		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.filter = filter;
	}
}

KSystem.include([
	'KInput',
	'KHidden',
	'KColorPicker',
	'KSlider',
	'KRadioButton',
	'KImageUpload',
	'CODAMediaList',
	'CODAForm'
], function() {
	CODAStylingToolbox.prototype = new CODAForm();
	CODAStylingToolbox.prototype.constructor = CODAStylingToolbox;

	CODAStylingToolbox.prototype.loadForm = function() {
		this.kinky.post(this, 'rest:/applications/self/get/theme', { type : this.data.channels.elements[0].type }, null, 'onLoadTheme');
	};

	CODAStylingToolbox.prototype.onLoadTheme = function(data) {
		this.theme = data;

		this.target.collection = 'rest:/applications/self/save/theme';
		this.target.href = undefined;

		if (!!this.filter && this.filter.indexOf('font') != -1) {
			this.kinky.get(this, 'https://www.googleapis.com/webfonts/v1/webfonts', { key : 'AIzaSyDfblLlAiPod2JUBceQqMCnXEmN3bQ-Fmw' }, null, 'onLoadFonts');
		}
		else {
			this.draw();
		}
	};

	CODAStylingToolbox.prototype.onLoadFonts = function(data) {
		this.fonts = data;
		this.draw();
	};

	CODAStylingToolbox.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target.id = this.target.href = data._id;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAStylingToolbox.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAStylingToolbox.prototype.addMinimizedPanel = function() {
		var go = new KButton(this, gettext('$APPLY'), 'apply', CODAForm.sendRest);
		go.hash = '/buttons/apply';
		go.setText('<i class="fa fa-chevron-circle-right"></i> ' + gettext('$APPLY'), true);
		go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(go);

		var panel = new KPanel(this);
		{
			panel.hash = '/general';

			if (this.filter.indexOf('font') != -1) {
				var warning = new KText(panel);
				{
					warning.setText(gettext('$CODA_STYLING_GOOGLE_FONT_LINK'), true);
				}
				panel.appendChild(warning);
			}

			for (var f in this.theme.fields) {
				if (!!this.theme.fields[f].separator) {
					if (!this.filter || this.filter.indexOf(this.theme.fields[f].options.type) != -1) {
						this.addSeparator(gettext(this.theme.fields[f].separator), panel);
					}
					var separator = new KHidden(panel, 'fields[' + f + '].separator');
					{
						separator.setValue(this.theme.fields[f].separator);
					}
					panel.appendChild(separator);
				}

				var name = new KHidden(panel, 'fields[' + f + '].name');
				{
					name.setValue(this.theme.fields[f].name);
				}
				panel.appendChild(name);

				var value = undefined;
				{
					if (!!this.filter && this.filter.indexOf(this.theme.fields[f].options.type) == -1) {
						value = new KHidden(panel, 'fields[' + f + '].value');
						value.setValue(this.theme.fields[f].value);
					}
					else {
						switch (this.theme.fields[f].options.type) {
							case 'font' : {
								value = new KAutoCompletion(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value');
								for (var k in this.fonts.items) {
									value.addOption(k, this.fonts.items[k].family, this.theme.fields[f].value == '"' + this.fonts.items[k].family + '"')
								}
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
							
							case 'color-opacity' : {
								value = new KSlider(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value', { percentage : false, limits : this.theme.fields[f].options.limits });
								value.setValue(this.theme.fields[f].value);
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
							case 'font-size' :
							case 'percentage' : {
								value = new KSlider(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value', { percentage : true, limits : this.theme.fields[f].options.limits });
								value.setValue(this.theme.fields[f].value);
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
							case 'image' : {
								value = new KImageUpload(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + '', 'fields[' + f + '].value', CODAGenericShell.getUploadAuthorization());
								var filter = { mimetype : 'image/*' };
								if (!!this.theme.fields[f].options.limits && this.theme.fields[f].options.limits instanceof Array && this.theme.fields[f].options.limits.length == 4) {
									var id = this.getShell().data.app_id;
									CODAImageCrop.addCrop(value, {
										force_resize : true,
										force_resize_to : (this.theme.fields[f].options.limits[0] == this.theme.fields[f].options.limits[1] ? (this.theme.fields[f].options.limits[2] == this.theme.fields[f].options.limits[3] ? 'both' : 'width') : (this.theme.fields[f].options.limits[2] == this.theme.fields[f].options.limits[3] ? 'height' : undefined)),
										keep_aspect_ratio : this.theme.fields[f].options.limits[0] == this.theme.fields[f].options.limits[1] && this.theme.fields[f].options.limits[2] == this.theme.fields[f].options.limits[3],
										only_metadata : false,
										width : this.theme.fields[f].options.limits[1] != -1 ? this.theme.fields[f].options.limits[1] : this.theme.fields[f].options.limits[0],
										height : this.theme.fields[f].options.limits[3] != -1 ? this.theme.fields[f].options.limits[3] : this.theme.fields[f].options.limits[2],
										media_server : 'media_url_2',
										target_uri : '/' + id.charAt(0) + '/' + id + '/styling/' + this.theme.fields[f].name.replace(/\$/g, '').toLowerCase() + '.png',
										out_mime : 'image/png'
									});
								}
								var mediaList = new CODAMediaList(value, filter);
								value.appendChild(mediaList);
								// Default images now have absolute URI which make them available and visible
								var v = this.theme.fields[f].value.replace(/url/, '').replace(/\(/, '').replace(/\)/, '').replace(/\'/g, '').trim();
								if (!!v) {
									value.setValue(v);
								}
								break;
							}
							case 'border-switch' : 
							case 'shadow-switch' : 
							case 'radio' : {
								value = new KRadioButton(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value');
								if (!!this.theme.fields[f].options.values) {
									for (var v in this.theme.fields[f].options.values) {
										value.addOption(this.theme.fields[f].options.values[v].id, gettext(this.theme.fields[f].options.values[v].label), this.theme.fields[f].options.values[v].id == this.theme.fields[f].value);
									}
								}
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
							case 'color' : {
								value = new KColorPicker(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value');
								value.setValue(ColorConverter.toRGB(this.theme.fields[f].value));
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
							default : {
								value = new KInput(panel, gettext(this.theme.fields[f].options.label || this.theme.fields[f].name) + ' *', 'fields[' + f + '].value');
								value.setValue(this.theme.fields[f].value);
								value.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
								break;
							}
						}
					}

					value.setHelp(gettext(this.theme.fields[f].name + '_HELP'), CODAStylingToolbox.getHelpOptions());
				}
				panel.appendChild(value);

				var name = new KHidden(panel, 'fields[' + f + '].options');
				{
					name.setValue(this.theme.fields[f].options);
				}
				panel.appendChild(name);
			}
		}
		this.addPanel(panel, gettext('$CODA_STYLING_MASTER_STYLE_TITLE'), true);

		this.hideConfirm();
		this.hideCancel();
	};

	CODAStylingToolbox.prototype.setDefaults = function(params){
		params.type = this.data.channels.elements[0].type; 
		for (var f in params.fields) {
			if (params.fields[f].options.type == 'image') {
				if (!!params.fields[f].value && params.fields[f].value.indexOf('url(') != 0) {
					params.fields[f].value = 'url(\'' + params.fields[f].value + '\')';
				}
				else if (!params.fields[f].value) {
					params.fields[f].value = 'url(\'\')';
				}
			}
			else if (params.fields[f].options.type == 'font') {
				if (!!this.fonts) {
					var index = (typeof params.fields[f].value == 'string' ? parseInt(params.fields[f].value) : params.fields[f].value);
					if (!isNaN(index)) {
						var font = this.fonts.items[index];
						params.fields[f].value = '"' + font.family + '"';
						params.fields[f].data = font;
					}
				}
			}
		}
	};

	CODAStylingToolbox.prototype.onAccepted = CODAStylingToolbox.prototype.onPost = CODAStylingToolbox.prototype.onCreated = function(data, request){
		this.enable();
		var shell = this.getShell();
		switch (shell.channel.type) {
			case 'facebook' : {
				shell.refreshFBFrame();
				break;
			}
			default : {
				window.document.getElementById('siteDiv').contentDocument.location.reload();
				break;
			}
		}
	};

	CODAStylingToolbox.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAStylingToolbox.getHelpOptions = function() {
		return KSystem.clone(CODAStylingToolbox.HELP_OPTIONS);
	};

	CODAStylingToolbox.HELP_OPTIONS = {
		gravity : {
			x : 'right',
			y : 'middle'
		},
		max : {
			width : 450
		},
		offsetX : 20,
		offsetY : 0,
		zoom : {}, 
		cssClass : 'CODAInputBaloon'
	};

	KSystem.included('CODAStylingToolbox');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

