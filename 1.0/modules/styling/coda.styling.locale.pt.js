///////////////////////////////////////////////////////////////////
// TOOLS
settext('$THEME-FONT-FAMILY-PRIMARY_HELP', 'Tipo de letra a aplicar a todos os conteúdos de texto da aplicação.', 'pt');
settext('$THEME-FONT-FAMILY-SECONDARY_HELP', 'Tipo de letra a aplicar a todos os títulos e sub-títulos da aplicação.', 'pt');
settext('$THEME-FONT-SIZE_HELP', 'Tamanho das fontes da aplicação definida em percentagem.<br> A alteração do tamanho da fonte tem impacto em todos os conteúdos de texto de uma forma proporcional, como por exemplo, entre títulos e sub-títulos., ', 'pt');
settext('$THEME-FANGATE-BACKGROUND-IMAGE_HELP', 'Imagem de fundo a aplicar à página de entrada<br>. Apenas será considerada se na definição da aplicação se tiver seleccionado a opção de haver uma página de entrada.', 'pt');
settext('$THEME-BODY-BACKGROUND-COLOR_HELP', 'Cor de fundo a aplicar às áreas das páginas que não tenham uma imagem.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BODY-BACKGROUND-IMAGE_HELP', '', 'pt');
settext('$THEME-HOME-BACKGROUND-COLOR_HELP', '', 'pt');
settext('$THEME-HOME-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para a página inicial da aplicação.<br> Tem em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'pt');
settext('$THEME-CONTENT-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para as páginas de conteúdo da aplicação.<br> Tem em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'pt');
settext('$THEME-END-BACKGROUND-IMAGE_HELP', 'Imagem de fundo para a página que é mostrada quando a aplicação está inativa.<br> Tem em consideração a dimensão da imagem, pois não haverá corte e será apresentada de cima para baixo e da esquerda para a direita.<br>Para que fique toda visível deverá ter, no máximo, 810px de largura.', 'pt');
settext('$THEME-OPACITY_HELP', 'Transparência a aplicar às cores, definida por um valor entre 0 e 1.', 'pt');
settext('$THEME-TITLE-COLOR1_HELP', 'Cor a aplicar aos títulos principais (heading 1) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TITLE-COLOR2_HELP', 'Cor a aplicar aos títulos secundários (heading 2) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TITLE-COLOR3_HELP', 'Cor a aplicar aos títulos terciários (heading 3) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TEXT-COLOR1_HELP', 'Cor a aplicar aos textos das labels (label) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TEXT-COLOR2_HELP', 'Cor a aplicar aos textos dos parágrafos (p) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TEXT-COLOR3_HELP', 'Cor a aplicar aos textos dos botões (button) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TEXT-COLOR4_HELP', '', 'pt');
settext('$THEME-WHEEL-COLOR1_HELP', 'Cor principal da roda de convites', 'pt');
settext('$THEME-WHEEL-COLOR2_HELP', 'Cor secundária da roda de convites', 'pt');
settext('$THEME-TEXT-CORRECT-COLOR_HELP', '', 'pt');
settext('$THEME-TEXT-INCORRECT-COLOR_HELP', '', 'pt');
settext('$THEME-BK-COLOR1_HELP', 'Cor de fundo a aplicar às secções (section) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BK-COLOR2_HELP', 'Cor de fundo a aplicar aos cabeçalhos das secções (header) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BK-COLOR3_HELP', 'Cor de fundo a aplicar às caixas de texto (ex: textarea) das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BK-COLOR4_HELP', '', 'pt');
settext('$THEME-BORDER-COLOR_HELP', 'Cor a aplicar às molduras dos elementos das páginas.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-TEXT-SHADOW-TITLES-SWITCH_HELP', 'Seleciona se os textos dos títulos têm ou não sombra (text-shadow).', 'pt');
settext('$THEME-BOX-SHADOW-SECTION-SWITCH_HELP', 'Seleciona se as secções (section) dos títulos têm ou não sombra (box-shadow).', 'pt');
settext('$THEME-BORDER-SECTION-SWITCH_HELP', 'Seleciona se as secções (section) têm ou não moldura (border).', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-SWITCH_HELP', 'Seleciona se as secções (section) têm ou não cantos arredondados (border-radius).', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-PARTICIPATION-SWITCH_HELP', '', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-FRIENDS-SWITCH_HELP', '', 'pt');
settext('$THEME-BORDER-PROGRESS-SWITCH_HELP', '', 'pt');
settext('$THEME-BUTTON-COLOR1_HELP', 'Cor a aplicar aos botões. Os botões têm gradientes e esta corresponde à primeira cor usada.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BUTTON-COLOR2_HELP', 'Cor a aplicar aos botões. Os botões têm gradientes e esta corresponde à segunda cor usada.<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BUTTON-COLOR3_HELP', 'Cor a aplicar aos botões. Esta corresponde à cor do botão quando é premido (on mouse down).<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BOX-SHADOW-BUTTON-SWITCH_HELP', 'Seleciona se os botões têm ou não sombra (box-shadow).', 'pt');
settext('$THEME-TEXT-SHADOW-BUTTON-SWITCH_HELP', 'Seleciona se os textos dos botões têm ou não sombra (text-shadow).', 'pt');
settext('$THEME-BORDER-BUTTON-SWITCH_HELP', 'Seleciona se os botões têm ou não moldura (border)', 'pt');
settext('$THEME-BORDER-RADIUS-BUTTON-SWITCH_HELP', 'Seleciona se os botões têm ou não cantos arredondados (border-radius).', 'pt');
settext('$THEME-SHARE-COLOR1_HELP', 'Cor principal a aplicar à roda de interacção com redes sociais para partilha (normalmente disponível no canto superior direito das aplicações).<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-SHARE-COLOR2_HELP', 'Cor secundária a aplicar à roda de interacção com redes sociais para partilha (normalmente disponível no canto superior direito das aplicações).<br> Utiliza o selector de cores ou insere o código hexadecimal da cor que queres usar.', 'pt');
settext('$THEME-BOX-SHADOW-SHARE-SWITCH_HELP', 'Seleciona se a roda de interacção com redes sociais para partilha têm ou não sombra (box-shadow).', 'pt');
settext('$THEME-TEXT-SHADOW-SHARE-SWITCH_HELP', 'Seleciona se os icons da roda de interacção com redes sociais para partilha têm ou não sombra (box-shadow).', 'pt');

settext('$THEME-ELEMENTS-TEXT-SHADOW-COLOR_HELP', '', 'pt');
settext('$THEME-BORDER-POST-SWITCH_HELP', '', 'pt');
settext('$THEME-BORDER-RADIUS-POST-SWITCH_HELP', '', 'pt');
settext('$THEME-BUTTON-TEXT-COLOR_HELP', '', 'pt');


//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAStylingLocale_Help_pt');
///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$STYLING', 'Estilos e Grafismo', 'pt');
settext('$CODA_STYLING_EDIT_MASTER_IMAGES', 'Imagens', 'pt');
settext('$CODA_STYLING_EDIT_MASTER_STYLE', 'Cores', 'pt');
settext('$CODA_STYLING_EDIT_MASTER_FONT', 'Letra', 'pt');
settext('$CODA_STYLING_EDIT_MASTER_TUNNING', 'Sombras', 'pt');
settext('$CODA_STYLING_EDIT_MASTER_BORDER', 'Molduras', 'pt');
settext('$CODA_STYLING_GOTO_PAGE', 'Ir para página...', 'pt');
settext('$CODA_STYLING_REFRESH', '<i class="fa fa-refresh"></i> Recarregar...', 'pt');
settext('$CODA_STYLING_PUBLISH', '<i class="fa fa-save"></i> Guardar', 'pt');
settext('$CODA_STYLING_REVERT', '<i class="fa fa-undo"></i> Reverter', 'pt');

settext('$CODA_STYLING_SAVE_CONFIRM', 'Caso a aplicação já tenha sido activada, as alterações irão ser reflectidas na versão pública. Desejas prosseguir?', 'pt');
settext('$CODA_STYLING_RESTORE_CONFIRM', 'Esta operação vai reverter TODAS as alterações efectuadas no módulo de Estilos & Grafismo. Desejas prosseguir?', 'pt');
settext('$CODA_STYLING_RESTORE_CONFIRM_AGAIN', 'Tens mesmo a certeza que desejas reverter TODAS as alterações, deixando a aplicação com os estilos e grafismos originais?', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBOX
settext('$CODA_STYLING_TOOLBOX_TITLE', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLS
settext('$CODA_STYLING_MASTER_STYLE_TITLE', 'Estilos', 'pt');

settext('$CODA_STYLING_TOOLBOX_TITLE', 'Estilos', 'pt');
settext('$CODA_STYLING_IMAGEBOX_TITLE', 'Imagens', 'pt');
settext('$CODA_STYLING_COLORBOX_TITLE', 'Cores', 'pt');
settext('$CODA_STYLING_SHADOWBOX_TITLE', 'Sombras', 'pt');
settext('$CODA_STYLING_FONTBOX_TITLE', 'Letra', 'pt');
settext('$CODA_STYLING_BORDERBOX_TITLE', 'Molduras', 'pt');
settext('$CODA_STYLING_GOOGLE_FONT_LINK', 'Para visualizares os tipos de letra disponíveis, consulta a página <a href="http://www.google.com/fonts" target="_blank">Google Fonts</a>. Se quiseres utilizar o teu tipo de letra, poderás submetê-lo ao repositório Google Fonts, <a href="https://docs.google.com/forms/d/1w2JOnVv_Vfcg1H_nploj1FRz4LcFsLaFmFkEj50PyW4/viewform" target="_blank">aqui</a>.', 'pt');

settext('BACKGROUND COLORS', 'Fundos', 'pt');
settext('TITLE COLORS', 'Títulos', 'pt');
settext('TEXT COLORS', 'Texto', 'pt');
settext('TITLE AND TEXT COLORS', 'Títulos & Texto', 'pt');
settext('BUTTON COLORS', 'Botões', 'pt');
settext('SHADOW COLORS', 'Sombras', 'pt');
settext('BORDER COLORS', 'Molduras', 'pt');
settext('WHEEL COLORS', 'Roda de Partilha', 'pt');
settext('LIST SWITCHS', 'Listagem de Posts', 'pt');
settext('POST SWITCHS', 'Post', 'pt');
settext('BUTTON SWITCHS', 'Botões', 'pt');
settext('SECTION SWITCHS', 'Blocos e Secções', 'pt');
settext('PROGRESS SWITCHS', 'Roda de Convites', 'pt');
settext('WHEEL SHADOWS', 'Roda de Partilha', 'pt');
settext('BUTTON SHADOWS', 'Botões', 'pt');
settext('TITLE SHADOWS', 'Títulos', 'pt');
settext('SECTION SHADOWS', 'Blocos e Secções', 'pt');
settext('INVITATION WHEEL COLORS', 'Roda de Convites e Prémios', 'pt');

settext('$APPLY', 'APLICAR', 'pt');
settext('$THEME-FONT-FAMILY-PRIMARY', 'Tipo de Letra do texto', 'pt');
settext('$THEME-FONT-FAMILY-SECONDARY', 'Tipo de Letra dos títulos', 'pt');
settext('$THEME-FONT-SIZE', 'Tamanho da letra', 'pt');
settext('$THEME-FANGATE-BACKGROUND-IMAGE', 'Imagem de fundo da landing-page/fan-gate', 'pt');
settext('$THEME-BODY-BACKGROUND-IMAGE', 'Imagem de fundo global', 'pt');
settext('$THEME-HOME-BACKGROUND-IMAGE', 'Imagem de fundo de homepage', 'pt');
settext('$THEME-CONTENT-BACKGROUND-IMAGE', 'Imagem de fundo de páginas de conteúdo', 'pt');
settext('$THEME-END-BACKGROUND-IMAGE', 'Imagem de fundo quando aplicação está inativa', 'pt');
settext('$THEME-BODY-BACKGROUND-COLOR', 'Cor de fundo global', 'pt');
settext('$THEME-HOME-BACKGROUND-COLOR', 'Cor de fundo de homepage', 'pt');
settext('$THEME-OPACITY', 'Transparência das cores (onde aplicável)', 'pt');
settext('$THEME-BORDER-COLOR', 'Cor principal das molduras (onde aplicável)', 'pt');
settext('$THEME-TITLE-COLOR1', 'Cor de títulos principal', 'pt');
settext('$THEME-TITLE-COLOR2', 'Cor de títulos secundária', 'pt');
settext('$THEME-TITLE-COLOR3', 'Cor de títulos terciária', 'pt');
settext('$THEME-TEXT-COLOR1', 'Cor de texto principal', 'pt');
settext('$THEME-TEXT-COLOR2', 'Cor de texto secundária', 'pt');
settext('$THEME-TEXT-COLOR3', 'Cor de texto terciária', 'pt');
settext('$THEME-TEXT-COLOR4', 'Cor de text 4', 'pt');
settext('$THEME-TEXT-CORRECT-COLOR', 'Cor de texto para respostas correctas', 'pt');
settext('$THEME-TEXT-INCORRECT-COLOR', 'Cor de texto para respostas incorrectas', 'pt');
settext('$THEME-BK-COLOR1', 'Cor de fundo principal', 'pt');
settext('$THEME-BK-COLOR2', 'Cor de fundo secundária', 'pt');
settext('$THEME-BK-COLOR3', 'Cor de fundo terciária', 'pt');
settext('$THEME-BK-COLOR4', 'Cor de fundo 4', 'pt');
settext('$THEME-WHEEL-COLOR1', 'Cor principal da roda de convites', 'pt');
settext('$THEME-WHEEL-COLOR2', 'Cor secundária da roda de convites', 'pt');
settext('$THEME-BOX-SHADOW-SECTION-SWITCH', 'A caixa do conteúdo da página tem sombra?', 'pt');
settext('on', 'LIGADO', 'pt');
settext('off', 'DESLIGADO', 'pt');
settext('$THEME-TEXT-SHADOW-TITLES-SWITCH', 'O texto dos títulos tem sombra?', 'pt');
settext('$THEME-BORDER-SECTION-SWITCH', 'A caixa de conteúdo de página tem moldura?', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-SWITCH', 'A caixa de conteúdo de página tem cantos arredondados?', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-PARTICIPATION-SWITCH', 'A caixa do conteúdo de página tem cantos arredondados?', 'pt');
settext('$THEME-BORDER-RADIUS-SECTION-FRIENDS-SWITCH', 'A caixa de visualização de amigos tem cantos arredondados?', 'pt');
settext('$THEME-BORDER-PROGRESS-SWITCH', 'A caixa de visualização de convites tem cantos arredondados?', 'pt');
settext('$THEME-BUTTON-COLOR1', 'Cor de botões principal', 'pt');
settext('$THEME-BUTTON-COLOR2', 'Cor de botões secundária', 'pt');
settext('$THEME-BUTTON-COLOR3', 'Cor de botões terciária', 'pt');
settext('$THEME-BOX-SHADOW-BUTTON-SWITCH', 'Os botões têm sombra?', 'pt');
settext('$THEME-TEXT-SHADOW-BUTTON-SWITCH', 'O texto dos botões tem sombra?', 'pt');
settext('$THEME-BORDER-BUTTON-SWITCH', 'Os botões têm moldura?', 'pt');
settext('$THEME-BORDER-RADIUS-BUTTON-SWITCH', 'Os botões têm cantos arredondados?', 'pt');
settext('$THEME-SHARE-COLOR1', 'Cor principal do círculo de share', 'pt');
settext('$THEME-SHARE-COLOR2', 'Cor de fundo do círculo de share', 'pt');
settext('$THEME-BOX-SHADOW-SHARE-SWITCH', 'O círculo de share tem sombra?', 'pt');
settext('$THEME-TEXT-SHADOW-SHARE-SWITCH', 'Os icones do círculo de share têm sombra?', 'pt');
settext('$THEME-ELEMENTS-TEXT-SHADOW-COLOR', 'Os itens da listagem têm sombra?', 'pt');
settext('$THEME-BUTTON-TEXT-COLOR', 'Cor de texto do botão', 'pt');
settext('$THEME-BORDER-POST-SWITCH', 'O item da listagem tem moldura?', 'pt');
settext('$THEME-BORDER-RADIUS-POST-SWITCH', 'O item da listagem tem cantos arredondados?', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAStylingLocale_pt');

