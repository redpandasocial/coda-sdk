settext('$CODA_ACCOUNTS_SHELL_TITLE', 'Participantes e Participações', 'br');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$NEW_USER_PROPERTIES', 'NOVO USUÁRIO', 'br');
settext('$EDIT_USER_PROPERTIES', 'Usuário ', 'br');

settext('$CODA_ACCOUNTS_ITEM_FORM_NAME', 'Nome', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_EMAIL', 'E-mail', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID', 'Nome de usuário', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_STATE', 'Estado', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID_LABEL', 'Nome de usuário', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS', 'Senha', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_CONFIRM', 'Confirmação de senha', 'br');

settext('id', 'Identificador', 'br');
settext('first_name', 'Nome', 'br');
settext('last_name', 'Sobrenome', 'br');
settext('link', 'URL do Perfil', 'br');
settext('username', 'Apelido', 'br');
settext('gender', 'Gênero', 'br');
settext('timezone', 'Fuso horário', 'br');
settext('locale', 'Idioma', 'br');
settext('birthday', 'Data de Nascimento', 'br');
settext('email_marketing', 'Email marketing', 'br');

settext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES', 'Participação', 'br');
settext('$PARTICIPATION_ITEM_FORM_OWNER', 'Participante', 'br');
settext('$PARTICIPATION_ITEM_FORM_START_DATE', 'Data de Início da Part.', 'br');
settext('$PARTICIPATION_ITEM_FORM_START_DATE_HELP', '', 'br');
settext('$PARTICIPATION_ITEM_FORM_END_DATE', 'Data de Fim da Part.', 'br');
settext('$PARTICIPATION_ITEM_FORM_END_DATE_HELP', '', 'br');
settext('$PARTICIPATION_ITEM_FORM_VALUE', 'Pontuação', 'br');
settext('$CODA_PARTICIPATION_FIELD_GENERIC_NAME', 'Campo de Submissão n. ', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_QUESTION', '<span style="font-weight: bold">Pergunta:</span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWERS', '<span style="font-weight: bold">Resposta:</span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT', '<span style="font-weight: bold">Correta?</span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT_TRUE', 'Sim', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT_FALSE', 'Não', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED', '<span style="font-weight: bold">Tempo limite atingido?</span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED_FALSE', 'Sim', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED_TRUE', 'Não', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIME', '<span style="font-weight: bold">Respondido em: </span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_POINTS', '<span style="font-weight: bold">Pontuação: </span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_FRIEND', '<span style="font-weight: bold">Amigo: </span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS', '<span style="font-weight: bold">Estado do convite: </span>', 'br');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_ALREADY_REGISTERED', 'Já estava registrado', 'br');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_INVITED', 'À espera de confirmação', 'br');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_REGISTERED', 'Aceito', 'br');

settext('$CODA_PARTICIPATION_STATUS_COMPLETED', 'Completa', 'br');
settext('$CODA_PARTICIPATION_STATUS_ACTIVE', 'Ativa', 'br');
settext('$CODA_PARTICIPATION_NOT_FINISHED_YET', 'Pendente', 'br');

settext('$CODA_PARTICIPATION_MEDIA_FIELD_NAME', 'Media submetido n.', 'br');
settext('$CODA_PARTICIPATION_FRIENDSDISCOUNT_FIELD_NAME', 'Amigo convidado n. ', 'br');
settext('$CODA_PARTICIPATION_QUIZ_FIELD_NAME', 'Pergunta n. ', 'br');

settext('$PARTICIPATION_WINNER_ITEM_FORM_RANKING', 'Posição', 'br');
settext('$PARTICIPATION_WINNER_ITEM_PRIZE_SELECTOR', 'Prêmio a atribuir', 'br');
settext('$PARTICIPATION_WINNER_GENERATE_PRIZE', 'Atribuir prêmio automaticamente?', 'br');
settext('$PARTICIPATION_WINNER_PRIZE_GENERATED', 'Informação', 'br');
settext('$PARTICIPATION_WINNER_PRIZE_SENDED', 'O prêmio selecionado já foi atribuído à participação', 'br');
settext('$CHECKBOX_TRUE_OPTION', 'SIM', 'br');

settext('$FORM_GENERALINFO_PANEL', 'INFO GERAL', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$CAMPAIGNS_PHASE', 'Fase «$PHASE_NAME»', 'br');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_ICON', '<i class="fa fa-user"></i>', 'br');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_TOOLTIP', 'Ver participante', 'br');
settext('$PARTICIPATIONS_LIST_WINNER_ICON', '<i class="fa fa-trophy"></i>', 'br');
settext('$PARTICIPATIONS_LIST_WINNER_TOOLTIP', 'Marcar como vencedor', 'br');
settext('$CAMPAIGNS_LIST_PARTICIPANT', '<i class="fa fa-user"></i> Participante', 'br');
settext('$CAMPAIGNS_LIST_START_DATE', '<i class="fa fa-calendar"></i> Data Início da Part.', 'br');
settext('$CAMPAIGNS_LIST_END_DATE', '<i class="fa fa-calendar"></i> Data Final da Part.', 'br');
settext('$CAMPAIGNS_LIST_POINTS', '<i class="fa fa-calendar"></i> Pontuação', 'br');
settext('$CAMPAIGNS_LIST_RANKING', '<i class="fa fa-calendar"></i> Posição', 'br');
settext('$CAMPAIGNS_LIST_VALUE', '<i class="fa fa-pencil"></i> Submissão', 'br');
settext('$LIST_CAMPAIGNPARTICIPATIONS_PROPERTIES', 'Participações', 'br');

settext('$PRIZE_TYPE_VOUCHER_PDF_CODE', 'application/pdf; charset=binary', 'br');
settext('$CODA_ACCOUNTS_URL', '<i class="fa fa-link"></i> Perfil', 'br');

settext('$CODA_PARTICIPATION_FILTER_PHASE', 'filtrar por fase...', 'br');

settext('$CODA_ACCOUNTS_NAME', 'Nome', 'br');
settext('$CODA_ACCOUNTS_ID', 'Identificador', 'br');
settext('$CODA_ACCOUNTS_LIST_TITLE', 'Participantes', 'br');
settext('$CODA_ACCOUNTS_MUG', '<i class="fa fa-picture-o"></i>', 'br');

settext('$CODA_ACCOUNTS_TYPE_INSTAGRAM', '<i class="fa fa-instagram"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_FACEBOOK', '<i class="fa fa-facebook-square"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_FLICKR', '<i class="fa fa-flickr"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_GOOGLE', '<i class="fa fa-google-plus-square"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_GOOGLE+', '<i class="fa fa-google-plus-square"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_LINKEDIN', '<i class="fa fa-linkedin-square"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_TUMBLR', '<i class="fa fa-tumblr-square"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_TWITTER', '<i class="fa fa-twitter"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_YAHOO', '<i class="rp rp-yahoo"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_BOX', '<i class="rp rp-box-rect"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_COPY', '<i class="fa fa-cloud"></i>', 'br');
settext('$CODA_ACCOUNTS_TYPE_DROPBOX', '<i class="fa fa-dropbox"></i>', 'br');

settext('$CODA_ACCOUNTS_LIST_PANEL', 'USUÁRIOS', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_RESET_USER_PASSWORD', 'Resetar senha', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_MANAGE_USER_SCOPES', 'Gerir Scopes', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_MANAGE_USER_SCOPES_SUCCESS', 'Altera\u00e7\u00e3o de scopes do utilizador feita com sucesso', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE', 'Alterar Estado do Usuário', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK', 'Bloquear Usuário', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE', 'Ativar Usuário', 'br');
settext('$CODA_ACCOUNTS_LIST_RESETPASS_BUTTON', '<i class="fa fa-key"></i>', 'br');
settext('$CODA_ACCOUNTS_LIST_CHANGESTATE_TOBLOCK_BUTTON', '<i class="fa fa-ban"></i>', 'br');
settext('$CODA_ACCOUNTS_LIST_CHANGESTATE_TOACTIVE_BUTTON', '<i class="fa fa-ok"></i>', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1', 'O usuário est\u00e1 com o estado ATIVO, deseja bloque\u00e1-lo?', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2', 'O usuário est\u00e1 com o estado BLOQUEADO, deseja ativ\u00e1-lo?', 'br');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3', 'O usuário n\u00e3o tem estado associado. Deseja ativ\u00e1-lo?', 'br');

settext('$CODA_ACCOUNTS_PARTICIPATIONS_VIEW_ACTION', '<i class="fa fa-rocket"></i> Ver participações', 'br');
settext('$CODA_ACCOUNTS_PARTICIPATIONS_FILTER_ACTION', '<i class="fa fa-rocket"></i> Filtrar por este participante', 'br');

settext('$CODA_LIST_EXPORT_BUTTON_CODACAMPAIGNPARTICIPATIONSLIST', 'EXPORTAR PARTICIPAÇÕES', 'br');
settext('$CODA_LIST_PARTICIPATIONS_REPORT', 'Clique no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$PARTICIPATIONS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'br');

settext('$CODA_LIST_EXPORT_BUTTON_CODAACCOUNTSLIST', 'EXPORTAR PARTICIPANTES', 'br');
settext('$CODA_LIST_PARTICIPANTS_REPORT', 'Clique no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'br');
settext('$PARTICIPANTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_BUTTON_PARTICIPATIONS', 'Participações', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_DESC', 'Consultar e exportar a lista de participações na sua campanha. Escolhe os seus vencedores.', 'br');

settext('$CODA_ACCOUNTS_LIST', 'Participantes', 'br');
settext('$CODA_ACCOUNTS_LIST_ACTION_DESCRIPTION', 'Consultae e exportar a lista de participantes na sua campanha.', 'br');
settext('$CODA_ACCOUNTS_ADD', 'Novo usuário', 'br');
settext('$CODA_ACCOUNTS_ADD_ACTION_DESCRIPTION', 'Adicione um novo acesso ao seu aplicativo.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$CODA_ACCOUNTS_PASS_CHANGED_SUCCESS', 'Senha resetada com sucesso', 'br');
settext('$CODA_ACCOUNTS_STATE_CHANGED_SUCCESS', 'Estado do usuário alterado com sucesso', 'br');
settext('$CODA_EXCEPTION_DUPLICATE_ENTRY', 'O apelido e / ou email inseridos j\u00e1 existem para outro usuário', 'br');
settext('$CODA_EXCEPTION_USERS_NOT_FOUND', 'O usuário n\u00e3o foi encontrado', 'br');
settext('$CODA_DELETE_USER_CONFIRM', 'Tem certeza que deseja remover o usuário?', 'br');
settext('$CODA_USER_DELETED_SUCCESS', 'Usuário removido com sucesso.', 'br');
settext('$CODA_USER_DELETED_ERROR', 'Ocorreu um erro na remo\u00e7\u00e3o do usuário', 'br');
settext('The given user was not found', '<span style="color: rgb(248, 82, 1);">O usuário n\u00e3o foi encontrado<span>', 'br');

settext('$ERROR_USER_PASSWORD_MUST_MATCH', 'As senhas têm que coincidir.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$CODA_ACCOUNTS_MODULE_ERROR_0', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAAccountsLocale_pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$CODA_ACCOUNTS_ITEM_FORM_NAME_HELP', 'Nome completo do usuário', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_EMAIL_HELP', 'Endereço de correio eletrônico do usuário, que será utilizado para contato.', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID_HELP', 'Nome que ser+a utilizado como identificador textual para o usuário. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para acessar o sistema.', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_STATE_HELP', 'Estado', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_HELP', 'Palavra chave que será utilizada nas credenciais (nome, palavra-chave) necessárias para acessar o sistema. ', 'br');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_CONFIRM_HELP', 'Confirmação da palavra-chave introduzida no campo acima.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAAccountsLocale_Help_pt');

