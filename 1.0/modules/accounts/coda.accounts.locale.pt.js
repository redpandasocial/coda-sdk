///////////////////////////////////////////////////////////////////
// FORMS
settext('$CODA_ACCOUNTS_ITEM_FORM_NAME_HELP', 'Nome completo do utilizador', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_EMAIL_HELP', 'Morada de correio electrónico a ser associada ao utilizador, utilizada como ponto de contacto.', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID_HELP', 'Nome a ser utilizado como identificador textual para o utilizador. Este valor é utilizado nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema.', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_STATE_HELP', 'Estado', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_HELP', 'Palavra chave a ser utilizada nas credenciais (nome, palavra-chave) necessárias para aceder ao sistema. ', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_CONFIRM_HELP', 'Confirmar Confirmação da palavra-chave introduzida no campo acima.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAAccountsLocale_Help_pt');

settext('$CODA_ACCOUNTS_SHELL_TITLE', 'Participantes e Participações', 'pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$NEW_USER_PROPERTIES', 'NOVO UTILIZADOR', 'pt');
settext('$EDIT_USER_PROPERTIES', 'Utilizador ', 'pt');

settext('$CODA_ACCOUNTS_ITEM_FORM_NAME', 'Nome', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_EMAIL', 'Email', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID', 'Accountsname', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_STATE', 'Estado', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_ID_LABEL', 'Accountsname', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS', 'Password', 'pt');
settext('$CODA_ACCOUNTS_ITEM_FORM_PASS_CONFIRM', 'Confirmar Password', 'pt');

settext('id', 'Identificador', 'pt');
settext('first_name', 'Primeiro nome', 'pt');
settext('last_name', 'Apelido', 'pt');
settext('link', 'URL de Perfil', 'pt');
settext('username', 'Nick', 'pt');
settext('gender', 'Género', 'pt');
settext('timezone', 'Timezone', 'pt');
settext('locale', 'Idioma', 'pt');
settext('birthday', 'Data de Nascimento', 'pt');
settext('email_marketing', 'Aceitas comunicações?', 'pt');

settext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES', 'Participação', 'pt');
settext('$PARTICIPATION_ITEM_FORM_OWNER', 'Participante', 'pt');
settext('$PARTICIPATION_ITEM_FORM_START_DATE', 'Data de Início da Part.', 'pt');
settext('$PARTICIPATION_ITEM_FORM_START_DATE_HELP', '', 'pt');
settext('$PARTICIPATION_ITEM_FORM_END_DATE', 'Data de Fim da Part.', 'pt');
settext('$PARTICIPATION_ITEM_FORM_END_DATE_HELP', '', 'pt');
settext('$PARTICIPATION_ITEM_FORM_VALUE', 'Pontuação', 'pt');
settext('$CODA_PARTICIPATION_FIELD_GENERIC_NAME', 'Campo de Submissão n. ', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_QUESTION', '<span style="font-weight: bold">Pergunta:</span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWERS', '<span style="font-weight: bold">Resposta:</span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT', '<span style="font-weight: bold">Correcta?</span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT_TRUE', 'Sim', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT_FALSE', 'Não', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED', '<span style="font-weight: bold">Tempo limite atingido?</span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED_FALSE', 'Sim', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED_TRUE', 'Não', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIME', '<span style="font-weight: bold">Respondido em: </span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_POINTS', '<span style="font-weight: bold">Pontuação: </span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_FRIEND', '<span style="font-weight: bold">Amigo: </span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS', '<span style="font-weight: bold">Estado do convite: </span>', 'pt');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_ALREADY_REGISTERED', 'Já estava registado', 'pt');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_INVITED', 'À espera de confirmação', 'pt');
settext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_REGISTERED', 'Aceite', 'pt');

settext('$CODA_PARTICIPATION_STATUS_COMPLETED', 'Completa', 'pt');
settext('$CODA_PARTICIPATION_STATUS_ACTIVE', 'Activa', 'pt');
settext('$CODA_PARTICIPATION_NOT_FINISHED_YET', 'Por terminar', 'pt');

settext('$CODA_PARTICIPATION_MEDIA_FIELD_NAME', 'Media submetido n.', 'pt');
settext('$CODA_PARTICIPATION_FRIENDSDISCOUNT_FIELD_NAME', 'Amigo convidado n. ', 'pt');
settext('$CODA_PARTICIPATION_QUIZ_FIELD_NAME', 'Pergunta n. ', 'pt');

settext('$PARTICIPATION_WINNER_ITEM_FORM_RANKING', 'Posição', 'pt');
settext('$PARTICIPATION_WINNER_ITEM_PRIZE_SELECTOR', 'Prémio a atribuir', 'pt');
settext('$PARTICIPATION_WINNER_GENERATE_PRIZE', 'Atribuir prémio automaticamente?', 'pt');
settext('$PARTICIPATION_WINNER_PRIZE_GENERATED', 'Informação', 'pt');
settext('$PARTICIPATION_WINNER_PRIZE_SENDED', 'O prémio seleccionado já foi atribuído à participação', 'pt');
settext('$CHECKBOX_TRUE_OPTION', 'SIM', 'pt');

settext('$FORM_GENERALINFO_PANEL', 'INFO GERAL', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LIST
settext('$CAMPAIGNS_PHASE', 'Fase «$PHASE_NAME»', 'pt');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_ICON', '<i class="fa fa-user"></i>', 'pt');
settext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_TOOLTIP', 'Ver participante', 'pt');
settext('$PARTICIPATIONS_LIST_WINNER_ICON', '<i class="fa fa-trophy"></i>', 'pt');
settext('$PARTICIPATIONS_LIST_WINNER_TOOLTIP', 'Marcar como Vencedora', 'pt');
settext('$CAMPAIGNS_LIST_PARTICIPANT', '<i class="fa fa-user"></i> Participante', 'pt');
settext('$CAMPAIGNS_LIST_START_DATE', '<i class="fa fa-calendar"></i> Data Início da Part.', 'pt');
settext('$CAMPAIGNS_LIST_END_DATE', '<i class="fa fa-calendar"></i> Data Final da Part.', 'pt');
settext('$CAMPAIGNS_LIST_POINTS', '<i class="fa fa-calendar"></i> Pontuação', 'pt');
settext('$CAMPAIGNS_LIST_RANKING', '<i class="fa fa-calendar"></i> Posição', 'pt');
settext('$CAMPAIGNS_LIST_VALUE', '<i class="fa fa-pencil"></i> Submissão', 'pt');
settext('$LIST_CAMPAIGNPARTICIPATIONS_PROPERTIES', 'Participações', 'pt');

settext('$PRIZE_TYPE_VOUCHER_PDF_CODE', 'application/pdf; charset=binary', 'pt');
settext('$CODA_ACCOUNTS_URL', '<i class="fa fa-link"></i> Perfil', 'pt');

settext('$CODA_PARTICIPATION_FILTER_PHASE', 'filtrar por fase...', 'pt');

settext('$CODA_ACCOUNTS_NAME', 'Nome', 'pt');
settext('$CODA_ACCOUNTS_ID', 'Identificador', 'pt');
settext('$CODA_ACCOUNTS_LIST_TITLE', 'Participantes', 'pt');
settext('$CODA_ACCOUNTS_MUG', '<i class="fa fa-picture-o"></i>', 'pt');

settext('$CODA_ACCOUNTS_TYPE_INSTAGRAM', '<i class="fa fa-instagram"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_FACEBOOK', '<i class="fa fa-facebook-square"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_FLICKR', '<i class="fa fa-flickr"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_GOOGLE', '<i class="fa fa-google-plus-square"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_GOOGLE+', '<i class="fa fa-google-plus-square"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_LINKEDIN', '<i class="fa fa-linkedin-square"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_TUMBLR', '<i class="fa fa-tumblr-square"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_TWITTER', '<i class="fa fa-twitter"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_YAHOO', '<i class="rp rp-yahoo"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_BOX', '<i class="rp rp-box-rect"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_COPY', '<i class="fa fa-cloud"></i>', 'pt');
settext('$CODA_ACCOUNTS_TYPE_DROPBOX', '<i class="fa fa-dropbox"></i>', 'pt');

settext('$CODA_ACCOUNTS_LIST_PANEL', 'UTILIZADORES', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_RESET_USER_PASSWORD', 'Reset Password', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_MANAGE_USER_SCOPES', 'Gerir Scopes', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_MANAGE_USER_SCOPES_SUCCESS', 'Altera\u00e7\u00e3o de scopes do utilizador feita com sucesso', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE', 'Alterar Estado do Utilizador', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_TO_BLOCK', 'Bloquear Utilizador', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_TO_ACTIVE', 'Activar Utilizador', 'pt');
settext('$CODA_ACCOUNTS_LIST_RESETPASS_BUTTON', '<i class="fa fa-key"></i>', 'pt');
settext('$CODA_ACCOUNTS_LIST_CHANGESTATE_TOBLOCK_BUTTON', '<i class="fa fa-ban"></i>', 'pt');
settext('$CODA_ACCOUNTS_LIST_CHANGESTATE_TOACTIVE_BUTTON', '<i class="fa fa-ok"></i>', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE1', 'O utilizador est\u00e1 com o estado ACTIVO, desejas bloque\u00e1-lo?', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE2', 'O utilizador est\u00e1 com o estado BLOQUEADO, desejas activ\u00e1-lo?', 'pt');
settext('$CODA_ACCOUNTS_LIST_USER_CHANGE_USER_STATE_CONFIRM_STATE3', 'O utilizador n\u00e3o tem estado associado. Desejas activ\u00e1-lo?', 'pt');

settext('$CODA_ACCOUNTS_PARTICIPATIONS_VIEW_ACTION', '<i class="fa fa-rocket"></i> Ver participações', 'pt');
settext('$CODA_ACCOUNTS_PARTICIPATIONS_FILTER_ACTION', '<i class="fa fa-rocket"></i> Filtrar por este participante', 'pt');

settext('$CODA_LIST_EXPORT_BUTTON_CODACAMPAIGNPARTICIPATIONSLIST', 'EXPORTAR PARTICIPAÇÕES', 'pt');
settext('$CODA_LIST_PARTICIPATIONS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$PARTICIPATIONS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'pt');

settext('$CODA_LIST_EXPORT_BUTTON_CODAACCOUNTSLIST', 'EXPORTAR PARTICIPANTES', 'pt');
settext('$CODA_LIST_PARTICIPANTS_REPORT', 'Clica no link abaixo para descarregar:<br><span class="CODAExportFile CODAExportDownload">$FILE_NAME</span><br>', 'pt');
settext('$PARTICIPANTS_MODULE_ERROR_REPORT_NO_CONTENT', 'N\u00e3o h\u00e1 dados a exportar', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_BUTTON_PARTICIPATIONS', 'Participações', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_DESC', 'Consulta e exporta a lista de partcipações na tua campanha. Escolhe os teus vencedores.', 'pt');

settext('$CODA_ACCOUNTS_LIST', 'Participantes', 'pt');
settext('$CODA_ACCOUNTS_LIST_ACTION_DESCRIPTION', 'Consulta e exporta a lista de participantes na sua campanha.', 'pt');
settext('$CODA_ACCOUNTS_ADD', 'Novo utilizador', 'pt');
settext('$CODA_ACCOUNTS_ADD_ACTION_DESCRIPTION', 'Adiciona um novo acesso à tua aplicação.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$CODA_ACCOUNTS_PASS_CHANGED_SUCCESS', 'Reset \u00e0 password feito com sucesso', 'pt');
settext('$CODA_ACCOUNTS_STATE_CHANGED_SUCCESS', 'Estado do utilizador alterado com sucesso', 'pt');
settext('$CODA_EXCEPTION_DUPLICATE_ENTRY', 'O username e / ou email inseridos j\u00e1 existem para outro utilizador', 'pt');
settext('$CODA_EXCEPTION_USERS_NOT_FOUND', 'O Utilizador n\u00e3o foi encontrado', 'pt');
settext('$CODA_DELETE_USER_CONFIRM', 'Tens a certeza que queres remover o utilizador?', 'pt');
settext('$CODA_USER_DELETED_SUCCESS', 'Utilizador removido com sucesso.', 'pt');
settext('$CODA_USER_DELETED_ERROR', 'Ocorreu um erro na remo\u00e7\u00e3o do utilizador', 'pt');
settext('The given user was not found', '<span style="color: rgb(248, 82, 1);">O utilizador n\u00e3o foi encontrado<span>', 'pt');

settext('$ERROR_USER_PASSWORD_MUST_MATCH', 'As passwords têm que coincidir.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$CODA_ACCOUNTS_MODULE_ERROR_0', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAAccountsLocale_pt');

