KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAAccountsDashboard(parent) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, [{
			icon : 'css:fa fa-group',
			activate : false,
			title : gettext('$CODA_ACCOUNTS_LIST'),
			description : '<span>' + gettext('$CODA_ACCOUNTS_LIST_ACTION_DESCRIPTION') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-accounts'
			}]
		}, 
		{
			icon : 'css:fa fa-rocket',
			activate : false,
			title : gettext('$DASHBOARD_BUTTON_PARTICIPATIONS'),
			description : '<span>' + gettext('$DASHBOARD_BUTTON_PARTICIPATIONS_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/list-participations'
			}]
		}
		]);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODAAccountsDashboard.prototype = new CODAMenuDashboard();
	CODAAccountsDashboard.prototype.constructor = CODAAccountsDashboard;

	CODAAccountsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAAccountsDashboard');
}, Kinky.CODA_INCLUDER_URL);

function CODAAccountsItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = false;
		this.target.collection = '/users';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KPassword'
], function() {
	CODAAccountsItemForm.prototype = new CODAForm();
	CODAAccountsItemForm.prototype.constructor = CODAAccountsItemForm;

	CODAAccountsItemForm.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"/wrml-schemas/accounts/accounts-document\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAAccountsItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();

	};

	CODAAccountsItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_USER_PROPERTIES') + ' \u00ab' + this.target.data.profile.name + '\u00bb');
		}

		var userItem = new KPanel(this);
		{
			userItem.hash = '/general';

			var mug = new KImage(userItem, this.data.profile.mug)
			userItem.appendChild(mug);

			var profile = new KText(userItem);
			profile.setText('<a href="' + this.data.profile.profile_url + '" target="_blank">' + this.data.profile.profile_url + '</a>');
			userItem.appendChild(profile);

			var name = new KStatic(userItem, gettext('$CODA_ACCOUNTS_ITEM_FORM_NAME'), 'profile.name');
			{
				if (!!this.data && !!this.data && !!this.data.profile.name) {
					name.setValue(this.data.profile.name);
				}
			}
			userItem.appendChild(name);

			for (var a in this.data.profile) {
				if (a == 'name' || a == 'mug' || a == 'client_id' || a == 'profile_url') {
					continue;
				}
				if (typeof this.data.profile[a] == 'object' && !!this.data.profile[a]._content) {
					var field = new KStatic(userItem, gettext(a), 'profile.' + a);
					{
						field.setValue(this.data.profile[a]._content);
					}
					userItem.appendChild(field);
				}
				else if (typeof this.data.profile[a] == 'object') {
					continue;
				}
				else {
					var field = new KStatic(userItem, gettext(a), 'profile.' + a);
					{
						field.setValue(this.data.profile[a]);
					}
					userItem.appendChild(field);
				}
			}
		}

		this.addPanel(userItem, gettext('$FORM_GENERAL_DATA_PANEL'), true);
		this.hideConfirm();
	};

	CODAAccountsItemForm.prototype.setDefaults = function(params){
		delete params.data;
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;
		delete params.scopes;

		if (!params.password || params.password == '') {
			delete params.password;
			delete params.confpassword;
		}
	};

	CODAAccountsItemForm.prototype.onBadRequest = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAAccountsItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.showMessage(gettext(data.message));
	};

	CODAAccountsItemForm.prototype.onNotFound = function(data, request) {
		this.showMessage(gettext('$CODA_EXCEPTION_USERS_NOT_FOUND'));
	};

	CODAAccountsItemForm.prototype.onPost = CODAAccountsItemForm.prototype.onCreated = function(data, request){
		this.getShell().dashboard.usersList.list.refreshPages();
		this.parent.closeMe();
		KBreadcrumb.dispatchURL({
			hash : '/edit' + data.http.headers.Location
		});
	};

	CODAAccountsItemForm.prototype.onPut = function(data, request) {
		if (this.scopes_p !== undefined) {
			var scopes = { scopes : this.scopes_p.childWidget('/scopes').getValue() };
			this.kinky.put(this, 'rest:' + this.target.href + '/scopes', scopes, null, 'onPutScopes');
		}
		else {
			this.enable();
			this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		}
	};

	CODAAccountsItemForm.prototype.onPutScopes = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAAccountsItemForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAAccountsItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAAccountsList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, {
			delete_button : false,
			edit_button : false,
			export_button : true
		});
		this.onechoice = false;
		this.pageSize = 5;
		this.table = {
			"type" : {
				label : '<i class="fa fa-globe"></i>',
				fixed : 20,
				align : 'center'
			},
			"profile.mug" : {
				label : gettext('$CODA_ACCOUNTS_MUG'),
				fixed : 20,
				align : 'center'
			},
			"profile.name" : {
				label : gettext('$CODA_ACCOUNTS_NAME'),
				orderBy : true
			},
			"profile.profile_url" : {
				label : gettext('$CODA_ACCOUNTS_URL')
			}
		};
		this.filterFields = [ 'profile.name' ];
		this.LIST_MARGIN = 50;
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODAAccountsList.prototype = new CODAList();
	CODAAccountsList.prototype.constructor = CODAAccountsList;

	CODAAccountsList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODAAccountsList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAListItem(list);
			{
				listItem.index = index;
				listItem.data = this.elements[index];
				listItem.data.profile.mug = listItem.data.profile.mug || 'css:fa fa-user';
				listItem.data.profile.mug = (listItem.data.profile.mug.indexOf('css:') == 0 ? '<i class="' + listItem.data.profile.mug.replace(/css:/gi, '') + '"></i>' :  '<img src="' + listItem.data.profile.mug + '"></img>');
				listItem.data.profile.profile_url = '<a href="' + listItem.data.profile.profile_url + '" target="_blank">' + listItem.data.profile.profile_url  + '</a>';
				listItem.data.href = listItem.data._id;
				listItem.data.type = gettext('$CODA_ACCOUNTS_TYPE_' + listItem.data.type.toUpperCase());
				listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
				listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);

				var detailsBt = new KButton(listItem, ' ', 'details', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAListItem');
					KBreadcrumb.dispatchEvent(null, {
						hash : '/view-account' + widget.data.href
					});
				});
				{
					detailsBt.hasClickOutListener = true;
					detailsBt.setText(gettext('$KLISTITEM_VIEW_ACTION'), true);
				}
				listItem.appendChild(detailsBt);

				var participationsBt = new KButton(listItem, ' ', 'participations', function(event) {
					KDOM.stopEvent(event);
					var widget = KDOM.getEventWidget(event, 'CODAListItem');
					KBreadcrumb.dispatchURL({
						hash : '/list-participations',
						query : widget.data.href.replace('/accounts/', '')
					});
				});
				{
					participationsBt.hasClickOutListener = true;
					participationsBt.setText(gettext('$CODA_ACCOUNTS_PARTICIPATIONS_VIEW_ACTION'), true);
				}
				listItem.appendChild(participationsBt);
			}
			list.appendChild(listItem);
		}
	};

	CODAAccountsList.prototype.exportList = function() {
		KBreadcrumb.dispatchEvent(null, {
			hash : '/generate-participants-report'
		});
	};

	KSystem.included('CODAAccountsList');
}, Kinky.CODA_INCLUDER_URL);

function CODAAccountsShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addCSSClass('CODAShell');
		this.addCSSClass('CODAApplicationsShell');
		this.addLocationListener(CODAAccountsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODAAccountsDashboard',
	'CODAAccountsList',
	'CODAAccountsItemForm',
	'CODACampaignParticipationsListItem',
	'CODACampaignParticipationsList',
	'CODACampaignParticipationItemForm',
	'CODACampaignParticipationWinnerForm'
], function() {
	CODAAccountsShell.prototype = new CODAGenericShell();
	CODAAccountsShell.prototype.constructor = CODAAccountsShell;

	CODAAccountsShell.prototype.loadShell = function() {
		this.kinky.get(this, 'rest:/campaigns?embed=elements(*(phases))', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadCampaigns');
	};

	CODAAccountsShell.prototype.onLoadCampaigns = function(data, request) {
		this.campaign = data.elements[0];
		this.draw();
	};

	CODAAccountsShell.prototype.draw = function() {
		this.dashboard = new CODAAccountsDashboard(this);
		this.appendChild(this.dashboard);

		this.setTitle(gettext('$CODA_ACCOUNTS_SHELL_TITLE'));

		CODAGenericShell.prototype.draw.call(this);

		this.addCSSClass('CODAApplicationsShellContent', this.content);
	};

	CODAAccountsShell.sendRequest = function() {
	};

	CODAAccountsShell.prototype.listAccounts = function(href, minimized) {
		var users_list = new CODAAccountsList(this, {
			links : {
				feed : { href :  '/accounts' }
			}
		});
		users_list.setTitle(gettext('$CODA_ACCOUNTS_LIST_TITLE'));
		this.makeDialog(users_list, { minimized : false, modal : true });
	};

	CODAAccountsShell.prototype.editAccounts = function(href) {
		var configForm = new CODAAccountsItemForm(this, href, null, null);
		{
			configForm.hash = '/accounts/advanced';
			configForm.setTitle(gettext('$EDIT_USER_PROPERTIES'));
		}
		var dialog = this.makeDialog(configForm, { minimized : true, modal : false });
	};


	CODAAccountsShell.prototype.listCampaignParticipations = function(href, minimized) {
		CODAAccountsShell.participations_list = new CODACampaignParticipationsList(this, {
			links : {
				feed : { href :  '/participations?fields=elements,size&embed=elements(*(participationcontents[1,0],owner))' }
			}
		}, this.campaign.phases);
		if (!!KBreadcrumb.getQuery() && KBreadcrumb.getQuery() != '') {
			CODAAccountsShell.participations_list.filterValue = KBreadcrumb.getQuery();
		}
		CODAAccountsShell.participations_list.setTitle(gettext('$LIST_CAMPAIGNPARTICIPATIONS_PROPERTIES'));
		this.makeDialog(CODAAccountsShell.participations_list, { minimized : false, modal : true });
	};

	CODAAccountsShell.prototype.editCampaignParticipation = function(href, minimized) {
		var configForm = new CODACampaignParticipationItemForm(this, href, null, null);
		{
			configForm.hash = '/campaignparticipation/advanced';
			configForm.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES'));
		}
		this.makeDialog(configForm, { minimized : minimized });
	};

	CODAAccountsShell.prototype.retrieveParticipationWinner = function(participation_id) {
		this.kinky.get(this, 'rest:/participationwinners?participation_id=' + participation_id, null, Kinky.SHELL_CONFIG[this.shell].headers, 'onParticipationWinnerRetrieved');
	};

	CODAAccountsShell.prototype.onParticipationWinnerRetrieved = function(data, request) {
		if(!!data && !!data.elements){
			var configForm = new CODACampaignParticipationWinnerForm(this, '/participationwinners/' + data.elements[0].id, null, null);
			{
				configForm.hash = '/campaignparticipationwinner/advanced';
				configForm.participation_id = data.elements[0].participation_id;
				configForm.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES'));
			}
			this.makeDialog(configForm, { minimized : true });
		}		
	};

	CODAAccountsShell.prototype.retrieveParticipationData = function(participation_id) {
		if (participation_id) {
				this.kinky.get(this, 'rest:/participations/' + participation_id + '?embed=owner', {}, { 'E-Tag' : (new Date()).getTime()}, 'onParticipationDataRetrieved' );
		}else {
				this.addParticipationWinnerWizard();
		}
	};

	CODAAccountsShell.prototype.onParticipationDataRetrieved = function(data, request) {
		this.data.participation = data;
		this.data.participation_id = data.id;
		if(!!data.campaign_id){
			this.data.campaign_id = data.campaign_id;	
		}
		if(!!data.phase_id){
			this.data.phase_id = data.phase_id;
		}		
		this.kinky.get(this, 'rest:/campaigns/' + this.data.participation.campaign_id + '/phases/' + this.data.participation.phase_id + '/prizes', {}, { 'E-Tag' : (new Date()).getTime()}, 'onPrizesRetrieved');
	};

	CODAAccountsShell.prototype.onPrizesRetrieved = function(data, request) {
		this.data.prizes = data;
		this.addParticipationWinnerWizard();
	};

	CODAAccountsShell.prototype.addParticipationWinnerWizard = function() {
		var screens = [ 'CODACampaignParticipationWinnerForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.data = {};
		if(!!this.data.participation){
			wiz.data.participation = this.data.participation;
			delete this.data.participation;	
		}
		if(!!this.data.campaign_id){
			wiz.data.campaign_id = this.data.campaign_id;
			delete this.data.campaign_id;	
		}
		if(!!this.data.phase_id){
			wiz.data.phase_id = this.data.phase_id;
			delete this.data.phase_id;	
		}
		if(!!this.data.prizes){
			wiz.data.prizes = this.data.prizes;
			delete this.data.prizes;	
		}		
		wiz.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES') + ' \u00ab' + wiz.data.participation.id + '\u00bb');
		wiz.hash = '/participationwinners';
		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			CODAAccountsShell.participations_list.list.refreshPages();
			this.parent.closeMe();
		};

		wiz.send = function(params) {
			params.campaign_id = this.data.campaign_id;
			params.phase_id = this.data.phase_id;
			params.participation_id = this.data.participation.id;

			if (!!params.owner){
				delete params.owner;
			}

			if (!!params.start_date){
				delete params.start_date;
			}

			if (!!params.end_date){
				delete params.end_date;
			}

			if (!!params.value){
				delete params.value;
			}

			if(params.prize_id == ""){
				delete params.prize_id;
			}
			
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + wiz.hash, params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODAAccountsShell.prototype.generateParticipationsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/participations', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onParticipationsReportGenerated');
	};

	CODAAccountsShell.prototype.onParticipationsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_PARTICIPATIONS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAAccountsShell.prototype.generateParticipantsReport = function() {
		this.kinky.post(this, 'rest:/generatereport/participants', null, Kinky.SHELL_CONFIG[this.shell].headers, 'onParticipantsReportGenerated');
	};

	CODAAccountsShell.prototype.onParticipantsReportGenerated = function(data, request) {
		KMessageDialog.alert({
			html : true,
			message : '<a target="_blank" class="CODAExportMessage" href=' + data.file + '>' + gettext('$CODA_LIST_PARTICIPANTS_REPORT').replace('$FILE_NAME', data.file.split('/').pop()) + '</a>',
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAAccountsShell.prototype.onError = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CODA_ACCOUNTS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAAccountsShell.prototype.onNoContent = function(data, request) {
		if(request.service == '/generatereport/participations'){
			KMessageDialog.alert({
				message : gettext('$PARTICIPATIONS_MODULE_ERROR_REPORT_NO_CONTENT'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}else if(request.service == '/generatereport/participants'){
			KMessageDialog.alert({
				message : gettext('$PARTICIPANTS_MODULE_ERROR_REPORT_NO_CONTENT'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}else if(request.service.indexOf('/participationwinners') != -1){
//			var configForm = new CODACampaignParticipationWinnerForm(this, null, null, null);
//			{
//				configForm.hash = '/campaignparticipationwinner/advanced';				
//				configForm.participation_id = request.service.substr(39);
//				configForm.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES'));
//			}
//			this.makeDialog(configForm, { minimized : true });
			this.retrieveParticipationData(request.service.substr(39));
		}
	};

	CODAAccountsShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]) {
			case 'list-accounts': {
				Kinky.getDefaultShell().listAccounts();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'view-account': {
				Kinky.getDefaultShell().editAccounts('/' + parts[2] + '/' + parts[3], false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'list-participations': {
				Kinky.getDefaultShell().listCampaignParticipations('/participations', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'view-participation': {
				Kinky.getDefaultShell().editCampaignParticipation('/' + parts[2] + '/' + parts[3], true);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'register-winner': {
				Kinky.getDefaultShell().retrieveParticipationWinner(parts[3]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-participations-report': {
				Kinky.getDefaultShell().generateParticipationsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'generate-participants-report': {
				Kinky.getDefaultShell().generateParticipantsReport();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	KSystem.included('CODAAccountsShell');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignParticipationItemForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href + '?embed=participationcontents[25-0]{+status},owner', layout, parentHref);
		this.config = this.config || {};
		this.config.headers = {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = '/participations';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText'
], function() {
	CODACampaignParticipationItemForm.prototype = new CODAForm();
	CODACampaignParticipationItemForm.prototype.constructor = CODACampaignParticipationItemForm;

	CODACampaignParticipationItemForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"/wrml-schemas/participations/participations-schema\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACampaignParticipationItemForm.prototype.loadForm = function() {
		if (!this.data.participationcontents) {
			this.kinky.get(this, 'rest:/formfillings?owner_id=' + this.data._id, {}, { 'E-Tag' : (new Date()).getTime() }, 'onLoadForm');
		}
		else {
			this.draw();
		}
	};

	CODACampaignParticipationItemForm.prototype.onLoadForm = function(data, request) {
		this.data.form = data;
		this.draw();
	};

	CODACampaignParticipationItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODACampaignParticipationItemForm.prototype.addMinimizedPanel = function() {
		if (!!this.target.href) {
			this.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES') + ' \u00ab' + this.target.data.id + '\u00bb');
		}

		var participation_item = new KPanel(this);
		{
			participation_item.hash = '/general';

			var owner = new KStatic(participation_item, gettext('$PARTICIPATION_ITEM_FORM_OWNER'), 'owner.profile.name');
			{
				if (!!this.data && !!this.data.owner.profile.name) {
					owner.setValue(this.data.owner.profile.name);
				}
			}
			participation_item.appendChild(owner);

			var start_date = new KStatic(participation_item, gettext('$PARTICIPATION_ITEM_FORM_START_DATE'), 'start_date');
			{
				if(!!this.data && !!this.data.start_date){
					start_date.setValue(this.data.start_date);
				}
			}
			start_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
			participation_item.appendChild(start_date);

			var end_date = new KStatic(participation_item, gettext('$PARTICIPATION_ITEM_FORM_END_DATE'), 'end_date');
			{
				if(!!this.data && !!this.data.end_date){
					end_date.setValue(this.data.end_date);
				}
				else {
					end_date.setValue(gettext('$CODA_PARTICIPATION_NOT_FINISHED_YET'));
				}
			}
			end_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
			participation_item.appendChild(end_date);

			var value = new KStatic(participation_item, gettext('$PARTICIPATION_ITEM_FORM_VALUE'), 'value');
			{
				value.setValue(CODACampaignParticipationItemForm.getPoints(this.data));
			}
			participation_item.appendChild(value);


		}

		CODACampaignParticipationItemForm.getSubmission(this.data, participation_item);

		this.addPanel(participation_item, gettext('$FORM_GENERALINFO_PANEL'), true);
		this.hideConfirm();
	};

	CODACampaignParticipationItemForm.prototype.setDefaults = function(params){
		if(!!params.requestTypes){
			delete params.requestTypes;
		}

		if(!!params.responseTypes){
			delete params.responseTypes;
		}
	};

	CODACampaignParticipationItemForm.prototype.onBadRequest = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationItemForm.prototype.onPreConditionFailed = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationItemForm.prototype.onNotFound = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationItemForm.getPoints = function(data) {
		if (!!data.rewards && !!data.rewards.reward_votes) {
			return data.rewards.reward_votes;
		}
		else if (!!data.rewards && !!data.rewards.reward_quiz_points) {
			return data.rewards.reward_quiz_points;
		}
		else if (!!data.rewards && !!data.rewards.reward_friends) {
			return data.rewards.reward_friends;
		}
		return '-';
	};

	CODACampaignParticipationItemForm.getSubmission = function(data, panel) {
		if (!!data.participationcontents) {
			for (var p = 0; p != data.participationcontents.elements.length; p++) {
				var content = data.participationcontents.elements[p];
				var mimefamily = content.mime_type.split('/')[0];

				var field = new KStatic(panel, gettext('$CODA_PARTICIPATION_FIELD_GENERIC_NAME') + ' ' + (p + 1), 'participationcontents[' + p + '].binary_url');
				panel.appendChild(field);

				switch(mimefamily) {
					case 'image' : {
						field.setLabel(gettext('$CODA_PARTICIPATION_MEDIA_FIELD_NAME') + ' ' + (p + 1));
						field.setValue('<img style="max-width: 350px;" src="' + content.binary_url + '"></img>');
						break;
					}
					case 'text' : {
						if (!!content.type) {
							var v = gettext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_FRIEND') + ' <a href="https://www.facebook.com/' + content.binary_url + '" target="_blank">' + content.binary_url + '</a><br/>';
							v += gettext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS') + gettext('$CODA_PARTICIPATION_FIELD_FRIENDSDISCOUNT_STATUS_' + content.type.toUpperCase());

							field.setLabel(gettext('$CODA_PARTICIPATION_FRIENDSDISCOUNT_FIELD_NAME') + ' ' + (p + 1));
							field.setValue(v);
						}
						else {
							try {
								var obj = JSON.parse(content.binary_url);
								if (!!obj && typeof obj == 'object') {
									if (!!obj.question) {
										var v = gettext('$CODA_PARTICIPATION_FIELD_QUIZ_QUESTION') + ' ' + obj.question.text + '<br/>';
										if (obj.answer_result.answer_in_time) {
											v += gettext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWERS') + '<br/>';
											for (var r = 0; r != obj.answers.length; r++) {
												v += '- ' + (!!obj.answers[r].text ? obj.answers[r].text : (!!obj.answers[r].url ? obj.answers[r].url : '-')) + '<br/>';
											}
											v += gettext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT') + ' ' + gettext(('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_CORRECT_' + obj.answer_result.answer_correct).toUpperCase()) + '<br>';
										}
										else {
											v += gettext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED') + ' ' + gettext(('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIMELIMIT_REACHED_' + obj.answer_result.answer_in_time).toUpperCase()) + '<br>';
										}
										v += gettext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_TIME') + ' ' + obj.answer_result.answer_time + 's<br>';
										v += gettext('$CODA_PARTICIPATION_FIELD_QUIZ_ANSWER_POINTS') + ' ' + content.rewards.reward_points;

										field.setLabel(gettext('$CODA_PARTICIPATION_QUIZ_FIELD_NAME') + ' ' + (p + 1));
										field.setValue(v);
									}
									else {
										field.setValue(content.binary_url);
									}
								}
								else {
									field.setValue(content.binary_url);
								}
							}
							catch(e) {
								field.setValue(content.binary_url || '-');
							}
						}
						break;
					}
				}
			}
		}
		else if (!!data.form) {
			for (var p = 0; p != data.form.elements.length; p++) {
				var content = data.form.elements[p];

				for (var a in content) {
					if (a == 'owner_id' || a == 'target_id' || a == 'links' || a == 'client_id' || a == '_id' || a == 'restServer' || a == 'href' || a == 'rel' || a == 'hash') {
						continue;
					}
					var field = new KStatic(panel, gettext(a), 'form.elements[' + p + '].' + a);
					field.setValue(content[a] != '' ? content[a] : '-');
					panel.appendChild(field);
				}
			}

		}
	};
	KSystem.included('CODACampaignParticipationItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODACampaignParticipationsListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
		this.config = {};
		this.config.headers = {};
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox',
	'KStatic'
], function() {
	CODACampaignParticipationsListItem.prototype = new CODAListItem();
	CODACampaignParticipationsListItem.prototype.constructor = CODACampaignParticipationsListItem;

	CODACampaignParticipationsListItem.prototype.remove = function() {
		var widget = this;

		KConfirmDialog.confirm({
			question : gettext('$DELETE_PARTICIPATION_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onParticipationDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODACampaignParticipationsListItem.prototype.onParticipationDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	CODACampaignParticipationsListItem.prototype.draw = function() {

		var detailsBt = new KButton(this, ' ', 'details', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAListItem');
			KBreadcrumb.dispatchEvent(null, {
				hash : '/view-participation' + widget.data.href
			});
		});
		{
			detailsBt.hasClickOutListener = true;
			detailsBt.setText(gettext('$KLISTITEM_VIEW_ACTION'), true);
		}
		this.appendChild(detailsBt);		

		var view_participantBt = new KButton(this, ' ', 'viewParticipant', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODACampaignParticipationsListItem');
			KBreadcrumb.dispatchURL({
				hash : '/view-account' + widget.data.owner_id
			});
		});
		{
			view_participantBt.setText(gettext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_ICON') + ' ' + (gettext('$PARTICIPATIONS_LIST_VIEW_PARTICIPANT_TOOLTIP')), true);
		}
		this.appendChild(view_participantBt);

		var register_winnerBt = new KButton(this, ' ', 'registerWinner', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAListItem');
			KBreadcrumb.dispatchURL({
				hash : '/register-winner' + widget.data.href
			});
		});
		{
			register_winnerBt.setText(gettext('$PARTICIPATIONS_LIST_WINNER_ICON') + ' ' + (gettext('$PARTICIPATIONS_LIST_WINNER_TOOLTIP')), true);
		}
		this.appendChild(register_winnerBt);

		CODAListItem.prototype.draw.call(this);

		var participationsBt = new KButton(this, ' ', 'participations', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAListItem');
			widget.parent.parent.execFilter(widget.data.owner_id.replace('/accounts/', ''), null, 0);
		});
		{
			participationsBt.hasClickOutListener = true;
			participationsBt.setText(gettext('$CODA_ACCOUNTS_PARTICIPATIONS_FILTER_ACTION'), true);
		}
		this.appendChild(participationsBt);


	};

	KSystem.included('CODACampaignParticipationsListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignParticipationsList(parent, data, phases) {
	if (parent != null) {
		CODAList.call(this, parent, data, null, {
			export_button : true,
			edit_button : false,
			filter_input : true
		});
		this.onechoice = false;
		this.pageSize = 5;
		this.phases = phases;
		this.table = {
				"owner.profile.name" : {
					label : gettext('$CAMPAIGNS_LIST_PARTICIPANT')
				},
				"start_date" : {
					label : gettext('$CAMPAIGNS_LIST_START_DATE'),
					align : 'center',
					orderBy : true
				},
				"end_date" : {
					label : gettext('$CAMPAIGNS_LIST_END_DATE'),
					align : 'center',
					orderBy : true
				},
				"points" : {
					label : gettext('$CAMPAIGNS_LIST_POINTS'),
					align : 'center',
					orderBy : true
				},
				"ranking" : {
					label : gettext('$CAMPAIGNS_LIST_RANKING'),
					align : 'center',
					orderBy : true
				},
				'submission' : {
					label : gettext('$CAMPAIGNS_LIST_VALUE')
				}
			};
		this.filterFields = [ 'owner_id', 'phase_id' ];
		this.LIST_MARGIN = 50;
		this.orderBy = '-end_date';
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODACampaignParticipationsList.prototype = new CODAList();
	CODACampaignParticipationsList.prototype.constructor = CODACampaignParticipationsList;

	CODACampaignParticipationsList.prototype.visible = function() {
		CODAList.prototype.visible.call(this);
		KDOM.fireEvent(this.getShell().dashboard.childAt(1).h3, 'click');
	};

	CODACampaignParticipationsList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODACampaignParticipationsList.prototype.orderResults = function(orderBy) {
		orderBy = orderBy.replace('points', this.getPointsField());
		CODAList.prototype.orderResults.call(this, orderBy);
	};

	CODACampaignParticipationsList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			if(!this.elements[index].hash && !!this.elements[index].participation_id){
				this.elements[index].hash = '/participations/' + this.elements[index].participation_id;
			}
			if(!this.elements[index].href && !!this.elements[index].participation_id){
				this.elements[index].href = '/participations/' + this.elements[index].participation_id;
			}
			if(!this.elements[index]._id && !!this.elements[index].participation_id){
				this.elements[index]._id = '/participations/' + this.elements[index].participation_id;
			}
			if(!!this.elements[index].participation_start_date){
				if(this.elements[index].participation_start_date.search("WEST") != -1){
					this.elements[index].participation_start_date = KSystem.formatDate('Y-M-d H:i:s', new Date(this.elements[index].participation_start_date.substring(0,this.elements[index].participation_start_date.length - 5)));
				}else{
					this.elements[index].participation_start_date = KSystem.formatDate('Y-M-d H:i:s', new Date(this.elements[index].participation_start_date));
				}

			}
			if(!!this.elements[index].participation_end_date){
				if(this.elements[index].participation_end_date.search("WEST") != -1){
					this.elements[index].participation_end_date = KSystem.formatDate('Y-M-d H:i:s', new Date(this.elements[index].participation_end_date.substring(0,this.elements[index].participation_end_date.length - 5)));
				}else{
					this.elements[index].participation_end_date = KSystem.formatDate('Y-M-d H:i:s', new Date(this.elements[index].participation_end_date));
				}
			}
			var listItem = new CODACampaignParticipationsListItem(list);
			{
				listItem.index = index;
				listItem.data = this.elements[index];
				listItem.data.end_date = listItem.data.end_date || gettext('$CODA_PARTICIPATION_NOT_FINISHED_YET');
				listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
				listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
				listItem.data.points = this.getPoints(this.elements[index]);
				listItem.data.submission = CODACampaignParticipationsList.getSubmission(this.elements[index]);

			}
			list.appendChild(listItem);
		}
	};

	CODACampaignParticipationsList.prototype.addActions = function() {
		var self = this;
		this.addFilter( function(parent) {
			var filterInput = new KCombo(parent, '', 'pages-main-search');
			{
				filterInput.setInnerLabel(gettext('$CODA_PARTICIPATION_FILTER_PHASE'))
				for (var p in self.phases.elements) {
					filterInput.addOption(self.phases.elements[p].id, gettext('$CAMPAIGNS_PHASE').replace('$PHASE_NAME', self.phases.elements[p].name), self.getShell().active_phase_href == self.phases.elements[p].href);
				}
				filterInput.addEventListener('change', function(event) {
					self.list.parent.filterResults(self.list, filterInput.getValue(), filterInput.index);
					parent.onSelection(filterInput.getValue(), filterInput.getValueLabel());
				});
			}
			return filterInput;
		});

	};

	CODACampaignParticipationsList.prototype.getPointsField = function() {
		if (!!this.pointsField) {
			return this.pointsField;
		}
		return 'value';
	};

	CODACampaignParticipationsList.prototype.exportList = function() {
		KBreadcrumb.dispatchEvent(null, {
			hash : '/generate-participations-report'
		});
	};

	CODACampaignParticipationsList.prototype.getPoints = function(data) {
		if (!!data.rewards && !!data.rewards.reward_votes) {
			this.pointsField = 'rewards.reward_votes';
			return data.rewards.reward_votes;
		}
		else if (!!data.rewards && !!data.rewards.reward_quiz_points) {
			this.pointsField = 'rewards.reward_quiz_points';
			return data.rewards.reward_quiz_points;
		}
		else if (!!data.rewards && !!data.rewards.reward_friends) {
			this.pointsField = 'rewards.reward_friends';
			return data.rewards.reward_friends;
		}
		return '-';
	};


	CODACampaignParticipationsList.getSubmission = function(data) {
		if (!!data.rewards && !!data.rewards.reward_quiz_time) {
			return 'em ' + data.rewards.reward_quiz_time + ' segundos';
		}
		else if (!!data.participationcontents && !!data.participationcontents.elements && data.participationcontents.elements.length != 0 && data.participationcontents.elements[0].mime_type.split('/')[0] == 'image') {
			return '<img src="' + data.participationcontents.elements[0].binary_url + '"></img>';
		}
		return gettext('$CODA_PARTICIPATION_STATUS_' + data.status.toUpperCase());
	};

	KSystem.included('CODACampaignParticipationsList');
}, Kinky.CODA_INCLUDER_URL);

function CODACampaignParticipationWinnerForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		//this.config = this.config || {};
		//this.config.headers = {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.nillable = true;
		this.target.collection = '/participationwinners';
	}
}

KSystem.include([
	'CODAForm',
	'KRadioButton',
	'KDate',
	'KTextArea',
	'KRichText'
], function() {
	CODACampaignParticipationWinnerForm.prototype = new CODAForm();
	CODACampaignParticipationWinnerForm.prototype.constructor = CODACampaignParticipationWinnerForm;

	CODACampaignParticipationWinnerForm.prototype.setData = function(data) {
		this.config.requestType = "KPanel";
		if (!!data) {
			this.target._id = this.target.href;
			this.target.data = data.elements[0];
			this.data = data.elements[0];
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"/wrml-schemas/participationwinners/participationwinners-schema\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODACampaignParticipationWinnerForm.prototype.loadForm = function() {
		if (!!this.participation_id) {
			this.kinky.get(this, 'rest:/participations/' + this.participation_id + '?embed=owner', {}, { 'E-Tag' : (new Date()).getTime()}, 'onRetrieveParticipation' );
		}else {
			this.draw();
		}
	};

	CODACampaignParticipationWinnerForm.prototype.onRetrieveParticipation = function(data, request) {
		this.data.participation = data;
		this.data.participation_id = data.id;
		if(!!data.campaign_id){
			this.data.campaign_id = data.campaign_id;	
		}
		if(!!data.phase_id){
			this.data.phase_id = data.phase_id;
		}		
		this.kinky.get(this, 'rest:/campaigns/' + this.data.participation.campaign_id + '/phases/' + this.data.participation.phase_id + '/prizes', {}, { 'E-Tag' : (new Date()).getTime()}, 'onLoadForm');
	};

	CODACampaignParticipationWinnerForm.prototype.onLoadForm = function(data, request) {
		this.data.prizes = data;
		this.draw();
	};

	CODACampaignParticipationWinnerForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODACampaignParticipationWinnerForm.prototype.addMinimizedPanel = function() {
		if (!!this.data.participation.href) {
			this.setTitle(gettext('$EDIT_CAMPAIGNPARTICIPATION_PROPERTIES') + ' \u00ab' + this.data.participation.id + '\u00bb');
		}

		var participation_winner_item = new KPanel(this);
		{
			participation_winner_item.hash = '/general';

			var owner = new KStatic(participation_winner_item, gettext('$PARTICIPATION_ITEM_FORM_OWNER'), 'owner');
			{
				if (!!this.data && !!this.data.participation && !!this.data.participation.owner && !!this.data.participation.owner.profile.name) {
					owner.setValue(this.data.participation.owner.profile.name);
				}
			}
			participation_winner_item.appendChild(owner);

			var start_date = new KStatic(participation_winner_item, gettext('$PARTICIPATION_ITEM_FORM_START_DATE'), 'start_date');
			{
				if(!!this.data && !!this.data.participation && !!this.data.participation.start_date){
					start_date.setValue(this.data.participation.start_date);
				}
			}
			start_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
			participation_winner_item.appendChild(start_date);

			var end_date = new KStatic(participation_winner_item, gettext('$PARTICIPATION_ITEM_FORM_END_DATE'), 'end_date');
			{
				if(!!this.data && !!this.data.participation && !!this.data.participation.end_date){
					end_date.setValue(this.data.participation.end_date);
				}
				else {
					end_date.setValue(gettext('$CODA_PARTICIPATION_NOT_FINISHED_YET'));
				}
			}
			end_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
			participation_winner_item.appendChild(end_date);

			var value = new KStatic(participation_winner_item, gettext('$PARTICIPATION_ITEM_FORM_VALUE'), 'value');
			{
				value.setValue(CODACampaignParticipationWinnerForm.getPoints(this.data.participation));
			}
			participation_winner_item.appendChild(value);

			var ranking = new KInput(participation_winner_item, gettext('$PARTICIPATION_WINNER_ITEM_FORM_RANKING'), 'ranking');
			{
				if(!!this.data && !!this.data.ranking){
					ranking.setValue(this.data.ranking);
				}
				ranking.setLength(8);
				ranking.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				ranking.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			participation_winner_item.appendChild(ranking);

			if(!!this.data.prizes && !!this.data.prizes.elements){
				var prize_selector = new KCombo(participation_winner_item, gettext('$PARTICIPATION_WINNER_ITEM_PRIZE_SELECTOR'), 'prize_id');
				prize_selector.addOption(null, '-', true);
				var generate_prize = null;
				var generated_prize = null;
				for (var index in this.data.prizes.elements){
					prize_selector.addOption(this.data.prizes.elements[index].id, this.data.prizes.elements[index].name, (!!this.data.prize_id && this.data.prize_id == this.data.prizes.elements[index].id) ? true: false);
					if(!!this.data.prize_id && this.data.prize_id == this.data.prizes.elements[index].id){
						if(!!this.data.prize_sended && this.data.prize_sended == true){
							var generated_prize = new KStatic(participation_winner_item, gettext('$PARTICIPATION_WINNER_PRIZE_GENERATED'), 'generated_prize');
							{
								generated_prize.setValue(gettext('$PARTICIPATION_WINNER_PRIZE_SENDED'));
								generated_prize.hash = '/generate-prize';
							}
						}else if(this.data.prizes.elements[index].digital_mime_type && this.data.prizes.elements[index].digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
							var generate_prize = new KCheckBox(participation_winner_item, gettext('$PARTICIPATION_WINNER_GENERATE_PRIZE'), 'generate_prize');
							generate_prize.addOption(true, gettext('$CHECKBOX_TRUE_OPTION'), false);
							generate_prize.setHelp(gettext('$PARTICIPATION_WINNER_GENERATE_PRIZE_HELP'), CODAForm.getHelpOptions());
							generate_prize.hash = '/generate-prize';
						}
					}
						
				}
				prize_selector.addEventListener('change', CODACampaignParticipationWinnerForm.onPrizeSelected);
				participation_winner_item.appendChild(prize_selector);
				if(generate_prize != null){
					participation_winner_item.appendChild(generate_prize);	
				}else if(generated_prize != null){
					participation_winner_item.appendChild(generated_prize);
				}				
			} 

		}
		this.addPanel(participation_winner_item, gettext('$FORM_GENERALINFO_PANEL'), true);				

	};

	CODACampaignParticipationWinnerForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{

			var owner = new KStatic(firstPanel, gettext('$PARTICIPATION_ITEM_FORM_OWNER'), 'owner');
			{
				if (!!parent.data && !!parent.data.participation && !!parent.data.participation.owner && !!parent.data.participation.owner.profile.name) {
					owner.setValue(parent.data.participation.owner.profile.name);
				}
			}
			firstPanel.appendChild(owner);
			
			var start_date = new KStatic(firstPanel, gettext('$PARTICIPATION_ITEM_FORM_START_DATE'), 'start_date');
			{
				if(!!parent.data && !!parent.data.participation && !!parent.data.participation.start_date){
					start_date.setValue(parent.data.participation.start_date);
				}
			}
			start_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_START_DATE_HELP'), CODAForm.getHelpOptions());
			firstPanel.appendChild(start_date);

			var end_date = new KStatic(firstPanel, gettext('$PARTICIPATION_ITEM_FORM_END_DATE'), 'end_date');
			{
				if(!!parent.data && !!parent.data.participation && !!parent.data.participation.end_date){
					end_date.setValue(parent.data.participation.end_date);
				}
				else {
					end_date.setValue(gettext('$CODA_PARTICIPATION_NOT_FINISHED_YET'));
				}
			}
			end_date.setHelp(gettext('$PARTICIPATION_ITEM_FORM_END_DATE_HELP'), CODAForm.getHelpOptions());
			firstPanel.appendChild(end_date);

			var value = new KStatic(firstPanel, gettext('$PARTICIPATION_ITEM_FORM_VALUE'), 'value');
			{
				value.setValue(CODACampaignParticipationWinnerForm.getPoints(parent.data.participation));
			}
			firstPanel.appendChild(value);

			var ranking = new KInput(firstPanel, gettext('$PARTICIPATION_WINNER_ITEM_FORM_RANKING'), 'ranking');
			{
				ranking.setLength(8);
				ranking.addValidator(/^([0-9]*)$/, gettext('$ERROR_MUST_BE_INTEGER'));
				ranking.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(ranking);

			if(!!parent.data && !!parent.data.prizes && !!parent.data.prizes.elements){
				var prize_selector = new KCombo(firstPanel, gettext('$PARTICIPATION_WINNER_ITEM_PRIZE_SELECTOR'), 'prize_id');
				prize_selector.addOption(null, '-', true);
				var generate_prize = null;
				var generated_prize = null;
				for (var index in parent.data.prizes.elements){
					prize_selector.addOption(parent.data.prizes.elements[index].id, parent.data.prizes.elements[index].name, false);						
				}
				prize_selector.addEventListener('change', CODACampaignParticipationWinnerForm.onPrizeSelected);
				firstPanel.appendChild(prize_selector);
				if(generate_prize != null){
					firstPanel.appendChild(generate_prize);	
				}				
			}

		}
		return [firstPanel];
	};

	CODACampaignParticipationWinnerForm.onPrizeSelected = function(event){
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);

		if (!!widget.parent.childWidget('/generate-prize')) {
			widget.parent.removeChild(widget.parent.childWidget('/generate-prize'));
		}
		if (!!widget.parent.childWidget('/generated-prize')) {
			widget.parent.removeChild(widget.parent.childWidget('/generated-prize'));
		}

		var selected_prize = null;
		for (var index in widget.parent.parent.data.prizes.elements){
			if (widget.parent.parent.data.prizes.elements[index].id == widget.getValue()){
				selected_prize = widget.parent.parent.data.prizes.elements[index];
				break;
			}
		}

		if(selected_prize != null){
			if(!!widget.parent.parent.data.prize_sended && widget.parent.parent.data.prize_sended == true){
				var generated_prize = new KStatic(widget.parent, gettext('$PARTICIPATION_WINNER_PRIZE_GENERATED'), 'generated_prize');
				{
					generated_prize.setValue(gettext('$PARTICIPATION_WINNER_PRIZE_SENDED'));
					generated_prize.hash = '/generate-prize';
				}
				widget.parent.appendChild(generated_prize);
			}else if(!!selected_prize.digital_mime_type && selected_prize.digital_mime_type == gettext('$PRIZE_TYPE_VOUCHER_PDF_CODE')){
				var generate_prize = new KCheckBox(widget.parent, gettext('$PARTICIPATION_WINNER_GENERATE_PRIZE'), 'generate_prize');
				generate_prize.addOption(true, gettext('$CHECKBOX_TRUE_OPTION'), false);
				generate_prize.setHelp(gettext('$PARTICIPATION_WINNER_GENERATE_PRIZE_HELP'), CODAForm.getHelpOptions());
				generate_prize.hash = '/generate-prize';
				widget.parent.appendChild(generate_prize);
			}
		}

	};

	CODACampaignParticipationWinnerForm.prototype.setDefaults = function(params){
		if (!!params.owner){
			delete params.owner;
		}

		if (!!params.start_date){
			delete params.start_date;
		}

		if (!!params.end_date){
			delete params.end_date;
		}

		if (!!params.value){
			delete params.value;
		}

		if (!!params.participation){
			delete params.participation;
		}

		if (!!params.prizes){
			delete params.prizes;
		}

		if(!!params.requestTypes){
			delete params.requestTypes;
		}

		if(!!params.responseTypes){
			delete params.responseTypes;
		}
	};

	CODACampaignParticipationWinnerForm.prototype.onPut = function(data, request){
		CODAAccountsShell.participations_list.list.refreshPages();
		//this.parent.closeMe();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODACampaignParticipationWinnerForm.prototype.onNoContent = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationWinnerForm.prototype.onBadRequest = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationWinnerForm.prototype.onPreConditionFailed = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationWinnerForm.prototype.onNotFound = function(data, request) {
		this.draw();
	};

	CODACampaignParticipationWinnerForm.getPoints = function(data) {
		if (!!data.rewards && !!data.rewards.reward_votes) {
			return data.rewards.reward_votes;
		}
		else if (!!data.rewards && !!data.rewards.reward_quiz_points) {
			return data.rewards.reward_quiz_points;
		}
		else if (!!data.rewards && !!data.rewards.reward_friends) {
			return data.rewards.reward_friends;
		}
		return '-';
	};

	KSystem.included('CODACampaignParticipationWinnerForm');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

