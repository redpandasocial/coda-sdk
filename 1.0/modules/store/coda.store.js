KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAStoreDashboard(parent, registered_providers) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, registered_providers);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODAStoreDashboard.prototype = new CODAMenuDashboard();
	CODAStoreDashboard.prototype.constructor = CODAStoreDashboard;

	CODAStoreDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, {
			icon : gettext('$CODA_STORE_PROVIDER_' + data.code.toUpperCase() + '_ICON'),
			activate : false,
			title : gettext('$CODA_STORE_PROVIDER_' + data.code.toUpperCase()),
			description : '<span>' + gettext('$CODA_STORE_PROVIDER_' + data.code.toUpperCase() + '_DESC') + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/edit' + data._id
			}]
		});
	};

	KSystem.included('CODAStoreDashboard');
}, Kinky.CODA_INCLUDER_URL);


function CODAStoreProviderForm(parent, href){
	if (parent != null) {
		CODALocaleForm.call(this, parent, href == null ? null : href, null, null);
		this.target.collection = href.substr(0, href.lastIndexOf('/'));
		this.nillable = true;
		this.single = true;
	}
}

KSystem.include([
	'CODALocaleForm'
], function() {
	CODAStoreProviderForm.prototype = new CODALocaleForm();
	CODAStoreProviderForm.prototype.constructor = CODAStoreProviderForm;

	CODAStoreProviderForm.prototype.setData = function(data) {
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;

			if (!!data.credentials.all_ALL) {		
				this.acceptedLocales = [ 'all_ALL' ];
				this.currentLocale = 'all_ALL';
			}
			else {
				for (var k in this.getShell().app_data.locales) {
					this.addLocale(this.getShell().app_data.locales[k]);
				}				
			}
		}
	};

	CODAStoreProviderForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAStoreProviderForm.prototype.addMinimizedPanel = function() {
		var panel = new KPanel(this);
		{
			panel.hash = '/general';
			CODAStoreProviderForm.addProviderFields(panel, this.data.code, this.data);
		}
		this.setTitle(gettext('$CODA_STORE_EDIT_PROVIDER_' + this.data.code.toUpperCase() + '_TITLE'));
		this.addPanel(panel, gettext('$CODA_STORE_PROVIDER_FORM_TITLE'), true);
	};


	CODAStoreProviderForm.prototype.setDefaults = function(params) {

	};

	CODAStoreProviderForm.addProviderFields = function(panel, provider, data, onWizard) {
		var provider = panel.getShell().providers[provider];

		for (var f in provider.provider_fields) {
			var conf = provider.provider_fields[f];
			var field = null;
			{
				if (!!onWizard) {
					eval('field = new ' + conf.type + '(panel, ' + (conf.type != 'KHidden' ? 'gettext(\'$CODA_STORE_PROVIDER_\' + provider.code.toUpperCase() + \'_\' + conf.name.toUpperCase() + \'_LABEL\'), ' : '') + '\'credentials.\' + conf.name)');
				}
				else if (!!data && !!data.credentials && !!data.credentials.all_ALL) {				
					eval('field = new ' + conf.type + '(panel, ' + (conf.type != 'KHidden' ? 'gettext(\'$CODA_STORE_PROVIDER_\' + provider.code.toUpperCase() + \'_\' + conf.name.toUpperCase() + \'_LABEL\'), ' : '') + '\'credentials.all_ALL.\' + conf.name)');
					field.isLocalized = true;
				}
				else {
					eval('field = new ' + conf.type + '(panel, ' + (conf.type != 'KHidden' ? 'gettext(\'$CODA_STORE_PROVIDER_\' + provider.code.toUpperCase() + \'_\' + conf.name.toUpperCase() + \'_LABEL\'), ' : '') + '\'credentials.\' + KLocale.LOCALE + \'.\' + conf.name)');
					field.isLocalized = true;
				}
				if (!!conf.value) {
					field.setValue(conf.value);
				}
				if (!!conf.options) {
					for (var o in conf.options) {
						var isset = false;
						if (!onWizard) {
							if (!!data && !!data.credentials && !!data.credentials.all_ALL && !!data.credentials.all_ALL[conf.name]) {
								isset = data.credentials.all_ALL[conf.name] == conf.options[o];
							}
							else if (!!data && !!data.credentials && !!data.credentials[KLocale.LOCALE] && !!data.credentials[KLocale.LOCALE][conf.name]) {
								isset = data.credentials[KLocale.LOCALE][conf.name] == conf.options[o];
							}
						}
						field.addOption(conf.options[o], gettext('$CODA_STORE_PROVIDER_' + provider.code.toUpperCase() + '_' + conf.options[o]), isset);
					}
				}
				else {
					if (!onWizard) {
						if (!!data && !!data.credentials && !!data.credentials.all_ALL && !!data.credentials.all_ALL[conf.name]) {
							field.setValue(data.credentials.all_ALL[conf.name]);
						}
						else if (!!data && !!data.credentials && !!data.credentials[KLocale.LOCALE] && !!data.credentials[KLocale.LOCALE][conf.name]) {
							field.setValue(data.credentials[KLocale.LOCALE][conf.name]);
						}
					}
				}
				if (conf.occurence == 'mandatory') {
					field.setHelp(gettext('$CODA_STORE_PROVIDER_' + provider.code.toUpperCase() + '_' + conf.name.toUpperCase() + '_HELP'), CODAForm.getHelpOptions());
					field.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				}
			}
			panel.appendChild(field);
		}
	};

	CODAStoreProviderForm.addWizardPanel = function(parent) {
		var shell = parent.getShell();

		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$CODA_STORE_PROVIDER_WIZ_GENERIC_PANEL_TITLE');
			firstPanel.description = gettext('$CODA_STORE_PROVIDER_WIZ_GENERIC_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-credit-card';

			var code = new KCombo(firstPanel, gettext('$CODA_STORE_PROVIDER_CODE') + '*', 'code');
			{
				var _providers = shell.providers;
				var _registered = shell.registered_providers;

				for (var p in _providers) {
					if (_providers[p].code == 'free') {
						continue;
					}
					var already = false;
					for (var r in _registered) {
						if (_providers[p].code == _registered[r].code) {
							already = true;
							break;
						}
					}
					if (already) {
						continue;
					}
					code.addOption(_providers[p].code, gettext('$CODA_STORE_PROVIDER_' + _providers[p].code.toUpperCase()), false);
				}
				
				code.setHelp(gettext('$CODA_STORE_PROVIDER_CODE_HELP'), CODAForm.getHelpOptions());
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(code);

		}
		
		var secondPanel = new KPanel(parent);
		{
			secondPanel.hash = '/provider/fields';
			secondPanel.title = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_PANEL_TITLE');
			secondPanel.description = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_PANEL_DESC');
			secondPanel.icon = 'css:fa fa-pencil';
		}

		if (!!shell && shell.className == 'CODAApplicationsShell') {
			parent.__onNext = parent.onNext;
			parent.onNext = function() {
				this.__onNext();
				switch(this.current) {
					case 8: {
						var panel = this.childWidget(this.hash + '/9');
						panel.title = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_' + this.values[8].code.toUpperCase() + '_PANEL_TITLE');
						panel.description = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_' + this.values[8].code.toUpperCase() + '_PANEL_DESC');
						panel.icon = 'css:rp rp-' + this.values[8].code;
						panel.removeAllChildren();

						var locale = new KCombo(panel, gettext('$CODA_STORE_PROVIDERS_COUNTRY'), 'locale');
						{
							locale.addOption('all_ALL', gettext('$CODA_STORE_PROVIDERS_COUNTRY_APPLY_ALL'), true);
							var available = Kinky.getDefaultShell().app_data.locales;
							for (var l in available) {
								locale.addOption(available[l], '<img style="width: 28px; padding-right: 5px; vertical-align: middle;" src="' + CODA_VERSION + '/images/flags/' + available[l].split('_')[1].toLowerCase() + '.png"></img>' + gettext('$CODA_COUNTRY_' + available[l]), false);
							}
							locale.setHelp(gettext('$CODA_STORE_PROVIDERS_COUNTRY_HELP'), CODAForm.getHelpOptions());
						}
						panel.appendChild(locale);

						CODAStoreProviderForm.addProviderFields(panel, this.values[8].code, null, true);
						break;
					}
				}
			};
		}

		return [ firstPanel, secondPanel ];
	};

	CODAStoreProviderForm.prototype.onNoContent = function(data, request) {
		CODALocaleForm.prototype.load.call(this);
	};

	CODAStoreProviderForm.prototype.onPost = CODAStoreProviderForm.prototype.onCreated = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAStoreProviderForm.prototype.onPut = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	var __shell = Kinky.getDefaultShell();
	if (!!__shell && __shell.className != 'CODAStoreShell') {
		__shell.onLoadStoreProviders = function(data, request) {
			delete data.http;
			this.providers = {};
			this.registered_providers = [];

			for (var p in data) {
				this.providers[data[p].code] = data[p];
			}			
			KSystem.included('CODAStoreProviderForm');
		};

		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		__shell.kinky.get(__shell, 'rest:/retrieveproviders', {}, headers, 'onLoadStoreProviders');
	}
	else {
		KSystem.included('CODAStoreProviderForm');
	}

}, Kinky.CODA_INCLUDER_URL);

function CODAStoreShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAStoreShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODAWizard',
	'CODAStoreDashboard',
	'CODAStoreProviderForm'
], function() {
	CODAStoreShell.prototype = new CODAGenericShell();
	CODAStoreShell.prototype.constructor = CODAStoreShell;

	CODAStoreShell.prototype.loadShell = function() {
		if(typeof(Storage) !== "undefined") {
			this.data.locales = JSON.parse(localStorage['locales_available']);
		}
		else {
			this.data.locales = [ "all_ALL" ];
		}

		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:/retrieveproviders', {}, headers, 'onLoadProviders');
	};

	CODAStoreShell.prototype.onLoadProviders = function(data, request) {
		delete data.http;
		this.providers = {};

		for (var p in data) {
			this.providers[data[p].code] = data[p];
		}

		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:/applications/' + this.data.app_id + '/paymentproviders', {}, headers, 'onLoadApplicationProviders');
	};

	CODAStoreShell.prototype.onLoadApplicationProviders = function(data, request) {
		this.registered_providers = data.elements;
		this.draw();
	};

	CODAStoreShell.prototype.draw = function() {
		this.setTitle(gettext('$STORE_SHELL_TITLE'));

		var addbt = new KButton(this, gettext('$CODA_STORE_PROVIDER_ADD'), 'add-provider', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/add'
			});
		});
		this.addToolbarButton(addbt, 3);

		this.dashboard = new CODAStoreDashboard(this, this.registered_providers);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAStoreShell.sendRequest = function() {
	};

	CODAStoreShell.prototype.editProvider = function(href) {
		var configForm = new CODAStoreProviderForm(this, href);
		{
			configForm.hash = '/store/provider';
		}
		this.makeDialog(configForm, { minimized : false });
	};

	CODAStoreShell.prototype.addProvider = function() {
		var unregistered = 0;
		for (var p in this.providers) {
			if (this.providers[p].code == 'free') {
				continue;
			}
			var already = false;
			for (var r in this.registered_providers) {
				if (this.providers[p].code == this.registered_providers[r].code) {
					already = true;
					break;
				}
			}
			if (already) {
				continue;
			}
			unregistered++;
		}

		if (unregistered == 0) {
			KMessageDialog.alert({
				message : gettext('$CODA_STORE_PROVIDER_ALL_REGISTERED'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}
		else {			
			var screens = [ 'CODAStoreProviderForm'];
			var wiz = new CODAWizard(this, screens);
			wiz.setTitle(gettext('$CODA_STORE_PROVIDERS_WIZ_TITLE'));
			wiz.hash = '/applications/' + this.data.app_id + '/paymentproviders';

			wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
				this.parent.closeMe();
				window.document.location.reload(true);
				return
			};

			wiz.onNext = function() {
				switch(this.current) {
					case 0: {
						var panel = this.childWidget(this.hash + '/1');
						panel.title = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_' + this.values[0].code.toUpperCase() + '_PANEL_TITLE');
						panel.description = gettext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_' + this.values[0].code.toUpperCase() + '_PANEL_DESC');
						panel.icon = 'css:rp rp-' + this.values[0].code;
						panel.removeAllChildren();

						var locale = new KCombo(panel, gettext('$CODA_STORE_PROVIDERS_COUNTRY'), 'locale');
						{
							locale.addOption('all_ALL', gettext('$CODA_STORE_PROVIDERS_COUNTRY_APPLY_ALL'), true);
							var available = Kinky.getDefaultShell().app_data.locales;
							for (var l in available) {
								locale.addOption(available[l], '<img style="width: 28px; padding-right: 5px; vertical-align: middle;" src="' + CODA_VERSION + '/images/flags/' + available[l].split('_')[1].toLowerCase() + '.png"></img>' + gettext('$CODA_COUNTRY_' + available[l]), false);
							}
							locale.setHelp(gettext('$CODA_STORE_PROVIDERS_COUNTRY_HELP'), CODAForm.getHelpOptions());
						}
						panel.appendChild(locale);

						CODAStoreProviderForm.addProviderFields(panel, this.values[0].code, null, true);
						break;
					}
					case 1: {
						break;
					}
					case 2: {

						break;
					}
				}
			};

			wiz.send = function(params) {
				try {
					if (!!params) {
						var credentials = params.credentials;
						params.credentials = { };
						params.credentials[params.locale] = credentials;
						delete params.locale;

						params.name = params.code;
						
						this.kinky.put(this, 'rest:' + wiz.hash + '/' + params.code, params, Kinky.SHELL_CONFIG[this.shell].headers);
					}
				}
				catch (e) {

				}
			};

			this.makeWizard(wiz);
		}
	};

	CODAStoreShell.prototype.onError = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CAMPAIGNS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAStoreShell.prototype.onNoContent = function(data, request) {
		if (request.service.indexOf('apppaymentproviders') != -1) {
			this.draw();
			KBreadcrumb.dispatchURL({
				hash : '/add'
			});
		}
		else if (request.service.indexOf('retrieveproviders')) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.post(this, 'rest:/apppaymentproviders', {}, headers, 'onLoadApplicatio0nProviders');
		}
		else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODAStoreShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'edit': {
				Kinky.getDefaultShell().editProvider('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5], true);

				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add': {
				Kinky.getDefaultShell().addProvider();

				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	KSystem.included('CODAStoreShell');		

}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

