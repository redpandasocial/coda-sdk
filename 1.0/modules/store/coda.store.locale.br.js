
///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$STORE_SHELL_TITLE', 'Gateways de Pagamento', 'br');
settext('$CODA_STORE_PROVIDER_ADD', '<i class="fa fa-plus"></i>Adicionar Gateway', 'br');

settext('$CODA_STORE_PROVIDER_PAYPAL_DESC', 'Gateway de pagamento associada ao PayPal. Configura as credenciais de PayPal necessárias para receberes os pagamentos da aplicação directamente na tua conta PayPal.', 'br');

settext('$CODA_STORE_PROVIDER_DEFAULT_ICON', 'css:fa fa-building', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_ICON', 'css:rp rp-paypal', 'br');

settext('$CODA_STORE_PROVIDER_ALL_REGISTERED', 'Já registaste todos os tipos de Gateway de Pagamento diponíveis nesta plataforma.', 'br');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$CODA_STORE_PROVIDERS_WIZ_TITLE', 'Nova Gateway de Pagamento', 'br');
settext('$CODA_STORE_PROVIDER_WIZ_GENERIC_PANEL_TITLE', 'Tipo de Gateway de Pagamento', 'br');
settext('$CODA_STORE_PROVIDER_WIZ_GENERIC_PANEL_DESC', 'Indica o tipo de Gateway de Pagamento.<br><br><i>Escolhe uma neste momento. Poderás adicionar mais após completar este processo, clicando no botão do cabeçalho "Adicionar Gateway".</i>', 'br');
settext('$CODA_STORE_PROVIDER_CODE', 'Tipo de Gateway', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL', 'PayPal', 'br');
settext('$CODA_STORE_PROVIDER_CODE_HELP', 'Indica a Gateway de Pagamento que pretendes configurar. Terás que ter uma conta associada à Gateway que escolheres, pois serão requisitados dados específicos dessa conta no ecrã seguinte.', 'br');
settext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_PAYPAL_PANEL_TITLE', 'PayPal', 'br');
settext('$CODA_STORE_PROVIDER_WIZ_SPECIFIC_PAYPAL_PANEL_DESC', 'Indica as credenciais para a aplicação PayPal associada à tua conta PayPal.<br>Se ainda não criaste uma aplicação PayPal, poderás criá-la <a target="_blank" href="https://developer.paypal.com/webapps/developer/applications/myapps">aqui</a>.<br><br><i>Poderás alterar estes dados em qualquer momento, acedendo a esta área.</i>', 'br');
settext('$CODA_STORE_EDIT_PROVIDER_PAYPAL_TITLE', 'Editar Gateway - PayPal', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_CLIENT_ID_LABEL', 'Identificador de cliente PayPal <span style="text-transform: lowercase !important;">(client_id)</span>', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_CLIENT_SECRET_LABEL', 'Segredo de cliente PayPal <span style="text-transform: lowercase !important;">(client_secret)</span>', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_SANDBOX_MODE_LABEL', 'Aplicação PayPal em modo de teste?', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_on', 'Sim', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_off', 'Não', 'br');
settext('$CODA_STORE_PROVIDERS_COUNTRY', 'Região em que estas credenciais estarão activas', 'br');
settext('$CODA_STORE_PROVIDERS_COUNTRY_APPLY_ALL', '-- APLICAR PARA O MUNDO INTEIRO --', 'br');

settext('$CODA_STORE_EDIT_PROVIDER_TITLE', '', 'br');

///////////////////////////////////////////////////////////////////
//MESSAGES & CONFIRMATIONS

///////////////////////////////////////////////////////////////////
// ERROR CODES
settext('$USERS_MODULE_ERROR_0', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAStoreLocale_pt');

///////////////////////////////////////////////////////////////////
// FORMS
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_CLIENT_ID_HELP', 'Se ainda não tens uma conta Paypal Business acede ao Paypal e cria uma conta.<br> Se já tens uma conta acede à tua página de aplicações e da que quiseres associar copia o Client ID e cola-o na caixa.', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_CLIENT_SECRET_HELP', 'Se ainda não tens uma conta Paypal Business acede ao Paypal e cria uma conta.<br> Se já tens uma conta acede à tua página de aplicações e da que quiseres associar copia o Client Secret e cola-o na caixa.', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_SANDBOX_MODE_HELP', 'Indica se a aplicação de PayPal para as quais estás a apresentar as credenciais deverá ser utilizada em modo de teste. O valor deste campo deverá ser "Sim" apenas se ainda estás a testar a integração da tua aplicação Redpanda com a tua aplicação PayPal.', 'br');
settext('$CODA_STORE_PROVIDER_PAYPAL_PAYPAL_GRANT_TYPE_HELP', '', 'br');
settext('$CODA_STORE_PROVIDERS_COUNTRY_HELP', 'Indique o país ou região em que serão válidas as credenciais introduzidas abaixo.', 'br');

KSystem.included('CODAStoreLocale_Help_pt');

