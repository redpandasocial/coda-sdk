///////////////////////////////////////////////////////////////////
// FORMS

// WIDGETS GENERAL
// titles
settext('$HELP_KWIDGET_CHILDREN_LIST', '<i class="fa fa-cogs"></i> WIDGETS', 'pt');
settext('$HELP_FEED_FEEDSCONFIG', '<i class="fa fa-rss"></i> Fontes de Dados', 'pt');
settext('$HELP_RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'pt');
settext('$HELP_TEMPLATE_DATA', '<i class="fa fa-magic"></i> ESQUEMA DE DADOS', 'pt');
settext('$HELP_WIDGET_GRAPHICAL_ATTRIBUTES', '<i class="fa fa-eye"></i> Atributos Gr\u00e1ficos', 'pt');
settext('$HELP_WIDGET_METADATA', '<i class="fa fa-flask"></i> METADATA', 'pt');

// labels
settext('$HELP_WIDGET_GRAPHICS_LAYOUT', 'Layout', 'pt');
settext('$HELP_WIDGET_RESOURCE_TEMPLATE', 'Template de Dados', 'pt');
settext('$HELP_WIDGET_RESOURCE_CLASS', 'Widget Alvo', 'pt');
settext('$HELP_WIDGET_CLASS', 'Classe CSS', 'pt');
settext('$HELP_WIDGET_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$HELP_WIDGET_HASH', 'Identificador (hash)', 'pt');
settext('$HELP_WIDGET_CONFIG_GRAPHICAL_DEFS', 'IMAGEM DE FUNDO', 'pt');
settext('$HELP_WIDGET_GRAPHICS_BACKGROUND', 'Imagem de Fundo', 'pt');

settext('$HELP_RESOURCE_FREETEXT', 'Termos', 'pt');

settext('$HELP_LAYOUT_DATA', 'ESQUEMA GR\u00c1FICO', 'pt');
settext('$HELP_LAYOUT', 'Escolha o Layout pretendido para este conte\u00fado', 'pt');

settext('$HELP_DOCROOT_RESTSERVER', 'Servidor REST', 'pt');
settext('$HELP_DOCROOT_DEFAULT_LANG', 'Idioma', 'pt');
settext('$HELP_DOCROOT_UIROOT', 'Servidor HTTP', 'pt');

settext('$HELP_LINK_REQUESTTYPES', 'Tipo de Widget', 'pt');
settext('$HELP_LINK_TARGET_CHILD_CONTAINER', 'Layout Alvo', 'pt');
settext('$HELP_LINK_TITLE', 'T\u00edtulo', 'pt');

settext('$HELP_METADATA_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$HELP_METADATA_DESIGNATION', 'Designa\u00e7\u00e3o', 'pt');
settext('$HELP_METADATA_GEO_COORDINATES', 'Coordenadas GPS', 'pt');
settext('$HELP_METADATA_URL', 'URL', 'pt');
settext('$HELP_METADATA_LOCATION_NAME', 'Localidade', 'pt');
settext('$HELP_METADATA_OBJECTTYPE', 'Tipo de Objecto (Opengraph)', 'pt');
settext('$HELP_METADATA_PICTURE', 'Logo para share', 'pt');

settext('$HELP_PERMISSIONS_GROUP', 'Grupo', 'pt');
settext('$HELP_PERMISSIONS_VALUE', 'Valor', 'pt');
//----------

// DOCROOT
//----------

// PAGE
settext('$HELP_LINK_RESOURCE_HASH', 'URL da P\u00e1gina', 'pt');
//----------

// TEXT
settext('$HELP_TEXT_TITLE', 'T\u00edtulo', 'pt');
settext('$HELP_TEXT_BODY', 'Corpo do Texto', 'pt');
settext('$HELP_TEXT_LEAD', 'Lead', 'pt');
//----------

// IMAGE
settext('$HELP_IMAGE_URL', 'Imagem', 'pt');
settext('$HELP_IMAGE_SCALED_URL', 'Thumb da Imagem', 'pt');
settext('$HELP_IMAGE_TITLE', 'LEGENDA', 'pt');
//----------

// LINK
settext('$HELP_LINK_IMAGE', 'Imagem', 'pt');
settext('$HELP_LINK_TARGET_WINDOW', 'Janela Alvo', 'pt');
settext('$HELP_LINK_TOOLTIP', 'Tooltip', 'pt');
settext('$HELP_LINK_URL', 'URL', 'pt');
settext('$HELP_LINK_TARGET_SAME', 'Mesma Janela', 'pt');
settext('$HELP_LINK_TARGET_OTHER', 'Nova Janela', 'pt');
//----------

// HIGHLIGHT
settext('$HELP_KHIGHLIGHT_ACTION_LIST', '<i class="fa fa-external-link"></i> Ac\u00e7\u00f5es', 'pt');
settext('$HELP_HIGHLIGTH_ACTIONS', 'AC\u00c7\u00d5ES', 'pt');
settext('$HELP_KLIST_LABEL_ACTIONS', 'AC\u00c7\u00d5ES  \u00bb ', 'pt');
//----------

// MENU
settext('$HELP_KLISTITEM_ADD_ACTION', 'Gerir Sub-Menu', 'pt');

settext('$HELP_KMENU_PAGES_PANEL', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'pt');
settext('$HELP_KMENULIST_NEWENTRY', 'NOVA ENTRADA', 'pt');

settext('$HELP_MENU_WIDGET_PAGES', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'pt');
settext('$HELP_MENU_HASH', 'HASH', 'pt');
settext('$HELP_MENU_HREF', 'HREF', 'pt');
settext('$HELP_MENU_TITLE', 'T\u00edtulo do Menu', 'pt');
settext('$HELP_MENU_CLASS', 'Classe de CSS', 'pt');
settext('$HELP_MENU_INTERNAL_PAGE_INFO', 'Seleccione uma p\u00e1gina do site', 'pt');
settext('$HELP_MENU_EXTERNAL_PAGE_INFO', 'ou insira o url de uma p\u00e1gina externa', 'pt');

//----------

// LIST
settext('$HELP_CONFIG_LIST_AUTO_ROTATE', 'Per\u00edodo de Rota\u00e7\u00e3o Autom\u00e1tica de Listagem', 'pt');
settext('$HELP_LIST_WIDGET_PAGESIZE', 'N\u00famero de Itens Por P\u00e1gina', 'pt');
//----------

// FEED
settext('$HELP_FEED_FEEDS', 'URL', 'pt');
settext('$HELP_FEED_OAUTH_ACCESSTOKEN', 'Acces Token', 'pt');
settext('$HELP_FEED_OAUTH_CLIENTID', 'Client ID', 'pt');
settext('$HELP_FEED_OAUTH_CLIENTSECRET', 'Client Secret', 'pt');
settext('$HELP_FEED_OAUTH_PROVIDER', 'Provider', 'pt');
//----------

// FEEDBACK
settext('$HELP_KFEEDBACK_TYPES', 'Permitir tipos de Feedback', 'pt');
settext('$HELP_KFEEDBACK_COMMENT_TYPE', 'Coment\u00e1rios', 'pt');
settext('$HELP_KFEEDBACK_VOTE_TYPE', 'Votos', 'pt');
settext('$HELP_KFEEDBACK_REVIEW_TYPE', 'Reviews', 'pt');
settext('$HELP_WIDGET_CONFIG_FEEDBACK_DESCRIPTION', 'Coment\u00e1rios / Votos / Reviews', 'pt');
//----------

// PANEL
//----------

// MAP
settext('$HELP_MAP_STREET_VIEW', 'Vista de Rua', 'pt');
settext('$HELP_MAP_DOUBLE_CLICK_ZOOM', 'Zoom em duplo-click', 'pt');
settext('$HELP_MAP_ZOOM', 'Zoom Inicial', 'pt');
settext('$HELP_MAP_MAP_TYPE', 'Tipo de Mapa', 'pt');
settext('$HELP_MAP_LATITUDE', 'Latitude', 'pt');
settext('$HELP_MAP_LONGITUDE', 'Longitude', 'pt');
settext('$HELP_MAP_DRAGGABLE_MARKERS', 'Marcadores Mov\u00edveis', 'pt');
settext('$HELP_MAP_ISDEFAULT', 'Pr\u00e9-definido', 'pt');
settext('$HELP_MAP_DEFAULT_MARKER_ICON', 'Icon Pr\u00e9-definido', 'pt');

settext('$HELP_MAP_MARKER_LIST', 'Markers', 'pt');

settext('$HELP_MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'pt');
settext('$HELP_MAP_MARKER_LONGITUDE', 'Longitude', 'pt');
settext('$HELP_MAP_MARKER_LATITUDE', 'Latitude', 'pt');
settext('$HELP_MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'pt');
settext('$HELP_MAP_MARKER_ICON', 'Icon', 'pt');
settext('$HELP_MAP_MARKER_BALLON', 'Descri\u00e7\u00e3o do Bal\u00e3o', 'pt');

settext('$HELP_MAP_MAP_TYPE_HIBRID', 'H\u00edbrido', 'pt');
settext('$HELP_MAP_MAP_TYPE_ROADMAP', 'Mapa de estradas', 'pt');
settext('$HELP_MAP_MAP_TYPE_SATELLITE', 'Sat\u00e9lite', 'pt');
settext('$HELP_MAP_MAP_TYPE_TERRAIN', 'Terreno', 'pt');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


KSystem.included('CODACMSLocale_Help_pt');

///////////////////////////////////////////////////////////////////
// KINKY CODA FRONTEND PLUGIN
settext('$CODA_WIDGET_TOOLBAR_SAVE', 'Guardar', 'pt');
settext('$CODA_WIDGET_TOOLBAR_EDIT', 'Editar', 'pt');
settext('$CODA_WIDGET_TOOLBAR_ADD', 'Adicionar Widget', 'pt');
settext('$CODA_WIDGET_TOOLBAR_DELETE', 'Remover', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$CODA_PUBLISH_CONFIRM', 'Deseja publicar esta vers\u00e3o do website?', 'pt');
settext('$CODA_PUBLISHED_SUCCES', 'Website publicado com sucesso.', 'pt');

settext('$CODA_DELETE_WIDGET_CONFIRM', 'Deseja remover esta widget?', 'pt');

settext('$CONFIRM_WIDGET_SAVED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget "$WIDGET_TYPE" guardada com sucesso!<span>', 'pt');
settext('$CONFIRM_WIDGET_CREATED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget "$WIDGET_TYPE" criada com sucesso!<span>', 'pt');
settext('$CONFIRM_WIDGET_SAVED_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro ao guardar a widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'pt');
settext('$CONFIRM_WIDGET_CREATED_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro ao criar a widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'pt');
settext('$CONFIRM_WIDGET_GET_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro no acesso aos dados da widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'pt');
settext('$CONFIRM_WIDGET_DELETED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget removida com sucesso</span>', 'pt');
settext('$CONFIRM_WIDGET_MOVED_SUCCESS', '<span style="color: rgb(161, 196, 0);">A widget foi movida com sucesso!<span>', 'pt');

settext('$CODA_REVERT_CANNOT_CURRENT_VERSION', 'A vers\u00e3o escolhida \u00e9 a vers\u00e3o actual.', 'pt');
settext('$CODA_REVERT_SELECT_VERSION', 'Escolha a vers\u00E3o para a qual pretende reverter.', 'pt');
settext('$CODA_REVERT_CONFIRM', 'Tem a certeza que pretende reverter o website para esta vers\u00E3o?\nTodas as altera\u00E7\u00F5es efectuadas ap\u00F3s $DATE ser\u00E3o perdidas.', 'pt');
settext('$CODA_REVERTED_SUCCES', 'A vers\u00E3o foi carregada com sucesso. Clique em "REFRESCAR" para visualizar as altera\u00E7\u00F5es.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBARS
settext('$CODA_SHELL_ADD_PREFIX', 'ADICIONAR ', 'pt');
settext('$CODA_SHELL_PAGE_LIST', 'MAPA', 'pt');
settext('$CODA_SHELL_ADD_PAGE', 'NOVA', 'pt');
settext('$CODA_SHELL_EDIT_CURRENT_PAGE', 'EDITAR P\u00c1GINA', 'pt');
settext('$CODA_SHELL_ADD_WIDGET', 'WIDGETS', 'pt');

settext('$CODA_HOVERCONTROL_MOVE_WIDGET', 'Mover Widget', 'pt');
settext('$CODA_HOVERCONTROL_ADD_WIDGET', 'Adicionar Widget', 'pt');

settext('$CODA_PAGE_OPTIONS_EDIT', 'Editar', 'pt');
settext('$CODA_PAGE_OPTIONS_ADDCHILD', 'Adicionar Widget', 'pt');
settext('$CODA_PAGE_OPTIONS_REMOVE', 'Remover', 'pt');

settext('$CODA_SHELL_RETURN_HOME', 'Voltar &agrave; lista<br>de m&oacute;dulos...', 'pt');
settext('$CODA_SHELL_EDIT_DOCROOT', 'Editar website', 'pt');
settext('$CODA_SHELL_LIST_PAGES', 'Ir para p&aacute;gina...', 'pt')
settext('$CODA_SHELL_NEW_ELEMENT', 'Adicionar novo elemento...', 'pt');
settext('$CODA_SHELL_EDIT_PAGE', 'Editar p&aacute;gina ', 'pt');

settext('$CODA_SHELL_REVERT', '<i class="fa fa-undo"></i> Reverter', 'pt');
settext('$CODA_SHELL_PUBLISH', '<i class="fa fa-exchange"></i> Publicar', 'pt');
settext('$CODA_SHELL_REFRESH', '<i class="fa fa-refresh"></i> Refrescar', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DIALOGS

// VERSIONS
settext('$VERSION_LIST_TITLE', 'Escolha a vers\u00E3o para a qual pretende reverter', 'pt');
//----------

// SITE DIALOG
settext('$SITE_PROPERTIES', 'WEBSITE', 'pt');
//----------

// NEW PAGE DIALOS
settext('$NEW_PAGE_PROPERTIES', 'ADICIONAR P\u00c1GINA', 'pt');
//----------

// PAGE DIALOG
settext('$PAGE_PROPERTIES', 'P\u00c1GINA', 'pt');
//----------

// NEW USER DIALOG
settext('$NEW_USER_PROPERTIES', 'ADICIONAR UTILIZADOR', 'pt');
//----------

// WIDGET DIALOG
settext('$WIDGET_PROPERTIES', 'WIDGET', 'pt');
settext('$NEW_WIDGET_PROPERTIES', 'ADICIONAR WIDGET', 'pt');
//----------

// SITEMAP DIALOG
settext('$CODA_PAGELIST_PAGES', 'MAPA DO SITE', 'pt');
settext('$CODA_PAGELIST_FILTER', '<i class="fa fa-search"></i> Filtrar Lista', 'pt');
//----------

// NEW WIDGET DIALOG
settext('$CAT_MEDIA', 'MEDIA', 'pt');
settext('$WIDGET_CONFIG_LINK_DESCRIPTION', '<i class="fa fa-link"></i> Hiperliga\u00e7\u00e3o', 'pt');
settext('$WIDGET_CONFIG_IMAGE_DESCRIPTION', '<i class="fa fa-picture-o"></i> Imagem', 'pt');
settext('$WIDGET_CONFIG_TEXT_DESCRIPTION', '<i class="fa fa-font"></i> Texto', 'pt');
settext('$WIDGET_CONFIG_HIGHLIGHT_DESCRIPTION', '<i class="fa fa-star"></i> Destaque', 'pt');
settext('$WIDGET_CONFIG_MAP_DESCRIPTION', '<i class="fa fa-map-marker"></i> Mapa', 'pt');

settext('$CAT_CONTAINER', 'AGREGADORES', 'pt');
settext('$WIDGET_CONFIG_GALLERY_DESCRIPTION', '<i class="fa fa-camera-retro"></i> Galeria', 'pt');
settext('$WIDGET_CONFIG_LIST_DESCRIPTION', '<i class="fa fa-list-alt"></i> Lista', 'pt');
settext('$WIDGET_CONFIG_PANEL_DESCRIPTION', '<i class="fa fa-columns"></i> Painel', 'pt');
settext('$WIDGET_CONFIG_MENU_DESCRIPTION', '<i class="fa fa-sitemap"></i> Menu', 'pt');
settext('$WIDGET_CONFIG_SCROLLEDLIST_DESCRIPTION', '<i class="fa fa-th"></i> Lista c/ Scroll', 'pt');
settext('$WIDGET_CONFIG_PAGEDLIST_DESCRIPTION', '<i class="fa fa-list-ol"></i> Lista c/ P\u00e1ginas', 'pt');

settext('$CAT_SOCIAL', 'REDES SOCIAIS', 'pt');
settext('$WIDGET_CONFIG_FEED_DESCRIPTION', '<i class="fa fa-rss"></i> Feed', 'pt');
settext('$WIDGET_CONFIG_SHARE_DESCRIPTION', '<i class="fa fa-share"></i> Partilha', 'pt');

settext('$CAT_LANGUAGE', 'IDIOMA', 'pt');
settext('$WIDGET_CONFIG_LANGSELECTION_DESCRIPTION', '<i class="fa fa-flag"></i> Selec\u00e7\u00e3o Idioma', 'pt');

settext('$CAT_PAGE', 'P\u00c1GINAS', 'pt');

settext('$WIDGET_CONFIG_SHELL_DESCRIPTION', 'Website', 'pt');
settext('$WIDGET_CONFIG_PAGE_DESCRIPTION', 'P\u00e1gina', 'pt');
settext('$WIDGET_CONFIG_SEARCH_DESCRIPTION', 'Pesquisa', 'pt');
settext('$WIDGET_CONFIG_NEWSLETTER_DESCRIPTION', 'Subscri\u00e7\u00e3o Newsletter', 'pt');
settext('$WIDGET_CONFIG_ORFEUPAGE_DESCRIPTION', 'P\u00e1gina Tipo Orfeu', 'pt');
settext('$WIDGET_CONFIG_CATALOGHIGHLIGHT_DESCRIPTION', 'Destaque Catalogo', 'pt');
settext('$WIDGET_CONFIG_CATALOGLIST_DESCRIPTION', 'Catalogo', 'pt');
settext('$WIDGET_CONFIG_ARTISTALBUMLIST_DESCRIPTION', 'Lista de Albuns (Artista)', 'pt');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// WIDGET TITLES
settext('$KLINK', 'Hiperliga\u00e7\u00e3o', 'pt');
settext('$KMENU', 'Menu', 'pt');
settext('$KLIST', 'Listagem', 'pt');
settext('$KTEXT', 'Texto', 'pt');
settext('$KHIGHLIGHT', 'Destaque', 'pt');
settext('$KPAGEDLIST', 'Lista c/ P\u00e1ginas', 'pt');
settext('$KPANEL', 'Painel', 'pt');
settext('$KIMAGE', 'Imagem', 'pt');
settext('$KSCROLLEDLIST', 'List c/ Scroll', 'pt');
settext('$KSHELL', 'Website', 'pt');
settext('$KSHELLPART', 'P\u00e1gina', 'pt');
settext('$KMAP', 'Mapa', 'pt');
settext('$KWIDGET', 'Widget', 'pt');
settext('$KFEED', 'Feed', 'pt');
settext('$KFEEDBACK', 'Feedback de Utilizador', 'pt');
settext('$OFSITESEARCH', 'Pesquisa', 'pt');
settext('$OFNEWSLETTERINPUT', 'Subscri\u00e7\u00e3o Newsletter', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FORMS

// WIDGETS GENERAL
// titles
settext('$KWIDGET_CHILDREN_LIST', '<i class="fa fa-cogs"></i> WIDGETS', 'pt');
settext('$FEED_FEEDSCONFIG', '<i class="fa fa-rss"></i> Fontes de Dados', 'pt');
settext('$RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'pt');
settext('$TEMPLATE_DATA', '<i class="fa fa-magic"></i> ESQUEMA DE DADOS', 'pt');
settext('$WIDGET_GRAPHICAL_ATTRIBUTES', '<i class="fa fa-eye"></i> Atributos Gr\u00e1ficos', 'pt');
settext('$WIDGET_METADATA', '<i class="fa fa-flask"></i> METADATA', 'pt');

// labels
settext('$WIDGET_GRAPHICS_LAYOUT', 'Layout', 'pt');
settext('$WIDGET_RESOURCE_TEMPLATE', 'Template de Dados', 'pt');
settext('$WIDGET_RESOURCE_CLASS', 'Widget Alvo', 'pt');
settext('$WIDGET_CLASS', 'Classe CSS', 'pt');
settext('$WIDGET_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$WIDGET_HASH', 'Identificador (hash)', 'pt');
settext('$WIDGET_CONFIG_GRAPHICAL_DEFS', 'IMAGEM DE FUNDO', 'pt');
settext('$WIDGET_GRAPHICS_BACKGROUND', 'Imagem de Fundo', 'pt');

settext('$RESOURCE_FREETEXT', 'Termos', 'pt');

settext('$LAYOUT_DATA', 'ESQUEMA GR\u00c1FICO', 'pt');
settext('$LAYOUT', 'Escolha o Layout pretendido para este conte\u00fado', 'pt');

settext('$DOCROOT_RESTSERVER', 'Servidor REST', 'pt');
settext('$DOCROOT_DEFAULT_LANG', 'Idioma', 'pt');
settext('$DOCROOT_UIROOT', 'Servidor HTTP', 'pt');

settext('$LINK_REQUESTTYPES', 'Tipo de Widget', 'pt');
settext('$LINK_TARGET_CHILD_CONTAINER', 'Layout Alvo', 'pt');
settext('$LINK_TITLE', 'T\u00edtulo', 'pt');

settext('$METADATA_DESCRIPTION', 'Descri\u00e7\u00e3o', 'pt');
settext('$METADATA_DESIGNATION', 'Designa\u00e7\u00e3o', 'pt');
settext('$METADATA_GEO_COORDINATES', 'Coordenadas GPS', 'pt');
settext('$METADATA_URL', 'URL', 'pt');
settext('$METADATA_LOCATION_NAME', 'Localidade', 'pt');
settext('$METADATA_OBJECTTYPE', 'Tipo de Objecto (Opengraph)', 'pt');
settext('$METADATA_PICTURE', 'Logo para share', 'pt');

settext('$PERMISSIONS_GROUP', 'Grupo', 'pt');
settext('$PERMISSIONS_VALUE', 'Valor', 'pt');
//----------

// DOCROOT
//----------

// PAGE
settext('$LINK_RESOURCE_HASH', 'URL da P\u00e1gina', 'pt');
settext('$PAGE_REQUESTTYPES', 'Template da P\u00e1gina', 'pt');
//----------

// TEXT
settext('$TEXT_BODY', 'Corpo do Texto', 'pt');
settext('$TEXT_LEAD', 'Lead', 'pt');
//----------

// IMAGE
settext('$IMAGE_URL', 'Imagem', 'pt');
settext('$IMAGE_SCALED_URL', 'Thumb da Imagem', 'pt');
settext('$IMAGE_TITLE', 'LEGENDA', 'pt');
//----------

// LINK
settext('$LINK_IMAGE', 'Imagem', 'pt');
settext('$LINK_TARGET_WINDOW', 'Janela Alvo', 'pt');
settext('$LINK_TOOLTIP', 'Tooltip', 'pt');
settext('$LINK_URL', 'URL', 'pt');
settext('$LINK_TARGET_SAME', 'Mesma Janela', 'pt');
settext('$LINK_TARGET_OTHER', 'Nova Janela', 'pt');
//----------

// HIGHLIGHT
settext('$KHIGHLIGHT_ACTION_LIST', '<i class="fa fa-external-link"></i> Ac\u00e7\u00f5es', 'pt');
settext('$HIGHLIGTH_ACTIONS', 'AC\u00c7\u00d5ES', 'pt');
settext('$KLIST_LABEL_ACTIONS', 'AC\u00c7\u00d5ES  \u00bb ', 'pt');
//----------

// MENU
settext('$KLISTITEM_ADD_ACTION', 'Gerir Sub-Menu', 'pt');

settext('$KMENU_PAGES_PANEL', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'pt');
settext('$KMENULIST_NEWENTRY', 'NOVA ENTRADA', 'pt');

settext('$MENU_WIDGET_PAGES', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'pt');
settext('$MENU_HASH', 'HASH', 'pt');
settext('$MENU_HREF', 'HREF', 'pt');
settext('$MENU_TITLE', 'T\u00edtulo do Menu', 'pt');
settext('$MENU_CLASS', 'Classe de CSS', 'pt');
settext('$MENU_INTERNAL_PAGE_INFO', 'Seleccione uma p\u00e1gina do site', 'pt');
settext('$MENU_EXTERNAL_PAGE_INFO', 'ou insira o url de uma p\u00e1gina externa', 'pt');

//----------

// LIST
settext('$CONFIG_LIST_AUTO_ROTATE', 'Per\u00edodo de Rota\u00e7\u00e3o Autom\u00e1tica de Listagem', 'pt');
settext('$LIST_WIDGET_PAGESIZE', 'N\u00famero de Itens Por P\u00e1gina', 'pt');
//----------

// FEED
settext('$FEED_FEEDS', 'URL', 'pt');
settext('$FEED_OAUTH_ACCESSTOKEN', 'Acces Token', 'pt');
settext('$FEED_OAUTH_CLIENTID', 'Client ID', 'pt');
settext('$FEED_OAUTH_CLIENTSECRET', 'Client Secret', 'pt');
settext('$FEED_OAUTH_PROVIDER', 'Provider', 'pt');
//----------

// FEEDBACK
settext('$KFEEDBACK_TYPES', 'Permitir tipos de Feedback', 'pt');
settext('$KFEEDBACK_COMMENT_TYPE', 'Coment\u00e1rios', 'pt');
settext('$KFEEDBACK_VOTE_TYPE', 'Votos', 'pt');
settext('$KFEEDBACK_REVIEW_TYPE', 'Reviews', 'pt');
settext('$WIDGET_CONFIG_FEEDBACK_DESCRIPTION', 'Coment\u00e1rios / Votos / Reviews', 'pt');
//----------

// PANEL
//----------

// MAP
settext('$MAP_STREET_VIEW', 'Vista de Rua', 'pt');
settext('$MAP_DOUBLE_CLICK_ZOOM', 'Zoom em duplo-click', 'pt');
settext('$MAP_ZOOM', 'Zoom Inicial', 'pt');
settext('$MAP_MAP_TYPE', 'Tipo de Mapa', 'pt');
settext('$MAP_LATITUDE', 'Latitude', 'pt');
settext('$MAP_LONGITUDE', 'Longitude', 'pt');
settext('$MAP_DRAGGABLE_MARKERS', 'Marcadores Mov\u00edveis', 'pt');
settext('$MAP_ISDEFAULT', 'Pr\u00e9-definido', 'pt');
settext('$MAP_DEFAULT_MARKER_ICON', 'Icon Pr\u00e9-definido', 'pt');

settext('$MAP_MARKER_LIST', 'Markers', 'pt');

settext('$MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'pt');
settext('$MAP_MARKER_LONGITUDE', 'Longitude', 'pt');
settext('$MAP_MARKER_LATITUDE', 'Latitude', 'pt');
settext('$MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'pt');
settext('$MAP_MARKER_ICON', 'Icon', 'pt');
settext('$MAP_MARKER_BALLON', 'Descri\u00e7\u00e3o do Bal\u00e3o', 'pt');

settext('$MAP_MAP_TYPE_HIBRID', 'H\u00edbrido', 'pt');
settext('$MAP_MAP_TYPE_ROADMAP', 'Mapa de estradas', 'pt');
settext('$MAP_MAP_TYPE_SATELLITE', 'Sat\u00e9lite', 'pt');
settext('$MAP_MAP_TYPE_TERRAIN', 'Terreno', 'pt');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACMSLocale_pt');

