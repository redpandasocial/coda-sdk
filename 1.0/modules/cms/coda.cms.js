KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODACMSShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.loadRetry = 0;
		
		this.addWindowResizeListener(CODACMSShell.resizeFrame);
		this.addLocationListener(CODACMSShell.actions);
		this.setStyle({
			backgroundColor : 'transparent'
		});
	}
}

KSystem.include([
	'KConfirmDialog',
	'KMessageDialog',
	'KButton',
	'CODAGenericShell'
], function() {
	CODACMSShell.prototype = new CODAGenericShell();
	CODACMSShell.prototype.constructor = CODACMSShell;
		
	CODACMSShell.prototype.loadShell = function() {
		CODA.ACTIVE_SHELL = this.app_data.channels.elements[0].document_id;
		this.kinky.get(this, 'rest:/widgetconfigs', {}, null, 'onLoadConfigs');
	};

	CODACMSShell.prototype.onLoadConfigs = function(data, request) {
		this.definedClasses = {};
		this.widgetConf = data;
		var elements = this.widgetConf.elements;
		this.widgetConf.elements = {};
		for ( var w in elements) {
			this.definedClasses[elements[w].type] = this.widgetConf.elements['widgetconfigs_' + elements[w].id] = elements[w];
		}
		KCache.commit('/widgetconfigs', this.widgetConf);
		this.kinky.get(this, 'rest:/widgetconfigs?type=KShell&parentType=KShell&boolop=OR', {}, null, 'onLoadShellConfig');
	};
	
	CODACMSShell.prototype.onLoadShellConfig = function(data, request) {
		this.shellConf = data;
		this.definedClasses[this.shellConf.elements[0].type] = this.shellConf.elements[0];
		KCache.commit('/shellconfigs', data);
		this.kinky.get(this, 'rest:/widgetconfigs?type=KShellPart&parentType=KShellPart&boolop=OR', {}, null, 'onLoadPagesConfig');
	};
	
	CODACMSShell.prototype.onLoadPagesConfig = function(data, request) {
		this.pageConf = data;
		KCache.commit('/pageconfigs', data);
		for ( var w in this.pageConf.elements) {
			this.definedClasses[this.pageConf.elements[w].type] = this.pageConf.elements[w];
		}
		KCache.commit('/definedclasses', this.definedClasses);
		this.kinky.post(this, 'rest:/layoutconfigs/get-config', {}, null, 'onLoadLayouts');
	};
	
	CODACMSShell.prototype.onLoadLayouts = function(data, request) {
		this.layoutConf = data;
		KCache.commit('/layoutconfigs', data);
		this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL.replace('docroots', 'website') + '/sitemap?flat=true', {}, null, 'onLoadPages');
	};
	
	CODACMSShell.prototype.onLoadPages = function(data, request) {
		var pageDef = {};
		for ( var i in data) {
			pageDef[data[i].hash] = data[i];
		}
		KCache.commit('/pagedefs', pageDef);
		this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL, {}, null, 'onLoadShell');
	};
	
	CODACMSShell.prototype.onLoadShell = function(data, request) {
		this.shellDef = data;
		//window.document.getElementById('siteDiv').src = '/modules/cms/proxy/' + Kinky.SHELL_CONFIG[this.shell].app_id + '/?wid=' + CODA.ACTIVE_SHELL.replace('/docroots/', '');
		window.document.getElementById('siteDiv').src = '/api/1.0/proxy-pass/http/' + Kinky.SHELL_CONFIG[this.shell].app_id + '/?wid=' + CODA.ACTIVE_SHELL.replace('/docroots/', '');
		KCache.commit('/shelldefs', data);
		this.draw();
	};
	
	CODACMSShell.prototype.onNoContent = function(data, request) {
		if (request.service.indexOf("/users/me") != -1) {
			this.loadShell()
			return;
		}
		else if (request.service.indexOf('/widgetconfigs') != -1) {
			this.widgetConf = (!!this.widgetConf ? this.widgetConf : {});
			this.kinky.post(this, 'rest:/layoutconfigs/get-config', {}, null, 'onLoadLayouts');
			return;
		}
		else if (request.service.indexOf('/layoutconfigs') != -1) {
			this.layoutConf = (!!this.layoutConf ? this.layoutConf : {});
			this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL.replace('docroots', 'website') + '/sitemap?flat=true', {}, null, 'onLoadPages');
			return;
		}
		else if (request.service.indexOf('/sitemap') != -1) {
			this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL, {}, null, 'onLoadShell');
			return;
		}
		this.draw();
	};

	CODACMSShell.prototype.onPublish = function(data, request) {
		this.hideOverlay(true);
		KMessageDialog.alert({
			message : gettext('$CODA_PUBLISHED_SUCCES'),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};


	CODACMSShell.prototype.onSiteRefresh = function(data, request) {
		this.kinky.put(this, 'rest:/page?docroot=' + CODA.ACTIVE_SHELL.replace('/docroots/', ''), {}, { 'X-Use-Work-Version' : 'true' }, 'onPageRefresh');
	};

	CODACMSShell.prototype.onPageRefresh = function(data, request) {
		CODACMSShell.refreshFrame();
	};

	CODACMSShell.prototype.onError = function(data, request) {
		/*KMessageDialog.alert({
			message : '(' + data.error_code + ') ' + data.message + (!!data.detail ? ':<br>' + JSON.stringify(data.detail) : ''),
			html : true,
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});*/
		this.shellDef = {
			title : '[NO SHELL]'
		};
		this.draw();
	};		

	CODACMSShell.prototype.onLoadVersions = function(data, request) {
		this.hideOverlay(true);
		var configForm = new CODAVersionsForm(Kinky.getDefaultShell(), data.versions);
		{
			configForm.hash = '/version/list';
		}
		this.makeWindow(configForm);
	};
	
	CODACMSShell.prototype.onRemove = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CONFIRM_WIDGET_DELETED_SUCCESS'),
			html : true,
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
		var pageDef = KCache.restore('/pagedefs');
		for ( var hash in pageDef) {
			if (pageDef[hash].href == request.service) {
				CODACMSShell.refreshFrame('');
				break;
			}
		}
	};
	
	CODACMSShell.prototype.draw = function() {
		this.setTitle(gettext('$CMS'));
		
		this.addToolbarSeparator('right');		

		var siteDetails = new KButton(this, '<i class="fa fa-globe"></i>' + gettext('$CODA_SHELL_EDIT_DOCROOT'), 'edit-docroot', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-full' + CODA.ACTIVE_SHELL
			});
		});
		this.addToolbarButton(siteDetails, 3, 'right');

		this.pageDetails = new KButton(this, '<i class="fa fa-pencil-square-o"></i>' + gettext('$CODA_SHELL_EDIT_CURRENT_PAGE'), 'edit-page', function(event) {
			KDOM.stopEvent(event);
			var button = KDOM.getEventWidget(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit' + button.pageHref
			});
		});
		this.addToolbarButton(this.pageDetails, 3, 'right');

		var pageList = new KButton(this, '<i class="fa fa-sitemap"></i>' + gettext('$CODA_SHELL_PAGE_LIST'), 'page-list', function(event) {
			KDOM.stopEvent(event);

			var pages = new CODAPageList(Kinky.getDefaultShell());
			{
				pages.hash = '/page/list';
			}
			
			var dialog = KFloatable.make(pages, {
				modal : true,
				width : 898,
				scale : {
					height : 0.9
				},
				buttons : 'close'
			});
			{
				dialog.addCSSClass('CODAPageListDialog');
			}
			KFloatable.show(dialog);
		});
		this.addToolbarButton(pageList, 3, 'right');
				
		var page = new KButton(this, '<i class="fa fa-file-o"></i>' + gettext('$CODA_SHELL_NEW_ELEMENT'), 'add-to', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/add-page'
			});
		});
		this.addToolbarButton(page, 1, 'right');
		
		var refresh = new KButton(this, gettext('$CODA_SHELL_REFRESH'), 'refresh', function(event) {
			KDOM.stopEvent(event);
			
			var button = KDOM.getEventWidget(event);
			button.addCSSClass('CODAFooterButton1Selected');

			button.kinky.put(button.getShell(), 'rest:' + CODA.ACTIVE_SHELL.replace('/docroots/', '/website/'), {}, { 'X-Use-Work-Version' : 'true' }, 'onSiteRefresh');
		});
		this.addBottomButton(refresh, 2);
		
		var revert = new KButton(this, gettext('$CODA_SHELL_REVERT'), 'revert', function(event) {
			KDOM.stopEvent(event);
			
			var scopes = KCache.restore('/admin-scopes');
			if (scopes.scopes.indexOf('admin') == -1 && scopes.scopes.indexOf('publish_versions') == -1) {
				KMessageDialog.alert({
					message : gettext('$CODA_PUBLISH_NO_PERMISSION'),
					shell : '/CODA',
					modal : true,
					width : 400,
					height : 300
				});
				return;
			}

			var button = KDOM.getEventWidget(event);
			button.addCSSClass('CODAFooterButton1Selected');
			button.getShell().showOverlay();
			button.kinky.get(button.parent, 'rest:/versions', {}, button.getShellConfig().headers, 'onLoadVersions');
		});
		this.addBottomButton(revert);

		var publish = new KButton(this, gettext('$CODA_SHELL_PUBLISH'), 'publish', function(event) {
			KDOM.stopEvent(event);
			
			var scopes = KCache.restore('/admin-scopes');
			if (scopes.scopes.indexOf('admin') == -1 && scopes.scopes.indexOf('publish_versions') == -1) {
				KMessageDialog.alert({
					message : gettext('$CODA_PUBLISH_NO_PERMISSION'),
					shell : '/CODA',
					modal : true,
					width : 400,
					height : 300
				});
				return;
			}

			var button = KDOM.getEventWidget(event);
			button.addCSSClass('CODAFooterButton1Selected');
			KConfirmDialog.confirm({
				question : gettext('$CODA_PUBLISH_CONFIRM'),
				callback : function() {
					button.getShell().showOverlay();
					var headers = button.getShellConfig().headers || {};
					button.getShell().kinky.post(button.getShell(), 'rest:/versions/publish', { docroot : CODA.ACTIVE_SHELL }, headers, 'onPublish');
				},
				shell : button.shell,
				modal : true,
				width : 400,
				height : 300
			});
		});
		this.addBottomButton(publish);
		
		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAShellContentBackground', this.content);				
	};
	
	CODACMSShell.refreshFrame = function(hash) {
		window.document.getElementById('siteDiv').contentDocument.location.reload();
		return;
		if ((hash == '') || !!hash) {
			var prefix = window.document.getElementById('siteDiv').contentDocument.location.href.split('#')[0];
			window.document.getElementById('siteDiv').contentDocument.location.href = prefix + (hash != '' ? '#' + hash : '');
			if (hash != '') {
				window.document.getElementById('siteDiv').contentDocument.location.reload();
			}
		}
		else {
		}
	};
	
	CODACMSShell.prototype.widgetOnLoadSchema = function(data, request) {
		if (!!request) {
			KCache.commit(request.service, data);
		}
		var uiClass = KSystem.getWRMLForm(data, 'coda.cms.');
		this.showWidgetForm(uiClass, this.current.href, this.current.minimized, this.current.parentHref);
	};
	
	CODACMSShell.prototype.widgetOnLoad = function(data, request) {
		this.current.targetData = data;
		var schemaName = KSystem.getWRMLSchema(data.data);
		this.current.schema = schemaName;
		this.current.requestType = KSystem.getWRMLClass(data.data);
		var schema = KCache.restore(schemaName);
		if (!!schema) {
			this.widgetOnLoadSchema(schema, null);
		}
		else {
			this.kinky.get(this, 'rest:' + schemaName, {}, null, 'widgetOnLoadSchema');
		}
	};

	CODACMSShell.prototype.onLoadWidget = function(params, request) {
		delete params.http;

		var callback = null;
		params.parent_root = CODA.ACTIVE_SHELL;
		var div = window.document.createElement('div');

		var data = CODACMSShell.SAVING_DATA;
		CODACMSShell.SAVING_DATA = null;
		eval('callback = ' + data.form + '.instantiate');

		for (var f in data.fields) {
			if (!!data.fields[f]) {
				div.innerHTML = data.fields[f];
				callback(params, f, div.childNodes[0]);
			}
		}

		var href = params.data.href || params.data.hash;
		this.kinky.put(this, 'rest:' + href, params, this.config.headers, 'onSaveWidget');
	};

	CODACMSShell.prototype.onSaveWidget = function(data, request) {
	};

	CODACMSShell.prototype.addWidget = function(parentHref) {
		this.current = {
			parentHref : parentHref
		};
		var screens = [ 'CODAWidgets' , 'CODAWidgetForm'];
		this.makeWizard(new CODANewWidgetWizard(this, screens, parentHref));	
	};

	CODACMSShell.prototype.addPage = function() {
		var screens = [ 'CODAPageForm'];
		var wiz = new CODAWizard(this, screens)
		wiz.setTitle(gettext('$NEW_PAGE_PROPERTIES'));
		wiz.hash = '/page/add';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			params.parent_root = CODA.ACTIVE_SHELL; 
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			if (request.service.indexOf("childwidgets") != -1) {
				window.document.location.reload(true);
			}
			else {
				this.kinky.put(this, 'rest:' + CODA.ACTIVE_SHELL + '/child' + data.http.headers["Location"].substring(1), {}, null);
			}
		};

		wiz.send = function(params) {	
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:/widgets', params);
				}
			}
			catch (e) {

			}
		};
		this.makeWizard(wiz);	
	};

	CODACMSShell.prototype.editWidget = function(href, minimized) {
		this.current = new Object();
		this.current.href = href;
		this.current.minimized = minimized;
		this.kinky.get(this, 'rest:' + href + '?fields=*&embed=children', {}, null, 'widgetOnLoad');
	};

	CODACMSShell.prototype.updateWidget = function(href, data) {
		CODACMSShell.SAVING_DATA = JSON.parse(Base64.decode(data));
		this.kinky.get(this, 'rest:' + href + '?fields=*', {}, this.config.headers, 'onLoadWidget');
	};

	CODACMSShell.prototype.removeWidget = function(href, widget) {
		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_WIDGET_CONFIRM'),
			callback : function() {
				if (!!widget) {
					widget.getParent('CODAForm').showMessage(gettext('$CODA_WORKING'), true);
					widget.kinky.remove(widget.getParent('CODAForm'), 'rest:' + href, {});
					widget.parent.removeChild(widget);
				}
				else {
					Kinky.getDefaultShell().kinky.remove(Kinky.getDefaultShell(), 'rest:' + href, {});
				}
			},
			shell : '/CODA',
			modal : true,
			width : 400
		});
		KBreadcrumb.dispatchURL({
			hash : ''
		});
	};

	CODACMSShell.prototype.moveWidget = function(parentHref, href) {
		parentHref = parentHref.replace('/page/', '/widgets/').replace('/website/', '/docroots/');
		var configForm = new CODAMoveWidgetForm(Kinky.getDefaultShell(), parentHref);
		{
			configForm.hash = '/widgets/move';
			configForm.minimized = true;
			configForm.childHref = href;
			configForm.dispatchState('minimized');
		}
		var dialog = KFloatable.make(configForm, {
			buttons : 'close,minimize',
			width : 470,
			modal : true,
			scale : {
				height : 0.7
			}
		});
		{
			dialog.closeMe = function() {
				configForm.onClose();
				KFloatable.prototype.closeMe.call(this);
			};
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAFloatablePropDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
			dialog.addCSSClass('CODAMoveWidgetFormDialog');
		}
		KFloatable.show(dialog);
	};

	CODACMSShell.prototype.showWidgetForm = function(uiClass, href, minimized, parentHref) {
		var configForm = null;
		eval('configForm = new ' + uiClass + '(Kinky.getShell(\'/CODA\'), href, null, parentHref)');
		{
			configForm.hash = (!!href ? href : '/new') + '/CODA/form';
			configForm.setData(Kinky.getDefaultShell().current.targetData);
			configForm.config.schema = Kinky.getDefaultShell().current.schema;
			configForm.config.requestType = Kinky.getDefaultShell().current.requestType;
			configForm.msg = Kinky.getDefaultShell().msg;
			Kinky.getDefaultShell().msg = null;
		}
		this.makeDialog(configForm, { minimized : minimized, modal : true});
	};
	
	CODACMSShell.showWidgets = function(addTo) {
		var configForm = new CODAWidgets(Kinky.getDefaultShell(), Kinky.getDefaultShell().widgetConf, addTo);
		{
			configForm.hash = '/widgets/selection';
		}
		
		var dialog = KFloatable.make(configForm, {
			buttons : 'close',
			modal : true,
			width : 800,
			scale : {
				height : 0.7
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAAddWidgetsDialog');
		}
		KFloatable.show(dialog);
	};

	CODACMSShell.prototype.visible = function() {
		CODAGenericShell.prototype.visible.call(this);
		var iframe = window.document.getElementById('siteDiv');

		this.panel.insertBefore(iframe, this.footer.panel);

		var gw = KDOM.getBrowserHeight();
		var fp = gw / 990 * 50;
		var fs = Math.round(fp);
		iframe.style.marginTop = (41 + fs) + 'px';
		iframe.style.height = (KDOM.getBrowserHeight() - 61 - fs) + 'px';

		KSystem.addTimer(function() {
			KCSS.setStyle({
				opacity : '1'
			}, [
			window.document.getElementById('siteDiv')
			]);
		}, 250);
	};

	CODACMSShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		switch (parts[1]) {
			case 'edit-new': {
				Kinky.getDefaultShell().msg = gettext('$CONFIRM_WIDGET_CREATED_SUCCESS');
				KBreadcrumb.dispatchURL({
					hash : hash.replace('-new', '')
				});
				break;
			}
			case 'edit': {
				Kinky.getDefaultShell().editWidget('/' + parts[2] + '/' + parts[3], true);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-full': {
				Kinky.getDefaultShell().editWidget('/' + parts[2] + '/' + parts[3], false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-to': {
				Kinky.getDefaultShell().addWidget('/' + parts[2] + '/' + parts[3]);
				break;
			}
			case 'add-page': {
				Kinky.getDefaultShell().addPage();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'remove': {
				Kinky.getDefaultShell().removeWidget('/' + parts[2] + '/' + parts[3]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'move': {
				Kinky.getDefaultShell().moveWidget('/' + parts[2] + '/' + parts[3], '/' + parts[4] + '/' + parts[5]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'save': {
				Kinky.getDefaultShell().updateWidget('/' + parts[2] + '/' + parts[3], parts[5]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};
	
	CODACMSShell.LEFT_FRAME_MARGIN = 75;

	KSystem.included('CODACMSShell');
}, Kinky.CODA_INCLUDER_URL);
function CODADataFeed(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = (!!data ? data : { data : {}, feedsConfig : {}});
		this.data.feedsConfig = this.data.feedsConfig || {};
		this.data.data = this.data.data || {};
		this.no_children = true;
		this.providers = [
		{
			"id": "local://",
			"label": "Local"
		},
		{
			"id": "https://graph.facebook.com/",
			"label": "Facebook"
		},
		{
			"id": "http://api.twitter.com/1/",
			"label": "Twitter"
		},
		{
			"id": "https://www.googleapis.com/plus/v1/",
			"label": "Google+"
		},
		{
			"id": "https://pinterest.com",
			"label": "Pinterest"
		}
		]	
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODADataFeed.prototype = new KPanel();
	CODADataFeed.prototype.constructor = CODADataFeed;

	CODADataFeed.prototype.go = function() {
		this.draw();
	};

	CODADataFeed.prototype.draw = function() {
		var f = new KInput(this, gettext('$FEED_FEEDS'), 'data.feeds');
		{
			if (!!this.data.data.feeds) {
				f.setValue(this.data.data.feeds);
			}
		}
		this.appendChild(f);

		var f = new KInput(this, gettext('$FEED_OAUTH_ACCESSTOKEN'), 'feedsConfig.accessToken');
		{
			if (!!this.data.feedsConfig.accessToken) {
				f.setValue(this.data.feedsConfig.accessToken);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$FEED_OAUTH_CLIENTID'), 'feedsConfig.clientid');
		{
			if (!!this.data.feedsConfig.clientid) {
				f.setValue(this.data.feedsConfig.clientid);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$FEED_OAUTH_CLIENTSECRET'), 'feedsConfig.clientsecret');
		{
			if (!!this.data.feedsConfig.clientsecret) {
				f.setValue(this.data.feedsConfig.clientsecret);
			}
		}
		this.appendChild(f);

		f = new KCombo(this, gettext('$FEED_OAUTH_PROVIDER'), 'feedsConfig.provider');
		{

			for ( var o in this.providers) {
				f.addOption(this.providers[o].id, this.providers[o].label, !!this.data.feedsConfig && !!this.data.feedsConfig.provider && (this.data.feedsConfig.provider == this.providers[o].id));
			}
		}
		this.appendChild(f);

		KPanel.prototype.draw.call(this);
	};	

	KSystem.included('CODADataFeed');
}, Kinky.CODA_INCLUDER_URL);

/*	
accessToken	Text	$FEED_OAUTH_ACCESSTOKEN						
clientid	Text	$FEED_OAUTH_CLIENTID						
clientsecret	Text	$FEED_OAUTH_CLIENTSECRET						
provider	Text	$FEED_OAUTH_PROVIDER			

*/

function CODADocrootForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/docroots';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODADocrootForm.prototype = new CODAWidgetForm();
	CODADocrootForm.prototype.constructor = CODADocrootForm;
	
	CODADocrootForm.prototype.draw = function() {
		CODAWidgetForm.prototype.draw.call(this);
	};
	
	CODADocrootForm.prototype.addMinimizedPanel = CODADocrootForm.prototype.addPanels = CODADocrootForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		general.hash = '/general';
		{
			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);
			
			var description = new KRichText(general, gettext('$TEXT_LEAD'), 'data.description');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);

			if (!wizard && !!this.target.data && !!this.target.data.data) {
				var pageType = new KCombo(general, gettext('$LINK_REQUESTTYPES') + '*', 'data.responseTypes');
				var widgetConf = Kinky.getDefaultShell().shellConf.elements;
				for ( var wc in widgetConf) {
					var types = ['application/json','application/wrml;schema="/wrml-schemas/widgets/docroot-resource"'];
					var wcType = 'application/vnd.kinky.' + widgetConf[wc].type;

					types.push(wcType);
					if (!!widgetConf[wc].template) {
						types.push('text/html;template=' + !!widgetConf[wc].template);
					}
					pageType.addOption(types, gettext(widgetConf[wc].description), this.config.requestType == widgetConf[wc].type);
				}
				pageType.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				general.appendChild(pageType);
			}			
		}
		
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};
	
	CODADocrootForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODADocrootForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAFeedbackForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAFeedbackForm.prototype = new CODAWidgetForm();
	CODAFeedbackForm.prototype.constructor = CODAFeedbackForm;

	CODAFeedbackForm.prototype.addMinimizedPanel = CODAFeedbackForm.prototype.addPanels = CODAFeedbackForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);

			var types = new KCheckBox(general, gettext('$KFEEDBACK_TYPES') + '*', 'data.types');
			{
				var options = [
					{
						"id" : "comments",
						"label" : "$KFEEDBACK_COMMENT_TYPE"
					},
					{
						"id" : "votes",
						"label" : "$KFEEDBACK_VOTE_TYPE"
					},
					{
						"id" : "reviews",
						"label" : "$KFEEDBACK_REVIEW_TYPE"
					}
				];

				for ( var o in options) {
					var selected = false;
					if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.types) {
						for ( var t in this.target.data.data.types) {
							if (this.target.data.data.types[t] == options[o].id) {
								selected = true;
								break;
							}
						}
					}
					types.addOption(options[o].id, gettext(options[o].label), selected);
				}

				types.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(types);

			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				description.height = 150;
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);

		}
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAFeedbackForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};


	KSystem.included('CODAFeedbackForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAFeedForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm',
	'KImageUpload'
], function() {
	CODAFeedForm.prototype = new CODAWidgetForm();
	CODAFeedForm.prototype.constructor = CODAFeedForm;
	
	CODAFeedForm.prototype.addPanels = CODAFeedForm.prototype.addMinimizedPanel = CODAFeedForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';
			
			var feedURL = new KInput(general, gettext('$FEED_FEEDS') + '*', 'data.feeds');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.feeds) {
					feedURL.setValue(this.target.data.data.feeds);
				}
				feedURL.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(feedURL);
			
			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);
			
			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				description.height = 150;
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);
			
			var perPage = new KInput(general, gettext('$LIST_WIDGET_PAGESIZE') + '*', 'data.pageSize');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.pageSize) {
					perPage.setValue(this.target.data.data.pageSize);
				}
				perPage.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(perPage);
			
			var autoRotate = new KInput(general, gettext('$CONFIG_LIST_AUTO_ROTATE') + '*', 'data.autoRotate');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.autoRotate) {
					autoRotate.setValue(this.target.data.data.autoRotate);
				}
				else {
					autoRotate.setValue('0');
				}
				autoRotate.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(autoRotate);
			
		}

		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAFeedForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};

	
	KSystem.included('CODAFeedForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAGraphicalAttributes(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = (!!data ? data : {});
		this.nolayouts = false;
		this.layoutconfigs = [];
		this.no_children = true;
	}
}

KSystem.include([
	'KPanel',
	'KCombo',
	'KImageUpload',
	'CODAMediaList',
	'CODALayouts'
], function() {
	CODAGraphicalAttributes.prototype = new KPanel();
	CODAGraphicalAttributes.prototype.constructor = CODAGraphicalAttributes;
	
	CODAGraphicalAttributes.prototype.onError = CODAGraphicalAttributes.prototype.onNotFound = CODAGraphicalAttributes.prototype.onNoContent = function(data, request) {
		if (request.service.indexOf('widgetconfigs') != -1 || request.service.indexOf('/parent') != -1) {			
			this.layoutconfigs = [];
			this.draw();
		}
		else {
			KPanel.prototype.onNoContent.call(this, data, request);
		}
	};
	
	CODAGraphicalAttributes.prototype.load = function() {
		this.kinky.get(this, 'rest:' + this.target.href + '/parent?embed=children&fields=children(elements)', {}, null, 'onLoadParent');
	};
	
	CODAGraphicalAttributes.prototype.onLoadParent = function(data, request) {
		this.layoutconfigs = [];
		if (!!data.children && !!data.children.elements) {
			var sent = false;
			for (var c in data.children.elements) {
				var cl = KSystem.getWRMLClass(data.children.elements[c]);
				if (cl == "KLayout") {
					sent = true;
					this.kinky.get(this, 'rest:' + data.children.elements[c].hash.replace(/_/g, '/'), {}, null, 'onLoadLayouts');
					break;
				}
			}
			if (!sent) {
				this.draw();
			}
		}
		else {
			this.draw();
		}
	};
	
	CODAGraphicalAttributes.prototype.onLoadLayouts = function(data, request) {
		var layouts = KCache.restore('/layoutconfigs');
		if (!!data.css_file) {
			this.setTargets(layouts[data.css_file], 0);
		}
		this.draw();
	};

	CODAGraphicalAttributes.prototype.setTargets = function(layout, level) {
		var prefix = "";

		for (var i = 0; i != level; i++) {
			prefix += '\u00a0\u00a0\u00a0\u00a0\u00a0';
		}

		this.layoutconfigs.push({ label : layout.css_file || layout.name, description : prefix + layout.description });
		for (var t in layout.targets) {
			this.setTargets(layout.targets[t], level + 1);
		}
	};

	CODAGraphicalAttributes.prototype.draw = function() {
		this.cssclass = new KInput(this, gettext('$MENU_CLASS'), 'data.class');
		{
			if (!!this.data && !!this.data.data && !!this.data.data["class"]) {
				this.cssclass.setValue(this.data.data["class"]);
			}
		}
		this.appendChild(this.cssclass);
		
		this.backgroundImage = new KImageUpload(this, gettext('$WIDGET_GRAPHICS_BACKGROUND'), 'data.graphics.background', CODAGenericShell.getUploadAuthorization());
		{
			var mediaList = new CODAMediaList(this.backgroundImage, 'image/*');
			this.backgroundImage.appendChild(mediaList);
			if (!!this.data && !!this.data.data && !!this.data.data.graphics && !!this.data.data.graphics.background) {
				this.backgroundImage.setValue(this.data.data.graphics.background);
			}
		}
		this.appendChild(this.backgroundImage);

		if (this.layoutconfigs.length != 0) {
			var target = new KRadioButton(this, gettext('$LINK_TARGET_CHILD_CONTAINER'), 'data.target');
			{
				for (var l in this.layoutconfigs) {
					target.addOption(this.layoutconfigs[l].label, gettext(this.layoutconfigs[l].description), !!this.data && !!this.data.data && (this.data.data.target == this.layoutconfigs[l].label));
				}
			}
			target.setStyle({
				padding: '5px'
			}, KRadioButton.OPTION_CONTAINER_DIV);
			this.appendChild(target);
		}

		if (this.nolayouts == false) {
			var layoutData = null;
			if (this.data.children) {
				for ( var c in this.data.children.elements) {
					var type = KSystem.getWRMLClass(this.data.children.elements[c]);
					if (type == 'KLayout') {
						layoutData = this.data.children.elements[c];
						break;
					}
				}
			}
			
			var uiclass = KSystem.getWRMLClass(this.data.data);
			var layoutFields = new CODALayouts(this, uiclass, layoutData);
			{
				layoutFields.hash = '/layouts';
			}
			this.appendChild(layoutFields);
		}
		
		KPanel.prototype.draw.call(this);
	};


	CODAGraphicalAttributes.addWizardPanel = function(parent) {
		var appPanel = new KPanel(parent);
		{

			var cssclass = new KInput(appPanel, gettext('$MENU_CLASS'), 'data.class');
			appPanel.appendChild(cssclass);

			var backgroundImage = new KImageUpload(appPanel, gettext('$WIDGET_GRAPHICS_BACKGROUND'), 'data.graphics.background', CODAGenericShell.getUploadAuthorization());
			{
				var mediaList = new CODAMediaList(backgroundImage, 'image/*');
				backgroundImage.appendChild(mediaList);
			}
			appPanel.appendChild(backgroundImage);

			if (false) {
				var target = new KRadioButton(appPanel, gettext('$LINK_TARGET_CHILD_CONTAINER'), 'data.target');
				{
					for (var l in this.layoutconfigs) {
						target.addOption(this.layoutconfigs[l].label, gettext(this.layoutconfigs[l].description), false);
					}
				}
				target.setStyle({
					padding: '5px'
				}, KRadioButton.OPTION_CONTAINER_DIV);
				appPanel.appendChild(target);
			}

			var layoutFields = new CODALayouts(appPanel);
			{
				layoutFields.hash = '/layouts';
			}
			appPanel.appendChild(layoutFields);
		}

		return appPanel;
	};
	KSystem.included('CODAGraphicalAttributes');
}, Kinky.CODA_INCLUDER_URL);
function CODAHighlightActionsForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.postData = null;
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAHighlightActionsForm.prototype = new CODAWidgetForm();
	CODAHighlightActionsForm.prototype.constructor = CODAHighlightActionsForm;

	CODAHighlightActionsForm.prototype.addMinimizedPanel = CODAHighlightActionsForm.prototype.addPanels = function() {

		var itemData = null;
		if (!!this.target.data && !!this.target.data.data) {
			try {
				eval("itemData = this.target.data." + this.dataPath);
			}catch (e) {
			}
		}

		// panel com defini��es do menu
		var general = new KPanel(this);
		{
			general.hash = '/general';

			var legend = new KInput(this, gettext('$LINK_TITLE') + '*', this.dataPath + '.title');
			{
				if (!!itemData && itemData.title) {
					legend.setValue(itemData.title);
				}
				legend.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(legend);

			var url = new KInput(this, gettext('$LINK_URL') + '*', this.dataPath + '.hash');
			{
				if (!!itemData && itemData.hash) {
					url.setValue(itemData.hash);
				}
				url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(url);

			var target = new KCombo(this, gettext('$LINK_TARGET_WINDOW') + '*', this.dataPath + '.targetWindow');
			{
				target.addOption('_self', gettext('$LINK_TARGET_SAME'), !!itemData && (itemData.targetWindow == '_self'));
				target.addOption('_blank', gettext('$LINK_TARGET_OTHER'), !!itemData && (itemData.targetWindow == '_blank'));
				target.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(target);
		}

		this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
	};

	CODAHighlightActionsForm.prototype.minimized_draw = function() {
		this.addCSSClass('CODAFormMinimized');
		this.addCSSClass('CODAFormContentMinimized', this.content);
		this.addCSSClass('CODAFormMessagesMinimized', this.messages);
		this.minimized = true;

		this.setTitle((!!this.target.href ? '' : gettext('$CODA_SHELL_ADD_PREFIX')) + gettext('$' + this.config.requestType.toUpperCase()) + (!!this.target.href && !!this.target.data.data && !!this.target.data.data.title ? ' \u00ab' + this.target.data.data.title + '\u00bb' : ''));

		this.setStyle({
			height : (Kinky.getWidget(this.CODA_mainForm).getHeight() - 100) + 'px'
		});

		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(go);

		this.addMinimizedPanel();

		KLayeredPanel.prototype.draw.call(this);
	};

	CODAHighlightActionsForm.prototype.instantiateDefaults = function(params) {
		var list = this.parentWidget;

		list.indexedElements['/item/' + list.indexedElementsLastIndex] = params.data.actions[params.data.actions.length - 1];
		list.indexedElementsCount++;
		list.indexedElementsLastIndex++;

		var dataToUpdate = [];
		for (var index in list.indexedElements){
			dataToUpdate.push(list.indexedElements[index]);
		}
		params.data.actions = dataToUpdate;
		list.data = dataToUpdate;
	};

	CODAHighlightActionsForm.prototype.onPut = function() {
		var list = this.parentWidget;
		if (!!list && list.indexedElementsCount > 0) {
			var newItemData = list.indexedElements['/item/' + (list.indexedElementsLastIndex - 1)];

			var listItem = new CODAHighlightActionsListItem(list.list);

			listItem.hash = '/item/' + (list.indexedElementsLastIndex - 1);
			listItem.data = newItemData;
			listItem.dataPath = 'data.actions[' + (list.indexedElementsLastIndex - 1) + ']';
			list.list.appendChild(listItem);
			if(list.indexedElementsCount == 1){
				list.list.pageVisible(list.list.page);
				list.noItems.style.display = 'none';
			}
		}
		this.parent.closeMe();
	};

	KSystem.included('CODAHighlightActionsForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAHighlightActionsListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox',
	'CODAHighlightActionsForm'
], function() {
	CODAHighlightActionsListItem.prototype = new CODAListItem();
	CODAHighlightActionsListItem.prototype.constructor = CODAHighlightActionsListItem;

	CODAHighlightActionsListItem.prototype.remove = function(href, listItem) {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_WIDGET_CONFIRM'),
			callback : function() {

				if (!widget.parent.parent.indexedElements[widget.hash]){
					return;
				}
				delete(widget.parent.parent.indexedElements[widget.hash]);
				widget.parent.parent.indexedElementsCount--;

				var data = widget.getParent('CODAForm').target.data;
				var dataToUpdate = [];

				for (var index in widget.parent.parent.indexedElements){
					dataToUpdate.push(widget.parent.parent.indexedElements[index]);
				}
				data.data.actions = dataToUpdate;
				widget.parent.parent.data = dataToUpdate;
				widget.kinky.put(widget, 'rest:' + widget.getParent('CODAForm').target.href, data, null);
			},
			shell : '/CODA',
			modal : true,
			width : 400
		});
	};

	CODAHighlightActionsListItem.prototype.onPut = function() {
		if (this.parent.parent.indexedElementsCount == 0){
			this.parent.parent.noItems.style.display = 'block';
//			this.parent.pages['page0'].parentNode.removeChild(this.parent.pages['page0']);
		}else{
			this.parent.parent.noItems.style.display = 'none';
		}
		this.parent.removeChild(this);
	};

	CODAHighlightActionsListItem.prototype.showDetails = function() {
		var href = this.getParent('CODAForm').config.href;
		var actionItemForm = new CODAHighlightActionsForm(this.getShell(), href, null, href);
		actionItemForm.hash = (!!href ? href : '/new') + '/CODA/form';
		actionItemForm.dataPath = this.dataPath;
		actionItemForm.parentWidget = this;
		actionItemForm.dispatchState('minimized');
		actionItemForm.CODA_mainForm = this.getParent('CODAHighlightForm').id;

		var dialog = KFloatable.make(actionItemForm, {
			width : 405,
			left : Kinky.getWidget(actionItemForm.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(actionItemForm.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}
		KFloatable.show(dialog);
	};

	KSystem.included('CODAHighlightActionsListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAHighlightActionsList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema);
	}
}

KSystem.include([
	'CODAList',
	'CODAHighlightActionsListItem',
	'CODAHighlightActionsForm'
], function() {
	CODAHighlightActionsList.prototype = new CODAList();
	CODAHighlightActionsList.prototype.constructor = CODAHighlightActionsList;

	CODAHighlightActionsList.prototype.save = function() {
		var params = {};
		if (!!this.data && this.data.length > 0){
			params = {
				'data.actions' : this.data
			};
		}
		return params;
	};

	CODAHighlightActionsList.prototype.addItems = function(list) {
		this.indexedElements = {};
		for ( var index in this.elements) {
			var listItem = new CODAHighlightActionsListItem(list);

			listItem.hash = '/item/' + index;
			listItem.data = this.elements[index];
			listItem.dataPath = 'data.actions[' + index + ']';
			list.appendChild(listItem);
			this.indexedElements[listItem.hash] = this.elements[index];
		}
		this.indexedElementsCount = this.elements.length;
		this.indexedElementsLastIndex = this.elements.length;
	};

	CODAHighlightActionsList.prototype.addItem = function() {

		var href = this.getParent('CODAForm').config.href;
		var actionItemForm = new CODAHighlightActionsForm(this.getShell(), href, null, href);
		actionItemForm.hash = (!!href ? href : '/new') + '/CODA/form';
		actionItemForm.dataPath = 'data.actions[' + this.indexedElementsLastIndex + ']';
		actionItemForm.dispatchState('minimized');
		actionItemForm.parentWidget = this;
		actionItemForm.CODA_mainForm = this.getParent('CODAHighlightForm').id;
		var dialog = KFloatable.make(actionItemForm, {
			width : 405,
			left : Kinky.getWidget(actionItemForm.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(actionItemForm.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}
		KFloatable.show(dialog);
	};

	KSystem.included('CODAHighlightActionsList');
}, Kinky.CODA_INCLUDER_URL);

function CODAHighlightForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm',
	'KImageUpload',
	'CODAMediaList',
	'CODAHighlightActionsList'
], function() {
	CODAHighlightForm.prototype = new CODAWidgetForm();
	CODAHighlightForm.prototype.constructor = CODAHighlightForm;
	
	CODAHighlightForm.prototype.addMinimizedPanel = CODAHighlightForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';
			
			var legend = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					legend.setValue(this.target.data.data.title);
				}
				legend.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(legend);
			
			var url = new KInput(general, gettext('$LINK_URL') + '*', 'data.url');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.url) {
					url.setValue(this.target.data.data.url);
				}
				url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(url);
			
			var target = new KCombo(general, gettext('$LINK_TARGET_WINDOW') + '*', 'data.targetWindow');
			{
				target.addOption('_self', gettext('$LINK_TARGET_SAME'), !wizard && !!this.target.data && !!this.target.data.data && (this.target.data.data.targetWindow == '_self'));
				target.addOption('_blank', gettext('$LINK_TARGET_OTHER'), !wizard && !!this.target.data && !!this.target.data.data && (this.target.data.data.targetWindow == '_blank'));
				target.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(target);
			
			var image = new KImageUpload(general, gettext('$IMAGE_URL'), 'data.image', CODAGenericShell.getUploadAuthorization());
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.image) {
					image.setValue(this.target.data.data.image);
				}
			}
			var mediaList = new CODAMediaList(image, 'image/*');
			image.appendChild(mediaList);
			general.appendChild(image);

			var body = null;
			if (!wizard) {
				body = new KRichText(general, gettext('$TEXT_BODY') + '*', 'data.description');
			}
			else {
				body = new KTextArea(general, gettext('$TEXT_BODY') + '*', 'data.description');
			}
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					body.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(body);
		}
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};
	
	CODAHighlightForm.prototype.addPanels = function() {
		this.addMinimizedPanel();
		
		if (!!this.target.data && !!this.target.data.data) {
			var elements = [];
			if (!!this.target.data && !!this.target.data.data && !!this.target.data.data.actions) {
				if (!(this.target.data.data.actions instanceof Array)) {
					this.target.data.data.actions = [
						this.target.data.data.actions
					];
				}
				for ( var c in this.target.data.data.actions) {
					elements.push({
						title : this.target.data.data.actions[c].title,
						description : '<b style="text-transform: uppercase;">' + this.target.data.data.actions[c].title + '</b><br>' + this.target.data.data.actions[c].hash,
						id : this.target.data.data.actions[c].hash,
						href : this.target.data.data.actions[c].hash
					});
				}
			}
			var panel = new CODAHighlightActionsList(this);
			panel.hash = '/links-actions';
			panel.elements = elements;
			this.addPanel(panel, gettext('$KHIGHLIGHT_ACTION_LIST'), this.nPanels == 0);
		}
	};
	
	CODAHighlightForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'link' : {
				CODAForm.createObject(params, 'data.url');
				params.data.url = node.href;
				break;
			}
			case 'text' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
			case 'image' : {
				CODAForm.createObject(params, 'data.image');
				params.data.image = node.src;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODAHighlightForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAImageForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm',
	'KImageUpload',
	'CODAMediaList'
], function() {
	CODAImageForm.prototype = new CODAWidgetForm();
	CODAImageForm.prototype.constructor = CODAImageForm;

	CODAImageForm.prototype.addMinimizedPanel = CODAImageForm.prototype.addPanels = CODAImageForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var image = new KImageUpload(general, gettext('$IMAGE_URL') + '*', 'data.image', CODAGenericShell.getUploadAuthorization());
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.image) {
					image.setValue(this.target.data.data.image);
				}
				image.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			var mediaList = new CODAMediaList(image, 'image/*');
			image.appendChild(mediaList);
			general.appendChild(image);

			var imageScaled = new KImageUpload(general, gettext('$IMAGE_SCALED_URL'), 'data.imageScaled', CODAGenericShell.getUploadAuthorization());
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.imageScaled) {
					imageScaled.setValue(this.target.data.data.imageScaled);
				}
			}
			var mediaListScaled = new CODAMediaList(imageScaled, 'image/*');
			imageScaled.appendChild(mediaListScaled);
			general.appendChild(imageScaled);

			var legend = new KInput(general, gettext('$IMAGE_TITLE'), 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					legend.setValue(this.target.data.data.title);
				}
			}
			general.appendChild(legend);
		}
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAImageForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'image' : {
				CODAForm.createObject(params, 'data.image');
				params.data.image = node.src;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODAImageForm');
}, Kinky.CODA_INCLUDER_URL);
function CODALayouts(parent, jsClass, layoutData) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.config = {
			href : '/widgetconfigs?type=' + jsClass
		};
		this.data = layoutData;
		this.jsClass = jsClass;
	}
}

KSystem.include([
	'KInput',
	'KCheckBox',
	'KRadioButton',
	'KTextArea',
	'KCombo',
	'KSelect',
	'KPanel',
	'KButton',
	'KHidden',
	'KStatic',
	'KJSONText',
	'KFileDropPanel'
], function() {
	CODALayouts.prototype = new KPanel();
	CODALayouts.prototype.constructor = CODALayouts;

	CODALayouts.prototype.onPreConditionFailed = CODALayouts.prototype.onNoContent = function(data, request) {
		this.abort();
	};

	CODALayouts.prototype.onLoad = function(data, request) {
		var name = null;
		for (name in data.elements) {
			if (data.elements[name].type == this.jsClass) {
				break;
			}
		}
		var headers = null;
		if (!!Kinky.getShell(this.parent.targetShell).config.http && !!Kinky.getShell(this.parent.targetShell).config.http.headers && !!Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']
			};
		}
		this.kinky.get(this, 'rest:/widgetconfigs/' + data.elements[name].id + '/layoutconfigs', {}, headers, 'onLoadLayoutConfigs');
	};

	CODALayouts.prototype.onLoadLayoutConfigs = function(data, request) {
		this.layoutConf = data.elements;
		var headers = null;
		if (!!Kinky.getShell(this.parent.targetShell).config.http && !!Kinky.getShell(this.parent.targetShell).config.http.headers && !!Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']
			};
		}
		this.kinky.post(this, 'rest:/layoutconfigs/get-config', {}, headers, 'onLoadLayouts');
	};

	CODALayouts.prototype.onLoadLayouts = function(data, request) {
		this.uiConf = [];
		for ( var layout in data) {
			if (!!data[layout].hash && !!this.layoutConf[data[layout].hash.substr(1)]) {
				this.uiConf.push(data[layout]);
			}
		}
		this.draw();
	};

	CODALayouts.prototype.draw = function() {
		var layout = new KRadioButton(this, gettext('$LAYOUT'), 'layout');
		{
			layout.hash = '/layouts';
			for ( var l in this.uiConf) {
				layout.addOption(this.uiConf[l], gettext(this.uiConf[l].description), (!!this.data && (this.data.hash == this.uiConf[l].hash)));
			}
		}
		this.appendChild(layout);
		KPanel.prototype.draw.call(this);
	};

	CODALayouts.prototype.onChildAdded = function(data, request) {
	};

	CODALayouts.prototype.onPut = CODALayouts.prototype.onCreated = CODALayouts.prototype.onPost = function(data, request) {
		var href = null;
		var config = null;
		for ( var link in data.links) {
			href = link;
			config = data.links[link];
			break;
		}
		var headers = null;
		if (!!Kinky.getShell(this.parent.targetShell).config.http && !!Kinky.getShell(this.parent.targetShell).config.http.headers && !!Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']
			};
		}

		this.kinky.put(this, 'rest:' + this.parent.target.href + '/child' + href.substr(1), config, headers, 'onChildAdded');
	};

	CODALayouts.prototype.sendRequest = function() {
		if (!this.childWidget('/layouts')) {
			return;
		}
		var layout = this.childWidget('/layouts').getValue();
		if (layout != '') {
			if (!!this.data) {
				this.kinky.put(this, 'rest:' + this.data.href, {
					parent_root : CODA.ACTIVE_SHELL,
					data : layout
				});
			}
			else {
				this.kinky.post(this, 'rest:/widgets', {
					parent_root : CODA.ACTIVE_SHELL,
					data : layout
				});
			}
		}
		else {
			if (!!this.data && !!this.data.href) {
				this.kinky.remove(this, 'rest:' + this.data.href, {});
			}
			return;
		}
	};

	KSystem.included('CODALayouts');
}, Kinky.CODA_INCLUDER_URL);

function CODALinkForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm',
	'KImageUpload',
	'CODAMediaList'
], function() {
	CODALinkForm.prototype = new CODAWidgetForm();
	CODALinkForm.prototype.constructor = CODALinkForm;
	
	 CODALinkForm.prototype.addMinimizedPanel = CODALinkForm.prototype.addPanels = CODALinkForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';
			
			var legend = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					legend.setValue(this.target.data.data.title);
				}
				legend.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(legend);
			
			var url = new KInput(general, gettext('$LINK_URL') + '*', 'data.url');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.url) {
					url.setValue(this.target.data.data.url);
				}
				url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(url);
			
			var target = new KCombo(general, gettext('$LINK_TARGET_WINDOW') + '*', 'data.targetWindow');
			{
				target.addOption('_self', gettext('$LINK_TARGET_SAME'), !wizard && !!this.target.data && !!this.target.data.data && (this.target.data.data.targetWindow == '_self'));
				target.addOption('_blank', gettext('$LINK_TARGET_OTHER'), !wizard && !!this.target.data && !!this.target.data.data && (this.target.data.data.targetWindow == '_blank'));
				target.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(target);
			
			var image = new KImageUpload(general, gettext('$IMAGE_URL'), 'data.image', CODAGenericShell.getUploadAuthorization());
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.image) {
					image.setValue(this.target.data.data.image);
				}
			}
			var mediaList = new CODAMediaList(image, 'image/*');
			image.appendChild(mediaList);
			general.appendChild(image);
		}

		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODALinkForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'link' : {
				CODAForm.createObject(params, 'data.url');
				params.data.url = node.href;
				break;
			}
			case 'text' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
			case 'image' : {
				CODAForm.createObject(params, 'data.image');
				params.data.image = node.src;
				break;
			}
		}
		return params;
	};
	
	KSystem.included('CODALinkForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAListForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAListForm.prototype = new CODAWidgetForm();
	CODAListForm.prototype.constructor = CODAListForm;

	CODAListForm.prototype.addMinimizedPanel = CODAListForm.prototype.addPanels = CODAListForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);

			var perPage = new KInput(general, gettext('$LIST_WIDGET_PAGESIZE') + '*', 'data.pageSize');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.pageSize) {
					perPage.setValue(this.target.data.data.pageSize);
				}
				perPage.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(perPage);

			var autoRotate = new KInput(general, gettext('$CONFIG_LIST_AUTO_ROTATE') + '*', 'data.autoRotate');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.autoRotate) {
					autoRotate.setValue(this.target.data.data.autoRotate);
				}
				else {
					autoRotate.setValue('0');
				}
				autoRotate.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(autoRotate);

			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);

		}
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAListForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODAListForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMapForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm',
	'CODAMapMarkersList'
], function() {
	CODAMapForm.prototype = new CODAWidgetForm();
	CODAMapForm.prototype.constructor = CODAMapForm;

	CODAMapForm.prototype.addMinimizedPanel = CODAMapForm.prototype.addPanels = CODAMapForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);

			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);

			var latitude = new KInput(general, gettext('$MAP_LATITUDE') + '*', 'data.latitude');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.latitude) {
					latitude.setValue(this.target.data.data.latitude);
				}
				latitude.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(latitude);

			var longitude = new KInput(general, gettext('$MAP_LONGITUDE') + '*', 'data.longitude');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.longitude) {
					longitude.setValue(this.target.data.data.longitude);
				}
				longitude.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(longitude);

			var options = [{"id":"google.maps.MapTypeId.HYBRID","label":"$MAP_MAP_TYPE_HIBRID"},{"id":"google.maps.MapTypeId.ROADMAP","label":"$MAP_MAP_TYPE_ROADMAP"},{"id":"google.maps.MapTypeId.SATELLITE","label":"$MAP_MAP_TYPE_SATELLITE"},{"id":"google.maps.MapTypeId.TERRAIN","label":"$MAP_MAP_TYPE_TERRAIN"}];
			var mapType = new KCombo(general, gettext('$MAP_MAP_TYPE'), 'data.mapType');

			var selected = null;

			if(!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.mapType){
				selected = this.target.data.data.mapType;
			}
			else{
				selected = 'google.maps.MapTypeId.ROADMAP';
			}

			for (var index in options){
				mapType.addOption(options[index].id, gettext(options[index].label), (selected == options[index].id ? true : false));
			}
			general.appendChild(mapType);

			var initialZoom = new KInput(general, gettext('$MAP_ZOOM') + '*', 'data.initialZoom');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.initialZoom && this.target.data.data.initialZoom != '') {
					initialZoom.setValue(this.target.data.data.initialZoom);
				}else{
					initialZoom.setValue(13);
				}
				initialZoom.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(initialZoom);
		}

		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAMapForm.prototype.addPanels = function() {
		this.addMinimizedPanel();
		var elements = [];
		if (!!this.target.data && !!this.target.data.data && !!this.target.data.data.markers) {
			if (!(this.target.data.data.markers instanceof Array)) {
				this.target.data.data.markers = [
					this.target.data.data.markers
				];
			}
			for ( var c in this.target.data.data.markers) {
				elements.push({
					title : this.target.data.data.markers[c].title,
					description : '<b style="text-transform: uppercase;">' + this.target.data.data.markers[c].title + '</b><br>' + this.target.data.data.markers[c].description,
					id : this.target.data.data.markers[c].hash,
					href : this.target.data.data.markers[c].hash
				});
			}
		}
		var panel = new CODAMapMarkersList(this);
		panel.hash = '/map-markers';
		panel.elements = elements;
		this.addPanel(panel, gettext('$MAP_MARKER_LIST'), this.nPanels == 0);
	};

	CODAMapForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};


	KSystem.included('CODAMapForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMapMarkersForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.previousMarkersData = null;
		this.postData = null;
	}
}

KSystem.include([
	'CODAWidgetForm',
	'CODAMediaList'
], function() {
	CODAMapMarkersForm.prototype = new CODAWidgetForm();
	CODAMapMarkersForm.prototype.constructor = CODAMapMarkersForm;
	
	CODAMapMarkersForm.prototype.addMinimizedPanel = CODAMapMarkersForm.prototype.addPanels = function() {
		
		var itemData = null;
		if (!!this.target.data && !!this.target.data.data) {
			try {
			}
			catch (e) {
			}
		}
		
		var general = new KPanel(this);
		{
			general.hash = '/general';
			
			var title = new KInput(this, gettext('$LINK_TITLE') + '*', this.dataPath + '.title');
			{
				if (!!itemData && itemData.title) {
					title.setValue(itemData.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);
			
			var description = new KRichText(this, gettext('$WIDGET_DESCRIPTION'), this.dataPath + '.description');
			{
				if (!!itemData && itemData.description) {
					description.setValue(itemData.description);
				}
			}
			general.appendChild(description);
			
			var latitude = new KInput(this, gettext('$MAP_MARKER_LATITUDE') + '*', this.dataPath + '.latitude');
			{
				if (!!itemData && itemData.latitude) {
					latitude.setValue(itemData.latitude);
				}
				latitude.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(latitude);
			
			var longitude = new KInput(this, gettext('$MAP_MARKER_LONGITUDE') + '*', this.dataPath + '.longitude');
			{
				if (!!itemData && itemData.longitude) {
					longitude.setValue(itemData.longitude);
				}
				longitude.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(longitude);
			
			var draggable = new KCheckBox(this, gettext('$MAP_MARKER_DRAGGABLE'), this.dataPath + '.draggable');
			{
				draggable.addOption(true, 'SIM', (itemData && itemData.draggable && itemData.draggable[0] ? !!itemData.draggable[0] : false));
			}
			general.appendChild(draggable);
			
			var isDefault = new KCheckBox(this, gettext('$MAP_ISDEFAULT'), this.dataPath + '.isDefault');
			{
				isDefault.addOption(true, 'SIM', (itemData && itemData.isDefault && itemData.isDefault[0] ? !!itemData.isDefault[0] : false));
			}
			general.appendChild(isDefault);
			
			var icon = new KImageUpload(this, gettext('$MAP_MARKER_ICON'), this.dataPath + '.icon', CODAGenericShell.getUploadAuthorization());
			{
				if (itemData && itemData.icon) {
					icon.setValue(itemData.icon);
				}
			}
			var mediaList = new CODAMediaList(icon, 'image/*');
			icon.appendChild(mediaList);
			general.appendChild(icon);
			
			var ballon = new KRichText(this, gettext('$MAP_MARKER_BALLON'), this.dataPath + '.ballon');
			{
				if (itemData && itemData.ballon) {
					ballon.setValue(itemData.ballon);
				}
			}
			general.appendChild(ballon);
		}
		
		this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
	};
	
	CODAMapMarkersForm.prototype.minimized_draw = function() {
		this.addCSSClass('CODAFormMinimized');
		this.addCSSClass('CODAFormContentMinimized', this.content);
		this.addCSSClass('CODAFormMessagesMinimized', this.messages);
		this.minimized = true;
		
		this.setTitle((!!this.target.href ? '' : gettext('$CODA_SHELL_ADD_PREFIX')) + gettext('$' + this.config.requestType.toUpperCase()) + (!!this.target.href && !!this.target.data.data && !!this.target.data.data.title ? ' \u00ab' + this.target.data.data.title + '\u00bb' : ''));
		
		this.setStyle({
			height : (Kinky.getWidget(this.CODA_mainForm).getHeight() - 100) + 'px'
		});
		
		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.addCSSClass('CODAFormButtonGo');
		this.appendChild(go);
		
		this.addMinimizedPanel();
		
		KLayeredPanel.prototype.draw.call(this);
	};
	
	CODAMapMarkersForm.prototype.save = function(toSend) {
// this.previousActionsData = (!!toSend ? toSend : (!!this.target.data ? this.target.data : {}));
		CODAWidgetForm.prototype.save.call(this, toSend);
	};
	
	CODAMapMarkersForm.prototype.instantiateDefaults = function(params) {
		if (!(params.data.markers instanceof Array)) {
			params.data.markers = [];
		}
		
		this.postData = params;
// this.setDefaults(params);
	};
	
	CODAMapMarkersForm.prototype.onPut = function() {
		if ((this.postData.data.markers.length > 0) && this.parentWidget) {
			var newItemData = this.postData.data.markers[this.postData.data.markers.length - 1];
			
			var list = this.parentWidget;
			var listItem = new CODAMapMarkersListItem(list);
			
			listItem.hash = '/item/' + list.elements.length;
			listItem.data = newItemData;
			listItem.dataPath = 'data.markers[' + list.elements.length + ']';
			list.appendChild(listItem);
			
			list.elements.push(newItemData);
		}
		this.parent.closeMe();
	};
	
	KSystem.included('CODAMapMarkersForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMapMarkersListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox',
	'CODAMapMarkersForm'
], function() {
	CODAMapMarkersListItem.prototype = new CODAListItem();
	CODAMapMarkersListItem.prototype.constructor = CODAMapMarkersListItem;

	CODAMapMarkersListItem.prototype.remove = function(href, listItem) {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_WIDGET_CONFIRM'),
			callback : function() {

				var data = widget.getParent('CODAForm').target.data;
				eval('data.' + widget.dataPath + ' = null');

				for (var i = 0; i != data.data.markers.length; i++){
					if (data.data.markers[i] == null){
						data.data.markers.splice(i, 1);
						i--;
					}
				}
				widget.kinky.put(widget, 'rest:' + widget.getParent('CODAForm').target.href, data, null);
			},
			shell : '/CODA',
			modal : true,
			width : 400
		});
	};

	CODAMapMarkersListItem.prototype.onPut = function() {
		this.parent.removeChild(this);
	};

	CODAMapMarkersListItem.prototype.showDetails = function() {
		var href = this.getParent('CODAForm').config.href;
		var mapMarkersForm = new CODAMapMarkersForm(this.getShell(), href, null, href);
		mapMarkersForm.hash = (!!href ? href : '/new') + '/CODA/form';
		mapMarkersForm.dataPath = this.dataPath;
		mapMarkersForm.dispatchState('minimized');
		mapMarkersForm.CODA_mainForm = this.getParent('CODAMapForm').id;

		var dialog = KFloatable.make(mapMarkersForm, {
			width : 405,
			left : Kinky.getWidget(mapMarkersForm.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(mapMarkersForm.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}
		KFloatable.show(dialog);
	};

	KSystem.included('CODAMapMarkersListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAMapMarkersList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema);
	}
}

KSystem.include([
	'CODAList',
	'CODAMapMarkersListItem',
	'CODAMapMarkersForm'
], function() {
	CODAMapMarkersList.prototype = new CODAList();
	CODAMapMarkersList.prototype.constructor = CODAMapMarkersList;

	CODAMapMarkersList.prototype.getRequest = function() {
		var params = {};
		return params;
	};

	CODAMapMarkersList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAMapMarkersListItem(list);

			listItem.hash = '/item/' + index;
			listItem.data = this.elements[index];
			listItem.dataPath = 'data.markers[' + index + ']';
			list.appendChild(listItem);
		}
	};

	CODAMapMarkersList.prototype.addItem = function() {

		var href = this.getParent('CODAForm').config.href;
		var mapMarkerForm = new CODAMapMarkersForm(this.getShell(), href, null, href);
		mapMarkerForm.hash = (!!href ? href : '/new') + '/CODA/form';
		mapMarkerForm.dataPath = 'data.markers[' + this.elements.length + ']';
		mapMarkerForm.dispatchState('minimized');
		mapMarkerForm.parentWidget = this;
		mapMarkerForm.CODA_mainForm = this.getParent('CODAMapForm').id;
		var dialog = KFloatable.make(mapMarkerForm, {
			width : 405,
			left : Kinky.getWidget(mapMarkerForm.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(mapMarkerForm.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}
		KFloatable.show(dialog);
	};

	KSystem.included('CODAMapMarkersList');
}, Kinky.CODA_INCLUDER_URL);

function CODAMenuForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		CODAMenuForm.breadcrumb = [];
	}
}

KSystem.include([
	'CODAWidgetForm',
	'CODAMenuList'
], function() {
	CODAMenuForm.prototype = new CODAWidgetForm();
	CODAMenuForm.prototype.constructor = CODAMenuForm;
	
	CODAMenuForm.prototype.addMinimizedPanel = CODAMenuForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';
			
			var title = new KInput(general, gettext('$MENU_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
			}
			general.appendChild(title);
			
		}
		
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
		
	};
	
	CODAMenuForm.prototype.addPanels = function() {
		this.addMinimizedPanel();
		
		var pagesPanel = new KPanel(this);
		{
			pagesPanel.hash = '/pages';
			var pageList = new CODAMenuList(pagesPanel, this.target.data.data);
			pageList.hash = '/pages';
			for ( var index in this.target.data.data.pages) {
				this.target.data.data.pages[index].id = pageList.hash + '/' + index;
				this.target.data.data.pages[index].pagePath = '.pages' + '[' + index + ']';
				this.target.data.data.pages[index].menulevel = 0;
				
			}
			pageList.elements = this.target.data.data.pages;
			pagesPanel.appendChild(pageList);
		}
		this.addPanel(pagesPanel, gettext('$KMENU_PAGES_PANEL'), this.nPanels == 0);
		
	};
	
	CODAMenuForm.prototype.breadcrumbNavigation = function(hash) {
		
		for ( var i = CODAMenuForm.breadcrumb.length; i != 0; i--) {
			if (CODAMenuForm.breadcrumb[i - 1].hash == hash) {
				break;
			}
			this.childWidget('/pages').childWidget(CODAMenuForm.breadcrumb[i - 1].hash).destroy();
			CODAMenuForm.breadcrumb.pop();
			
		}
		this.childWidget('/pages').childWidget(hash).setStyle({
			display : 'block'
		});
	};
	
	CODAMenuForm.prototype.manageLists = function(widget) {
		
		widget.getParent('CODAMenuList').setStyle({
			display : 'none'
		});
		
		if (!widget.data.pages) {
			widget.data.pages = new Array();
		}
		var pagesPanel = this.childWidget('/pages');
		
		var submenulist = new CODAMenuList(pagesPanel, widget.parent, widget.parent);
		submenulist.hash = widget.data.id;
		
		for ( var index in widget.data.pages) {
			widget.data.pages[index].id = widget.data.id + '/' + index;
			widget.data.pages[index].pagePath = widget.data.pagePath + '.pages' + '[' + index + ']';
			widget.data.pages[index].data = widget.data.data;
			if (!isNaN(widget.data.menulevel)) {
				widget.data.pages[index].menulevel = widget.data.menulevel + 1;
			}
		}
		
		submenulist.elements = widget.data.pages;
		submenulist.data = widget.data;
		pagesPanel.appendChild(submenulist); // se n�o passar nada vai ter ao parent. ao passar o CODAForm vai andar recursivamente para tr�s at� econtrar um desse tipo
		// os elementos t�m que ser sempre adiconados a qualquer coisa (appendChild), sen�o apenas existem
		
	};

	CODAMenuForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'menu' : {
				CODAForm.createObject(params, 'data.menu');
				params.data.menu = node.innerHTML;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODAMenuForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMenuItemForm(parent, layout, parentHref, itemMenuPath) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, layout, parentHref);
	}
}

KSystem.include([
	'CODAWidgetForm',
	'KRadioButton'
], function() {
	CODAMenuItemForm.prototype = new CODAWidgetForm();
	CODAMenuItemForm.prototype.constructor = CODAMenuItemForm;

	CODAMenuItemForm.prototype.addMinimizedPanel = CODAMenuItemForm.prototype.addPanels = function() {

		var menuItem = new KPanel(this);
		{
			menuItem.hash = '/general';

			var title = new KInput(menuItem, gettext('$MENU_TITLE') + '*', 'data' + this.data.pagePath + '.title');
			{
				if (!!this.data && !!this.data.title) {
					title.setValue(this.data.title);
				}
			}
			menuItem.appendChild(title);

			var internalHashSelector = new KCombo(menuItem, gettext('$MENU_INTERNAL_PAGE_INFO'), 'data' + this.data.pagePath + '.hashInternal');
			var isInternalPage = false;
			for ( var index in KCache.restore('/pagedefs')) {
				var selected = false;
				if (!!this.data && !!this.data.hash && (KCache.restore('/pagedefs')[index].hash == this.data.hash)) {
					selected = true;
					isInternalPage = true;
				}
				internalHashSelector.addOption(KCache.restore('/pagedefs')[index].hash, KCache.restore('/pagedefs')[index].title, selected);
			}
			menuItem.appendChild(internalHashSelector);

			var externalHash = new KInput(menuItem, gettext('$MENU_EXTERNAL_PAGE_INFO'), 'data' + this.data.pagePath + '.hashExternal');
			{
				if (!!this.data && !!this.data.hash && (isInternalPage == false)) {
					externalHash.setValue(this.data.hash);
				}
			}
			menuItem.appendChild(externalHash);
		}

		this.addPanel(menuItem, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
	};

	CODAMenuItemForm.prototype.minimized_draw = function() {
		this.addCSSClass('CODAFormMinimized');
		this.addCSSClass('CODAFormContentMinimized', this.content);
		this.addCSSClass('CODAFormMessagesMinimized', this.messages);
		this.minimized = true;

		this.setTitle((!!this.target.href ? '' : gettext('$CODA_SHELL_ADD_PREFIX')) + gettext('$' + this.config.requestType.toUpperCase()) + (!!this.target.href && !!this.target.data.data && !!this.target.data.data.title ? ' \u00ab' + this.target.data.data.title + '\u00bb' : ''));

		this.setStyle({
			height : (Kinky.getWidget(this.CODA_mainForm).getHeight() - 140) + 'px'
		});

		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(go);

		this.addMinimizedPanel();

		KLayeredPanel.prototype.draw.call(this);
	};

	CODAMenuItemForm.prototype.setDefaults = function(params) {

		var itemFormData = null;
		eval('itemFormData = params.data' + this.data.pagePath);

		if (itemFormData.hashInternal != '') {
			itemFormData.hash = itemFormData.hashInternal;
			delete (itemFormData.hashInternal);
			delete (itemFormData.hashExternal);
		}
		else if (itemFormData.hashExternal != '') {
			itemFormData.hash = itemFormData.hashExternal;
			delete (itemFormData.hashInternal);
			delete (itemFormData.hashExternal);
		}

	};

	CODAMenuItemForm.prototype.onPut = function(data, request) {
		CODAWidgetForm.prototype.onPut.call(this);
	};

	KSystem.included('CODAMenuItemForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAMenuListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
	}

}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox',
	'CODAMenuItemForm'
], function() {
	CODAMenuListItem.prototype = new CODAListItem();
	CODAMenuListItem.prototype.constructor = CODAMenuListItem;

	CODAMenuListItem.prototype.remove = function() {
		var widget = this;

		KConfirmDialog.confirm({
			question : gettext('$CODA_DELETE_WIDGET_CONFIRM'),
			callback : function() {

				var data = widget.getParent('CODAMenuForm').target.data;
				eval('data.data' + widget.data.pagePath + ' = null');

				for (var i = 0; i != data.data.pages.length; i++){
					if (data.data.pages[i] == null){
						data.data.pages.splice(i, 1);
						i--;
					}
				}
				widget.kinky.put(widget, 'rest:' + widget.getParent('CODAMenuForm').target.href, data, null);
			},
			shell : '/CODA',
			modal : true,
			width : 400
		});
	};

	CODAMenuListItem.prototype.onPut = function() {
		this.parent.removeChild(this);
	};

	CODAMenuListItem.prototype.showDetails = function() {
		var href = this.getParent('CODAForm').config.href;
		var menuitemform = new CODAMenuItemForm(this.getShell(), href, null, href);
		menuitemform.hash = (!!href ? href : '/new') + '/CODA/form';
		menuitemform.data = this.data;
		menuitemform.dispatchState('minimized');
		menuitemform.CODA_mainForm = this.getParent('CODAMenuForm').id;

		var dialog = KFloatable.make(menuitemform, {
			width : 405,
			left : Kinky.getWidget(menuitemform.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(menuitemform.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}

		KFloatable.show(dialog);
	};

	CODAMenuListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);
		// bot�o para ver mais
		var addBt = new KButton(this, ' ', 'add', function(event) {
			KDOM.stopEvent(event);
			if (!!Kinky.getActiveModalWidget().target.minimized) {
				Kinky.unsetModal();
			}
			var widgetFormParent = KDOM.getEventWidget(event, 'CODAMenuForm');
			var widget = KDOM.getEventWidget(event, 'CODAMenuListItem');

			widgetFormParent.manageLists(widget);
		});
		{
			addBt.addCSSClass('CODAMenuListItemAddButton');
			var params = {
				text : gettext('$KLISTITEM_ADD_ACTION') + '<div></div>',
				delay : 1000,
				offsetX : 0,
				offsetY : 20,
				cssClass : 'CODAListItemButtonBaloon',
				gravity : {
					y : 'bottom',
					x : 'center'
				}
			};
			KBaloon.make(addBt, params);
		}
		this.appendChild(addBt);

	};

	KSystem.included('CODAMenuListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAMenuList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema);
		
	}
	
}

KSystem.include([
	'CODAList',
	'CODAMenuListItem',
	'KLink'
], function() {
	CODAMenuList.prototype = new CODAList();
	CODAMenuList.prototype.constructor = CODAMenuList;
	
	CODAMenuList.prototype.getRequest = function() {
		var params = {};
		return params;
	};
	
	CODAMenuList.prototype.visible = function() {
		KPanel.prototype.visible.call(this);
		if (this.elements.length != 0) {
			this.list.setStyle({
				height : (this.getParent('CODAForm').panel.offsetHeight - 370) + 'px'
			}, KWidget.CONTENT_DIV);
		}
	};
	
	CODAMenuList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAMenuListItem(list);
			listItem.hash = '/item/' + index;
			listItem.data = this.elements[index];
			listItem.index = index;
			list.appendChild(listItem);
		}
	};
	
	CODAMenuList.prototype.draw = function() {
		CODAList.prototype.draw.call(this);
		
		var breadcrumbObj = {
			'hash' : this.hash,
			'title' : this.data.title
		};
		CODAMenuForm.breadcrumb.push(breadcrumbObj);
		
		var breadcrumbPanel = window.document.createElement('div');
		var first = true;
		for ( var index in CODAMenuForm.breadcrumb) {
			
			if (!first) {
				var spanSep = window.document.createElement('span');
				spanSep.innerHTML = '>';
				breadcrumbPanel.appendChild(spanSep);
			}
			first = false;
			
			var link = window.document.createElement('a');
			link.innerHTML = CODAMenuForm.breadcrumb[index].title;
			link.alt = CODAMenuForm.breadcrumb[index].hash;
			KDOM.addEventListener(link, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widgetParent = KDOM.getEventWidget(event, 'CODAMenuForm');
				widgetParent.breadcrumbNavigation(element.alt);
			});
			breadcrumbPanel.appendChild(link);
		}
		this.setTitle(breadcrumbPanel, true);
		
	};
	
	CODAMenuList.prototype.addItem = function() {
		var newPagePath = '';
		if (!!this.elements && (this.elements.length > 0)) {
			newPagePath = (!!this.data.pagePath ? this.data.pagePath : '') + '.pages[' + this.elements.length + ']';
		}
		else {
			newPagePath = (!!this.data.pagePath ? this.data.pagePath : '') + '.pages[0]';
		}
		var href = this.getParent('CODAForm').config.href;
		var menuitemform = new CODAMenuItemForm(this.getShell(), href, null, href);
		menuitemform.hash = (!!href ? href : '/new') + '/CODA/form';
		menuitemform.data = {
			pagePath : newPagePath,
			newPage : 'new'
		};
		menuitemform.dispatchState('minimized');
		menuitemform.CODA_mainForm = this.getParent('CODAMenuForm').id;
		
		var dialog = KFloatable.make(menuitemform, {
			width : 405,
			left : Kinky.getWidget(menuitemform.CODA_mainForm).offsetLeft() + 480,
			top : Kinky.getWidget(menuitemform.CODA_mainForm).offsetTop() + 100,
			modal : false,
			buttons : 'close',
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 100,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		});
		{
			dialog.addCSSClass('CODAWidgetsDialog');
			dialog.addCSSClass('CODAMinimizedDialog');
		}
		
		KFloatable.show(dialog);
	};
	
	KSystem.included('CODAMenuList');
}, Kinky.CODA_INCLUDER_URL);

function CODAMenuPages(parent, data, schema) {
	if (parent != null) {
		CODAWidgetList.call(this, parent, data, schema);
	}
}

KSystem.include([
	'CODAWidgetList'
], function() {
	CODAMenuPages.prototype = new CODAWidgetList();
	CODAMenuPages.prototype.constructor = CODAMenuPages;
	
	CODAMenuPages.prototype.onLoadPages = function(data, request) {
		this.elements = data.links;
		this.draw();
	};
	
	CODAMenuPages.prototype.draw = function() {
		if (!this.elements) {
			this.kinky.get(this, 'rest:/widgets?fields=links&requestTypes=application/wrml;schema="/wrml-schemas/widgets/page-resource"', {}, null, 'onLoadPages');
			return;
		}
		
		if (!!this.data && !!this.data.graphics) {
			delete this.data.graphics;
		}
		CODAWidgetList.prototype.draw.call(this);
	};
	
	KSystem.included('CODAMenuPages');
}, Kinky.CODA_INCLUDER_URL);

function CODAMetadata(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = (!!data ? data : {});
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAMetadata.prototype = new KPanel();
	CODAMetadata.prototype.constructor = CODAMetadata;

	CODAMetadata.prototype.go = function() {
		this.draw();
	};

	CODAMetadata.prototype.draw = function() {
		f = new KInput(this, gettext('$METADATA_DESIGNATION'), 'data.metadata.designation');
		{
			if (!!this.data && !!this.data.designation) {
				f.setValue(this.data.designation);
			}
		}
		this.appendChild(f);

		f = new KImageUpload(this, gettext('$METADATA_PICTURE'), 'data.metadata.picture', CODAGenericShell.getUploadAuthorization());
		{
			if (!!this.data && !!this.data.picture) {
				f.setValue(this.data.picture);
			}
		}
		var mediaList = new CODAMediaList(f, 'image/*');
		f.appendChild(mediaList);
		this.appendChild(f);

		f = new KTextArea(this, gettext('$METADATA_DESCRIPTION'), 'data.metadata.description');
		{
			if (!!this.data && !!this.data.description) {
				f.setValue(this.data.description);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$METADATA_LOCATION_NAME'), 'data.metadata.locationName');
		{
			if (!!this.data && !!this.data.locationName) {
				f.setValue(this.data.locationName);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$METADATA_GEO_COORDINATES'), 'data.metadata.geoCoordinates');
		{
			if (!!this.data && !!this.data.geoCoordinates) {
				f.setValue(this.data.geoCoordinates);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$METADATA_URL'), 'data.metadata.href');
		{
			if (!!this.data && !!this.data.href) {
				f.setValue(this.data.href);
			}
		}
		this.appendChild(f);

		f = new KInput(this, gettext('$METADATA_OBJECTTYPE'), 'data.metadata.objectType');
		{
			if (!!this.data && !!this.data.objectType) {
				f.setValue(this.data.objectType);
			}
		}
		this.appendChild(f);


		KPanel.prototype.draw.call(this);
	};	

	CODAMetadata.addWizardPanel = function(parent) {
		var appPanel = new KPanel(parent);
		{
			f = new KInput(appPanel, gettext('$LINK_TITLE'), 'data.metadata.designation');
			appPanel.appendChild(f);

			f = new KImageUpload(appPanel, gettext('$METADATA_PICTURE'), 'data.metadata.picture', CODAGenericShell.getUploadAuthorization());
			var mediaList = new CODAMediaList(f, 'image/*');
			f.appendChild(mediaList);
			appPanel.appendChild(f);

			f = new KTextArea(appPanel, gettext('$METADATA_DESCRIPTION'), 'data.metadata.description');
			appPanel.appendChild(f);

		}

		return appPanel;
	};	

	KSystem.included('CODAMetadata');
}, Kinky.CODA_INCLUDER_URL);

/*	
description	Text	$METADATA_DESCRIPTION						
designation	Text	$METADATA_DESIGNATION						
geoCoordinates	Text	$METADATA_GEO_COORDINATES						
href	Text	$WIDGET_METADATA_URL						
locationName	Text	$METADATA_LOCATION_NAME						
objectType	Text	$METADATA_OBJECTTYPE						
picture	Text	$METADATA_PICTURE						

*/

function CODAMoveWidgetForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAMoveWidgetForm.prototype = new CODAWidgetForm();
	CODAMoveWidgetForm.prototype.constructor = CODAMoveWidgetForm;
	
	CODAMoveWidgetForm.prototype.destroy = function() {
		KBreadcrumb.dispatchURL({
			hash : ''
		});
		CODAWidgetForm.prototype.destroy.call(this);
	};
	
	CODAMoveWidgetForm.prototype.addMinimizedPanel = function() {
		this.addChildrenPanel();
		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', function(event) {
			var form = KDOM.getEventWidget(event).parent;
			KBreadcrumb.dispatchURL({
				hash : ''
			});
			form.parent.closeMe();
		});
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAFormButtonNoGo CODAFormButton');
		this.appendChild(nogo);
	};
	
	CODAMoveWidgetForm.prototype.showMessage = function(message, persistent) {
		this.messages.innerHTML = message;
		this.setStyle({
			left : Math.round((470 - this.messages.offsetWidth) / 2) + 'px'
		}, this.messages);
		KEffects.addEffect(this.messages, {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 300,
			applyToElement : true,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			},
			onComplete : function(w, t) {
				if (!!w && !persistent) {
					KSystem.addTimer('CODAForm.hideMessage(\'' + w.id + '\')', 3000);
				}
			}
		});
		
	};
	
	CODAMoveWidgetForm.prototype.addChildrenPanel = function() {
		if (this.target.children.length == 0) {
			return;
		}
		var elements = [];
		var exclusion = "|children|graphics|href|http|links|metadata|rel|requestTypes|responseTypes|target|template|actions|_id|restServer";
		
		var children = this.target.children;
		for ( var c in children) {
			if (((this.config.href.indexOf('/docroots/') != -1) && /(.*)Page|KShellPart/.test(KSystem.getWRMLClass(children[c]))) || (/KLayout/.test(KSystem.getWRMLClass(children[c])))) {
				continue;
			}
			
			var description = '';
			for ( var d in children[c]) {
				if (exclusion.indexOf('|' + d + '|') == -1) {
					description += '<p style="text-overflow: ellipsis; white-space: nowrap; max-width: 280px; overflow: hidden;"><b style="text-transform: uppercase;">' + gettext(d) + '</b>:&nbsp;<span>' + children[c][d] + '</span></p>';
				}
			}
			description += '<p style="text-overflow: ellipsis; white-space: nowrap; max-width: 280px; overflow: hidden;"><b style="text-transform: uppercase;">Tipo</b>:&nbsp;<span>' + KSystem.getWRMLClass(children[c]) + '</span></p>';
			
			elements.push({
				title : children[c].title,
				description : '<b style="text-transform: uppercase;">' + gettext('$' + KSystem.getWRMLClass(children[c]).toUpperCase()) + ' - ' + children[c].title + '</b><br>' + description,
				id : children[c]._id,
				schema : KSystem.getWRMLSchema(children[c]),
				href : children[c].href,
				selected : children[c].href == this.childHref
			});
		}
		
		if (elements.length != 0) {
			var panel = new CODAWidgetList(this);
			panel.hash = '/children';
			panel.elements = elements;
			this.addPanel(panel, gettext('$KWIDGET_CHILDREN_LIST'), this.nPanels == 0);
		}
	};
	
	KSystem.included('CODAMoveWidgetForm');
}, Kinky.CODA_INCLUDER_URL);
function CODANewWidgetWizard(parent, screens, parent_href) {
	if (parent != null) {
		CODAWizard.call(this, parent, screens);
		this.setTitle(gettext('$WIDGET_ADD_NEW'));
		this.hash = '/widgets/add';
		this.parent_href = parent_href;
	}
}

KSystem.include([
	'CODAWizard'
], function() {
	CODANewWidgetWizard.prototype = new CODAWizard();
	CODANewWidgetWizard.prototype.constructor = CODANewWidgetWizard;

	CODANewWidgetWizard.prototype.onLoadSchema = function(data, request) {	
		KCache.commit(request.service, data);
		this.fireNext();
	};

	CODANewWidgetWizard.prototype.onPrevious = function() {	
	};

	CODANewWidgetWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				if (this.forms.length > 2) {
					this.removePanel(2);
				}
				var widgetType = this.values[0].widgetType;
				var confs = Kinky.getDefaultShell().widgetConf;
				var conf = confs.elements[widgetType];
				var schema = KCache.restore(conf.schema);

				this.schemaName = conf.schema;
				this.requestType = conf.type;
				
				if (!schema) {
					this.kinky.get(this, 'rest:' + conf.schema, {}, null, 'onLoadSchema');
					this.cancelNext = true;
					return;
				}

				var uiClass = KSystem.getWRMLForm(schema, 'coda.cms.');
				this.addPanel(uiClass);
				this.setInfo(gettext(conf.description));
				break;
			}
		}
	};

	CODANewWidgetWizard.prototype.mergeValues = function() {
		var params = {};
		for (var v  = 1 ; v != this.values.length; v++) {
			if (!!this.values[v].data) {
				if (!params.data) {
					params.data = {};
				}
				for (var f in this.values[v].data) {
					params.data[f] = this.values[v].data[f];
				}
			}
			for (var f in this.values[v]) {
				if (f != 'data') {
					params[f] = this.values[v][f];
				}
			}
		}
		if (!params.data.requestTypes) {
			params.data.requestTypes = [
				"application/json",
				"application/wrml;schema=\"" + this.schemaName + "\"",
				"application/vnd.kinky." + this.requestType
			];
		}
		if (!params.data.responseTypes) {
			params.data.responseTypes = [
				"application/json",
				"application/wrml;schema=\"" + this.schemaName + "\"",
				"application/vnd.kinky." + this.requestType
			];
		}
		else {
			params.data.requestTypes = params.data.responseTypes;
		}
		params.parent_root = CODA.ACTIVE_SHELL;

		return params;
	};

	CODANewWidgetWizard.prototype.onPut= CODANewWidgetWizard.prototype.onPost = CODANewWidgetWizard.prototype.onCreated = function(data, request) {
		if (request.service.indexOf("childwidgets") != -1) {
			window.document.location.reload(true);
		}
		else {
			this.kinky.put(this, 'rest:' + this.parent_href + '/child' + data.http.headers["Location"].substring(1), {}, null);
		}
	};

	CODANewWidgetWizard.prototype.onWidgetAdded = function(data, request) {
		this.kinky.put(this, 'rest:' + this.parent_href + '/child' + data.http.headers["Location"].substring(1), {}, null);
	};

	CODANewWidgetWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				this.kinky.post(this, 'rest:/widgets', params, null, 'onWidgetAdded');
			}
		}
		catch (e) {

		}
	};

	KSystem.included('CODANewWidgetWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAPageForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAPageForm.prototype = new CODAWidgetForm();
	CODAPageForm.prototype.constructor = CODAPageForm;

	CODAPageForm.prototype.onPost = CODAPageForm.prototype.onCreated = function(data, request) {
		this.createdHref = data.http.headers.Location;

		if (!!this.target.parent) {
			var config = null;
			var href = null;
			for ( var link in data.links) {
				href = link;
				config = data.links[link];
				break;
			}
			this.kinky.put(this, 'rest:' + this.target.parent + '/child' + href.substr(1), config, this.config.headers, 'onWidgetAssociated');
		}
		else {
			KCache.clear('/pagedefs');
			this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL.replace('docroots', 'website') + '/sitemap?flat=true', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadPages');
		}
	};

	CODAPageForm.prototype.onWidgetAssociated = function(data, request) {
		this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL.replace('docroots', 'website') + '/sitemap?flat=true', {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onLoadPages');
	};

	CODAPageForm.prototype.onLoadPages = function(data, request) {
		var pageDef = {};
		for ( var i in data) {
			pageDef[data[i].hash] = data[i];
		}
		for ( var hash in pageDef) {
			if (pageDef[hash].href == this.createdHref) {
				CODACMSShell.refreshFrame(hash);
				break;
			}
		}
		KCache.clear('/pagedefs');
		KCache.commit('/pagedefs', pageDef);

		var layout = this.childWidget('/layouts');
		if (!!layout) {
			layout.sendRequest();
		}
		this.parent.closeMe();
		if (this.minimized) {
			Kinky.unsetModal();
		}

		KBreadcrumb.dispatchURL({
			hash : '/edit-new' + this.createdHref
		});
	};

	CODAPageForm.prototype.addMinimizedPanel = CODAPageForm.prototype.addPanels = CODAPageForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);

			var pageUrl = new KInput(general, gettext('$LINK_RESOURCE_HASH') + '*', 'data.hash');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.hash) {
					pageUrl.setValue(this.target.data.data.hash);
				}
				pageUrl.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(pageUrl);

			if (wizard || !this.target.data || !this.target.data.data){
				var pageType = new KCombo(general, gettext('$PAGE_REQUESTTYPES') + '*', 'data.requestTypes');
				var widgetConf = Kinky.getDefaultShell().pageConf.elements;
				for ( var wc in widgetConf) {
					var types = ['application/json','application/wrml;schema="/wrml-schemas/widgets/page-resource"'];
					var wcType = 'application/vnd.kinky.' + widgetConf[wc].type;
					types.push(wcType);
					if (!!widgetConf[wc].template) {
						types.push('text/html;template=' + !!widgetConf[wc].template);
					}
					pageType.addOption(types, gettext(widgetConf[wc].description), false);
				}
				pageType.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				general.appendChild(pageType);
			}

			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					description.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(description);

		}
		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAPageForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};


	KSystem.included('CODAPageForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAPageList(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.pageListData = new Object();
		this.addCSSClass('CODAPageList');
		
		CODAPageList.breadcrumb = [];
	}
}

KSystem.include([
	'KPanel',
	'KList',
	'KLink',
	'KInput'
], function() {
	CODAPageList.prototype = new KPanel();
	CODAPageList.prototype.constructor = CODAPageList;
	
	CODAPageList.prototype.load = function() {
		this.kinky.get(this, 'rest:' + CODA.ACTIVE_SHELL.replace('docroots', 'website') + '/sitemap', {});
	};
	
	CODAPageList.prototype.onLoad = function(data, request) {
		this.pageListData = data;
		this.draw();
	};
	
	CODAPageList.prototype.draw = function() {
		
		var mainPanel = new KPanel(this);
		mainPanel.addCSSClass('MainLevelPanel');
		mainPanel.addCSSClass('EspecialContent', KWidget.CONTENT_DIV);
		mainPanel.setTitle(gettext('$CODA_PAGELIST_PAGES'));
		{
			var pointerImg = window.document.createElement('i');
			pointerImg.className = ' CODAToobarDialogPointer fa fa-caret-up';
			mainPanel.panel.appendChild(pointerImg);
			
			this.newPageList(mainPanel, this.pageListData, true, 0);
		}
		this.appendChild(mainPanel);
		
		KPanel.prototype.draw.call(this);
	};
	
	CODAPageList.prototype.filterResults = function(list, value) {
		var childs = list.childWidgets();
		var regexp = new RegExp('(.*)' + value.replace(' ', '(.*)') + '(.*)', 'i');
		for ( var index in childs) {
			if (childs[index] instanceof KLink) {
				if (regexp.test(childs[index].data.hash.substr(childs[index].data.hash.lastIndexOf('/')))) {
					childs[index].setStyle({
						display : 'block'
					});
				}
				else {
					childs[index].setStyle({
						display : 'none'
					});
				}
			}
		}
		list.resultsArrived = true;
	};
	
	CODAPageList.prototype.newPageList = function(container, pages, flag, level) {
		
		var filterInput = new KInput(container, '', 'pages-main-search');
		filterInput.setInnerLabel(gettext('$CODA_PAGELIST_FILTER'));
		filterInput.addEventListener('keyup', function(event) {
			var widget = KDOM.getEventWidget(event);
			if (widget.parent.list && widget.parent.list.resultsArrived) {
				widget.parent.list.resultsArrived = false;
				widget.getParent('CODAPageList').filterResults(widget.parent.list, widget.getValue());
			}
		});
		container.appendChild(filterInput);
		
		container.list = new KList(container);
		container.list.level = level;
		container.list.last = {};
		container.list.resultsArrived = true;
		for ( var index in pages) {
			if (!pages[index].hash) {
				continue;
			}
			var item = new KLink(container.list);
			item.setLinkText(pages[index].hash.substr(pages[index].hash.lastIndexOf('/')));
			item.data = pages[index];
			
			if (item.data.pages) {
				var pageListHtml = '';
				var count = 0;
				for ( var p in item.data.pages) {
					pageListHtml += '<li>' + item.data.pages[p].hash.substr(item.data.pages[p].hash.lastIndexOf('/')) + '</li>';
					count += 20;
				}
				
				var pointer = window.document.createElement('div');
				pointer.className = ' CODAListPagesPointer ';
				pointer.style.display = 'none';
				item.content.appendChild(pointer);				
			}
			
			item.addEventListener('click', function(event) {
				var widget = KDOM.getEventWidget(event);
				if (!widget.data.noPage) {
					var prefix = window.document.getElementById('siteDiv').contentDocument.location.href.split('#')[0];
					window.document.getElementById('siteDiv').contentDocument.location.href = prefix + '#' + widget.data.hash;
					widget.getParent('CODAPageList').parent.closeMe();
				}
			}, KLink.TEXT_ELEMENT);
			
			item.addEventListener('mouseover', function(event) {
				var widget = KDOM.getEventWidget(event);
				if (!widget) {
					return;
				}
				if (!widget.data.noPage) {
					widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'underline';
				}
			}, KLink.TEXT_ELEMENT);
			
			item.addEventListener('mouseout', function(event) {
				var widget = KDOM.getEventWidget(event);
				if (!widget) {
					return;
				}
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'none';
			}, KLink.TEXT_ELEMENT);
			
			if (!!item.data.pages) {
				item.childPagesBt = window.document.createElement('div');
				item.childPagesBt.className = 'ChildPagesBt';
				item.childPagesBt.innerHTML = '<i class="fa fa-chevron-right"></i>';
				// item.childPagesBt.style.display = 'none';
				item.panel.appendChild(item.childPagesBt);
				
				var firstLevelFlag = flag ? flag : false;
				
				KDOM.addEventListener(item.childPagesBt, 'click', function(event) {
					var widget = KDOM.getEventWidget(event);
					var widgetParent = KDOM.getEventWidget(event, 'CODAPageList');
					widgetParent.createSubPageList(widget.data.hash, widget.data.hash, widget.data.pages, firstLevelFlag, widgetParent.level + 1);
					widget.addCSSClass('KLinkMarked');
					if (!!widget.parent.last['level' + (widget.parent.level + 1)]) {
						widget.parent.last['level' + (widget.parent.level + 1)].removeCSSClass('KLinkMarked');
					}
					widget.parent.last['level' + (widget.parent.level + 1)] = widget;
				});
			}
			else {
				item.addCSSClass('KLinkNoChildren');
			}
			
			container.list.appendChild(item);
		}
		container.appendChild(container.list);
	};
	
	CODAPageList.prototype.createSubPageList = function(parentHash, parentTitle, pages, flag, level) {
		
		if (!this.childPagesPanel) {
			this.childPagesPanel = new KPanel(this);
			this.childPagesPanel.addCSSClass('SecondaryLevelPanel');
			this.childPagesPanel.addCSSClass('EspecialContent', KWidget.CONTENT_DIV);
			{
				this.childPagesPanel.newDiv = window.document.createElement('div');
				this.childPagesPanel.newDiv.className = 'NewDiv';
				this.childPagesPanel.newDiv.style.width = '0px';
				this.childPagesPanel.content.appendChild(this.childPagesPanel.newDiv);
				
				this.childPagesPanel.breadcrumbDiv = window.document.createElement('div');
				this.childPagesPanel.breadcrumbDiv.className = 'BreadcrumbDiv';
				this.childPagesPanel.content.appendChild(this.childPagesPanel.breadcrumbDiv);
				
				var detail = window.document.createElement('div');
				detail.className = 'Detail';
				this.childPagesPanel.content.appendChild(detail);
			}
			this.appendChild(this.childPagesPanel);
		}
		else {
			if (flag) {
				this.breadcrumbNavigation();
			}
			else {
				var prevBC = parentHash.substr(0, parentHash.lastIndexOf('/'));
				this.breadcrumbNavigation(prevBC);
			}
		}
		
		var listPanel = new KPanel(this.childPagesPanel);
		listPanel.hash = parentHash;
		
		var breadcrumbObj = {
			'hash' : parentHash.substr(parentHash.lastIndexOf('/')),
			'title' : parentTitle
		};
		CODAPageList.breadcrumb.push(breadcrumbObj);
		
		this.newPageList(listPanel, pages, false, level);
		this.childPagesPanel.newDiv.style.width = (parseInt(this.childPagesPanel.newDiv.style.width.replace('px', '')) + 250) + 'px';
		this.childPagesPanel.appendChild(listPanel, this.childPagesPanel.newDiv);
		this.refreshBreadcrumb();
		
	};
	
	CODAPageList.prototype.breadcrumbNavigation = function(hash) {
		for ( var i = CODAPageList.breadcrumb.length; i != 0; i--) {
			if (CODAPageList.breadcrumb[i - 1].title == hash) {
				break;
			}
			this.childPagesPanel.newDiv.style.width = (parseInt(this.childPagesPanel.newDiv.style.width.replace('px', '')) - 250) + 'px';
			this.childPagesPanel.childWidget(CODAPageList.breadcrumb[i - 1].title).destroy();
			CODAPageList.breadcrumb.pop();
		}
		this.refreshBreadcrumb();
	};
	
	CODAPageList.prototype.refreshBreadcrumb = function() {
		
		this.childPagesPanel.breadcrumbDiv.innerHTML = '';
		
		var breadcrumbDiv = window.document.createElement('div');
		
		for ( var index in CODAPageList.breadcrumb) {
			var link = window.document.createElement('a');
			link.innerHTML = CODAPageList.breadcrumb[index].hash;
			link.alt = CODAPageList.breadcrumb[index].title;
			KDOM.addEventListener(link, 'click', function(event) {
				var element = KDOM.getEventTarget(event);
				var widgetParent = KDOM.getEventWidget(event, 'CODAPageList');
				widgetParent.breadcrumbNavigation(element.alt);
			});
			breadcrumbDiv.appendChild(link);
		}
		this.childPagesPanel.breadcrumbDiv.appendChild(breadcrumbDiv);
	};
	
	KSystem.included('CODAPageList');
}, Kinky.CODA_INCLUDER_URL);
function CODAPageOptions(parent, href, title) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.pageListData = new Object();
		this.addCSSClass('CODAPageOptions CODAPageList');
		this.href = href;
		this.pagetitle = title;
	}
}

KSystem.include([
	'KPanel',
	'KList',
	'KLink',
	'CODAPageForm',
	'KInput'
], function() {
	CODAPageOptions.prototype = new KPanel();
	CODAPageOptions.prototype.constructor = CODAPageOptions;
	
	CODAPageOptions.prototype.load = function() {
		this.draw();
	};
	
	CODAPageOptions.prototype.draw = function() {
		
		var mainPanel = new KPanel(this);
		{
			mainPanel.addCSSClass('MainLevelPanel');
			mainPanel.addCSSClass('EspecialContent', KWidget.CONTENT_DIV);
			mainPanel.setTitle(this.pagetitle);
			{
				var pointerImg = window.document.createElement('img');
				pointerImg.className = ' CODAToobarDialogPointer ';
				pointerImg.src = Kinky.getShellConfig(this.shell).docroot + 'images/widget_dialog_attached_bg.png';
				mainPanel.panel.appendChild(pointerImg);
			}
			this.appendChild(mainPanel);
		}
		
		var editPage = new KLink(mainPanel);
		{
			editPage.setLinkText(gettext('$CODA_PAGE_OPTIONS_EDIT'));
			editPage.addCSSClass('KLinkNoChildren');
			
			editPage.addEventListener('click', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event);
				
				var configForm = new CODAPageForm(Kinky.getDefaultShell(), widget.getParent('CODAPageOptions').href);
				{
					configForm.hash = widget.getParent('CODAPageOptions').href + '/CODA/form';
				}
				
				var dialog = KFloatable.make(configForm, {
					modal : true,
					width : 940,
					scale : {
						height : 0.9
					}
				});
				{
					dialog.addCSSClass('CODAFloatablePropDialog');
					dialog.addCSSClass('CODAPageEditDialog');
				}
				
				widget.getParent('CODAPageOptions').parent.closeMe();
				KFloatable.show(dialog);
			}, KLink.TEXT_ELEMENT);
			
			editPage.addEventListener('mouseover', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'underline';
			}, KLink.TEXT_ELEMENT);
			
			editPage.addEventListener('mouseout', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'none';
			}, KLink.TEXT_ELEMENT);
			
			editPage.setStyle({
				marginLeft : '10px'
			});
			
		}
		mainPanel.appendChild(editPage);
		
		var removePage = new KLink(mainPanel);
		{
			removePage.setLinkText(gettext('$CODA_PAGE_OPTIONS_REMOVE'));
			removePage.addCSSClass('KLinkNoChildren');
			
			removePage.addEventListener('click', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event);
				KBreadcrumb.dispatchURL({
					hash : '/remove' + widget.getParent('CODAPageOptions').href
				});
				widget.getParent('CODAPageOptions').parent.closeMe();
				
			}, KLink.TEXT_ELEMENT);
			
			removePage.addEventListener('mouseover', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'underline';
			}, KLink.TEXT_ELEMENT);
			
			removePage.addEventListener('mouseout', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'none';
			}, KLink.TEXT_ELEMENT);
			
			removePage.setStyle({
				marginLeft : '10px'
			});
			
		}
		mainPanel.appendChild(removePage);
		
		var addWidget = new KLink(mainPanel);
		{
			addWidget.setLinkText(gettext('$CODA_PAGE_OPTIONS_ADDCHILD'));
			addWidget.addCSSClass('KLinkNoChildren');
			
			addWidget.addEventListener('click', function(event) {
				KDOM.stopEvent(event);
				var widget = KDOM.getEventWidget(event);
				KBreadcrumb.dispatchURL({
					hash : '/add-to' + widget.getParent('CODAPageOptions').href
				});
				widget.getParent('CODAPageOptions').parent.closeMe();
			}, KLink.TEXT_ELEMENT);
			
			addWidget.addEventListener('mouseover', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'underline';
			}, KLink.TEXT_ELEMENT);
			
			addWidget.addEventListener('mouseout', function(event) {
				var widget = KDOM.getEventWidget(event);
				widget.panel.getElementsByTagName('a')[0].style.textDecoration = 'none';
			}, KLink.TEXT_ELEMENT);
			
			addWidget.setStyle({
				marginLeft : '10px'
			});
			
		}
		mainPanel.appendChild(addWidget);
		
		KPanel.prototype.draw.call(this);
	};
	
	KSystem.included('CODAPageOptions');
}, Kinky.CODA_INCLUDER_URL);
function CODAPanelForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAPanelForm.prototype = new CODAWidgetForm();
	CODAPanelForm.prototype.constructor = CODAPanelForm;

	CODAPanelForm.prototype.addMinimizedPanel = CODAPanelForm.prototype.addPanels = CODAPanelForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(title);

			var description = null;
			if (!wizard) {
				description = new KRichText(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			else {
				description = new KTextArea(general, gettext('$WIDGET_DESCRIPTION'), 'data.description');
			}
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					desctiption.setValue(this.target.data.data.description);
				}
			}
			general.appendChild(desctiption);
		}

		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};

	CODAPanelForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'description' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
		}
		return params;
	};
	KSystem.included('CODAPanelForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAShareForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODAShareForm.prototype = new CODAWidgetForm();
	CODAShareForm.prototype.constructor = CODAShareForm;
	
	CODAShareForm.prototype.draw = function() {
		CODAWidgetForm.prototype.draw.call(this);
		this.removeChild(this.childWidget('/advanced-properties'));
	};
	
	KSystem.included('CODAShareForm');
}, Kinky.CODA_INCLUDER_URL);
function CODATemplate(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.data = (!!data ? data : {});
		this.jsClass = KSystem.getWRMLClass(this.data);
		this.config = {
			href : '/widgetconfigs'
		};
		this.fieldID = 'data.template';
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODATemplate.prototype = new KPanel();
	CODATemplate.prototype.constructor = CODATemplate;

	CODATemplate.prototype.onPreConditionFailed = CODATemplate.prototype.onNoContent = function(data, request) {
		this.abort();
	};

	CODATemplate.prototype.onLoad = function(data, request) {
		var name = null;
		for (name in data.elements) {
			if (data.elements[name].type == this.jsClass) {
				break;
			}
		}
		this.widgetName = name;
		this.widgetConf = data;
		var headers = null;
		if (!!Kinky.getShell(this.parent.targetShell).config.http && !!Kinky.getShell(this.parent.targetShell).config.http.headers && !!Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']
			};
		}
		this.kinky.get(this, 'rest:/widgetconfigs/' + data.elements[name].id + '/resources', {}, headers, 'onLoadResources');
	};

	CODATemplate.prototype.onLoadResources = function(data, request) {

		var collectionHref = null;
		try {
			collectionHref = this.data.template.collection.href;
		} catch (e) {
		}

		var resource = new KCombo(this, gettext('$WIDGET_RESOURCE_TEMPLATE'), 'collection');
		{
			resource.hash = '/resource-template';
			resource.addOption('', '--');

			for ( var res in data) {
				if (res == 'http') {
					continue;
				}
				resource.addOption(data[res], gettext(data[res].title), (data[res].href == collectionHref));
			}

			resource.addEventListener('change', CODATemplate.selectedSchema);
		}
		this.appendChild(resource);

		if (!this.widgetConf.elements[this.widgetName].isLeaf) {

			var widgetType = null;
			try {
				widgetType = this.data.template.widget.type;
			} catch (e) {
			}

			var resourceClass = new KCombo(this, gettext('$WIDGET_RESOURCE_CLASS'), 'widget');
			{
				resourceClass.hash = '/resource-class';
				resourceClass.addOption('', '--');

				for ( var res in this.widgetConf.elements) {
					resourceClass.addOption(this.widgetConf.elements[res], gettext(this.widgetConf.elements[res].description), (this.widgetConf.elements[res].type == widgetType));
				}

				resourceClass.addEventListener('change', CODATemplate.selectedClass);
			}
			this.appendChild(resourceClass);
		}
		else {
			var headers = null;
			if (!!Kinky.getShell(this.parent.targetShell).config.http && !!Kinky.getShell(this.parent.targetShell).config.http.headers && !!Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']) {
				headers = {
					'Accept-Language' : Kinky.getShell(this.parent.targetShell).config.http.headers['Content-Language']
				};
			}
			this.kinky.get(this, 'rest:' + this.widgetConf.elements[this.widgetName].schema.replace(/\-resource/, '-document'), {}, headers, 'onLoadClass');
		}
		KPanel.prototype.draw.call(this);
		var collectionHref = null;
		try {
			collectionHref = this.data.template.collection.href;
			if (!!collectionHref && !!this.childWidget('/resource-template')){
				KDOM.fireEvent(this.childWidget('/resource-template').combo, 'propertychange');
			}
		} catch (e) {
		}

		var widgetType = null;
		try {
			widgetType = this.data.template.widget.type;
			if (!!widgetType && !!this.childWidget('/resource-class')){
				KDOM.fireEvent(this.childWidget('/resource-class').combo, 'propertychange');
			}
		} catch (e) {
		}
	};

	CODATemplate.prototype.drawTranslation = function() {

		var translationTable = null;
		try {
			translationTable = this.data.template.translationTable;
		} catch (e) {
		}

		for ( var fieldC in this.widgetClass.fields) {
			if (this.widgetClass.fields[fieldC].hidden || (this.widgetClass.fields[fieldC].type == 'Schema') || (/hash|target/.test(fieldC))) {
				continue;
			}

			var combo = new KCombo(this, gettext(this.widgetClass.fields[fieldC].description), fieldC);

			var value = null;
			for (var index in translationTable){
				if (translationTable[index].dstField == fieldC){
					value = translationTable[index].srcField;
					break;
				}
			}

			for ( var fieldS in this.widgetSchema.fields) {
				combo.addOption({
					srcField : fieldS,
					dstField : fieldC
				}, gettext(this.widgetSchema.fields[fieldS].description), (fieldS == value));
			}
			this.appendChild(combo);
		}
	};

	CODATemplate.prototype.onLoadSchema = function(data, request) {
		this.widgetSchema = data;
		if (!!this.childWidget('/resource-class') && !!this.widgetClass) {
			this.drawTranslation();
		}
		else if (!this.childWidget('/resource-class')) {
			this.drawTranslation();
		}
	};

	CODATemplate.prototype.onLoadClass = function(data, request) {
		this.widgetClass = data;
		if (!!this.widgetSchema) {
			this.drawTranslation();
		}
	};

	CODATemplate.selectedSchema = function(event) {
		var value = KDOM.getEventWidget(event).getValue();
		var form = KDOM.getEventWidget(event, 'CODATemplate');

		var headers = null;
		if (!!Kinky.getShell(form.parent.targetShell).config.http && !!Kinky.getShell(form.parent.targetShell).config.http.headers && !!Kinky.getShell(form.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(form.parent.targetShell).config.http.headers['Content-Language']
			};
		}
		form.kinky.get(form, 'rest:' + value.schema_href.replace(/(\-collection)|(\-store)|(\-resource)/, '-document'), {}, headers, 'onLoadSchema');
	};

	CODATemplate.selectedClass = function(event) {
		var value = KDOM.getEventWidget(event).getValue();
		var form = KDOM.getEventWidget(event, 'CODATemplate');

		var headers = null;
		if (!!Kinky.getShell(form.parent.targetShell).config.http && !!Kinky.getShell(form.parent.targetShell).config.http.headers && !!Kinky.getShell(form.parent.targetShell).config.http.headers['Content-Language']) {
			headers = {
				'Accept-Language' : Kinky.getShell(form.parent.targetShell).config.http.headers['Content-Language']
			};
		}
		form.kinky.get(form, 'rest:' + value.schema.replace(/\-resource/, '-document'), {}, headers, 'onLoadClass');

	};

	CODATemplate.prototype.getRequest = function() {
		var params = {
			collection : null,
			widget : null,
			translationTable : []
		};

		for ( var child in this.childWidgets()) {
			var input = this.childWidget(child);
			var field = input.getInputID();
			var value = input.getValue();
			var validation = input.validate();

			if (validation != true) {
				input.focus();
				input.errorArea.innerHTML = validation;
				return;
			}

			if ((field == 'collection') || (field == 'widget')) {
				if (!!value && (value != '')) {
					params[field] = value;
				}
			}
			else {
				if (!!value && (value != '')) {
					params['translationTable'].push(value);
				}
			}
		}

		return params;
	};

	KSystem.included('CODATemplate');
}, Kinky.CODA_INCLUDER_URL);

function CODATextForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWidgetForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
	}
}

KSystem.include([
	'CODAWidgetForm'
], function() {
	CODATextForm.prototype = new CODAWidgetForm();
	CODATextForm.prototype.constructor = CODATextForm;

	CODATextForm.prototype.addMinimizedPanel = CODATextForm.prototype.addPanels = CODATextForm.addWizardPanel = function(parent) {
		var wizard = true;
		if (!parent) {
			wizard = false;
			parent = this;
		}

		var general = new KPanel(parent);
		{
			general.hash = '/general';

			var title = new KInput(general, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					title.setValue(this.target.data.data.title);
				}
				title.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				title.setHelp(gettext('$HELP_TEXT_TITLE'), CODAForm.getHelpOptions());
			}
			general.appendChild(title);

			var lead = null;
			if (!wizard) {
				lead = new KRichText(general, gettext('$TEXT_LEAD'), 'data.description');
			}
			else {
				lead = new KTextArea(general, gettext('$TEXT_LEAD'), 'data.description');
			}
			{
				lead.height = 150;
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.description) {
					lead.setValue(this.target.data.data.description);
				}
				lead.setHelp(gettext('$HELP_TEXT_LEAD'), CODAForm.getHelpOptions());
			}
			general.appendChild(lead);

			var body = null;
			if (!wizard) {
				body = new KRichText(general, gettext('$TEXT_BODY') + '*', 'data.body');
			}
			else {
				body = new KTextArea(general, gettext('$TEXT_BODY') + '*', 'data.body');
			}
			{
				body.height = 400;
				if (!wizard && !!this.target.data && !!this.target.data.data && !!this.target.data.data.body) {
					body.setValue(this.target.data.data.body);
				}
				body.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				body.setHelp(gettext('$HELP_TEXT_BODY'), CODAForm.getHelpOptions());
			}
			general.appendChild(body);
		}

		if (!wizard) {
			this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
		}
		else {
			return general;
		}
	};


	CODATextForm.instantiate = function(params, field, node) {
		switch (field) {
			case 'title' : {
				CODAForm.createObject(params, 'data.title');
				params.data.title = node.innerHTML;
				break;
			}
			case 'lead' : {
				CODAForm.createObject(params, 'data.description');
				params.data.description = node.innerHTML;
				break;
			}
			case 'body' : {
				CODAForm.createObject(params, 'data.body');
				params.data.body = node.innerHTML;
				break;
			}
		}
		return params;
	};

	KSystem.included('CODATextForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAVersionsForm(parent, data) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.elements = data;

		this.messages = window.document.createElement('div');
		this.messages.innerHTML = '&nbsp;';
		this.setStyle({
			opacity : 0
		}, this.messages);
		this.panel.appendChild(this.messages);
		this.justpublished = true;
	}
}

KSystem.include([
	'KPanel',
	'CODAList'
], function() {
	CODAVersionsForm.prototype = new KPanel();
	CODAVersionsForm.prototype.constructor = CODAVersionsForm;
	
	CODAVersionsForm.prototype.destroy = function() {
		KBreadcrumb.dispatchURL({
			hash : ''
		});
		KPanel.prototype.destroy.call(this);
	};

	CODAVersionsForm.prototype.onClose = function() {
	};
	
	CODAVersionsForm.prototype.onRestore = function(data) {
		var shell = this.shell;
		this.parent.closeMe();

		KMessageDialog.alert({
			message : gettext('$CODA_REVERTED_SUCCES'),
			shell : shell,
			modal : true,
			width : 400,
			height: 300
		});
	};

	CODAVersionsForm.prototype.onPreconditionFailed = function(data) {
		dump("error on restore");
	};
	
	CODAVersionsForm.prototype.load = function() {
		this.draw();
	};

	CODAVersionsForm.prototype.visible = function(tween) {
		this.setStyle({
			height : (this.getParent('KFloatable').height - 50) + 'px'
		}, KWidget.CONTENT_DIV);
		KPanel.prototype.visible.call(this, tween);
	};

	CODAVersionsForm.prototype.showMessage = function(message, persistent) {
		this.messages.innerHTML = message;
		this.setStyle({
			left : Math.round((470 - this.messages.offsetWidth) / 2) + 'px'
		}, this.messages);
		KEffects.addEffect(this.messages, {
			f : KEffects.easeOutExpo,
			type : 'fade',
			duration : 300,
			applyToElement : true,
			go : {
				alpha : 1
			},
			from : {
				alpha : 0
			},
			onComplete : function(w, t) {
				if (!!w && !persistent) {
					KSystem.addTimer('CODAVersionsForm.hideMessage(\'' + w.id + '\')', 3000);
				}
			}
		});
		
	};
	
	CODAVersionsForm.prototype.draw = function() {
		this.addCSSClass('CODAForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		this.setTitle(gettext('$VERSION_LIST_TITLE'));

		var elements = [];		
		var children = this.elements;
		for ( var c in children) {
			if (this.justpublished && children[c].message == 'saved') {
				continue;
			}
			var description = '';
			for ( var d in children[c]) {
				description += '<p style="text-overflow: ellipsis; white-space: nowrap; max-width: 280px; overflow: hidden;"><b style="text-transform: uppercase;">' + gettext(d) + '</b>:&nbsp;<span>' + children[c][d] + '</span></p>';
			}

			var date = new Date(parseInt(children[c].timestamp) * 1000);
			children[c].date = KSystem.formatDate('d/M/Y H:i:s', date);

			elements.push({
				title : KSystem.formatDate('d/M/Y H:i:s', date) + ' | ' + children[c].commiter + '\u00a0\u00a0\u00a0\u00a0\u00a0> ' + children[c].message.toUpperCase(),
				description : description,
				id : children[c].oid,
				oid : children[c].oid,
				href : children[c].oid,
				value : children[c],
				cssClass : (elements.length == 0 ? 'CURRENT ' : '') + children[c].message.toUpperCase(),
				selected : false
			});
		}

		this.current = elements[0].id;
		
		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAVersionsForm.send);
		go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
		go.addCSSClass('CODAFormButtonGo CODAFormButton');
		this.appendChild(go);

		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', function(event) {
			var form = KDOM.getEventWidget(event).parent;
			KBreadcrumb.dispatchURL({
				hash : ''
			});
			form.parent.closeMe();
		});
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAFormButtonNoGo CODAFormButton');
		this.appendChild(nogo);

		if (elements.length != 0) {
			this.list = new CODAList(this);
			this.list.hash = '/children';
			this.list.onechoice = true;
			this.list.elements = elements;
			this.list.visible = function() {
				KPanel.prototype.visible.call(this);
				var parentForm = this.getParent('CODAVersionsForm');
				this.list.setStyle({
					overflow : 'auto',
					height : (parentForm.panel.offsetHeight - 280) + 'px'
				}, KWidget.CONTENT_DIV);
			};
			this.appendChild(this.list);
		}

		KPanel.prototype.draw.call(this);
	};

	CODAVersionsForm.hideMessage = function(id) {
		var form = Kinky.getWidget(id);
		form.timer = null;
		if (!!form) {
			KEffects.addEffect(form.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (!!w) {
						w.messages.innerHTML = '&nbsp;';
					}
				}
			});
		}
	};

	CODAVersionsForm.send = function(e) {
		KDOM.stopEvent(e);
		var list = KDOM.getEventWidget(e).parent.list;
		if (list.getSelection().length == 0) {
			KMessageDialog.alert({
				message : gettext('$CODA_REVERT_SELECT_VERSION'),
				shell : list.shell,
				modal : true,
				width : 400,
				height : 300
			});
			return;
		}
		var version = list.getSelection()[0];
		if (version.oid == list.parent.current) {
			KMessageDialog.alert({
				message : gettext('$CODA_REVERT_CANNOT_CURRENT_VERSION'),
				shell : list.shell,
				modal : true,
				width : 400,
				height : 300
			});
			return;
		}

		KConfirmDialog.confirm({
			question : gettext('$CODA_REVERT_CONFIRM').replace('$DATE', version.date),
			callback : function() {
				list.kinky.post(list.parent, "rest:/versions/" + version.oid + '/restore', {}, null, 'onRestore');
			},
			shell : '/CODA',
			modal : true,
			width : 400
		});

	};

	KSystem.included('CODAVersionsForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAWidgetForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.target.collection = '/widgets';
		this.canEditAdvanced = true;//CODAForm.hasScope('admin');
		this.moreOptionsButton = true;
	}
}

KSystem.include([
	'KImageUpload',
	'CODAForm',
	'CODAPermissions',
	'CODAGraphicalAttributes'
], function() {
	CODAWidgetForm.prototype = new CODAForm();
	CODAWidgetForm.prototype.constructor = CODAWidgetForm;

	CODAWidgetForm.prototype.load = function(alreadyRetrieved) {
		if (!alreadyRetrieved) {
			var widget = this;
			KSystem.include([
				'CODAWidgetList'
			], function() {
				widget.load(true);
			});
			return;
		}

		if (!!this.config && !!this.config.href && !this.target.data) {
			this.kinky.get(this, 'rest:' + KSystem.urlify(this.config.href, {
				fields : '*',
				embed : 'children'
			}), {}, this.config.headers);
		}
		else if (!!this.target.data) {
			this.onLoad(this.target.data);
		}
		else {
			this.onLoad();
		}
	};

	CODAWidgetForm.prototype.setData = function(data) {
		if (!data) {
			return;
		}

		if (!!data.data) {
			this.config.schema = KSystem.getWRMLSchema(data.data).replace(/\-document/g, '-resource');
			this.config.requestType = KSystem.getWRMLClass(data.data);
		}

		if (!!data._id && ((data._id.split('/').length > 2))) {
			this.target._id = data._id;
			this.target.data = data;
		}
		else {
			delete data._id;
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.target.data = {
				data : {
					target : this.target.layout,
					requestTypes : types,
					responseTypes : types
				}
			};
		}
	};

	CODAWidgetForm.prototype.loadForm = function(data, request) {
		this.setTitle('<button onclick="CODAWidgetForm.goUp(event)"><i class="fa fa-arrow-left"></i> / </button>' + (!!this.target.href ? '' : gettext('$CODA_SHELL_ADD_PREFIX')) + gettext('$' + this.config.requestType.toUpperCase()) + (!!this.target.href && !!this.target.data.data && !!this.target.data.data.title ? ' \u00ab' + this.target.data.data.title + '\u00bb' : ''), true);
		this.draw();
	};

	CODAWidgetForm.prototype.onNotFound = CODAForm.prototype.onNoContent = function(data, request) {
		this.target.childsize = 0;
		if (request.service.indexOf('/childwidgets') != -1) {
			this.draw();
		}
		else {
			this.activate();
		}
	};

	CODAWidgetForm.prototype.draw = function() {
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		var go = new KButton(this, gettext('$CONFIRM'), 'go', CODAForm.sendRest);
		go.setText('<i class="fa fa-check-circle"></i> ' + gettext('$CONFIRM'), true);
		go.addCSSClass('CODAFormButtonGo');
		this.appendChild(go);

		var nogo = new KButton(this, gettext('$CANCEL'), 'nogo', CODAForm.closeMe);
		nogo.setText('<i class="fa fa-times-circle"></i> ' + gettext('$CANCEL'), true);
		nogo.addCSSClass('CODAFormButtonNoGo');
		this.appendChild(nogo);

		this.addPanels();
		if (!!this.target.href) {
			this.addChildrenPanel();
			this.addGraphicalAttributesPanel();
			this.addMetadataPanel();
		}

		if (!!this.target.href && this.canEditAdvanced) {
			this.addPermissionsPanel();
			this.addDataFeedPanel();
			this.addTemplatePanel();
		}		

		KLayeredPanel.prototype.draw.call(this);
		if (!!this.msg) {
			this.showMessage(this.msg.replace("$WIDGET_TYPE", gettext('$' + this.config.requestType.toUpperCase())));
			this.msg = null;
		}
	};

	CODAWidgetForm.prototype.addTemplatePanel = function() {
		var panel = new CODATemplate(this, this.target.data.data);
		panel.hash = '/template';
		this.addPanel(panel, gettext('$TEMPLATE_DATA'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addMetadataPanel = function() {
		var panel = new CODAMetadata(this, this.target.data.data.metadata);
		panel.hash = '/metadata';
		this.addPanel(panel, gettext('$WIDGET_METADATA'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addDataFeedPanel = function() {
		var panel = new CODADataFeed(this, this.target.data);
		panel.hash = '/datafeed';
		this.addPanel(panel, gettext('$FEED_FEEDSCONFIG'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addPermissionsPanel = function() {
		var panel = new CODAPermissions(this, (!!this.target.data ? this.target.data.permissions : null));
		panel.hash = '/permissions';
		this.addPanel(panel, gettext('$RESOURCE_PERMISSIONS'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addGraphicalAttributesPanel = function() {
		var panel = new CODAGraphicalAttributes(this, this.target.data);
		panel.hash = '/graphicalattributes';
		panel.target = {
			href : this.target.href
		};
		this.addPanel(panel, gettext('$WIDGET_GRAPHICAL_ATTRIBUTES'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addChildrenPanel = function() {
		return;
		var panel = new CODAWidgetList(this, {
			links : {
				feed : { href : (!!this.target.href ? this.target.href : '') + '/childwidgets?fields=elements,size' }
			}
		});
		panel.hash = '/children';
		this.addPanel(panel, gettext('$KWIDGET_CHILDREN_LIST'), this.nPanels == 0);
	};

	CODAWidgetForm.addWizardPanel = CODAGraphicalAttributes.addWizardPanel ;/*function(parent) {
		var panel = new CODAMetadata(parent, null);
		panel.hash = '/metadata';
		return panel;
	};*/

	CODAWidgetForm.sub_addWizardPanel = function(parent) {
		var appPanel = new KPanel(parent);
		return appPanel;
	}

	CODAWidgetForm.prototype.onLayoutSaved = function(data, request) {
	};

	CODAWidgetForm.prototype.onWidgetAssociated = function(data, request) {
	};

	CODAWidgetForm.prototype.onPut = function(data, request) {
		this.enable();
		var layout = this.childWidget('/graphicalattributes') ? this.childWidget('/graphicalattributes').childWidget('/layouts') : null;
		if (!!layout) {
			layout.sendRequest();
		}
		this.showMessage(gettext('$CONFIRM_WIDGET_SAVED_SUCCESS').replace("$WIDGET_TYPE", gettext('$' + this.config.requestType.toUpperCase())));
	};

	CODAWidgetForm.prototype.onRemove = function(data, request) {
		this.showMessage(gettext('$CONFIRM_WIDGET_DELETED_SUCCESS'));
	};

	CODAWidgetForm.prototype.onGet = function(data, request) {
	};

	CODAWidgetForm.prototype.onError = function(data, request) {
		if (request.action == 'put') {
			this.showMessage(gettext('$CONFIRM_WIDGET_SAVED_FAIL').replace("$WIDGET_TYPE", gettext('$' + this.config.requestType.toUpperCase())));
		}
		if (request.action == 'post') {
			this.showMessage(gettext('$CONFIRM_WIDGET_CREATED_FAIL').replace("$WIDGET_TYPE", gettext('$' + this.config.requestType.toUpperCase())));
		}
		else {
			this.showMessage(gettext('$CONFIRM_WIDGET_GET_FAIL').replace("$WIDGET_TYPE", gettext('$' + this.config.requestType.toUpperCase())));
		}
	};

	CODAWidgetForm.prototype.onPost = CODAWidgetForm.prototype.onCreated = function(data, request) {
		if (!!this.target.parent) {
			var config = null;
			var href = null;
			for ( var link in data.links) {
				href = link;
				config = data.links[link];
				break;
			}
			this.kinky.put(this, 'rest:' + this.target.parent + '/child' + href.substr(1), config, this.config.headers, 'onWidgetAssociated');
		}

		var layout = this.childWidget('/graphicalattributes') ? this.childWidget('/graphicalattributes').childWidget('/layouts') : null;
		if (!!layout) {
			layout.sendRequest();
		}
		this.parent.closeMe();
		if (this.minimized) {
			Kinky.unsetModal();
		}

		KBreadcrumb.dispatchURL({
			hash : '/edit-new' + data.http.headers.Location
		});
	};

	CODAWidgetForm.prototype.instantiateDefaults = function(params) {
		params.parent_root = CODA.ACTIVE_SHELL;

		params.data = (!!params.data ? params.data : {});

		if (!params.data.requestTypes) {
			params.data.requestTypes = [
				"application/json",
				"application/wrml;schema=\"" + this.config.schema + "\"",
				"application/vnd.kinky." + this.config.requestType
			];
		}
		if (!params.data.responseTypes) {
			params.data.responseTypes = [
				"application/json",
				"application/wrml;schema=\"" + this.config.schema + "\"",
				"application/vnd.kinky." + this.config.requestType
			];
		}

		if (!params.data.graphics) {
			params.data.graphics = {};
		}
		if (!params.data.metadata) {
			params.data.metadata = {};
		}
		if (!params.feedsConfig) {
			params.feedsConfig = {};
		}
		if (!params.permissions) {
			params.permissions = [];
		}

		this.setDefaults(params);
	};

	CODAWidgetForm.prototype.setDefaults = function(params) {
	};

	CODAWidgetForm.prototype.addMinimizedPanel = function() {
		var general = new KPanel(this);
		{
			general.hash = '/general';

			var legend = new KInput(this, gettext('$LINK_TITLE') + '*', 'data.title');
			{
				if (!!this.target.data && !!this.target.data.data && !!this.target.data.data.title) {
					legend.setValue(this.target.data.data.title);
				}
				legend.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(legend);

			var type = new KCombo(this, gettext('$LINK_REQUESTTYPES') + '*', 'data.requestTypes');
			{
				var widgetConf = Kinky.getDefaultShell().widgetConf.elements;
				for ( var wc in widgetConf) {
					var value = [
						'application/json'
					];
					value.push('application/wrml;schema="' + widgetConf[wc].schema) + '"';
					value.push('application/vnd.kinky.' + widgetConf[wc].type);
					type.addOption(value, gettext(widgetConf[wc].description), false);
				}
				type.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			general.appendChild(type);
		}
		this.addPanel(general, gettext('$FORM_GENERAL_DATA_PANEL'), this.nPanels == 0);
	};

	CODAWidgetForm.prototype.addPanels = function() {
	};

	CODAWidgetForm.instantiate = function(params, field, node) {
	};

	KSystem.included('CODAWidgetForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAWidgetListItem(parent) {
	if (parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'KPanel',
	'CODAListItem',
	'KButton',
	'KCheckBox'
], function() {
	CODAWidgetListItem.prototype = new CODAListItem();
	CODAWidgetListItem.prototype.constructor = CODAWidgetListItem;
	
	CODAWidgetListItem.prototype.move = function(order) {
		if (this.data.order == order) {
			return;
		}		
		this.getParent('CODAForm').showMessage(gettext('$CODA_WORKING'), true);
		this.kinky.post(this, 'rest:' + this.getParent('CODAForm').config.href + '/childwidgets/order-child', {
			href : this.data.href,
			order : order,
			offset : order - this.data.order
		}, null, 'onMove');		
	};
	
	CODAWidgetListItem.prototype.remove = function() {
		KBreadcrumb.dispatchURL({
			hash : '/remove' + this.data.href
		})
	};

	KSystem.included('CODAWidgetListItem');
}, Kinky.CODA_INCLUDER_URL);

function CODAWidgetList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, {
			add_button : true
		});
		this.onechoice = true;
		this.pageSize = 5;
		this.orderBy = 'order';
		this.table = {
			"title" : {
				label : 'Título'
			},
			"type" : {
				label : "Tipo"
			}
		};
		this.filterFields = [ 'title' ];
		this.LIST_MARGIN = 650;
	}
}

KSystem.include([
	'CODAList',
	'CODAWidgetForm',
	'CODAWidgetListItem'
], function() {
	CODAWidgetList.prototype = new CODAList();
	CODAWidgetList.prototype.constructor = CODAWidgetList;
	
	CODAWidgetList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-to' + this.parent.config.href
		});
	};
	
	CODAWidgetList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAWidgetListItem(list);
			listItem.data = this.elements[index];
			listItem.data.id = this.elements[index]._id;
			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
			listItem.data.type = gettext('$' + listItem.data.requestType.toUpperCase());
			list.appendChild(listItem);
		}
	};
	
	KSystem.included('CODAWidgetList');
}, Kinky.CODA_INCLUDER_URL);

function CODAWidgets() {
}

KSystem.include([
	'KButton',
	'KLayeredPanel',
	'KPanel'
], function() {
	
	CODAWidgets.addWizardPanel = function(parent) {
		var confs = Kinky.getDefaultShell().widgetConf;
		var categories = {};
		
		for ( var w in confs.elements) {
			if (!categories[confs.elements[w].category]) {
				categories[confs.elements[w].category] = [];
			}
			categories[confs.elements[w].category].push(confs.elements[w]);
		}
		
		var appPanel = new KPanel(parent);
		{
			appPanel.addCSSClass('CODAApplications');
			appPanel.params = {};

			var accordion = new KLayeredPanel(appPanel);
			{
				accordion.hash = '/widget/categories';
				var first = true;
				for ( var c in categories) {
					var panel = new KPanel(accordion);
					{
						panel.hash = '/widget/categories/' + c;
						for ( var w in categories[c]) {
							var button = new KButton(panel, gettext(categories[c][w].description), 'widgetconfigs_' + categories[c][w].id, function(event) {
								var bt = KDOM.getEventWidget(event);
								if (!!appPanel.last) {
									appPanel.last.removeCSSClass('CODAApplicationsButtonSelected');
								}
								appPanel.last = bt;
								bt.addCSSClass('CODAApplicationsButtonSelected');
								appPanel.params = {
									widgetType : bt.getInputID()
								};
							});
							{
								var desc = gettext(categories[c][w].description);
								if (desc.indexOf('class="icon') == -1) {
									desc = '<i class="fa fa-cog"></i>' + desc;
								}
								button.setText(desc, true);
								button.addCSSClass('CODAApplicationsButton');
								button.addCSSClass('CODAApplications' + categories[c][w].type + 'Button');
							}
							panel.appendChild(button);
						}
					}
					accordion.addPanel(panel, gettext(c), first);
					first = false;
				}
			}
			appPanel.appendChild(accordion);
		}

		appPanel.validate = function() {
			if (!this.params.widgetType) {
				this.getParent().showMessage(gettext('$WIDGETS_SELECT_WIDGET_ERROR'), false);
				return false;
			}
			return true;
		};

		return appPanel;
	};
	
	KSystem.included('CODAWidgets');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

