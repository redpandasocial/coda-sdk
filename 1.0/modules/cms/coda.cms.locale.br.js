///////////////////////////////////////////////////////////////////
// KINKY CODA FRONTEND PLUGIN
settext('$CODA_WIDGET_TOOLBAR_SAVE', 'Guardar', 'br');
settext('$CODA_WIDGET_TOOLBAR_EDIT', 'Editar', 'br');
settext('$CODA_WIDGET_TOOLBAR_ADD', 'Adicionar Widget', 'br');
settext('$CODA_WIDGET_TOOLBAR_DELETE', 'Remover', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES & CONFIRMATIONS
settext('$CODA_PUBLISH_CONFIRM', 'Deseja publicar esta vers\u00e3o do website?', 'br');
settext('$CODA_PUBLISHED_SUCCES', 'Website publicado com sucesso.', 'br');

settext('$CODA_DELETE_WIDGET_CONFIRM', 'Deseja remover esta widget?', 'br');

settext('$CONFIRM_WIDGET_SAVED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget "$WIDGET_TYPE" guardada com sucesso!<span>', 'br');
settext('$CONFIRM_WIDGET_CREATED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget "$WIDGET_TYPE" criada com sucesso!<span>', 'br');
settext('$CONFIRM_WIDGET_SAVED_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro ao guardar a widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'br');
settext('$CONFIRM_WIDGET_CREATED_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro ao criar a widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'br');
settext('$CONFIRM_WIDGET_GET_FAIL', '<span style="color: rgb(248, 82, 1);">Ocorreu um erro no acesso aos dados da widget "$WIDGET_TYPE". Por favor, tente mais tarde<span>', 'br');
settext('$CONFIRM_WIDGET_DELETED_SUCCESS', '<span style="color: rgb(161, 196, 0);">Widget removida com sucesso</span>', 'br');
settext('$CONFIRM_WIDGET_MOVED_SUCCESS', '<span style="color: rgb(161, 196, 0);">A widget foi movida com sucesso!<span>', 'br');

settext('$CODA_REVERT_CANNOT_CURRENT_VERSION', 'A vers\u00e3o escolhida \u00e9 a vers\u00e3o actual.', 'br');
settext('$CODA_REVERT_SELECT_VERSION', 'Escolha a vers\u00E3o para a qual pretende reverter.', 'br');
settext('$CODA_REVERT_CONFIRM', 'Tem a certeza que pretende reverter o website para esta vers\u00E3o?\nTodas as altera\u00E7\u00F5es efectuadas ap\u00F3s $DATE ser\u00E3o perdidas.', 'br');
settext('$CODA_REVERTED_SUCCES', 'A vers\u00E3o foi carregada com sucesso. Clique em "REFRESCAR" para visualizar as altera\u00E7\u00F5es.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// ERROR CODES
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBARS
settext('$CODA_SHELL_ADD_PREFIX', 'ADICIONAR ', 'br');
settext('$CODA_SHELL_PAGE_LIST', 'MAPA', 'br');
settext('$CODA_SHELL_ADD_PAGE', 'NOVA', 'br');
settext('$CODA_SHELL_EDIT_CURRENT_PAGE', 'EDITAR P\u00c1GINA', 'br');
settext('$CODA_SHELL_ADD_WIDGET', 'WIDGETS', 'br');

settext('$CODA_HOVERCONTROL_MOVE_WIDGET', 'Mover Widget', 'br');
settext('$CODA_HOVERCONTROL_ADD_WIDGET', 'Adicionar Widget', 'br');

settext('$CODA_PAGE_OPTIONS_EDIT', 'Editar', 'br');
settext('$CODA_PAGE_OPTIONS_ADDCHILD', 'Adicionar Widget', 'br');
settext('$CODA_PAGE_OPTIONS_REMOVE', 'Remover', 'br');

settext('$CODA_SHELL_RETURN_HOME', 'Voltar &agrave; lista<br>de m&oacute;dulos...', 'br');
settext('$CODA_SHELL_EDIT_DOCROOT', 'Editar website', 'br');
settext('$CODA_SHELL_LIST_PAGES', 'Ir para p&aacute;gina...', 'br');
settext('$CODA_SHELL_NEW_ELEMENT', 'Adicionar novo elemento...', 'br');
settext('$CODA_SHELL_EDIT_PAGE', 'Editar p&aacute;gina ', 'br');

settext('$CODA_SHELL_REVERT', '<i class="fa fa-undo"></i> Reverter', 'br');
settext('$CODA_SHELL_PUBLISH', '<i class="fa fa-exchange"></i> Publicar', 'br');
settext('$CODA_SHELL_REFRESH', '<i class="fa fa-refresh"></i> Refrescar', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DIALOGS

// VERSIONS
settext('$VERSION_LIST_TITLE', 'Escolha a vers\u00E3o para a qual pretende reverter', 'br');
//----------

// SITE DIALOG
settext('$SITE_PROPERTIES', 'WEBSITE', 'br');
//----------

// NEW PAGE DIALOS
settext('$NEW_PAGE_PROPERTIES', 'ADICIONAR P\u00c1GINA', 'br');
//----------

// PAGE DIALOG
settext('$PAGE_PROPERTIES', 'P\u00c1GINA', 'br');
//----------

// NEW USER DIALOG
settext('$NEW_USER_PROPERTIES', 'ADICIONAR UTILIZADOR', 'br');
//----------

// WIDGET DIALOG
settext('$WIDGET_PROPERTIES', 'WIDGET', 'br');
settext('$NEW_WIDGET_PROPERTIES', 'ADICIONAR WIDGET', 'br');
//----------

// SITEMAP DIALOG
settext('$CODA_PAGELIST_PAGES', 'MAPA DO SITE', 'br');
settext('$CODA_PAGELIST_FILTER', '<i class="fa fa-search"></i> Filtrar Lista', 'br');
//----------

// NEW WIDGET DIALOG
settext('$CAT_MEDIA', 'MEDIA', 'br');
settext('$WIDGET_CONFIG_LINK_DESCRIPTION', '<i class="fa fa-link"></i> Hiperliga\u00e7\u00e3o', 'br');
settext('$WIDGET_CONFIG_IMAGE_DESCRIPTION', '<i class="fa fa-picture-o"></i> Imagem', 'br');
settext('$WIDGET_CONFIG_TEXT_DESCRIPTION', '<i class="fa fa-font"></i> Texto', 'br');
settext('$WIDGET_CONFIG_HIGHLIGHT_DESCRIPTION', '<i class="fa fa-star"></i> Destaque', 'br');
settext('$WIDGET_CONFIG_MAP_DESCRIPTION', '<i class="fa fa-map-marker"></i> Mapa', 'br');

settext('$CAT_CONTAINER', 'AGREGADORES', 'br');
settext('$WIDGET_CONFIG_GALLERY_DESCRIPTION', '<i class="fa fa-camera-retro"></i> Galeria', 'br');
settext('$WIDGET_CONFIG_LIST_DESCRIPTION', '<i class="fa fa-list-alt"></i> Lista', 'br');
settext('$WIDGET_CONFIG_PANEL_DESCRIPTION', '<i class="fa fa-columns"></i> Painel', 'br');
settext('$WIDGET_CONFIG_MENU_DESCRIPTION', '<i class="fa fa-sitemap"></i> Menu', 'br');
settext('$WIDGET_CONFIG_SCROLLEDLIST_DESCRIPTION', '<i class="fa fa-th"></i> Lista c/ Scroll', 'br');
settext('$WIDGET_CONFIG_PAGEDLIST_DESCRIPTION', '<i class="fa fa-list-ol"></i> Lista c/ P\u00e1ginas', 'br');

settext('$CAT_SOCIAL', 'REDES SOCIAIS', 'br');
settext('$WIDGET_CONFIG_FEED_DESCRIPTION', '<i class="fa fa-rss"></i> Feed', 'br');
settext('$WIDGET_CONFIG_SHARE_DESCRIPTION', '<i class="fa fa-share"></i> Partilha', 'br');

settext('$CAT_LANGUAGE', 'IDIOMA', 'br');
settext('$WIDGET_CONFIG_LANGSELECTION_DESCRIPTION', '<i class="fa fa-flag"></i> Selec\u00e7\u00e3o Idioma', 'br');

settext('$CAT_PAGE', 'P\u00c1GINAS', 'br');

settext('$WIDGET_CONFIG_SHELL_DESCRIPTION', 'Website', 'br');
settext('$WIDGET_CONFIG_PAGE_DESCRIPTION', 'P\u00e1gina', 'br');
settext('$WIDGET_CONFIG_SEARCH_DESCRIPTION', 'Pesquisa', 'br');
settext('$WIDGET_CONFIG_NEWSLETTER_DESCRIPTION', 'Subscri\u00e7\u00e3o Newsletter', 'br');
settext('$WIDGET_CONFIG_ORFEUPAGE_DESCRIPTION', 'P\u00e1gina Tipo Orfeu', 'br');
settext('$WIDGET_CONFIG_CATALOGHIGHLIGHT_DESCRIPTION', 'Destaque Catalogo', 'br');
settext('$WIDGET_CONFIG_CATALOGLIST_DESCRIPTION', 'Catalogo', 'br');
settext('$WIDGET_CONFIG_ARTISTALBUMLIST_DESCRIPTION', 'Lista de Albuns (Artista)', 'br');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// WIDGET TITLES
settext('$KLINK', 'Hiperliga\u00e7\u00e3o', 'br');
settext('$KMENU', 'Menu', 'br');
settext('$KLIST', 'Listagem', 'br');
settext('$KTEXT', 'Texto', 'br');
settext('$KHIGHLIGHT', 'Destaque', 'br');
settext('$KPAGEDLIST', 'Lista c/ P\u00e1ginas', 'br');
settext('$KPANEL', 'Painel', 'br');
settext('$KIMAGE', 'Imagem', 'br');
settext('$KSCROLLEDLIST', 'List c/ Scroll', 'br');
settext('$KSHELL', 'Website', 'br');
settext('$KSHELLPART', 'P\u00e1gina', 'br');
settext('$KMAP', 'Mapa', 'br');
settext('$KWIDGET', 'Widget', 'br');
settext('$KFEED', 'Feed', 'br');
settext('$KFEEDBACK', 'Feedback de Utilizador', 'br');
settext('$OFSITESEARCH', 'Pesquisa', 'br');
settext('$OFNEWSLETTERINPUT', 'Subscri\u00e7\u00e3o Newsletter', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FORMS

// WIDGETS GENERAL
// titles
settext('$KWIDGET_CHILDREN_LIST', '<i class="fa fa-cogs"></i> WIDGETS', 'br');
settext('$FEED_FEEDSCONFIG', '<i class="fa fa-rss"></i> Fontes de Dados', 'br');
settext('$RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'br');
settext('$TEMPLATE_DATA', '<i class="fa fa-magic"></i> ESQUEMA DE DADOS', 'br');
settext('$WIDGET_GRAPHICAL_ATTRIBUTES', '<i class="fa fa-eye"></i> Atributos Gr\u00e1ficos', 'br');
settext('$WIDGET_METADATA', '<i class="fa fa-flask"></i> METADATA', 'br');

// labels
settext('$WIDGET_GRAPHICS_LAYOUT', 'Layout', 'br');
settext('$WIDGET_RESOURCE_TEMPLATE', 'Template de Dados', 'br');
settext('$WIDGET_RESOURCE_CLASS', 'Widget Alvo', 'br');
settext('$WIDGET_CLASS', 'Classe CSS', 'br');
settext('$WIDGET_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$WIDGET_HASH', 'Identificador (hash)', 'br');
settext('$WIDGET_CONFIG_GRAPHICAL_DEFS', 'IMAGEM DE FUNDO', 'br');
settext('$WIDGET_GRAPHICS_BACKGROUND', 'Imagem de Fundo', 'br');

settext('$RESOURCE_FREETEXT', 'Termos', 'br');

settext('$LAYOUT_DATA', 'ESQUEMA GR\u00c1FICO', 'br');
settext('$LAYOUT', 'Escolha o Layout pretendido para este conte\u00fado', 'br');

settext('$DOCROOT_RESTSERVER', 'Servidor REST', 'br');
settext('$DOCROOT_DEFAULT_LANG', 'Idioma', 'br');
settext('$DOCROOT_UIROOT', 'Servidor HTTP', 'br');

settext('$LINK_REQUESTTYPES', 'Tipo de Widget', 'br');
settext('$LINK_TARGET_CHILD_CONTAINER', 'Layout Alvo', 'br');
settext('$LINK_TITLE', 'T\u00edtulo', 'br');

settext('$METADATA_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$METADATA_DESIGNATION', 'Designa\u00e7\u00e3o', 'br');
settext('$METADATA_GEO_COORDINATES', 'Coordenadas GPS', 'br');
settext('$METADATA_URL', 'URL', 'br');
settext('$METADATA_LOCATION_NAME', 'Localidade', 'br');
settext('$METADATA_OBJECTTYPE', 'Tipo de Objecto (Opengraph)', 'br');
settext('$METADATA_PICTURE', 'Logo para share', 'br');

settext('$PERMISSIONS_GROUP', 'Grupo', 'br');
settext('$PERMISSIONS_VALUE', 'Valor', 'br');
//----------

// DOCROOT
//----------

// PAGE
settext('$LINK_RESOURCE_HASH', 'URL da P\u00e1gina', 'br');
settext('$PAGE_REQUESTTYPES', 'Template da P\u00e1gina', 'br');
//----------

// TEXT
settext('$TEXT_BODY', 'Corpo do Texto', 'br');
settext('$TEXT_LEAD', 'Lead', 'br');
//----------

// IMAGE
settext('$IMAGE_URL', 'Imagem', 'br');
settext('$IMAGE_SCALED_URL', 'Thumb da Imagem', 'br');
settext('$IMAGE_TITLE', 'LEGENDA', 'br');
//----------

// LINK
settext('$LINK_IMAGE', 'Imagem', 'br');
settext('$LINK_TARGET_WINDOW', 'Janela Alvo', 'br');
settext('$LINK_TOOLTIP', 'Tooltip', 'br');
settext('$LINK_URL', 'URL', 'br');
settext('$LINK_TARGET_SAME', 'Mesma Janela', 'br');
settext('$LINK_TARGET_OTHER', 'Nova Janela', 'br');
//----------

// HIGHLIGHT
settext('$KHIGHLIGHT_ACTION_LIST', '<i class="fa fa-external-link"></i> Ac\u00e7\u00f5es', 'br');
settext('$HIGHLIGTH_ACTIONS', 'AC\u00c7\u00d5ES', 'br');
settext('$KLIST_LABEL_ACTIONS', 'AC\u00c7\u00d5ES  \u00bb ', 'br');
//----------

// MENU
settext('$KLISTITEM_ADD_ACTION', 'Gerir Sub-Menu', 'br');

settext('$KMENU_PAGES_PANEL', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'br');
settext('$KMENULIST_NEWENTRY', 'NOVA ENTRADA', 'br');

settext('$MENU_WIDGET_PAGES', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'br');
settext('$MENU_HASH', 'HASH', 'br');
settext('$MENU_HREF', 'HREF', 'br');
settext('$MENU_TITLE', 'T\u00edtulo do Menu', 'br');
settext('$MENU_CLASS', 'Classe de CSS', 'br');
settext('$MENU_INTERNAL_PAGE_INFO', 'Seleccione uma p\u00e1gina do site', 'br');
settext('$MENU_EXTERNAL_PAGE_INFO', 'ou insira o url de uma p\u00e1gina externa', 'br');

//----------

// LIST
settext('$CONFIG_LIST_AUTO_ROTATE', 'Per\u00edodo de Rota\u00e7\u00e3o Autom\u00e1tica de Listagem', 'br');
settext('$LIST_WIDGET_PAGESIZE', 'N\u00famero de Itens Por P\u00e1gina', 'br');
//----------

// FEED
settext('$FEED_FEEDS', 'URL', 'br');
settext('$FEED_OAUTH_ACCESSTOKEN', 'Acces Token', 'br');
settext('$FEED_OAUTH_CLIENTID', 'Client ID', 'br');
settext('$FEED_OAUTH_CLIENTSECRET', 'Client Secret', 'br');
settext('$FEED_OAUTH_PROVIDER', 'Provider', 'br');
//----------

// FEEDBACK
settext('$KFEEDBACK_TYPES', 'Permitir tipos de Feedback', 'br');
settext('$KFEEDBACK_COMMENT_TYPE', 'Coment\u00e1rios', 'br');
settext('$KFEEDBACK_VOTE_TYPE', 'Votos', 'br');
settext('$KFEEDBACK_REVIEW_TYPE', 'Reviews', 'br');
settext('$WIDGET_CONFIG_FEEDBACK_DESCRIPTION', 'Coment\u00e1rios / Votos / Reviews', 'br');
//----------

// PANEL
//----------

// MAP
settext('$MAP_STREET_VIEW', 'Vista de Rua', 'br');
settext('$MAP_DOUBLE_CLICK_ZOOM', 'Zoom em duplo-click', 'br');
settext('$MAP_ZOOM', 'Zoom Inicial', 'br');
settext('$MAP_MAP_TYPE', 'Tipo de Mapa', 'br');
settext('$MAP_LATITUDE', 'Latitude', 'br');
settext('$MAP_LONGITUDE', 'Longitude', 'br');
settext('$MAP_DRAGGABLE_MARKERS', 'Marcadores Mov\u00edveis', 'br');
settext('$MAP_ISDEFAULT', 'Pr\u00e9-definido', 'br');
settext('$MAP_DEFAULT_MARKER_ICON', 'Icon Pr\u00e9-definido', 'br');

settext('$MAP_MARKER_LIST', 'Markers', 'br');

settext('$MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'br');
settext('$MAP_MARKER_LONGITUDE', 'Longitude', 'br');
settext('$MAP_MARKER_LATITUDE', 'Latitude', 'br');
settext('$MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'br');
settext('$MAP_MARKER_ICON', 'Icon', 'br');
settext('$MAP_MARKER_BALLON', 'Descri\u00e7\u00e3o do Bal\u00e3o', 'br');

settext('$MAP_MAP_TYPE_HIBRID', 'H\u00edbrido', 'br');
settext('$MAP_MAP_TYPE_ROADMAP', 'Mapa de estradas', 'br');
settext('$MAP_MAP_TYPE_SATELLITE', 'Sat\u00e9lite', 'br');
settext('$MAP_MAP_TYPE_TERRAIN', 'Terreno', 'br');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODACMSLocale_pt');

///////////////////////////////////////////////////////////////////
// FORMS

// WIDGETS GENERAL
// titles
settext('$HELP_KWIDGET_CHILDREN_LIST', '<i class="fa fa-cogs"></i> WIDGETS', 'br');
settext('$HELP_FEED_FEEDSCONFIG', '<i class="fa fa-rss"></i> Fontes de Dados', 'br');
settext('$HELP_RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'br');
settext('$HELP_TEMPLATE_DATA', '<i class="fa fa-magic"></i> ESQUEMA DE DADOS', 'br');
settext('$HELP_WIDGET_GRAPHICAL_ATTRIBUTES', '<i class="fa fa-eye"></i> Atributos Gr\u00e1ficos', 'br');
settext('$HELP_WIDGET_METADATA', '<i class="fa fa-flask"></i> METADATA', 'br');

// labels
settext('$HELP_WIDGET_GRAPHICS_LAYOUT', 'Layout', 'br');
settext('$HELP_WIDGET_RESOURCE_TEMPLATE', 'Template de Dados', 'br');
settext('$HELP_WIDGET_RESOURCE_CLASS', 'Widget Alvo', 'br');
settext('$HELP_WIDGET_CLASS', 'Classe CSS', 'br');
settext('$HELP_WIDGET_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$HELP_WIDGET_HASH', 'Identificador (hash)', 'br');
settext('$HELP_WIDGET_CONFIG_GRAPHICAL_DEFS', 'IMAGEM DE FUNDO', 'br');
settext('$HELP_WIDGET_GRAPHICS_BACKGROUND', 'Imagem de Fundo', 'br');

settext('$HELP_RESOURCE_FREETEXT', 'Termos', 'br');

settext('$HELP_LAYOUT_DATA', 'ESQUEMA GR\u00c1FICO', 'br');
settext('$HELP_LAYOUT', 'Escolha o Layout pretendido para este conte\u00fado', 'br');

settext('$HELP_DOCROOT_RESTSERVER', 'Servidor REST', 'br');
settext('$HELP_DOCROOT_DEFAULT_LANG', 'Idioma', 'br');
settext('$HELP_DOCROOT_UIROOT', 'Servidor HTTP', 'br');

settext('$HELP_LINK_REQUESTTYPES', 'Tipo de Widget', 'br');
settext('$HELP_LINK_TARGET_CHILD_CONTAINER', 'Layout Alvo', 'br');
settext('$HELP_LINK_TITLE', 'T\u00edtulo', 'br');

settext('$HELP_METADATA_DESCRIPTION', 'Descri\u00e7\u00e3o', 'br');
settext('$HELP_METADATA_DESIGNATION', 'Designa\u00e7\u00e3o', 'br');
settext('$HELP_METADATA_GEO_COORDINATES', 'Coordenadas GPS', 'br');
settext('$HELP_METADATA_URL', 'URL', 'br');
settext('$HELP_METADATA_LOCATION_NAME', 'Localidade', 'br');
settext('$HELP_METADATA_OBJECTTYPE', 'Tipo de Objecto (Opengraph)', 'br');
settext('$HELP_METADATA_PICTURE', 'Logo para share', 'br');

settext('$HELP_PERMISSIONS_GROUP', 'Grupo', 'br');
settext('$HELP_PERMISSIONS_VALUE', 'Valor', 'br');
//----------

// DOCROOT
//----------

// PAGE
settext('$HELP_LINK_RESOURCE_HASH', 'URL da P\u00e1gina', 'br');
//----------

// TEXT
settext('$HELP_TEXT_TITLE', 'T\u00edtulo', 'br');
settext('$HELP_TEXT_BODY', 'Corpo do Texto', 'br');
settext('$HELP_TEXT_LEAD', 'Lead', 'br');
//----------

// IMAGE
settext('$HELP_IMAGE_URL', 'Imagem', 'br');
settext('$HELP_IMAGE_SCALED_URL', 'Thumb da Imagem', 'br');
settext('$HELP_IMAGE_TITLE', 'LEGENDA', 'br');
//----------

// LINK
settext('$HELP_LINK_IMAGE', 'Imagem', 'br');
settext('$HELP_LINK_TARGET_WINDOW', 'Janela Alvo', 'br');
settext('$HELP_LINK_TOOLTIP', 'Tooltip', 'br');
settext('$HELP_LINK_URL', 'URL', 'br');
settext('$HELP_LINK_TARGET_SAME', 'Mesma Janela', 'br');
settext('$HELP_LINK_TARGET_OTHER', 'Nova Janela', 'br');
//----------

// HIGHLIGHT
settext('$HELP_KHIGHLIGHT_ACTION_LIST', '<i class="fa fa-external-link"></i> Ac\u00e7\u00f5es', 'br');
settext('$HELP_HIGHLIGTH_ACTIONS', 'AC\u00c7\u00d5ES', 'br');
settext('$HELP_KLIST_LABEL_ACTIONS', 'AC\u00c7\u00d5ES  \u00bb ', 'br');
//----------

// MENU
settext('$HELP_KLISTITEM_ADD_ACTION', 'Gerir Sub-Menu', 'br');

settext('$HELP_KMENU_PAGES_PANEL', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'br');
settext('$HELP_KMENULIST_NEWENTRY', 'NOVA ENTRADA', 'br');

settext('$HELP_MENU_WIDGET_PAGES', '<i class="fa fa-sitemap"></i> P\u00c1GINAS', 'br');
settext('$HELP_MENU_HASH', 'HASH', 'br');
settext('$HELP_MENU_HREF', 'HREF', 'br');
settext('$HELP_MENU_TITLE', 'T\u00edtulo do Menu', 'br');
settext('$HELP_MENU_CLASS', 'Classe de CSS', 'br');
settext('$HELP_MENU_INTERNAL_PAGE_INFO', 'Seleccione uma p\u00e1gina do site', 'br');
settext('$HELP_MENU_EXTERNAL_PAGE_INFO', 'ou insira o url de uma p\u00e1gina externa', 'br');

//----------

// LIST
settext('$HELP_CONFIG_LIST_AUTO_ROTATE', 'Per\u00edodo de Rota\u00e7\u00e3o Autom\u00e1tica de Listagem', 'br');
settext('$HELP_LIST_WIDGET_PAGESIZE', 'N\u00famero de Itens Por P\u00e1gina', 'br');
//----------

// FEED
settext('$HELP_FEED_FEEDS', 'URL', 'br');
settext('$HELP_FEED_OAUTH_ACCESSTOKEN', 'Acces Token', 'br');
settext('$HELP_FEED_OAUTH_CLIENTID', 'Client ID', 'br');
settext('$HELP_FEED_OAUTH_CLIENTSECRET', 'Client Secret', 'br');
settext('$HELP_FEED_OAUTH_PROVIDER', 'Provider', 'br');
//----------

// FEEDBACK
settext('$HELP_KFEEDBACK_TYPES', 'Permitir tipos de Feedback', 'br');
settext('$HELP_KFEEDBACK_COMMENT_TYPE', 'Coment\u00e1rios', 'br');
settext('$HELP_KFEEDBACK_VOTE_TYPE', 'Votos', 'br');
settext('$HELP_KFEEDBACK_REVIEW_TYPE', 'Reviews', 'br');
settext('$HELP_WIDGET_CONFIG_FEEDBACK_DESCRIPTION', 'Coment\u00e1rios / Votos / Reviews', 'br');
//----------

// PANEL
//----------

// MAP
settext('$HELP_MAP_STREET_VIEW', 'Vista de Rua', 'br');
settext('$HELP_MAP_DOUBLE_CLICK_ZOOM', 'Zoom em duplo-click', 'br');
settext('$HELP_MAP_ZOOM', 'Zoom Inicial', 'br');
settext('$HELP_MAP_MAP_TYPE', 'Tipo de Mapa', 'br');
settext('$HELP_MAP_LATITUDE', 'Latitude', 'br');
settext('$HELP_MAP_LONGITUDE', 'Longitude', 'br');
settext('$HELP_MAP_DRAGGABLE_MARKERS', 'Marcadores Mov\u00edveis', 'br');
settext('$HELP_MAP_ISDEFAULT', 'Pr\u00e9-definido', 'br');
settext('$HELP_MAP_DEFAULT_MARKER_ICON', 'Icon Pr\u00e9-definido', 'br');

settext('$HELP_MAP_MARKER_LIST', 'Markers', 'br');

settext('$HELP_MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'br');
settext('$HELP_MAP_MARKER_LONGITUDE', 'Longitude', 'br');
settext('$HELP_MAP_MARKER_LATITUDE', 'Latitude', 'br');
settext('$HELP_MAP_MARKER_DRAGGABLE', 'Mov\u00edvel', 'br');
settext('$HELP_MAP_MARKER_ICON', 'Icon', 'br');
settext('$HELP_MAP_MARKER_BALLON', 'Descri\u00e7\u00e3o do Bal\u00e3o', 'br');

settext('$HELP_MAP_MAP_TYPE_HIBRID', 'H\u00edbrido', 'br');
settext('$HELP_MAP_MAP_TYPE_ROADMAP', 'Mapa de estradas', 'br');
settext('$HELP_MAP_MAP_TYPE_SATELLITE', 'Sat\u00e9lite', 'br');
settext('$HELP_MAP_MAP_TYPE_TERRAIN', 'Terreno', 'br');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


KSystem.included('CODACMSLocale_Help_pt');

