KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAAnalyticsStatisticsChart(parent, data) {
	if (parent != null) {
		CODAGraphForm.call(this, parent);
		this.data = data;
		this.filterFields = null;
		this.filterValue = [];
		this.headers = {};
		this.helpURL = CODAGenericShell.generateHelpURL(this);

	}
}

KSystem.include([
	'CODAGraphForm'
], function() {
	CODAAnalyticsStatisticsChart.prototype = new CODAGraphForm();
	CODAAnalyticsStatisticsChart.prototype.constructor = CODAAnalyticsStatisticsChart;

	CODAAnalyticsStatisticsChart.prototype.visible = function() {
		var display = this.display;
		CODAGraphForm.prototype.visible.call(this);
		
		if (!!display) {
			return;
		}
		
		var id = this.id;
		var graphs = this.graphs;
		var self = this;
		google.load('visualization', '1.0', {'packages':['corechart', 'table'], callback : function() { 
				self.loadCharts(); 
				self.drawCharts(); 
			} 
		});
	};
	
	CODAAnalyticsStatisticsChart.prototype.loadCharts = function() {		
		this.graphs = {};
		
		var pageviews_chart = this.processPageviewsData();
		
		this.graphs['graph1'] = { data : pageviews_chart.chart_data, options : pageviews_chart.chart_options, type : pageviews_chart.chart_type};	

		var events_chart = this.processEventsData();

		if (events_chart != false){
			var formatter = new google.visualization.ColorFormat();
			formatter.addRange(100, null, 'blue', 'white');
			formatter.format(events_chart.chart_data, 3);
			formatter.format(events_chart.chart_data, 4);

			this.graphs['graph2'] = { data : events_chart.chart_data, options : events_chart.chart_options, type : events_chart.chart_type};	
		}

	};

	CODAAnalyticsStatisticsChart.prototype.drawCharts = function() {
		
		for (var gid in this.graphs) {
			var chart_div = window.document.createElement('div');
			chart_div.style.display = 'inline-block';
			chart_div.id = this.id + '_' + gid;
			this.content.appendChild(chart_div);

			var chart = null;

			switch (this.graphs[gid].type){
				case 'LineChart' : 
				chart = new google.visualization.LineChart(chart_div);
				break;
				case 'PieChart' :
				chart = new google.visualization.PieChart(chart_div);
				break;
				case 'Table' : 
				chart = new google.visualization.Table(chart_div);
				break;
			}

			if (chart != null){
				chart.draw(this.graphs[gid].data, this.graphs[gid].options);		
			}

		}

	};

	CODAAnalyticsStatisticsChart.prototype.processPageviewsData = function() {

		var chart_data = new Array();
		chart_data[0] = new Array();
		chart_data[0][0] = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_PAGES');
		chart_data[0][1] = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_VISITS');

		if(!!this.data.pageviews.rows){
			var i = 1;
			for(var j = 0; j < this.data.pageviews.rows.length; j++){
				chart_data[i] = new Array();
				chart_data[i][0] = this.data.pageviews.rows[j][2];
				chart_data[i][1] = Number(this.data.pageviews.rows[j][3]);
				i++;				
			}			
		}else{
			chart_data[1] = new Array();
			chart_data[1][0] = '';
			chart_data[1][1] = 1;
		}
		
		var data = new google.visualization.arrayToDataTable(chart_data);
		if (!!this.data.pageviews.rows){
			title = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES1') + this.data.pageviews.query['start-date'] + gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES2') + this.data.pageviews.query['end-date'];
		}else{
			title = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_NO_DATA');
		}
		var options = {
			'title': title,
			'width': !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 120),
			'height': 400,
			'legend': { position: 'right'}
		};

		var type = 'PieChart';

		var return_data = {};
		return_data.chart_data = data;
		return_data.chart_options = options;
		return_data.chart_type = type;

		return return_data;
	}

	CODAAnalyticsStatisticsChart.prototype.processEventsData = function() {

		var chart_data = new Array();
		chart_data[0] = new Array();
		chart_data[0][0] = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_PAGES');
		chart_data[0][1] = gettext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_VISITS');

		var data = new google.visualization.DataTable();
		var no_table_data = false;
		if(!!this.data.events.columnHeaders){

			for (var i = 0; i < this.data.events.columnHeaders.length; i++){
				
				var column_data_name;

				switch (this.data.events.columnHeaders[i]['name']){

					case 'ga:eventAction' : {
						column_data_name = gettext('$ANALYTICS_STATISTICS_EVENTS_EVENT_ACTION_LABEL');
						break;
					}
					case 'ga:eventCategory' : {
						column_data_name = gettext('$ANALYTICS_STATISTICS_EVENTS_EVENT_CATEGORY_LABEL');
						break;
					}
					case 'ga:eventLabel' : {
						column_data_name = gettext('$ANALYTICS_STATISTICS_EVENTS_EVENT_LABEL_LABEL');
						break;
					}
					case 'ga:totalEvents' : {
						column_data_name = gettext('$ANALYTICS_STATISTICS_EVENTS_TOTAL_EVENTS_LABEL');
						break;
					}
					case 'ga:uniqueEvents' : {
						column_data_name = gettext('$ANALYTICS_STATISTICS_EVENTS_UNIQUE_EVENTS_LABEL');
						break;
					}
					default : {
						column_data_name = this.data.events.columnHeaders[i]['name'];
						break;
					}

				}

				if(!!this.data.events.rows){
					var column_data_type;
					
					switch (this.data.events.columnHeaders[i]['dataType']) {

						case 'INTEGER' : {
							column_data_type = 'number'; 
							break;
						}
						case 'FLOAT' : {
							column_data_type = 'number';
							break;
						}
						case 'STRING' : {
							column_data_type = 'string';
							break;
						}
						default : {
							column_data_type = 'string';
							break;
						}

					}
					data.addColumn(column_data_type, column_data_name);	
				}else{
					data.addColumn('string', column_data_name);
				}
			}

		}else{
			no_table_data = true;
			//data.addColumn('string', gettext('$ANALYTICS_STATISTICS_EVENTS_TABLE_NODATA'));
		}

		var data_rows = new Array();
		
		if(!!this.data.events.rows){

			for (var i = 0; i < this.data.events.rows.length; i++){
				
				data_rows[i] = new Array();

				for (var j = 0; j < this.data.events.rows[i].length; j++){
					
					if (this.data.events.columnHeaders[j]['name'] == 'ga:eventAction'){
						data_rows[i][j] = gettext('$ANALYTICS_STATISTICS_EVENTS_ACTION_' + this.data.events.rows[i][j].substr(this.data.events.rows[i][j].lastIndexOf('/')+1).toUpperCase());
						if (data_rows[i][j].substr(0,1) == '$'){
							data_rows[i][j] = this.data.events.rows[i][j];
						}
					}else if (this.data.events.columnHeaders[j]['name'] == 'ga:eventCategory'){
						data_rows[i][j] = gettext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_' + this.data.events.rows[i][j].substr(this.data.events.rows[i][j].lastIndexOf('/')+1).toUpperCase());
						if (data_rows[i][j].substr(0,1) == '$'){
							data_rows[i][j] = this.data.events.rows[i][j];
						}
					}else if (this.data.events.columnHeaders[j]['name'] == 'ga:eventLabel'){
						data_rows[i][j] = gettext('$ANALYTICS_STATISTICS_EVENTS_LABEL_' + this.data.events.rows[i][j].substr(this.data.events.rows[i][j].lastIndexOf('/')+1).toUpperCase());
						if (data_rows[i][j].substr(0,1) == '$'){
							data_rows[i][j] = this.data.events.rows[i][j];
						}
					}else{
						if (this.data.events.columnHeaders[j]['dataType'] == 'INTEGER' || this.data.events.columnHeaders[j]['dataType'] == 'FLOAT' ){
							data_rows[i][j] = Number(this.data.events.rows[i][j]);
						}else{
							data_rows[i][j] = this.data.events.rows[i][j];
						}
					}
				}

			}

		}else{
			data_rows[0] = new Array();
			
			if(!!this.data.events.columnHeaders){
				for (var i = 0; i < this.data.events.columnHeaders.length; i++){
					data_rows[0][i] = gettext('$ANALYTICS_STATISTICS_EVENTS_TABLE_NODATA');
				}	
			}else{
				//data_rows[0][0] = gettext('$ANALYTICS_STATISTICS_EVENTS_TABLE_NODATA');
			}
		}

		data.addRows(data_rows);

		var options = {
			'width': !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 150),
			'allowHtml': true, 
			'showRowNumber': true
		};

		var type = 'Table';

		var return_data = {};
		return_data.chart_data = data;
		return_data.chart_options = options;
		return_data.chart_type = type;
		
		if (no_table_data == true){
			return false;			
		}

		return return_data;

	};
	
	KSystem.included('CODAAnalyticsStatisticsChart');
}, Kinky.CODA_INCLUDER_URL);
function CODAGeneralStatisticsChart(parent, data, statistics_type) {
	if (parent != null) {
		CODAGraphForm.call(this, parent);
		this.data = data;
		this.filterFields = null;
		this.filterValue = [];
		this.headers = {};
		this.statistics_type = statistics_type;
		this.helpURL = CODAGenericShell.generateHelpURL(this);
	}
}

KSystem.include([
	'CODAGraphForm'
], function() {
	CODAGeneralStatisticsChart.prototype = new CODAGraphForm();
	CODAGeneralStatisticsChart.prototype.constructor = CODAGeneralStatisticsChart;

	CODAGeneralStatisticsChart.prototype.visible = function() {
		var display = this.display;
		CODAGraphForm.prototype.visible.call(this);

		if (!!display) {
			return;
		}

		var id = this.id;
		var graphs = this.graphs;
		var self = this;
		google.load('visualization', '1.0', {'packages':['corechart'], callback : function() { 
				self.loadCharts(); 
				self.drawCharts(); 
			} 
		});
	};

	CODAGeneralStatisticsChart.prototype.loadCharts = function() {		
		this.graphs = {};

		if (!!this.statistics_type){
			if (this.statistics_type == "participations"){

				var participations_chart = this.processParticipationsData();

				this.graphs['graph1'] = { data : participations_chart.chart_data, options : participations_chart.chart_options, type : participations_chart.chart_type};

			}
			else if (this.statistics_type == "participants" || this.statistics_type == "customers"){
				CODAStatisticsShell.cur_width = !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 120)

				var age_interval1 = 0;
				var age_interval1_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL1');
				var age_interval1_male = 0;
				var age_interval1_female = 0;
				var age_interval1_unknown = 0;
				var age_interval2 = 0;
				var age_interval2_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL2');
				var age_interval2_male = 0;
				var age_interval2_female = 0;
				var age_interval2_unknown = 0;
				var age_interval3 = 0;
				var age_interval3_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL3');
				var age_interval3_male = 0;
				var age_interval3_female = 0;
				var age_interval3_unknown = 0;
				var age_interval4 = 0;
				var age_interval4_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL4');
				var age_interval4_male = 0;
				var age_interval4_female = 0;
				var age_interval4_unknown = 0;
				var age_interval5 = 0;
				var age_interval5_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL5');
				var age_interval5_male = 0;
				var age_interval5_female = 0;
				var age_interval5_unknown = 0;
				var age_interval6 = 0;
				var age_interval6_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL6');
				var age_interval6_male = 0;
				var age_interval6_female = 0;
				var age_interval6_unknown = 0;
				var age_interval7 = 0;
				var age_interval7_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL7');
				var age_interval7_male = 0;
				var age_interval7_female = 0;
				var age_interval7_unknown = 0;
				var age_interval8 = 0;
				var age_interval8_label = gettext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL8');
				var age_interval8_male = 0;
				var age_interval8_female = 0;
				var age_interval8_unknown = 0;
				var gender_male = 0;
				var gender_female = 0;
				var gender_notavailable = 0;

				var all_locations = new Array();

				var email_marketing_allows = 0;
				var email_marketing_notallows = 0;
				var email_marketing_notdefined = 0;


				if(!!this.data.participants){

					var participants = this.data.participants;

					var chart_data = new Array();

					var today = new Date();

					for (var i = 0; i < participants.length; i++){

						var gender = null;
						if(!!participants[i].gender){
							if (participants[i].gender == "male"){
								gender_male++;
							}else if (participants[i].gender == "female"){
								gender_female++;
							}
							gender = participants[i].gender;
						}else{
							gender_notavailable++
							gender = "gender_notavailable";
						}

						if(!!participants[i].birthday && participants[i].birthday != ""){

							var participant_birthday = new Date(participants[i].birthday);
							var ageDifMs = Date.now() - participant_birthday.getTime();
							var ageDate = new Date(ageDifMs);
							var age = Math.abs(ageDate.getUTCFullYear() - 1970);

							if(age <= 17){
								age_interval1++;
								if (gender == "male"){
									age_interval1_male++;
								}else if(gender == "female"){
									age_interval1_female++;
								}else{
									age_interval1_unknown++;
								}	
							}else if(age >= 18 && age <= 24){
								age_interval2++;
								if (gender == "male"){
									age_interval2_male++;
								}else if(gender == "female"){
									age_interval2_female++;
								}else{
									age_interval2_unknown++;
								}							
							}else if(age >= 25 && age <= 34){
								age_interval3++;
								if (gender == "male"){
									age_interval3_male++;
								}else if(gender == "female"){
									age_interval3_female++;
								}else{
									age_interval3_unknown++;
								}
							}else if(age >= 35 && age <= 44){
								age_interval4++;
								if (gender == "male"){
									age_interval4_male++;
								}else if(gender == "female"){
									age_interval4_female++;
								}else{
									age_interval4_unknown++;
								}
							}else if(age >= 45 && age <= 54){
								age_interval5++;
								if (gender == "male"){
									age_interval5_male++;
								}else if(gender == "female"){
									age_interval5_female++;
								}else{
									age_interval5_unknown++;
								}
							}else if(age >= 24 && age <= 64){
								age_interval6++;
								if (gender == "male"){
									age_interval6_male++;
								}else if(gender == "female"){
									age_interval6_female++;
								}else{
									age_interval6_unknown++;
								}
							}else if(age >= 65){
								age_interval7++;
								if (gender == "male"){
									age_interval7_male++;
								}else if(gender == "female"){
									age_interval7_female++;
								}else{
									age_interval7_unknown++;
								}
							}

						}else{

							age_interval8++;

						}

						if(participants[i].location_id != "" && participants[i].location_name != ""){

							if(participants[i].location_name.indexOf(",") != -1){
								participants[i].location_name = participants[i].location_name.replace(/ /g,'').split(",");
								participants[i].location_name = participants[i].location_name[participants[i].location_name.length - 2].toString();
							}

							if(all_locations.length > 0){
								var found = false;
								for (var j = 0; j < all_locations.length; j++){

									if(participants[i].location_name.toLowerCase() == all_locations[j][0]){
										found = true;
										all_locations[j][1] = all_locations[j][1] + 1; 
										break;
									}			

								}

								if (found == false){
									all_locations[j] = new Array();
									all_locations[j][0] = participants[i].location_name.toLowerCase();
									all_locations[j][1] = 1;	
								}

							}else{
								all_locations[0] = new Array();
								all_locations[0][0] = participants[i].location_name.toLowerCase();
								all_locations[0][1] = 1;
							}

						}

						if(participants[i].email_marketing != ""){
							if(participants[i].email_marketing == "allows"){
								email_marketing_allows++;
							}else if(participants[i].email_marketing == "does not allow"){
								email_marketing_notallows++;
							}
						}else{
							//email_marketing_notdefined++;
							email_marketing_notallows++;
						}
						
					}
				}
				
				{

					var chart_data = new Array();
					chart_data[0] = new Array();
					chart_data[0][0] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_AGE');
					chart_data[0][1] = title =  this.statistics_type == "participants" ? gettext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_LOCATION_CHART_LABEL_CUSTOMERS') : '';
					chart_data[1] = new Array();
					chart_data[1][0] = age_interval2_label;
					chart_data[1][1] = age_interval1_male + age_interval1_female;
					chart_data[2] = new Array();
					chart_data[2][0] = age_interval2_label;
					chart_data[2][1] = age_interval2_male + age_interval2_female;
					chart_data[3] = new Array();
					chart_data[3][0] = age_interval3_label;
					chart_data[3][1] = age_interval3_male + age_interval3_female;
					chart_data[4] = new Array();
					chart_data[4][0] = age_interval4_label;
					chart_data[4][1] = age_interval4_male + age_interval4_female;
					chart_data[5] = new Array();
					chart_data[5][0] = age_interval5_label;
					chart_data[5][1] = age_interval5_male + age_interval5_female;
					chart_data[6] = new Array();
					chart_data[6][0] = age_interval6_label;
					chart_data[6][1] = age_interval6_male + age_interval6_female;
					chart_data[7] = new Array();
					chart_data[7][0] = age_interval7_label;
					chart_data[7][1] = age_interval7_male + age_interval7_female;
					if(age_interval8_male > 0 || age_interval8_female > 0){
						chart_data[8] = new Array();
						chart_data[8][0] = age_interval8_label;
						chart_data[8][1] = age_interval8_male + age_interval8_female;
					}			

					var data = new google.visualization.arrayToDataTable(chart_data);

					var title = null;
					if (!!this.data.participants){
						title = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS');
					}else{
						title =  this.statistics_type == "participants" ? gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS') : '';
					}
					var options = {
						'title': title,
						'width':400,
						'height':400,
						'legend': { position: 'bottom'}
					};

					var type = 'ColumnChart';

					this.graphs['graph1'] = { data : data, options : options, type : type};

				}

				{

					chart_data = new Array();
					chart_data[0] = new Array();
					chart_data[0][0] = gettext('$PARTICIPANTS_STATISTICS_GENDER_CHART_LABEL_GENDER');
					chart_data[0][1] = gettext('$PARTICIPANTS_STATISTICS_GENDER_CHART_LABEL_GENDER');
					chart_data[1] = new Array();
					chart_data[1][0] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_MALE');
					chart_data[1][1] = gender_male;					
					chart_data[2] = new Array();
					chart_data[2][0] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_FEMALE');
					chart_data[2][1] = gender_female;
					chart_data[3] = new Array();
					chart_data[3][0] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE');
					chart_data[3][1] = gender_notavailable;

					var data = new google.visualization.arrayToDataTable(chart_data);

					var title = null;
					if (!!this.data.participants){
						title = gettext('$PARTICIPANTS_STATISTICS_GENDER_CHART_LABEL_GENDER');
					}else{
						title =  this.statistics_type == "participants" ? gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS') : '';
					}
					var options = {
						'title': title,
						'width':400,
						'height':400,
						'legend': { position: 'bottom'}
					};

					var type = 'PieChart';

					this.graphs['graph2'] = { data : data, options : options, type : type};

				}

				{

					chart_data[0] = new Array();
					chart_data[0][0] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_AGE');
					chart_data[0][1] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_MALE');
					chart_data[0][2] = gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_FEMALE');
					chart_data[1] = new Array();
					chart_data[1][0] = age_interval1_label;
					chart_data[1][1] = age_interval1_male;
					chart_data[1][2] = age_interval1_female;
					chart_data[2] = new Array();
					chart_data[2][0] = age_interval2_label;
					chart_data[2][1] = age_interval2_male;
					chart_data[2][2] = age_interval2_female;
					chart_data[3] = new Array();
					chart_data[3][0] = age_interval3_label;
					chart_data[3][1] = age_interval3_male;
					chart_data[3][2] = age_interval3_female;
					chart_data[4] = new Array();
					chart_data[4][0] = age_interval4_label;
					chart_data[4][1] = age_interval4_male;
					chart_data[4][2] = age_interval4_female;
					chart_data[5] = new Array();
					chart_data[5][0] = age_interval5_label;
					chart_data[5][1] = age_interval5_male;
					chart_data[5][2] = age_interval5_female;
					chart_data[6] = new Array();
					chart_data[6][0] = age_interval6_label;
					chart_data[6][1] = age_interval6_male;
					chart_data[6][2] = age_interval6_female;
					chart_data[7] = new Array();
					chart_data[7][0] = age_interval7_label;
					chart_data[7][1] = age_interval7_male;
					chart_data[7][2] = age_interval7_female;
					if(age_interval8_male > 0 || age_interval8_female > 0){
						chart_data[8] = new Array();
						chart_data[8][0] = age_interval8_label;
						chart_data[8][1] = age_interval8_male;
						chart_data[8][2] = age_interval8_female;	
					}								

					var data = new google.visualization.arrayToDataTable(chart_data);

					var title = null;
					if (!!this.data.participants){
						title = gettext('$PARTICIPANTS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS');
					}else{
						title =  this.statistics_type == "participants" ? gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS') : '';
					}
					var options = {
						'title': title,
						'width':400,
						'height':400,
						'legend': { position: 'bottom'}
					};

					var type = 'ColumnChart';

					this.graphs['graph3'] = { data : data, options : options, type : type};

				}

				{

					if (all_locations.length > 0){
						all_locations.sort(function(a,b){return parseInt(a[1],10) - parseInt(b[1],10)}).reverse();

						if (all_locations.length > 5){
							all_locations = all_locations.slice(0,4)
						}

						all_locations.unshift([gettext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_LOCATION'), gettext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS')]);

						var data = new google.visualization.arrayToDataTable(all_locations);
						var title = gettext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_TITLE');

						var options = {
							'title': title,
							'width':400,
							'height':400,
							'legend': { position: 'bottom'}
						};

						var type = 'PieChart';

						this.graphs['graph4'] = { data : data, options : options, type : type};

					}

				}

				{

					var emailmarketing_data = new Array();
					emailmarketing_data[0] = new Array();
					emailmarketing_data[0][0] = gettext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_DATAUSE');
					emailmarketing_data[0][1] = gettext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZATION');
					emailmarketing_data[1] = new Array();
					emailmarketing_data[1][0] = gettext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZE');
					emailmarketing_data[1][1] = email_marketing_allows;
					emailmarketing_data[2] = new Array();
					emailmarketing_data[2][0] = gettext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_NOAUTHORIZE');
					emailmarketing_data[2][1] = email_marketing_notallows;

					var data = new google.visualization.arrayToDataTable(emailmarketing_data);

					if (!!this.data.participants){
						title = gettext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_TITLE');
					}else{
						title = this.statistics_type == "participants" ? gettext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS') : this.statistics_type == "customers" ? gettext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS') : '';
					}

					var options = {
						'title': title,
						'width':400,
						'height':400,
						'legend': { position: 'bottom'}
					};

					var type = 'PieChart';

					this.graphs['graph5'] = { data : data, options : options, type : type};

				}
			}else if (this.statistics_type == "carts"){

				var carts_chart = this.processCartsData();

				this.graphs['graph6'] = { data : carts_chart.chart_data, options : carts_chart.chart_options, type : carts_chart.chart_type};

				var carts_details_charts = this.processCartsDataDetails();

				for (one_detail_chart in carts_details_charts){

					dump(carts_details_charts[one_detail_chart]);

				}

			}
		}		
	};

	CODAGeneralStatisticsChart.prototype.drawCharts = function() {

		for (var gid in this.graphs) {
			var chart_div = window.document.createElement('div');
			chart_div.style.display = 'inline-block';
			chart_div.id = this.id + '_' + gid;
			this.content.appendChild(chart_div);

			var chart = null;

			switch (this.graphs[gid].type){
				case 'LineChart' : 
				chart = new google.visualization.LineChart(chart_div);
				break;
				case 'PieChart' :
				chart = new google.visualization.PieChart(chart_div);
				break;
				case 'ColumnChart' :
				chart = new google.visualization.ColumnChart(chart_div);
				break;
			}

			if (chart != null){
				chart.draw(this.graphs[gid].data, this.graphs[gid].options);
			}

		}

	};

	CODAGeneralStatisticsChart.prototype.processParticipationsData = function() {

		var chart_data = new Array();
		chart_data[0] = new Array();
		chart_data[0][0] = gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_DATE');
		chart_data[0][1] = gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_PARTICIPATIONS');

		if(!!this.data.participations){
			var participations = this.data.participations;
			var first_part_date = participations[0].start_date;
			var last_part_date = participations[participations.length - 1].start_date;

			var first_date = new Date(first_part_date.substr(0,4), first_part_date.substr(5,2) - 1, first_part_date.substr(8,2));
			var last_date = new Date(last_part_date.substr(0,4), last_part_date.substr(5,2) - 1, last_part_date.substr(8,2));

			var oneDay = 24*60*60*1000;
			var diffDays = Math.round(Math.abs((first_date.getTime() - last_date.getTime())/(oneDay)));

			var day_interval = 0;
			if(diffDays <= 5){
				day_interval = 1;
			}else if(diffDays <= 30){
				day_interval = 2;
			}else if(diffDays <= 60){
				day_interval = 4;
			}else if(diffDays <= 120){
				day_interval = 8;
			}else{
				day_interval = 10;
			}

			var i = 0;
			var index = 1;
			while (i <= diffDays){
				var next_start_date = null;
				var index_date = null;
				chart_data[index] = new Array();
				if (i+day_interval > diffDays && i != 0){
					chart_data[index][0] = last_part_date.substr(0,4) + '-' +  last_part_date.substr(5,2)  + '-' + last_part_date.substr(8,2);
					index_date = new Date(last_part_date.substr(0,4) + '-' +  last_part_date.substr(5,2)  + '-' + last_part_date.substr(8,2));
				}else{
					if(i==0){
						index_date = new Date(first_part_date.substr(0,4) + '-' +  first_part_date.substr(5,2) + '-' + first_part_date.substr(8,2));
						index_date.setDate(index_date.getDate());
					}else{
						index_date = new Date(chart_data[index-1][0]);
						index_date.setDate(index_date.getDate()+day_interval);
					}							

					var day = index_date.getDate();
					var month = index_date.getMonth() + 1;
					var year = index_date.getFullYear();
					chart_data[index][0] = year + '-' +  (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day :day);
					next_start_date = chart_data[index][0];
				}

				var total_part = 0;
				for(var j = 0; j < participations.length; j++){
					var participation_date = new Date(participations[j].start_date.substr(0,4), participations[j].start_date.substr(5,2) - 1, participations[j].start_date.substr(8,2));
					if (i==0){
						if (participation_date <= new Date(index_date)){
							total_part++;
						}
					}else{
						if (participation_date <= new Date(index_date) && participation_date > new Date(chart_data[index-1][0])){
							total_part++;
						}	
					}
				}
				chart_data[index][1] = total_part;
				index++;
				i = i+day_interval;
			}

		}else{
			chart_data[1] = new Array();
			chart_data[1][0] = '';
			chart_data[1][1] = 0;
		}

		var data = new google.visualization.arrayToDataTable(chart_data);

		var title = null;
		if (!!this.data.participations){
			title =  gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS1') + chart_data[1][0] + gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS2') + chart_data[chart_data.length - 1][0];
		}else{
			title = gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_NO_PARTICIPATIONS');
		}

		var options = {
			'title': title,
			'width': !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 120),
			'height': 600,
			'legend': { position: 'bottom'}
		};

		var type = 'LineChart';

		var return_data = {};
		return_data.chart_data = data;
		return_data.chart_options = options;
		return_data.chart_type = type;

		return return_data;

	};

	CODAGeneralStatisticsChart.prototype.processCartsData = function() {

		var chart_data = new Array();
		chart_data[0] = new Array();
		chart_data[0][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_DATE');
		chart_data[0][1] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_CARTS');

		if(!!this.data.carts){
			var carts = this.data.carts;
			var first_part_date = carts[0].status_date;
			var last_part_date = carts[carts.length - 1].status_date;

			var first_date = new Date(first_part_date.substr(0,4), first_part_date.substr(5,2) - 1, first_part_date.substr(8,2));
			var last_date = new Date(last_part_date.substr(0,4), last_part_date.substr(5,2) - 1, last_part_date.substr(8,2));

			var oneDay = 24*60*60*1000;
			var diffDays = Math.round(Math.abs((first_date.getTime() - last_date.getTime())/(oneDay)));

			var day_interval = 0;
			if(diffDays <= 5){
				day_interval = 1;
			}else if(diffDays <= 30){
				day_interval = 2;
			}else if(diffDays <= 60){
				day_interval = 4;
			}else if(diffDays <= 120){
				day_interval = 8;
			}else{
				day_interval = 10;
			}

			var i = 0;
			var index = 1;
			while (i <= diffDays){
				var next_status_date = null;
				var index_date = null;
				chart_data[index] = new Array();
				if (i+day_interval > diffDays && i != 0){
					chart_data[index][0] = last_part_date.substr(0,4) + '-' +  last_part_date.substr(5,2)  + '-' + last_part_date.substr(8,2);
					index_date = new Date(last_part_date.substr(0,4) + '-' +  last_part_date.substr(5,2)  + '-' + last_part_date.substr(8,2));
				}else{
					if(i==0){
						index_date = new Date(first_part_date.substr(0,4) + '-' +  first_part_date.substr(5,2) + '-' + first_part_date.substr(8,2));
						index_date.setDate(index_date.getDate());
					}else{
						index_date = new Date(chart_data[index-1][0]);
						index_date.setDate(index_date.getDate()+day_interval);
					}							

					var day = index_date.getDate();
					var month = index_date.getMonth() + 1;
					var year = index_date.getFullYear();
					chart_data[index][0] = year + '-' +  (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day :day);
					next_status_date = chart_data[index][0];
				}

				var total_part = 0;
				for(var j = 0; j < carts.length; j++){
					var cart_date = new Date(carts[j].status_date.substr(0,4), carts[j].status_date.substr(5,2) - 1, carts[j].status_date.substr(8,2));
					if (i==0){
						if (cart_date <= new Date(index_date)){
							total_part++;
						}
					}else{
						if (cart_date <= new Date(index_date) && cart_date > new Date(chart_data[index-1][0])){
							total_part++;
						}	
					}

				}
				chart_data[index][1] = total_part;
				index++;
				i = i+day_interval;
			}

		}else{
			chart_data[1] = new Array();
			chart_data[1][0] = '';
			chart_data[1][1] = 0;
		}

		var data = new google.visualization.arrayToDataTable(chart_data);

		var title = null;
		if (!!this.data.carts){
			title =  gettext('$CARTS_STATISTICS_CARTS_CHART_TITLE_CARTS1') + chart_data[1][0] + gettext('$CARTS_STATISTICS_CARTS_CHART_TITLE_CARTS2') + chart_data[chart_data.length - 1][0];
		}else{
			title = gettext('$CARTS_STATISTICS_CARTS_CHART_TITLE_NO_CARTS');
		}

		var options = {
			'title': title,
			'width': !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 120),
			'height': 600,
			'legend': { position: 'bottom'}
		};

		var type = 'LineChart';

		var return_data = {};
		return_data.chart_data = data;
		return_data.chart_options = options;
		return_data.chart_type = type;

		return return_data;

	};

	CODAGeneralStatisticsChart.prototype.processCartsDataDetails = function() {

		var chart_status_data = new Array();
		var status_provisional = 0;
		var status_inserted = 0;
		var status_dispatched = 0;
		var status_sent = 0;
		var status_canceled = 0;
		
		chart_status_data[0] = new Array();
		chart_status_data[0][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_STATUS');
		chart_status_data[0][1] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_STATUS_LABEL');

		if(!!this.data.carts){
			
			var carts = this.data.carts;

			for(var i = 0; i < carts.length; i++){
				
				switch (carts[i].status) {

					case 'provisional' : {
						status_provisional++;
						break;
					}
					case 'inserted' : {
						status_inserted++;
						break;
					}
					case 'dispatched' : {
						status_dispatched++;
						break;
					}
					case 'sent' : {
						status_sent++;
						break;
					}
					case 'canceled' : {
						status_canceled++;
						break;
					}

				}

			}						

		}

		chart_status_data[1] = new Array();
		chart_status_data[1][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_PROVISIONAL');
		chart_status_data[1][1] = status_provisional;
		chart_status_data[2] = new Array();
		chart_status_data[2][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_INSERTED');
		chart_status_data[2][1] = status_inserted;
		chart_status_data[3] = new Array();
		chart_status_data[3][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_PROVISIONAL');
		chart_status_data[3][1] = status_dispatched;
		chart_status_data[4] = new Array();
		chart_status_data[4][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_INSERTED');
		chart_status_data[4][1] = status_sent;
		chart_status_data[5] = new Array();
		chart_status_data[5][0] = gettext('$CARTS_STATISTICS_STATUS_CHART_LABEL_PROVISIONAL');
		chart_status_data[5][1] = status_canceled;

		var status_data = new google.visualization.arrayToDataTable(chart_status_data);

		var status_title = null;
		if (!!this.data.carts){
			//title =  gettext('$CARTS_STATISTICS_STATUS_CHART_TITLE_CARTS1') + chart_data[1][0] + gettext('$CARTS_STATISTICS_STATUS_CHART_TITLE_CARTS2') + chart_data[chart_data.length - 1][0];
			title =  gettext('$CARTS_STATISTICS_STATUS_CHART_TITLE_CARTS1');
		}else{
			title = gettext('$CARTS_STATISTICS_STATUS_CHART_TITLE_NO_CARTS');
		}

		var status_options = {
			'title': title,
			'width': !!CODAStatisticsShell.cur_width ? CODAStatisticsShell.cur_width : (CODAStatisticsShell.cur_width = this.getParent('KFloatable').getWidth() - 120),
			'height': 600,
			'legend': { position: 'bottom'}
		};

		var status_type = 'PieChart';

		var return_data = new Array();
		var return_status_chart = {};
		return_status_chart.chart_name = 'status';
		return_status_chart.chart_data = status_data;
		return_status_chart.chart_options = status_options;
		return_status_chart.chart_type = status_type;

		return_data.push(return_status_chart);

		return return_data;

	};

	KSystem.included('CODAGeneralStatisticsChart');
}, Kinky.CODA_INCLUDER_URL);
function CODAGraphForm(parent, href, layout, parentHref) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.target = {
			href : href,
			layout : layout,
			parent : parentHref
		};
		this.childForms = {};
		this.schemas = {};
		this.processing = 1;

		this.moreOptionsButton = false;

		this.hasChildrenPanel = true;
		this.config = {
			href : (!!href ? href : null)
		};

		this.messages = window.document.createElement('div');
		this.messages.innerHTML = '&nbsp;';
		this.setStyle({
			opacity : 0
		}, this.messages);
		this.panel.appendChild(this.messages);

		this.headers = {};
		this.cur_width = 0;

		this.actions = [];
		this.helpURL = CODAGenericShell.generateHelpURL(this);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAGraphForm.prototype = new KPanel();
	CODAGraphForm.prototype.constructor = CODAGraphForm;

	CODAGraphForm.prototype.refresh = function() {
		if (!!this.getParent('CODAFormIconDashboard')) {
			var list = this.getParent('CODAFormIconDashboard');
			var temp = list.getIconFor(this.target.data);
			list.SELECTED.options = temp.options;
			temp.destroy();
			delete temp;
			list.SELECTED.refresh();
		}
		else {
			var dialog = this.getParent('KFloatable');
			var dispatched = dialog._url;
			dialog.closeMe();
			KSystem.addTimer(function() {
				KBreadcrumb.dispatchURL({
					hash : dispatched
				});
			}, 750);
		}
	};

	CODAGraphForm.prototype.load = function() {
		if (!!this.config && !!this.config.href && !this.target.data) {
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, this.headers);
			this.kinky.get(this, (this.config.href.indexOf(':') == -1 ? 'rest:' : '') + this.config.href, {}, headers);
		}
		else if (!!this.target.data) {
			this.onLoad(this.target.data);
		}
		else {
			this.onLoad();
		}
	};

	CODAGraphForm.prototype.refreshData = function() {
		this.getParent('KFloatable');
	};

	CODAGraphForm.prototype.setData = function(data) {
		if (!!data) {
			this.target._id = data.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAGraphForm.prototype.onLoad = function(data, request) {
		this.setData(data);
		this.loadForm();
	};

	CODAGraphForm.prototype.onLoadSchema = function(data, request) {
		data = { schema : data };

		if (request.service == this.config.schema) {
			this.rootSchema = data.schema;
		}
		this.schemas[request.service] = data.schema;
		if (!!request.action) {
			KCache.commit(request.service, data);
		}
		this.loadForm();
	};

	CODAGraphForm.prototype.loadForm = function() {
		this.draw();
	};

	CODAGraphForm.prototype.onNotFound = CODAGraphForm.prototype.onNoContent = function(data, request) {
		this.activate();
	};

	CODAGraphForm.prototype.onUnauthorized = function(data) {

	};

	CODAGraphForm.prototype.focus = function(dispatcher) {
		if (!!dispatcher) {
			return;
		}
		for (var c in this.childWidget(this.selected).childWidgets()) {
			var child = this.childWidget(this.selected).childWidget(c);
			if (child instanceof KAbstractInput && !(child instanceof KStatic)) {
				KSystem.addTimer(function() {
					child.focus();
				}, 150);
				break;
			}
		}
	};

	CODAGraphForm.prototype.visible = function(tween) {
		if (this.display) {
			return;
		}
		KPanel.prototype.visible.call(this);

		this.setStyle({
			height : (this.getParent('KFloatable').height - 70) + 'px'
		}, KWidget.CONTENT_DIV);

		if (!!this.header && !!this.messages) {
			this.panel.insertBefore(this.header, this.messages);
		}

		var floatable = this.getParent('KFloatable');
		this.getShell().onFormOpen(floatable);
	};

	CODAGraphForm.prototype.draw = function() {
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAGraphForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);

		KPanel.prototype.draw.call(this);
		this.makeHelp();
		if (!!this.msg) {
			this.showMessage(this.msg);
			this.msg = null;
		}
	};

	CODAGraphForm.prototype.hideConfirm = function() {
		this.childWidget('/buttons/confirm').setStyle({
			display : 'none'
		});
		if (this.className.indexOf('Wizard') == -1) {
			this.childWidget('/buttons/cancel').setStyle({
				right : this.minimized ? '21px' : '20px'
			});
		}
	};

	CODAGraphForm.prototype.hideCancel = function() {
		this.childWidget('/buttons/cancel').setStyle({
			display : 'none'
		});
	};

	CODAGraphForm.prototype.makeHelp = function() {
		CODAGenericShell.makeHelp(this);
	};

	CODAGraphForm.prototype.showMessage = function(message, persistent) {
		if (!!this.timer) {
			KSystem.removeTimer(this.timer);
			this.timer = null;
		}
		if (persistent) {
			this.disable(true);
		}
		else {
			this.enable();
			this.messages.innerHTML = message;
			this.removeCSSClass('CODAFormErrorMessages', this.messages);
			this.addCSSClass('CODAFormSuccessMessages', this.messages);

			this.setStyle({
				display : 'block',
				position: 'fixed',
				bottom : '50px',
				right: '360px'
			}, this.messages);

			KEffects.addEffect(this.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				applyToElement : true,
				go : {
					alpha : 1
				},
				from : {
					alpha : 0
				},
				onComplete : function(w, t) {
					if (!!w && !persistent) {
						w.timer = KSystem.addTimer('CODAGraphForm.hideMessage(\'' + w.id + '\')', 3000);
					}
				}
			});
		}

	};

	CODAGraphForm.hideMessage = function(id) {
		var form = Kinky.getWidget(id);
		if (!form) {
			return;
		}
		form.timer = null;
		if (!!form) {
			form.enable();
			KEffects.addEffect(form.messages, {
				f : KEffects.easeOutExpo,
				type : 'fade',
				duration : 500,
				go : {
					alpha : 0
				},
				from : {
					alpha : 1
				},
				onComplete : function(w, t) {
					if (!!w) {
						w.messages.style.display = 'none';
						w.messages.innerHTML = '&nbsp;';
					}
				}
			});
		}
	};

	CODAGraphForm.prototype.onDestroy = function() {
		this.cleanCache();
	};

	CODAGraphForm.prototype.onClose = function() {
		if (!this.minimized) {
			for (var a in this.actions) {
				this.getShell().removeToolbarButton(this.actions[a]);
			}
		}
		KBreadcrumb.dispatchEvent(null, {
			action : '/close/form' + this.config.href
		});
	};

	CODAGraphForm.prototype.addSeparator = function(text, panel) {
		var c = (panel.activated() ? panel.content : panel.container);
		var h4 = window.document.createElement('h4');
		h4.innerHTML = text;
		h4.className = ' CODAGraphFormSeparator ';
		c.appendChild(h4);
	};

	CODAGraphForm.prototype.addAction = function(button) {
		button.hash = '/' + this.className + '/' + button.inputID;
		button.data = {
			tween : {
				show : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 1
					},
					from : {
						alpha : 0
					}
				},
				hide : {
					f : KEffects.easeOutExpo,
					type : 'fade',
					duration : 750,
					go : {
						alpha : 0
					},
					from : {
						alpha : 1
					}
				}
			}
		}
		button.panel.style.opacity = '0';
		this.actions.push(button);
		this.getShell().addToolbarButton(button, 0, 'right', true);
	};

	CODAGraphForm.prototype.onGet = function(data, request) {
	};

	CODAGraphForm.prototype.onError = function(data, request) {
		this.enable();
		var msg = undefined;
		eval('msg = ' + gettext('$CONFIRM_OPERATION_ERROR').replace('[NUMBER]', data.error_code));
		this.showMessage(msg);
		this.removeCSSClass('CODAFormSuccessMessages', this.messages);
		this.addCSSClass('CODAFormErrorMessages', this.messages);
	};

	CODAGraphForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	CODAGraphForm.prototype.onPost = CODAGraphForm.prototype.onCreated = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAGraphForm.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAGraphForm.prototype.enable = function() {
		if (this.loader_t !== undefined) {
			KSystem.removeTimer(this.loader_t);
			this.loader_t = undefined;
			return;
		}
		KPanel.prototype.enable.call(this);
	};

	CODAGraphForm.prototype.disable = function(communicating) {
		if (!!communicating) {
			if (this.loader_t === undefined) {
				var self = this;
				this.loader_t = KSystem.addTimer(function() {
					self.loader_t = undefined;
					KPanel.prototype.disable.call(self);
				}, 250);
			}
		}
		else {
			if (this.loader_t !== undefined) {
				KSystem.removeTimer(this.loader_t);
				this.loader_t = undefined;
			}
			KPanel.prototype.disable.call(this);
		}
	};

	CODAGraphForm.closeMe = function(event) {
		KDOM.stopEvent(event);
		var form = KDOM.getEventWidget(event).parent;
		form.parent.closeMe();
	};

	CODAGraphForm.getHelpOptions = function() {
		return KSystem.clone(CODAGraphForm.HELP_OPTIONS);
	};

	KWidget.STATE_FUNCTIONS .push('onClose');

	KSystem.included('CODAGraphForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAStatisticsDashboard(parent) {
	if (parent != null) {
		var icons = [];

		if(!!parent.statistics_buttons){

			for (var one_button in parent.statistics_buttons){

				icons.push({
					icon : gettext('$DASHBOARD_BUTTON_' + parent.statistics_buttons[one_button].toUpperCase() + '_STATISTICS_ICON'),
					activate : false,
					title : gettext('$DASHBOARD_BUTTON_' + parent.statistics_buttons[one_button].toUpperCase() + '_STATISTICS'),
					description : '<span>' + gettext('$DASHBOARD_BUTTON_' + parent.statistics_buttons[one_button].toUpperCase() + '_STATISTICS_DESC') + '</span>',
					actions : [{
						icon : 'css:fa fa-chevron-right',
						desc : gettext('$DASHBOARD_BUTTON_' + parent.statistics_buttons[one_button].toUpperCase() + '_STATISTICS_LIST'),
						link : '#/' + parent.statistics_buttons[one_button] + '-statistics' + '/' + this.getShell().data.app_id
					}]
				});				

			}

				/*icons.push({
					icon : 'css:fa fa-group',
					activate : false,
					title : gettext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS'),
					description : '<span>' + gettext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_DESC') + '</span>',
					actions : [{
						icon : 'css:fa fa-chevron-right',
						desc : gettext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_LIST'),
						link : '#/participants-statistics' + '/' + this.getShell().data.app_id
					}]
				});

				icons.push({
					icon : 'css:fa fa-rocket',
					activate : false,
					title : gettext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS'),
					description : '<span>' + gettext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_DESC') + '</span>',
					actions : [{
						icon : 'css:fa fa-chevron-right',
						desc : gettext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_LIST'),
						link : '#/participations-statistics' + '/' + this.getShell().data.app_id
					}]
				});

				icons.push({
					icon : 'css:fa fa-flask',
					activate : false,
					title : gettext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS'),
					description : '<span>' + gettext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_DESC') + '</span>',
					actions : [{
						icon : 'css:fa fa-chevron-right',
						desc : gettext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_LIST'),
						link : '#/analytics-statistics' + '/' + this.getShell().data.app_id
					}]
				});
				*/
		}

		

		CODAMenuDashboard.call(this, parent, {}, icons);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAStatisticsDashboard.prototype = new CODAMenuDashboard();
	CODAStatisticsDashboard.prototype.construtor = CODAStatisticsDashboard;

	CODAStatisticsDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, data);
	};

	KSystem.included('CODAStatisticsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAStatisticsShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAStatisticsShell.actions);
		this.statistics_buttons = [];
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAStatisticsDashboard',
	'CODAGeneralStatisticsChart',
	'CODAAnalyticsStatisticsChart'
], function(){
	CODAStatisticsShell.prototype = new CODAGenericShell();
	CODAStatisticsShell.prototype.constructor = CODAStatisticsShell;

	CODAStatisticsShell.prototype.loadShell = function() {
		this.statistics_buttons.push('analytics');
		this.app = this.app_data;

		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:/campaigns', {}, headers, 'onLoadCampaign');
	};

	CODAStatisticsShell.prototype.onLoadCampaign = function(data, request) {
		if(!!data && !!data.elements && data.elements.length == 1){
			this.active_campaign_href = data.elements[0].href;
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.get(this, 'rest:' + data.elements[0].href + '/phases?orderBy=+start_date', {}, headers, 'onLoadCampaignPhases');
			
		}
		else{
			this.draw();
		}
		this.statistics_buttons.push('participants');
		this.statistics_buttons.push('participations');
	};

	CODAStatisticsShell.prototype.onLoadCampaignPhases = function(data, request) {
		
		if(!!data && !!data.elements && data.elements.length > 0){
			this.all_campaign_phases = data.elements;
			var actual_phase = this.findActualPhase(data.elements);
			if(actual_phase){
				this.active_phase_href = actual_phase;
			}
		}else{
			this.all_campaign_phases = new Array();
		}

		//var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		//headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		//this.kinky.head(this, 'rest:' + '/participations', {}, headers, 'onLoadParticipationsCheck');
		this.draw();
	};

	/*
	CODAStatisticsShell.prototype.onLoadParticipationsCheck = function(data, request) {
		dump(data);
		dump(request);
		this.draw();	
	};
	*/

	CODAStatisticsShell.prototype.findActualPhase = function(elements) {
		if(!!elements && elements.length > 0){
			var actual_date = new Date();
			var actual_phase_href = null;

			if(actual_date < new Date(elements[0].start_date)){
				actual_phase_href = elements[0].href;
			}
			else if(actual_date > new Date(elements[elements.length - 1].end_date)){
				actual_phase_href = elements[elements.length - 1].href;
			}
			else{
				for ( var index in elements){
					if(actual_date >= new Date(elements[index].start_date) && actual_date <= new Date(elements[index].end_date)){
						actual_phase_href = elements[index].href;
						break;
					}else{
						continue;
					}
				}
			}
			return actual_phase_href;
		}
		return false;
	};

	CODAStatisticsShell.prototype.draw = function() {
		this.setTitle(gettext('$STATISTICS_SHELL_TITLE'));

		this.dashboard = new CODAStatisticsDashboard(this);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAStatisticsShell.sendRequest = function() {
	};

	CODAStatisticsShell.prototype.listGeneralStatistics = function(href, minimized) {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		params = {};
		this.kinky.post(this, 'rest:' + href, params, headers, 'onListGeneralStatistics');
	};

	CODAStatisticsShell.prototype.onListGeneralStatistics = function(data, request) {
		var statistics_type = request.service.substr(request.service.indexOf("statistics_type=") + 16, request.service.length - request.service.indexOf("statistics_type=") + 16);
		var generalstatistics = new CODAGeneralStatisticsChart(this, data, statistics_type);
		generalstatistics.hash = '/generalstatistics/hash/';
		switch (statistics_type) {
			case 'participations' : {
				if(!!data.participations){
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_PARTICIPATIONS_TITLE') + ' (' + data.participations.length + gettext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_SPARTICIPATIONS') + ')');
				}else{
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_PARTICIPATIONS_TITLE'));
				}				
				break;
			}
			case 'participants' : {
				if(!!data.participants){
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_PARTICIPANTS_TITLE') + ' (' + data.participants.length + gettext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_SPARTICIPANTS') + ')');
				}else{
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_PARTICIPANTS_TITLE'));
				}
				break;
			}
			case 'customers' : {
				if(!!data.participants){
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_CUSTOMERS_TITLE') + ' (' + data.participants.length + gettext('$CUSTOMERS_STATISTICS_LOCATION_CHART_LABEL_CUSTOMERS') + ')');
				}else{
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_CUSTOMERS_TITLE'));
				}
				break;
			}
			case 'carts' : {
				if(!!data.carts){
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_CARTS_TITLE') + ' (' + data.carts.length + gettext('$CARTS_STATISTICS_CARTS_CHART_LABEL_SCARTS') + ')');
				}else{
					generalstatistics.setTitle(gettext('$GENERAL_STATISTICS_CARTS_TITLE'));
				}
				break;
			}
		}
		this.makeDialog(generalstatistics, { minimized : false, modal : true });
	};

	CODAStatisticsShell.prototype.listAnalyticsStatistics = function(href, minimized) {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.post(this, 'rest:' + href, {}, headers, 'onListAnalyticsStatistics');
	};

	CODAStatisticsShell.prototype.onListAnalyticsStatistics = function(data, request) {
		var analyticsstatistics = new CODAAnalyticsStatisticsChart(this, data);
		analyticsstatistics.hash = '/analyticsstatistics/hash/';
		analyticsstatistics.setTitle(gettext('$ANALYTICS_STATISTICS_TITLE'));
		this.makeDialog(analyticsstatistics, { minimized : false, modal : true });
	};

	CODAStatisticsShell.prototype.onLoadCartsCheck = function(data, request) {
		this.statistics_buttons.push('customers');
		this.statistics_buttons.push('carts');
		this.draw();
	};

	CODAStatisticsShell.prototype.onNoContent = function(data, request) {
		if(request.service.indexOf('/generalstatistics') == request.service.length - 18){
			alert("no statistics");
		}else if (request.service.indexOf('/analyticsstatistics') == request.service.length - 20){
			alert("no statistics");
		}else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODAStatisticsShell.prototype.onNotFound = function(data, request) {
		if(request.service == '/campaigns'){
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.head(this, 'rest:' + '/carts', {}, headers, 'onLoadCartsCheck');
		}else if(request.service.indexOf('/phases') != -1){
			this.draw();
		}else if(request.service.indexOf('/participations') != -1){
			this.draw();
		}else{
			this.draw();
		}
	};

	CODAStatisticsShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]){
			case 'general-statistics' : {
				if(!!widget.active_campaign_href){
					var actualPhaseSplit = widget.active_phase_href.split('/');
					var actualPhaseID = actualPhaseSplit[actualPhaseSplit.length - 1];
					if (!!actualPhaseID){
						Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?phase_id=' + actualPhaseID, false);
						KBreadcrumb.dispatchURL({
							hash : ''
						});
					}
				}else{
					Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve', false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'participants-statistics' : {
				Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?statistics_type=participants', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'participations-statistics' : {
				if(!!widget.active_campaign_href){
					var actualPhaseSplit = widget.active_phase_href.split('/');
					var actualPhaseID = actualPhaseSplit[actualPhaseSplit.length - 1];
					if (!!actualPhaseID){
						Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?phase_id=' + actualPhaseID + '&statistics_type=participations', false);
						KBreadcrumb.dispatchURL({
							hash : ''
						});
					}
				}else{
					Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?statistics_type=participations', false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'analytics-statistics' : {
				//if(!!widget.active_campaign_href){
				//	var actualPhaseSplit = widget.active_phase_href.split('/');
				//	var actualPhaseID = actualPhaseSplit[actualPhaseSplit.length - 1];
				//	if (!!actualPhaseID){
						Kinky.getDefaultShell().listAnalyticsStatistics('/analyticsstatistics/' + parts[2] + '/retrieve', false);
						KBreadcrumb.dispatchURL({
							hash : ''
						});
				//	}
				//}
				break;
			}
			case 'customers-statistics' : {
				Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?statistics_type=customers', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'carts-statistics' : {
				Kinky.getDefaultShell().listGeneralStatistics('/generalstatistics/' + parts[2] + '/retrieve?statistics_type=carts', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}

	};

	KSystem.included('CODAStatisticsShell');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

