///////////////////////////////////////////////////////////////////
//FORMS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
///////////////////////////////////////////////////////////////////

KSystem.included('CODAStatisticsLocale_Help_pt');
///////////////////////////////////////////////////////////////////
//SHELL
settext('$GENERAL_STATISTICS_PARTICIPATIONS_TITLE', 'Estatísticas de Participações', 'pt');
settext('$GENERAL_STATISTICS_PARTICIPANTS_TITLE', 'Estatísticas de Participantes', 'pt');
settext('$GENERAL_STATISTICS_CUSTOMERS_TITLE', 'Estatísticas de Clientes', 'pt');
settext('$GENERAL_STATISTICS_CARTS_TITLE', 'Estatísticas de Encomendas', 'pt');
settext('$ANALYTICS_STATISTICS_TITLE', 'Estatísticas de Acessos e Navegação', 'pt');
settext('$STATISTICS_SHELL_TITLE', 'Estatísticas Gerais', 'pt');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS', 'Estatísticas Gerais', 'pt');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_DESC', 'Consulta as estatísticas gerais da aplicação', 'pt');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_ICON', '', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS', 'Participantes', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_DESC', 'Consulta as estatísticas de participantes da aplicação', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_ICON', 'css:fa fa-group', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS', 'Participações', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_DESC', 'Consulta as estatísticas de participações da aplicação', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_ICON', 'css:fa fa-rocket', 'pt');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS', 'Acessos e Navegação', 'pt');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_DESC', 'Consulta as estatísticas de acessos e navegação na aplicação', 'pt');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_ICON', 'css:fa fa-flask', 'pt');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS', 'Clientes', 'pt');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_DESC', 'Consulta as estatísticas de clientes da aplicação', 'pt');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_ICON', 'css:fa fa-group', 'pt');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS', 'Compras', 'pt');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_DESC', 'Consulta as estatísticas de compras da aplicação', 'pt');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_LIST', 'LISTA', 'pt');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_ICON', 'css:fa fa-shopping-cart', 'pt');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_DATE', 'Data', 'pt');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_PARTICIPATIONS', 'Participações', 'pt');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_SPARTICIPATIONS', ' participações', 'pt');

settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS1', 'Participações entre ', 'pt');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS2', ' e ', 'pt');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_NO_PARTICIPATIONS', 'Não há participações', 'pt');

settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL1', '13-17', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL2', '18-24', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL3', '25-34', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL4', '35-44', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL5', '45-54', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL6', '54-64', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL7', '65+', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL8', 'não definido', 'pt');

settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_AGE', 'Idade', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_MALE', 'Masculino', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_FEMALE', 'Feminino', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE', 'não disponível', 'pt');

settext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS', 'Idade', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS', 'Não há participantes', 'pt');
settext('$PARTICIPANTS_STATISTICS_GENDER_CHART_LABEL_GENDER', 'Género', 'pt');
settext('$PARTICIPANTS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS', 'Idade / Género', 'pt');

settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_LOCATION', 'Localização', 'pt');
settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS', 'Participantes', 'pt');
settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_SPARTICIPANTS', ' participantes', 'pt');
settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_TITLE', 'Top 5 de localizações dos participantes', 'pt');

settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_DATAUSE', 'Uso de Dados', 'pt');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZATION', 'Autorização', 'pt');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZE', 'autoriza', 'pt');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_NOAUTHORIZE', 'não autoriza', 'pt');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_UNDEFINED', 'não definido', 'pt');

settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_TITLE', 'Autorização do uso de dados', 'pt');

settext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS', 'Não há clientes', 'pt');
settext('$CUSTOMERS_STATISTICS_LOCATION_CHART_LABEL_CUSTOMERS', ' clientes', 'pt');

settext('$CARTS_STATISTICS_CARTS_CHART_LABEL_DATE', 'Data', 'pt');
settext('$CARTS_STATISTICS_CARTS_CHART_LABEL_CARTS', 'Encomendas', 'pt');
settext('$CARTS_STATISTICS_CARTS_CHART_LABEL_SCARTS', ' encomendas', 'pt');

settext('$CARTS_STATISTICS_CARTS_CHART_TITLE_CARTS1', 'Encomendas entre ', 'pt');
settext('$CARTS_STATISTICS_CARTS_CHART_TITLE_CARTS2', ' e ', 'pt');
settext('$CARTS_STATISTICS_CARTS_CHART_TITLE_NO_CARTS', 'Não há encomendas', 'pt');

settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_PAGES', 'Páginas', 'pt');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_VISITS', 'Visitas', 'pt');

settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES1', 'Acesso a páginas entre ', 'pt');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES2', ' e ', 'pt');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_NO_DATA', 'Não há dados para o período indicado', 'pt');

settext('$ANALYTICS_STATISTICS_EVENTS_TABLE_NODATA', 'sem dados', 'pt');

settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_ACTION_LABEL', 'Ação do Evento', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_CATEGORY_LABEL', 'Categoria de Evento', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_LABEL_LABEL', 'Etiqueta do Evento', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_TOTAL_EVENTS_LABEL', 'Total de Eventos', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_UNIQUE_EVENTS_LABEL', 'Eventos Exclusivos', 'pt');

settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SUBMIT', 'Submissão', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_CLICK', 'Click', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SHARE_PARTICIPATION', 'Partilha da Participação', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SHARE_APP', 'Partilha da Aplicação', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_INVITE', 'Convite', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SOURCE', 'Fonte', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_MESSAGE', 'Mensagem', 'pt');

settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_PARTICIPATION', 'Participação', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_BUTTON', 'Botão', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_SHARE_BUBBLE', 'Roda de Partilha', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_SOCIAL', 'Social', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_FOOTER', 'Rodapé', 'pt');

settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_START_QUIZ_FIRST', 'Acesso ao início de quiz pelo link mais visível', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_START_QUIZ_SECOND', 'Acesso ao início de quiz pelo link menos visível', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_SHARE_ELEMENT', 'Click no elemento de partilha', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FACEBOOK', 'Facebook', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_GOOGLEPLUS', 'Googleplus', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_LINKEDIN', 'Linkedin', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PINTEREST', 'Pinterest', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_TWITTER', 'Twitter', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_ACCESS_FORM_SUBMIT', 'Acesso ao formulário de submissão', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_BEGINNING', 'Iniciaram o questionário', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_END', 'Chegaram ao fim do questionário', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_HALF', 'Chegaram a meio do questionário', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_MAX_PARTICIPATIONS', 'Tentaram participar depois de terem feito todas as participações posíveis', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_NO_PHASE', 'Tentativa de participação quando não havia fase ativa', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_NO_STOCK', 'Tentativa de participação sem stock disponível', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_START', 'Início de preenchimento de formulário', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_NEXT_PAGE', 'Pedido para próxima página', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_NEXT_ERRORS', 'Pedido para próxima página e há erros de preenchimento', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_GET_PRIZES_DOWNLOAD', 'Download de um voucher', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VOTE', 'Voto numa participação', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VOTE_AGAIN', 'Tentativa de voto em participação já votada pelo utilizador', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VIEW_PRIZES', 'Acesso à lista dos meus prémios', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_MENTION_TAGGING_BUTTON', 'Seleção de amigos por botão mention tagging', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_MENTION_TAGGING_INVITE', 'Envio de convite por mention tagging', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_UPLOAD', 'Upload', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VIEW_FRIENDS', 'Lista de amigos que aceitaram convite', 'pt');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_WEBSITE', 'Website', 'pt');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//LISTS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//WIZARDS
///////////////////////////////////////////////////////////////////

KSystem.included('CODAStatisticsLocale_pt');
