///////////////////////////////////////////////////////////////////
//SHELL
settext('$GENERAL_STATISTICS_PARTICIPATIONS_TITLE', 'Estatísticas de Participações', 'br');
settext('$GENERAL_STATISTICS_PARTICIPANTS_TITLE', 'Estatísticas de Participantes', 'br');
settext('$GENERAL_STATISTICS_CUSTOMERS_TITLE', 'Estatísticas de Clientes', 'br');
settext('$GENERAL_STATISTICS_CARTS_TITLE', 'Estatísticas de Encomendas', 'br');
settext('$ANALYTICS_STATISTICS_TITLE', 'Estatísticas de Acessos e Navegação', 'br');
settext('$STATISTICS_SHELL_TITLE', 'Estatísticas Gerais', 'br');
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS', 'Estatísticas Gerais', 'br');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_DESC', 'Consulta as estatísticas gerais da aplicação', 'br');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_GENERAL_STATISTICS_ICON', '', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS', 'Participantes', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_DESC', 'Consulta as estatísticas de participantes da aplicação', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPANTS_STATISTICS_ICON', 'css:fa fa-group', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS', 'Participações', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_DESC', 'Consulta as estatísticas de participações da aplicação', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_PARTICIPATIONS_STATISTICS_ICON', 'css:fa fa-rocket', 'br');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS', 'Acessos e Navegação', 'br');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_DESC', 'Consulta as estatísticas de acessos e navegação na aplicação', 'br');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_ANALYTICS_STATISTICS_ICON', 'css:fa fa-flask', 'br');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS', 'Clientes', 'br');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_DESC', 'Consulta as estatísticas de clientes da aplicação', 'br');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_CUSTOMERS_STATISTICS_ICON', 'css:fa fa-group', 'br');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS', 'Compras', 'br');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_DESC', 'Consulta as estatísticas de compras da aplicação', 'br');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_LIST', 'LISTA', 'br');
settext('$DASHBOARD_BUTTON_CARTS_STATISTICS_ICON', 'css:fa fa-shopping-cart', 'br');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//FORMS
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_DATE', 'Data', 'br');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_PARTICIPATIONS', 'Participações', 'br');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_LABEL_SPARTICIPATIONS', ' participações', 'br');

settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS1', 'Participações entre ', 'br');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_PARTICIPATIONS2', ' e ', 'br');
settext('$PARTICIPATIONS_STATISTICS_PARTICIPATIONS_CHART_TITLE_NO_PARTICIPATIONS', 'Não há participações', 'br');

settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL1', '13-17', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL2', '18-24', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL3', '25-34', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL4', '35-44', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL5', '45-54', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL6', '54-64', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL7', '65+', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_INTERVAL8', 'não definido', 'br');

settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_AGE', 'Idade', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_MALE', 'Masculino', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_FEMALE', 'Feminino', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_LABEL_NOTAVAILABLE', 'não disponível', 'br');

settext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_PARTICIPANTS', 'Idade', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_CHART_TITLE_NO_PARTICIPANTS', 'Não há participantes', 'br');
settext('$PARTICIPANTS_STATISTICS_GENDER_CHART_LABEL_GENDER', 'Género', 'br');
settext('$PARTICIPANTS_STATISTICS_AGE_GENDER_CHART_TITLE_PARTICIPANTS', 'Idade / Género', 'br');

settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_LOCATION', 'Localização', 'br');
settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_PARTICIPANTS', 'Participantes', 'br');
settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_LABEL_SPARTICIPANTS', ' participantes', 'br');

settext('$CUSTOMERS_STATISTICS_AGE_CHART_TITLE_NO_CUSTOMERS', 'Não há clientes', 'br');
settext('$CUSTOMERS_STATISTICS_LOCATION_CHART_LABEL_CUSTOMERS', 'Clientes', 'br');

settext('$PARTICIPANTS_STATISTICS_LOCATION_CHART_TITLE', 'Top 5 de localizações dos participantes', 'br');

settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_DATAUSE', 'Uso de Dados', 'br');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZATION', 'Autorização', 'br');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_AUTHORIZE', 'autoriza', 'br');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_NOAUTHORIZE', 'não autoriza', 'br');
settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_LABEL_UNDEFINED', 'não definido', 'br');

settext('$PARTICIPANTS_STATISTICS_EMAILMARKETING_CHART_TITLE', 'Autorização do uso de dados', 'br');

settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_PAGES', 'Páginas', 'br');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_LABEL_VISITS', 'Visitas', 'br');

settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES1', 'Acesso a páginas entre ', 'br');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_PAGES2', ' e ', 'br');
settext('$ANALYTICS_STATISTICS_PAGEVIEWS_CHART_TITLE_NO_DATA', 'Não há dados para o período indicado', 'br');

settext('$ANALYTICS_STATISTICS_EVENTS_TABLE_NODATA', 'sem dados', 'br');

settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_ACTION_LABEL', 'Ação do Evento', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_CATEGORY_LABEL', 'Categoria de Evento', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_EVENT_LABEL_LABEL', 'Etiqueta do Evento', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_TOTAL_EVENTS_LABEL', 'Total de Eventos', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_UNIQUE_EVENTS_LABEL', 'Eventos Exclusivos', 'br');

settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SUBMIT', 'Submissão', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_CLICK', 'Click', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SHARE_PARTICIPATION', 'Partilha da Participação', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SHARE_APP', 'Partilha da Aplicação', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_INVITE', 'Convite', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_SOURCE', 'Fonte', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_ACTION_MESSAGE', 'Mensagem', 'br');

settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_PARTICIPATION', 'Participação', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_BUTTON', 'Botão', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_SHARE_BUBBLE', 'Roda de Partilha', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_SOCIAL', 'Social', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_CATEGORY_FOOTER', 'Rodapé', 'br');

settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_START_QUIZ_FIRST', 'Acesso ao início de quiz pelo link mais visível', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_START_QUIZ_SECOND', 'Acesso ao início de quiz pelo link menos visível', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_SHARE_ELEMENT', 'Click no elemento de partilha', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FACEBOOK', 'Facebook', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_GOOGLEPLUS', 'Googleplus', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_LINKEDIN', 'Linkedin', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PINTEREST', 'Pinterest', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_TWITTER', 'Twitter', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_ACCESS_FORM_SUBMIT', 'Acesso ao formulário de submissão', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_BEGINNING', 'Iniciaram a participação no formulário', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_END', 'Terminaram a participação no formulário', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATE_QUIZ_HALF', 'Chegaram a meio do formulário', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_MAX_PARTICIPATIONS', 'Tentaram participar depois de terem feito todas as participações posíveis', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_NO_PHASE', 'Tentativa de participação quando não havia fase ativa', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_PARTICIPATION_INVALID_NO_STOCK', 'Tentativa de participação sem stock disponível', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_START', 'Início de preenchimento de formulário', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_NEXT_PAGE', 'Pedido para próxima página', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_FILLING_NEXT_ERRORS', 'Pedido para próxima página e há erros de preenchimento', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_GET_PRIZES_DOWNLOAD', 'Download de um voucher', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VOTE', 'Voto numa participação', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VOTE_AGAIN', 'Tentativa de voto em participação já votada pelo utilizador', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VIEW_PRIZES', 'Acesso à lista dos meus prémios', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_MENTION_TAGGING_BUTTON', 'Seleção de amigos por botão mention tagging', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_MENTION_TAGGING_INVITE', 'Envio de convite por mention tagging', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_UPLOAD', 'Upload', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_VIEW_FRIENDS', 'Lista de amigos que aceitaram convite', 'br');
settext('$ANALYTICS_STATISTICS_EVENTS_LABEL_WEBSITE', 'Website', 'br');

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//LISTS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//WIZARDS
///////////////////////////////////////////////////////////////////

KSystem.included('CODAStatisticsLocale_br');
///////////////////////////////////////////////////////////////////
//FORMS
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
//DASHBOARD
///////////////////////////////////////////////////////////////////

KSystem.included('CODAStatisticsLocale_Help_br');
