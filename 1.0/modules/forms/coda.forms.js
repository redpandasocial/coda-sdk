KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAFieldItemForm(parent, href, parent_href){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		if(parent_href != null){
			this.target.collection = parent_href;
		}
		this.nillable = true;
		if(href != null){
			this.href_to_fields_list = href.substring(0, href.indexOf('/fields') + 7);
		}else if(parent_href != null){
			this.href_to_fields_list = parent_href;
		}
	}
}

KSystem.include([
	'CODAForm',
	'CODAArrayList'
], function() {
	CODAFieldItemForm.prototype = new CODAForm();
	CODAFieldItemForm.prototype.constructor = CODAFieldItemForm;

	CODAFieldItemForm.prototype.load = function(){
		if(!!this.href_to_fields_list){
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.get(this, 'rest:' + this.href_to_fields_list, {}, headers, 'onListFields');
		}else{
			CODAForm.prototype.load.call(this);
		}
	};

	CODAFieldItemForm.prototype.onListFields = function(data, request){
		if(!!data && !!data.elements){
			KCache.commit('/sectionfields', data.elements);
		}
		CODAForm.prototype.load.call(this);
	};

	CODAFieldItemForm.prototype.setData = function(data) {
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAFieldItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAFieldItemForm.prototype.addMinimizedPanel = function() {
		if(!!this.data && !!this.data.label){
			this.setTitle(gettext('$EDIT_FIELD_PROPERTIES') + ' \u00ab' + this.data.label + '\u00bb');
		}else{
			this.setTitle(gettext('$ADD_FIELD_PROPERTIES'));
		}

		var field_item_panel = new KPanel(this);
		{
			field_item_panel.hash = '/general';

			var code = new KHidden(field_item_panel, 'code');
			{
				if (!!this.data && !!this.data.code){
					code.setValue(this.data.code);
				}
			}
			field_item_panel.appendChild(code);

			var label = new KInput(field_item_panel, gettext('$FIELD_ITEM_LABEL'), 'label');
			{
				if(!!this.data && !!this.data.label){
					label.setValue(this.data.label);
				}
				label.setHelp(gettext('$FIELD_ITEM_LABEL_HELP'), CODAForm.getHelpOptions());
			}
			field_item_panel.appendChild(label);

			var sectionfields = KCache.restore('/sectionfields');
			if (!!sectionfields){

				var sectionfields_combo = new KCombo(field_item_panel, gettext('$FIELD_ITEM_ORDER'), 'order');
				{
					for (var index in sectionfields){
						if((!!this.data && !!this.data.order && this.data.order == sectionfields [index].order)){
							sectionfields_combo.addOption(sectionfields [index].order, gettext('$ORDER_SAME'), (!!this.data && !!this.data.order && this.data.order == sectionfields [index].order) ? true : false);
						}else{
							if (index == 0){
								sectionfields_combo.addOption(1, gettext('$ORDER_BEGINNING'), (!!this.data && !!this.data.order && this.data.order == 1) ? true : false);
							}else{
								if(index < this.data.order){
									sectionfields_combo.addOption(sectionfields [index].order, gettext('$ORDER_AFTER') + ' ' + (!!sectionfields [index-1].label ? sectionfields [index-1].label : sectionfields [index].order), (!!this.data && !!this.data.order && this.data.order == sectionfields [index].order) ? true : false);	
								}else{
									sectionfields_combo.addOption(sectionfields [index].order, gettext('$ORDER_AFTER') + ' ' + (!!sectionfields [index].label ? sectionfields [index].label : sectionfields [index].order), (!!this.data && !!this.data.order && this.data.order == sectionfields [index].order) ? true : false);	
								}
							}
						}
					}
				}
				sectionfields_combo.setHelp(gettext('$FIELD_ITEM_ORDER_HELP'), CODAForm.getHelpOptions());
				field_item_panel.appendChild(sectionfields_combo);
			}

			var required = new KCheckBox(field_item_panel, gettext('$FIELD_ITEM_REQUIRED'), 'required');
			required.addOption(true, gettext('$FIELD_REQUIRED_YES'), (!!this.data && !!this.data.required) ? true : false);
			required.setHelp(gettext('$FIELD_ITEM_REQUIRED_HELP'), CODAForm.getHelpOptions());
			field_item_panel.appendChild(required);
			
			var type = new KCombo(field_item_panel, gettext('$FIELD_ITEM_TYPE') + ' *', 'type');
			{
				type.addOption('textarea_small', gettext('$FIELD_TYPE_TEXTAREASMALL'), (!!this.data && !!this.data.type && this.data.type == 'textarea_small') ? true : false);
				type.addOption('textarea_medium', gettext('$FIELD_TYPE_TEXTAREAMEDIUM'), (!!this.data && !!this.data.type && this.data.type == 'textarea_medium') ? true : false);
				type.addOption('textarea_large', gettext('$FIELD_TYPE_TEXTAREALARGE'), (!!this.data && !!this.data.type && this.data.type == 'textarea_large') ? true : false);
				type.addOption('checkbox', gettext('$FIELD_TYPE_CHECKBOX'), (!!this.data && !!this.data.type && this.data.type == 'checkbox') ? true : false);
				type.addOption('zipcode', gettext('$FIELD_TYPE_ZIPCODE'), (!!this.data && !!this.data.type && this.data.type == 'zipcode') ? true : false);
				type.addOption('combobox', gettext('$FIELD_TYPE_COMBOBOX'), (!!this.data && !!this.data.type && this.data.type == 'combobox') ? true : false);
				type.addOption('date', gettext('$FIELD_TYPE_DATE'), (!!this.data && !!this.data.type && this.data.type == 'date') ? true : false);
				type.addOption('birthdate', gettext('$FIELD_TYPE_BIRTHDATE'), (!!this.data && !!this.data.type && this.data.type == 'birthdate') ? true : false);
				type.addOption('email', gettext('$FIELD_TYPE_EMAIL'), (!!this.data && !!this.data.type && this.data.type == 'email') ? true : false);
				type.addOption('input', gettext('$FIELD_TYPE_INPUT'), (!!this.data && !!this.data.type && this.data.type == 'input') ? true : false);
				type.addOption('listbox', gettext('$FIELD_TYPE_LISTBOX'), (!!this.data && !!this.data.type && this.data.type == 'listbox') ? true : false);
				type.addOption('double', gettext('$FIELD_TYPE_DOUBLE'), (!!this.data && !!this.data.type && this.data.type == 'double') ? true : false);
				type.addOption('integer', gettext('$FIELD_TYPE_INTEGER'), (!!this.data && !!this.data.type && this.data.type == 'integer') ? true : false);
				type.addOption('radiobutton', gettext('$FIELD_TYPE_RADIOBUTTON'), (!!this.data && !!this.data.type && this.data.type == 'radiobutton') ? true : false);

				type.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				type.setHelp(gettext('$FIELD_ITEM_TYPE_HELP'), CODAForm.getHelpOptions());

			}
			field_item_panel.appendChild(type);

		}
		this.addPanel(field_item_panel, gettext('$FORM_GENERALINFO_PANEL'), true);

		var options_panel = new CODAArrayList(this, (!!this.data && !!this.data.options && this.data.options != [] ? this.data.options : []), 'options', {id : undefined, label : undefined, parentCode : this.data.code}, { one_choice : false, add_button : true, filter_input : false});		
		options_panel.hash = '/field/options';
		this.addPanel(options_panel, gettext('$FIELD_OPTIONS_LIST_PANEL'), false);	

	};

	CODAFieldItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/sectionfield_1';
			firstPanel.title = gettext('$SECTION_FIELD_WIZARD_TITLE1');
			firstPanel.description = gettext('$SECTION_FIELD_WIZARD_INFO1');
			firstPanel.icon = 'css:fa fa-info';

			var label = new KInput(firstPanel, gettext('$FIELD_ITEM_LABEL'), 'label');
			label.setHelp(gettext('$FIELD_ITEM_LABEL_HELP'), CODAForm.getHelpOptions());
			firstPanel.appendChild(label);

			var sectionfields = KCache.restore('/sectionfields');
			if (!!sectionfields){

				var sectionfields_combo = new KCombo(firstPanel, gettext('$FIELD_ITEM_ORDER'), 'order');
				{
					sectionfields_combo.addOption(1, gettext('$ORDER_BEGINNING'), false);

					for (var index in sectionfields){
						sectionfields_combo.addOption(sectionfields [index].order + 1, gettext('$ORDER_AFTER') + ' ' + sectionfields [index].code, false);
					}					
				}
				sectionfields_combo.setHelp(gettext('$FIELD_ITEM_ORDER_HELP'), CODAForm.getHelpOptions());
				firstPanel.appendChild(sectionfields_combo);

			}

			var required = new KCheckBox(firstPanel, gettext('$FIELD_ITEM_REQUIRED'), 'required');
			{
				required.addOption(true, gettext('$FIELD_REQUIRED_YES'), false);
				required.setHelp(gettext('$FIELD_ITEM_REQUIRED_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(required);
			
			var type = new KCombo(firstPanel, gettext('$FIELD_ITEM_TYPE') + ' *', 'type');
			{
				type.addOption('textarea_small', gettext('$FIELD_TYPE_TEXTAREASMALL'), false);
				type.addOption('textarea_medium', gettext('$FIELD_TYPE_TEXTAREAMEDIUM'), false);
				type.addOption('textarea_large', gettext('$FIELD_TYPE_TEXTAREALARGE'), false);
				type.addOption('checkbox', gettext('$FIELD_TYPE_CHECKBOX'), false);
				type.addOption('zipcode', gettext('$FIELD_TYPE_ZIPCODE'), false);
				type.addOption('combobox', gettext('$FIELD_TYPE_COMBOBOX'), false);
				type.addOption('date', gettext('$FIELD_TYPE_DATE'), false);
				type.addOption('birthdate', gettext('$FIELD_TYPE_BIRTHDATE'), false);
				type.addOption('email', gettext('$FIELD_TYPE_EMAIL'), false);
				type.addOption('input', gettext('$FIELD_TYPE_INPUT'), false);
				type.addOption('listbox', gettext('$FIELD_TYPE_LISTBOX'), false);
				type.addOption('double', gettext('$FIELD_TYPE_DOUBLE'), false);
				type.addOption('integer', gettext('$FIELD_TYPE_INTEGER'), false);
				type.addOption('radiobutton', gettext('$FIELD_TYPE_RADIOBUTTON'), false);

				type.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				type.setHelp(gettext('$FIELD_ITEM_TYPE_HELP'), CODAForm.getHelpOptions());
			}
			firstPanel.appendChild(type);

		}

		return [firstPanel];

	};

	CODAFieldItemForm.prototype.setDefaults = function(params){
		
		if(!params.order || params.order == ''){
			delete params.order;
		}

		if(!!params.required && params.required.length == 1 && params.required[0] == true){
			params.required = true;
		}else{
			params.required = false;
		}
	
	};

	CODAFieldItemForm.prototype.onPost = CODAFieldItemForm.prototype.onCreated = function(data, request){
		CODAFormsShell.section_fields_list.list.refreshPages();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAFieldItemForm.prototype.onPut = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
		//if (!!CODAArrayList.OPENED) {
		//	return;
		//}
		CODAFormsShell.section_fields_list.list.refreshPages();
	};

	KSystem.included('CODAFieldItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAFieldListItem (parent){
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAFieldListItem.prototype = new CODAListItem();
	CODAFieldListItem.prototype.constructor = CODAFieldListItem;

	CODAFieldListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_FIELD_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onFieldDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAFieldListItem.prototype.onFieldDelete = function(data, message) {
		this.parent.removeChild(this);
		CODAFormsShell.section_fields_list.list.refreshPages();
	};

	CODAFieldListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);

		var fields_orderdownBt = new KButton(this, '', 'fieldorderdown', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAFieldListItem');
			widget.data.order = widget.data.order + 1;
			widget.kinky.put(widget, 'rest:' + widget.data.href, widget.data);
		});
		{
			fields_orderdownBt.setText('<i class="fa fa-chevron-down"></i> ' + gettext('$MOVE_DOWN'), true);
		}

		this.appendChild(fields_orderdownBt);

		var fields_orderupBt = new KButton(this, '', 'fieldorderup', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAFieldListItem');
			widget.data.order = widget.data.order - 1;
			widget.kinky.put(widget, 'rest:' + widget.data.href, widget.data);
		});
		{
			fields_orderupBt.setText('<i class="fa fa-chevron-up"></i> ' + gettext('$MOVE_UP'), true);
		}

		this.appendChild(fields_orderupBt);

	};

	CODAFieldListItem.prototype.move = function(order) {
		this.data.order = order;
		this.data.options = JSON.stringify(this.data.options);
		delete this.data.type_name;
		this.kinky.put(this, 'rest:' + this.data.href, this.data);
	};

	CODAFieldListItem.prototype.onPut = function(data, request) {
		CODAFormsShell.section_fields_list.list.refreshPages();
		this.enable();
		//this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	KSystem.included('CODAFieldListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAFieldsList(parent, data) {
	if(parent != null){
		CODAList.call(this, parent, data, '', { one_choice : true, add_button : true});
		this.pageSize = 5;
		this.table = {
			"label" : {
				label : gettext('$FORMS_LIST_LABEL')
			},
			"type_name" : {
				label : gettext('$FORMS_LIST_TYPE')
			}
		};
		this.LIST_MARGIN = 310;
		this.filterFields = [ 'label' ];
		this.orderBy = 'order';
	}
}

KSystem.include([
	'CODAList',
	'CODAFieldListItem'
], function(){
	CODAFieldsList.prototype = new CODAList();
	CODAFieldsList.prototype.constructor = CODAFieldsList;

	CODAFieldsList.prototype.addItems = function(list) {

		for(var index in this.elements) {
			var listItem = new CODAFieldListItem(list);
			listItem.data = this.elements[index];

			if(!!listItem.data.type && listItem.data.type != ''){
				switch (listItem.data.type){
					case 'textarea_small' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_TEXTAREASMALL');
						break;
					}
					case 'textarea_medium' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_TEXTAREAMEDIUM');
						break;
					}
					case 'textarea_large' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_TEXTAREALARGE');
						break;
					}
					case 'checkbox' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_CHECKBOX');
						break;
					}
					case 'zipcode' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_ZIPCODE');
						break;
					}
					case 'combobox' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_COMBOBOX');
						break;
					}
					case 'date' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_DATE');
						break;
					}
					case 'birthdate' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_BIRTHDATE');
						break;
					}
					case 'email' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_EMAIL');
						break;
					}
					case 'input' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_INPUT');
						break;
					}
					case 'listbox' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_LISTBOX');
						break;
					}
					case 'double' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_DOUBLE');
						break;
					}
					case 'integer' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_INTEGER');
						break;
					}
					case 'radiobutton' : {
						listItem.data.type_name = gettext('$FIELD_TYPE_RADIOBUTTON');
						break;
					}
				}
			}

			list.appendChild(listItem);
		}

	};

	CODAFieldsList.prototype.addItem = function() {
//		KBreadcrumb.dispatchURL({
//			hash : '/add-field' + this.parent.data.href
//		});
		KBreadcrumb.dispatchURL({
			hash : '/add-field-wizard' + this.parent.data.href
		});
	};

	CODAFieldsList.prototype.visible = function() {
		CODAList.prototype.visible.call(this);
		if (this.opened() && (!this.elements || this.elements.length == 0)) {
//			KBreadcrumb.dispatchURL({
//				hash : '/add-field' + this.parent.data.href
//			});
			KBreadcrumb.dispatchURL({
				hash : '/add-field-wizard' + this.parent.data.href
			});
		}
	};

	KSystem.included('CODAFieldsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAFieldWizard(parent, section_fields_href){
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODAFieldItemForm']);
		this.setTitle(gettext('$SECTION_FIELD_NEW'));
		this.hash = '/section/field';
		this.post_href = section_fields_href;
	}
}

KSystem.include([
 	'CODAWizard'
], function() {
	CODAFieldWizard.prototype = new CODAWizard();
	CODAFieldWizard.prototype.constructor = CODAFieldWizard;

	CODAFieldWizard.prototype.mergeValues = function() {
		var params = {};
		for (var v  = 0 ; v != this.values.length; v++) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}
		return params;
	};

	CODAFieldWizard.prototype.send = function(params) {
		try {
			if (!!params) {
				
				if(!params.order || params.order == ''){
					delete params.order;
				}
				
				if(!!params.required && params.required.length == 1 && params.required[0] == true){
					params.required = true;
				}else{
					params.required = false;
				}
				
				this.kinky.post(this, 'rest:' + this.post_href, params);
			}
		}
		catch (e) {

		}
	};

	CODAFieldWizard.prototype.onPost = CODAFieldWizard.prototype.onCreated = function(data, request) {
		CODAFormsShell.section_fields_list.list.refreshPages();
		this.parent.closeMe();
		this.enable();
	};

	KSystem.included('CODAFieldWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAFormItemForm(parent, href){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		this.target.collection = '/forms';
		this.nillable = true;
	}
}

KSystem.include([
	'CODASectionsList'
], function() {
	CODAFormItemForm.prototype = new CODAForm();
	CODAFormItemForm.prototype.constructor = CODAFormItemForm;

	CODAFormItemForm.prototype.setData = function(data) {
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODAFormItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAFormItemForm.prototype.addMinimizedPanel = function() {
		if(!!this.data && !!this.data.name){
			this.setTitle(gettext('$EDIT_FORM_PROPERTIES') + ' \u00ab' + this.data.name + '\u00bb');
		}else{
			this.setTitle(gettext('$ADD_FORM_PROPERTIES'));
		}

		var form_item_panel = new KPanel(this);
		{

			form_item_panel.hash = '/general';

			var code = null;
			if(!!this.data && !!this.data.code){
				code = new KStatic(form_item_panel, gettext('$FORM_ITEM_CODE'), 'code');
				code.setValue(this.data.code);
			}else{
				code = new KInput(form_item_panel, gettext('$FORM_ITEM_CODE'), 'code');
			}
			code.setHelp(gettext('$FORM_ITEM_CODE_HELP'), CODAForm.getHelpOptions());
			code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			form_item_panel.appendChild(code);

			var name = new KInput(form_item_panel, gettext('$FORM_ITEM_NAME') + '*', 'name');
			if(!!this.data && !!this.data.name){
				name.setValue(this.data.name);
			}
			name.setHelp(gettext('$FORM_ITEM_NAME_HELP'), CODAForm.getHelpOptions());
			name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			form_item_panel.appendChild(name);

		}

		this.addPanel(form_item_panel, gettext('$FORM_GENERALINFO_PANEL'), true);

	};

	CODAFormItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$CODA_FORMS_WIZ_' + parent.className.toUpperCase() + '_PANEL_TITLE');
			firstPanel.description = gettext('$CODA_FORMS_WIZ_' + parent.className.toUpperCase() + '_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-list-alt';

			if (parent.className == 'CODAWizard') {
				var name = new KInput(firstPanel, gettext('$FORM_ITEM_NAME') + '*', 'name');
				name.setHelp(gettext('$FORM_ITEM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				firstPanel.appendChild(name);
			}
			else {
				var code = null;
				code = new KInput(firstPanel, gettext('$FORM_ITEM_CODE'), 'code');
				code.setHelp(gettext('$FORM_ITEM_CODE_HELP'), CODAForm.getHelpOptions());
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				firstPanel.appendChild(code);

				var name = new KInput(firstPanel, gettext('$FORM_ITEM_NAME') + '*', 'name');
				name.setHelp(gettext('$FORM_ITEM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				firstPanel.appendChild(name);
			}
		}
		return [firstPanel];
	};

	CODAFormItemForm.prototype.onPost = CODAFormItemForm.prototype.onCreated = function(data, request){
		if (!!CODAFormsShell.forms_list && !!CODAFormsShell.forms_list.list) {
			CODAFormsShell.forms_list.list.refreshPages();
		}
		else {
			window.document.location.reload(true);
		}
		//this.parent.closeMe();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAFormItemForm.prototype.onPut = function(data, request){
		CODAFormsShell.forms_list.list.refreshPages();
//		this.parent.closeMe();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	KSystem.included('CODAFormItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODAFormListItem(parent) {
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODAFormListItem.prototype = new CODAListItem();
	CODAFormListItem.prototype.constructor = CODAFormListItem;

	CODAFormListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_FORM_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onFormDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAFormListItem.prototype.draw = function() {
		CODAListItem.prototype.draw.call(this);

		var sections_listBt = new KButton(this, '', 'sectionsList', function(event) {
			KDOM.stopEvent(event);
			var widget = KDOM.getEventWidget(event, 'CODAFormListItem');
			KBreadcrumb.dispatchURL({
				hash : '/sections-list' + widget.data.href
			});
		});
		{
			sections_listBt.setText('<i class="fa fa-list"></i> ' + gettext('$FORM_SECTIONS_LIST'), true);
		}

		this.appendChild(sections_listBt);

	};

	CODAFormListItem.prototype.onFormDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	KSystem.included('CODAFormListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODAFormsDashboard(parent, forms) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, forms);
	}
}

KSystem.include([
	'CODAMenuDashboard'
 ], function(){
	CODAFormsDashboard.prototype = new CODAMenuDashboard();
	CODAFormsDashboard.prototype.construtor = CODAFormsDashboard;

	CODAFormsDashboard.prototype.getIconFor = function(data) {
		var icon = {
			icon : 'text:' + String.fromCharCode(97 + this.nChildren) + '<i class="fa fa-pencil"></i>',
			activate : false,
			href : data.href,
			title : '«' + data.name + '»',
			description : '<span>' + gettext('$DASHBOARD_BUTTON_LIST_FORM_DESC').replace('$FORM_NAME', data.name) + '</span>',
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/sections-list' + data.href
			}, {
				icon : 'css:fa fa-trash-o',
				desc : gettext('$REMOVE'),
				events : [
					{
						name : 'click',
						callback : CODAFormsDashboard.remove
					}
				]
			}]
		}
		return new CODAIcon(this, icon);
	};

	CODAFormsDashboard.prototype.onFormDelete = function(data, request) {
		window.document.location.reload(true);
	};

	CODAFormsDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_FORM_CONFIRM'),
			callback : function() {
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onFormDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400,
			height: 300
		});
	};

	KSystem.included('CODAFormsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAFormsList (parent, data) {
	if (parent != null) {
		CODAList.call(this, parent, data, '', { one_choice : true, add_button : true});
		this.pageSize = 5;
		this.table = {
			"code" : {
				label : gettext('$FORMS_LIST_CODE')
			},
			"name" : {
				label : gettext('$FORMS_LIST_NAME')
			}
		};
		this.LIST_MARGIN = 50;
		this.filterFields = [ 'name' ];
		this.orderBy = 'code';
	}
}

KSystem.include([
	'CODAFormListItem'
], function() {
	CODAFormsList.prototype = new CODAList();
    CODAFormsList.prototype.constructor = CODAFormsList;

    CODAFormsList.prototype.addItems = function(list) {

    	for(var index in this.elements) {
    		var listItem = new CODAFormListItem(list);
    		listItem.data = this.elements[index];

    		list.appendChild(listItem);
    	}

    };

    CODAFormsList.prototype.addItem = function() {
//    	KBreadcrumb.dispatchURL({
//			hash : '/add-form'
//		});
    	KBreadcrumb.dispatchURL({
			hash : '/add-form-wizard'
		});
	};

	CODAFormsList.prototype.onNoElements = function(data, request) {
		this.getParent('KFloatable').closeMe();
//		KBreadcrumb.dispatchURL({
//			hash : '/add-form'
//		});
		KBreadcrumb.dispatchURL({
			hash : '/add-form-wizard'
		});
	};

    KSystem.included('CODAFormsList');
}, Kinky.CODA_INCLUDER_URL);
function CODAFormsShell(parent) {
	if(parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAFormsShell.actions);
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',

	'CODAFormsDashboard',
	'CODAFormsList',
	'CODAFormItemForm',
	'CODASectionItemForm',
	'CODAFieldItemForm',
	'CODASectionsListDashboard',
	'CODAFieldWizard'
], function(){
	CODAFormsShell.prototype = new CODAGenericShell();
	CODAFormsShell.prototype.constructor = CODAFormsShell;

	CODAFormsShell.prototype.loadShell = function() {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		this.kinky.get(this, 'rest:/forms?fields=elements,size', {}, headers, 'onLoadForms');
	};

	CODAFormsShell.prototype.onLoadForms = function(data, request) {
		this.forms = !!data.elements ? data.elements : [];
		this.draw();
	};

	CODAFormsShell.prototype.draw = function() {
		this.setTitle(gettext('$FORMS_SHELL_TITLE'));

		this.dashboard = new CODAFormsDashboard(this, this.forms);
		this.appendChild(this.dashboard);

		var addformbt = new KButton(this, gettext('$CODA_LIST_ADD_BUTTON_CODAFORMSLIST'), 'add-form', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/add-form-wizard'
			});
		});
		this.addToolbarButton(addformbt, 3);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODAFormsShell.sendRequest = function() {
	};

	CODAFormsShell.prototype.listForms = function(href, minimized) {
		CODAFormsShell.forms_list = new CODAFormsList(this, {
			links : {
				feed : { href : href + '?fields=elements,size'}
			}
		});
		CODAFormsShell.forms_list.setTitle(gettext('$LIST_FORMS_PROPERTIES'));
		this.makeDialog(CODAFormsShell.forms_list, {minimized : minimized, modal : true});

	};

	CODAFormsShell.prototype.addForm = function() {
		var configForm = new CODAFormItemForm(this, null);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAFormsShell.prototype.addFormWizard = function() {

		var screens = [ 'CODAFormItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_FORM_PROPERTIES'));
		wiz.hash = '/form';

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			if (!!CODAFormsShell.forms_list && !!CODAFormsShell.forms_list.list) {
				CODAFormsShell.forms_list.list.refreshPages();
			}
			else {
				window.document.location.reload(true);
			}
			this.parent.closeMe();
		};

		wiz.send = function(params) {
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + '/forms', params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);

	};

	CODAFormsShell.prototype.addSection = function(parent_href) {
		if(!parent_href && !!this.form_clicked){
			parent_href = this.form_clicked;
			delete this.form_clicked;
		}
		var configForm = new CODASectionItemForm(this, null, parent_href);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAFormsShell.prototype.addSectionWizardGetSections = function(parent_href) {

		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:' + parent_href, {}, headers, 'onSectionWizardGetSections');

	};

	CODAFormsShell.prototype.onSectionWizardGetSections = function(data, request) {
		if(!!data && !!data.elements){
			KCache.commit('/formsections', data.elements);
			this.addSectionWizard(request.service);
		}
	};

	CODAFormsShell.prototype.addSectionWizard = function(parent_href) {
		var screens = [ 'CODASectionItemForm'];
		var wiz = new CODAWizard(this, screens);
		wiz.setTitle(gettext('$ADD_SECTION_PROPERTIES'));
		if(parent_href != null){
			wiz.hash = parent_href;
		}else if(!!this.form_clicked){
			wiz.hash = this.form_clicked;
		}

		wiz.mergeValues = function() {
			var params = {};
			for (var v  = 0 ; v != this.values.length; v++) {
				for (var f in this.values[v]) {
					params[f] = this.values[v][f];
				}
			}
			return params;
		};

		wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
			this.getParent('KFloatable').closeMe();
			KBreadcrumb.dispatchURL({
				hash : '/sections-list' + request.service
			});
			this.parent.closeMe();
			return;
		};

		wiz.send = function(params) {
			if(!params.order){
				delete params.order;
			}
			try {
				if (!!params) {
					this.kinky.post(this, 'rest:' + wiz.hash, params, Kinky.SHELL_CONFIG[this.shell].headers);
				}
			}
			catch (e) {

			}
		};

		this.makeWizard(wiz);
	};

	CODAFormsShell.prototype.addField = function(parent_href) {
		var configForm = new CODAFieldItemForm(this, null, parent_href);
		this.makeDialog(configForm, {minimized : true});
	};

	CODAFormsShell.prototype.addFieldWizard = function(parent_href) {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:' + parent_href, {}, headers, 'onListSectionFields');
	};

	CODAFormsShell.prototype.onListSectionFields = function(data, request) {
		if(!!data && !!data.elements){
			KCache.commit('/sectionfields', data.elements);
		}
		this.makeWizard(new CODAFieldWizard(this, request.service));
	};

	CODAFormsShell.prototype.listSections = function(href, minimized) {
		var headers = { 'E-Tag' : '' + (new Date()).getTime() };
		headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
		this.kinky.get(this, 'rest:' + href + '/sections', {}, headers, 'onListSections');
	};

	CODAFormsShell.prototype.onListSections = function(data, request) {
		var sectionsListInfo = new CODASectionsListDashboard(this, {
				links : {
					feed : { href :  request.service }
				},
				filter_input : false,
				add_button : true 
			},
			(!!data.elements ? data.elements : []),
			request.service
		);
		sectionsListInfo.hash = '/sections/hash';
		sectionsListInfo.setTitle(gettext('$LIST_SECTIONS_PROPERTIES'));
		this.makeDialog(sectionsListInfo, { minimized : false, modal : true});
	};

	CODAFormsShell.prototype.editForm = function(href, minimized) {
		var configForm = new CODAFormItemForm(this, href);
		this.makeDialog(configForm, {minimized : minimized, modal : false});
	};

	CODAFormsShell.prototype.editSection = function(href, minimized) {
		var configForm = new CODASectionItemForm(this, href);
		{
			configForm.hash = '/sections/advanced';
			configForm.setTitle(gettext('$EDIT_SECTION_PROPERTIES'));
		}
		this.makeListForm(configForm, {minimized : false, modal : true});
	};

	CODAFormsShell.prototype.editField = function(href, minimized) {
		var configForm = new CODAFieldItemForm(this, href);
		this.makeMiddleDialog(configForm, {minimized : false, modal : true});
	};

	CODAFormsShell.prototype.onNoContent = function(data, request) {
		if(request.service.indexOf('/sections') == request.service.length - 9){
			this.form_clicked = request.service;
			KCache.clear('/formsections');
			CODAFormsShell.prototype.addSectionWizard.call(this);
		}
		else if(request.service.indexOf('/fields') == request.service.length - 7){
			KCache.clear('/sectionfields');
			this.makeWizard(new CODAFieldWizard(this, request.service));
		}
		else{
			CODAGenericShell.prototype.onNoContent.call(this, data, request);
		}
	};

	CODAFormsShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]){
			case 'list-forms' : {
				Kinky.getDefaultShell().listForms('/forms', false);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-form' : {
				Kinky.getDefaultShell().addForm();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-form-wizard' : {
				Kinky.getDefaultShell().addFormWizard();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-section' : {
				Kinky.getDefaultShell().addSection('/' + parts[2] + '/' + parts[3] + '/sections');
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-section-wizard' : {
				Kinky.getDefaultShell().addSectionWizardGetSections('/' + parts[2] + '/' + parts[3] + '/sections');
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-field' : {
				Kinky.getDefaultShell().addField('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5] + '/fields');
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-field-wizard' : {
				Kinky.getDefaultShell().addFieldWizard('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5] + '/fields');
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add-field-option-wizard' : {
				Kinky.getDefaultShell().addFieldOptionWizard('/' + parts[2] + '/' + parts[3] + '/' + parts[4] + '/' + parts[5] + '/' + parts[6] + '/' + parts[7]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'sections-list' : {
				Kinky.getDefaultShell().listSections('/' + parts[2] + '/' + parts[3]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit' : {
				if (parts[parts.length - 2] == 'forms'){
					Kinky.getDefaultShell().editForm('/forms/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}else if (parts[parts.length - 2] == 'sections'){
					Kinky.getDefaultShell().editSection('/forms/' + parts[3] + '/sections/' + parts[5], false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}else if (parts[parts.length - 2] == 'fields'){
					Kinky.getDefaultShell().editField('/forms/' + parts[3] + '/sections/' + parts[5] + '/fields/' + parts[7], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
			case 'edit-full' : {
				if (parts[parts.length - 2] == 'forms'){
					Kinky.getDefaultShell().editForm('/forms/' + parts[3], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}else if (parts[parts.length - 2] == 'sections'){
					Kinky.getDefaultShell().editSection('/forms/' + parts[3] + '/sections/' + parts[5], false);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}else if (parts[parts.length - 2] == 'fields'){
					Kinky.getDefaultShell().editField('/forms/' + parts[3] + '/sections/' + parts[5] + '/fields/' + parts[7], true);
					KBreadcrumb.dispatchURL({
						hash : ''
					});
				}
				break;
			}
		}

	};

	KSystem.included('CODAFormsShell');
}, Kinky.CODA_INCLUDER_URL);
function CODASectionItemForm(parent, href, parent_href){
	if (parent != null) {
		CODAForm.call(this, parent, href == null ? null : href, null, null);
		if(parent_href != null){
			this.target.collection = parent_href;
		}
		this.nillable = true;
		if(href != null){
			this.href_to_sections_list = href.substring(0, href.indexOf('/sections') + 9);
		}else if(parent_href != null){
			this.href_to_sections_list = parent_href;
		}
	}
}

KSystem.include([
	'CODAForm',
	'CODAFieldsList'
], function() {
	CODASectionItemForm.prototype = new CODAForm();
	CODASectionItemForm.prototype.constructor = CODASectionItemForm;

	CODASectionItemForm.prototype.load = function(){
		if(!!this.href_to_sections_list){
			var headers = { 'E-Tag' : '' + (new Date()).getTime() };
			headers = KSystem.merge(headers, Kinky.SHELL_CONFIG[this.shell].headers);
			this.kinky.get(this, 'rest:' + this.href_to_sections_list, {}, headers, 'onListSections');
		}else{
			CODAForm.prototype.load.call(this);
		}
	};

	CODASectionItemForm.prototype.onListSections = function(data, request){
		if(!!data && !!data.elements){
			KCache.commit('/formsections', data.elements);
		}
		CODAForm.prototype.load.call(this);
	};

	CODASectionItemForm.prototype.setData = function(data) {
		if(!!data) {
			this.target._id = this.target.href;
			this.target.data = data;
			this.data = data;
		}
	};

	CODASectionItemForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODASectionItemForm.prototype.addMinimizedPanel = function() {
		this.setTitle(gettext('$EDIT_SECTION_PROPERTIES') + ' \u00ab' + this.data.label + '\u00bb');

		var section_item_panel = new KPanel(this);
		{

			section_item_panel.hash = '/general';

			var code = new KHidden(section_item_panel, 'code');
			{
				if (!!this.data && !!this.data.code){
					code.setValue(this.data.code);
				}
			}
			section_item_panel.appendChild(code);

			var label = new KInput(section_item_panel, gettext('$SECTION_ITEM_LABEL'), 'label');
			{
				if(!!this.data && !!this.data.label){
					label.setValue(this.data.label);
				}
				label.setHelp(gettext('$SECTION_ITEM_LABEL_HELP'), CODAForm.getHelpOptions());
			}
			section_item_panel.appendChild(label);

			var formsections = KCache.restore('/formsections');
			if (!!formsections) {
				var formsections_combo = new KCombo(section_item_panel, gettext('$SECTION_ITEM_ORDER'), 'order');
				{
					for (var index in formsections){
						if((!!this.data && !!this.data.order && this.data.order == formsections [index].order)){
							formsections_combo.addOption(formsections [index].order, gettext('$ORDER_SAME'), (!!this.data && !!this.data.order && this.data.order == formsections [index].order) ? true : false);
						}else{
							if (index == 0){
								formsections_combo.addOption(1, gettext('$ORDER_BEGINNING'), (!!this.data && !!this.data.order && this.data.order == 1) ? true : false);
							}else{
								if(index < this.data.order){
									formsections_combo.addOption(formsections [index].order, gettext('$SECTION_ORDER_AFTER') + ' ' + (!!formsections [index-1].label ? formsections [index-1].label : formsections [index].order), (!!this.data && !!this.data.order && this.data.order == formsections [index].order) ? true : false);	
								}else{
									formsections_combo.addOption(formsections [index].order, gettext('$SECTION_ORDER_AFTER') + ' ' + (!!formsections [index].label ? formsections [index].label : formsections [index].order), (!!this.data && !!this.data.order && this.data.order == formsections [index].order) ? true : false);	
								}
							}
						}
					}
					formsections_combo.setHelp(gettext('$SECTION_ITEM_ORDER_HELP'), CODAForm.getHelpOptions());
				}
				section_item_panel.appendChild(formsections_combo);
			}

		}

		this.addPanel(section_item_panel, gettext('$FORM_GENERALINFO_PANEL'), false);
		
		CODAFormsShell.section_fields_list = new CODAFieldsList(this, {
			links : {
				feed : { href : this.data.href + '/fields' + '?fields=elements,size'}
			}
		});
		CODAFormsShell.section_fields_list.hash = '/fields';
		this.addPanel(CODAFormsShell.section_fields_list, gettext('$FIELDS_LIST_PANEL'), true);

	};

	CODASectionItemForm.prototype.setDefaults = function(params){
		if(!params.order || params.order == ''){
			delete params.order;
		}
	};

	CODASectionItemForm.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$CODA_FORM_SECTIONS_WIZ_' + parent.className.toUpperCase() + '_PANEL_TITLE');
			firstPanel.description = gettext('$CODA_FORM_SECTIONS_WIZ_' + parent.className.toUpperCase() + '_PANEL_DESC');
			firstPanel.icon = 'css:fa fa-list-alt';

			var label = new KInput(firstPanel, gettext('$SECTION_ITEM_LABEL') + ' *', 'label');
			label.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			label.setHelp(gettext('$SECTION_ITEM_LABEL_HELP'), CODAForm.getHelpOptions());
			firstPanel.appendChild(label);

			var formsections = KCache.restore('/formsections');
			if (!!formsections) {
				var formsections_combo = new KCombo(firstPanel, gettext('$SECTION_ITEM_ORDER'), 'order');
				{
					formsections_combo.addOption(1, gettext('$ORDER_BEGINNING'), false);
					for (var index in formsections){
						formsections_combo.addOption(formsections [index].order + 1, gettext('$SECTION_ORDER_AFTER') + ' ' + (!!formsections [index].label ? formsections [index].label : formsections [index].order), false);
					}
					formsections_combo.setHelp(gettext('$SECTION_ITEM_ORDER_HELP'), CODAForm.getHelpOptions());
				}
				firstPanel.appendChild(formsections_combo);
			}
		}
		return [firstPanel];
	};

	CODASectionItemForm.prototype.onNoContent = function(data, request){
		CODAForm.prototype.load.call(this);
	};

	CODASectionItemForm.prototype.onPost = CODASectionItemForm.prototype.onCreated = function(data, request){
		this.getParent('CODADashboard').refresh();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODASectionItemForm.prototype.onPut = function(data, request){
		this.refresh();
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	KSystem.included('CODASectionItemForm');
}, Kinky.CODA_INCLUDER_URL);

function CODASectionListItem (parent){
	if(parent != null) {
		CODAListItem.call(this, parent);
	}
}

KSystem.include([
	'CODAListItem'
], function(){
	CODASectionListItem.prototype = new CODAListItem();
	CODASectionListItem.prototype.constructor = CODASectionListItem;

	CODASectionListItem.prototype.remove = function() {
		var widget = this;
		KConfirmDialog.confirm({
			question : gettext('$DELETE_SECTION_CONFIRM'),
			callback : function() {
				widget.kinky.remove(widget, 'rest:' + widget.data.href, {}, Kinky.SHELL_CONFIG[this.shell].headers, 'onSectionDelete');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODASectionListItem.prototype.onSectionDelete = function(data, message) {
		this.parent.removeChild(this);
	};

	KSystem.included('CODASectionListItem');
}, Kinky.CODA_INCLUDER_URL);
function CODASectionsListDashboard(parent, options, apps, form_href) {
	if (parent != null) {
		CODAFormIconDashboard.call(this, parent, options, apps);
		this.elements_number = apps.length;
		this.filterFields = [ 'name' ];
		if(!!form_href){
			this.section_form_href = form_href;
		}
		if(apps.length == 0){
			this.addItem();
		}
	}
}

KSystem.include([
    'KConfirmDialog',
	'CODAIcon',
	'CODAFormIconDashboard'
], function() {
	CODASectionsListDashboard.prototype = new CODAFormIconDashboard();
	CODASectionsListDashboard.prototype.constructor = CODASectionsListDashboard;

	CODASectionsListDashboard.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-section-wizard' + this.section_form_href
		});
	};

	CODASectionsListDashboard.prototype.getIconFor = function(data) {

		var dummy = window.document.createElement('p');
		if(!!data.description){
			dummy.innerHTML = data.description;
		}else{
			dummy.innerHTML = '';
		}

		var aditional = '';

		var actions = [];
		actions.push({
			icon : 'css:fa fa-pencil',
			link : '#/edit' + data.href,
			desc : 'Gerir'
		});
		actions.push({
			icon : 'css:fa fa-trash-o',
			desc : 'Remover',
			events : [
			{
				name : 'click',
				callback : CODASectionsListDashboard.remove
			}

			]
		});

		return new CODAIcon(this, {
			icon : 'text:' + (this.nChildren + 1) + '.',
			activate : false,
			title : data.label,
			href : data.href,
			description : '<span>' + dummy.textContent + '</span>' + aditional,
			actions : actions
		});
	};

	CODASectionsListDashboard.remove = function(event) {
		KDOM.stopEvent(event);
		var widget = KDOM.getEventWidget(event);
		KConfirmDialog.confirm({
			question : gettext('$DELETE_SECTION_CONFIRM'),
			callback : function() {
				widget.parent.kinky.remove(widget.parent, 'rest:' + widget.options.href, {}, null, 'onSectionDelete');
			},
			shell : widget.shell,
			modal : true,
			width : 400,
			height: 300
		});
	};

	CODASectionsListDashboard.prototype.onSectionDelete = function(data, request) {
		this.getParent('KFloatable').closeMe();
		if(this.elements_number == 1){
			KBreadcrumb.dispatchURL({
				hash : '/list-forms'
			});
		}else{
			KBreadcrumb.dispatchURL({
				hash : '/sections-list' + request.service
			});
		}
		return;
	};

	KSystem.included('CODASectionsListDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODASectionsList(parent, data) {
	if(parent != null){
		CODAList.call(this, parent, data, '', { one_choice : true, add_button : true});
		this.pageSize = 5;
		this.table = {
			"code" : {
				label : gettext('$FORMS_LIST_CODE')
			},
			"label" : {
				label : gettext('$FORMS_LIST_LABEL')
			},
			"order" : {
				label : gettext('$FORMS_LIST_ORDER')
			}
		};
		this.LIST_MARGIN = 250;
		this.filterFields = [ 'label' ];
		this.orderBy = 'code';
	}
}

KSystem.include([
	'CODAList',
	'KLink',
	'CODASectionListItem'
], function(){
	CODASectionsList.prototype = new CODAList();
	CODASectionsList.prototype.constructor = CODASectionsList;

	CODASectionsList.prototype.addItems = function(list) {

		for(var index in this.elements) {
			var listItem = new CODASectionListItem(list);
			listItem.data = this.elements[index];

			list.appendChild(listItem);
		}

	};

	CODASectionsList.prototype.addItem = function() {
		KBreadcrumb.dispatchURL({
			hash : '/add-section' + this.parent.data.href
		});
	};

	CODASectionsList.prototype.visible = function() {
		CODAList.prototype.visible.call(this);
		if (this.opened() && (!this.elements || this.elements.length == 0)) {
			KBreadcrumb.dispatchURL({
				hash : '/add-section' + this.parent.data.href
			});
		}
	};

	KSystem.included('CODASectionsList');
}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

