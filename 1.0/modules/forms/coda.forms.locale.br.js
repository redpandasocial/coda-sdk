///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$FORMS_SHELL_TITLE', 'Formulários', 'br');

settext('$DASHBOARD_BUTTON_LIST_FORM', 'Formul\u00e1rio ', 'br');
settext('$DASHBOARD_BUTTON_LIST_FORM_DESC', 'Adicione e configure as páginas e campos de cada página do formulário «$FORM_NAME»', 'br');

settext('$CODA_LIST_ADD_BUTTON_CODAFORMSLIST', '<i class="fa fa-pencil"></i> Novo formulário', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODASECTIONSLISTDASHBOARD', '<i class="fa fa-file-o"></i> Nova página', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAFIELDSLIST', '<i class="fa fa-th-list"></i> Novo campo', 'br');

///////////////////////////////////////////////////////////////////
//FORMS
settext('$ADD_FORM_PROPERTIES', 'Novo Formulário', 'br');
settext('$EDIT_FORM_PROPERTIES', 'Editar Formulário ', 'br');
settext('$ADD_SECTION_PROPERTIES', 'Nova Página', 'br');
settext('$EDIT_SECTION_PROPERTIES', 'Editar Página ', 'br');
settext('$ADD_FIELD_PROPERTIES', 'Novo Campo', 'br');
settext('$EDIT_FIELD_PROPERTIES', 'Editar Campo', 'br');
settext('$LIST_FORMS_PROPERTIES', 'Formul\u00e1rios', 'br');
settext('$LIST_SECTIONS_PROPERTIES', 'Páginas do formulário', 'br');
settext('$FORM_ITEM_CODE', 'C\u00f3digo', 'br');
settext('$FORM_ITEM_NAME', 'Nome', 'br');
settext('$FORM_ITEM_OPTION', 'Opção', 'br');
settext('$FORM_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$SECTIONS_LIST_PANEL', '<i class="fa fa-adjust"></i> Sec\u00e7\u00f5es', 'br');
settext('$FIELDS_LIST_PANEL', '<i class="fa fa-th-list"></i> Campos', 'br');
settext('$FIELD_OPTIONS_LIST_PANEL', '<i class="fa fa-bars"></i> Opções de Escolha', 'br');

settext('$FORM_SECTIONS_LIST', 'Páginas', 'br');

settext('$SECTIONS_LIST_BACK', 'Voltar \u00e0 lista de formul\u00e1rios', 'br');
settext('$SECTION_FIELD_NEW', 'Novo Campo', 'br');

settext('$SECTION_ITEM_LABEL', 'Nome', 'br');
settext('$SECTION_ITEM_ORDER', 'Posição', 'br');
settext('$FIELD_ITEM_LABEL', 'Nome/Etiqueta/Legenda', 'br');
settext('$FIELD_ITEM_TYPE', 'Tipo de Campo', 'br');
settext('$FIELD_ITEM_ORDER', 'Posição', 'br');

settext('$FIELD_TYPE_INPUT', 'Input', 'br');
settext('$FIELD_TYPE_TEXTAREASMALL', 'Caixa de texto - pequena', 'br');
settext('$FIELD_TYPE_TEXTAREAMEDIUM', 'Caixa de texto - m\u00e9dia', 'br');
settext('$FIELD_TYPE_TEXTAREALARGE', 'Caixa de texto - grande', 'br');
settext('$FIELD_TYPE_DATE', 'Data', 'br');
settext('$FIELD_TYPE_BIRTHDATE', 'Data de Nascimento', 'br');
settext('$FIELD_TYPE_RADIOBUTTON', 'Radio button (escolha única)', 'br');
settext('$FIELD_TYPE_CHECKBOX', 'Check box (múltipla escolha)', 'br');
settext('$FIELD_TYPE_COMBOBOX', 'Combo Box (caixa de escolha única)', 'br');
settext('$FIELD_TYPE_LISTBOX', 'List Box (caixa de múltipla escolha)', 'br');
settext('$FIELD_TYPE_EMAIL', 'Endere\u00e7o de email', 'br');
settext('$FIELD_TYPE_ZIPCODE', 'C\u00f3digo-Postal', 'br');
settext('$FIELD_TYPE_INTEGER', 'N\u00famero Inteiro', 'br');
settext('$FIELD_TYPE_DOUBLE', 'N\u00famero Fraccion\u00e1rio de Precis\u00e3o Dupla', 'br');

settext('$FIELD_ITEM_OPTIONS', 'Op\u00e7\u00f5es (inserir objecto JSON)', 'br');

settext('$FIELD_ITEM_REQUIRED', 'Campo de preenchimento obrigat\u00f3rio?', 'br');
settext('$FIELD_REQUIRED_YES', 'SIM', 'br');
settext('$FIELD_REQUIRED_NO', 'N\u00c3O', 'br');

settext('$FIELD_ADD_OPTION', 'Nova op\u00e7\u00e3o', 'br');
settext('$FIELD_REMOVE_OPTION', 'Remover op\u00e7\u00e3o', 'br');
settext('$ORDER_BEGINNING', 'No in\u00edcio', 'br');
settext('$ORDER_AFTER', 'Depois do campo - ', 'br');
settext('$SECTION_ORDER_AFTER', 'Depois da página - ', 'br');
settext('$MOVE_DOWN', 'Mover para baixo', 'br');
settext('$MOVE_UP', 'Mover para cima', 'br');

settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_ID', 'Valor da Opção', 'br');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_LABEL', 'Nome/Etiqueta/Legenda da Opção', 'br');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_PARENTCODE', 'Identificador do Campo (não alterar)', 'br');
settext('$CODA_LIST_ADD_BUTTON_CODAFIELDITEMFORM_FIELD_OPTIONSARRAYLIST', '<i class="fa fa-bars"></i> Nova Opção de Escolha', 'br');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_WIZARD', 'Nova Opção de Escolha', 'br');
settext('$CODAFIELDITEMFORM_ADD_ARRAY_ELEMENT_PANEL_TITLE', 'Código e Texto', 'br');
settext('$CODAFIELDITEMFORM_ADD_ARRAY_ELEMENT_PANEL_DESC', 'Defina o código que deseja identificar essa opção.<br>Defina o texto que irá ser apresentado ao usuário como opção.<br>', 'br');
settext('$CODAFIELDITEMFORM_EDIT_ARRAY_ELEMENT_PROPERTIES', 'Editar Opção de Escolha', 'br');

///////////////////////////////////////////////////////////////////
//LISTS
settext('$FORMS_LIST_CODE', 'C\u00f3digo', 'br');
settext('$FORMS_LIST_NAME', 'Nome', 'br');
settext('$FORMS_LIST_LABEL', 'Nome/Etiqueta/Legenda', 'br');
settext('$FORMS_LIST_ORDER', 'Posição', 'br');
settext('$FORMS_LIST_TYPE', 'Tipo de Campo', 'br');

settext('$DELETE_FORM_CONFIRM', 'Tem certeza que deseja remover o formul\u00e1rio?', 'br');
settext('$DELETE_SECTION_CONFIRM', 'Tem certeza que deseja remover a página?', 'br');
settext('$DELETE_FIELD_CONFIRM', 'Tem certeza que deseja remover o campo?', 'br');

///////////////////////////////////////////////////////////////////
//WIZARD
settext('$SECTION_FIELD_WIZARD_TITLE1', 'Campo de página', 'br');
settext('$SECTION_FIELD_WIZARD_INFO1', 'Defina um código para o campo que será único para a página.<br/> Define a label (nome) que será apresentado.<br/>Escolha em que posição vai ficar.<br>Selecione o tipo de campo.<br/><br/>Se escolher um campo que defina as opções de escolha, você poderá adicionar selecionando o campo na listagem e clicando em «Opções de Escolha».', 'br');

settext('$SECTION_FIELD_WIZARD_TITLE2', 'Opções de campo', 'br');
settext('$SECTION_FIELD_WIZARD_INFO2', 'Defina as opções que deseja disponibilizar para seleção como valor do campo no preenchimento do formulário<br>Não há limitação do número de opções disponíveis<br>Adicione uma nova opção pelo botão + e remova uma das existentes pelo botão -', 'br');

settext('$CODA_FORMS_WIZ_CODAWIZARD_PANEL_TITLE', 'Formulário', 'br');
settext('$CODA_FORMS_WIZ_CODAWIZARD_PANEL_DESC', 'Defina o identificador textual para o formulário. Servirá para o identificar nesta plataforma.<br>', 'br');

settext('$CODA_FORM_SECTIONS_WIZ_CODAAPPLICATIONWIZARD_PANEL_TITLE', 'Página inicial do Formulário', 'br');
settext('$CODA_FORM_SECTIONS_WIZ_CODAAPPLICATIONWIZARD_PANEL_DESC', 'Introduza o nome para a primeira página do formulário.<br><i>Podes adicionar mais páginas e campos para cada uma das páginas na área de gestão de formulários.</i>', 'br');

settext('$CODA_FORM_SECTIONS_WIZ_CODAWIZARD_PANEL_TITLE', 'Página de Formulário', 'br');
settext('$CODA_FORM_SECTIONS_WIZ_CODAWIZARD_PANEL_DESC', 'Introduz o nome para a página do formulário.<br>Selecione a posição onde será inserida.<br><br>', 'br');

KSystem.included('CODAFormsLocale_pt');
///////////////////////////////////////////////////////////////////
// FORMS
settext('$FIELD_ITEM_TYPE_HELP', 'Escolha o tipo de campo que deseja adicionar<br>O tipo de campo que escolher dará reflexo no formato e nas validações que serão feitas no preenchimento<br>Se o tipo de campo escolhido for Radio Button, Combo Box ou List Box terá que definir as opções que deseja disponibilizar para seleção', 'br');
settext('$FIELD_ITEM_LABEL_HELP', 'Texto que será apresentado como nome/etiqueta/legenda do campo.', 'br');
settext('$FIELD_ITEM_REQUIRED_HELP', 'Indique se o campo terá que ser ou não preenchido para que o usuário possa enviar o formulário.', 'br');
settext('$FIELD_OPTIONS_LIST_CODE_HELP', 'Valor da opção. Poderá ser igual ao Nome/Etiqueta/Legenda se não pretende armazenar um valor diferente do apresentado ao usuário do aplicativo.', 'br');
settext('$FIELD_OPTIONS_LIST_LABEL_HELP', 'Texto que será apresentado ao usuário do aplicativo.', 'br');
settext('$FORM_ITEM_CODE_HELP', 'Código do campo. Serve para referência interna e não será apresentado ao usuário.', 'br');

settext('$FORM_ITEM_NAME_HELP', 'Texto para identificar o formulário nesta plataforma.', 'br');
settext('$SECTION_ITEM_LABEL_HELP', 'Texto para identificar a página de formulário nesta plataforma e no aplicativo. Irá aparecer como nome da página.', 'br');
settext('$SECTION_ITEM_ORDER_HELP', 'Posição em que a página irá aparecer no preenchimento do formulário.', 'br');

settext('$FIELD_ITEM_ORDER_HELP', 'Posição em que o campo irá aparecer na página no preenchimento do formulário.', 'br');

settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_ID_HELP', '', 'br');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_LABEL_HELP', '', 'br');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_PARENTCODE_HELP', '', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAFormsLocale_Help_pt');

