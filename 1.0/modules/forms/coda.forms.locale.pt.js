///////////////////////////////////////////////////////////////////
// FORMS
settext('$FIELD_ITEM_TYPE_HELP', 'Escolhe o tipo de campo que queres adicionar<br>A escolha do tipo de campo tem reflexo no formato e nas validações que são feitas na altura do seu preenchimento<br>Se o tipo de campo escolhido for Radio Button, Combo Box ou List Box tens que definir as opções que queres disponibilizar para seleção', 'pt');
settext('$FIELD_ITEM_LABEL_HELP', 'Texto a ser apresentado como nome/etiqueta/legenda do campo.', 'pt');
settext('$FIELD_ITEM_REQUIRED_HELP', 'Indica se o campo tem que ser ou não preenchido para que o utilizador possa submeter o formulário.', 'pt');
settext('$FIELD_OPTIONS_LIST_CODE_HELP', 'Valor da da opção. Poderá ser igual ao Nome/Etiqueta/Legenda se não pretender armazenar um valor diferente do apresentado ao utilizador da aplicação.', 'pt');
settext('$FIELD_OPTIONS_LIST_LABEL_HELP', 'Texto a ser apresentado ao utilizador da aplicação.', 'pt');
settext('$FORM_ITEM_CODE_HELP', 'Código do campo. Serve para referência interna e não será apresentado ao utilizador.', 'pt');

settext('$FORM_ITEM_NAME_HELP', 'Identificador textual para o formulário. Servirá para o identificar nesta plataforma.', 'pt');
settext('$SECTION_ITEM_LABEL_HELP', 'Identificador textual para o página de formulário. Servirá para o identificar nesta plataforma e na aplicação. Irá aparecer como nome da página.', 'pt');
settext('$SECTION_ITEM_ORDER_HELP', 'Posição em que a página irá aparecer no preenchimento do formulário.', 'pt');

settext('$FIELD_ITEM_ORDER_HELP', 'Posição em que o campo irá aparecer na página no preenchimento do formulário.', 'pt');

settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_ID_HELP', '', 'pt');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_LABEL_HELP', '', 'pt');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_PARENTCODE_HELP', '', 'pt')

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAFormsLocale_Help_pt');

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$FORMS_SHELL_TITLE', 'Formulários', 'pt');

settext('$DASHBOARD_BUTTON_LIST_FORM', 'Formul\u00e1rio ', 'pt');
settext('$DASHBOARD_BUTTON_LIST_FORM_DESC', 'Adiciona e configura as páginas e campos de cada página do formulário «$FORM_NAME»', 'pt');

settext('$CODA_LIST_ADD_BUTTON_CODAFORMSLIST', '<i class="fa fa-pencil"></i> Novo formulário', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODASECTIONSLISTDASHBOARD', '<i class="fa fa-file-o"></i> Nova página', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAFIELDSLIST', '<i class="fa fa-th-list"></i> Novo campo', 'pt');

///////////////////////////////////////////////////////////////////
//FORMS
settext('$ADD_FORM_PROPERTIES', 'Novo Formulário', 'pt');
settext('$EDIT_FORM_PROPERTIES', 'Editar Formulário ', 'pt');
settext('$ADD_SECTION_PROPERTIES', 'Nova Página', 'pt');
settext('$EDIT_SECTION_PROPERTIES', 'Editar Página ', 'pt');
settext('$ADD_FIELD_PROPERTIES', 'Novo Campo', 'pt');
settext('$EDIT_FIELD_PROPERTIES', 'Editar Campo', 'pt');
settext('$LIST_FORMS_PROPERTIES', 'Formul\u00e1rios', 'pt');
settext('$LIST_SECTIONS_PROPERTIES', 'Páginas do formulário', 'pt');
settext('$FORM_ITEM_CODE', 'C\u00f3digo', 'pt');
settext('$FORM_ITEM_NAME', 'Nome', 'pt');
settext('$FORM_ITEM_OPTION', 'Opção', 'pt');
settext('$FORM_GENERALINFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$SECTIONS_LIST_PANEL', '<i class="fa fa-adjust"></i> Sec\u00e7\u00f5es', 'pt');
settext('$FIELDS_LIST_PANEL', '<i class="fa fa-th-list"></i> Campos', 'pt');
settext('$FIELD_OPTIONS_LIST_PANEL', '<i class="fa fa-bars"></i> Opções de Escolha', 'pt');

settext('$FORM_SECTIONS_LIST', 'Páginas', 'pt');

settext('$SECTIONS_LIST_BACK', 'Voltar \u00e0 lista de formul\u00e1rios', 'pt');
settext('$SECTION_FIELD_NEW', 'Novo Campo', 'pt');

settext('$SECTION_ITEM_LABEL', 'Nome', 'pt');
settext('$SECTION_ITEM_ORDER', 'Posição', 'pt');
settext('$FIELD_ITEM_LABEL', 'Nome/Etiqueta/Legenda', 'pt');
settext('$FIELD_ITEM_TYPE', 'Tipo de Campo', 'pt');
settext('$FIELD_ITEM_ORDER', 'Posição', 'pt');

settext('$FIELD_TYPE_INPUT', 'Input', 'pt');
settext('$FIELD_TYPE_TEXTAREASMALL', 'Caixa de texto - pequena', 'pt');
settext('$FIELD_TYPE_TEXTAREAMEDIUM', 'Caixa de texto - m\u00e9dia', 'pt');
settext('$FIELD_TYPE_TEXTAREALARGE', 'Caixa de texto - grande', 'pt');
settext('$FIELD_TYPE_DATE', 'Data', 'pt');
settext('$FIELD_TYPE_BIRTHDATE', 'Data de Nascimento', 'pt');
settext('$FIELD_TYPE_RADIOBUTTON', 'Radio button (escolha única)', 'pt');
settext('$FIELD_TYPE_CHECKBOX', 'Check box (escolha múltipla)', 'pt');
settext('$FIELD_TYPE_COMBOBOX', 'Combo Box (caixa de escolha única)', 'pt');
settext('$FIELD_TYPE_LISTBOX', 'List Box (caixa de escolha múltipla)', 'pt');
settext('$FIELD_TYPE_EMAIL', 'Endere\u00e7o de email', 'pt');
settext('$FIELD_TYPE_ZIPCODE', 'C\u00f3digo-Postal', 'pt');
settext('$FIELD_TYPE_INTEGER', 'N\u00famero Inteiro', 'pt');
settext('$FIELD_TYPE_DOUBLE', 'N\u00famero Fraccion\u00e1rio de Precis\u00e3o Dupla', 'pt');

settext('$FIELD_ITEM_OPTIONS', 'Op\u00e7\u00f5es (inserir objecto JSON)', 'pt');

settext('$FIELD_ITEM_REQUIRED', 'Campo de preenchimento obrigat\u00f3rio?', 'pt');
settext('$FIELD_REQUIRED_YES', 'SIM', 'pt');
settext('$FIELD_REQUIRED_NO', 'N\u00c3O', 'pt');

settext('$FIELD_ADD_OPTION', 'Nova op\u00e7\u00e3o', 'pt');
settext('$FIELD_REMOVE_OPTION', 'Remover op\u00e7\u00e3o', 'pt');
settext('$ORDER_BEGINNING', 'No in\u00edcio', 'pt');
settext('$ORDER_AFTER', 'Depois do campo - ', 'pt');
settext('$ORDER_SAME', 'Manter posição atual', 'pt');
settext('$SECTION_ORDER_AFTER', 'Depois da página - ', 'pt');
settext('$MOVE_DOWN', 'Mover para baixo', 'pt');
settext('$MOVE_UP', 'Mover para cima', 'pt');

settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_ID', 'Valor da Opção', 'pt');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_LABEL', 'Nome/Etiqueta/Legenda da Opção', 'pt');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_PARENTCODE', 'Identificador do Campo (não alterar)', 'pt');
settext('$CODA_LIST_ADD_BUTTON_CODAFIELDITEMFORM_FIELD_OPTIONSARRAYLIST', '<i class="fa fa-bars"></i> Nova Opção de Escolha', 'pt');
settext('$CODAFIELDITEMFORM_ARRAY_ELEMENT_WIZARD', 'Nova Opção de Escolha', 'pt');
settext('$CODAFIELDITEMFORM_ADD_ARRAY_ELEMENT_PANEL_TITLE', 'Código e Texto', 'pt');
settext('$CODAFIELDITEMFORM_ADD_ARRAY_ELEMENT_PANEL_DESC', 'Define o código pelo qual queres identificar esta opção.<br>Define o texto que irá ser apresentado ao utilizador como opção.<br>', 'pt');
settext('$CODAFIELDITEMFORM_EDIT_ARRAY_ELEMENT_PROPERTIES', 'Editar Opção de Escolha', 'pt');

///////////////////////////////////////////////////////////////////
//LISTS
settext('$FORMS_LIST_CODE', 'C\u00f3digo', 'pt');
settext('$FORMS_LIST_NAME', 'Nome', 'pt');
settext('$FORMS_LIST_LABEL', 'Nome/Etiqueta/Legenda', 'pt');
settext('$FORMS_LIST_ORDER', 'Posição', 'pt');
settext('$FORMS_LIST_TYPE', 'Tipo de Campo', 'pt');

settext('$DELETE_FORM_CONFIRM', 'Tens a certeza que queres remover o formul\u00e1rio?', 'pt');
settext('$DELETE_SECTION_CONFIRM', 'Tens a certeza que queres remover a página?', 'pt');
settext('$DELETE_FIELD_CONFIRM', 'Tens a certeza que queres remover o campo?', 'pt');

///////////////////////////////////////////////////////////////////
//WIZARD
settext('$SECTION_FIELD_WIZARD_TITLE1', 'Campo de página', 'pt');
settext('$SECTION_FIELD_WIZARD_INFO1', 'Define um código para o campo que tem que ser único para a página.<br/> Define a label (nome) com que será apresentado.<br/>Escolhe em que posição vai ficar.<br>Selecciona o tipo de campo.<br/><br/>Se escolheres um campo que implique a definição de opções de escolha, poderás adicioná-las seleccionando o campo na listagem e clicando em «Opções de Escolha».', 'pt');

settext('$SECTION_FIELD_WIZARD_TITLE2', 'Opções de campo', 'pt');
settext('$SECTION_FIELD_WIZARD_INFO2', 'Define as opções que quer disponibilizar para selecção como valor do campo no preenchimento do formulário<br>Não há limitação do número de opções disponíveis<br>Adiciona uma nova opção pelo botão + e remove uma das existentes pelo botão -', 'pt');

settext('$CODA_FORMS_WIZ_CODAWIZARD_PANEL_TITLE', 'Formulário', 'pt');
settext('$CODA_FORMS_WIZ_CODAWIZARD_PANEL_DESC', 'Define o identificador textual para o formulário. Servirá para o identificar nesta plataforma.<br>', 'pt');

settext('$CODA_FORM_SECTIONS_WIZ_CODAAPPLICATIONWIZARD_PANEL_TITLE', 'Página inicial do Formulário', 'pt');
settext('$CODA_FORM_SECTIONS_WIZ_CODAAPPLICATIONWIZARD_PANEL_DESC', 'Introduz o nome para a primeira página do formulário.<br><i>Podes adicionar mais páginas e campos para cada uma das páginas na área de gestão de formulários.</i>', 'pt');

settext('$CODA_FORM_SECTIONS_WIZ_CODAWIZARD_PANEL_TITLE', 'Página de Formulário', 'pt');
settext('$CODA_FORM_SECTIONS_WIZ_CODAWIZARD_PANEL_DESC', 'Introduz o nome para a página do formulário.<br>Selecciona a posição onde será inserida.<br><br>', 'pt');

KSystem.included('CODAFormsLocale_pt');
