settext('$SOCIAL_INTEGRATION_SHELL_TITLE', 'Integração com Redes Sociais', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_ADD', '<i class="rp rp-share"></i> Configurar Nova Rede Social', 'br');
settext('$CODA_SOCIAL_INTEGRATION_FORM_TITLE', '', 'br');

settext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_TITLE', 'Dados do feed de $SOCIAL_NET', 'br');
settext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_DESC', 'Indique o URL para a página de perfil de $SOCIAL_NET a mostrar no aplicativo.<br>Indique se pretende que o usuário do aplicativo se autentique no $SOCIAL_NET para visualizar o seu feed ou se pretende utilizar apenas uma conta de $SOCIAL_NET para acessar o seu feed.', 'br');
settext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID', 'URL para a página de perfil de $SOCIAL_NET', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE', 'Como te pretendes ligar o seu feed de $SOCIAL_NET, através', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_END_USER', 'da autenticação efetivada por cada usuário', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PROXY_USER', 'de uma conta única para as quais deténs as credenciais', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX', 'Será utilizada a conta de ', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER', 'Será utilizada a conta de <b>$SOCIAL_NET</b> de cada usuário', 'br');

settext('$CODA_SOCIAL_INTEGRATION_PROVIDERS_WIZ_TITLE', 'Integrar com Rede Social', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_TITLE', 'Rede Social', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_DESC', 'Indique em qual rede social pretende configurar uma integração.', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE', 'Escolha a rede social', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_INSTAGRAM', '<i class="fa fa-instagram"></i> Instagram', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_TWITTER', '<i class="fa fa-twitter-square"></i> Twitter', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_FLICKR', '<i class="fa fa-flickr"></i> Flickr', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_TUMBLR', '<i class="fa fa-tumblr-square"></i> Tumblr', 'br');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_YOUTUBE', '<i class="fa fa-youtube-square"></i> Youtube', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_GOOGLE', '<i class="fa fa-google-plus-square"></i> Google+', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_FACEBOOK', '<i class="fa fa-facebook-square"></i> Facebook', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_DASHBOARD', '$SOCIAL_NET', 'br');
settext('$CODA_SOCIAL_INTEGRATION_DASHBOARD_DESC', 'Configure a forma como a seu aplicativo integra com o $SOCIAL_NET', 'br');
settext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE', 'Integração com o $SOCIAL_NET', 'br');

settext('$CODA_SOCIAL_INTEGRATION_INSTAGRAM_ICON', 'css:fa fa-instagram', 'br');
settext('$CODA_SOCIAL_INTEGRATION_FLICKR_ICON', 'css:fa fa-flickr', 'br');
settext('$CODA_SOCIAL_INTEGRATION_TWITTER_ICON', 'css:fa fa-twitter-square', 'br');
settext('$CODA_SOCIAL_INTEGRATION_TUMBLR_ICON', 'css:fa fa-tumblr-square', 'br');
settext('$CODA_SOCIAL_INTEGRATION_YOUTUBE_ICON', 'css:fa fa-youtube-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_GOOGLE_ICON', 'css:fa fa-google-plus-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FACEBOOK_ICON', 'css:fa fa-facebook-square', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE_HELP', 'Indique em qual rede social pretende configurar uma integração.', 'br');
settext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID_HELP', 'Indique o URL para a página de perfil de $SOCIAL_NET a mostrar no seu aplicativo.', 'br');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE_HELP', 'Indique se pretende que o usuário do seu aplicativo se autentique no $SOCIAL_NET para visualizar o seu feed ou se pretende utilizar apenas uma conta de $SOCIAL_NET para acessar o seu feed.', 'br');
