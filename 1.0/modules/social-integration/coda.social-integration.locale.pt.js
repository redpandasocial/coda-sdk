settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE_HELP', 'Indica qual a rede social com a qual pretendes configurar uma integração', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID_HELP', 'Indica o URL para a página de perfil de $SOCIAL_NET a mostrar na tua aplicação.', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE_HELP', 'Indica se pretendes que o utilizador da tua aplicação se autentique no $SOCIAL_NET para visualizar a tua feed ou se pretendes utilizar apenas uma conta de $SOCIAL_NET para aceder à tua feed.', 'pt');
settext('$SOCIAL_INTEGRATION_SHELL_TITLE', 'Integração com Redes Sociais', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_ADD', '<i class="rp rp-share"></i> Configurar Nova Rede Social', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FORM_TITLE', '', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_TITLE', 'Dados da feed de $SOCIAL_NET', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_DESC', 'Indica o URL para a página de perfil de $SOCIAL_NET a mostrar na aplicação.<br>Indica se pretendes que o utilizador da aplicação se autentique no $SOCIAL_NET para visualizar a tua feed ou se pretendes utilizar apenas uma conta de $SOCIAL_NET para aceder à tua feed.', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID', 'URL para a página de perfil de $SOCIAL_NET', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE', 'Como te pretendes ligar à tua feed de $SOCIAL_NET, através', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_END_USER', 'da autenticação efectivada por cada utilizador', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PROXY_USER', 'de uma conta única para as quais deténs as credenciais', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX', 'Será utilizada a conta de ', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER', 'Será utilizada a conta de <b>$SOCIAL_NET</b> de cada utilizador', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_PROVIDERS_WIZ_TITLE', 'Integrar com Rede Social', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_TITLE', 'Rede Social', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_DESC', 'Indica qual a rede social com a qual pretendes configurar uma integração.', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE', 'Escolhe a rede social', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_INSTAGRAM', '<i class="fa fa-instagram"></i> Instagram', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_TWITTER', '<i class="fa fa-twitter-square"></i> Twitter', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_FLICKR', '<i class="fa fa-flickr"></i> Flickr', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_TUMBLR', '<i class="fa fa-tumblr-square"></i> Tumblr', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_YOUTUBE', '<i class="fa fa-youtube-square"></i> Youtube', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_GOOGLE', '<i class="fa fa-google-plus-square"></i> Google+', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_PROVIDER_FACEBOOK', '<i class="fa fa-facebook-square"></i> Facebook', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_DASHBOARD', '$SOCIAL_NET', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_DASHBOARD_DESC', 'Configura a forma como a tua aplicação integra com o $SOCIAL_NET', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE', 'Integração com o $SOCIAL_NET', 'pt');

settext('$CODA_SOCIAL_INTEGRATION_INSTAGRAM_ICON', 'css:fa fa-instagram', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FLICKR_ICON', 'css:fa fa-flickr', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_TWITTER_ICON', 'css:fa fa-twitter-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_TUMBLR_ICON', 'css:fa fa-tumblr-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_YOUTUBE_ICON', 'css:fa fa-youtube-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_GOOGLE_ICON', 'css:fa fa-google-plus-square', 'pt');
settext('$CODA_SOCIAL_INTEGRATION_FACEBOOK_ICON', 'css:fa fa-facebook-square', 'pt');

