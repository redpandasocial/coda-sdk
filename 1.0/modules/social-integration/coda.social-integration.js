KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODASocialIntegrationDashboard(parent, registered_providers) {
	if (parent != null) {
		CODAMenuDashboard.call(this, parent, {}, registered_providers);
	}
}

KSystem.include([
	'CODAMenuDashboard'
], function() {
	CODASocialIntegrationDashboard.prototype = new CODAMenuDashboard();
	CODASocialIntegrationDashboard.prototype.constructor = CODASocialIntegrationDashboard;

	CODASocialIntegrationDashboard.prototype.getIconFor = function(data) {
		return new CODAIcon(this, {
			icon : gettext('$CODA_SOCIAL_INTEGRATION_' + data.type.toUpperCase() + '_ICON'),
			activate : false,
			title : gettext('$CODA_SOCIAL_INTEGRATION_DASHBOARD').replace(/\$SOCIAL_NET/g, data.type.toUpperCase()),
			description : '<span>' + gettext('$CODA_SOCIAL_INTEGRATION_DASHBOARD_DESC').replace(/\$SOCIAL_NET/g, data.type.toUpperCase()) + '</span>' ,
			actions : [{
				icon : 'css:fa fa-chevron-right',
				desc : 'Listar',
				link : '#/edit/' + data.type
			}]
		});
	};

	KSystem.included('CODASocialIntegrationDashboard');
}, Kinky.CODA_INCLUDER_URL);


function CODASocialIntegrationFacebook(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'INSTAGRAM'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationFacebook.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationFacebook.prototype.constructor = CODASocialIntegrationFacebook;

	CODASocialIntegrationFacebook.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Facebook', 'https://graph.facebook.com');
		this.addPanel(panel, 'Facebook', true);
	};

	CODASocialIntegrationFacebook.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Facebook', 'https://graph.facebook.com');
	};

	CODASocialIntegrationFacebook.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'facebook');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/facebook-feed' + CODA_VERSION + '/accounts/facebook/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(docroot), "", "width=820,height=450,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/facebook-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/facebook');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/facebook-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Facebook') + '</span>';
		}
	};

	KSystem.included('CODASocialIntegrationFacebook');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationFlickr(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'FLICKR'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationFlickr.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationFlickr.prototype.constructor = CODASocialIntegrationFlickr;

	CODASocialIntegrationFlickr.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Flickr', 'http://api.flickr.com/services/rest');
		this.addPanel(panel, 'Flickr', true);
	};

	CODASocialIntegrationFlickr.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Flickr', 'http://api.flickr.com/services/rest');
	};

	CODASocialIntegrationFlickr.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'flickr');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/flickr-feed' + CODA_VERSION + '/accounts/flickr/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var reredir = restroot + '?redirect_uri=' + encodeURIComponent(docroot);
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(reredir), "", "width=980,height=680,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/flickr-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/flickr');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/flickr-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Flickr') + '</span>';
		}
	};


	KSystem.included('CODASocialIntegrationFlickr');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationForm(parent){
	if (parent != null) {
		CODAForm.call(this, parent);
		this.nillable = true;
		this.single = true;
		this.rest_prefix + 'oauth';
	}

}

KSystem.include([
	'CODAForm'
], function() {
	CODASocialIntegrationForm.prototype = new CODAForm();
	CODASocialIntegrationForm.prototype.constructor = CODASocialIntegrationForm;

	CODASocialIntegrationForm.prototype.setData = function(data) {
		this.target.data = this.data = this.getShell().app_data;
		this.target.href = this.data._id;
	};

	CODASocialIntegrationForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODASocialIntegrationForm.prototype.setDefaults = function(params) {
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;

		delete params.channels;
		delete params.owner;
	};

	CODASocialIntegrationForm.addWizardPanel = function(parent, data, type, api_rest_url_address) {
		var typelc = type.toLowerCase();

		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			console.log('gettext');
			firstPanel.title = gettext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_TITLE').replace(/\$SOCIAL_NET/g, type);
			firstPanel.description = gettext('$CODA_SOCIAL_INTEGRATION_WIZ_GENERIC_PANEL_DESC').replace(/\$SOCIAL_NET/g, type);
			firstPanel.icon = gettext('$CODA_SOCIAL_INTEGRATION_' + type.toUpperCase() + '_ICON');

			var follow_id = new KInput(firstPanel, gettext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID').replace(/\$SOCIAL_NET/g, type) + '*', 'connected_networks.' + typelc + '.follow_id');
			{
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].follow_id) {
					follow_id.setValue(data.connected_networks[typelc].follow_id);
				}
				follow_id.setHelp(gettext('$CODA_SOCIAL_INTEGRATION_FOLLOW_ID_HELP').replace(/\$SOCIAL_NET/g, type), CODAForm.getHelpOptions());
				follow_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				follow_id.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
			}
			firstPanel.appendChild(follow_id);

			var credentials_type = new KRadioButton(firstPanel, gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE').replace(/\$SOCIAL_NET/g, type) + '*', 'connected_networks.' + typelc + '.credentials_type');
			{
				credentials_type.addOption('end-user', gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_END_USER').replace(/\$SOCIAL_NET/g, type), false);
				credentials_type.addOption('proxy-user', gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PROXY_USER').replace(/\$SOCIAL_NET/g, type), false);
				credentials_type.setHelp(gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_TYPE_HELP').replace(/\$SOCIAL_NET/g, type), CODAForm.getHelpOptions());
				credentials_type.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				eval('credentials_type.addEventListener(\'change\', CODASocialIntegration' + type + '.change)');

				credentials_type.refresh = function(args) {
					var host = window.document.location.host.split('.');
					var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/' + typelc + '-feed' + CODA_VERSION;

					this.parent.childWidget('/api_url').setValue(restroot + '/' + typelc);
					this.parent.childWidget('/access_token').setValue(args.access_token);
					this.parent.childWidget('/account_id').setValue(args.user_id);
					this.parent.childWidget('/expires_in').setValue(args.expires_in);
					this.kinky.get(this, restroot + args.user_id, { access_token : args.access_token }, { 'Authorization' : 'OAuth2.0 ' + args.access_token }, 'onUser');
				};
				credentials_type.refreshInfo = function(args) {
					this.kinky.get(this, 'rest:' + args.account_id, {  }, { 'Authorization' : 'OAuth2.0 ' + args.access_token }, 'onUser');
				};
				credentials_type.onUser = function(data, request) {
					Kinky.SHELL_CONFIG[this.shell].headers.Authorization = 'OAuth2.0 ' + decodeURIComponent(Kinky.SHELL_CONFIG[this.shell].module_token);
					this.parent.enable();
					this.parent.childWidget('/' + typelc + '-info').content.innerHTML = '<img src="' + data.mug + '"></img><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX').replace(/\$SOCIAL_NET/g, type) + '<b>' + data.name + '</b></span>';
				}; 

				credentials_type.visible = function() {
					if (this.display) {
						return;
					}
					KRadioButton.prototype.visible.call(this);
					if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].credentials_type) {
						this.setValue(data.connected_networks[typelc].credentials_type);
					}
				}
			}
			firstPanel.appendChild(credentials_type);

			var user_info = new KPanel(firstPanel);
			{
				user_info.hash = '/' + typelc + '-info';
				user_info.addCSSClass('CODASocialIntegrationUserInfo');
			}
			firstPanel.appendChild(user_info);

			var api_url = new KHidden(firstPanel, 'connected_networks.' + typelc + '.api_url');
			{
				api_url.hash = '/api_url';
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].api_url) {
					api_url.setValue(data.connected_networks[typelc].api_url);
				}
				else {
					var host = window.document.location.host.split('.');
					var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/' + typelc + '-feed' + CODA_VERSION;
					api_url.setValue(restroot + '/' + typelc);
				}
			}
			firstPanel.appendChild(api_url);
			var access_token = new KHidden(firstPanel, 'connected_networks.' + typelc + '.access_token');
			{
				access_token.hash = '/access_token';
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].access_token) {
					access_token.setValue(data.connected_networks[typelc].access_token);
				}
			}
			firstPanel.appendChild(access_token);
			var account_id = new KHidden(firstPanel, 'connected_networks.' + typelc + '.account_id');
			{
				account_id.hash = '/account_id';
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].account_id) {
					account_id.setValue(data.connected_networks[typelc].account_id);
				}
			}
			firstPanel.appendChild(account_id);
			var expires_in = new KHidden(firstPanel, 'connected_networks.' + typelc + '.expires_in');
			{
				expires_in.hash = '/expires_in';
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].expires_in) {
					expires_in.setValue(data.connected_networks[typelc].expires_in);
				}
			}
			firstPanel.appendChild(expires_in);
			var typeinput = new KHidden(firstPanel, 'connected_networks.' + typelc + '.type');
			{
				typeinput.setValue('' + typelc + '');
				if (!!data && !!data.connected_networks && !!data.connected_networks[typelc] && !!data.connected_networks[typelc].type) {
					typeinput.setValue(data.connected_networks[typelc].type);
				}
			}
			firstPanel.appendChild(typeinput);

		}

		return firstPanel;
	};

	CODASocialIntegrationForm.prototype.onNoContent = function(data, request) {
		CODAForm.prototype.load.call(this);
	};

	CODASocialIntegrationForm.prototype.onPost = CODASocialIntegrationForm.prototype.onCreated = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODASocialIntegrationForm.prototype.onPut = function(data, request){
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODASocialIntegrationForm.prototype.save = function(toSend) {
		this.disable(true);

		if (!!this.target.http && !!this.target.http.headers && !!this.target.http.headers['Content-Language']) {
			this.target.http.headers['Accept-Language'] = this.target.http.headers['Content-Language'];
		}

		var params = (!!toSend ? toSend : (!!this.target.data ? this.target.data : {}));
		var doSubmit = true;

		for ( var c in this.childWidgets()) {
			var panel = this.childWidget(c);
			if (panel instanceof KPanel) {
				if (!!panel.getRequest) {
					var value = panel.getRequest();
					if (!!value) {
						CODAForm.createObject(params, panel.fieldID.split('.'));
						eval('params.' + panel.fieldID + ' = value;');
					}
				}
				else {
					for ( var i in panel.childWidgets()) {
						var input = panel.childWidget(i);

						if ((input instanceof KAbstractInput || input instanceof KHidden) && !(input instanceof KButton)) {
							var value = input.getValue();
							var validation = input.validate();
							var objpath = input.getInputID();
							CODAForm.createObject(params, objpath.split('.'));
							var oldvalue = null;
							eval('oldvalue = params.' + objpath + ' ;');

							if (validation != true) {
								if (doSubmit) {
									input.focus();
								}
								input.errorArea.innerHTML = validation;
								doSubmit = false;
							}
							else if (!!value && value != '') {
								eval('params.' + objpath + ' = value');
							}
							else if(!!oldvalue && oldvalue != '') {
								eval('params.' + objpath + ' = null');
							}

							if (input.getMetadata) {
								var meta = input.getMetadata();
								if (!!meta) {
									eval('params.' + objpath + '_metadata = meta');
								}
							}
						}
						else if (!!input.parent.save) {
							var value = input.parent.save();

							for ( var c in value) {
								var oldvalue = null;
								eval('oldvalue = params.' + c + ' ;');

								if ((!!value[c] && value[c] != '')) {
									CODAForm.createObject(params, c.split('.'));
									eval('params.' + c + ' = value[c]');
								}
								else if((!!oldvalue && oldvalue != '')) {
									eval('params.' + c + ' = null');
								}
							}
						}
					}
				}
			}
		}
		delete params.http;
		this.instantiateDefaults(params);

		try {
			if (!!params && doSubmit) {
				for (var p in params.connected_networks) {
					var type_data = params.connected_networks[p];
					for (var f in type_data) {
						if (!type_data[f] || type_data[f] == '') {
							delete type_data[f];
						}
					}
				}

				var headers = { 'E-Tag' : '' + (new Date()).getTime() };
				headers = KSystem.merge(headers, this.config.headers || {});
				headers = KSystem.merge(headers, this.headers);2
				headers['X-Fields-Nillable'] = '' + this.nillable;
				headers['Authorization'] = 'OAuth2.0 ' + decodeURIComponent(Kinky.getDefaultShell().data.module_token);
				delete params.channels;

				if (!!this.target.href) {
					this.kinky.put(this, 'oauth:' + this.target.href, params, headers);
				}
				else {
					this.kinky.post(this, 'oauth:' + this.target.collection, params, headers);
				}
			}
			else if(!doSubmit){
				this.enable();
				CODAForm.hideMessage(this.id);
			}
		}
		catch (e) {

		}
	};

	CODASocialIntegrationForm.getProviderData = function(input, type) {
		var form = input.getParent('CODAForm')
		if (!form) {
			return undefined;
		}
		else {
			return form.data.connected_networks[type];
		}
	};

	KSystem.included('CODASocialIntegrationForm');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationGoogle(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'INSTAGRAM'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationGoogle.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationGoogle.prototype.constructor = CODASocialIntegrationGoogle;

	CODASocialIntegrationGoogle.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Google', 'https://graph.facebook.com');
		this.addPanel(panel, 'Google', true);
	};

	CODASocialIntegrationGoogle.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Google', 'https://graph.facebook.com');
	};

	CODASocialIntegrationGoogle.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'google');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/google-feed' + CODA_VERSION + '/accounts/google/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(docroot), "", "width=820,height=450,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/google-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/google');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/google-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Facebook') + '</span>';
		}
	};

	KSystem.included('CODASocialIntegrationGoogle');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationInstagram(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'INSTAGRAM'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationInstagram.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationInstagram.prototype.constructor = CODASocialIntegrationInstagram;

	CODASocialIntegrationInstagram.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Instagram', 'https://api.instagram.com/v1');
		this.addPanel(panel, 'Instagram', true);
	};

	CODASocialIntegrationInstagram.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Instagram', 'https://api.instagram.com/v1');
	};

	CODASocialIntegrationInstagram.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'instagram');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/instagram-feed' + CODA_VERSION + '/accounts/instagram/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(docroot), "", "width=820,height=450,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/instagram-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/instagram');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/instagram-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Instagram') + '</span>';
		}
	};

	KSystem.included('CODASocialIntegrationInstagram');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationProvider(){
}
{
	CODASocialIntegrationProvider.prototype = new Object();
	CODASocialIntegrationProvider.prototype.constructor = CODASocialIntegrationProvider;

	CODASocialIntegrationProvider.addWizardPanel = function(parent) {
		var firstPanel = new KPanel(parent);
		{
			firstPanel.hash = '/general';
			firstPanel.title = gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_TITLE');
			firstPanel.description = gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_WIZ_GENERIC_PANEL_DESC');
			firstPanel.icon = 'css:rp rp-share';

			var code = new KRadioButton(firstPanel, gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE') + '*', 'type');
			{
				var providers = parent.getShell().unregistered_providers;
				for (var p in providers) {
					code.addOption(providers[p], gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_' + providers[p].toUpperCase()), false);
				}

				code.setHelp(gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_CODE_HELP'), CODAForm.getHelpOptions());
				code.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			firstPanel.appendChild(code);
		}

		return [ firstPanel, new KPanel(parent) ];
	};

	KSystem.included('CODASocialIntegrationProvider');
}

function CODASocialIntegrationShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODASocialIntegrationShell.actions);
		this.available_providers = {		
			"instagram" : {
				type : 'oauth2',
				template_id : 'CODASocialIntegrationInstagram',
				icon : 'fa fa-instagram'
			},
			"twitter" : {
				type : 'oauth1',
				template_id : 'CODASocialIntegrationTwitter',
				icon : 'fa fa-twitter-square'
			},
			"flickr" : {
				type : 'oauth1',
				template_id : 'CODASocialIntegrationFlickr',
				icon : 'fa fa-flickr'
			},
			"tumblr" : {
				type : 'oauth1',
				template_id : 'CODASocialIntegrationTumblr',
				icon : 'fa fa-tumblr-square'
			},
			"youtube" : {
				type : 'oauth2',
				template_id : 'CODASocialIntegrationYoutube',
				icon : 'fa fa-youtube-square'
			},
			"google" : {
				type : 'oauth2',
				template_id : 'CODASocialIntegrationGoogle',
				icon : 'fa fa-google-plus-square'
			},
			"facebook" : {
				type : 'oauth2',
				template_id : 'CODASocialIntegrationFacebook',
				icon : 'fa fa-facebook-square'
			}
		};
	}
}

KSystem.include([
	'KLayout',
	'KText',
	'CODAGenericShell',
	'CODAForm',
	'CODAToolbarDialog',
	'CODAWizard',
	'CODASocialIntegrationDashboard'
], function() {
	CODASocialIntegrationShell.prototype = new CODAGenericShell();
	CODASocialIntegrationShell.prototype.constructor = CODASocialIntegrationShell;

	CODASocialIntegrationShell.prototype.loadShell = function() {
		this.registered_providers = this.app_data.connected_networks;
		this.draw();
	};

	CODASocialIntegrationShell.prototype.draw = function() {
		this.setTitle(gettext('$SOCIAL_INTEGRATION_SHELL_TITLE'));

		var addbt = new KButton(this, gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_ADD'), 'add-provider', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/add'
			});
		});
		this.addToolbarButton(addbt, 3);

		this.dashboard = new CODASocialIntegrationDashboard(this, this.registered_providers);
		this.appendChild(this.dashboard);

		CODAGenericShell.prototype.draw.call(this);
		this.addCSSClass('CODAModulesShellContent', this.content);
	};

	CODASocialIntegrationShell.sendRequest = function() {
	};

	CODASocialIntegrationShell.prototype.editProvider = function(href) {
		var providerClass = this.available_providers[href].template_id;
		var configForm = null;
		eval('configForm = new ' + providerClass + '(this)');
		{
			configForm.hash = '/social/provider/' + href;
		}
		this.makeDialog(configForm, { minimized : false });
	};

	CODASocialIntegrationShell.prototype.addProvider = function() {
		this.unregistered_providers = [];
		for (var p in this.available_providers) {
			var already = false;
			for (var r in this.registered_providers) {
				if (p == r) {
					already = true;
					break;
				}
			}
			if (already) {
				continue;
			}
			this.unregistered_providers.push(p);
		}

		if (this.unregistered_providers.length == 0) {
			KMessageDialog.alert({
				message : gettext('$CODA_SOCIAL_INTEGRATION_PROVIDER_ALL_REGISTERED'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300
			});
		}
		else {			
			var screens = [ 'CODASocialIntegrationProvider'];
			var wiz = new CODAWizard(this, screens);
			wiz.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_PROVIDERS_WIZ_TITLE'));
			wiz.hash = '/applications/' + this.data.app_id + '/socialproviders';

			wiz.onPut = wiz.onPost = wiz.onCreated = function(data, request) {
				this.parent.closeMe();
				window.document.location.reload(true);
				return
			};

			wiz.onNext = function() {
				switch(this.current) {
					case 0: {
						this.removePanel(1);
						this.addPanel(this.getShell().available_providers[this.values[0].type].template_id);
						break;
					}
				}
			};

			wiz.send = function(params) {
				var app_data = this.getShell().app_data;
				var type_data = this.values[1].connected_networks[this.values[0].type];

				for (var f in type_data) {
					if (!type_data[f] || type_data[f] == '') {
						delete type_data[f];
					}
				}

				app_data.connected_networks[this.values[0].type] = type_data;
				delete app_data.links;
				delete app_data.requestTypes;
				delete app_data.responseTypes;
				delete app_data.rel;
				delete app_data.restServer;

				delete app_data.channels;
				delete app_data.owner;
				try {
					this.kinky.put(this, 'oauth:/applications/' + app_data.id, app_data, Kinky.SHELL_CONFIG[this.shell].headers);
				}
				catch (e) {
				}
			};

			this.makeWizard(wiz);
		}
	};

	CODASocialIntegrationShell.prototype.onError = function(data, request) {
		KMessageDialog.alert({
			message : gettext('$CAMPAIGNS_MODULE_ERROR_' + data.error_code),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODASocialIntegrationShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'edit': {
				Kinky.getDefaultShell().editProvider(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add': {
				Kinky.getDefaultShell().addProvider();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	KSystem.included('CODASocialIntegrationShell');
}, Kinky.CODA_INCLUDER_URL);
function CODASocialIntegrationTumblr(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'TUMBLR'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationTumblr.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationTumblr.prototype.constructor = CODASocialIntegrationTumblr;

	CODASocialIntegrationTumblr.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Tumblr', 'https://api.tumblr.com/v2');
		this.addPanel(panel, 'Tumblr', true);
	};

	CODASocialIntegrationTumblr.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Tumblr', 'https://api.tumblr.com/v2');
	};

	CODASocialIntegrationTumblr.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'tumblr');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/tumblr-feed' + CODA_VERSION + '/accounts/tumblr/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var reredir = restroot + '?redirect_uri=' + encodeURIComponent(docroot);
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(reredir), "", "width=820,height=640,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/tumblt-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/tumblr');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/tumblr-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Tumblr') + '</span>';
		}
	};


	KSystem.included('CODASocialIntegrationTumblr');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationTwitter(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'TWITTER'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationTwitter.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationTwitter.prototype.constructor = CODASocialIntegrationTwitter;

	CODASocialIntegrationTwitter.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Twitter', 'https://api.twitter.com/1.1');
		this.addPanel(panel, 'Twitter', true);
	};

	CODASocialIntegrationTwitter.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Twitter', 'https://api.twitter.com/1.1');
	};

	CODASocialIntegrationTwitter.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'twitter');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/twitter-feed' + CODA_VERSION + '/accounts/twitter/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var reredir = restroot + '?redirect_uri=' + encodeURIComponent(docroot);
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(reredir), "", "width=820,height=450,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/twitter-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/twitter');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/twitter-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Twitter') + '</span>';
		}
	};


	KSystem.included('CODASocialIntegrationTwitter');
}, Kinky.CODA_INCLUDER_URL);

function CODASocialIntegrationYoutube(parent){
	if (parent != null) {
		CODASocialIntegrationForm.call(this, parent);
		this.setTitle(gettext('$CODA_SOCIAL_INTEGRATION_EDIT_TITLE').replace(/\$SOCIAL_NET/g, 'YOUTUBE'));
	}
}

KSystem.include([
	'CODASocialIntegrationForm'
], function() {
	CODASocialIntegrationYoutube.prototype = new CODASocialIntegrationForm();
	CODASocialIntegrationYoutube.prototype.constructor = CODASocialIntegrationYoutube;

	CODASocialIntegrationYoutube.prototype.addMinimizedPanel = function() {
		var panel = CODASocialIntegrationForm.addWizardPanel(this, this.data, 'Youtube', 'https://www.googleapis.com/youtube/v3');
		this.addPanel(panel, 'Youtube', true);
	};

	CODASocialIntegrationYoutube.addWizardPanel = function(parent, data) {
		return CODASocialIntegrationForm.addWizardPanel(parent, data, 'Youtube', 'https://www.googleapis.com/youtube/v3');
	};

	CODASocialIntegrationYoutube.change = function(event) {
		var credentials_type = KDOM.getEventWidget(event);
		var data = CODASocialIntegrationForm.getProviderData(credentials_type, 'youtube');

		if (credentials_type.getValue() == 'proxy-user') {
			if (!!data && data.credentials_type == 'proxy-user') {
				credentials_type.refreshInfo(data);
			}
			else {
				var docroot = window.document.location.protocol + '//' + window.document.location.host + CODA_VERSION + '/modules/social-integration/connected.html';
				var restroot =  window.document.location.protocol + '//' + window.document.location.host + '/api/youtube-feed' + CODA_VERSION + '/accounts/google/connect';
				credentials_type.parent.disable();
				window.CODA_WAITING_AUTH = credentials_type;
				var pup = window.open(restroot + '?display=popup' + (!!credentials_type.getShell().data.app_id ? '&client_id=' + credentials_type.getShell().data.app_id : '') + '&redirect_uri=' + encodeURIComponent(docroot), "", "width=820,height=570,status=1,location=1,resizable=no,left=200,top=200");
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		else {
			if (!!data) {
				data.credentials_type = 'end-user';
			}
			var host = window.document.location.host.split('.');
			var restroot =  window.document.location.protocol + '//' + (host.length > 2 ? 'api' + '.' + host[1] + '.' + host[2] : host[0] + '.' + host[1]) + '/youtube-feed' + CODA_VERSION;
			credentials_type.parent.childWidget('/api_url').setValue(restroot + '/youtube');
			credentials_type.parent.childWidget('/access_token').setValue(null);
			credentials_type.parent.childWidget('/account_id').setValue(null);
			credentials_type.parent.childWidget('/expires_in').setValue(null);
			credentials_type.parent.childWidget('/youtube-info').content.innerHTML = '<i class="fa fa-user"></i><span>' + gettext('$CODA_SOCIAL_INTEGRATION_CREDENTIALS_PREFIX_END_USER').replace(/\$SOCIAL_NET/g, 'Youtube') + '</span>';
		}
	};

	KSystem.included('CODASocialIntegrationYoutube');
}, Kinky.CODA_INCLUDER_URL);

KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

