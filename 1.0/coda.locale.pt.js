settext('$CONFIRM_OPERATION_ERROR', '"<img src=\\"/imgs/error.png\\"></img> Uppps, " + gettext("$ERROR_NUMBER_[NUMBER]").replace(\'$FIELDS\', geterrorfields(data))', 'pt');
settext('$POPUP_BLOCKER', 'O bloqueador de novas janelas do seu browser está a impedir que prossiga com a navegação.<br>Por favor, adicione este domínio às excepções.', 'pt');

///////////////////////////////////////////////////////////////////
// BACKEND ERROR MESSAGES
settext('$ERROR_NUMBER_undefined', 'a ligação com o servidor perdeu-se.<br>Recarrega a página e tenta outra vez.', 'pt');
settext('$ERROR_NUMBER_1073', 'o URL que indicaste não é válido.', 'pt');
settext('$ERROR_NUMBER_932', 'o método HTTP utilizado não é compatível com este serviço.', 'pt');
settext('$ERROR_NUMBER_926', 'os seguintes campos não têm valores válidos: $FIELDS', 'pt');
settext('$ERROR_NUMBER_926_FIELD_TYPE', 'tipo', 'pt');
settext('$ERROR_NUMBER_926_FIELD_FIELDS', 'valor', 'pt');
settext('$ERROR_NUMBER_927', 'os seguintes campos não têm valores válidos: $FIELDS', 'pt');
settext('$ERROR_NUMBER_1025', 'a página do formulário já tem um campo com esse código.', 'pt');
settext('$ERROR_NUMBER_1527', 'a página em questão já utilizou o período de gratuitidade. Escolhe outro plano que se adeque às tuas necessidades.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// VALIDATORS ERROR MESSAGES
settext('$CODA_EXCEPTION_FIELDS_MISSING', 'Faltam preencher campos obrigat\u00f3rios', 'pt');
settext('$ERROR_MUST_NOT_BE_NULL', 'campo obrigat\u00f3rio', 'pt');
settext('$ERROR_MUST_BE_BETWEEN_0_1', 'valor do campo deverá ser um número entre 0 e 1', 'pt');
settext('$ERROR_MUST_BE_INTEGER', 'valor do campo deverá ser um número inteiro', 'pt');
settext('$ERROR_MUST_BE_DOUBLE', 'valor do campo deverá ser um número decimal', 'pt');
settext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE', 'valor do campo deverá ser um número (inteiro ou decimal)', 'pt');
settext('$ERROR_DATE_MUST_BE_GREATER_THAN_START_DATE', 'data de fim tem que ser superior à data de início.', 'pt');
settext('$ERROR_MUST_BE_URL', 'o valor do campo tem de ser um URL válido (e.g. http://google.com)', 'pt');
settext('$ERROR_MUST_BE_EMAIL', 'o valor do campo tem que ser um email válido', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LOGIn
settext('$CODA_LOGIN_USERNAME_LABEL', 'ENDEREÇO DE E-MAIL', 'pt');
settext('$CODA_LOGIN_PASSWORD_LABEL', 'PALAVRA-CHAVE', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// USER WELCOME                                      
settext('$CODA_USER_WELCOME_PHRASE', 'Bem-Vindo, ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FILE MANAGEMENT
settext('$KFILE_BUTTON_UPLOAD_TEXT', '<i class="fa fa-upload"></i><span>&nbsp;&nbsp;UPLOAD</span>', 'pt');
settext('$KFILE_BUTTON_LABEL_ADD_TEXT', 'ADICIONAR:', 'pt');
settext('$KFILE_BUTTON_LABEL_CHANGE_TEXT', 'ALTERAR:', 'pt');
settext('$KFILE_NO_IMAGE_THUMB', 'CODAFileUploadNoImage', 'pt');

settext('$KIMAGEUPLOAD_BUTTON_UPLOAD_TEXT', '<i class="fa fa-upload"></i><span>&nbsp;&nbsp;UPLOAD</span>', 'pt');
settext('$KIMAGEUPLOAD_NO_IMAGE_THUMB', 'CODAImageUploadNoImage', 'pt');

settext('$KMEDIALIST_TITLE', 'Servidores Media', 'pt');
settext('$KMEDIALIST_BACK', '\u2190', 'pt');
settext('$KMEDIALIST_ROOT', 'Root', 'pt');
settext('$KMEDIALIST_OPEN', '<i class="fa fa-cloud"></i><span>&nbsp;&nbsp;PROCURAR...</span>', 'pt');
settext('$KMEDIALIST_NO_SERVER', 'Escolhe um servidor', 'pt');

settext('$KMEDIALIST_CREATEFOLDER', 'Criar', 'pt');
settext('$KMEDIALIST_NEWFOLDER', 'Nova Pasta', 'pt');
settext('$KMEDIALIST_NEWFILE', 'Adicionar Ficheiro', 'pt');

settext('$KMEDIALIST_SELECTEDFILES_OPTIONS', 'Op\u00e7\u00f5es de Ficheiro', 'pt');
settext('$KMEDIALIST_REMOVE', 'Remover', 'pt');
settext('$KMEDIALIST_EXTERNAL', 'Link Externo', 'pt');
settext('$KMEDIALIST_EXTERNAL_TEXT', 'Copia o link que se segue:', 'pt');

settext('$KMEDIALIST_RENAME', 'Renomear Ficheiro', 'pt');
settext('$KMEDIALIST_RENAME_BT', 'OK', 'pt');

settext('$CODA_CLOUD_BROWSER', '<i class="fa fa-cloud-upload"></i> Browser...', 'pt');

settext('$CODA_IMAGE_CROP', '<i class="fa fa-resize-full"></i> Recortar...', 'pt');

settext('$CODA_USERS_INFO_APPSTORAGE_LINK', '<i class="fa fa-hdd-o"></i> Internal Storage ', 'pt');
settext('$CODA_USERS_INFO_SOCIALNETWORK_SEPARATOR', 'Social Media', 'pt');
settext('$CODA_USERS_INFO_CLOUDSTORAGE_SEPARATOR', 'Cloud Storage', 'pt');

settext('$CODA_USERS_INFO_REFRESH_BUTTON', '<i class="fa fa-refresh"></i> REFRESCAR DADOS', 'pt');

settext('$CODA_USERS_INFO_NOT_CONNECTED_DESC', 'Conecta-te para usufruires das seguintes funcionalidades:<br><ul><li>Acesso directo às tuas imagens e videos&nbsp;&nbsp;&bull;</li><li>Acesso à tua lista de contactos&nbsp;&nbsp;&bull;</li><li>Partilha fácil das tuas aplicações&nbsp;&nbsp;&bull;</li><li>Acesso a ficheiros armazenados na cloud&nbsp;&nbsp;&bull;</li><li>Configuração de notificações&nbsp;&nbsp;&bull;</li></ul>', 'pt')
settext('$CODA_USERS_INFO_CONNECTED_DESC', 'Agora já podes usufruir de:<ul><li>Acesso directo às tuas imagens e videos&nbsp;&nbsp;&bull;</li><li>Acesso à tua lista de contactos&nbsp;&nbsp;&bull;</li><li>Partilha fácil das tuas aplicações&nbsp;&nbsp;&bull;</li><li>Acesso a ficheiros armazenados na cloud&nbsp;&nbsp;&bull;</li><li>Configuração de notificações&nbsp;&nbsp;&bull;</li></ul>', 'pt')

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES
settext('$CODA_WORKING', '<i class="fa fa-exchange"></i> Em comunica\u00e7\u00e3o, por favor, aguarde...', 'pt');
settext('$CONFIRM_SAVED_SUCCESS', '<img src="/imgs/success.png"></img>Já está,<br>as alterações foram guardadas ;)', 'pt');
settext('$CONFIRM_REMOVED_SUCCESS', '<img src="/imgs/success.png"></img>Já está,<br>as o registo foi removido ;)', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FORMS & DIALOGS

// TITLES & LABELS
settext('$FORM_GENERAL_DATA_PANEL', '<i class="fa fa-adjust"></i> Defini\u00e7\u00f5es Gerais', 'pt');
settext('$RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'pt');
settext('$CODA_FORM_HELP_TIP', 'Clica para acederes à documentação sobre este ecrã.', 'pt');

settext('$KRESOURCE_PERMISSIONS_LABEL', 'Podes gerir aqui as permiss\u00f5es', 'pt');

settext('$LINK_HREF', 'HREF', 'pt');
settext('$LINK_REL', 'URI Relacional', 'pt');
settext('$LINK_RESPONSETYPES', 'Formato de Resposta', 'pt');
settext('$LINK_REQUESTTYPES', 'Formato de Pedido', 'pt');
//----------

// BUTTONS
settext('$REMOVE', 'Remover', 'pt');
settext('$CODA_SHELL_ADD_PREFIX', 'ADICIONAR ', 'pt');
settext('$NEXT', 'CONTINUAR', 'pt');
settext('$PREVIOUS', 'ANTERIOR', 'pt');
settext('$CONFIRM', 'CONFIRMAR', 'pt');
settext('$CANCEL', 'FECHAR', 'pt');
settext('$MORE_OPTIONS', '<i class="fa fa-wrench"></i> MAIS OP\u00c7\u00d5ES...', 'pt');
settext('$KCONFIRMDIALOG_OK_BUTTON_LABEL', 'CONFIRMAR', 'pt');
settext('$KCONFIRMDIALOG_CANCEL_BUTTON_LABEL', 'CANCELAR', 'pt');
settext('$KMESSAGEDIALOG_OK_BUTTON_LABEL', 'OK', 'pt');
settext('$KUPLOADBUTTON_UPLOADING_TEXT', 'enviando ficheiro...', 'pt');

//----------

// LISTS
settext('$KLIST_ADD_ACTION', 'NOVO ELEMENTO', 'pt');

settext('$KPAGEDLIST_PREV_BUTTON', '<i class="fa fa-angle-left"></i>', 'pt');
settext('$KPAGEDLIST_NEXT_BUTTON', '<i class="fa fa-angle-right"></i>', 'pt');
settext('$KPAGEDLIST_FIRST_BUTTON', '<i class="fa fa-angle-double-left"></i>', 'pt');
settext('$KPAGEDLIST_LAST_BUTTON', '<i class="fa fa-angle-double-right"></i>', 'pt');

settext('$KLISTITEM_EDIT_ACTION', 'Editar', 'pt');
settext('$KLISTITEM_VIEW_ACTION', '<i class="fa fa-eye"></i> Ver detalhes...', 'pt');
settext('$KLISTITEM_DELETE_ACTION', 'Remover', 'pt');

settext('$CODA_LIST_HEADER_TOTAL_LABEL', 'Nº de registos', 'pt');
settext('$CODA_LIST_HEADER_TOTALPAGES_LABEL', 'Nº de páginas', 'pt');
settext('$CODA_LIST_FILTER', 'filtrar por texto...', 'pt');
settext('$CODA_LIST_NO_ITEMS', 'N\u00e3o existem elementos para mostrar nesta vista', 'pt');
settext('$CODA_LIST_DROP_PANEL_TITLE', 'Arrasta elementos da lista para aqui, para executar operações em bulk e de re-ordenação.', 'pt')
settext('$CODA_LIST_ADD_BUTTON', '<i class="fa fa-plus"></i> NOVO', 'pt');
settext('$CODA_LIST_EXPORT_BUTTON', '<i class="fa fa-share-square-o"></i> EXPORTAR', 'pt');
settext('$CODA_LIST_FILTER_TIP', 'Filtar listagem', 'pt');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_MODULES_DOCROOTS_TITLE', '<i class="fa fa-globe"></i> Websites', 'pt');
settext('$DASHBOARD_MODULES_MODULES_TITLE', '<i class="fa fa-suitcase"></i> Módulos de Gestão', 'pt');
settext('$DASHBOARD_NOTIFICATIONS', '<i class="fa fa-bell"></i> Notificações', 'pt');
settext('$DASHBOARD_CALENDAR', '<i class="fa fa-calendar"></i> Eventos', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
settext('$CODA_HEADER_MENU_ACTVATOR', '<i class="fa fa-bars"></i>', 'pt');
settext('$CODA_SHELL_LOGOUT_BT', '<i class="fa fa-sign-out"></i>Sair', 'pt');
settext('$CODA_SHELL_USER_BT', '<i class="fa fa-user"></i>Perfil', 'pt');
settext('$CODA_SHELL_APPS_BT', '<i class="fa fa-puzzle-piece"></i>Aplicações', 'pt');
settext('$CODA_SHELL_BACK_BT', '<i class="fa fa-chevron-left"></i>', 'pt');
settext('$CODA_SHELL_NOTIFICATIONS_BT', '<i class="fa fa-bell"></i>Notificações', 'pt');

settext('$CODA_TOOLBAR_FILTER_BUTTON', 'FILTRAR...', 'pt');
settext('$CODA_TOOLBAR_FILTERING_TITLE', 'Filtros', 'pt');
settext('$CODA_NOTIFICATIONS_TITLE', 'Notificações', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CLOUD BROWSER
settext('$CODA_CLOUD_OPENFOLDER_ACTION', 'Abrir', 'pt');
settext('$CODA_CLOUD_USE_ACTION', 'Escolher', 'pt');
settext('$CODA_CLOUD_DOWNLOAD_ACTION', 'Descarregar', 'pt');
settext('$CODA_CLOUD_UPONLEVEL_ACTION', '..', 'pt');
settext('$CODA_CLOUD_UPLOAD_ACTION', 'Enviar ficheiro...', 'pt');
settext('$CODA_CLOUD_REMOVE_ACTION', 'Remover ficheiro', 'pt');
settext('$CODA_CLOUD_BROWSER_CREATE_SHARED', 'Para poderes utilizar o ficheiro tem que ser criado um Link Partilhado. Desejas continuar?', 'pt');
settext('$CODA_CLOUD_BROWSER_MIME_NOTACCEPTED', 'Este tipo de ficheiro não é permitido. Deves escolher um ficheiro do tipo "$MIMETYPE"', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FACEBOOK
settext('$CODA_USERS_NO_FACEBOOK', 'Ainda não estás conectado ao Facebook.', 'pt')
settext('$CODA_USERS_NO_FACEBOOK_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_FACEBOOK', 'Já estás conectado ao Facebook.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_FACEBOOK_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_FACEBOOK_LINK_BUTTON', '<i class="fa fa-facebook-square"></i> LIGAR-ME AO FACEBOOK <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_FACEBOOK_LINK', '<i class="fa fa-facebook-square"></i> Facebook ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TWITTER
settext('$CODA_USERS_NO_TWITTER', 'Ainda não estás conectado ao Twitter.', 'pt')
settext('$CODA_USERS_NO_TWITTER_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_TWITTER', 'Já estás conectado ao Twitter.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_TWITTER_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_TWITTER_LINK_BUTTON', '<i class="fa fa-twitter"></i> LIGAR-ME AO TWITTER <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_TWITTER_LINK', '<i class="fa fa-twitter"></i> Twitter ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// GOOGLE
settext('$CODA_USERS_NO_GOOGLE', 'Ainda não estás conectado ao Google+.', 'pt')
settext('$CODA_USERS_NO_GOOGLE_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_GOOGLE', 'Já estás conectado ao Google+.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_GOOGLE_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_GOOGLE_LINK_BUTTON', '<i class="fa fa-google-plus-square"></i> LIGAR-ME AO GOOGLE+ <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_GOOGLE_LINK', '<i class="fa fa-google-plus-square"></i> Google+ ', 'pt');
settext('$CODA_USERS_INFO_YOUTUBE_LINK', '<i class="fa fa-youtube"></i> Youtube ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// INSTAGRAM
settext('$CODA_USERS_NO_INSTAGRAM', 'Ainda não estás conectado ao Instagram.', 'pt')
settext('$CODA_USERS_NO_INSTAGRAM_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_INSTAGRAM', 'Já estás conectado ao Instagram.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_INSTAGRAM_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_INSTAGRAM_LINK_BUTTON', '<i class="fa fa-instagram"></i> LIGAR-ME AO INSTAGRAM <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_INSTAGRAM_LINK', '<i class="fa fa-instagram"></i> Instagram ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// YAHOO
settext('$CODA_USERS_NO_YAHOO', 'Ainda não estás conectado ao Yahoo.', 'pt')
settext('$CODA_USERS_NO_YAHOO_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_YAHOO', 'Já estás conectado ao Yahoo.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_YAHOO_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_YAHOO_LINK_BUTTON', '<i class="rp rp-yahoo"></i> LIGAR-ME AO YAHOO <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_YAHOO_LINK', '<i class="rp rp-yahoo"></i> Yahoo ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FLICKR
settext('$CODA_USERS_NO_FLICKR', 'Ainda não estás conectado ao Flickr.', 'pt')
settext('$CODA_USERS_NO_FLICKR_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_FLICKR', 'Já estás conectado ao Flickr.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_FLICKR_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_FLICKR_LINK_BUTTON', '<i class="fa fa-flickr"></i> LIGAR-ME AO FLICKR <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_FLICKR_LINK', '<i class="fa fa-flickr"></i> Flickr ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TUMBLR
settext('$CODA_USERS_NO_TUMBLR', 'Ainda não estás conectado ao Tumblr.', 'pt')
settext('$CODA_USERS_NO_TUMBLR_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_TUMBLR', 'Já estás conectado ao Tumblr.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_TUMBLR_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_TUMBLR_LINK_BUTTON', '<i class="fa fa-tumblr-square"></i> LIGAR-ME AO TUMBLR <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_TUMBLR_LINK', '<i class="fa fa-tumblr-square"></i> Tumblr ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LINKEDIN
settext('$CODA_USERS_NO_LINKEDIN', 'Ainda não estás conectado ao LinkedIn.', 'pt')
settext('$CODA_USERS_NO_LINKEDIN_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_LINKEDIN', 'Já estás conectado ao LinkedIn.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_LINKEDIN_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_LINKEDIN_LINK_BUTTON', '<i class="fa fa-linkedin-square"></i> LIGAR-ME AO LINKEDIN <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_LINKEDIN_LINK', '<i class="fa fa-linkedin-square"></i> LinkedIn ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DROPBOX
settext('$CODA_USERS_NO_DROPBOX', 'Ainda não estás conectado ao Dropbox.', 'pt')
settext('$CODA_USERS_NO_DROPBOX_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_DROPBOX', 'Já estás conectado ao Dropbox.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_DROPBOX_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_DROPBOX_LINK_BUTTON', '<i class="fa fa-dropbox"></i> LIGAR-ME AO DROPBOX <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_DROPBOX_LINK', '<i class="fa fa-dropbox"></i> Dropbox ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// BOX
settext('$CODA_USERS_NO_BOX', 'Ainda não estás conectado ao Box.', 'pt')
settext('$CODA_USERS_NO_BOX_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_BOX', 'Já estás conectado ao Box.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_BOX_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_BOX_LINK_BUTTON', '<i class="rp rp-box-rect"></i> LIGAR-ME AO BOX <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_BOX_LINK', '<i class="rp rp-box-rect"></i> Box ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// COPY
settext('$CODA_USERS_NO_COPY', 'Ainda não estás conectado ao Copy.', 'pt')
settext('$CODA_USERS_NO_COPY_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_COPY', 'Já estás conectado ao Copy.', 'pt')
settext('$CODA_USERS_CONNECTED_TO_COPY_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'pt')
settext('$CODA_USERS_COPY_LINK_BUTTON', '<i class="fa fa-cloud"></i> LIGAR-ME AO COPY <i class="fa fa-arrow-right"></i>', 'pt');
settext('$CODA_USERS_INFO_COPY_LINK', '<i class="fa fa-cloud"></i> Copy ', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FEEDBACK
settext('$CODA_FEEDBACK_WIZARD_TITLE', '<i class="fa fa-bug"></i> FEEDBACK', 'pt');
settext('$CODA_CONTACT_US', '<i class="fa fa-bug"></i> FEEDBACK', 'pt');
settext('$CODA_FEEDBACK_BACKOFFICE_GENERAL', 'Ferramenta de gestão (my.redpandasocial)', 'pt');
settext('$CODA_FEEDBACK_APP_GENERAL', 'Aplicação', 'pt');
settext('$CODA_FEEDBACK_GENERAL', 'Outra', 'pt');

settext('$CODA_FEEDBACK_SUCCESS_MSG', 'O teu <i>feedback</i> foi armazenado.\nEsperamos ser breves na resposta.\nObrigado.', 'pt');

settext('$CODA_FEEDBACK_WIZ_REFERSTO', 'Esta mensagem de feedback refere-se a:', 'pt');
settext('$CODA_FEEDBACK_WIZ_REFERSTO_APP', 'Esta mensagem de feedback refere-se à aplicação', 'pt');
settext('$CODA_FEEDBACK_WIZ_REFERSTO_BACKOFFICE', 'Esta mensagem de feedback refere-se à área de gestão:', 'pt');

settext('APP Promotions', 'Loja de Promoções', 'pt');
settext('APP Flash', 'Loja de Vendas Flash', 'pt');
settext('APP Store', 'Loja Social', 'pt');
settext('APP Contentoffser', 'Oferta de Conteúdos', 'pt');
settext('APP Friendsdisccount', 'DesConta com Amigos', 'pt');
settext('APP Instagram', 'Feed de Instagram', 'pt');
settext('APP Photocontest', 'Concurso de Fotografias', 'pt');
settext('APP Quizrankcontest', 'Concurso de Perguntas e Respostas', 'pt');
settext('APP Quizanswerpage', 'Quiz de Escolha Múltipla', 'pt');
settext('APP Videocontest', 'Concurso de Vídeos', 'pt');

settext('$CODA_FEEDBACK_WIZ_REFERSTO_HELP', 'Seleciona a área a que se refere a tua mensagem de feedback.', 'pt');
settext('$CODA_FEEDBACK_WIZ_KIND', 'Tipo de mensagem de feedback ', 'pt');
settext('$CODA_FEEDBACK_BUG', 'Erro', 'pt');
settext('$CODA_FEEDBACK_SUGGESTION', 'Sugestão/Melhoramento', 'pt');
settext('$CODA_FEEDBACK_PROPOSAL', 'Pedido de nova funcionalidade', 'pt');
settext('$CODA_FEEDBACK_WIZ_KIND_HELP', 'Indique o tipo de feedback:<br>- <i>Erro</i>: encontraste um erro na plataforma (ex: um link quebrado, uma página que fica em branco, etc).<br>- <i>Sugestão/Melhoramento</i>: tens uma ideia de como melhorar uma funcionalidade existente.<br>- <i>Pedido de funcionalidade</i>: sentes falta de uma funcionalidade que achas útil para a plataforma.', 'pt');
settext('$CODA_FEEDBACK_WIZ_TITLE', 'Título da mensagem', 'pt');
settext('$CODA_FEEDBACK_WIZ_TITLE_HELP', 'Indica o título da tua mensagem, uma pequena frase que resuma a tua mensagem.', 'pt');
settext('$CODA_FEEDBACK_WIZ_DESC', 'Descrição da mensagem', 'pt');
settext('$CODA_FEEDBACK_WIZ_DESC_HELP', 'Descreve o Erro/Sugestão/Pedido de funcionalidade o mais detalhadamente que conseguires, providenciando toda a informação que permita contextualizar o erro/sugestão/pedido.', 'pt');
settext('$CODA_FEEDBACK_WIZ_NAME', 'O teu nome', 'pt');
settext('$CODA_FEEDBACK_WIZ_NAME_HELP', 'Introduz o nome pelo qual desejas ser identificado nas respostas a esta mensagem.', 'pt');
settext('$CODA_FEEDBACK_WIZ_EMAIL', 'O teu endereço de e-mail', 'pt');
settext('$CODA_FEEDBACK_WIZ_EMAIL_HELP', 'Introduz o endereço de e-mail onde desejas receber as respostas a esta mensagem.', 'pt');

settext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_TITLE', 'Categorização da mensagem de Feedback', 'pt');
settext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_DESC', 'Indica a área da plataforma a que se refere a tua mensagem.<br>Indica o tipo de feedback a que a tua mensagem se refere.', 'pt');
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_PANEL_TITLE', 'Categorização da mensagem de Feedback', 'pt');
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_APP_PANEL_DESC', 'Indica a aplicação a que se refere a tua mensagem', 'pt')
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_BACKOFFICE_PANEL_DESC', 'Indica a área de gestão a que se refere a tua mensagem', 'pt')
settext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_TITLE', 'Mensagem', 'pt');
settext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_DESC', 'Descreve o teu feedback indicando o título e corpo da mensagem.', 'pt');
settext('$CODA_FEEDBACK_WIZ_USER_PANEL_TITLE', 'Dados de Contacto', 'pt');
settext('$CODA_FEEDBACK_WIZ_USER_PANEL_DESC', 'Indica o nome e endereço de e-mail que desejas utilizar nesta interação.', 'pt');

settext('$CODA_FEEDBACK_WIZ_SCREENSHOT', 'Captura de ecrã (screenshot)', 'pt');
settext('$CODA_FEEDBACK_WIZ_SCREENSHOT_HELP', 'Junta à tua mensagem de feedback uma imagem com a captura de ecrã (screenshot) que ajude a compreender a tua sugestão/problema.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MODULES DASH
settext('CODA Applications Module', 'Aplicações', 'pt');
settext('CODA MyInfo Module', 'Perfil de Utilizador', 'pt');
settext('CODA Accounts Module DESCRIPTION', 'Vê quem já participou e o detalhe de cada participação.<br> Marca quais são as participações vencedoras e atribui-lhes prémios.<br>', 'pt');
settext('CODA Accounts Module', 'Participantes e Participações', 'pt');
settext('CODA Users Module DESCRIPTION', 'Cria, apaga e altera os utilizadores da tua aplicação bem como a forma como acedem ao sistema.', 'pt');
settext('CODA Users Module', 'Utilizadores', 'pt');
settext('CODA CMS Module DESCRIPTION', 'Altera o aspecto da tua aplicação, compõe os conteúdos que irão aparecer, adiciona páginas e outros elementos gráficos e publica.', 'pt');
settext('CODA CMS Module', 'Gestão de Conteúdos', 'pt');
settext('CODA Campaigns Module DESCRIPTION', 'Define a calendarização para a tua aplicação e configura os prémios que os participantes vão receber.', 'pt');
settext('CODA Campaigns Module', 'Calendarização e Prémios', 'pt');
settext('CODA Forms Module DESCRIPTION', 'Configura formulários definindo as suas páginas e campos', 'pt');
settext('CODA Forms Module', 'Formulários', 'pt');
settext('CODA Payments Module DESCRIPTION', 'Consulta e manda executar pagamentos', 'pt');
settext('CODA Payments Module', 'Pagamentos', 'pt');
settext('CODA Products Module DESCRIPTION', 'Faz a gestão dos produtos que queres disponibilizar na tua aplicação', 'pt');
settext('CODA Products Module', 'Produtos', 'pt');
settext('CODA Vouchers Module DESCRIPTION', 'Configura os vouchers que vais atribuir.<br> Consulta os que já foram atribuídos.<br> Dá baixa dos que forem sendo utilizados.', 'pt');
settext('CODA Vouchers Module', 'Vouchers', 'pt');
settext('CODA Styling Module DESCRIPTION', 'Altera o aspecto gráfico da tua aplicação e edita os textos e títulos.', 'pt');
settext('CODA Styling Module', 'Estilos e Grafismo', 'pt');
settext('CODA Social Integration DESCRIPTION', 'Configura o formato de integração da aplicação com as várias redes sociais.', 'pt');
settext('CODA Social Integration', 'Integração com Redes Sociais', 'pt');
settext('CODA Templates & Themes DESCRIPTION', 'Cria e configura os Templates e Themes de aplicações', 'pt');
settext('CODA Templates & Themes', 'Templates & Themes', 'pt');
settext('CODA Statistics DESCRIPTION', 'Acesso às estatísticas da aplicação: utilizadores da aplicação e acesso a páginas.', 'pt');
settext('CODA Statistics', 'Estatísticas', 'pt');
settext('CODA Store Module DESCRIPTION', 'Adicione e configure as Gateways de Pagamento para os pagamentos efectuados na sua aplicação.', 'pt');
settext('CODA Store Module', 'Gateways de Pagamento', 'pt');
settext('$CODA_STYLING_CHANNEL_FACEBOOK_TITLE', '&nbsp;', 'pt');
settext('REDPANDA Quiz Module DESCRIPTION', 'Gere as perguntas e as respostas do teu quiz.<br> Extrai a lista de perguntas e respostas para veres todos os dados das que já configuraste', 'pt');
settext('REDPANDA Quiz Module', 'Perguntas e Respostas', 'pt');
settext('Cardapio Module DESCRIPTION', 'Gerir cardápios do restaurante.', 'pt');
settext('Cardapio Module', 'Cardápios', 'pt');
settext('Info Module DESCRIPTION', 'Gestão de mesas, salas, garçon e publicidade.', 'pt');
settext('Info Module', 'Restaurante', 'pt');
settext('Data Module DESCRIPTION', 'Gestão de pedidos recebidos e consulta de informação agregada sobre a operação do restaurante.', 'pt');
settext('Data Module', 'Operação', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KCalendar.localization = KCalendar.localization || {};
KCalendar.localization['weekDayName'] = new Array('Domingo', 'Segunda', 'Ter&ccedil;a', 'Quarta', 'Quinta', 'Sexta', 'S&aacute;bado');
KCalendar.localization['monthName'] = new Array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');

///////////////////////////////////////////////////////////////////
// LOCALES
settext('$CODA_LOCALE_pt_PT', 'Português (Portugal)', 'pt');
settext('$CODA_LOCALE_pt_BR', 'Português (Brasil)', 'pt');
settext('$CODA_LOCALE_en_US', 'Inglês (E.U.A.)', 'pt');
settext('$CODA_LOCALE_en_GB', 'Inglês (Reino Unido)', 'pt');
settext('$CODA_LOCALE_es_ES', 'Castelhano (Espanha)', 'pt');
settext('$CODA_LOCALE_es_MX', 'Castelhano (México)', 'pt');

// REGIONS
settext('$CODA_COUNTRY_pt_PT', 'Portugal', 'pt');
settext('$CODA_COUNTRY_pt_BR', 'Brasil', 'pt');
settext('$CODA_COUNTRY_en_US', 'E.U.A.', 'pt');
settext('$CODA_COUNTRY_en_GB', 'Reino Unido', 'pt');
settext('$CODA_COUNTRY_es_ES', 'Espanha', 'pt');
settext('$CODA_COUNTRY_es_MX', 'México', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// GENERAL
settext('$CODA_NOTIFICATIONS_MARK_ALL_READ', 'MARCAR TUDO COMO LIDO', 'pt');
settext('$CODA_NOTIFICATIONS_EMPTY', 'Sem notificações pendentes.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//////////////////////////////////////////////////////////////////
// PERMISSIONS
settext('$CODA_NOTIFICATIONS_APPLICATION_ADDED', 'Nova aplicação criada', 'pt');
settext('$CODA_NOTIFICATION_APPLICATION_ADDED_MESSAGE', 'A aplicação «$APP_NAME» foi criada com sucesso. Já podes pré-visualizar, alterar e publicar a tua nova aplicação.', 'pt');
settext('$CODA_NOTIFICATIONS_PERMS_ADMIN_ADDED', 'Permissões de administração concedidas!', 'pt');
settext('$CODA_NOTIFICATION_PERMS_ADMIN_ADDED_MESSAGE', 'Foram-te concedidas permissões de adminsitração para a aplicação «$APP_NAME»', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


