///////////////////////////////////////////////////////////////////
// SHELL
settext('$APP_SHELL_TITLE', 'aplicativos', 'br');
settext('$MODULE_SHELL_TITLE', '', 'br');
settext('$CODA_APPS_ADD', 'CRIAR APLICATIVO <i class="fa fa-magic"></i>', 'br');
settext('$CODA_APPS_ADD_TOP', '<i class="fa fa-magic"></i>CRIAR APLICATIVO', 'br');
settext('$APP_NO_ITEMS', 'Não existem aplicativos', 'br');
settext('$APP_CALL_TO_ACTION', 'Crie seu primeiro aplicativo! <i class="fa fa-level-down"></i>', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATIONS DASHBOARD
settext('$APP_PREVIEW_ACTION', '<i class="fa fa-eye"></i> VISUALIZAR', 'br');
settext('$APP_EDIT_ACTION', '<i class="fa fa-wrench"></i> DEFINIÇÕES', 'br');
settext('$APP_ADD_USER_ACTION', '<i class="fa fa-group"></i> ATRIBUIR PERMISSÔES DE ADMIN.', 'br');
settext('$APP_REMOVE_ACTION', 'REMOVER <i class="fa fa-trash-o"></i>', 'br');
settext('$APP_ACTIVATE_ACTION', '<i class="fa fa-check"></i> ATIVAR', 'br');
settext('$APP_DELETE_CONFIRM_MSG', 'Tem certeza que deseja ocultar este aplicativo? Esta ação não poderá ser revertida.', 'br');
settext('$APP_CANCEL_CONFIRM_MSG', 'Tem certeza que deseja ocultar este aplicativo? <br><br><span class="KConfirmDialogQuestionFootnoteText">Os dados do aplicativo e a reativação estarão disponíveis em "Meu Perfil > Aplicativos" durante o período de 30 dias.</span>', 'br');
settext('$APP_BLOCK_ACTION', 'BLOQUEAR <i class="fa fa-ban"></i>', 'br');
settext('$APP_CANCEL_ACTION', 'DESLIGAR <i class="fa fa-power-off"></i> ', 'br');
settext('$APP_ADDTOPAGE_ACTION', '<i class="rp rp-publish"></i> PUBLICAR', 'br');
settext('$CODA_UNIQUE_URL', 'LINK', 'pt')

settext('$CODA_ICON_STATE_FA-MAGIC GRAY', 'Aplicativo criado, mas não ativado. Para ativar, entre na área de gestão do aplicativo e clique em «Ativar Aplicativo»', 'br');
settext('$CODA_ICON_STATE_RP RP-PUBLISH GREEN', 'Aplicativo ativo. Se ainda não publicou, faça-o agora.', 'br');
settext('$CODA_ICON_STATE_FA-CHECK GREEN', 'Aplicativo ativo. Se ainda não publicou, faça-o agora.', 'br');
settext('$CODA_ICON_STATE_FA-BAN RED', 'Aplicativo bloqueado, acesso não permitido aos usuários. Para reativa-lo acesse a área de gestão do aplicativo.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION WIZARD
settext('$APP_ADD_NEW', 'NOVO APLICATIVO', 'br');
settext('$APP_OWNER_ADDED_MESSAGE', 'As permissões de administrador foram atribuídas ao usuário.', 'br');
settext('$APP_OWNER_DELETED_MESSAGE', 'Foram revogadas as permissões de administrador ao usuário', 'br');

settext('$APP_FORM_NAME', 'TÍTULO', 'br');
settext('$APP_FORM_MAX_PARTICIPATIONS', 'NÚMERO MÁXIMO DE PARTICIPAÇÕES POR USUÁRIO', 'br');
settext('$APP_FORM_FAN_GATE', 'Página de Entrada (Landing Page) / Fan Gate?', 'br');
settext('$APP_FORM_FAN_GATE_YES', 'Sim', 'br');
settext('$APP_FORM_FAN_GATE_NO', 'Não', 'br');
settext('$APP_FORM_REST_URL', 'ENDPOINT DO APLICATIVO', 'br');
settext('$APP_FORM_ICON', 'IMAGEM DO APLICATIVO', 'br');
settext('$APP_FORM_DESCRIPTION', 'DESCRIÇÃO', 'br');

settext('$CODA_APPS_SUBSCRIPTION_TITLE', 'Assinar plano', 'br');
settext('$CODA_APPS_SUBSCRIPTION_TYPE', 'Duração da assinatura', 'br');
settext('BR.M12', 'ANUAL', 'br');
settext('BR.M06', 'SEMESTRAL', 'br');
settext('BR.M01', 'MENSAL', 'br');
settext('BR.D15', 'Testar Gratuitamente durante <b>15 dias</b>', 'br');

settext('M01_FINFINITE', 'Assinatura mensal com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M01_F1000000', 'Assinatura mensal até 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M01_F500000', 'Assinatura mensal até 500 mil fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M01_F100000', 'Assinatura mensal até 100 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M01_F50000', 'Assinatura mensal até 50 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M01_F10000', 'Assinatura mensal até 10 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M01_F5000', 'Assinatura mensal até 5 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M01_F1000', 'Assinatura mensal até mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('D15_FINFINITE', 'Assinatura quinzenal gratuíta com utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M12_FINFINITE', 'Assinatura anual com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M12_F1000000', 'Assinatura anual até 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M12_F500000', 'Assinatura anual até 500 mil fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M12_F100000', 'Assinatura anual até 100 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M12_F50000', 'Assinatura anual até 50 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M12_F10000', 'Assinatura anual até 10 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M12_F5000', 'Assinatura anual até 5 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M12_F1000', 'Assinatura anual até mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M06_FINFINITE', 'Assinatura semestral com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M06_F1000000', 'Assinatura semestral até 1 milhão de fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M06_F500000', 'Assinatura semestral até 500 mil fãs, utilização ilimitada, white label, suporte preferencial ', 'br');
settext('M06_F100000', 'Assinatura semestral até 100 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M06_F50000', 'Assinatura semestral até 50 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M06_F10000', 'Assinatura semestral até 10 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M06_F5000', 'Assinatura semestral até 5 mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');
settext('M06_F1000', 'Assinatura semestral até mil fãs, utilização ilimitada, sem white label, suporte padrão', 'br');

settext('$CODA_APPS_SUBSCRIPTION_PRODUCT_TITLE', 'Plano de assinatura', 'br');
settext('$CODA_APPS_SUBSCRIPTION_PRODUCT', 'Planos de assinatura disponíveis', 'br');
settext('$CODA_APPS_SUBSCRIPTION_FREEMIUM', 'Crie todos os aplicativos que quiser e teste-os por 15 dias na sua página, sem compromissos e sem necessidade de cartão de crédito.<br><br>Aceite os termos e condições abaixo e clique em "Continuar" para terminar.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_PROVIDER', 'Selecione a forma de pagamento:', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CURRENCY', '$VALUE $CURRENCY', 'br');

settext('net_value', 'Valor líquido', 'br');
settext('discount_amount', 'Desconto', 'br');
settext('vat', 'IVA', 'br');
settext('total', 'Total', 'br');

settext('$CODA_APPS_SUBSCRIPTION_FREE_SUMMARY', '<h4>O seu aplicativo é grátis!</h4><br>As assinaturas para páginas com número de fãs inferior ou igual a 1000 são gratuitas.<br>A duração da assinatura é mensal, mas você pode renovar quantas vezes quiser.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT', 'Li e aceito os <a target="_blank" href="https://www.redpandasocial.com/apps-termos-e-condicoes">Termos e Condições</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT_TEXT', 'Se ainda não leu, consulte <a target="_blank" href="https://www.redpandasocial.com/apps-termos-e-condicoes">aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>.', 'br');

settext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE', 'Possui um código promocional?<br><span>Digite ou cole e obtenha desconto na assinatura!</span>', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER', 'Número do cartão de crédito', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CARDCVV', 'CVV', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME', 'Nome presente no cartão de crédito', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE', 'Data de validade do cartão de crédito', 'br');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS', 'Endereço para cobrança', 'br');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE', 'CEP', 'br');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY', 'País', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_TITLE', 'Dados de Cartão de Crédito', 'br');
settext('$CODA_APPS_SUBSCRIPTION_ADDRESS_TITLE', 'Morada para Recibo de pagamento', 'br');

settext('$CODA_APPS_SUBSCRIPTION_PROMOCODE_FREE', 'O seu código promocional permite que você assine o produto escolhido de forma gratuita.<br>Clique em "Continuar" para finalizar o processo de ativação.', 'br');

settext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_TITLE', 'Nome & Página de entrada ', 'br');
settext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_DESC', 'Digite o nome que você irá usar para identificar o seu aplicativo aqui e nas redes sociais.<br>Determine se o seu aplicativo terá uma página de entrada.<br><br><i>Você poderá alterar esses dados na área de gestão do aplicativo.</i><br>', 'br');
settext('$CODA_APPLICATION_WIZ_SHARING_PANEL_TITLE', 'Dados para compartilhar', 'br');
settext('$CODA_APPLICATION_WIZ_SHARING_PANEL_DESC', 'Escolha a imagem e o texto descritivo que será utilizado como metadata do seu aplicativo.<br>Estes dados serão utilizados para compartilhar com as redes sociais e outros.<br><br><i>Você poderá alterar esses dados na área de gestão do aplicativo.</i><br>', 'br');
settext('$CODA_APPLICATION_WIZ_FBPAGEINFO_PANEL_TITLE', 'Fanpage de Facebook', 'br');
settext('$CODAAPPLICATIONSUBSCRIPTIONWIZARD_FBPAGEINFO_PANEL_DESC', 'Indique em qual fanpage você irá usar o aplicativo.<br><br><i>Fanpage em que este aplicativo será associado <b>NÃO</b> poderá ser alterada posteriormente.</i><br>', 'br');
settext('$CODASUBSRIPTIONWIZARD_FBPAGEINFO_PANEL_DESC', 'Indique em qual fanpage essa assinatura esta associada.<br><br><i>Fanpage em que esta assinatura será associada <b>NÃO</b> poderá ser alterada posteriormente.</i><br>', 'br');
settext('$CODA_APPS_CREATED_SUCCESS_MESSAGE', 'Fantástico, o seu aplicativo foi criado!<br><br><span class="KMessageDialogMessageFootnoteText">Clique no ícone do aplicativo (atrás dessa mensagem) para acessar à área de gestão.</span>', 'br');
settext('$CODA_SUBSCRIPTION_SUCCESS_MSG', 'Fantástico, sua assinatura foi ativada!<br><br>Você já pode publicar os aplicativos na sua fanpage.', 'br');
settext('$ERROR_PAGE_NOT_FOUND_OR_NOT_PUBLIC', 'Essa página não existe ou não é pública.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION FORM
settext('$APPS_EDIT_PROPERTIES', 'APLICATIVO', 'br');
settext('$APP_NOT_YET_ACTIVATED', '<span>Aplicativo ainda não está ativo</span>', 'br');
settext('$APP_FORM_ID_LABEL', 'ID DO APLICATIVO (<span style="text-transform: lowercase !important;">client_id</span>)', 'br');
settext('$APP_FORM_SECRET_LABEL', 'SEGREDO DO APLICATIVO (<span style="text-transform: lowercase !important;">client_secret</span>)', 'br');
settext('$APP_FORM_CREATION_DATE_LABEL', 'DATA DE REGISTRO', 'br');
settext('$APP_FORM_TINY_URL', 'URL único', 'pt');

settext('$APP_FORM_INFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'br');
settext('$APP_FORM_SETTINGS_PANEL', '<i class="fa fa-cogs"></i> Info Técnica', 'br');
settext('$APP_FORM_OWNERS_PANEL', '<i class="fa fa-group"></i> Administradores', 'br');
settext('$APP_FORM_FACEBOOK_PANEL', '<i class="fa fa-facebook-square"></i> Facebook', 'br');
settext('$APP_FORM_MOBILE_PANEL', '<i class="fa fa-tablet"></i> Mobile', 'pt');
settext('$APP_FORM_WEBSITE_PANEL', '<i class="fa fa-globe"></i> Website', 'br');
settext('$APP_FORM_BACKOFFICE_PANEL', '<i class="fa fa-cogs"></i> Backoffice', 'br');

settext('$APP_ADD_OWNER_APP', 'Atribuir acesso de administrador', 'br');
settext('$APP_OWNER_HREF', 'Pesquise pelo endereço de e-mail', 'br');

settext('$APP_OWNER_LIST_HREF', '<i class="fa fa-envelope"></i>&nbsp;&nbsp;E-mail', 'br');
settext('$APP_OWNER_LIST_NAME', '<i class="fa fa-user"></i>&nbsp;&nbsp;Nome', 'br');
settext('$APP_OWNER_LIST_ROLE', '<i class="fa fa-key"></i>&nbsp;&nbsp;Permissões', 'br');

settext('$APP_FORM_LOCALES', 'Disponibilizar esta aplicação nos seguintes idiomas', 'br');
settext('$APP_FORM_LOCALES_PANEL', '<i class="fa fa-flag"></i> Idiomas', 'br');

settext('$FORM_GENERAL_DATA_PANEL', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CHANNEL RESOLVER FORM
settext('$APP_FORM_FACEBOOK_PAGE_URL', 'URL PARA A FANPAGE', 'br');
settext('$APP_FORM_FACEBOOK_PAGE_ID', 'IDENTIFICADOR DA FANPAGE', 'br');
settext('$APP_FORM_FACEBOOK_DOCROOT', 'URL ARMAZENAMENTO DA APLICAÇÂO FACEBOOK', 'br');

settext('$APP_FORM_MOBILE_REFERER', 'Domínio base da aplicação móvel', 'pt');
settext('$APP_FORM_MOBILE_PAGE_URL', 'URL base da aplicação móvel', 'pt');
settext('$APP_FORM_MOBILE_DOCROOT', 'URL armazenamento da aplicação móvel', 'pt');

settext('$APP_FORM_WEBSITE_ID', 'Domínio base do Website', 'br');
settext('$APP_FORM_WEBSITE_DOCROOT', 'URL base para o Website', 'br');
settext('$APP_FORM_WEBSITE_LOCALE', 'Idioma', 'br');
settext('$APP_FORM_WEBSITE_PREFIX', 'Prefixo', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAApplicationsLocale_pt');

///////////////////////////////////////////////////////////////////
// APPLICATION WIZARD
settext('$APP_FORM_NAME_HELP', 'Digite o título do seu aplicativo. Será utilizado para:<br>- identificar o aplicativo <strong>nesta plataforma</strong>;<br>- identificar o aplicativo <strong>nas redes sociais</strong>.', 'br');
settext('$APP_FORM_FAN_GATE_HELP', 'Defina se existe uma página de entrada (landing page).<br>No caso de <strong>aplicativos de Facebook</strong>, indique se o aplicativo tem <strong>"Fan Gate"</strong>.', 'br');
settext('$APP_FORM_REST_URL_HELP', '', 'br');
settext('$APP_FORM_ICON_HELP', 'Escolha uma imagem para identificar o aplicativo. A imagem utilizada para:<br>- representar o aplicativo <strong>nesta plataforma</strong>;<br>- representar o aplicativo <strong>nas redes sociais</strong>.', 'br');
settext('$APP_FORM_DESCRIPTION_HELP', 'Pequena descrição do seu aplicativo.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP', 'Selecione o período do plano de assinatura . No final do período indicado, se não existir outra assinatura ativa, os aplicativos associados a esta assinatura ficarão bloqueados.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE_HELP', 'Se você recebeu um código promocional, digite ou cole aqui e será descontado a porcentagem associada do total a pagar.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER_HELP', 'Número associado ao seu cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDCVV_HELP', 'Código de confirmação (CVV) presente no verso do seu cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME_HELP', 'Nome do titular do cartão de crédito, como apresentado no cartão', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE_HELP', 'Data de expiração do cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS_HELP', 'Endereço a ser associado ao recibo deste pagamento.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE_HELP', 'Código postal do endereço a ser associado ao recibo deste pagamento.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY_HELP', 'País de origem do endereço a ser associado a este pagamento.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_HELP', 'Digite o número associado ao Cartão de crédito.<br>Digite o código de confirmação (CVV) no verso do Cartão de crédito.<br>Digite a data de expirtação do Cartão de crédito.', 'br');
settext('$CODA_APPS_SUBSCRIPTION_ADDRESS_HELP', 'Digite a morada a ser associada ao recibo para este pagamento.<br>Digite o código postal associado à morada.<br>Digite o país associado à morada.', 'br');

settext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP2', 'Escolha a duração do plano de assinatura.<br/><br>Se você ainda não conhece os planos de pagamento e a tabela de preços, <a href="https://www.redpandasocial.com/$LANG/pricing" target="_blank">consulte aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>', 'br');
settext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP', 'Escolha o plano de assinatura que melhor se adeque às suas necessidades.<br/><br>Se você ainda não conhece os planos de pagamento e a tabela de preços, <a href="https://www.redpandasocial.com/$LANG/pricing" target="_blank">consulte aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION FORM
settext('$APP_OWNER_HREF_HELP', 'Digite o endereço (completo ou parcial) e pressione "enter" ou clique no botão para procurar', 'br');
settext('$APP_OWNER_INFO_HELP', 'Digite o endereço (completo ou parcial) e pressione "enter" ou clique no botão para procurar', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CHANNEL RESOLVER FORM
settext('$APP_FORM_FACEBOOK_PAGE_URL_HELP', 'Digite ou cole o link (url) da fanpage que deseja associar este aplicativo.<br>Se tiver dificuldades de lembrar o endereço, abra uma nova janela/aba, navegue até à fanpage em questão, copie o endereço da barra de endereço do seu browser e cole aqui.', 'br');
settext('$APP_FORM_FACEBOOK_PAGE_ID_HELP', '', 'br');
settext('$APP_FORM_FACEBOOK_PAGE_INFO_HELP', 'Digite ou cole o URL da página e pressione "enter" ou clique <i class="fa fa-level-up"></i> no botão para atualizar', 'br');
settext('$APP_FORM_FACEBOOK_DOCROOT_HELP', '', 'br');

settext('$APP_FORM_MOBILE_ID_HELP', '', 'pt');
settext('$APP_FORM_MOBILE_DOCROOT_HELP', '', 'pt');

settext('$APP_FORM_WEBSITE_ID_HELP', '', 'br');
settext('$APP_FORM_WEBSITE_DOCROOT_HELP', '', 'br');
settext('$APP_FORM_WEBSITE_LOCALE_HELP', '', 'br');
settext('$APP_FORM_WEBSITE_PREFIX_HELP', '', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAApplicationsLocale_Help_pt');

