///////////////////////////////////////////////////////////////////
// APPLICATION WIZARD
settext('$APP_FORM_NAME_HELP', 'Define o título da tua aplicação. Será utilizado para:<br>- identificar a aplicação <strong>nesta plataforma</strong>;<br>- identificar a aplicação em <strong>partilhas nas diversas redes sociais</strong>.', 'pt');
settext('$APP_FORM_FAN_GATE_HELP', 'Escolhe se existe uma página de entrada (landing page).<br>No caso de <strong>aplicações de Facebook</strong>, indica se a aplicação tem <strong>"Fan Gate"</strong>.', 'pt');
settext('$APP_FORM_REST_URL_HELP', '', 'pt');
settext('$APP_FORM_ICON_HELP', 'Define a imagem que vai identificar a aplicação. A imagem utilizada para:<br>- representar a aplicação <strong>nesta plataforma</strong>;<br>- representar a aplicação em <strong>partilhas nas diversas redes sociais</strong>.', 'pt');
settext('$APP_FORM_DESCRIPTION_HELP', 'Descreve sucintamente a tua aplicação.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP', 'Escolhe a duração para o plano de subscrição. No final da duração indicada, se não existir outra subscrição activa, as aplicações associadas a esta subscrição ficaram bloqueadas.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE_HELP', 'Se te foi atribuído um código promocional, podes introduzi-lo aqui e será descontada ao total a pagar, a percentagem associada.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER_HELP', 'Número associado ao teu cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDCVV_HELP', 'Código de confirmação (CVV) presente no verso do teu cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME_HELP', 'Nome do titular do cartão de crédito, como apresentado no cartão', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE_HELP', 'Data de expiração do cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS_HELP', 'Morada a ser associada ao recibo para este pagamento.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE_HELP', 'Código postal para a morada a ser associada ao recibo para este pagamento.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY_HELP', 'País de origem para a morada a ser associada a este pagamento.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_HELP', 'Indica o número associado ao Cartão de crédito.<br>Indica o código de confirmação (CVV) no verso do Cartão de crédito.<br>Indica a data de expirtação do Cartão de crédito.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_ADDRESS_HELP', 'Indica a morada a ser associada ao recibo para este pagamento.<br>Indica o código postal associado à morada.<br>Indica o país associado à morada.', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP2', 'Escolhe a duração para o plano de subscrição que pretendes associar à página de Facebook.<br/><br>Se ainda não conheces os planos de pagamento e a tabela de preços, <a href="https://www.redpandasocial.com/$LANG/pricing" target="_blank">consulta aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP', 'Escolhe o plano de subscrição que melhor se adequa às tuas necessidades.<br/><br>Se ainda não conheces os planos de pagamento e a tabela de preços, <a href="https://www.redpandasocial.com/$LANG/pricing" target="_blank">consulta aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>', 'pt');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION FORM
settext('$APP_OWNER_HREF_HELP', 'Escreve o endereço (completo ou parcial) e pressiona "enter" ou clica no botão para procurar', 'pt');
settext('$APP_OWNER_INFO_HELP', 'Escreve o endereço (completo ou parcial) e pressiona "enter" ou clica no botão para procurar', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CHANNEL RESOLVER FORM
settext('$APP_FORM_FACEBOOK_PAGE_URL_HELP', 'Escreve o endereço de internet (url) da página de Facebook à que queres associar esta aplicação.<br>Se tiveres dificuldades ou não te lembrares do endereço, abre uma nova janela/aba, navega até à página de Facebook em questão, copia o endereço da barra de endereço do seu browser e cola-o aqui.', 'pt');
settext('$APP_FORM_FACEBOOK_PAGE_ID_HELP', '', 'pt');
settext('$APP_FORM_FACEBOOK_PAGE_INFO_HELP', 'Introduz o URL da página e pressiona "enter" ou clica <i class="fa fa-level-up"></i> no botão para refrescar', 'pt');
settext('$APP_FORM_FACEBOOK_DOCROOT_HELP', '', 'pt');

settext('$APP_FORM_MOBILE_ID_HELP', '', 'pt');
settext('$APP_FORM_MOBILE_DOCROOT_HELP', '', 'pt');

settext('$APP_FORM_WEBSITE_ID_HELP', '', 'pt');
settext('$APP_FORM_WEBSITE_DOCROOT_HELP', '', 'pt');
settext('$APP_FORM_WEBSITE_LOCALE_HELP', '', 'pt');
settext('$APP_FORM_WEBSITE_PREFIX_HELP', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAApplicationsLocale_Help_pt');

///////////////////////////////////////////////////////////////////
// SHELL
settext('$APP_SHELL_TITLE', 'aplicações', 'pt');
settext('$MODULE_SHELL_TITLE', '', 'pt');
settext('$CODA_APPS_ADD', 'CRIAR APLICAÇÃO <i class="fa fa-magic"></i>', 'pt');
settext('$CODA_APPS_ADD_TOP', '<i class="fa fa-magic"></i>CRIAR APLICAÇÃO', 'pt');
settext('$APP_NO_ITEMS', 'Não existem aplicações', 'pt');
settext('$APP_CALL_TO_ACTION', 'Cria a tua primeira aplicação! <i class="fa fa-level-down"></i>', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATIONS DASHBOARD
settext('$APP_PREVIEW_ACTION', '<i class="fa fa-eye"></i> VISUALIZAR', 'pt');
settext('$APP_EDIT_ACTION', '<i class="fa fa-wrench"></i> DEFINIÇÕES', 'pt');
settext('$APP_ADD_USER_ACTION', '<i class="fa fa-group"></i> ATRIBUIR PERMISSÔES ADMIN.', 'pt');
settext('$APP_REMOVE_ACTION', 'REMOVER <i class="fa fa-trash-o"></i>', 'pt');
settext('$APP_ACTIVATE_ACTION', '<i class="fa fa-check"></i> ATIVAR', 'pt');
settext('$APP_DELETE_CONFIRM_MSG', 'Tens a certeza que desejas remover esta aplicação? Esta acção não poderá ser revertida.', 'pt');
settext('$APP_CANCEL_CONFIRM_MSG', 'Tens a certeza que desejas desligar esta aplicação? <br><br><span class="KConfirmDialogQuestionFootnoteText">Os dados da aplicação e a reactivação ficarão disponíveis em "Meu Perfil > Aplicações" durante um período de 30 dias.</span>', 'pt');
settext('$APP_BLOCK_ACTION', 'BLOQUEAR <i class="fa fa-ban"></i>', 'pt');
settext('$APP_CANCEL_ACTION', 'DESLIGAR <i class="fa fa-power-off"></i> ', 'pt');
settext('$APP_ADDTOPAGE_ACTION', '<i class="rp rp-publish"></i> PUBLICAR', 'pt');
settext('$CODA_UNIQUE_URL', 'LINK', 'pt')

settext('$CODA_ICON_STATE_FA-MAGIC GRAY', 'Aplicação está criada, mas não activa. Para activar, entra na área de gestão da aplicação e clica em «Activar Aplicação»', 'pt');
settext('$CODA_ICON_STATE_RP RP-PUBLISH GREEN', 'Aplicação está activa. Se ainda não o fizeste, podes torná-la pública.', 'pt');
settext('$CODA_ICON_STATE_FA-CHECK GREEN', 'Aplicação está activa. Se ainda não o fizeste, podes torná-la pública.', 'pt');
settext('$CODA_ICON_STATE_FA-BAN RED', 'Aplicação está bloqueada, não permitindo o acesso dos utilizadores. Podes reactiva-la na área de gestão da aplicação.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION WIZARD
settext('$APP_ADD_NEW', 'NOVA APLICAÇÃO', 'pt');
settext('$APP_OWNER_ADDED_MESSAGE', 'Foram atribuídas ao utilizador as permissões de administrador.', 'pt');
settext('$APP_OWNER_DELETED_MESSAGE', 'Foram revogas ao utilizador as permissões de administrador.', 'pt');

settext('$APP_FORM_NAME', 'TÍTULO', 'pt');
settext('$APP_FORM_MAX_PARTICIPATIONS', 'NÚMERO MÁXIMO DE PARTICIPAÇÕES POR UTILIZADOR', 'pt');
settext('$APP_FORM_FAN_GATE', 'Página de Entrada (Landing Page) / Fan Gate?', 'pt');
settext('$APP_FORM_FAN_GATE_YES', 'Sim', 'pt');
settext('$APP_FORM_FAN_GATE_NO', 'Não', 'pt');
settext('$APP_FORM_REST_URL', 'ENDPOINT DA APLICAÇÃO', 'pt');
settext('$APP_FORM_ICON', 'IMAGEM DA APLICAÇÃO', 'pt');
settext('$APP_FORM_DESCRIPTION', 'DESCRIÇÃO', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_TITLE', 'Subscrever plano', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_TYPE', 'Duração da subscrição', 'pt');
settext('PT.M12', 'ANUAL', 'pt');
settext('PT.M06', 'SEMESTRAL', 'pt');
settext('PT.M01', 'MENSAL', 'pt');
settext('PT.D15', 'Experimentar Gratuitamente durante <b>15 dias</b>', 'pt');

settext('M01_FINFINITE', 'Subscrição mensal com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M01_F1000000', 'Subscrição mensal até 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M01_F500000', 'Subscrição mensal até 500 mil fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M01_F100000', 'Subscrição mensal até 100 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M01_F50000', 'Subscrição mensal até 50 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M01_F10000', 'Subscrição mensal até 10 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M01_F5000', 'Subscrição mensal até 5 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M01_F1000', 'Subscrição mensal até mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('D15_FINFINITE', 'Subscrição quinzenal gratuíta com utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M12_FINFINITE', 'Subscrição anual com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M12_F1000000', 'Subscrição anual até 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M12_F500000', 'Subscrição anual até 500 mil fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M12_F100000', 'Subscrição anual até 100 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M12_F50000', 'Subscrição anual até 50 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M12_F10000', 'Subscrição anual até 10 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M12_F5000', 'Subscrição anual até 5 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M12_F1000', 'Subscrição anual até mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M06_FINFINITE', 'Subscrição semestral com mais de 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M06_F1000000', 'Subscrição semestral até 1 milhão de fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M06_F500000', 'Subscrição semestral até 500 mil fãs, utilização ilimitada, white label, suporte preferêncial', 'pt');
settext('M06_F100000', 'Subscrição semestral até 100 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M06_F50000', 'Subscrição semestral até 50 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M06_F10000', 'Subscrição semestral até 10 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M06_F5000', 'Subscrição semestral até 5 mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');
settext('M06_F1000', 'Subscrição semestral até mil fãs, utilização ilimitada, sem white label, suporte standard', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_PRODUCT_TITLE', 'Plano de Subscrição', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_PRODUCT', 'Planos de Subscrição disponíveis', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_FREEMIUM', 'Constrói todas as aplicações que quiseres e utiliza-as durante 15 dias na tua página, sem compromissos e sem necessidade de cartão de crédito.<br><br>Aceita os termos e condições abaixo e clica em "Continuar" para terminar.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_PROVIDER', 'Selecciona uma forma de pagamento:', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CURRENCY', '$VALUE $CURRENCY', 'pt');

settext('net_value', 'Valor líquido', 'pt');
settext('discount_amount', 'Desconto', 'pt');
settext('vat', 'IVA', 'pt');
settext('total', 'Total', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_FREE_SUMMARY', '<h4>A tua aplicação é gratuita!</h4><br>As subscrições para páginas de Facebook com número de fãs inferior ou igual a 1000 são gratuitas.<br>A duração da subscrição é mensal, mas pode renová-la tantas vezes quantas quiser.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT', 'Li e aceito os <a target="_blank" href="https://www.redpandasocial.com/apps-termos-e-condicoes">Termos e Condições</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>.', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT_TEXT', 'Se ainda não leste, verifica <a target="_blank" href="https://www.redpandasocial.com/apps-termos-e-condicoes">aqui</a> <s style="font-size: 10px;"><i class="fa fa-external-link"></i></s>.', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE', 'Tens um código promocional?<br><span>Redime-o agora e obtem desconto na subscrição!</span>', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER', 'Número do cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDCVV', 'CVV', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME', 'Nome presente no cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE', 'Data de expirtação do cartão de crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS', 'Morada de faturação', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE', 'Código Postal', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY', 'País', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_TITLE', 'Dados de Cartão de Crédito', 'pt');
settext('$CODA_APPS_SUBSCRIPTION_ADDRESS_TITLE', 'Morada para Recibo de pagamento', 'pt');

settext('$CODA_APPS_SUBSCRIPTION_PROMOCODE_FREE', 'O teu código promocional permite-lhe subscrever o produto escolhido de forma gratuita.<br>Clica em "Continuar" para finalizar o processo de ativação.', 'pt');

settext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_TITLE', 'Nome & Página de entrada ', 'pt');
settext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_DESC', 'Indica o nome que pretendes que identifique a tua aplicação neste sistema e nas partilhas em redes sociais.<br>Determina se a tua aplicação terá uma página de entrada.<br><br><i>Poderás alterar estes valores na área de gestão da aplicação.</i><br>', 'pt');
settext('$CODA_APPLICATION_WIZ_SHARING_PANEL_TITLE', 'Dados para partilha', 'pt');
settext('$CODA_APPLICATION_WIZ_SHARING_PANEL_DESC', 'Escolhe a imagem e o texto descritivo que será utilizado como metadata da tua aplicação.<br>Estes dados serão utilizados em partilhas em redes sociais e outros.<br><br><i>Poderás alterar estes valores na área de gestão da aplicação.</i><br>', 'pt');
settext('$CODA_APPLICATION_WIZ_FBPAGEINFO_PANEL_TITLE', 'Página de Fãs de Facebook', 'pt');
settext('$CODAAPPLICATIONWIZARD_FBPAGEINFO_PANEL_DESC', 'Indica qual a página de fãs do Facebook que irá alojar esta aplicação.<br><br><i>A página à qual esta aplicação ficará associada <b>NÃO</b> poderá ser alterada posteriormente.</i><br>', 'pt');		 
settext('$CODAAPPLICATIONSUBSCRIPTIONWIZARD_FBPAGEINFO_PANEL_DESC', 'Indica qual a página de fãs do Facebook que irá alojar esta aplicação.<br><br><i>A página à qual esta aplicação ficará associada <b>NÃO</b> poderá ser alterada posteriormente.</i><br>', 'pt');
settext('$CODASUBSRIPTIONWIZARD_FBPAGEINFO_PANEL_DESC', 'Indica qual a página de fãs do Facebook a que se refere esta subscrição.<br><br><i>A página à qual esta subscrição ficará associada <b>NÃO</b> poderá ser alterada posteriormente.</i><br>', 'pt');
settext('$CODA_APPS_CREATED_SUCCESS_MESSAGE', 'Fantástico, a aplicação está criada!<br><br><span class="KMessageDialogMessageFootnoteText">Clica no ícone da aplicação (mesmo atrás desta mensagem) para acederes à área de gestão.</span>', 'pt');
settext('$CODA_SUBSCRIPTION_SUCCESS_MSG', 'Fantástico, a subscrição foi ativada!<br><br>Já podes disponibilizar as aplicações na tua página de Facebook.', 'pt');
settext('$ERROR_PAGE_NOT_FOUND_OR_NOT_PUBLIC', 'Esta página de Facebook não existe ou não é pública.', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// APPLICATION FORM
settext('$APPS_EDIT_PROPERTIES', 'APLICAÇÃO', 'pt');
settext('$APP_NOT_YET_ACTIVATED', '<span>Aplicação ainda não activada</span>', 'pt');
settext('$APP_FORM_ID_LABEL', 'ID DA APLICAÇÃO (<span style="text-transform: lowercase !important;">client_id</span>)', 'pt');
settext('$APP_FORM_SECRET_LABEL', 'SECREDO DA APLICAÇÃO (<span style="text-transform: lowercase !important;">client_secret</span>)', 'pt');
settext('$APP_FORM_CREATION_DATE_LABEL', 'DATA DE REGISTO', 'pt');
settext('$APP_FORM_TINY_URL', 'URL único', 'pt');

settext('$APP_FORM_INFO_PANEL', '<i class="fa fa-info-circle"></i> Info Geral', 'pt');
settext('$APP_FORM_SETTINGS_PANEL', '<i class="fa fa-cogs"></i> Info Técnica', 'pt');
settext('$APP_FORM_OWNERS_PANEL', '<i class="fa fa-group"></i> Administradores', 'pt');
settext('$APP_FORM_FACEBOOK_PANEL', '<i class="fa fa-facebook-square"></i> Facebook', 'pt');
settext('$APP_FORM_MOBILE_PANEL', '<i class="fa fa-tablet"></i> Mobile', 'pt');
settext('$APP_FORM_WEBSITE_PANEL', '<i class="fa fa-globe"></i> Website', 'pt');
settext('$APP_FORM_BACKOFFICE_PANEL', '<i class="fa fa-cogs"></i> Backoffice', 'pt');

settext('$APP_ADD_OWNER_APP', 'Atribuir acesso de administração', 'pt');
settext('$APP_OWNER_HREF', 'Pesquise pelo endereço de e-mail', 'pt');

settext('$APP_OWNER_LIST_HREF', '<i class="fa fa-envelope"></i>&nbsp;&nbsp;E-mail', 'pt');
settext('$APP_OWNER_LIST_NAME', '<i class="fa fa-user"></i>&nbsp;&nbsp;Nome', 'pt');
settext('$APP_OWNER_LIST_ROLE', '<i class="fa fa-key"></i>&nbsp;&nbsp;Permissões', 'pt');

settext('$APP_FORM_LOCALES', 'Disponibilizar esta aplicação nos seguintes idiomas', 'pt');
settext('$APP_FORM_LOCALES_PANEL', '<i class="fa fa-flag"></i> Idiomas', 'pt');

settext('$FORM_GENERAL_DATA_PANEL', '', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CHANNEL RESOLVER FORM
settext('$APP_FORM_FACEBOOK_PAGE_URL', 'URL PARA A PÁGINA DE FACEBOOK', 'pt');
settext('$APP_FORM_FACEBOOK_PAGE_ID', 'IDENTIFICADOR DA PÁGINA DE FACEBOOK', 'pt');
settext('$APP_FORM_FACEBOOK_DOCROOT', 'URL ARMAZENAMENTO DA APLICAÇÂO FACEBOOK', 'pt');

settext('$APP_FORM_MOBILE_REFERER', 'Domínio base da aplicação móvel', 'pt');
settext('$APP_FORM_MOBILE_PAGE_URL', 'URL base da aplicação móvel', 'pt');
settext('$APP_FORM_MOBILE_DOCROOT', 'URL armazenamento da aplicação móvel', 'pt');

settext('$APP_FORM_WEBSITE_ID', 'Domínio base do Website', 'pt');
settext('$APP_FORM_WEBSITE_DOCROOT', 'URL base para o Website', 'pt');
settext('$APP_FORM_WEBSITE_LOCALE', 'Idioma', 'pt');
settext('$APP_FORM_WEBSITE_PREFIX', 'Prefixo', 'pt');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KSystem.included('CODAApplicationsLocale_pt');

