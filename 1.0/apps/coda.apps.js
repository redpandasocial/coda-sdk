KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
KSystem.includes[jsClass[index]] = {
include : null
};
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

function CODAApplicationFacebookResolver(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
		this.setStyle({
			minHeight : '300px'
		});
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAApplicationFacebookResolver.prototype = new KPanel();
	CODAApplicationFacebookResolver.prototype.constructor = CODAApplicationFacebookResolver;

	CODAApplicationFacebookResolver.prototype.pageInfo = function(data) {
		this.childWidget('/page_url').enable();
		if (!!data.error || !data.likes) {
			this.childWidget('/page_url').setValue('https://www.facebook.com/');
			this.childWidget('/page_url').addCSSClass('KAbstractInputError');
			this.childWidget('/page_url').errorArea.innerHTML = gettext('$ERROR_PAGE_NOT_FOUND_OR_NOT_PUBLIC');
			this.page_id.setValue('');
			this.page_info.style.textAlign = 'right';
			this.page_info.innerHTML = gettext('$APP_FORM_FACEBOOK_PAGE_INFO_HELP');
		}
		else {
			this.childWidget('/page_url').validate();
			this.page_id.setValue(data.id);
			this.page_info.style.textAlign = 'left';
			this.page_info.innerHTML = '<img src="' + data.picture.data.url + '"/><b>' + data.name + '</b><br/>' + (!!data.about ? data.about : '');
		}
	};

	CODAApplicationFacebookResolver.prototype.draw = function() {
		var page_url = new KInput(this, gettext('$APP_FORM_FACEBOOK_PAGE_URL') + ' *', 'facebook.app_url');
		{
			page_url.hash = '/page_url';
			page_url.setValue('https://www.facebook.com/');
			page_url.setHelp(gettext('$APP_FORM_FACEBOOK_PAGE_URL_HELP'), CODAForm.getHelpOptions());
			page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			page_url.addValidator(/^(sftp|ftp|http|https):\/\/[^ "]+$/, gettext('$ERROR_MUST_BE_URL'));

			var getpage = new KInputAction(page_url, function(event) {
				var _this = KDOM.getEventWidget(event).getParent('CODAApplicationFacebookResolver');
				var url = page_url.getValue();
				if (url == '') {
					return;
				}
				page_url.disable();
				var id = url.substring(url.lastIndexOf('/') + 1);

				var head = document.getElementsByTagName('head')[0];
				var script = document.createElement('script');
				script.type = 'text/javascript';
				script.src = 'https://graph.facebook.com/' + id + '?fields=id,link,picture,name,about,likes&callback=Kinky.bunnyMan.widgets[\'' +  _this.id + '\'].pageInfo';
				head.appendChild(script);
			}, '<i class="fa fa-refresh"></i>');
			page_url.appendChild(getpage);

		}
		this.appendChild(page_url);

		this.page_info = window.document.createElement('div');
		this.page_info.className = ' CODAApplicationFBPageInfo ';
		this.page_info.style.textAlign = 'right';
		this.page_info.innerHTML = gettext('$APP_FORM_FACEBOOK_PAGE_INFO_HELP');
		this.container.appendChild(this.page_info);

		this.page_id = new KHidden(this, 'facebook.page_id');
		{
			this.page_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
		}
		this.appendChild(this.page_id);

		KPanel.prototype.draw.call(this);

		this.title = gettext('$CODA_APPLICATION_WIZ_FBPAGEINFO_PANEL_TITLE');
		this.description = gettext('$' + this.parent.className.toUpperCase() + '_FBPAGEINFO_PANEL_DESC');
		this.icon = 'css:fa fa-facebook-square';
	};

	CODAApplicationFacebookResolver.addWizardPanel = function(parent) {
		return [ new CODAApplicationFacebookResolver(parent) ];
	};

	KSystem.included('CODAApplicationFacebookResolver');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationForm(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAForm.call(this, parent, href, layout, parentHref);
		this.config = this.config || {};
		this.canEditAdvanced = CODAForm.hasScope('admin');
		this.target.collection = '/applications';
		this.nillable = true;
	}
}

KSystem.include([
	'CODAForm',
	'CODAApplicationOwnerList',
	'KRadioButton',
	'KDate',
	'KPassword'
], function() {
	CODAApplicationForm.prototype = new CODAForm();
	CODAApplicationForm.prototype.constructor = CODAApplicationForm;
 
	CODAApplicationForm.prototype.setData = function(data) {
		this.config.schema = undefined;
		this.config.requestType = "KPanel";

		if (!!data) {
			this.target.id = this.target.href = data._id;
			this.target.data = data;
			this.data = this.target.data;
		}
		else {
			var types = [
				'application/json',
				'application/wrml;schema=\"' + this.config.schema + '\"',
				'application/vnd.kinky.' + this.config.requestType
			];
			this.data = this.target.data = {
				requestTypes : types,
				responseTypes : types
			};
		}
	};

	CODAApplicationForm.prototype.addPanels = function() {
		this.removeCSSClass('CODAForm');
		this.addCSSClass('CODASideForm');
		this.addCSSClass('CODAFormContent', this.content);
		this.addCSSClass('CODAFormMessages', this.messages);
		this.addMinimizedPanel();
	};

	CODAApplicationForm.prototype.pageInfo = function(data) {
		this.page_info.innerHTML = '<img src="' + data.picture.data.url + '"/><b>' + data.name + '</b><br/>' + (!!data.about ? data.about : '');
		this.page_info.style.textAlign = 'left';
		this.childWidget('/facebook').panel.appendChild(this.page_info);
	};

	CODAApplicationForm.prototype.addMinimizedPanel = function() {
		var data = this.data;
		if (!!data.id) {
			this.setTitle(gettext('$APPS_EDIT_PROPERTIES') + ' \u00ab' + data.id + '\u00bb');
		}

		var appPanel = new KPanel(this);
		{
			appPanel.hash = '/general';

			if (!!Kinky.CODA_SHORT_URL) {
				var tiny_url = new KStatic(appPanel, gettext('$APP_FORM_TINY_URL') + '', 'tiny_url');
				{
					if (!!data && !!data.id) {
						tiny_url.setValue(Kinky.CODA_SHORT_URL + data.id);
					}
				}
				appPanel.appendChild(tiny_url);

				var qrcode = new KImage(appPanel, 'http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&qzone=1&margin=0&size=320x320&ecc=L&data=' + encodeURIComponent(Kinky.CODA_SHORT_URL + data.id));
				appPanel.appendChild(qrcode);
			}
			
			if(data.id != null){

				var id = new KStatic(appPanel, gettext('$APP_FORM_ID_LABEL') + ' *', 'id');
				{
					id.setValue(data.id);
				}
				appPanel.appendChild(id);
			}

			if(data.secret != null){
				var secret = new KStatic(appPanel, gettext('$APP_FORM_SECRET_LABEL') + ' *', 'secret');
				{
					secret.setValue(data.secret);
				}
				appPanel.appendChild(secret);
			}

			var rest_url = new KInput(appPanel, gettext('$APP_FORM_REST_URL') + ' *', 'rest_url');
			{
				if (!!data && !!data.rest_url) {
					rest_url.setValue(data.rest_url);
				}
				rest_url.setHelp(gettext('$APP_FORM_REST_URL_HELP'), CODAForm.getHelpOptions());
				rest_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			appPanel.appendChild(rest_url);

			if(data.creation_date != null){
				var creation_date = new KStatic(appPanel, gettext('$APP_FORM_CREATION_DATE_LABEL') + ' *', 'creation_date');
				{
					creation_date.setValue(data.creation_date);
				}
				appPanel.appendChild(creation_date);
			}

		}

		var infoPanel = new KPanel(this);
		infoPanel.hash = '/general2';
		{
			var name = new KInput(infoPanel, gettext('$APP_FORM_NAME') + ' *', 'name');
			{
				if (!!data && !!data.name) {
					name.setValue(data.name);
				}
				name.setHelp(gettext('$APP_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			infoPanel.appendChild(name);

			var icon = new KImageUpload(infoPanel, gettext('$APP_FORM_ICON'), 'icon', CODAGenericShell.getUploadAuthorization());
			{
				if (!!data && !!data.icon && data.icon.indexOf('css:') != 0) {
					icon.setValue(data.icon);
				}
				icon.setHelp(gettext('$APP_FORM_ICON_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(icon, 'image/*');
				icon.appendChild(mediaList);	
			}
			infoPanel.appendChild(icon);

			var description = new KTextArea(infoPanel, gettext('$APP_FORM_DESCRIPTION'), 'description');
			{
				if (!!data && !!data.description) {
					description.setValue(data.description);
				}
				description.setHelp(gettext('$APP_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());
			}
			infoPanel.appendChild(description);

		}	
		this.addPanel(infoPanel, gettext('$APP_FORM_INFO_PANEL'), true);
		this.addPanel(appPanel, gettext('$APP_FORM_SETTINGS_PANEL'), false);

		var isLocalizable = true;
		if (!!data.channels) {
			for (var c in data.channels.elements) {
				var channel = data.channels.elements[c];
				var channelPanel = new KPanel(this);
				channelPanel.hash = '/' + channel.type;

				switch(channel.type) {
					case 'facebook' : {
						var fan_gate = new KRadioButton(channelPanel, gettext('$APP_FORM_FAN_GATE'), channel.type + '.fan_gate');
						{
							fan_gate.addOption('yes', gettext('$APP_FORM_FAN_GATE_YES'), channel.fan_gate == 'yes');
							fan_gate.addOption('no', gettext('$APP_FORM_FAN_GATE_NO'), channel.fan_gate == 'no');
							fan_gate.setHelp(gettext('$APP_FORM_FAN_GATE_HELP'), CODAForm.getHelpOptions());
						}
						channelPanel.appendChild(fan_gate);

						isLocalizable = false;

						var docroot = new KInput(channelPanel, gettext('$APP_FORM_FACEBOOK_DOCROOT') + ' *', 'facebook.document_root');
						{
							docroot.setValue(channel.document_root);
							docroot.setHelp(gettext('$APP_FORM_FACEBOOK_DOCROOT_HELP'), CODAForm.getHelpOptions());
							docroot.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
							docroot.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
						}
						channelPanel.appendChild(docroot);

						var page_id = new KStatic(channelPanel, gettext('$APP_FORM_FACEBOOK_PAGE_ID') + ' *', 'facebook.page_id');
						{
							page_id.setValue(channel.page_id);
							page_id.setHelp(gettext('$APP_FORM_FACEBOOK_PAGE_ID_HELP'), CODAForm.getHelpOptions());
							page_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
						}
						channelPanel.appendChild(page_id);

						var page_url = new KStatic(channelPanel, gettext('$APP_FORM_FACEBOOK_PAGE_URL') + ' *', 'facebook.app_url');
						{
							page_url.setValue(channel.app_url);
							page_url.setHelp(gettext('$APP_FORM_FACEBOOK_PAGE_URL_HELP'), CODAForm.getHelpOptions());
							page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
							page_url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
						}
						channelPanel.appendChild(page_url);

						this.page_info = window.document.createElement('div');
						this.page_info.className = ' CODAApplicationFBPageInfo CODAApplicationFBPageInfoSF ';

						var head = document.getElementsByTagName('head')[0];
						var script = document.createElement('script');
						script.type = 'text/javascript';
						script.src = 'https://graph.facebook.com/' + channel.page_id + '?fields=id,link,picture,name,about&callback=Kinky.bunnyMan.widgets[\'' +  this.id + '\'].pageInfo';
						head.appendChild(script);

						break;
					}
					case 'website' : {
						var page_id = new KStatic(channelPanel, gettext('$APP_FORM_MOBILE_ID') + ' *', 'website.referer_domain');
						{
							page_id.setValue(channel.referer_domain);
							page_id.setHelp(gettext('$APP_FORM_WEBSITE_ID_HELP'), CODAForm.getHelpOptions());
							page_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
						}
						channelPanel.appendChild(page_id);

						var page_url = new KStatic(channelPanel, gettext('$APP_FORM_WEBSITE_DOCROOT') + ' *', 'website.document_root');
						{
							page_url.setValue(channel.document_root);
							page_url.setHelp(gettext('$APP_FORM_WEBSITE_DOCROOT_HELP'), CODAForm.getHelpOptions());
							page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
							page_url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
						}
						channelPanel.appendChild(page_url);

						var lang = new KInput(channelPanel, gettext('$APP_FORM_WEBSITE_LOCALE') + ' *', 'website.lang');
						{
							lang.setValue(channel.lang);
							lang.setHelp(gettext('$APP_FORM_WEBSITE_LOCALE_HELP'), CODAForm.getHelpOptions());
							lang.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
						}
						channelPanel.appendChild(lang);

						break;
					}				
					case 'mobile' : {
						var page_id = new KStatic(channelPanel, gettext('$APP_FORM_MOBILE_REFERER') + ' *', 'mobile.referer_domain');
						{
							page_id.setValue(channel.referer_domain);
							page_id.setHelp(gettext('$APP_FORM_MOBILE_ID_HELP'), CODAForm.getHelpOptions());
							page_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
						}
						channelPanel.appendChild(page_id);

						var page_url = new KStatic(channelPanel, gettext('$APP_FORM_MOBILE_PAGE_URL') + ' *', 'mobile.app_url');
						{
							page_url.setValue(channel.app_url);
							page_url.setHelp(gettext('$APP_FORM_FACEBOOK_PAGE_URL_HELP'), CODAForm.getHelpOptions());
							page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
							page_url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
						}
						channelPanel.appendChild(page_url);

						var page_url = new KStatic(channelPanel, gettext('$APP_FORM_MOBILE_DOCROOT') + ' *', 'mobile.document_root');
						{
							page_url.setValue(channel.document_root);
							page_url.setHelp(gettext('$APP_FORM_MOBILE_DOCROOT_HELP'), CODAForm.getHelpOptions());
							page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
							page_url.addValidator(/(^(sftp|ftp|http|https):\/\/[^ "]+$)|(^$)/, gettext('$ERROR_MUST_BE_URL'));
						}
						channelPanel.appendChild(page_url);

						break;
					}				
				}
				
				this.addPanel(channelPanel, gettext('$APP_FORM_' + channel.type.toUpperCase() + '_PANEL'), false);
				
			}
		}

		if (!!isLocalizable) {
			var locales_panel = new KPanel(this);
			locales_panel.hash = '/general3';
			{
				var locales = new KCheckBox(locales_panel, gettext('$APP_FORM_LOCALES'), 'locales');
				{
					var available = Kinky.getDefaultShell().data.locales;
					for (var l in available) {
						var selected = false;
						if (!!data.locales) {
							for (var m in data.locales) {
								if (data.locales[m] == available[l]) {
									selected = true;
									break;
								}
							}
						}

						locales.addOption(available[l], '<img style="width: 28px; padding-right: 5px;" src="' + CODA_VERSION + '/images/flags/' + available[l].split('_')[1].toLowerCase() + '.png"></img>' + gettext('$CODA_LOCALE_' + available[l]), selected);
					}

					if (available.length == 0) {
						locales.addOption(KLocale.LOCALE, gettext('$CODA_LOCALE_' + KLocale.LOCALE), true);
					}
				}
				locales_panel.appendChild(locales);
			}
			this.addPanel(locales_panel, gettext('$APP_FORM_LOCALES_PANEL'), false);
		}
		
		var adminPanel = new CODAApplicationOwnerList(this, {
			links : {
				feed : { href :   '/owners?embed=elements(*(owner))&application_id=' + this.getShell().data.app_id }
			}
		});
		adminPanel.hash = '/administrators';
		this.addPanel(adminPanel, gettext('$APP_FORM_OWNERS_PANEL'), false);

	};

	CODAApplicationForm.addWizardPanel = function(parent)  {
		var appPanel = new KPanel(parent);
		{
			appPanel.hash = '/general';
			appPanel.title = gettext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_TITLE');
			appPanel.description = gettext('$CODA_APPLICATION_WIZ_GENERIC_PANEL_DESC');
			appPanel.icon = 'css:fa fa-info-circle';

			var name = new KInput(appPanel, gettext('$APP_FORM_NAME') + ' *', 'name');
			{
				name.setHelp(gettext('$APP_FORM_NAME_HELP'), CODAForm.getHelpOptions());
				name.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			appPanel.appendChild(name);

			var fan_gate = new KRadioButton(appPanel, gettext('$APP_FORM_FAN_GATE'), 'fan_gate');
			{
				fan_gate.addOption('yes', gettext('$APP_FORM_FAN_GATE_YES'), true);
				fan_gate.addOption('no', gettext('$APP_FORM_FAN_GATE_NO'), false);
				fan_gate.setHelp(gettext('$APP_FORM_FAN_GATE_HELP'), CODAForm.getHelpOptions());
			}
			appPanel.appendChild(fan_gate);

			var rest_url = new KHidden(appPanel, 'rest_url');
			{
				rest_url.hash = '/rest_url';
			}
			appPanel.appendChild(rest_url);
		}

		var secondPanel = new KPanel(parent);
		{
			secondPanel.hash = '/general2';
			secondPanel.title = gettext('$CODA_APPLICATION_WIZ_SHARING_PANEL_TITLE');
			secondPanel.description = gettext('$CODA_APPLICATION_WIZ_SHARING_PANEL_DESC');
			secondPanel.icon = 'css:fa fa-share-square-o';

			var description = new KTextArea(secondPanel, gettext('$APP_FORM_DESCRIPTION') + '*', 'description');
			{
				description.setHelp(gettext('$APP_FORM_DESCRIPTION_HELP'), CODAForm.getHelpOptions());
				description.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			secondPanel.appendChild(description);

			var icon = new KImageUpload(secondPanel, gettext('$APP_FORM_ICON'), 'icon', CODAGenericShell.getUploadAuthorization());
			{
				icon.setHelp(gettext('$APP_FORM_ICON_HELP'), CODAForm.getHelpOptions());
				var mediaList = new CODAMediaList(icon, 'image/*');
				icon.appendChild(mediaList);	
			}
			secondPanel.appendChild(icon);

		}			

		return [appPanel, secondPanel];
	};


	CODAApplicationForm.prototype.setDefaults = function(params){
		delete params.links;
		delete params.requestTypes;
		delete params.responseTypes;
		delete params.rel;
		delete params.restServer;

		delete params.channels;
		delete params.owner;

		if (!!params.facebook) {
			params.facebook.max_participations = -1;
		}
		if (!!params.website) {
			params.website.max_participations = -1;
		}

	};

	CODAApplicationForm.prototype.onPost = CODAApplicationForm.prototype.onCreated = function(data, request){
		this.getShell().dashboard.refresh();
		this.parent.closeMe();
	};

	CODAApplicationForm.prototype.onPut = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_SAVED_SUCCESS'));
	};

	CODAApplicationForm.prototype.onRemove = function(data, request) {
		this.enable();
		this.showMessage(gettext('$CONFIRM_REMOVED_SUCCESS'));
	};

	KSystem.included('CODAApplicationForm');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationOwnerAddWizard(parent, href, layout, parentHref) {
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODAApplicationOwnerAddWizard' ]);
		this.setTitle(gettext('$APP_ADD_OWNER_APP'));
		this.hash = '/apps/owner/add';
	}
}

KSystem.include([
	'CODAWizard',
	'KRadioButton',
	'KDate',
	'KPassword'
], function() {
	CODAApplicationOwnerAddWizard.prototype = new CODAWizard();
	CODAApplicationOwnerAddWizard.prototype.constructor = CODAApplicationOwnerAddWizard;

	CODAApplicationOwnerAddWizard.userInfo = function(data) {
		if (!!data.elements) {
			data = data.elements[0];
		}
		this.childWidget('/owner_href').setValue(data.id)
		this.childWidget('/owner_href').validate();
		this.page_info.style.marginTop = '0px';
		this.page_info.style.textAlign = 'left';
		this.page_info.innerHTML = '<img src="' + data.mug + '"/><b>' + data.name + '</b><br/>';
	};

	CODAApplicationOwnerAddWizard.prototype.mergeValues = function() {
		var params = {};

		for (var v  = 0 ; v != this.values.length; v++) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}

		params.application_id = this.getShell().data.app_id;
		params.role = 'administrator';

		return params;
	};

	CODAApplicationOwnerAddWizard.prototype.onPut = CODAApplicationOwnerAddWizard.prototype.onPost = CODAApplicationOwnerAddWizard.prototype.onCreated = function(data, request) {
		var _shell = this.shell;
		this.getParent('KFloatable').closeMe();
		KMessageDialog.alert({
			message : gettext('$APP_OWNER_ADDED_MESSAGE'),
			shell : _shell,
			modal : true,
			width : 400,
			height : 300
		});		
	};

	CODAApplicationOwnerAddWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				this.kinky.put(this, 'rest:/owners', params);
			}
		}
		catch (e) {

		}
	};

	CODAApplicationOwnerAddWizard.addWizardPanel = function(parent)  {
		var appPanel = new KPanel(parent);
		{
			appPanel.hash = '/general';
			appPanel.onLoadUser = CODAApplicationOwnerAddWizard.userInfo;

			var owner_href = new KAutoCompletion(appPanel, gettext('$APP_OWNER_HREF') + ' *', 'href');
			{
				owner_href.hash = '/owner_href';
				owner_href.setSearchParams({
					service : '/users',
					field : 'email',
					value_field : '_id',
					icon_field : 'mug',
					label_field : 'name'
				});
				owner_href.setHelp(gettext('$APP_OWNER_HREF_HELP'), CODAForm.getHelpOptions());
				owner_href.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			appPanel.appendChild(owner_href);

			appPanel.page_info = window.document.createElement('div');
			appPanel.page_info.className = ' CODAApplicationOwnerInfo ';
			appPanel.page_info.style.textAlign = 'right';
			appPanel.page_info.innerHTML = '<i class="fa fa-level-up pull-right fa-2x"></i>' + gettext('$APP_OWNER_INFO_HELP') + '&nbsp;&nbsp;';
			appPanel.container.appendChild(appPanel.page_info);

		}

		return [ appPanel ];

	};

	KSystem.included('CODAApplicationOwnerAddWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationOwnerList(parent, data, schema) {
	if (parent != null) {
		CODAList.call(this, parent, data, schema, { one_choice : false, add_button : false, edit_button : false });
		this.onechoice = false;
		this.pageSize = 5;
		this.table = {
			"owner.email" : {
				label : gettext('$APP_OWNER_LIST_HREF')
			},
			"owner.name" : {
				label :gettext('$APP_OWNER_LIST_NAME')
			},
			"role" : {
				label :gettext('$APP_OWNER_LIST_ROLE')
			}
		};
	}
}

KSystem.include([
	'CODAList',
	'KLink'
], function() {
	CODAApplicationOwnerList.prototype = new CODAList();
	CODAApplicationOwnerList.prototype.constructor = CODAApplicationOwnerList;

	CODAApplicationOwnerList.prototype.onUserDelete = function() {
		this.list.refreshPages();
		KMessageDialog.alert({
			message : gettext('$APP_OWNER_DELETED_MESSAGE'),
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});
	};

	CODAApplicationOwnerList.prototype.getRequest = function() {
		return undefined;
	};

	CODAApplicationOwnerList.prototype.addItems = function(list) {
		for ( var index in this.elements) {
			var listItem = new CODAListItem(list);
			listItem.index = index;
			listItem.data = this.elements[index];
			listItem.data.requestType = KSystem.getWRMLClass(this.elements[index]);
			listItem.data.schema = KSystem.getWRMLSchema(this.elements[index]);
			listItem.remove = function() {
				var params = {
					href : this.data.href,
					application_id : this.getShell().data.app_id
				}
				this.getParent('CODAList').kinky.remove(this.getParent('CODAList'), 'rest:/owners', params, null, 'onUserDelete');
			};
			list.appendChild(listItem);
		}
	};

	KSystem.included('CODAApplicationOwnerList');
}, Kinky.CODA_INCLUDER_URL);

function CODAApplicationsDashboard(parent, options, apps) {
	if (parent != null) {
		CODAIconDashboard.call(this, parent, options, apps);
		this.columns = 5;
	}
}

KSystem.include([
	'KBubble',
	'CODAIcon',
	'CODAIconDashboard'
], function() {
	CODAApplicationsDashboard.prototype = new CODAIconDashboard();
	CODAApplicationsDashboard.prototype.constructor = CODAApplicationsDashboard;

	CODAApplicationsDashboard.prototype.addPanels = function() {
		CODAIconDashboard.prototype.addPanels.call(this);
	};

	CODAApplicationsDashboard.prototype.getIconFor = function(data) {
		var dummy = window.document.createElement('p');
		dummy.innerHTML = data.description;

		var aditional = '';
		if (!!data.channels && data.channels.elements.length != 0) {
			for (var v in data.channels.elements) {
				var channel = data.channels.elements[v];
				switch (channel.type) {
					case 'facebook' : {
						break;
					}
				}
			}
		}

		var icon = ''
		switch (data.state) {
			case 'created' : {
				icon = 'fa-magic gray';
				break;
			}
			case 'active' : {
				icon = 'fa-check green';
				break;
			}
			case 'no_user_content' : {
				icon = 'fa-ban red';
				break;
			}
			case 'canceled' : {
				icon = 'fa-power-off red';
				break;
			}
			case 'archived' : {
				icon = 'fa-archive purple';
				break;
			}
			default: {
				icon = 'fa-magic gray';
			}
		}

		var ret = new CODAIcon(this, {
			icon : data.icon,
			activate : icon,
			title : data.name,
			description : '<span>' + dummy.textContent + '</span><span><b>ID:</b> ' + data.id + '</span>' + aditional + (!!Kinky.CODA_SHORT_URL ? '<span style="width: 200%; padding: 5px 0px;"><b>' + gettext('$CODA_UNIQUE_URL') + ':</b><a style="text-decoration: none;" target="_blank" href="' + Kinky.CODA_SHORT_URL + data.id + '">' + Kinky.CODA_SHORT_URL + data.id + '</a></span>' : ''),
			actions : [
			{
				icon : 'css:fa fa-chevron-right',
				link : '/modules?app_id=' + data.id,
				desc : 'Gerir'
			}
			]
		});

		if (!!this.getShell().notifications) {
			var notifications = this.getShell().notifications.elements;
			var number = 0;
			var level = 0

			for (var n in notifications) {
				if (notifications[n].target_href == '/applications/' + data.id) {
					number++;
					if (notifications[n].level > level) {
						level = notifications[n].level;
					}
				}
			}
			if (number != 0) {
				KBubble.make(ret, {
					text : number,
					id : '/applications/' + data.id,
					cssClass : 'CODAIconDashboardBubble CODAIconDashboardBubble' + level,
					offsetX : '-0.25em',
					offsetY : '-0.75em',
					gravity : {
						x : 'right',
						y : 'bottom'
					},
					callback : CODAGenericShell.showNotifications
				});				
			}
		}
		return ret;
	};

	KSystem.included('CODAApplicationsDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplications() {
}

KSystem.include([
	'KButton',
	'KLayeredPanel',
	'KPanel'
], function() {
	
	CODAApplications.addWizardPanel = function(parent) {
		var themes = KCache.restore('/applications/themes');
		var categories = {};
		
		for ( var w in themes) {
			if (!categories[themes[w].category]) {
				categories[themes[w].category] = [];
			}
			categories[themes[w].category].push(themes[w]);
		}

		var appPanel = new KPanel(parent);
		{
			appPanel.addCSSClass('CODAApplications');
			appPanel.params = {};

			var accordion = new KLayeredPanel(appPanel);
			{
				accordion.hash = '/apps/categories';
				var first = true;
				for ( var c in categories) {
					var panel = new KPanel(accordion);
					{
						panel.hash = '/apps/categories/' + c;
						for ( var w in categories[c]) {
							var desc = gettext(categories[c][w].name.toUpperCase());
							var button = new KButton(panel, desc, 'app_' + categories[c][w].id, function(event) {
								var bt = KDOM.getEventWidget(event);
								if (!!appPanel.last) {
									appPanel.last.removeCSSClass('CODAApplicationsButtonSelected');
								}
								appPanel.last = bt;
								bt.addCSSClass('CODAApplicationsButtonSelected');
								appPanel.params = {
									theme : bt.theme
								};
							});
							{
								desc = '<b>' + desc + '</b><br><span>' + gettext(categories[c][w].description) + '</span>';
								if (categories[c][w].icon.indexOf('css:') == 0) {
									desc = '<i class="' + categories[c][w].icon.replace('css:', '') + '  fa-2x pull-left"></i>' + desc;
								}

								button.theme = categories[c][w];
								button.setText(desc, true);
								button.addCSSClass('CODAApplicationsButton');
								button.addCSSClass('CODAApplications' + categories[c][w].type + 'Button');
							}
							panel.appendChild(button);
						}
					}
					var ctitle = gettext('$APP_CATEGORY_' + c.toUpperCase());
					accordion.addPanel(panel, ctitle, first);
					first = false;
				}
			}
			appPanel.appendChild(accordion);
		}

		appPanel.validate = function() {
			if (!this.params.theme) {
				this.getParent().showMessage(gettext('$APPS_SELECT_APPLICATIONS_ERROR'), false);
				return false;
			}
			return true;
		};

		return appPanel;
	};
	
	KSystem.included('CODAApplications');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationsShell(parent) {
	if (parent != null) {
		if(typeof(Storage) !== "undefined") {
			localStorage.dashboard = '[]';
		}

		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAApplicationsShell.actions);
		this.hinted = { hint1 : '1.html' };

	}
}

KSystem.include([
	'KButton',
	'KShell',
	'KPanel',
	'KImage',
	'KText',
	'CODAGenericShell',
	'CODAApplicationWizard',
	'CODAApplicationForm',
	'CODAApplications',
	'CODAApplicationsDashboard',
	'HTMLEntities'
], function() {
	CODAApplicationsShell.prototype = new CODAGenericShell();
	CODAApplicationsShell.prototype.constructor = CODAApplicationsShell;

	CODAApplicationsShell.prototype.onLoadApplicationData = function(data, request) {
		if (!!this.data.create_template_id) {
			var tweak_forms = window.document.createElement('script');
			tweak_forms.charset = 'utf-8';
			tweak_forms.src =  '/tweak/' + this.module + '/forms/' + this.data.create_template_id + '.js';
			window.document.getElementsByTagName('head')[0].appendChild(tweak_forms);

			var tweak_css = window.document.createElement('link');
			tweak_css.rel = 'stylesheet';
			tweak_css.type = 'text/css';
			tweak_css.href =  '/tweak/' + this.module + '/css/' + this.data.create_template_id + '.css';
			window.document.getElementsByTagName('head')[0].appendChild(tweak_css);
		}

		CODAGenericShell.prototype.onLoadApplicationData.call(this, data, request);
	};
	
	CODAApplicationsShell.prototype.loadShell = function() {
		this.kinky.get(this, 'rest:/applications/owners/me?state=m/active|no_user_content|created/i&orderBy=active&embed=elements(*(channels,owner))', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadOwners');
	};

	CODAApplicationsShell.prototype.onNotFound = CODAApplicationsShell.prototype.onNoContent = function(data, request) {
		if (request.service.indexOf('/owners') != -1) {
			this.onLoadOwners({ elements : []});
		}
		else if (request.service.indexOf('/themes') != -1) {
			this.onLoadThemes({ elements : []});
		}
		else if (request.service.indexOf('/templates') != -1) {
			this.onLoadTemplates({ elements : []});
		}
		else if (request.service.indexOf('/locales') != -1) {
			this.onLoadLocales({ elements : []});
		}
		else {
			this.onLoadThemes({ elements : []});
		}
	};

	CODAApplicationsShell.prototype.onLoadOwners = function(data, request) {
		this.data.apps = data.elements;
		if(typeof(Storage) !== "undefined") {
			localStorage['apps_owned'] = JSON.stringify(this.data.apps);
		}
		this.kinky.get(this, 'rest:/applications/locales/available', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadLocales');
	};

	CODAApplicationsShell.prototype.onLoadLocales = function(data, request) {
		this.data.locales = data.elements;
		if(typeof(Storage) !== "undefined") {
			localStorage['locales_available'] = JSON.stringify(this.data.locales);
		}
		this.kinky.get(this, 'rest:/themes', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadThemes');
	};

	CODAApplicationsShell.prototype.onLoadThemes = function(data, request) {
		this.data.themes = {};
		for (var t in data.elements) {
			this.data.themes[data.elements[t].id] = data.elements[t];
		}
		KCache.commit('/applications/themes', this.data.themes);
		this.kinky.get(this, 'rest:/templates', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadTemplates');
	};
	
	CODAApplicationsShell.prototype.onLoadTemplates = function(data, request) {
		this.data.templates = {};
		for (var t in data.elements) {
			this.data.providers[data.elements[t].id] = {
				services : data.elements[t].rest_url + '/' + data.elements[t].default_version,
				data : data.elements[t].rest_url + '/' + data.elements[t].default_version
			};
			KConnectionsPool.registerConnector(data.elements[t].id, 'KConnectionREST');
			this.data.templates[data.elements[t].id] = data.elements[t];
		}
		KCache.commit('/applications/templates', this.data.templates);
		this.draw();
	};
	
	CODAApplicationsShell.prototype.draw = function() {
		if (!!this.data.apps && this.data.apps.length != 0) {
			var modulesContainer = new CODAApplicationsDashboard(this, {}, this.data.apps);
			this.appendChild(modulesContainer);
		}
		else {
			var calltoaction = new KText(this);
			{
				calltoaction.setLead(gettext('$APP_NO_ITEMS'));
				calltoaction.setText(gettext('$APP_CALL_TO_ACTION'));
				calltoaction.setStyle({
					width: (KDOM.getBrowserWidth() +40) + 'px',
					height: (KDOM.getBrowserHeight() - 85) + 'px'
				});
				calltoaction.setStyle({
					right : (Math.round(KDOM.getBrowserWidth() / 2) - 200) + 'px'
				}, calltoaction.body);
			}
			this.appendChild(calltoaction);
		}

		var addbt = new KButton(this, gettext('$CODA_APPS_ADD'), 'add', function(event) {
			KDOM.stopEvent(event);
			window.top.location = window.top.document.location.protocol + '//' + window.top.document.location.hostname + '/' + KLocale.LANG + '/add-application/';
		});
		this.addBottomButton(addbt);

		this.setTitle(gettext('$APP_SHELL_TITLE'));

		CODAGenericShell.prototype.draw.call(this);
	};
	
	CODAApplicationsShell.prototype.visible = function() {
		var already = this.display;
		CODAGenericShell.prototype.visible.call(this);
		if (already) {
			return;
		}

		if (!!this.data.create_template_id) {
			KBreadcrumb.dispatchURL({
				hash : '/add'
			});
		}
		else if (!!this.data.subscribe_plan) {
			KBreadcrumb.dispatchURL({
				hash : '/subscribe'
			});
		}
		else if (!!this.data.create_status && this.data.create_status == 'success') {
			window.top.document.location = window.top.document.location.href.split('#')[0] + '#';
			KMessageDialog.alert({
				message : gettext('$CODA_APPS_CREATED_SUCCESS_MESSAGE'),
				shell : this.shell,
				modal : true,
				width : 400,
				height : 300,
				html : true
			});
		}
	};

	CODAApplicationsShell.prototype.add = function() {
		this.makeWizard(new CODAApplicationWizard(this, this.data.create_template_version + '_' + this.data.create_template_id));
	};

	CODAApplicationsShell.prototype.subscribe = function() {
		this.kinky.get(this, 'rest:/products?orderBy=base_reference&base_reference=m/' + KLocale.LANG.toUpperCase() + '\\.(.*)/i', { }, null, 'onLoadProducts');
	};

	CODAApplicationsShell.prototype.onLoadProducts = function(data, request) {
		this.makeWizard(new CODASubsriptionWizard(this, data));
	};

	CODAApplicationsShell.prototype.activateApplication = function(app_id) {
		alert('IMPLEMENT PAYMENT SYSTEM\nACTIVATING ' + app_id);
		this.kinky.put(this, 'rest:/applications/' + app_id, { "active" : true }, Kinky.SHELL_CONFIG[this.shell].headers);		
	};

	CODAApplicationsShell.prototype.onRemove = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location.reload(true);
	};

	CODAApplicationsShell.prototype.onPost = function(data, request) {
	};

	CODAApplicationsShell.prototype.onPut = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location.reload(true);
	};

	CODAApplicationsShell.actions = function(widget, hash) {
		var parts = hash.split('/');
		
		switch (parts[1]) {
			case 'activate': {
				Kinky.getDefaultShell().activateApplication(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'add': {
				Kinky.getDefaultShell().add();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'subscribe': {
				Kinky.getDefaultShell().subscribe();
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	CODAApplicationsShell.CHANNELS = [ "facebook", "backoffice", "iframe", "website", "module" ];

	KSystem.included('CODAApplicationsShell');

}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationSubsriptionWizard(parent, products) {
	if (parent != null) {
		CODAWizard.call(this, parent, ['CODAApplicationSubsriptionWizard']);
		this.setTitle(gettext('$CODA_APPS_SUBSCRIPTION_TITLE'));
		this.hash = '/apps/subscribe';
		this.products = products;
	}
}

KSystem.include([
	'CODAWizard'
], function() {
	CODAApplicationSubsriptionWizard.prototype = new CODAWizard();
	CODAApplicationSubsriptionWizard.prototype.constructor = CODAApplicationSubsriptionWizard;

	CODAApplicationSubsriptionWizard.prototype.onPrevious = function() {	
		switch(this.current) {
			case 1: {
				delete this.choosen;
				this.choosen = undefined;
				this.childWidget('/apps/subscribe/1').childWidget('/products').clear();
				this.providerCode.clear();
				break;
			}
			case 2: {
				this.providerCode.clear();
				if (this.choosen.available_subscriptions.length == 1 && this.choosen.available_subscriptions[0].total_value == 0) {
					this.stepOverPrevious();
				}
				break;
			}
			case 4 : {
				if (this.values[2].paymentProvider_code == 'paypal' || this.values[2].paymentProvider_code == 'free') {
					this.stepOverPrevious();
				}
				break;
			}
		}
	};

	CODAApplicationSubsriptionWizard.prototype.onNext = function() {
		switch(this.current) {
			case 0: {
				if (!this.choosen) {
					this.cancelNext = true;

					var params = this.values[0];
					params.app_id = '/applications/' + this.getShell().data.app_id;
					for (var i in this.products.elements) {
						if (this.products.elements[i]._id == params.product_id) {
							params.subscription_id = this.products.elements[i].base_reference;
							break;
						}
					}

					this.kinky.post(this, 'rest:/applications/subscribe/plan', params);
				}
				else {
					if (this.choosen.available_subscriptions.length == 1 && this.choosen.available_subscriptions[0].total_value == 0) {
						this.stepOverNext();
						var panel = this.childWidget('/apps/subscribe/2');
						panel.title = gettext(this.choosen.available_subscriptions[0].reference);
						var text = '<b>' + gettext('total') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', KSystem.truncateDouble(this.choosen.available_subscriptions[0].total_value, 2)).replace('$CURRENCY', this.choosen.available_subscriptions[0].currency) + '</span><br>';
						if (!!this.values[0].promotional_code && this.values[0].promotional_code != '') {
							text += '<br>' + gettext('$CODA_APPS_SUBSCRIPTION_PROMOCODE_FREE') + '</span><br>';
						}
						else {
							text += '<br>' + gettext('$CODA_APPS_SUBSCRIPTION_FREEMIUM') + '</span><br>';							
						}
						panel.description = text;
						panel.icon = 'css:fa fa-shopping-cart';

						var products = this.childWidget('/apps/subscribe/1').childWidget('/products');
						products.addOption(this.choosen.available_subscriptions[0], 'free', true);
						this.values[1].product_id = this.choosen.available_subscriptions[0];

						this.providerCode.addOption('free', 'Redpanda', true);
						this.providerCode.setStyle({
							display : 'none'
						});

					}
					else {
						var panel = this.childWidget('/apps/subscribe/1');
						var products = panel.childWidget('/products');
						for (var p in this.choosen.available_subscriptions) {
							products.addOption(this.choosen.available_subscriptions[p], '<b style="font-size: 16px;">' + KSystem.truncateDouble(this.choosen.available_subscriptions[p].total_value, 2) + '&nbsp;' + this.choosen.available_subscriptions[p].currency + '</b><br><i class="fa fa-chevron-right"></i> <i>' + gettext(this.choosen.available_subscriptions[p].reference) + '</i><br>&nbsp;', false);
						}
					}
				}
				break;
			}
			case 1: {
				var choosed = this.childWidget('/apps/subscribe/1').childWidget('/products').getValue();

				var panel = this.childWidget('/apps/subscribe/2');
				panel.title = gettext(choosed.reference);
				var text = '<br><b>' + gettext('net_value') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', parseFloat(choosed.net_value).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				if (!!choosed.discount_amount) {
					text += '<b>' + gettext('discount_amount') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', parseFloat(choosed.discount_amount).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				}
				if (!!choosed.discount_percentage) {
					text += '<b>' + gettext('discount_amount') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', (parseFloat(choosed.discount_percentage) * parseFloat(choosed.net_value)).toFixed(2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				}
				text += '<b>' + gettext('vat') + '</b>: <span>' + (parseFloat(choosed.vat) * 100) + '%</span></br>';
				text += '<b>' + gettext('total') + '</b>: <span>' + gettext('$CODA_APPS_SUBSCRIPTION_CURRENCY').replace('$VALUE', KSystem.truncateDouble(choosed.total_value, 2)).replace('$CURRENCY', choosed.currency) + '</span><br>';
				panel.description = text;
				panel.icon = 'css:fa fa-shopping-cart';

				if (parseFloat(KSystem.truncateDouble(choosed.total_value, 2)) == 0) {
					this.providerCode.addOption('free', 'Redpanda', true);
					this.providerCode.setStyle({
						display : 'none'
					});

					panel.description += '<br><span>';
				}
				else {
					this.providerCode.setStyle({
						display : 'block'
					});
					this.providerCode.clear();
					for (var p in this.choosen.providers) {
						if (this.choosen.providers[p].code == 'free' ) {
							continue;
						}
						this.providerCode.addOption(this.choosen.providers[p].code, this.choosen.providers[p].name, false);
					}
				}
				break;
			}
			case 2: {
				if (this.values[2].paymentProvider_code == 'paypal' || this.values[2].paymentProvider_code == 'free') {
					this.stepOverNext();
				}
				break;
			}
			case 3: {
				if (this.values[2].paymentProvider_code == 'free') {
					this.stepOverNext();
				}
				break;
			}
		}
	};

	CODAApplicationSubsriptionWizard.prototype.mergeValues = function() {
		var params = {};

		for (var v in this.values) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}

		if (!!params.product_id && typeof(params.product_id) == 'object' ) {
			params.subscription_id = params.product_id.reference;
			params.product_id = '/products/' + params.product_id.product_id;				
		}
		else {
			for (var i in this.products.elements) {
				if (this.products.elements[i]._id == params.product_id) {
					params.subscription_id = this.products.elements[i].base_reference;
					break;
				}
			}				
		}
		params.terms_accepted = (!!params.terms_accepted && params.terms_accepted.length == 1 ? params.terms_accepted[0] : 'no');
		params.app_id = '/applications/' + this.getShell().data.app_id;
		params.lang = KLocale.LANG.toUpperCase();
		params.locale = KLocale.LOCALE;
		params.format = 'json';

		return params;
	};

	CODAApplicationSubsriptionWizard.prototype.onAccepted = function(data, request) {
		this.choosen = data;
		this.fireNext();
	};

	CODAApplicationSubsriptionWizard.prototype.onAppActivated = function(data, request) {
		window.document.location.reload(true);
	};

	CODAApplicationSubsriptionWizard.prototype.onPost = CODAApplicationSubsriptionWizard.prototype.onCreated = function(data, request) {
		this.kinky.post(this, 'rest:/applications/' + this.getShell().data.app_id + '/activate', { }, null, 'onAppActivated');
	};

	CODAApplicationSubsriptionWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				window.CODA_APPS_WAITING_PAYMENT = this;
				var url = CODA_VERSION + '/modules/payments/payment_ongoing.html?rest_url=' + encodeURIComponent(this.getShell().data.providers.rest.services);
				for (var p in params) {
					url += '&' + p + '=' + encodeURIComponent(params[p]);
				}
				var pup = window.open(url, '*', 'width=1000,height=800,resizable=yes,scrollbars=yes');
				KSystem.popup.check(pup, Kinky.getDefaultShell());
			}
		}
		catch (e) {

		}
	};

	CODAApplicationSubsriptionWizard.addWizardPanel = function(parent)  {
		var typePanel = new KPanel(parent);
		{
			typePanel.hash = '/general';
			typePanel.title = gettext('$CODA_APPS_SUBSCRIPTION_TYPE');
			typePanel.description = gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP2').replace('$LANG', KLocale.LANG);
			typePanel.icon = 'css:fa fa-shopping-cart';

			var product_id = new KRadioButton(typePanel, gettext('$CODA_APPS_SUBSCRIPTION_TYPE') + '*', 'product_id');
			{
				for (var i in parent.products.elements) {
					product_id.addOption(parent.products.elements[i]._id, gettext(parent.products.elements[i].base_reference), false);
				}

				product_id.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP'), CODAForm.getHelpOptions());
				product_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			typePanel.appendChild(product_id);

			var promotionalCode = new KInput(typePanel, gettext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE') + '*', 'promotional_code');
			{
				promotionalCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_PROMOTIONALCODE_HELP'), CODAForm.getHelpOptions());
			}
			typePanel.appendChild(promotionalCode);
		}

		var productsPanel = new KPanel(parent);
		{
			productsPanel.hash = '/general1';
			productsPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_TITLE');
			productsPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP').replace('$LANG', KLocale.LANG);
			productsPanel.icon = 'css:fa fa-shopping-cart';

			var product_id = new KRadioButton(productsPanel, gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT') + '*', 'product_id');
			{
				product_id.hash = '/products';
				product_id.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_PRODUCT_HELP'), CODAForm.getHelpOptions());
				product_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			productsPanel.appendChild(product_id);
		}

		var providerPanel = new KPanel(parent);
		{
			providerPanel.hash = '/general2';

			parent.providerCode = new KRadioButton(providerPanel, gettext('$CODA_APPS_SUBSCRIPTION_PROVIDER') + '*', 'paymentProvider_code');
			{
				parent.providerCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_TYPE_HELP'), CODAForm.getHelpOptions());
				parent.providerCode.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			providerPanel.appendChild(parent.providerCode);

			var termsAccepted = new KCheckBox(providerPanel, '', 'terms_accepted');
			{
				termsAccepted.addOption("yes", '', false);
				termsAccepted.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
				termsAccepted.addCSSClass('CODAApplicationSubsriptionTermsCheck');
			}
			providerPanel.appendChild(termsAccepted);				

			var termsText = new KText(providerPanel); 
			{
				termsText.setStyle({
					marginLeft: '-15px',
					top: '4px'
				});
				termsText.setText(gettext('$CODA_APPS_SUBSCRIPTION_TERMS_ACCEPT'));
				termsText.addCSSClass('CODAApplicationSubsriptionTermsText');
			}
			providerPanel.appendChild(termsText);

		}

		var paymentPanel = new KPanel(parent);
		{
			paymentPanel.hash = '/general3';
			paymentPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_TITLE');
			paymentPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_CREDITCARD_HELP').replace('$LANG', KLocale.LANG);
			paymentPanel.icon = 'css:fa fa-credit-card';

			var cardNumber = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER') + '*', 'card_number');
			{
				cardNumber.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDNUMBER_HELP'), CODAForm.getHelpOptions());
				cardNumber.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardNumber);

			var cardCvv = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDCVV') + '*', 'card_cvv');
			{
				cardCvv.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDCVV_HELP'), CODAForm.getHelpOptions());
				cardCvv.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardCvv);

			var cardHolder = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME') + '*', 'card_holder_name');
			{
				cardHolder.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDHOLDERNAME_HELP'), CODAForm.getHelpOptions());
				cardHolder.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardHolder);

			var cardExpDate = new KInput(paymentPanel, gettext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE') + '*', 'card_expiration_date', 'Y/M', 'Y/M');
			{
				cardExpDate.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_CARDEXPDATE_HELP'), CODAForm.getHelpOptions());
				cardExpDate.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			paymentPanel.appendChild(cardExpDate);

		}

		var addressPanel = new KPanel(parent);
		{
			addressPanel.hash = '/general4';
			addressPanel.title = gettext('$CODA_APPS_SUBSCRIPTION_ADDRESS_TITLE');
			addressPanel.description = gettext('$CODA_APPS_SUBSCRIPTION_ADDRESS_HELP').replace('$LANG', KLocale.LANG);
			addressPanel.icon = 'css:fa fa-envelope';

			var billingAddress = new KTextArea(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS') + '*', 'billing_address');
			{
				billingAddress.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGADDRESS_HELP'), CODAForm.getHelpOptions());
				billingAddress.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingAddress);

			var billingPostalCode = new KInput(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE') + '*', 'billing_postal_code');
			{
				billingPostalCode.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGZIPCODE_HELP'), CODAForm.getHelpOptions());
				billingPostalCode.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingPostalCode);

			var billingCountry = new KAutoCompletion(addressPanel, gettext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY') + '*', 'billing_country');
			{
				for (var c in CODA_COUNTRIES) {
					billingCountry.addOption(getlocale(CODA_COUNTRIES[c].name), getlocale(CODA_COUNTRIES[c].name), false);
				}

				billingCountry.setHelp(gettext('$CODA_APPS_SUBSCRIPTION_BILLINGCOUNTRY_HELP'), CODAForm.getHelpOptions());
				billingCountry.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			}
			addressPanel.appendChild(billingCountry);

		}

		return [ typePanel, productsPanel, providerPanel, paymentPanel, addressPanel ];

	};

	KSystem.included('CODAApplicationSubsriptionWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationWebsiteResolver(parent) {
	if (parent != null) {
		KPanel.call(this, parent);
	}
}

KSystem.include([
	'KPanel'
], function() {
	CODAApplicationWebsiteResolver.prototype = new KPanel();
	CODAApplicationWebsiteResolver.prototype.constructor = CODAApplicationWebsiteResolver;

	CODAApplicationWebsiteResolver.prototype.draw = function() {

		var page_id = new KInput(this, gettext('$APP_FORM_WEBSITE_ID') + ' *', 'website.referer_domain');
		{
			page_id.setHelp(gettext('$APP_FORM_WEBSITE_ID_HELP'), CODAForm.getHelpOptions());
			page_id.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
			page_id.addEventListener('change', function(e) {
				var id = KDOM.getEventWidget(e);
				var  url = id.getParent().childWidget('/page_url');
				var val = id.getValue();
				url.setValue('http://' + val + '/');
			});
		}
		this.appendChild(page_id);

		var page_url = new KInput(this, gettext('$APP_FORM_WEBSITE_DOCROOT') + ' *', 'website.document_root');
		{
			page_url.hash = '/page_url';
			page_url.setHelp(gettext('$APP_FORM_WEBSITE_DOCROOT_HELP'), CODAForm.getHelpOptions());
			page_url.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
		}
		this.appendChild(page_url);

		var lang = new KInput(this, gettext('$APP_FORM_WEBSITE_LOCALE') + ' *', 'website.lang');
		{
			lang.setHelp(gettext('$APP_FORM_WEBSITE_LOCALE_HELP'), CODAForm.getHelpOptions());
			lang.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
		}
		this.appendChild(lang);

		var hash = new KInput(this, gettext('$APP_FORM_WEBSITE_PREFIX') + ' *', 'website.hash');
		{
			hash.setHelp(gettext('$APP_FORM_WEBSITE_PREFIX_HELP'), CODAForm.getHelpOptions());
			hash.addValidator(/(.+)/, gettext('$ERROR_MUST_NOT_BE_NULL'));
		}
		this.appendChild(hash);

		KPanel.prototype.draw.call(this);
	};

	KSystem.included('CODAApplicationWebsiteResolver');
}, Kinky.CODA_INCLUDER_URL);
function CODAApplicationWizard(parent, themeID) {
	if (parent != null) {
		this.theme = KCache.restore('/applications/themes')[themeID];
		this.template = KCache.restore('/applications/templates')[this.theme.template_id]

		var wizscreens = [ 'CODAApplicationForm' ];
		if (!!this.template["facebook"]) {
			wizscreens.push('CODAApplicationFacebookResolver');
		}
		if (!!this.template["website"]) {
			wizscreens.push('CODAApplicationWebsiteResolver');
		}
		if (!!this.theme.wizard) {
			this.includes = [];
			for (var w in this.theme.wizard) {
				wizscreens.push(this.theme.wizard[w].panel);
				this.includes.push(this.theme.wizard[w].panel);
				if (!!this.theme.wizard[w].include) {
					var inclocale = window.document.createElement('script');
					inclocale.charset = 'utf-8';
					inclocale.src = (this.theme.wizard[w].include.indexOf('/modules') == 0 ? CODA_VERSION : '') + this.theme.wizard[w].include.replace('.min.js', '.locale.' + KLocale.LANG + '.min.js');
					window.document.getElementsByTagName('head')[0].appendChild(inclocale);
				}
			}
			for (var w in this.theme.wizard) {
				if (!!this.theme.wizard[w].include) {
					var inc = window.document.createElement('script');
					inc.charset = 'utf-8';
					inc.src = (this.theme.wizard[w].include.indexOf('/modules') == 0 ? CODA_VERSION : '') + this.theme.wizard[w].include;
					window.document.getElementsByTagName('head')[0].appendChild(inc);
				}
			}
		}

		CODAWizard.call(this, parent, wizscreens);
		this.setTitle(gettext('$APP_ADD_NEW') + ' - ' + getlocale(this.theme, 'name'));
		this.hash = '/apps/add';


	}
}

KSystem.include([
	'CODAWizard',
	'CODAApplicationFacebookResolver'
], function() {
	CODAApplicationWizard.prototype = new CODAWizard();
	CODAApplicationWizard.prototype.constructor = CODAApplicationWizard;

	CODAApplicationWizard.prototype.load = function() {
		if (!!this.includes && this.includes.length != 0) {
			var self = this;
			KSystem.include(this.includes, function() {
				var tweak_locale = window.document.createElement('script');
				tweak_locale.charset = 'utf-8';
				tweak_locale.src =  '/tweak/' + self.getShell().module + '/locale/' + self.getShell().data.create_template_id + '.' + KLocale.LANG.toLowerCase() + '.js';
				window.document.getElementsByTagName('head')[0].appendChild(tweak_locale);

				KSystem.include([ 'CODAApplicationsTweak_' + self.getShell().data.create_template_id ], function() {
					self.draw();
				});

				KSystem.addTimer(function() {
					KSystem.included('CODAApplicationsTweak_' + self.getShell().data.create_template_id);
				}, 250);
			});
		}
		else {
			this.draw();
		}
	};

	CODAApplicationWizard.prototype.onClose = function() {
		if (this.created) {
			window.top.document.location = window.top.document.location.href.split('?')[0] + '#cs=success';
		}
		else {
			window.top.document.location = window.top.document.location.href.split('?')[0];
		}
	};

	CODAApplicationWizard.prototype.onPrevious = function() {
	};

	CODAApplicationWizard.prototype.onNext = function() {
	};

	CODAApplicationWizard.prototype.visible = function() {
		CODAWizard.prototype.visible.call(this);
		this.forms[0].childWidget('/rest_url').setValue(this.template.rest_url);
	};

	CODAApplicationWizard.prototype.mergeValues = function() {
		var params = {};
		var ndefaults = !!this.template["facebook"] || !!this.template["website"] ? 3 : 2;

		for (var v  = 0 ; v != ndefaults; v++) {
			for (var f in this.values[v]) {
				params[f] = this.values[v][f];
			}
		}
		params.type = this.theme.template_id;
		if (!params.icon || params.icon == '') {
			params.icon = this.theme.icon;
		}
		if (!params.description || params.description == '') {
			params.description = this.theme.description;
		}

		params.max_participations = -1;

		for (var c in CODAApplicationsShell.CHANNELS) {
			var channel = CODAApplicationsShell.CHANNELS[c];
			if (!!this.template[channel]) {
				if (!params[channel]) {
					params[channel] = {};
				}
				params[channel].theme = this.theme.href;
				params[channel].max_participations = params.max_participations;
				params[channel].fan_gate = params.fan_gate;
			}
		}

		delete params.max_participations;
		delete params.fan_gate;

		var ret = [ params ];
		if (this.values.length > ndefaults) {
			for (var v = ndefaults; v != this.values.length; v++){
				ret.push(this.values[v]);
			}
		}

		return ret;
	};

	CODAApplicationWizard.prototype.onPost = CODAApplicationWizard.prototype.onCreated = function(data, request) {
		this.created = true;
		this.getParent('KFloatable').closeMe();
	};

	CODAApplicationWizard.prototype.send = function(params) {	
		try {
			if (!!params) {
				this.kinky.post(this, this.template.id + ':/applications/by-theme/create', params);
			}
		}
		catch (e) {
		}
	};

	KSystem.included('CODAApplicationWizard');
}, Kinky.CODA_INCLUDER_URL);
function CODAModulesDashboard(parent, options, apps) {
	if (parent != null) {
		CODAIconDashboard.call(this, parent, options, apps);
		this.reducedDescription = true;
		this.columns = 5;
	}
}

KSystem.include([
	'CODAIcon',
	'CODAIconDashboard'
], function() {
	CODAModulesDashboard.prototype = new CODAIconDashboard();
	CODAModulesDashboard.prototype.constructor = CODAModulesDashboard;

	CODAModulesDashboard.prototype.getIconFor = function(data) {
		var dummy = window.document.createElement('p');
		dummy.innerHTML = gettext(data.application.description + ' DESCRIPTION');
		var ret = undefined;
		var notifications = undefined;
		if (!!this.getShell().notifications) {
			notifications = this.getShell().notifications.elements;
		}
		
		if (data.application.name.toLowerCase().indexOf('styling') != -1) {
			var number = 0;
			var level = 0
			if (!!notifications) {
				for (var n in notifications) {
					if (notifications[n].target_href == data.application._id) {
						number++;
						if (notifications[n].level > level) {
							level = notifications[n].level;
						}
					}
				}
			}

			var channels = this.getShell().app_data.channels.elements;
			ret = [];
			for (var k in channels) {
				var ico = new CODAIcon(this, {
					icon : data.application.icon,
					activate : false,
					title : gettext(data.application.name) + gettext('$CODA_STYLING_CHANNEL_' + channels[k].type.toUpperCase() + '_TITLE'),
					description : '<span>' + dummy.textContent + '</span>' ,
					actions : [{
						icon : 'css:fa fa-chevron-right',
						link : data.application.rest_url + '/?app_id=' + data.application_id + '&target_id=' + this.getShell().data.app_id + '&channel_type=' + channels[k].type + '&access_token=' + encodeURIComponent(this.getShellConfig().headers.Authorization.replace('OAuth2.0 ', '')),
						desc : 'Gerir'
					}]
				})

				if (!!notifications) {
					if (number != 0) {
						KBubble.make(ico, {
							text : number,
							cssClass : 'CODAIconDashboardBubble CODAIconDashboardBubble' + level,
							offsetX : '-0.25em',
							offsetY : '-0.75em',
							gravity : {
								x : 'right',
								y : 'bottom'
							}
						});				
					}
				}

				ret.push(ico);
				break;
			}

			return ret;
		}
		else {
			ret = new CODAIcon(this, {
				icon : data.application.icon,
				activate : false,
				title : gettext(data.application.name),
				description : '<span>' + dummy.textContent + '</span>' ,
				actions : [{
					icon : 'css:fa fa-chevron-right',
					link : data.application.rest_url + '/?app_id=' + data.application_id + '&target_id=' + this.getShell().data.app_id + '&access_token=' + encodeURIComponent(this.getShellConfig().headers.Authorization.replace('OAuth2.0 ', '')),
					desc : 'Gerir'
				}]
			});
			
			if (!!notifications) {
				var number = 0;
				var level = 0
				for (var n in notifications) {
					if (notifications[n].target_href == data.application._id) {
						number++;
						if (notifications[n].level > level) {
							level = notifications[n].level;
						}
					}
				}
				if (number != 0) {
					KBubble.make(ret, {
						text : number,
						cssClass : 'CODAIconDashboardBubble CODAIconDashboardBubble' + level,
						offsetX : '-0.25em',
						offsetY : '-0.75em',
						gravity : {
							x : 'right',
							y : 'bottom'
						}
					});				
				}
			}
		}

		return ret;
	};

	KSystem.included('CODAModulesDashboard');
}, Kinky.CODA_INCLUDER_URL);
function CODAModulesShell(parent) {
	if (parent != null) {
		CODAGenericShell.call(this, parent);
		this.addLocationListener(CODAModulesShell.actions);
	}
}

KSystem.include([
	'KButton',
	'KPanel',
	'KImage',
	'KText',
	'CODAGenericShell',
	'CODAApplicationForm',
	'CODAApplicationOwnerAddWizard',
	'CODAModulesDashboard',
	'CODAApplicationSubsriptionWizard',
	'HTMLEntities'
], function() {
	CODAModulesShell.prototype = new CODAGenericShell();
	CODAModulesShell.prototype.constructor = CODAModulesShell;
	
	CODAModulesShell.prototype.loadShell = function() {
		if(typeof(Storage) !== "undefined") {
			this.data.locales = JSON.parse(localStorage['locales_available']);
		}
		else {
			this.data.locales = [ KLocale.LOCALE ];
		}
		this.kinky.get(this, 'rest:/applications/' + this.data.app_id + '/modules', {}, { 'E-Tag' : '' + (new Date()).getTime() }, 'onLoadModules');
	};

	CODAModulesShell.prototype.onNoContent = function(data, request) {
		this.onLoad({ elements : [] });
	};

	CODAModulesShell.prototype.onPreConditionFailed = function(data, request) {
		if (request.service.indexOf('/activate') != -1) {
			this.subscriptionData = data.data;
			KBreadcrumb.dispatchURL({
				hash : '/subscribe-payment/' + this.data.app_id
			});
			return;
		}
		this.draw();
	};

	CODAModulesShell.prototype.onLoadModules = function(data, request) {
		this.data.modules = data.elements;
		this.draw();
	};

	CODAModulesShell.prototype.onRemove = CODAModulesShell.prototype.onRemoveApp = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location = CODAGenericShell.popHistory();
	};

	CODAModulesShell.prototype.onAppActivated = function(data, request) {
		window.document.location.reload(true);
	};

	CODAModulesShell.prototype.onAppCanceled = function(data, request) {
		window.top.document.location.reload(true);
	};

	CODAModulesShell.prototype.draw = function() {
		this.dashboard = new CODAModulesDashboard(this, {}, (!!this.data.modules ? this.data.modules : []));
		this.appendChild(this.dashboard);

		var editbt = new KButton(this, gettext('$APP_EDIT_ACTION'), 'edit-app', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/edit-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
			});
		});
		this.addToolbarButton(editbt, 3);

		var usersbt = new KButton(this, gettext('$APP_ADD_USER_ACTION'), 'add-user-app', function(event) {
			KDOM.stopEvent(event);
			KBreadcrumb.dispatchURL({
				hash : '/user-add-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
			});
		});
		this.addToolbarButton(usersbt, 3);

		if (this.app_data.type != 'backoffice') {
			switch (this.app_data.state) {
				case 'active' : {
					var previewbt = new KButton(this, gettext('$APP_PREVIEW_ACTION'), 'preview-app', function(event) {
						KDOM.stopEvent(event);
						var shell = KDOM.getEventWidget(event).getShell();
						if (!shell.app_data || !shell.app_data.channels || !shell.app_data.channels.elements || shell.app_data.channels.elements.length == 0) {
							return;
						}
						var h = window.document.location.host.split('.');
						var phost = '//prototype.';
						if (h.length > 2) {
							phost += h[1] + '.' + h[2];
						}
						else {
							phost += h;
						}
						phost += '/?referrer_app=' + shell.app_data.id + '&fb_app_id=' + shell.app_data.channels.elements[0].app_id + '&page_id=' + shell.app_data.channels.elements[0].page_id;
						var pup = window.open(phost, "_blank");
						KSystem.popup.check(pup, Kinky.getDefaultShell());
					});
					this.addBottomButton(previewbt, 4);

					var addtopage = new KButton(this, gettext('$APP_ADDTOPAGE_ACTION'), 'add-app-topage', function(event) {
						KDOM.stopEvent(event);
						var shell = KDOM.getEventWidget(event).getShell();
						if (!shell.app_data || !shell.app_data.channels || !shell.app_data.channels.elements || shell.app_data.channels.elements.length == 0) {
							return;
						}
						var phost = 'https://www.facebook.com/dialog/pagetab?app_id=' + shell.app_data.channels.elements[0].app_id + '&redirect_uri=' + encodeURIComponent(shell.app_data.rest_url + '/opengraph/applications/' + shell.app_data.id);
						var pup = window.open(phost, "_blank");
						KSystem.popup.check(pup, Kinky.getDefaultShell());
					});
					this.addBottomButton(addtopage);

					var activtebt = new KButton(this, gettext('$APP_BLOCK_ACTION'), 'block-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/block-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(activtebt, 3);

					var removebt = new KButton(this, gettext('$APP_CANCEL_ACTION'), 'canel-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/cancel-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(removebt, 3);
					break;
				}
				case 'no_user_content' : {
					var previewbt = new KButton(this, gettext('$APP_PREVIEW_ACTION'), 'preview-app', function(event) {
						KDOM.stopEvent(event);
						var shell = KDOM.getEventWidget(event).getShell();
						if (!shell.app_data || !shell.app_data.channels || !shell.app_data.channels.elements || shell.app_data.channels.elements.length == 0) {
							return;
						}
						var h = window.document.location.host.split('.');
						var phost = '//prototype.';
						if (h.length > 2) {
							phost += h[1] + '.' + h[2];
						}
						else {
							phost += h;
						}
						phost += '/?referrer_app=' + shell.app_data.id + '&fb_app_id=' + shell.app_data.channels.elements[0].app_id + '&page_id=' + shell.app_data.channels.elements[0].page_id;
						var pup = window.open(phost, "_blank");
						KSystem.popup.check(pup, Kinky.getDefaultShell());
					});
					this.addBottomButton(previewbt, 4);

					var activtebt = new KButton(this, gettext('$APP_ACTIVATE_ACTION'), 'activate-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/activate-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(activtebt);

					var removebt = new KButton(this, gettext('$APP_CANCEL_ACTION'), 'canel-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/cancel-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(removebt, 3);

					break;
				}
				case 'created' : 
				default : {
					var previewbt = new KButton(this, gettext('$APP_PREVIEW_ACTION'), 'preview-app', function(event) {
						KDOM.stopEvent(event);
						var shell = KDOM.getEventWidget(event).getShell();
						if (!shell.app_data || !shell.app_data.channels || !shell.app_data.channels.elements || shell.app_data.channels.elements.length == 0) {
							return;
						}
						var h = window.document.location.host.split('.');
						var phost = '//prototype.';
						if (h.length > 2) {
							phost += h[1] + '.' + h[2];
						}
						else {
							phost += h;
						}
						phost += '/?referrer_app=' + shell.app_data.id + '&fb_app_id=' + shell.app_data.channels.elements[0].app_id + '&page_id=' + shell.app_data.channels.elements[0].page_id;
						var pup = window.open(phost, "_blank");
						KSystem.popup.check(pup, Kinky.getDefaultShell());
					});
					this.addBottomButton(previewbt, 4);

					var activtebt = new KButton(this, gettext('$APP_ACTIVATE_ACTION'), 'activate-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/activate-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(activtebt);

					var removebt = new KButton(this, gettext('$APP_REMOVE_ACTION'), 'remove-app', function(event) {
						KDOM.stopEvent(event);
						KBreadcrumb.dispatchURL({
							hash : '/remove-app/' +  KDOM.getEventWidget(event).getShell().data.app_id
						});
					});
					this.addBottomButton(removebt, 3);
					break;
				}
			}
		}
		this.setTitle('"' + this.app_data.name + '"');

		CODAGenericShell.prototype.draw.call(this);
	};
	
	CODAModulesShell.prototype.activateApp = function(app_id) {
		this.kinky.post(this, 'rest:/applications/' + this.data.app_id + '/activate', { }, Kinky.SHELL_CONFIG[this.shell].headers, 'onAppActivated');
	};

	CODAModulesShell.prototype.blockApp = function(app_id) {
		this.kinky.post(this, 'rest:/applications/' + this.data.app_id + '/block', { }, Kinky.SHELL_CONFIG[this.shell].headers, 'onAppActivated');
	};

	CODAModulesShell.prototype.cancelApp = function(app_id) {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$APP_CANCEL_CONFIRM_MSG'),
			callback : function() {
				self.kinky.post(self, 'rest:/applications/' + self.data.app_id + '/cancel', { }, Kinky.SHELL_CONFIG[self.shell].headers, 'onAppCanceled');
			},
			shell : this.shell,
			html: true,
			modal : true,
			width : 400,
			height : 300
		});		
	};

	CODAModulesShell.prototype.archiveApp = function(app_id) {
		this.kinky.post(this, 'rest:/applications/' + this.data.app_id + '/archive', { }, Kinky.SHELL_CONFIG[this.shell].headers, 'onAppActivated');
	};


	CODAModulesShell.prototype.activateModule = function(module_id) {
	};

	CODAModulesShell.prototype.deactivateModule = function(module_id) {
	};

	CODAModulesShell.prototype.onCreated = CODAModulesShell.prototype.onPost = function(data, request) {
		CODAGenericShell.popHistory();
		window.document.location.reload(true);
	};

	CODAModulesShell.prototype.editApp = function(app_id) {
		var configForm = new CODAApplicationForm(this, '/applications/' + app_id, null, null);
		{
			configForm.hash = '/applications/edit';
			configForm.setTitle(gettext('$APPS_EDIT_PROPERTIES'));
		}
		var dialog = this.makeDialog(configForm, { minimized : false, modal : true });
	};

	CODAModulesShell.prototype.removeApp = function(app_id) {
		var self = this;
		KConfirmDialog.confirm({
			question : gettext('$APP_DELETE_CONFIRM_MSG'),
			callback : function() {
				self.kinky.remove(self, KSystem.urlify('rest:/applications/' + app_id), {}, null, 'onRemoveApp');
			},
			shell : this.shell,
			modal : true,
			width : 400,
			height : 300
		});	
	};

	CODAModulesShell.prototype.addOwnerApp = function(app_id) {
		this.makeWizard(new CODAApplicationOwnerAddWizard(this));
	};

	CODAModulesShell.prototype.subscribePayment = function() {
		this.kinky.get(this, 'rest:/products?orderBy=base_reference&base_reference=m/' + KLocale.LANG.toUpperCase() + '\\.(.*)/i', { }, null, 'onLoadProducts');
	};

	CODAModulesShell.prototype.onLoadProducts = function(data, request) {
		this.makeWizard(new CODAApplicationSubsriptionWizard(this, data));
	};

	CODAModulesShell.actions = function(widget, hash) {
		var parts = hash.split('/');

		switch (parts[1]) {
			case 'activate': {
				Kinky.getDefaultShell().activateModule(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'deactivate': {
				Kinky.getDefaultShell().deactivateModule(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'edit-app': {
				Kinky.getDefaultShell().editApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'remove-app': {
				Kinky.getDefaultShell().removeApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}	
			case 'user-add-app': {
				Kinky.getDefaultShell().addOwnerApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}			
			case 'activate-app': {
				Kinky.getDefaultShell().activateApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'block-app': {
				Kinky.getDefaultShell().blockApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'cancel-app': {
				Kinky.getDefaultShell().cancelApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'archive-app': {
				Kinky.getDefaultShell().archiveApp(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
			case 'subscribe-payment': {
				Kinky.getDefaultShell().subscribePayment(parts[2]);
				KBreadcrumb.dispatchURL({
					hash : ''
				});
				break;
			}
		}
	};

	CODAModulesShell.CHANNELS = [ "facebook", "backoffice", "iframe", "website", "module", "mobile" ];

	KSystem.included('CODAModulesShell');

}, Kinky.CODA_INCLUDER_URL);
KSystem.include = function(jsClass, callback, includer) {
if (!(jsClass instanceof Array)) {
jsClass = [
jsClass
];
}
for ( var index in jsClass) {
if (!KSystem.includes[jsClass[index]]) {
var include = window.document.createElement('script');
KSystem.includes[jsClass[index]] = {
include : include
};
var url = (!!includer ? includer : Kinky.INCLUDER_URL) + jsClass[index].replace(/\./g, '/') + '.js';
include.charset = 'utf-8';
include.src = url;
window.document.getElementsByTagName('head')[0].appendChild(include);
}
}
if (callback) {
KSystem.addTimer(function() {
KSystem.includeWait(callback, jsClass);
}, 20);
}
};

