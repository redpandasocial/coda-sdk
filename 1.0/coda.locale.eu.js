settext('$CODA_WORKING', '<i class="fa fa-exchange"></i> Working, please wait...', 'eu');
settext('$CODA_HEADER_MENU_ACTVATOR', '<i class="fa fa-bars"></i>', 'eu');
settext('$CODA_SHELL_LOGOUT_BT', '<i class="fa fa-sign-out"></i>Logout', 'eu');
settext('$CODA_SHELL_NOTIFICATIONS_BT', '<i class="fa fa-bell"></i>Notifications', 'eu');
settext('$CODA_CONTACT_US', '<i class="fa fa-bug"></i> FEEDBACK', 'eu');
settext('$CODA_SHELL_BACK_BT', '<i class="fa fa-chevron-left"></i>', 'eu');
settext('$APP_EDIT_ACTION', '<i class="fa fa-wrench"></i> CONFIGS', 'eu');
settext('$APP_ADD_USER_ACTION', '<i class="fa fa-group"></i> ADD ADMIN.', 'eu');
settext('$APP_PREVIEW_ACTION', '<i class="fa fa-eye"></i> Edit', 'eu');
settext('$APP_ACTIVATE_ACTION', '<i class="fa fa-check"></i> Launch', 'eu');
settext('$APP_REMOVE_ACTION', 'REMOVE <i class="fa fa-trash-o"></i>', 'eu');
settext('CODA Users Module DESCRIPTION', 'Module to manage backoffice users.', 'eu');
settext('CODA Users Module', 'Backoffice Users Module', 'eu');
settext('Ideas Module DESCRIPTION', 'Module to manage submitted projects.', 'eu');
settext('Ideas Module', 'Projects Module', 'eu');
settext('$CODA_DELETE_USER_CONFIRM', 'Remove?', 'eu');

settext('$CONFIRM', 'Confirm', 'eu');
settext('$CANCEL', 'Cancel', 'eu');
settext('$ERROR_MUST_NOT_BE_NULL', 'mandatory', 'eu');
settext('$CODA_LIST_NO_ITEMS', 'No records to show.', 'eu');
settext('$CODA_LIST_HEADER_TOTAL_LABEL', 'Total records', 'eu');

settext('$KLISTITEM_EDIT_ACTION', 'Edit', 'eu');
settext('$KLISTITEM_DELETE_ACTION', 'Remove', 'eu');
settext('$KCONFIRMDIALOG_CANCEL_BUTTON_LABEL', 'Cancel', 'eu');
settext('$KCONFIRMDIALOG_OK_BUTTON_LABEL', 'OK', 'eu');

settext('$KPAGEDLIST_PREV_BUTTON', '<i class="fa fa-arrow-left"></i>', 'eu');
settext('$KPAGEDLIST_NEXT_BUTTON', '<i class="fa fa-arrow-right"></i>', 'eu');

settext('$CODA_LIST_FILTER_TIP', 'Click to filter', 'eu');

settext('$FORM_GENERAL_DATA_PANEL', 'General', 'eu');

