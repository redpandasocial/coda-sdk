CODA_NAVIGATION_FROM = 1;

function load() {
	if (KBrowserDetect.browsername == 'Explorer' && KBrowserDetect.version < 9) {
		var lang = 'pt';
	
		if (!!window.top && !!window.top.document && !!window.top.document.location) {
			var uriparts = window.top.document.location.href.split('/');
			for(var index = uriparts.length - 1; index != 0; index--) {
				if (uriparts[index].length == 2) {
					lang = uriparts[index];
				}
			}
		}

		var browsers = [{
			name : 'Chrome',
			version : '31+',
			download : 'http://www.google.com/chrome/',
			icon : 'chrome.png'	
		}, {
			name : 'Firefox',
			version : '26+',
			download : 'http://www.mozilla.org/firefox/new/',
			icon : 'firefox.png'	
		}, {
			name : 'Internet Explorer',
			version : '9+',
			download : 'http://windows.microsoft.com/en-us/internet-explorer/download-ie',
			icon : 'ie.png'	
		}, {
			name : 'Safari',
			version : '5.1+',
			download : 'http://support.apple.com/downloads/#internet',
			icon : 'safari.png'	
		}, {
			name : 'Opera',
			version : '12+',
			download : 'http://www.opera.com/download',
			icon : 'opera.png'	
		}

		];
		var div = window.document.getElementById('loading');
		switch (lang) {
			case 'pt' : {
				div.innerHTML = '<h1>Apesar das nossas aplicações suportarem todos os browsers e versões correntes, a nossa ferramenta de gestão suporta apenas os seguintes:</h1>';
				browsers[2].download = 'http://windows.microsoft.com/pt-pt/internet-explorer/download-ie'
				break;
			}
			case 'br' : {
				div.innerHTML = '<h1>Apesar de nossos aplicativos suportarem todos os browsers e versões correntes, a nossa ferramenta de gestão suporta apenas os seguintes:</h1>';
				browsers[2].download = 'http://windows.microsoft.com/pt-br/internet-explorer/download-ie'
				break;
			}
			case 'us' : {
				div.innerHTML = '<h1>Although our applications support all current browsers and versions, the managing tool only supports the following:</h1>';
				break;
			}
			case 'eu' : {
				div.innerHTML = '<h1>Although our applications support all current browsers and versions, the managing tool only supports the following:</h1>';
				browsers[2].download = 'http://windows.microsoft.com/en-gb/internet-explorer/download-ie'
				break;
			}
			case 'es' : {
				div.innerHTML = '<h1>Although our applications support all current browsers and versions, the managing tool only supports the following:</h1>';
				browsers[2].download = 'http://windows.microsoft.com/es-es/internet-explorer/download-ie'
				break;
			}
		}
		div.innerHTML += '<br><br>';
		for (var b in browsers) {
			div.innerHTML += '<div class="browser"><img src="/1.0/images/browsers/' + browsers[b].icon + '"></img><p>' + browsers[b].name + '<br><b>' + browsers[b].version + '</b><br><a href="' + browsers[b].download + '">download</a></p></div>'
		}

		var iframe = window.document.getElementById('viewport');
		iframe.parentNode.removeChild(iframe);
		return;
	}

	var query = window.document.location.href.split('?');
	var args = null;
	if (query.length == 2) {
		args = deserializeURI(query[1].split('#')[0]);
	}
	else {
		args = {};
	}

	var access_token = null;
	if (!!args.access_token) {
		access_token = args.access_token;
		if(typeof(Storage) !== "undefined") {
			localStorage.access_token = access_token;
		}

		delete args.access_token;
		var otherargs = '?';
		for (var a in args) {
			if (otherargs != '?') {
				otherargs += '&';
			}
			otherargs += a + '=' + encodeURIComponent(args[a]);
		}
		
		window.document.location = query[0] + (otherargs != '?' ? otherargs : '');
	}
	else if(typeof(Storage) !== "undefined") {
		if (localStorage.access_token !== undefined) {
			access_token = localStorage.access_token;
		}
	}

	if (!access_token) {
		window.document.location = '//' + window.parent.document.location.host + '/1.0/login/';
	}

	var channel = null;
	if (window.XMLHttpRequest) { 
		channel = new window.XMLHttpRequest();
	}
	else if (window.ActiveXObject) { 
		try {
			channel = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			try {
				channel = new ActiveXObject("Microsoft.XMLHTTP");
			} 
			catch (e1) {
			}
		}
	}

	channel.onreadystatechange = function() {
		channel.onreadystatechange = function() {
			if (channel.readyState == 4) {
				if (channel.status == 0) {
					return;
				}
 
				if (channel.status != 200) {
					window.document.location = '//' + window.document.location.host + '/1.0/login/';
					return;
				}

				if (channel.responseText != '' && channel.getResponseHeader("Content-Type").indexOf('json') != -1) {
					var data = JSON.parse(channel.responseText);
					window.document.getElementById('viewport').src = '//' + window.document.location.host + '/app-modules/micros-info/?rest_url=/backend/1.0/&token=' + encodeURIComponent(access_token) + '&module_token=' + encodeURIComponent(access_token);
				}
				else {
					return;
				}

			}			
		};

	};

	channel.open('GET', '//' + window.document.location.host + '/api/1.0/users/me?embed=accounts,scopes', true);
	channel.setRequestHeader("Authorization", "OAuth2.0 " + access_token);
	channel.setRequestHeader("Accept", "application/json");
	channel.setRequestHeader("X-Requested-With", "XHR");
	channel.setRequestHeader("Pragma", "no-cache");
	channel.setRequestHeader("Cache-Control", "no-store");
	channel.setRequestHeader("E-Tag", "" + (new Date()).getTime());

	try {
		channel.send(null);
	}
	catch (e) {
	}

}

function deserializeURI(str, options) {
	var pairs = str.split(/&amp;|&/i),
	h = {},
	options = options || {};
	for(var i = 0; i < pairs.length; i++) {
		var kv = pairs[i].split('=');
		kv[0] = decodeURIComponent(kv[0]);
		if(!options.except || options.except.indexOf(kv[0]) == -1) {
			if((/^\w+\[\w+\]$/).test(kv[0])) {
				var matches = kv[0].match(/^(\w+)\[(\w+)\]$/);
				if(typeof h[matches[1]] === 'undefined') {
					h[matches[1]] = {};
				}
				h[matches[1]][matches[2]] = decodeURIComponent(kv[1]);
			} else {
				h[kv[0]] = decodeURIComponent(kv[1]);
			}
		}
	}
	return h;
}

var KBrowserDetect = {
	init : function() {
		this.browsername = this.searchString(this.dataBrowser);
		this.browsername = (!!this.browsername ? this.browsername : "An unknown browser");
		this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || -1;
		this.OS = this.searchString(this.dataOS);
		this.OS = (!!this.OS ? this.OS : "an unknown OS");
		
		if (((this.browsername.indexOf("Firefox") != -1) || (this.browsername.indexOf("Iceweasel") != -1) || (this.browsername.indexOf("Mozilla") != -1)) && (parseInt(navigator.appVersion) >= 5)) {
			this.browser = 1;
		}
		else if ((this.browsername.indexOf("Explorer") != -1) && (parseInt(navigator.appVersion) >= 4)) {
			this.browser = 2;
		}
		else if (this.browsername.indexOf("Opera") != -1) {
			this.browser = 3;
		}
		else if (this.browsername.indexOf("Safari") != -1) {
			this.browser = 4;
		}
		else if (this.browsername.indexOf("Chrome") != -1) {
			this.browser = 5;
		}
	},
	searchString : function(data) {
		for ( var i = 0; i < data.length; i++) {
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1) {
					return data[i].identity;
				}
			}
			else if (dataProp) {
				return data[i].identity;
			}
		}
	},
	searchVersion : function(dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) {
			return null;
		}
		return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	},
	dataBrowser : [
		{
			string : navigator.userAgent,
			subString : "Chrome",
			identity : "Chrome"
		},
		{
			string : navigator.userAgent,
			subString : "OmniWeb",
			versionSearch : "OmniWeb/",
			identity : "OmniWeb"
		},
		{
			string : navigator.vendor,
			subString : "Apple",
			identity : "Safari",
			versionSearch : "Version"
		},
		{
			prop : window.opera,
			identity : "Opera"
		},
		{
			string : navigator.vendor,
			subString : "iCab",
			identity : "iCab"
		},
		{
			string : navigator.vendor,
			subString : "KDE",
			identity : "Konqueror"
		},
		{
			string : navigator.userAgent,
			subString : "Firefox",
			identity : "Firefox"
		},
		{
			string : navigator.vendor,
			subString : "Camino",
			identity : "Camino"
		},
		{ // for newer Netscapes (6+)
			string : navigator.userAgent,
			subString : "Netscape",
			identity : "Netscape"
		},
		{
			string : navigator.userAgent,
			subString : "MSIE",
			identity : "Explorer",
			versionSearch : "MSIE"
		},
		{
			string : navigator.userAgent,
			subString : "Gecko",
			identity : "Mozilla",
			versionSearch : "rv"
		},
		{ // for older Netscapes (4-)
			string : navigator.userAgent,
			subString : "Mozilla",
			identity : "Netscape",
			versionSearch : "Mozilla"
		}
	],
	dataOS : [
		{
			string : navigator.platform,
			subString : "Win",
			identity : "Windows"
		},
		{
			string : navigator.platform,
			subString : "Mac",
			identity : "Mac"
		},
		{
			string : navigator.platform,
			subString : "Linux",
			identity : "Linux"
		}
	]

};

KBrowserDetect.init();
if (window.attachEvent) {
	window.attachEvent('onload', load);
} else {
	window.addEventListener('load', load, false);
}