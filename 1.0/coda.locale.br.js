settext('$CONFIRM_OPERATION_ERROR', '"<img src=\\"/imgs/error.png\\"></img> Uppps, " + gettext("$ERROR_NUMBER_[NUMBER]").replace(\'$FIELDS\', geterrorfields(data))', 'br');

///////////////////////////////////////////////////////////////////
// BACKEND ERROR MESSAGES
settext('$ERROR_NUMBER_undefined', 'a ligação com o servidor perdeu-se.<br>Recarrega a página e tenta outra vez.', 'br');
settext('$ERROR_NUMBER_1073', 'o URL que indicaste não é válido.', 'br');
settext('$ERROR_NUMBER_932', 'o método HTTP utilizado não é compatível com este serviço.', 'br');
settext('$ERROR_NUMBER_926', 'os seguintes campos não têm valores válidos: $FIELDS', 'br');
settext('$ERROR_NUMBER_926_FIELD_TYPE', 'tipo', 'br');
settext('$ERROR_NUMBER_926_FIELD_FIELDS', 'valor', 'br');
settext('$ERROR_NUMBER_927', 'os seguintes campos não têm valores válidos: $FIELDS', 'br');
settext('$ERROR_NUMBER_1025', 'a página do formulário já tem um campo com esse código.', 'br');
settext('$ERROR_NUMBER_1527', 'a página em questão já utilizou o período de gratuitidade. Escolhe outro plano que se adeque às tuas necessidades.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// VALIDATORS ERROR MESSAGES
settext('$CODA_EXCEPTION_FIELDS_MISSING', 'Faltam preencher campos obrigat\u00f3rios', 'br');
settext('$ERROR_MUST_NOT_BE_NULL', 'campo obrigat\u00f3rio', 'br');
settext('$ERROR_MUST_BE_BETWEEN_0_1', 'valor do campo deverá ser um número entre 0 e 1', 'br');
settext('$ERROR_MUST_BE_INTEGER', 'valor do campo deverá ser um número inteiro', 'br');
settext('$ERROR_MUST_BE_DOUBLE', 'valor do campo deverá ser um número decimal', 'br');
settext('$ERROR_MUST_BE_INTEGER_OR_DOUBLE', 'valor do campo deverá ser um número (inteiro ou decimal)', 'br');
settext('$ERROR_DATE_MUST_BE_GREATER_THAN_START_DATE', 'data de fim tem que ser superior à data de início.', 'br');
settext('$ERROR_MUST_BE_URL', 'o valor do campo tem de ser um URL válido (e.g. http://google.com)', 'br');
settext('$ERROR_MUST_BE_EMAIL', 'o valor do campo tem que ser um email válido', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LOGIn
settext('$CODA_LOGIN_USERNAME_LABEL', 'ENDEREÇO DE E-MAIL', 'br');
settext('$CODA_LOGIN_PASSWORD_LABEL', 'PALAVRA-CHAVE', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// USER WELCOME                                      
settext('$CODA_USER_WELCOME_PHRASE', 'Bem-Vindo, ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FILE MANAGEMENT
settext('$KFILE_BUTTON_UPLOAD_TEXT', '<i class="fa fa-upload"></i><span>&nbsp;&nbsp;UPLOAD</span>', 'br');
settext('$KFILE_BUTTON_LABEL_ADD_TEXT', 'ADICIONAR:', 'br');
settext('$KFILE_BUTTON_LABEL_CHANGE_TEXT', 'ALTERAR:', 'br');
settext('$KFILE_NO_IMAGE_THUMB', 'CODAFileUploadNoImage', 'br');

settext('$KIMAGEUPLOAD_BUTTON_UPLOAD_TEXT', '<i class="fa fa-upload"></i><span>&nbsp;&nbsp;UPLOAD</span>', 'br');
settext('$KIMAGEUPLOAD_NO_IMAGE_THUMB', 'CODAImageUploadNoImage', 'br');

settext('$KMEDIALIST_TITLE', 'Servidores Media', 'br');
settext('$KMEDIALIST_BACK', '\u2190', 'br');
settext('$KMEDIALIST_ROOT', 'Root', 'br');
settext('$KMEDIALIST_OPEN', '<i class="fa fa-cloud"></i><span>&nbsp;&nbsp;PROCURAR...</span>', 'br');
settext('$KMEDIALIST_NO_SERVER', 'Escolhe um servidor', 'br');

settext('$KMEDIALIST_CREATEFOLDER', 'Criar', 'br');
settext('$KMEDIALIST_NEWFOLDER', 'Nova Pasta', 'br');
settext('$KMEDIALIST_NEWFILE', 'Adicionar Ficheiro', 'br');

settext('$KMEDIALIST_SELECTEDFILES_OPTIONS', 'Op\u00e7\u00f5es de Ficheiro', 'br');
settext('$KMEDIALIST_REMOVE', 'Remover', 'br');
settext('$KMEDIALIST_EXTERNAL', 'Link Externo', 'br');
settext('$KMEDIALIST_EXTERNAL_TEXT', 'Copia o link que se segue:', 'br');

settext('$KMEDIALIST_RENAME', 'Renomear Ficheiro', 'br');
settext('$KMEDIALIST_RENAME_BT', 'OK', 'br');

settext('$CODA_CLOUD_BROWSER', '<i class="fa fa-cloud-upload"></i> Browser...', 'br');

settext('$CODA_IMAGE_CROP', '<i class="fa fa-resize-full"></i> Recortar...', 'br');

settext('$CODA_USERS_INFO_APPSTORAGE_LINK', '<i class="fa fa-hdd-o"></i> Internal Storage ', 'br');
settext('$CODA_USERS_INFO_SOCIALNETWORK_SEPARATOR', 'Social Media', 'br');
settext('$CODA_USERS_INFO_CLOUDSTORAGE_SEPARATOR', 'Cloud Storage', 'br');

settext('$CODA_USERS_INFO_REFRESH_BUTTON', '<i class="fa fa-refresh"></i> REFRESCAR DADOS', 'br');

settext('$CODA_USERS_INFO_NOT_CONNECTED_DESC', 'Conecta-te para usufruires das seguintes funcionalidades:<br><ul><li>Acesso directo às tuas imagens e videos&nbsp;&nbsp;&bull;</li><li>Acesso à tua lista de contactos&nbsp;&nbsp;&bull;</li><li>Partilha fácil das tuas aplicações&nbsp;&nbsp;&bull;</li><li>Acesso a ficheiros armazenados na cloud&nbsp;&nbsp;&bull;</li><li>Configuração de notificações&nbsp;&nbsp;&bull;</li></ul>', 'br');
settext('$CODA_USERS_INFO_CONNECTED_DESC', 'Agora já podes usufruir de:<ul><li>Acesso directo às tuas imagens e videos&nbsp;&nbsp;&bull;</li><li>Acesso à tua lista de contactos&nbsp;&nbsp;&bull;</li><li>Partilha fácil das tuas aplicações&nbsp;&nbsp;&bull;</li><li>Acesso a ficheiros armazenados na cloud&nbsp;&nbsp;&bull;</li><li>Configuração de notificações&nbsp;&nbsp;&bull;</li></ul>', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MESSAGES
settext('$CODA_WORKING', '<i class="fa fa-exchange"></i> Em comunica\u00e7\u00e3o, por favor, aguarde...', 'br');
settext('$CONFIRM_SAVED_SUCCESS', '<img src="/imgs/success.png"></img>Já está,<br>as alterações foram guardadas ;)', 'br');
settext('$CONFIRM_REMOVED_SUCCESS', '<img src="/imgs/success.png"></img>Já está,<br>as o registo foi removido ;)', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FORMS & DIALOGS

// TITLES & LABELS
settext('$FORM_GENERAL_DATA_PANEL', '<i class="fa fa-adjust"></i> Defini\u00e7\u00f5es Gerais', 'br');
settext('$RESOURCE_PERMISSIONS', '<i class="fa fa-lock"></i> PERMISS\u00d5ES', 'br');
settext('$CODA_FORM_HELP_TIP', 'Clica para acederes à documentação sobre este ecrã.', 'br');

settext('$KRESOURCE_PERMISSIONS_LABEL', 'Podes gerir aqui as permiss\u00f5es', 'br');

settext('$LINK_HREF', 'HREF', 'br');
settext('$LINK_REL', 'URI Relacional', 'br');
settext('$LINK_RESPONSETYPES', 'Formato de Resposta', 'br');
settext('$LINK_REQUESTTYPES', 'Formato de Pedido', 'br');
//----------

// BUTTONS
settext('$REMOVE', 'Remover', 'br');
settext('$CODA_SHELL_ADD_PREFIX', 'ADICIONAR ', 'br');
settext('$NEXT', 'CONTINUAR', 'br');
settext('$PREVIOUS', 'ANTERIOR', 'br');
settext('$CONFIRM', 'CONFIRMAR', 'br');
settext('$CANCEL', 'FECHAR', 'br');
settext('$MORE_OPTIONS', '<i class="fa fa-wrench"></i> MAIS OP\u00c7\u00d5ES...', 'br');
settext('$KCONFIRMDIALOG_OK_BUTTON_LABEL', 'CONFIRMAR', 'br');
settext('$KCONFIRMDIALOG_CANCEL_BUTTON_LABEL', 'CANCELAR', 'br');
settext('$KMESSAGEDIALOG_OK_BUTTON_LABEL', 'OK', 'br');
settext('$KUPLOADBUTTON_UPLOADING_TEXT', 'enviando ficheiro...', 'br');

//----------

// LISTS
settext('$KLIST_ADD_ACTION', 'NOVO ELEMENTO', 'br');

settext('$KPAGEDLIST_PREV_BUTTON', '<i class="fa fa-angle-left"></i>', 'br');
settext('$KPAGEDLIST_NEXT_BUTTON', '<i class="fa fa-angle-right"></i>', 'br');
settext('$KPAGEDLIST_FIRST_BUTTON', '<i class="fa fa-angle-double-left"></i>', 'br');
settext('$KPAGEDLIST_LAST_BUTTON', '<i class="fa fa-angle-double-right"></i>', 'br');

settext('$KLISTITEM_EDIT_ACTION', 'Editar', 'br');
settext('$KLISTITEM_VIEW_ACTION', '<i class="fa fa-eye"></i> Ver detalhes...', 'br');
settext('$KLISTITEM_DELETE_ACTION', 'Remover', 'br');

settext('$CODA_LIST_HEADER_TOTAL_LABEL', 'Nº de registos', 'br');
settext('$CODA_LIST_HEADER_TOTALPAGES_LABEL', 'Nº de páginas', 'br');
settext('$CODA_LIST_FILTER', 'filtrar por texto...', 'br');
settext('$CODA_LIST_NO_ITEMS', 'N\u00e3o existem elementos para mostrar nesta vista', 'br');
settext('$CODA_LIST_DROP_PANEL_TITLE', 'Arrasta elementos da lista para aqui, para executar operações em bulk e de re-ordenação.', 'br');
settext('$CODA_LIST_ADD_BUTTON', '<i class="fa fa-plus"></i> NOVO', 'br');
settext('$CODA_LIST_EXPORT_BUTTON', '<i class="fa fa-share-square-o"></i> EXPORTAR', 'br');
settext('$CODA_LIST_FILTER_TIP', 'Filtar listagem', 'br');
//----------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DASHBOARD
settext('$DASHBOARD_MODULES_DOCROOTS_TITLE', '<i class="fa fa-globe"></i> Websites', 'br');
settext('$DASHBOARD_MODULES_MODULES_TITLE', '<i class="fa fa-suitcase"></i> Módulos de Gestão', 'br');
settext('$DASHBOARD_NOTIFICATIONS', '<i class="fa fa-bell"></i> Notificações', 'br');
settext('$DASHBOARD_CALENDAR', '<i class="fa fa-calendar"></i> Eventos', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TOOLBAR
settext('$CODA_HEADER_MENU_ACTVATOR', '<i class="fa fa-bars"></i>', 'br');
settext('$CODA_SHELL_LOGOUT_BT', '<i class="fa fa-sign-out"></i>Sair', 'br');
settext('$CODA_SHELL_USER_BT', '<i class="fa fa-user"></i>Perfil', 'br');
settext('$CODA_SHELL_APPS_BT', '<i class="fa fa-puzzle-piece"></i>Aplicações', 'br');
settext('$CODA_SHELL_BACK_BT', '<i class="fa fa-chevron-left"></i>', 'br');
settext('$CODA_SHELL_NOTIFICATIONS_BT', '<i class="fa fa-bell"></i>Notificações', 'br');

settext('$CODA_TOOLBAR_FILTER_BUTTON', 'FILTRAR...', 'br');
settext('$CODA_TOOLBAR_FILTERING_TITLE', 'Filtros', 'br');
settext('$CODA_NOTIFICATIONS_TITLE', 'Notificações', 'br');

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// CLOUD BROWSER
settext('$CODA_CLOUD_OPENFOLDER_ACTION', 'Abrir', 'br');
settext('$CODA_CLOUD_USE_ACTION', 'Escolher', 'br');
settext('$CODA_CLOUD_DOWNLOAD_ACTION', 'Descarregar', 'br');
settext('$CODA_CLOUD_UPONLEVEL_ACTION', '..', 'br');
settext('$CODA_CLOUD_UPLOAD_ACTION', 'Enviar ficheiro...', 'br');
settext('$CODA_CLOUD_REMOVE_ACTION', 'Remover ficheiro', 'br');
settext('$CODA_CLOUD_BROWSER_CREATE_SHARED', 'Para poderes utilizar o ficheiro tem que ser criado um Link Partilhado. Desejas continuar?', 'br');
settext('$CODA_CLOUD_BROWSER_MIME_NOTACCEPTED', 'Este tipo de ficheiro não é permitido. Deves escolher um ficheiro do tipo "$MIMETYPE"', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FACEBOOK
settext('$CODA_USERS_NO_FACEBOOK', 'Ainda não estás conectado ao Facebook.', 'br');
settext('$CODA_USERS_NO_FACEBOOK_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_FACEBOOK', 'Já estás conectado ao Facebook.', 'br');
settext('$CODA_USERS_CONNECTED_TO_FACEBOOK_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_FACEBOOK_LINK_BUTTON', '<i class="fa fa-facebook-square"></i> LIGAR-ME AO FACEBOOK <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_FACEBOOK_LINK', '<i class="fa fa-facebook-square"></i> Facebook ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TWITTER
settext('$CODA_USERS_NO_TWITTER', 'Ainda não estás conectado ao Twitter.', 'br');
settext('$CODA_USERS_NO_TWITTER_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_TWITTER', 'Já estás conectado ao Twitter.', 'br');
settext('$CODA_USERS_CONNECTED_TO_TWITTER_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_TWITTER_LINK_BUTTON', '<i class="fa fa-twitter"></i> LIGAR-ME AO TWITTER <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_TWITTER_LINK', '<i class="fa fa-twitter"></i> Twitter ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// GOOGLE
settext('$CODA_USERS_NO_GOOGLE', 'Ainda não estás conectado ao Google+.', 'br');
settext('$CODA_USERS_NO_GOOGLE_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_GOOGLE', 'Já estás conectado ao Google+.', 'br');
settext('$CODA_USERS_CONNECTED_TO_GOOGLE_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_GOOGLE_LINK_BUTTON', '<i class="fa fa-google-plus-square"></i> LIGAR-ME AO GOOGLE+ <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_GOOGLE_LINK', '<i class="fa fa-google-plus-square"></i> Google+ ', 'br');
settext('$CODA_USERS_INFO_YOUTUBE_LINK', '<i class="fa fa-youtube"></i> Youtube ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// INSTAGRAM
settext('$CODA_USERS_NO_INSTAGRAM', 'Ainda não estás conectado ao Instagram.', 'br');
settext('$CODA_USERS_NO_INSTAGRAM_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_INSTAGRAM', 'Já estás conectado ao Instagram.', 'br');
settext('$CODA_USERS_CONNECTED_TO_INSTAGRAM_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_INSTAGRAM_LINK_BUTTON', '<i class="fa fa-instagram"></i> LIGAR-ME AO INSTAGRAM <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_INSTAGRAM_LINK', '<i class="fa fa-instagram"></i> Instagram ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// YAHOO
settext('$CODA_USERS_NO_YAHOO', 'Ainda não estás conectado ao Yahoo.', 'br');
settext('$CODA_USERS_NO_YAHOO_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_YAHOO', 'Já estás conectado ao Yahoo.', 'br');
settext('$CODA_USERS_CONNECTED_TO_YAHOO_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_YAHOO_LINK_BUTTON', '<i class="rp rp-yahoo"></i> LIGAR-ME AO YAHOO <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_YAHOO_LINK', '<i class="rp rp-yahoo"></i> Yahoo ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FLICKR
settext('$CODA_USERS_NO_FLICKR', 'Ainda não estás conectado ao Flickr.', 'br');
settext('$CODA_USERS_NO_FLICKR_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_FLICKR', 'Já estás conectado ao Flickr.', 'br');
settext('$CODA_USERS_CONNECTED_TO_FLICKR_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_FLICKR_LINK_BUTTON', '<i class="fa fa-flickr"></i> LIGAR-ME AO FLICKR <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_FLICKR_LINK', '<i class="fa fa-flickr"></i> Flickr ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// TUMBLR
settext('$CODA_USERS_NO_TUMBLR', 'Ainda não estás conectado ao Tumblr.', 'br');
settext('$CODA_USERS_NO_TUMBLR_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_TUMBLR', 'Já estás conectado ao Tumblr.', 'br');
settext('$CODA_USERS_CONNECTED_TO_TUMBLR_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_TUMBLR_LINK_BUTTON', '<i class="fa fa-tumblr-square"></i> LIGAR-ME AO TUMBLR <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_TUMBLR_LINK', '<i class="fa fa-tumblr-square"></i> Tumblr ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// LINKEDIN
settext('$CODA_USERS_NO_LINKEDIN', 'Ainda não estás conectado ao LinkedIn.', 'br');
settext('$CODA_USERS_NO_LINKEDIN_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_LINKEDIN', 'Já estás conectado ao LinkedIn.', 'br');
settext('$CODA_USERS_CONNECTED_TO_LINKEDIN_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_LINKEDIN_LINK_BUTTON', '<i class="fa fa-linkedin-square"></i> LIGAR-ME AO LINKEDIN <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_LINKEDIN_LINK', '<i class="fa fa-linkedin-square"></i> LinkedIn ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// DROPBOX
settext('$CODA_USERS_NO_DROPBOX', 'Ainda não estás conectado ao Dropbox.', 'br');
settext('$CODA_USERS_NO_DROPBOX_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_DROPBOX', 'Já estás conectado ao Dropbox.', 'br');
settext('$CODA_USERS_CONNECTED_TO_DROPBOX_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_DROPBOX_LINK_BUTTON', '<i class="fa fa-dropbox"></i> LIGAR-ME AO DROPBOX <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_DROPBOX_LINK', '<i class="fa fa-dropbox"></i> Dropbox ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// BOX
settext('$CODA_USERS_NO_BOX', 'Ainda não estás conectado ao Box.', 'br');
settext('$CODA_USERS_NO_BOX_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_BOX', 'Já estás conectado ao Box.', 'br');
settext('$CODA_USERS_CONNECTED_TO_BOX_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_BOX_LINK_BUTTON', '<i class="rp rp-box-rect"></i> LIGAR-ME AO BOX <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_BOX_LINK', '<i class="rp rp-box-rect"></i> Box ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// COPY
settext('$CODA_USERS_NO_COPY', 'Ainda não estás conectado ao Copy.', 'br');
settext('$CODA_USERS_NO_COPY_DESC', 'Liga-te e partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_CONNECTED_TO_COPY', 'Já estás conectado ao Copy.', 'br');
settext('$CODA_USERS_CONNECTED_TO_COPY_DESC', 'Partilha as tuas apps com os teus contactos e chama-os para o Redpanda.', 'br');
settext('$CODA_USERS_COPY_LINK_BUTTON', '<i class="fa fa-cloud"></i> LIGAR-ME AO COPY <i class="fa fa-arrow-right"></i>', 'br');
settext('$CODA_USERS_INFO_COPY_LINK', '<i class="fa fa-cloud"></i> Copy ', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// FEEDBACK
settext('$CODA_FEEDBACK_WIZARD_TITLE', '<i class="fa fa-bug"></i> FEEDBACK', 'br');
settext('$CODA_CONTACT_US', '<i class="fa fa-bug"></i> FEEDBACK', 'br');
settext('$CODA_FEEDBACK_BACKOFFICE_GENERAL', 'Minhas aplicações (my.redpandasocial)', 'br');
settext('$CODA_FEEDBACK_APP_GENERAL', 'Aplicação', 'br');
settext('$CODA_FEEDBACK_GENERAL', 'Outra', 'br');

settext('$CODA_FEEDBACK_SUCCESS_MSG', 'O teu <i>feedback</i> foi armazenado.\nEsperamos ser breves na resposta.\nObrigado.', 'br');

settext('$CODA_FEEDBACK_WIZ_REFERSTO', 'Esta mensagem de feedback refere-se a:', 'br');
settext('$CODA_FEEDBACK_WIZ_REFERSTO_APP', 'Esta mensagem de feedback refere-se à aplicação', 'br');
settext('$CODA_FEEDBACK_WIZ_REFERSTO_BACKOFFICE', 'Esta mensagem de feedback refere-se à área de gestão:', 'br');

settext('APP Promotions', 'Loja de Promoções', 'br');
settext('APP Flash', 'Loja de Vendas Flash', 'br');
settext('APP Store', 'Loja Social', 'br');
settext('APP Contentoffser', 'Oferta de Conteúdos', 'br');
settext('APP Friendsdisccount', 'DesConta com Amigos', 'br');
settext('APP Instagram', 'Feed de Instagram', 'br');
settext('APP Photocontest', 'Concurso de Fotografias', 'br');
settext('APP Quizrankcontest', 'Concurso de Perguntas e Respostas', 'br');
settext('APP Quizanswerpage', 'Quiz de Escolha Múltipla', 'br');
settext('APP Videocontest', 'Concurso de Vídeos', 'br');

settext('$CODA_FEEDBACK_WIZ_REFERSTO_HELP', 'Seleciona a área a que se refere a tua mensagem de feedback.', 'br');
settext('$CODA_FEEDBACK_WIZ_KIND', 'Tipo de mensagem de feedback ', 'br');
settext('$CODA_FEEDBACK_BUG', 'Erro', 'br');
settext('$CODA_FEEDBACK_SUGGESTION', 'Sugestão/Melhoramento', 'br');
settext('$CODA_FEEDBACK_PROPOSAL', 'Pedido de nova funcionalidade', 'br');
settext('$CODA_FEEDBACK_WIZ_KIND_HELP', 'Indique o tipo de feedback:<br>- <i>Erro</i>: encontraste um erro na plataforma (ex: um link quebrado, uma página que fica em branco, etc).<br>- <i>Sugestão/Melhoramento</i>: tens uma ideia de como melhorar uma funcionalidade existente.<br>- <i>Pedido de funcionalidade</i>: sentes falta de uma funcionalidade que achas útil para a plataforma.', 'br');
settext('$CODA_FEEDBACK_WIZ_TITLE', 'Título da mensagem', 'br');
settext('$CODA_FEEDBACK_WIZ_TITLE_HELP', 'Indica o título da tua mensagem, uma pequena frase que resuma a tua mensagem.', 'br');
settext('$CODA_FEEDBACK_WIZ_DESC', 'Descrição da mensagem', 'br');
settext('$CODA_FEEDBACK_WIZ_DESC_HELP', 'Descreve o Erro/Sugestão/Pedido de funcionalidade o mais detalhadamente que conseguires, providenciando toda a informação que permita contextualizar o erro/sugestão/pedido.', 'br');
settext('$CODA_FEEDBACK_WIZ_NAME', 'O teu nome', 'br');
settext('$CODA_FEEDBACK_WIZ_NAME_HELP', 'Introduz o nome pelo qual desejas ser identificado nas respostas a esta mensagem.', 'br');
settext('$CODA_FEEDBACK_WIZ_EMAIL', 'O teu endereço de e-mail', 'br');
settext('$CODA_FEEDBACK_WIZ_EMAIL_HELP', 'Introduz o endereço de e-mail onde desejas receber as respostas a esta mensagem.', 'br');

settext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_TITLE', 'Categorização da mensagem de Feedback', 'br');
settext('$CODA_FEEDBACK_WIZ_CATEGORY_PANEL_DESC', 'Indica a área da plataforma a que se refere a tua mensagem.<br>Indica o tipo de feedback a que a tua mensagem se refere.', 'br');
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_PANEL_TITLE', 'Categorização da mensagem de Feedback', 'br');
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_APP_PANEL_DESC', 'Indica a aplicação a que se refere a tua mensagem', 'br');
settext('$CODA_FEEDBACK_WIZ_CATEGORY2_BACKOFFICE_PANEL_DESC', 'Indica a área de gestão a que se refere a tua mensagem', 'br');
settext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_TITLE', 'Mensagem', 'br');
settext('$CODA_FEEDBACK_WIZ_MESSAGE_PANEL_DESC', 'Descreve o teu feedback indicando o título e corpo da mensagem.', 'br');
settext('$CODA_FEEDBACK_WIZ_USER_PANEL_TITLE', 'Dados de Contacto', 'br');
settext('$CODA_FEEDBACK_WIZ_USER_PANEL_DESC', 'Indica o nome e endereço de e-mail que desejas utilizar nesta interação.', 'br');

settext('$CODA_FEEDBACK_WIZ_SCREENSHOT', 'Captura de ecrã (screenshot)', 'br');
settext('$CODA_FEEDBACK_WIZ_SCREENSHOT_HELP', 'Junta à tua mensagem de feedback uma imagem com a captura de ecrã (screenshot) que ajude a compreender a tua sugestão/problema.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// MODULES DASH
settext('CODA Applications Module', 'Aplicações', 'br');
settext('CODA MyInfo Module', 'Perfil de Utilizador', 'br');
settext('CODA Accounts Module DESCRIPTION', 'Vê quem já participou e o detalhe de cada participações.<br> Marca quais são as participações vencedoras e atribui-lhes prémios.<br>', 'br');
settext('CODA Accounts Module', 'Participantes e Participações', 'br');
settext('CODA Users Module DESCRIPTION', 'Cria, apaga e altera os utilizadores da tua aplicação bem como a forma como acedem ao sistema.', 'br');
settext('CODA Users Module', 'Utilizadores', 'br');
settext('CODA CMS Module DESCRIPTION', 'Altera o aspecto da tua aplicação, compõe os conteúdos que irão aparecer, adiciona páginas e outros elementos gráficos e publica.', 'br');
settext('CODA CMS Module', 'Gestão de Conteúdos', 'br');
settext('CODA Campaigns Module DESCRIPTION', 'Define a calendarização para a tua aplicação e configura os prémios que os participantes vão receber.', 'br');
settext('CODA Campaigns Module', 'Calendarização e Prémios', 'br');
settext('CODA Forms Module DESCRIPTION', 'Configura formulários definindo as suas páginas e campos', 'br');
settext('CODA Forms Module', 'Formulários', 'br');
settext('CODA Payments Module DESCRIPTION', 'Consulta e manda executar pagamentos', 'br');
settext('CODA Payments Module', 'Pagamentos', 'br');
settext('CODA Products Module DESCRIPTION', 'Faz a gestão dos produtos que queres disponibilizar na tua aplicação', 'br');
settext('CODA Products Module', 'Produtos', 'br');
settext('CODA Vouchers Module DESCRIPTION', 'Configura os vouchers que vais atribuir.<br> Consulta os que já foram atribuídos.<br> Dá baixa dos que forem sendo utilizados.', 'br');
settext('CODA Vouchers Module', 'Vouchers', 'br');
settext('CODA Styling Module DESCRIPTION', 'Altera o aspecto gráfico da tua aplicação e edita os textos e títulos.', 'br');
settext('CODA Styling Module', 'Estilos e Grafismo', 'br');
settext('CODA Social Integration DESCRIPTION', 'Configura o formato de integração da aplicação com as várias redes sociais.', 'br');
settext('CODA Social Integration', 'Integração com Redes Sociais', 'br');
settext('CODA Templates & Themes DESCRIPTION', 'Cria e configura os Templates e Themes de aplicações', 'br');
settext('CODA Templates & Themes', 'Templates & Themes', 'br');
settext('CODA Statistics DESCRIPTION', 'Acesso às estatísticas do aplicativo: utilizadores da aplicação e acesso a páginas.', 'br');
settext('CODA Statistics', 'Estatísticas', 'br');
settext('CODA Store Module DESCRIPTION', 'Adicione e configure as Gateways de Pagamento para os pagamentos efectuados na sua aplicação.', 'br');
settext('CODA Store Module', 'Gateways de Pagamento', 'br');
settext('$CODA_STYLING_CHANNEL_FACEBOOK_TITLE', '&nbsp;', 'br');
settext('REDPANDA Quiz Module DESCRIPTION', 'Gere as perguntas e as respostas do teu quiz.<br> Extrai a lista de perguntas e respostas para veres todos os dados das que já configuraste', 'br');
settext('REDPANDA Quiz Module', 'Perguntas e Respostas', 'br');
settext('Cardapio Module DESCRIPTION', 'Gerir cardápios do restaurante.', 'br');
settext('Cardapio Module', 'Cardápios', 'br');
settext('Info Module DESCRIPTION', 'Gestão de mesas, salas, garçon e publicidade.', 'br');
settext('Info Module', 'Restaurante', 'br');
settext('Data Module DESCRIPTION', 'Gestão de pedidos recebidos e consulta de informação agregada sobre a operação do restaurante.', 'br');
settext('Data Module', 'Operação', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

KCalendar.localization = KCalendar.localization || {};
KCalendar.localization['weekDayName'] = new Array('Domingo', 'Segunda', 'Ter&ccedil;a', 'Quarta', 'Quinta', 'Sexta', 'S&aacute;bado');
KCalendar.localization['monthName'] = new Array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');

///////////////////////////////////////////////////////////////////
// LOCALES
settext('$CODA_LOCALE_pt_PT', 'Português (Portugal)', 'br');
settext('$CODA_LOCALE_pt_BR', 'Português (Brasil)', 'br');
settext('$CODA_LOCALE_en_US', 'Inglês (E.U.A.)', 'br');
settext('$CODA_LOCALE_en_GB', 'Inglês (Reino Unido)', 'br');
settext('$CODA_LOCALE_es_ES', 'Castelhano (Espanha)', 'br');
settext('$CODA_LOCALE_es_MX', 'Castelhano (México)', 'br');

// REGIONS
settext('$CODA_COUNTRY_pt_PT', 'Portugal', 'br');
settext('$CODA_COUNTRY_pt_BR', 'Brasil', 'br');
settext('$CODA_COUNTRY_en_US', 'E.U.A.', 'br');
settext('$CODA_COUNTRY_en_GB', 'Reino Unido', 'br');
settext('$CODA_COUNTRY_es_ES', 'Espanha', 'br');
settext('$CODA_COUNTRY_es_MX', 'México', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

///////////////////////////////////////////////////////////////////
// GENERAL
settext('$CODA_NOTIFICATIONS_MARK_ALL_READ', 'MARCAR TUDO COMO LIDO', 'br');
settext('$CODA_NOTIFICATIONS_EMPTY', 'Sem notificações pendentes.', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//////////////////////////////////////////////////////////////////
// PERMISSIONS
settext('$CODA_NOTIFICATIONS_APPLICATION_ADDED', 'Nova aplicação criada', 'br');
settext('$CODA_NOTIFICATION_APPLICATION_ADDED_MESSAGE', 'A aplicação «$APP_NAME» foi criada com sucesso. Já podes pré-visualizar, alterar e publicar a tua nova aplicação.', 'br');
settext('$CODA_NOTIFICATIONS_PERMS_ADMIN_ADDED', 'Permissões de administração concedidas!', 'br');
settext('$CODA_NOTIFICATION_PERMS_ADMIN_ADDED_MESSAGE', 'Foram-te concedidas permissões de adminsitração para a aplicação «$APP_NAME»', 'br');
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


